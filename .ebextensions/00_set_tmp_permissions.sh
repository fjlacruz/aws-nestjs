files:
  "/opt/elasticbeanstalk/hooks/appdeploy/pre/00_set_tmp_permissions.sh":
    mode: "000755"
    owner: root
    group: root
    content: |
      #!/usr/bin/env bash
      chown -R ec2-user /var
      chown -R $USER:$(id -gn $USER) /var/.config
      chown -R nodejs:nodejs /var/.npm