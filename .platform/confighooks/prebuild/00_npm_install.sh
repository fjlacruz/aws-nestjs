#!/bin/bash
cd /var/app/current
echo "================================================================================================================================================================================"
echo "==================================================================/var/app/current=============================================================================================="
echo "================================================================================================================================================================================"
ls
echo "================================================================================================================================================================================"
echo "================================================================================================================================================================================"
echo "================================================================================================================================================================================"
sudo chmod -R 777 /var/app/current
cd /var/app/current
sudo npm cache clean --force
sudo rm -rf node_modules
sudo rm package-lock.json yarn.lock
sudo -u webapp npm install sharp
sudo npm install