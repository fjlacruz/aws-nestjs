import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional } from "class-validator";

export class ClaseLicenciaRequeridaTipoTramiteDTO {
  
    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    idClaseLicenciaRequeridaTipoTramite: number;
    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    idOficina: number;
    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    idTipoTramite: number;
    @ApiPropertyOptional()
    @IsNotEmpty()
    @IsNumber()
    idClaseLicencia: number;
    @ApiPropertyOptional()
    @IsNotEmpty()
    @IsNumber()
    antiguedadAnos: number;
}