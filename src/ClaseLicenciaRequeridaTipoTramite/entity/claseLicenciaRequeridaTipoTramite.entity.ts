import { ApiProperty } from "@nestjs/swagger";
import { ClasesLicenciasEntity } from "src/clases-licencia/entity/clases-licencias.entity";
import { TiposTramiteEntity } from "src/tipos-tramites/entity/tipos-tramite.entity";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import { TramitesEntity } from "src/tramites/entity/tramites.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne} from "typeorm";

@Entity('ClaseLicenciaRequeridaTipoTramite')
export class ClaseLicenciaRequeridaTipoTramite {

    @PrimaryGeneratedColumn()
    idClaseLicenciaRequeridaTipoTramite: number;

    @Column()
    idOficina: number;
    @JoinColumn({ name: 'idOficina' })
    @ManyToOne(() => OficinasEntity, oficinas => oficinas.licenciaRequerida)
    readonly oficinas: OficinasEntity;

    @Column()
    idTipoTramite: number;
    @JoinColumn({ name: 'idTipoTramite' })
    @ManyToOne(() => TiposTramiteEntity, tiposTramite => tiposTramite.licenciaRequerida)
    readonly tiposTramite: TiposTramiteEntity;

    @Column()
    idClaseLicencia: number;
    @JoinColumn({ name: 'idClaseLicencia' })
    @ManyToOne(() => ClasesLicenciasEntity, clasesLicencias => clasesLicencias.licenciaRequerida)
    readonly clasesLicencias: ClasesLicenciasEntity;

    @Column()
    antiguedadAnos: number;
}
