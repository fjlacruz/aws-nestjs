import { Body, Controller, Delete, Get, Param, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { Page, PageRequest } from '../anotaciones-resolucion/pagination-anotaciones-resolucion.entity';
import { HeaderUtil } from '../base/base/header-util';
import { AuthGuard } from '@nestjs/passport';
import { Post as PostMethod } from '@nestjs/common/decorators/http/request-mapping.decorator';
import { AnotacionesResolucionService } from './anotaciones-resolucion.service';
import { AnotacionesResolucionEntity } from './anotaciones-resolucion.entity';

@ApiTags('Tipo Unidad Monetaria')
@Controller('anotaciones-resolucion')
export class AnotacionesResolucionController {
  constructor(private readonly anotacionesResolucionService: AnotacionesResolucionService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all records',
    type: AnotacionesResolucionEntity,
  })
  async getAll(@Req() req: Request): Promise<AnotacionesResolucionEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.anotacionesResolucionService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una Sentencia por Id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: AnotacionesResolucionEntity,
  })
  async getOne(@Param('id') id: string): Promise<AnotacionesResolucionEntity> {
    return await this.anotacionesResolucionService.findById(id);
  }

  @PostMethod('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea una nueva Sentencia' })
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    type: AnotacionesResolucionEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() anotacionesResolucionEntity: AnotacionesResolucionEntity): Promise<AnotacionesResolucionEntity> {
    const created = await this.anotacionesResolucionService.save(anotacionesResolucionEntity);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: AnotacionesResolucionEntity,
  })
  async put(@Req() req: Request, @Body() anotacionesResolucionEntity: AnotacionesResolucionEntity): Promise<AnotacionesResolucionEntity> {
    return await this.anotacionesResolucionService.update(anotacionesResolucionEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: AnotacionesResolucionEntity,
  })
  async putId(@Req() req: Request, @Body() anotacionesResolucionEntity: AnotacionesResolucionEntity): Promise<AnotacionesResolucionEntity> {
    return await this.anotacionesResolucionService.update(anotacionesResolucionEntity);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Elimina una Sentencia' })
  @ApiResponse({
    status: 204,
    description: 'The record has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
    return await this.anotacionesResolucionService.deleteById(id);
  }
}
