import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { TipoAnotacionEntity } from '../tipo-anotacion/tipo-anotacion.entity';

@Entity('AnotacionesResolucion')
export class AnotacionesResolucionEntity {
  @PrimaryGeneratedColumn()
  idAnotacionesResolucion: number;
  @Column()
  @ApiModelProperty()
  idTipoAnotaciones: number;
  @JoinColumn({name: 'idTipoAnotaciones'})
  @ManyToOne(() => TipoAnotacionEntity, roles => roles.idTipoAnotaciones, { eager: false })
  readonly tipoAnotacionEntity: TipoAnotacionEntity;
  @Column()
  @ApiModelProperty()
  observacion: string;
  @Column()
  @ApiModelProperty()
  created_at: Date;
  @Column()
  @ApiModelProperty()
  ingresadaPor: number;
  @Column()
  @ApiModelProperty()
  idSentencias: number;
}
