import { Module } from '@nestjs/common';
import { AnotacionesResolucionController } from './anotaciones-resolucion.controller';
import { AnotacionesResolucionService } from './anotaciones-resolucion.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnotacionesResolucionEntity } from './anotaciones-resolucion.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AnotacionesResolucionEntity])],
  controllers: [AnotacionesResolucionController],
  providers: [AnotacionesResolucionService],
  exports: [AnotacionesResolucionService],
})
export class AnotacionesResolucionModule {}
