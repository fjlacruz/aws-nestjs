import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { AnotacionesResolucionEntity } from './anotaciones-resolucion.entity';

const relationshipNames = [];

@Injectable()
export class AnotacionesResolucionService {
  constructor(
    @InjectRepository(AnotacionesResolucionEntity)
    private readonly anotacionesResolucionEntityRepository: Repository<AnotacionesResolucionEntity>
  ) {}

  async findById(id: string): Promise<AnotacionesResolucionEntity | undefined> {
    const options = { relations: relationshipNames };
    const result = await this.anotacionesResolucionEntityRepository.findOne(id, options);
    return result;
  }

  async findByfields(options: FindOneOptions<AnotacionesResolucionEntity>): Promise<AnotacionesResolucionEntity | undefined> {
    const result = await this.anotacionesResolucionEntityRepository.findOne(options);
    return result;
  }

  async findAndCount(options: FindManyOptions<AnotacionesResolucionEntity>): Promise<[AnotacionesResolucionEntity[], number]> {
    options.relations = relationshipNames;
    const resultList = await this.anotacionesResolucionEntityRepository.findAndCount(options);
    const anotacionesResolucionEntities: AnotacionesResolucionEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(anotacion => anotacionesResolucionEntities.push(anotacion));
      resultList[0] = anotacionesResolucionEntities;
    }
    return resultList;
  }

  async save(sentenciaEntity: AnotacionesResolucionEntity): Promise<AnotacionesResolucionEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.anotacionesResolucionEntityRepository.save(entity);
    return result;
  }

  async update(sentenciaEntity: AnotacionesResolucionEntity): Promise<AnotacionesResolucionEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.anotacionesResolucionEntityRepository.save(entity);
    return result;
  }

  async deleteById(id: string): Promise<void | undefined> {
    await this.anotacionesResolucionEntityRepository.delete(id);
    const entityFind = await this.findById(id);
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
