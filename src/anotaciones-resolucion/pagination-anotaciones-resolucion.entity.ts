import { BaseEntity } from '../base/base/base.entity';
import { Type, Expose as JsonProperty } from 'class-transformer';

export class Sort {
  public property: string;
  public direction: 'ASC' | 'DESC' | string;
  constructor(sort: string) {
    if (sort) {
      [this.property, this.direction] = sort.split(',');
    }
  }

  asOrder(): any {
    const order = {};
    order[this.property] = this.direction;
    return order;
  }
}

export class PageRequest {
  @JsonProperty()
  page = 0;
  @JsonProperty()
  size = 20;
  @Type(() => Sort)
  sort: Sort = new Sort('idAnotacionesResolucion,ASC');

  constructor(page: number | string | any, size: number | string | any, sort: string | number | any) {
    this.page = +page || this.page;
    this.size = +size || this.size;
    this.sort = sort ? new Sort(sort) : this.sort;
  }
}

export class PageBaseEntity<T extends BaseEntity> {
  constructor(public content: T[], public total: number, public pageable: PageRequest) {}
}

export class Page<T> {
  constructor(public content: T[], public total: number, public pageable: PageRequest) {}
}
