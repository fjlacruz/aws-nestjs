import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { ServiciosComunesService } from 'src/shared/services/ServiciosComunes/ServiciosComunes.services';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { User } from 'src/users/entity/user.entity';
import { ApelacionesController } from './controller/apelaciones.controller';
import { ApelacionesEntity } from './entities/apelaciones.entity';
import { DocsApelacionesEntity } from './entities/docs-apelaciones.entity';
import { ApelacionesService } from './services/apelaciones.service';

@Module({
  imports: [ TypeOrmModule.forFeature([ TramitesClaseLicencia, 
                                        ApelacionesEntity, 
                                        DocsApelacionesEntity, 
                                        ColaExaminacionEntity, 
                                        TramitesEntity, 
                                        TiposTramiteEntity,
                                        EstadosTramiteEntity,
                                        estadosExaminacionEntity,
                                        TipoExaminacionesEntity,
                                        ClasesLicenciasEntity,
                                        EstadosSolicitudEntity,
                                        User,
                                        RolesUsuarios])],
  controllers: [ApelacionesController],
  providers: [ApelacionesService, ServiciosComunesService, AuthService],
  exports: [ApelacionesService]
})
export class ApelacionesModule {}
