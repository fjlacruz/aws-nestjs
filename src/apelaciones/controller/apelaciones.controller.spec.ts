import { Test, TestingModule } from '@nestjs/testing';
import { ApelacionesController } from './apelaciones.controller';

describe('ApelacionesController', () => {
  let controller: ApelacionesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApelacionesController],
    }).compile();

    controller = module.get<ApelacionesController>(ApelacionesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
