import { Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApelacionesDTO } from '../dto/apelaciones.dto';
import { DocsApelacionesDTO } from '../dto/docs-apelaciones.dto';
import { ApelacionesService } from '../services/apelaciones.service';

@Controller('apelaciones')
@ApiTags('Apelaciones')
@ApiBearerAuth()
export class ApelacionesController {
  constructor(private apelacionesService: ApelacionesService) {}

  @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes por run' })
  @Get('solicitudes/:run')
  async getSolicitudes(
    @Param('run') run: number = null,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.apelacionesService.solicitudes(
      Number(run),
      {
        page,
        limit,
        route: '/registro-pago/allEstadoRegistroPago',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    //const join = [];
    //for (const solicitud of data.items) {
    //const examinaciones = await this.apelacionesService.examinaciones(solicitud.idTramite);

    //for (const examinacion of examinaciones) join.push({ ...solicitud, examinacion });
    //}
    //data.items = join;
    return data;
  }

  // Método para recoger las examinaciones para los apartados preidoneidad moral e idoneidad moral
  @Get('tramitesParaApelacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getTramitesParaApelacion(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any, //CreateColaExaminacionDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Query('recuperarHistorial') recuperarHistorial: boolean,
    @Req() req: any
  ) {
    //const idMunicipalidad = this.authService.oficina().idOficina;
    const data = await this.apelacionesService.getSolicitudesApelaciones(
      req,
      query.idSolicitud,
      query.idEstadoSolicitud,
      query.idTramite,
      query.idEstadoTramite,
      query.RUN,
      query.tipoTramite,
      query.claseLicencia,
      query.fechaInicio,
      query.tipoExaminacion,
      query.estadoExaminacion,
      {
        page,
        limit,
        route: '/apelaciones/tramitesParaApelacion',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    return data;
  }

  @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes' })
  @Get('solicitudes')
  async getSolicitudesPorRun(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.apelacionesService.solicitudes(
      null,
      {
        page,
        limit,
        route: '/registro-pago/allEstadoRegistroPago',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    const join = [];
    for (const solicitud of data.items) {
      const examinaciones = await this.apelacionesService.examinaciones(solicitud.idTramite);

      for (const examinacion of examinaciones) join.push({ ...solicitud, examinacion });
    }
    data.items = join;
    return data;
  }

  @ApiOperation({ summary: 'Servicio que crea un documento de respaldo de la apelacion' })
  @Post('documento')
  async postDocumento(@Body() dto: DocsApelacionesDTO) {
    return this.apelacionesService.documentoCrear(dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que crea un documento de respaldo de la apelacion' })
  @Post('apelacion')
  async postApelacion(@Body() dto: ApelacionesDTO) {
    return this.apelacionesService.apelacionCrear(dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que crea un documento de respaldo de la apelacion' })
  @Post('crearApelacion')
  async postApelacionV2(@Body() dto: any, @Req() req: any) {
    return this.apelacionesService.apelacionCrearV2(req, dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @Get('getTipoTramites')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los tipos de trámites' })
  async getTipoTramites() {
    const data = await this.apelacionesService.getTipoTramites();
    return { data };
  }

  @Get('getClasesLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las clases de licencia' })
  async getClasesLicencia() {
    const data = await this.apelacionesService.getClasesLicencias();
    return { data };
  }

  @Get('getEstadosTramites')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados de trámites' })
  async getEstadosTramites() {
    const data = await this.apelacionesService.getEstadosTramites();
    return { data };
  }

  @Get('getEstadosTramitesOtorgamientoDenegaciones')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados de trámites' })
  async getEstadosTramitesOtorgamientoDenegaciones() {
    const data = await this.apelacionesService.getEstadosTramitesOtorgamientoDenegaciones();
    return { data };
  }

  @Get('getEstadosExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados de examinación' })
  async getEstadosExaminacion() {
    const data = await this.apelacionesService.getEstadosExaminacion();
    return { data };
  }

  @Get('getTiposExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los tipos de examinación' })
  async getTiposExaminacion() {
    const data = await this.apelacionesService.getTiposExaminacion();
    return { data };
  }

  @Get('getEstadosSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados de examinación' })
  async getEstadosSolicitud() {
    const data = await this.apelacionesService.getEstadosSolicitud();
    return { data };
  }

  @Get('test')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados de examinación' })
  async test() {
    return 'test';
  }
}
