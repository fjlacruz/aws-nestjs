import { ApiPropertyOptional } from "@nestjs/swagger";

export class TramiteApelacionDto {
    constructor(){}

    @ApiPropertyOptional()
    run: number;

    @ApiPropertyOptional()
    nombrePostulante;

    @ApiPropertyOptional()
    idSolicitud: number;

    @ApiPropertyOptional()
    idTramite: number;

    @ApiPropertyOptional()
    clasesLicencias: string[];

    @ApiPropertyOptional()
    fechaInicio: Date;

    @ApiPropertyOptional()
    idEstadoTramite: number;

    @ApiPropertyOptional()
    estadoExaminacion: boolean;

    @ApiPropertyOptional()
    estadoExaminacionString: string;

    @ApiPropertyOptional()
    idTipoExaminacion: number;    

    @ApiPropertyOptional()
    tipoExaminacion: string;      

    @ApiPropertyOptional()
    estadoTramiteString: string;  

    @ApiPropertyOptional()
    estadoSolicitudString: string; 
    
    @ApiPropertyOptional()
    idTipoTramite: number;

    @ApiPropertyOptional()
    tipoTramiteString: string; 

    @ApiPropertyOptional()
    runFormateado: string;

    @ApiPropertyOptional()
    abreviacionLicencia: string;

    @ApiPropertyOptional()
    telefonoPostulante: string; 

    @ApiPropertyOptional()
    comunaPostulante: string; 

    @ApiPropertyOptional()
    regionPostulante: string; 

    @ApiPropertyOptional()
    emailPostulante: string; 

    @ApiPropertyOptional()
    idColaExaminacion: number;
    
}