import { ApiPropertyOptional } from "@nestjs/swagger";

export class ApelacionesDTO
{

    @ApiPropertyOptional()
    idTramite: number;

    @ApiPropertyOptional()
    idcolaexaminacion: number;

    @ApiPropertyOptional()
    observacion: string;

    @ApiPropertyOptional()
    ingresadapor: number;

    @ApiPropertyOptional()
    idEstadosExaminacion: number;

    //
    @ApiPropertyOptional()
    fechaIngreso: Date;

    @ApiPropertyOptional()
    idTipoExaminacion: number;

    @ApiPropertyOptional()
    documentos: any[];
}