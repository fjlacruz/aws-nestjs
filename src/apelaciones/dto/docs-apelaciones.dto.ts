import { ApiPropertyOptional } from "@nestjs/swagger";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

export class DocsApelacionesDTO 
{
    @ApiPropertyOptional()
    nombrearchivo: string;

    @Column({ type: "bytea"})
    binary: Buffer;

    @Column()
    idapelaciones: number;
}