import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('apelaciones')
export class ApelacionesEntity {
  @PrimaryGeneratedColumn()
  idapelaciones: number;

  @Column()
  idcolaexaminacion: number;

  @Column()
  fechaingreso: Date;

  @Column()
  observacion: string;

  @Column()
  ingresadapor: number;

  @Column()
  apelacionaprobada: boolean;

  @Column()
  fechaaprobacion: Date;

  @Column()
  aprobadapor: number;
}
