import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'docsapelaciones')
export class DocsApelacionesEntity 
{
    @PrimaryGeneratedColumn()
    iddocsapelacion:number;

    @Column()
    nombrearchivo: string;

    @Column({ type: "bytea"})
    binary: Buffer;

    @Column()
    created_at: Date;

    @Column()
    idapelaciones: number;
}