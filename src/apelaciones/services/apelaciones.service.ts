import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EntityManager, getManager, Like, Repository } from 'typeorm';
import { ApelacionesDTO } from '../dto/apelaciones.dto';
import { DocsApelacionesDTO } from '../dto/docs-apelaciones.dto';
import { ApelacionesEntity } from '../entities/apelaciones.entity';
import { DocsApelacionesEntity } from '../entities/docs-apelaciones.entity';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { validarStringEsEntero } from 'src/shared/Common/ValidadoresFormato';
import { Resultado } from 'src/utils/resultado';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { ColaExaminacionTramiteNMEntity } from 'src/tramites/entity/cola-examinacion-tramite-n-m.entity';
import { ColaExaminacion } from 'src/tramites/entity/cola-examinacion.entity';
import { formatearRun } from 'src/shared/ValidarRun';
import { TramiteApelacionDto } from '../dto/TramiteApelacionDto.dto';
import { ServiciosComunesService } from 'src/shared/services/ServiciosComunes/ServiciosComunes.services';
import { ComunasEntity } from 'src/escuela-conductores/entity/comunas.entity';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { docsapelacionesEntity } from 'src/solicitudes/entity/docsapelaciones.entity';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';

@Injectable()
export class ApelacionesService {
  constructor(
    @InjectRepository(TramitesClaseLicencia)
    private readonly tramitesLicenciaRepository: Repository<TramitesClaseLicencia>,

    @InjectRepository(DocsApelacionesEntity)
    private readonly documentosRepository: Repository<DocsApelacionesEntity>,

    @InjectRepository(TramitesEntity) private readonly tramitesEntityRespository: Repository<TramitesEntity>,

    private serviciosComunesService: ServiciosComunesService,

    private readonly authService: AuthService
  ) {}

  solicitudes(run?: number, options?: IPaginationOptions, orden?: string, ordenarPor?: string): Promise<any> {
    const join = [];

    const where = run ? 'postulante.RUN = ' + run.toString() : 'true';

    const solicitudes = this.tramitesLicenciaRepository
      .createQueryBuilder('tralic')
      .innerJoinAndSelect('tralic.clasesLicencias', 'claLic')
      .innerJoinAndSelect('tralic.tramite', 'tramite')
      .innerJoinAndSelect('tramite.tiposTramite', 'tipoTramite')
      .innerJoinAndSelect('tramite.solicitudes', 'solicitud')
      .innerJoinAndSelect('tramite.estadoTramite', 'estTra')
      .innerJoinAndSelect('solicitud.postulante', 'postulante')
      // .innerJoinAndSelect('postulante.region', 'region')
      .innerJoinAndSelect('postulante.comunas', 'comuna')
      .innerJoinAndSelect('solicitud.estadosSolicitud', 'estado')

      //.where('postulante.RUN = :run', { run })

      .andWhere('tramite.idEstadoTramite = 12 or tramite.idEstadoTramite = 13');
    return paginate<any>(solicitudes, options);
    // for (const solicitud of solicitudes) {
    //   const examinaciones = await this.examinaciones(solicitud.idTramite);
    //
    //   for (const examinacion of examinaciones) join.push({ ...solicitud, examinacion });
    // }
    // return join;
  }

  async getSolicitudesApelaciones(
    req:any,
    //idTipoExaminacion: string,
    idSolicitud?: string,
    idEstadoSolicitud?: string,
    idTramite?: string,
    idEstadoTramite?: string,
    run?: string,
    tipoTramite?: string,
    claseLicencia?: string,
    fechaCreacion?: Date,
    tipoExaminacion?: string,
    estadoExaminacion?: string,
    options?: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
    //recogerHistorico? : boolean
  ) {
    let res: Resultado = new Resultado();

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaModificacionesEspecialesPorApelaciones);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      if (
        (claseLicencia && !validarStringEsEntero(claseLicencia)) ||
        (tipoTramite && !validarStringEsEntero(tipoTramite)) ||
        (idEstadoSolicitud && !validarStringEsEntero(idEstadoSolicitud)) ||
        (idTramite && !validarStringEsEntero(idTramite)) ||
        (estadoExaminacion && !validarStringEsEntero(estadoExaminacion)) ||
        (idEstadoTramite && !validarStringEsEntero(idEstadoTramite)) ||
        (idSolicitud && !validarStringEsEntero(idSolicitud))
      ) {
        res.Error = 'Existe un error en los datos enviados, por favor revise los tipos de datos enviados.';
        res.ResultadoOperacion = false;
        res.Respuesta = [];

        return res;
      }

      let resultadoApelaciones: TramiteApelacionDto[] = [];

      const qbTramites = this.tramitesEntityRespository
        .createQueryBuilder('tramites')

        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .innerJoinAndMapOne('tramites.solicitudes', Solicitudes, 'solicitudes', 'tramites.idSolicitud = solicitudes.idSolicitud')
        .innerJoinAndMapOne(
          'solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'estadosSolicitud.idEstado = solicitudes.idEstado'
        )
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne('postulante.comunas', ComunasEntity, 'comunas', 'comunas.idComuna = postulante.idComuna')
        .innerJoinAndMapOne('comunas.regiones', RegionesEntity, 'regiones', 'regiones.idRegion = comunas.idRegion')
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        )
        .innerJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .innerJoinAndMapMany(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacion,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'colaExaminacion.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNMEstadoAlerta',
          'colaExaminacionNMEstadoAlerta.idTramite = colaExaminacion.idTramite'
        )
        .leftJoinAndMapMany(
          'colaExaminacionNMEstadoAlerta.colaExaminacionEstadoAlerta',
          ColaExaminacionEntity,
          'colaExaminacionEstadoAlerta',
          'colaExaminacionNMEstadoAlerta.idColaExaminacion = colaExaminacionEstadoAlerta.idColaExaminacion'
        )
        //Para traer las descripciones de las examinaciones
        .innerJoinAndMapOne(
          'colaExaminacion.tipoExaminaciones',
          TipoExaminacionesEntity,
          'tipoExaminaciones',
          'colaExaminacion.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion'
        )
        .innerJoinAndMapOne(
          'colaExaminacion.estadoExaminaciones',
          estadosExaminacionEntity,
          'estadoExaminaciones',
          'colaExaminacion.idEstadosExaminacion = estadoExaminaciones.idEstadosExaminacion'
        )

        // Se recogen los trámites con estado denegado por sml o denegado por jpl
        .andWhere('(tramites.idEstadoTramite = 12 or tramites.idEstadoTramite = 13)')

        // Se recogen las colas de examinación de tipo idoneidad moral o exámen médico (reprobados)
        .andWhere(
          '((colaExaminacion.idTipoExaminacion = 5 and colaExaminacion.idEstadosExaminacion = 36)' +
            'or (colaExaminacion.idTipoExaminacion = 4 and colaExaminacion.idEstadosExaminacion = 11))'
        )

        // Se recogen las solicitudes que no están en estado borrador
        .andWhere('(solicitudes.idEstado != 1)');

      // Filtros
      if (run) {
        const rut = run.split('-');
        let runsplited: string = rut[0];
        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');
        const div = rut[1];
        qbTramites.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbTramites.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }

      if (claseLicencia)
        qbTramites.andWhere('clasesLicencias.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: claseLicencia,
        });
      if (fechaCreacion)
        qbTramites.andWhere('"solicitudes"."FechaCreacion"::date = :created_at::date', {
          //"FechaCreacion" > '2022-07-20'
          created_at: fechaCreacion,
        });

      //Filtro por estado de trámite
      if (idEstadoTramite)
        qbTramites.andWhere('tramites.idEstadoTramite = :_idEstadoTramite', {
          _idEstadoTramite: idEstadoTramite,
        });

      //Filtro por tipo de tramite
      if (tipoTramite)
        qbTramites.andWhere('tramites.idTipoTramite = :_idEstadosExaminacion', {
          _idEstadosExaminacion: tipoTramite,
        });

      // Filtrar por id solicitud
      if (idSolicitud)
        qbTramites.andWhere('solicitudes.idSolicitud = :_idSolicitud', {
          _idSolicitud: idSolicitud,
        });

      // Filtrar por estado solicitud
      if (idEstadoSolicitud)
        qbTramites.andWhere('solicitudes.idEstado = :_idEstadoSolicitud', {
          _idEstadoSolicitud: idEstadoSolicitud,
        });

      // Filtrar por id tramite
      if (idTramite)
        qbTramites.andWhere('tramites.idTramite = :_idTramite', {
          _idTramite: idTramite,
        });

      // Filtrar por tipo de examinación
      if (tipoExaminacion)
        qbTramites.andWhere('colaExaminacion.idTipoExaminacion = :_idTipoExaminacion', {
          _idTipoExaminacion: tipoExaminacion,
        });

      // Filtrar por estado de examinación
      if (estadoExaminacion)
        qbTramites.andWhere('colaExaminacion.idEstadosExaminacion = :_idEstadoExaminacion', {
          _idEstadoExaminacion: estadoExaminacion,
        });

      if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qbTramites.orderBy('colaExaminacion.idColaExaminacion', 'ASC');
      if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qbTramites.orderBy('colaExaminacion.idColaExaminacion', 'DESC');
      if (orden === 'RUN' && ordenarPor === 'ASC') qbTramites.orderBy('postulante.RUN', 'ASC');
      if (orden === 'RUN' && ordenarPor === 'DESC') qbTramites.orderBy('postulante.RUN', 'DESC');
      if (orden === 'postulante' && ordenarPor === 'ASC') qbTramites.orderBy('postulante.Nombres', 'ASC');
      if (orden === 'postulante' && ordenarPor === 'DESC') qbTramites.orderBy('postulante.Nombres', 'DESC');
      if (orden === 'ClaseLicencia' && ordenarPor === 'ASC') qbTramites.orderBy('clasesLicencias.Abreviacion', 'ASC');
      if (orden === 'ClaseLicencia' && ordenarPor === 'DESC') qbTramites.orderBy('clasesLicencias.Abreviacion', 'DESC');
      if (orden === 'IdTramite' && ordenarPor === 'ASC') qbTramites.orderBy('colaExaminacion.idTramite', 'ASC');
      if (orden === 'IdTramite' && ordenarPor === 'DESC') qbTramites.orderBy('colaExaminacion.idTramite', 'DESC');
      if (orden === 'IdSolicitud' && ordenarPor === 'ASC') qbTramites.orderBy('solicitudes.idSolicitud', 'ASC');
      if (orden === 'IdSolicitud' && ordenarPor === 'DESC') qbTramites.orderBy('solicitudes.idSolicitud', 'DESC');
      if (orden === 'estadoSolicitud' && ordenarPor === 'ASC') qbTramites.orderBy('estadosSolicitud.Nombre', 'ASC');
      if (orden === 'estadoSolicitud' && ordenarPor === 'DESC') qbTramites.orderBy('estadosSolicitud.Nombre', 'DESC');
      if (orden === 'EstadoTramite' && ordenarPor === 'ASC') qbTramites.orderBy('estadoTramite.Nombre', 'ASC');
      if (orden === 'EstadoTramite' && ordenarPor === 'DESC') qbTramites.orderBy('estadoTramite.Nombre', 'DESC');
      if (orden === 'tipoExamiancion' && ordenarPor === 'ASC') qbTramites.orderBy('tipoExaminaciones.nombreExaminacion', 'ASC');
      if (orden === 'tipoExamiancion' && ordenarPor === 'DESC') qbTramites.orderBy('tipoExaminaciones.nombreExaminacion', 'DESC');
      if (orden === 'estadoExaminacion' && ordenarPor === 'ASC') qbTramites.orderBy('estadoExaminaciones.nombreEstado', 'ASC');
      if (orden === 'estadoExaminacion' && ordenarPor === 'DESC') qbTramites.orderBy('estadoExaminaciones.nombreEstado', 'DESC');
      if (orden === 'fechaTramite' && ordenarPor === 'ASC') qbTramites.orderBy('solicitudes.FechaCreacion', 'ASC');
      if (orden === 'fechaTramite' && ordenarPor === 'DESC') qbTramites.orderBy('solicitudes.FechaCreacion', 'DESC');

      const resultadoTramites: any = await paginate<TramitesEntity>(qbTramites, options);

      // Transformacion al DTO
      resultadoTramites.items.forEach(resT => {
        let tramiteRecorrido: TramitesEntity = resT as TramitesEntity;

        tramiteRecorrido.colaExaminacionNM.forEach(examinacionTramite => {
          let tApelacion: TramiteApelacionDto = new TramiteApelacionDto();

          tApelacion.run = +(tramiteRecorrido.solicitudes.postulante.RUN + '' + tramiteRecorrido.solicitudes.postulante.DV);
          tApelacion.nombrePostulante =
            tramiteRecorrido.solicitudes.postulante.Nombres +
            ' ' +
            tramiteRecorrido.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            tramiteRecorrido.solicitudes.postulante.ApellidoMaterno;
          tApelacion.telefonoPostulante = tramiteRecorrido.solicitudes.postulante.Telefono;
          tApelacion.emailPostulante = tramiteRecorrido.solicitudes.postulante.Email;
          tApelacion.regionPostulante = tramiteRecorrido.solicitudes.postulante.comunas.regiones.Nombre;
          tApelacion.comunaPostulante = tramiteRecorrido.solicitudes.postulante.comunas.Nombre;

          tApelacion.idSolicitud = tramiteRecorrido.idSolicitud;
          tApelacion.runFormateado = formatearRun(
            tramiteRecorrido.solicitudes.postulante.RUN + '-' + tramiteRecorrido.solicitudes.postulante.DV
          );
          tApelacion.abreviacionLicencia = tramiteRecorrido.tramitesClaseLicencia.clasesLicencias.Abreviacion;
          tApelacion.fechaInicio = tramiteRecorrido.solicitudes.FechaCreacion;
          tApelacion.idTramite = tramiteRecorrido.idTramite;
          tApelacion.estadoTramiteString = tramiteRecorrido.estadoTramite.Nombre;
          tApelacion.tipoTramiteString = tramiteRecorrido.tiposTramite.Nombre;
          tApelacion.estadoSolicitudString = tramiteRecorrido.solicitudes.estadosSolicitud.Nombre;

          tApelacion.estadoExaminacion = examinacionTramite.colaExaminacion[0].aprobado;
          tApelacion.estadoExaminacionString = examinacionTramite.colaExaminacion[0].estadoExaminaciones.nombreEstado;
          tApelacion.tipoExaminacion = examinacionTramite.colaExaminacion[0].tipoExaminaciones.nombreExaminacion;
          tApelacion.idTipoExaminacion = examinacionTramite.colaExaminacion[0].tipoExaminaciones.idTipoExaminacion;
          tApelacion.idColaExaminacion = examinacionTramite.colaExaminacion[0].idColaExaminacion;

          resultadoApelaciones.push(tApelacion);
        });

        // Mapear examinaciones y su estado de examinación
      });

      resultadoTramites.items = resultadoApelaciones;

      res.ResultadoOperacion = true;
      res.Respuesta = resultadoTramites;

      return res;
    } catch (Error) {
      res.Error = 'Ha ocurrido un error en la respuesta, por favor consulte con el administrador de la aplicación.';
      res.ResultadoOperacion = false;
      res.Respuesta = [];

      return res;
    }
  }

  async examinaciones(idTramite: number) {
    const examinaciones = await getManager()
      .createQueryBuilder()
      .select('"cola".*, "esta"."nombreEstado", tipo.nombreExaminacion')
      .from('ColaExaminacionTramiteNM', 'colaTram')
      .innerJoin(ColaExaminacionEntity, 'cola', 'cola.idColaExaminacion        = "colaTram"."idColaExaminacion"')
      .innerJoin('estadosExaminacion', 'esta', '"esta"."idEstadosExaminacion" = "cola"."idEstadosExaminacion"')
      .innerJoin(TipoExaminacionesEntity, 'tipo', 'tipo.idTipoExaminacion        = cola.idTipoExaminacion')
      .where('"colaTram"."idTramite" = :idTramite', { idTramite })
      .andWhere('cola.aprobado = :aprobado', { aprobado: false })
      .andWhere('( cola.idTipoExaminacion = 4 OR cola.idTipoExaminacion = 6 )')
      .getRawMany();

    return examinaciones;
  }

  async documentoCrear(dto: DocsApelacionesDTO) {
    dto['created_at'] = new Date();
    return await this.documentosRepository.save(dto);
  }

  async apelacionCrear(dto: ApelacionesDTO) {
    const fecha = new Date();
    const fechaingreso = fecha;
    const fechaaprobacion = fecha;
    const apelacionaprobada = true;
    const idCola = dto.idcolaexaminacion;
    const idEstadosExaminacion = dto.idEstadosExaminacion;

    return await getManager().transaction(async (manager: EntityManager) => {
      const apelacion = await manager.save(ApelacionesEntity, { ...dto, fechaingreso, fechaaprobacion, apelacionaprobada });

      await manager.update(ColaExaminacionEntity, idCola, { aprobado: true, idEstadosExaminacion });
      return apelacion;
    });
  }

  async apelacionCrearV2(req:any, dto: ApelacionesDTO) {
    const resultado = new Resultado();
    const fechaingreso = dto.fechaIngreso;
    const fechaaprobacion = new Date();
    const apelacionaprobada = true;
    const idCola = dto.idcolaexaminacion;

    return await getManager().transaction(async (manager: EntityManager) => {
      let apelacion;
      let tramites: any[] = [];

      try {

        let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ModificacionesEspecialesProrrogasOportunidadesSolicitarAprobacion);
    
        // En caso de que el usuario no sea correcto
        if (!usuarioValidado.ResultadoOperacion) {
          return usuarioValidado;
        }	 

        if (dto.idTipoExaminacion != 4 && dto.idTipoExaminacion != 5) {
          resultado.Error = 'El tipo de examinación no es válido para la acción realizada.';
          resultado.ResultadoOperacion = false;

          return resultado;
        }

        // Guardamos apelacion
        apelacion = await manager.save(ApelacionesEntity, { ...dto, fechaingreso, fechaaprobacion, apelacionaprobada });

        let estado: estadosExaminacionEntity;

        // Buscamos el estado de examinacion
        if (dto.idTipoExaminacion == 4) {
          // Si es tipo Médico
          estado = await manager.findOne(estadosExaminacionEntity, { where: { idEstadosExaminacion: 38 } }); // Estado correspondiente a aprobado por reconsideracion SML
        } else if (dto.idTipoExaminacion == 5) {
          // Si es tipo Idoneidad Moral
          estado = await manager.findOne(estadosExaminacionEntity, { where: { idEstadosExaminacion: 37 } }); // Estado correspondiente a aprobado por reconsideracion SML
        }

        if (!estado) {
          resultado.Error =
            'Se ha producido un error en la asignación del nuevo estado para la examinación, por favor contacte con el administrador.';
          resultado.ResultadoOperacion = false;

          return resultado;
        }

        // Actualizamos el estado de examinacion
        await manager.update(ColaExaminacionEntity, idCola, { aprobado: true, idEstadosExaminacion: estado.idEstadosExaminacion });

        // Buscamos los tramites
        let colaExaminacionTramiteNM = await manager.find(ColaExaminacionTramiteNMEntity, {
          relations: [
            'tramites',
            'tramites.colaExaminacionNM',
            'tramites.colaExaminacionNM.colaExaminacion',
            'tramites.colaExaminacionNM.colaExaminacion.estadoExaminaciones',
          ],
          where: qb => {
            qb.where('"ColaExaminacionTramiteNMEntity"."idColaExaminacion" = :idColaExaminacion', {
              idColaExaminacion: dto.idcolaexaminacion,
            });
          },
        });

        colaExaminacionTramiteNM.forEach(tramiteNM => {
          tramites.push({
            idTramite: tramiteNM.idTramite,
            examinaciones: tramiteNM.tramites.colaExaminacionNM.map(x => x.colaExaminacion.estadoExaminaciones.codigo),
            idSolicitud: tramiteNM.tramites.idSolicitud,
          });
        });

        let estadoTramites : EstadosTramiteEntity[] = await manager.find(EstadosTramiteEntity);

        // actualizamos los tramites
        for(const tramite of tramites){
        //tramites.forEach(async tramite => {
          // Estado de tramite nuevo
          let estadoTramite : EstadosTramiteEntity[];

          // Si todas las examinaciones estan aprobadas, actualizamos a "A la espera de informar otorgamiento"
          if (
            tramite.examinaciones.includes('1015') && // Examen medico aprobado
            tramite.examinaciones.includes('1021') && // Examen teorico aprobado
            tramite.examinaciones.includes('1010') && // Idoneidad moral aprobado
            tramite.examinaciones.includes('1027') // Examen practico aprobado
          ) {
            estadoTramite = estadoTramites.filter(x => x.codigo == '1103');
          } else if (
            // Si tiene alguna de las examinaciones suspensas, actualizamos a "A la espera de informar denegacion"
            tramite.examinaciones.includes('1016') || // Examen medico reprobado
            tramite.examinaciones.includes('1022') || // Examen teorico reprobado 1º op
            tramite.examinaciones.includes('1024') || // Examen teorico reprobado 2º op
            tramite.examinaciones.includes('1028') || // Examen practico reprobado 1º op
            tramite.examinaciones.includes('1030') || // Examen practico reprobado 2º op
            tramite.examinaciones.includes('1011') // Idoneidad moral reprobado
          ) {
            estadoTramite = estadoTramites.filter(x => x.codigo == '1102');
          } 
          else {
            // Si tiene aprobadas y alguna este pendiente y ninguna suspensa (reprobado), actualizamos a "Tramite reiniciado por reconsideracion"

            estadoTramite = estadoTramites.filter(x => x.codigo == '1110');
          }

          if (estadoTramite && estadoTramite.length > 0) {
            let res = await manager.update(TramitesEntity, tramite.idTramite, {
              idEstadoTramite: estadoTramite[0].idEstadoTramite,
              update_at: new Date(),
            });
          }
        }

        dto.documentos.forEach(async element => {
          let doc = new DocsApelacionesEntity();
          doc.binary = element.base64;
          doc.nombrearchivo = element.nombreOriginal;
          doc.idapelaciones = apelacion.idapelaciones;
          doc.created_at = new Date();

          await manager.save(docsapelacionesEntity, doc);
        });

        // Actualizamos el estado de la/s solicitud/es
        // Recogemos sólo las solicitudes no repetidas
        let uniquesIdSolicitud: number[] = tramites
          .map(x => x.idSolicitud)
          .filter(function (elem, index, self) {
            return index === self.indexOf(elem);
          });

        uniquesIdSolicitud.forEach(async idSolicitudReconsiderada => {
          await manager.update(Solicitudes, idSolicitudReconsiderada, { idEstado: 12 }); // Reabierta por reconsideración JPL o SML
        });
      } catch (e) {
        resultado.Error = 'Se ha producido un error en la aplicación, por favor contacte con el administrador.';
        resultado.ResultadoOperacion = false;

        return resultado;
      }

      resultado.Respuesta = apelacion;
      resultado.ResultadoOperacion = true;

      return resultado;
    });
  }

  public async getTipoTramites() {
    return this.serviciosComunesService.getTipoTramites();
  }

  public async getEstadosTramites() {
    // Estados de trámites admitidos :    12 - "Denegación Informada SML"
    //                                    13 - "Denegación Informada JPL"

    const filtroIds: number[] = [12, 13, 3, 1];

    return this.serviciosComunesService.getEstadosTramites(filtroIds);
  }

  public async getEstadosTramitesOtorgamientoDenegaciones() {
    // Estados de trámites admitidos :    2 - "A la espera de informar denegación"
    //                                    3 - "A la espera de informar otorgamiento"

    const filtroIds: number[] = [2,3];

    return this.serviciosComunesService.getEstadosTramites(filtroIds);
  }

  public async getClasesLicencias() {
    return this.serviciosComunesService.getClasesLicencia();
  }

  public async getEstadosExaminacion() {
    // Estados de examinación admitidos : 11 - ""Examen médico reprobado""
    //                                    36 - ""Reprobado por idoneidad moral""

    const filtroIds: number[] = [11, 36];

    return this.serviciosComunesService.getEstadosExaminacion(filtroIds);
  }

  public async getTiposExaminacion() {
    // Tipos de examinación admitidos : 5 - "Examinación Idoneidad Moral"
    //                                  4 - "Examinación Médica"

    const filtroIds: number[] = [4, 5];
    return this.serviciosComunesService.getTiposExaminacion(filtroIds);
  }

  public async getEstadosSolicitud() {
    return this.serviciosComunesService.getEstadosSolicitud();
  }
}
