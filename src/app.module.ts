import { GradoModule } from './grados/grado.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { PermisosModule } from './permisos/permisos.module';
import { GrupoPermisosModule } from './grupo-permisos/grupo-permisos.module';
import { DocsRequeridoRolModule } from './docsRequeridoRol/docsRequeridoRol.module';
import { Connection } from 'typeorm';
import { reserva, localConfig, sglc } from './orm.config';
import { AuthModule } from './auth/auth.module';
import { IngresoPostulanteModule } from './ingreso-postulante/ingreso-postulante.module';
import { SolicitudesModule } from './solicitudes/solicitudes.module';
import { ConfigModule } from '@nestjs/config';
import { ExamenTeoricoModule } from './examen-teorico/examen-teorico.module';
import { PaginaModule } from './pagina/pagina.module';
import { RegistroPagoModule } from './registro-pago/registro-pago.module';
import { FileManagerModule } from './file-manager/file-manager.module';
import { MenuSglModule } from './menu-sgl/menu-sgl.module';
import { RolesModule } from './roles/roles.module';
import { RolesUsuariosModule } from './roles-usuarios/roles-usuarios.module';
import { InstitucionesModule } from './instituciones/instituciones.module';
import { CarpetaConductorModule } from './carpeta-conductor/carpeta-conductor.module';
import { ConductorModule } from './conductor/conductor.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { ParametrosModule } from './parametros/parametros.module';
import { UploadImagenUsuarioModule } from './upload-imagen-usuario/upload-imagen-usuario.module';
import { RegionesModule } from './regiones/regiones.module';
import { ComunasModule } from './comunas/comunas.module';
import { ClasesLicenciaModule } from './clases-licencia/clases-licencia.module';
import { EscuelaConductoresModule } from './escuela-conductores/escuela-conductores.module';
import { TramitesModule } from './tramites/tramites.module';
import { ConsolidadoTramitesModule } from './consolidado-tramites/consolidado-tramites.module';
import { TiposTramitesModule } from './tipos-tramites/tipos-tramites.module';
import { TramitesClaseLicenciaModule } from './tramites-clase-licencia/tramites-clase-licencia.module';
import { OpcionesNivelEducacionalModule } from './opciones-nivel-educacional/opciones-nivel-educacional.module';
import { DocumentosUsuariosModule } from './documentos-usuarios/documentos-usuarios.module';
import { DocumentosTramitesModule } from './documentos-tramites/documentos-tramites.module';
import { SglcLogModule } from './sglc-log/sglc-log.module';
import { ImagenesPostulanteModule } from './imagenes-postulante/imagenes-postulante.module';
import { CertificadoEscuelaConductorModule } from './certificado-escuela-conductor/certificado-escuela-conductor.module';
import { DocumentoSolicitudPostulanteModule } from './documento-solicitud-postulante/documento-solicitud-postulante.module';
import { ReservaHoraModule } from './reserva-hora/reserva-hora.module';
import { NotificacionesModule } from './notificaciones/notificaciones.module';
import { DocumentosColaExamPracticosModule } from './documentos-cola-exam-practicos/documentos-cola-exam-practicos.module';
import { TipoResultadoColaExamPracticosModule } from './tipo-resultado-cola-exam-practicos/tipo-resultado-cola-exam-practicos.module';
import { ResultadoColaExamPracticosModule } from './resultado-cola-exam-practicos/resultado-cola-exam-practicos.module';
import { OpcionEstadosCivilModule } from './opcion-estados-civil/opcion-estados-civil.module';
import { TiposDocumentoModule } from './tipos-documento/tipos-documento.module';
import { TramiteDocumentoRequeridoModule } from './tramite-documento-requerido/tramite-documento-requerido.module';
import { ValidarRunModule } from './validar-run/validar-run.module';
import { FotoLicenciaModule } from './foto-licencia/foto-licencia.module';
import { BloqueoActivosModule } from './bloqueo-activos/bloqueo-activos.module';
import { GenerarTokenClaveUnicaModule } from './generar-token-clave-unica/generar-token-clave-unica.module';
import { DocumentosExamenTeoricoModule } from './documentos-examen-teorico/documentos-examen-teorico.module';
import { SeccionesFormularioModule } from './secciones-formulario/secciones-formulario.module';
import { FormulariosExaminacionesModule } from './formularios-examinaciones/formularios-examinaciones.module';
import { ColaExaminacionModule } from './cola-examinacion/cola-examinacion.module';
import { OpcionesSexoModule } from './opciones-sexo/opciones-sexo.module';
import { RespuestasSeleccionModule } from './respuestas-seleccion/respuestas-seleccion.module';
import { TiposRespuestaPreguntaModule } from './tipos-respuesta-pregunta/tipos-respuesta-pregunta.module';
import { TipoDeTramiteClaseLicenciaInstitucionModule } from './tipo-de-tramite-clase-licencia-institucion/tipo-de-tramite-clase-licencia-institucion.module';
import { ResultadoExaminacionModule } from './resultado-examinacion/resultado-examinacion.module';
import { RespuestasFormularioExaminacionModule } from './respuestas-formulario-examinacion/respuestas-formulario-examinacion.module';
import { DocumentosRespuestaModule } from './documentos-respuesta/documentos-respuesta.module';
import { ResultadoExaminacionEstadoModule } from './resultado-examinacion-estado/resultado-examinacion-estado.module';
import { MunicipalidadModule } from './municipalidad/municipalidad.module';
import { CertificadoAntecConducObtenRenovLicenciaModule } from './certificado-antec-conduc-obten-renov-licencia/certificado-antec-conduc-obten-renov-licencia.module';
import { OpcionNacionalidadesModule } from './opcion-nacionalidades/opcion-nacionalidades.module';
import { CalidadJuridicaModule } from './calidad-juridica/calidad-juridica.module';
import { EstamentoModule } from './estamento/estamento.module';
import { FolioModule } from './folio/folio.module';
import { ProveedorModule } from './proveedor/proveedor.module';
import { ResolucionesJudicialesModule } from './resoluciones-judiciales/resoluciones-judiciales.module';
import { TipoInstitucionModule } from './tipo-institucion/tipo-institucion.module';
import { CambiosEstadoModule } from './cambios-estado/cambios-estado.module';
import { ProrrogasModule } from './prorrogas/prorrogas.module';
import { PostulanteModule } from './postulante/postulante.module';
import { TipoInfraccionModule } from './tipo-infraccion/tipo-infraccion.module';
import { TipoJuzgadoModule } from './tipo-juzgado/tipo-juzgado.module';
import { OficinaModule } from './oficina/oficina.module';
import { ApelacionesModule } from './apelaciones/apelaciones.module';
import { DocumentmanagerModule } from './documentmanager/documentmanager.module';
import { ComunicadosModule } from './comunicados/comunicados.module';
import { PreguntasModule } from './preguntas/preguntas.module';
import { ClaveUnicaModule } from './clave-unica/clave-unica.module';
import { MotivosDenegacionModule } from './motivos-denegacion/motivos-denegacion.module';
import { InterSglModule } from './intersgl/intersgl.module';
import { ConsultaHistoricaModule } from './consulta-historica/consulta-historica.module';
import { SentenciaModule } from './sentencia/sentencia.module';
import { TipoUnidadMonetariaModule } from './tipo-unidad-monetaria/tipo-unidad-monetaria.module';
import { InfraccionModule } from './infraccion/infraccion.module';
import { AnotacionesResolucionModule } from './anotaciones-resolucion/anotaciones-resolucion.module';
import { TipoAnotacionModule } from './tipo-anotacion/tipo-anotacion.module';
import { OportunidadModule } from './oportunidad/oportunidad.module';
import { CambioEstadorceiModule } from './cambio-estadorcei/cambio-estadorcei.module';
import { ScheduleModule } from '@nestjs/schedule';
import { SituacionLaboralModule } from './situacion-laboral/situacion-laboral.module';
import { SolicitudesAltaUsuariosModule } from './solicitudes-alta-usuarios/solicitudes-alta-usuarios.module';
import { RestriccionesModule } from './restricciones/restricciones.module';
import { JornadasLaboralesModule } from './jornadas-laborales/jornadas-laborales.module';
import { SolicitudesProximasCierreModule } from './solicitudes-proximas-cierre/solicitudes-proximas-cierre.module'

@Module({
  imports: [
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot(sglc),
    TypeOrmModule.forRoot(reserva),
    ConfigModule.forRoot({ envFilePath: 'dev.env', isGlobal: true }),
    UsersModule,
    PermisosModule,
    GrupoPermisosModule,
    DocsRequeridoRolModule,
    AuthModule,
    IngresoPostulanteModule,
    SolicitudesModule,
    NotificacionesModule,
    ExamenTeoricoModule,
    PaginaModule,
    RegistroPagoModule,
    FileManagerModule,
    MenuSglModule,
    RolesModule,
    RolesUsuariosModule,
    InstitucionesModule,
    CarpetaConductorModule,
    ConductorModule,
    DocumentmanagerModule,
    ComunicadosModule,
    PreguntasModule,
    MailerModule.forRoot({
      transport: {
        host: "email-smtp.sa-east-1.amazonaws.com",
        port: "25, 587",
        secure: true,
        auth: {
          user: "AKIARA72SJBHBMXUROOP",
          pass: "BFIsYvugmtPBd0y0WmO7a7ZcueG3HaHwvd3IJZ+grzSt"
        }
      }
    }),
    ParametrosModule,
    UploadImagenUsuarioModule,
    RegionesModule,
    ComunasModule,
    ClasesLicenciaModule,
    EscuelaConductoresModule,
    TramitesModule,
    ConsolidadoTramitesModule,
    TiposTramitesModule,
    TramitesClaseLicenciaModule,
    OpcionesNivelEducacionalModule,
    DocumentosUsuariosModule,
    DocumentosTramitesModule,
    SglcLogModule,
    ImagenesPostulanteModule,
    CertificadoEscuelaConductorModule,
    DocumentoSolicitudPostulanteModule,
    ReservaHoraModule,
    DocumentosColaExamPracticosModule,
    TipoResultadoColaExamPracticosModule,
    ResultadoColaExamPracticosModule,
    OpcionEstadosCivilModule,
    OpcionNacionalidadesModule,
    CalidadJuridicaModule,
    GradoModule,
    EstamentoModule,
    TramiteDocumentoRequeridoModule,
    ValidarRunModule,
    TiposDocumentoModule,
    TramiteDocumentoRequeridoModule,
    FotoLicenciaModule,
    BloqueoActivosModule,
    GenerarTokenClaveUnicaModule,
    DocumentosExamenTeoricoModule,
    OpcionesSexoModule,
    ColaExaminacionModule,
    RespuestasSeleccionModule,
    SeccionesFormularioModule,
    FormulariosExaminacionesModule,
    TiposRespuestaPreguntaModule,
    TipoDeTramiteClaseLicenciaInstitucionModule,
    ResultadoExaminacionModule,
    RespuestasFormularioExaminacionModule,
    CertificadoAntecConducObtenRenovLicenciaModule,
    DocumentosRespuestaModule,
    ResultadoExaminacionEstadoModule,
    MunicipalidadModule,
    FolioModule,
    ProveedorModule,
    ResolucionesJudicialesModule,
    SentenciaModule,
    TipoInstitucionModule,
    CambiosEstadoModule,
    ProrrogasModule,
    PostulanteModule,
    TipoInfraccionModule,
    TipoJuzgadoModule,
    OficinaModule,
    ApelacionesModule,
    ClaveUnicaModule,
    MotivosDenegacionModule,
    InterSglModule,
    ConsultaHistoricaModule,
    SentenciaModule,
    TipoUnidadMonetariaModule,
    InfraccionModule,
    AnotacionesResolucionModule,
    TipoAnotacionModule,
    OportunidadModule,
    CambioEstadorceiModule,
    SituacionLaboralModule,
    SolicitudesAltaUsuariosModule,
    RestriccionesModule,
    JornadasLaboralesModule,
    SolicitudesProximasCierreModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) { }
}
