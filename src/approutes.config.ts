export const RUTA_IMAGENES_LICENCIA: string = '/mygit/sglcbackend/src/shared/Images/';
export const RUTA_IMAGENES_FUENTES_LICENCIA: string = '/mygit/sglcbackend/src/shared/Fonts/';

//local
//export const BASE_URL_BACK_MOBILE = "http://localhost:3001";
// dev
export const BASE_URL_BACK_MOBILE = 'https://sglcbackendmobiledev.ingeniaglobal.cl';
// qa
//export const BASE_URL_BACK_MOBILE = "https://sglcbackendmobiledev.ingeniaglobal.cl";

export const URL_MOBILE_CREAR_NOTIFICACION = '/notificaciones/crearNotificacion';
export const URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_COMUNAS = '/notificaciones/crearNotificacionConAudienciaComunas';
export const URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_CONDUCTOR = '/notificaciones/crearNotificacionConAudienciaPrivada';
export const URL_MOBILE_CREAR_LICENCIA = '/documentos-clase-licencia/crearDocumentoLicencia';

export const BASE_URL_REGISTRO_CIVIL = 'https://4qnzjlsq1k.execute-api.sa-east-1.amazonaws.com';

export const URL_REGISTRO_CIVIL_HISTORICO = '/api/rnc-historicornc';
export const URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES = '/api/certificadoantecedentes';
export const URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES_JUZGADO = '/api/certificadoantecedentesjuzgado';

export const URL_REGISTRO_CIVIL_ESTADO_RPI_POSTULANTE = '/api/rpi';
