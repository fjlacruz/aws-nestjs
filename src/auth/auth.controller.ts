import { Controller, Get, Post, Patch, Req, UseGuards, Request, Body, NotFoundException, SetMetadata } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import CreateUserDto from 'src/users/DTO/createUser.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtAuthGuard } from './jwt-auth.guard';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UsersService } from 'src/users/services/users.service';
import { CreateUserClaveUnicaDto } from 'src/users/DTO/createUserClaveUnicaDto';
import { Public } from './guards/auth.guard';
import { ResetPassworDTO } from 'src/users/DTO/resetPassworDTO';
import { UpdatePasswordDto } from 'src/users/DTO/update-password.dto';
import { Resultado } from 'src/utils/resultado';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';

@ApiTags('Servicios-Authenticacion')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService, private userService: UsersService) {}

  @ApiOperation({ summary: 'Servicio de Login SIN Autenticación.' })
  @Post('login')
  loginRun(@Req() req: any) {
    return req.user;
  }

  @ApiOperation({ summary: 'Servicio de prueba. NO requiere autenticación' })
  @Get('profile')
  profile() {
    return ' ESTOS SON SU DATOS';
  }

  @ApiOperation({ summary: 'Servicio de prueba. SI requiere autenticación' })
  @Get('token/profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @ApiOperation({ summary: 'Servicio de Authentication, crea un token de session' })
  //@UseGuards(LocalAuthGuard)
  @Public()
  @Post('token/login')
  async loginTok(@Body() createUserDto: CreateUserDto) {
    const user = await this.authService.validateUserRun(createUserDto.RUN, createUserDto.passwd);
    if (!user) throw new NotFoundException('user dont exist');

    const tokenValue = this.authService.login(createUserDto.RUN);

    const userValuID = this.userService.getUserLoginRun(createUserDto.RUN);

    if ((await userValuID).conectado != true || (await userValuID).conectado != false) {
      createUserDto.tokenCU = (await tokenValue).access_token;
      //createUserDto.conectado = false; // To do
      createUserDto.conectado = true;
      this.userService.updateUserLogin((await userValuID).idUsuario, createUserDto);

      return tokenValue;
    } else {
      return 'Ya se encuentra conectado';
    }
  }

  @ApiOperation({ summary: 'Servicio de Authentication, crea un token de session para clave unica' })
  //@UseGuards(LocalAuthGuard)
  @Post('token/login-clave-unica')
  async loginTokClaveUnica(@Body() createUserClaveUnicaDto: CreateUserClaveUnicaDto) {
    const tokenValue = this.authService.login(createUserClaveUnicaDto.RUN);
    return tokenValue;
  }

  @ApiOperation({ summary: 'Servicio de Authentication, elimina session' })
  //@UseGuards(LocalAuthGuard)
  @Post('token/logout')
  async logoutTok(@Body() createUserDto: CreateUserDto) {
    const userValuID = this.userService.getUserLoginRun(createUserDto.RUN);

    if ((await userValuID).conectado == true) {
      createUserDto.conectado = false;
      createUserDto.tokenCU = '';

      this.userService.update((await userValuID).idUsuario, createUserDto);

      return 'Se ha elimina la session - conexión false';
    } else {
      return 'Ya se esta eliminada la session';
    }
  }

  @Post('resetPassword')
  @Public()
  @ApiOperation({ summary: 'Servicio de envio de correo para recuperar contraseña usuario' })
  async resetPassword(@Body() resetPassworDTO: ResetPassworDTO) {
    const data = await this.userService.sendMailResetPassword(resetPassworDTO.email);
    return { data };
  }


  @Patch('updatePassword')
  @Public()
  @ApiOperation({ summary: 'Servicio para cambiar el password del usuario' })
  async updatePassword(@Body() updatePasswordDto: UpdatePasswordDto) {
    const data = await this.userService.resetPassword(updatePasswordDto);
    return { data };
  }

  @Post('verificaSiUsuarioTienePermiso')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para actualizare los datos del domicilio del postulante',
  })
  async verificaSiUsuarioTienePermiso(@Body() data: {permisoParaVerificar: string[]}, @Req() req: any) {

    let validarPermisos = await this.authService.checkIfUserHavePermission(req, data.permisoParaVerificar);
    return validarPermisos;
  }

  @Post('verificaAdministrador')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para actualizare los datos del domicilio del postulante',
  })
  async verificaAdministrador(@Body() data: any, @Req() req: any) {

    let resAdmin : Resultado = new Resultado();

    try{
      // No importa el permiso que le enviemos, sólo queremos saber si es superadmin (por eso cadena vacía, no existe alguno con cadena vacía)
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, '');
      
      resAdmin.Respuesta = this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios);
      resAdmin.ResultadoOperacion = true;
      resAdmin.Mensaje = "Se devuelve resultado de si el usuario es administrador.";

      return resAdmin;
    }
    catch(Error){
      resAdmin.ResultadoOperacion = false;
      resAdmin.Mensaje = "Fallo al comprobar si el usuario es administrador.";

      return resAdmin;
    }
  }  
}
