export class AuthDTO {
    user: string;
    iat: string;
    exp: string;
}