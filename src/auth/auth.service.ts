import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Brackets, getConnection, Repository } from 'typeorm';
import { User } from 'src/users/entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Resultado } from 'src/utils/resultado';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { SECRET_KEY_FRONT, tipoRolSuperUsuarioID } from 'src/constantes';
import { AuthDTO } from './auth.dto';
import { Roles } from '../roles/entity/roles.entity';
import { RolPermiso } from '../rolPermiso/entity/rolPermiso.entity';
import { OficinasEntity } from '../tramites/entity/oficinas.entity';
import { Permisos } from '../permisos/entity/permisos.entity';
import { REQUEST } from '@nestjs/core';
import * as CryptoJS from 'crypto-js';
import { TipoRolesIds } from 'src/constantes';
import { TipoRol } from 'src/tipo-rol/entity/tipo-rol.entity';
import { GrupoPermisosRoles } from 'src/grupo-permisos/entity/grupo-permisos-roles.entity';
import { GrupoPermisos } from 'src/grupo-permisos/entity/grupo-permisos.entity';
import { GrupoPermisoXPermisos } from 'src/grupo-permisos/entity/grupo-permiso-permisos.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { AuthorizedPermissionsResult } from './dto/authorizedPermissionsResult.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,

    @Inject(REQUEST)
    private request: any,

    @InjectRepository(User)
    private readonly userRepo: Repository<User>,
    @InjectRepository(RolesUsuarios)
    private readonly rolUsuarioRepo: Repository<RolesUsuarios>,
    // @InjectRepository(OficinasEntity)
    // private readonly oficinasRepo: Repository<OficinasEntity>
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    console.log('validate user');
    const user = await this.userRepo.findOne({ email });
    if (user && user.passwd === pass) {
      return user;
    }
    return null;
  }

  async validateUserRun(run: number, passwd: string) {
    //console.log('validateUserRun');

    // Desencriptamos el password que nos llega del front
    var decryptedPasswordFront = CryptoJS.AES.decrypt(passwd, SECRET_KEY_FRONT).toString(CryptoJS.enc.Utf8);

    const user = await getConnection()
      .createQueryBuilder()
      .select('Usuarios')
      .from(User, 'Usuarios')
      .where('Usuarios.RUN = :run', { run: run })
      //.andWhere('Usuarios.passwd = :passwd', { passwd: passwd })
      .getOne();

    if (!user) return null;

    // Desencriptamos el password almacenado en la BBDD
    var decryptedPasswordBBDD = CryptoJS.AES.decrypt(user.passwd, SECRET_KEY_FRONT).toString(CryptoJS.enc.Utf8);

    // Si no coinciden los dos passwords desencriptados devolvemos null
    if (decryptedPasswordFront != decryptedPasswordBBDD) return null;

    return user;
  }

  async login(run: number) {
    const user = await this.userRepo.findOne({ RUN: run });
    const idUsuario = await this.userRepo.findOne({ RUN: run });
    console.log('***********************************************************************************************************');
    console.log('************************************** idUsuario **********************************************************');
    console.log('***********************************************************************************************************');
    console.log(idUsuario);
    const oficinas = await getConnection()
      .createQueryBuilder(OficinasEntity, 'oficinas')
      .innerJoin(RolesUsuarios, 'rolUsuario', 'rolUsuario.idOficina = oficinas.idOficina')
      .select(['oficinas.idOficina', 'oficinas.Nombre'])
      .where('rolUsuario.idUsuario = :idUsuario', { idUsuario: user.idUsuario })
      .getMany();
    //devuelve las oficinas para los reportes de elastic
    const ofReportes = await getConnection().query(`select OficinasReportes(${idUsuario.idUsuario})`);
    const rolesReportes = await getConnection().query(`select roles_reportes(${idUsuario.idUsuario})`);

    const roles = await getConnection()
      .createQueryBuilder(Roles, 'roles')
      .innerJoin(RolesUsuarios, 'rolUsuario', 'rolUsuario.idRol = roles.idRol')
      .select(['roles.Nombre'])
      .where('rolUsuario.idUsuario = :idUsuario', { idUsuario: user.idUsuario })
      .getMany();

    const permisos = await getConnection()
      .createQueryBuilder(Permisos, 'permisos')
      .innerJoin(RolPermiso, 'rolPermiso', 'permisos.idPermiso = rolPermiso.idPermiso')
      .innerJoin(Roles, 'roles', 'roles.idRol        = rolPermiso.idRol')
      .innerJoin(RolesUsuarios, 'rolUsuario', 'roles.idRol        = rolUsuario.idRol')
      .select(['permisos.Nombre', 'permisos.Codigo'])
      .where('rolUsuario.idUsuario = :idUsuario', { idUsuario: user.idUsuario })
      .getMany();

    // TODO: rolesPermisos queda en veremos.
    const rolesPermisos = this.rolUsuarioRepo
      .createQueryBuilder('rolesUsuario')
      .leftJoinAndMapMany('rolesUsuario.idOficina', OficinasEntity, 'oficinas', 'rolesUsuario.idOficina = oficinas.idOficina')
      .leftJoinAndMapMany('rolesUsuario.idRol', Roles, 'rol', 'rolesUsuario.idRol = rol.idRol')
      .leftJoinAndMapMany('rol.idRol', RolPermiso, 'rp', 'rol.idRol = rp.idRol')
      .leftJoinAndMapMany('rp.idPermiso', Permisos, 'permisos', 'rp.idPermiso = permisos.idPermiso')
      .select([
        'rolesUsuario.idRolesUsuario',
        'oficinas.idOficina',
        'oficinas.Nombre',
        'permisos.Nombre',
        'permisos.Codigo',
        'rol.Nombre',
        'rp.idPermiso',
      ]);
    rolesPermisos.andWhere('rolesUsuario.idUsuario = :idUsuario', { idUsuario: user.idUsuario });
    const roles2 = await rolesPermisos.getMany();

    console.log(roles2);

    const payload = {
      oficinaSeleccionada: oficinas[0],
      user: run,
      id: idUsuario.idUsuario,
      ofReportes,
      rolesReportes,
    };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  /**
   * Método que comprueba la validez de un token y si dicho usuario tiene los permisos necesários reflejados en los roles
   * @param tokenPermisos
   * @returns ResultadoOperación true || false, Mensaje -> información adicional, Respuesta -> Permisos requeridos que tiene el usuario
   * // TODO: este metodo tiene que ser usado en el validate no en los Controllers.
   */
  async checkUserAndRol(tokenPermisos: TokenPermisoDto): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    //let userPermisosNames = [];

    try {
      

      // Comprobamos si la cadena es válida y no es tá vacía
      if (tokenPermisos.tokenCU && tokenPermisos.tokenCU != '') {
        let token: AuthDTO = this.jwtService.decode(tokenPermisos.tokenCU) as AuthDTO;

        // Comprobamos si el array existe o que no esté vacío
        if (Array.isArray(tokenPermisos.permisos) && tokenPermisos.permisos.length) {
          const user = await this.userRepo.findOne({ RUN: parseInt(token.user) });

          // Comprobamos si el token pertenece a algún usuario
          if (user) {
            // Comprobamos si este usuario está activo
            if (user.Activo == true) {
              // Traemos los roles que tiene el usuario, con sus permisos
              const rolesUsuario = await this.rolUsuarioRepo.find({
                relations: [
                  'roles',
                  'roles.tipoRoles',
                  'roles.rolPermiso',
                  'roles.rolPermiso.permiso',
                  'roles.grupoPermisosRoles',
                  'roles.grupoPermisosRoles.grupoPermisos',
                  'roles.grupoPermisosRoles.grupoPermisos.grupoPermisoXPermisos',
                  'roles.grupoPermisosRoles.grupoPermisos.grupoPermisoXPermisos.permisos',
                ],

                where: { idUsuario: user.idUsuario, activo: true },
              });

              // Comprobamos si el usuario tiene roles asignados
              if (Array.isArray(rolesUsuario) && rolesUsuario.length) {
                //let rawPermisosNames = [];
                const rolePermitidos: RolesUsuarios[] = [];

                // Con que tenga rol Superusuario, lo devolvemos sin comprobar los permisos de este
                const rolSuperUsuario = rolesUsuario.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

                if (rolSuperUsuario != undefined) {
                  rolePermitidos.push(rolSuperUsuario);
                }

                // Por cada RolUsuario obtenemos los Permisos y los Grupo Permisos
                rolesUsuario.forEach(userRol => {
                  // Comparamos el Permiso necesitado con los Permisos de cada Rol y obtenemos su codido
                  if (userRol.roles.rolPermiso.find(x => tokenPermisos.permisos.includes(x.permiso.Codigo))) {
                    rolePermitidos.push(userRol);
                  }

                  // Comparamos el Permiso necesitado con los Permisos de cada GrupoPermiso y obtenemos su codido
                  userRol.roles.grupoPermisosRoles.forEach(grupoPermisoRol => {
                    if (grupoPermisoRol.grupoPermisos.grupoPermisoXPermisos.find(x => tokenPermisos.permisos.includes(x.permisos.Codigo))) {
                      rolePermitidos.push(userRol);
                    }
                  });
                });

                // Eliminar Duplicados
                const rolesPermitidosFiltrados = rolePermitidos.filter((x, i) => rolePermitidos.indexOf(x) === i);

                if (rolesPermitidosFiltrados.length === 0) {
                  resultado.ResultadoOperacion = false;
                  resultado.Error = 'Los permisos facilitados no coinciden';
                  resultado.Mensaje = 'El usuario no tiene el permiso requerido';
                } else {
                  resultado.Respuesta = rolesPermitidosFiltrados;
                  resultado.ResultadoOperacion = true;
                  resultado.Mensaje = 'El usuario tiene el permiso requerido';
                }
              } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'El usuario no tiene roles asignados';
              }
            } else {
              resultado.ResultadoOperacion = false;
              resultado.Error = 'Usted está desactivado. Por favor póngase en contacto con el área técnica.';
            }
          } else {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Token inválido, no existe usuario con este token';
          }
        } else {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'No se han pasado roles por parámetro o el array de roles está vacío';
        }
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'El parámetro token es indefinido o vacío';
      }
    } catch (error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = error;
    }
    return resultado;
  }

  private decodeToken() {
    const bearer = this.request.headers.authorization!;
    if (bearer) {
      const token = bearer.split(' ')[1];
      const decode = <any>this.jwtService.decode(token);
      return decode;
    }
    return null;
  }

  oficinas() {
    const decode = this.decodeToken();
    if (decode) {
      const oficinas = decode.oficinas.map((oficina: any) => oficina.idOficina);
      console.log('oficinas()', oficinas);
      return oficinas;
    }
  }

  oficina() {
    const decode = this.decodeToken();
    if (decode) {
      const oficina = decode.oficinaSeleccionada;
      console.log('oficina()', oficina);
      return oficina;
    }
    return null;
  }

  async usuario() {
    const decode = this.decodeToken();
    if (decode) {
      const run = decode.user;
      const usuario = await this.userRepo.findOne({ where: { RUN: run }, cache: true });
      console.log('usuario()', usuario);
      return usuario;
    }
    return null;
  }

  tienePermiso(codigo: string) {
    console.log(
      '============================================= Entro a verificar permisos ======================================================================='
    );
    console.log(
      '============================================= Entro a verificar permisos ======================================================================='
    );
    console.log(
      '============================================= Entro a verificar permisos ======================================================================='
    );
    const decode = this.decodeToken();
    if (decode) {
      const existe = decode.permisos.some((permiso: any) => permiso.Codigo == codigo);

      console.log(existe);
      return true;
    }
  }

  async checkPermission(req: any, codigoPermiso: string) {
    let validarPermisos: Resultado = new Resultado();

    try {
      let splittedBearerToken = req.headers.authorization.split(' ');
      let token = splittedBearerToken[1];

      let tokenPermisos = new TokenPermisoDto(token, [codigoPermiso]);

      validarPermisos = await this.checkUserAndRol(tokenPermisos);

      return validarPermisos;
    } catch (Error) {
      validarPermisos.Error = 'Se ha producido un error en la validación de permisos';
      validarPermisos.ResultadoOperacion = false;

      return validarPermisos;
    }
  }

  // método para consultar según los roles del usuario si es superusuario
  checkUserSuperAdmin(rolesPermisos: RolesUsuarios[]): boolean {
    if (!rolesPermisos || rolesPermisos.length < 1) {
      return false;
    } else {
      // tipo superusuario
      return rolesPermisos.filter(x => x.roles.tipoRol == TipoRolesIds.Superusuario).length > 0;
    }
  }

  // método para devolver las oficinas a las que está asociado el usuario
  getOficinasUser(rolesPermisos: RolesUsuarios[]) : number[] {
    if (!rolesPermisos || rolesPermisos.length < 1) {
      return null;
    }

    let rolesP = rolesPermisos.filter(x => x.roles.tipoRol == TipoRolesIds.Oficina && x.idOficina != null);
    let oficinasAsociadasRegion: number[] = [];

    if(rolesPermisos.filter(y => y.roles.tipoRol == TipoRolesIds.Regional).length > 0){
      let idsRegiones : number[] = rolesPermisos.filter(y => y.roles.tipoRol == TipoRolesIds.Regional).map(x => x.idRegion);

      if(idsRegiones && idsRegiones.length > 0){
        // Añadir ids de oficinas para la región
        rolesPermisos.filter(y => y.roles.tipoRol == TipoRolesIds.Regional).forEach( rolP => {
          if(rolP && rolP.regiones && rolP.regiones.comunasAuxPermisos && rolP.regiones.comunasAuxPermisos.length > 0){
            rolP.regiones.comunasAuxPermisos.forEach(com => {
              if(com && com.institucionesAuxPermisos && com.institucionesAuxPermisos.oficinasAuxPermisos && com.institucionesAuxPermisos.oficinasAuxPermisos.length > 0){
                com.institucionesAuxPermisos.oficinasAuxPermisos.forEach( ofi => {
                  oficinasAsociadasRegion.push(ofi.idOficina);
                })
              }
            })
          }
        });
      }
    }

    if ((!rolesP || rolesP.length < 1) && (!oficinasAsociadasRegion || oficinasAsociadasRegion.length < 1) ) return null;
    else {
      let oficinasRespuesta : number[] = [];

      oficinasRespuesta = oficinasRespuesta.concat(oficinasAsociadasRegion);
      oficinasRespuesta = oficinasRespuesta.concat(rolesP.map(x => x.idOficina));

      return oficinasRespuesta;
    };
  }

  /**
   * Método que comprueba la validez de un token y si dicho usuario tiene los permisos necesários reflejados en los roles
   * @param tokenPermisos
   * @returns ResultadoOperación true || false, Mensaje -> información adicional, Respuesta -> Permisos requeridos que tiene el usuario
   * // TODO: este metodo tiene que ser usado en el validate no en los Controllers.
   */
  async checkUserAndRolAndReturnRolesFromPermission(req: any, permisoAutorizado: string, idOficinaAutorizada?: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    //let userPermisosNames = [];

    try {
      let splittedBearerToken = req.headers.authorization.split(' ');
      let token = splittedBearerToken[1];

      let tokenPermisos = new TokenPermisoDto(token, [permisoAutorizado]);

      // Comprobamos si la cadena es válida y no es tá vacía
      if (tokenPermisos.tokenCU && tokenPermisos.tokenCU != '') {
        let token: AuthDTO = this.jwtService.decode(tokenPermisos.tokenCU) as AuthDTO;

        // Comprobamos si el array existe o que no esté vacío
        if (Array.isArray(tokenPermisos.permisos) && tokenPermisos.permisos.length) {
          const qbUser = this.userRepo
            .createQueryBuilder('Usuario')
            .leftJoinAndMapMany('Usuario.rolesUsuarios', RolesUsuarios, 'rolesUsuarios', 'Usuario.idUsuario = rolesUsuarios.idUsuario')
            .leftJoinAndMapOne('rolesUsuarios.regiones', RegionesEntity, 'regiones', 'rolesUsuarios.idRegion = regiones.idRegion')
            .leftJoinAndMapMany('regiones.comunasAuxPermisos', ComunasEntity, 'comunasAuxPermisos', 'regiones.idRegion = comunasAuxPermisos.idRegion')
            .leftJoinAndMapOne('comunasAuxPermisos.institucionesAuxPermisos', InstitucionesEntity, 'institucionesAuxPermisos', 'comunasAuxPermisos.idComuna = institucionesAuxPermisos.idComuna')
            .leftJoinAndMapMany('institucionesAuxPermisos.oficinasAuxPermisos', OficinasEntity, 'oficinasAuxPermisos', 'institucionesAuxPermisos.idInstitucion = oficinasAuxPermisos.idInstitucion')

            .leftJoinAndMapOne('rolesUsuarios.oficinas', OficinasEntity, 'oficinas', 'rolesUsuarios.idOficina = oficinas.idOficina')
            .leftJoinAndMapOne('oficinas.instituciones', InstitucionesEntity, 'instituciones', 'oficinas.idInstitucion = instituciones.idInstitucion')
            .leftJoinAndMapOne('instituciones.comunas', ComunasEntity, 'comunas', 'instituciones.idComuna = comunas.idComuna')
            .leftJoinAndMapOne('rolesUsuarios.roles', Roles, 'roles', 'rolesUsuarios.idRol = roles.idRol')
            .leftJoinAndMapOne('roles.tipoRoles', TipoRol, 'tipoRoles', 'roles.tipoRol = tipoRoles.idTipoRol')
            .leftJoinAndMapMany('roles.rolPermiso', RolPermiso, 'rolPermiso', 'roles.idRol = rolPermiso.idRol')
            .leftJoinAndMapOne('rolPermiso.permiso', Permisos, 'permiso', 'rolPermiso.idPermiso = permiso.idPermiso')
            .leftJoinAndMapMany('roles.grupoPermisosRoles', GrupoPermisosRoles, 'grupoPermisosRoles', 'roles.idRol = grupoPermisosRoles.idRol')
            .leftJoinAndMapOne('grupoPermisosRoles.grupoPermisos', GrupoPermisos,'grupoPermisos','grupoPermisosRoles.idGrupoPermiso = grupoPermisos.idGrupoPermiso')
            .leftJoinAndMapMany('grupoPermisos.grupoPermisoXPermisos', GrupoPermisoXPermisos,'grupoPermisoXPermisos','grupoPermisos.idGrupoPermiso = grupoPermisoXPermisos.idGrupoPermiso')
            .leftJoinAndMapOne('grupoPermisoXPermisos.permisosAux', Permisos,'permisosAux','grupoPermisoXPermisos.idPermiso = permisosAux.idPermiso');

          qbUser.where(
            new Brackets(subQb => {
              subQb
                .where('permiso.Codigo IN (:..._codigoPermiso)', {
                  _codigoPermiso: tokenPermisos.permisos,
                })
                .orWhere('permisosAux.Codigo IN (:..._codigoPermiso)', {
                  _codigoPermiso: tokenPermisos.permisos,
                })
                .orWhere('roles.tipoRol = 1');
            })
          );

          qbUser.andWhere('Usuario.RUN = :_RUN', { _RUN: parseInt(token.user) });

          //     if(idOficinaAutorizada) // Si se informa una oficina se agrega al filtro
          //     {
          //       qb.andWhere('rolesUsuario.idOficina')
          //     }

          const usuarioRoles: User = await qbUser.getOne();

          if (!usuarioRoles) {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'El usuario no tiene los permisos requeridos.';

            return resultado;
          }

          if (usuarioRoles.Activo == false) {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Usted está desactivado. Por favor póngase en contacto con el área técnica.';

            return resultado;
          }

          // Comprobamos si el usuario tiene roles asignados
          if (Array.isArray(usuarioRoles.rolesUsuarios) && usuarioRoles.rolesUsuarios.length) {
            //let rawPermisosNames = [];
            const rolePermitidos: RolesUsuarios[] = [];

            // Con que tenga rol Superusuario, lo devolvemos sin comprobar los permisos de este
            const rolSuperUsuario = usuarioRoles.rolesUsuarios.find(x => x.roles.tipoRol == 1);

            if (rolSuperUsuario != undefined) {
              rolePermitidos.push(rolSuperUsuario);
            }

            // Por cada RolUsuario obtenemos los Permisos y los Grupo Permisos
            usuarioRoles.rolesUsuarios.forEach(userRol => {
              // Comparamos el Permiso necesitado con los Permisos de cada Rol y obtenemos su codido
              if (userRol.roles.rolPermiso.find(x => tokenPermisos.permisos.includes(x.permiso.Codigo))) {
                rolePermitidos.push(userRol);
              }

              // Comparamos el Permiso necesitado con los Permisos de cada GrupoPermiso y obtenemos su codido
              userRol.roles.grupoPermisosRoles.forEach(grupoPermisoRol => {
                if (grupoPermisoRol.grupoPermisos.grupoPermisoXPermisos.find(x => tokenPermisos.permisos.includes(x.permisosAux.Codigo))) {
                  rolePermitidos.push(userRol);
                }
              });
            });

            // Eliminar Duplicados
            const rolesPermitidosFiltrados = rolePermitidos.filter((x, i) => rolePermitidos.indexOf(x) === i);

            if (rolesPermitidosFiltrados.length === 0) {
              resultado.ResultadoOperacion = false;
              resultado.Error = 'Los permisos facilitados no coinciden';
              resultado.Mensaje = 'El usuario no tiene el permiso requerido';
            } else {
              usuarioRoles.rolesUsuarios = rolesPermitidosFiltrados;
              resultado.Respuesta = usuarioRoles;
              resultado.ResultadoOperacion = true;
              resultado.Mensaje = 'El usuario tiene el permiso requerido';
            }
          } else {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'El usuario no tiene roles asignados';
          }
        } else {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'No se han pasado roles por parámetro o el array de roles está vacío';
        }
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'El parámetro token es indefinido o vacío';
      }
    } catch (error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = error;
    }
    return resultado;
  }

  /**
   * Método que comprueba la validez de un token y si dicho usuario tiene los permisos necesários reflejados en los roles
   * @param tokenPermisos
   * @returns ResultadoOperación true || false, Mensaje -> información adicional, Respuesta -> Permisos requeridos que tiene el usuario
   * // TODO: este metodo tiene que ser usado en el validate no en los Controllers.
   */
  async getUserAndRolesWithToken(req: any): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    //let userPermisosNames = [];

    try {
      let splittedBearerToken = req.headers.authorization.split(' ');
      let token = splittedBearerToken[1];

      let tokenPermisos = new TokenPermisoDto(token, ['menuAdmin']);

      // Comprobamos si la cadena es válida y no es tá vacía
      if (tokenPermisos.tokenCU && tokenPermisos.tokenCU != '') {
        let token: AuthDTO = this.jwtService.decode(tokenPermisos.tokenCU) as AuthDTO;

        // Comprobamos si el array existe o que no esté vacío
        if (Array.isArray(tokenPermisos.permisos) && tokenPermisos.permisos.length) {
          const user = await this.userRepo.findOne({ RUN: parseInt(token.user) });

          // Comprobamos si el token pertenece a algún usuario
          if (user) {
            // Comprobamos si este usuario está activo
            if (user.Activo == true) {
              // Traemos los roles que tiene el usuario, con sus permisos
              const rolesUsuario = await this.rolUsuarioRepo.find({
                relations: [
                  'roles',
                  'roles.tipoRoles',
                  'roles.rolPermiso',
                  'roles.rolPermiso.permiso',
                  'roles.grupoPermisosRoles',
                  'roles.grupoPermisosRoles.grupoPermisos',
                  'roles.grupoPermisosRoles.grupoPermisos.grupoPermisoXPermisos',
                  'roles.grupoPermisosRoles.grupoPermisos.grupoPermisoXPermisos.permisos',
                ],

                where: { idUsuario: user.idUsuario, activo: true },
              });

              // Comprobamos si el usuario tiene roles asignados
              if (Array.isArray(rolesUsuario) && rolesUsuario.length) {
                //let rawPermisosNames = [];
                const rolePermitidos: RolesUsuarios[] = [];

                // Con que tenga rol Superusuario, lo devolvemos sin comprobar los permisos de este
                const rolSuperUsuario = rolesUsuario.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

                if (rolSuperUsuario != undefined) {
                  rolePermitidos.push(rolSuperUsuario);
                }

                // Por cada RolUsuario obtenemos los Permisos y los Grupo Permisos
                rolesUsuario.forEach(userRol => {
                  rolePermitidos.push(userRol);

                  // // Comparamos el Permiso necesitado con los Permisos de cada Rol y obtenemos su codido
                  // if (userRol.roles.rolPermiso.find(x => tokenPermisos.permisos.includes(x.permiso.Codigo))) {
                  //   rolePermitidos.push(userRol);
                  // }

                  // // Comparamos el Permiso necesitado con los Permisos de cada GrupoPermiso y obtenemos su codido
                  // userRol.roles.grupoPermisosRoles.forEach(grupoPermisoRol => {
                  //   if (grupoPermisoRol.grupoPermisos.grupoPermisoXPermisos.find(x => tokenPermisos.permisos.includes(x.permisos.Codigo))) {
                  //     rolePermitidos.push(userRol);
                  //   }
                  // });
                });

                // Eliminar Duplicados
                const rolesPermitidosFiltrados = rolePermitidos.filter((x, i) => rolePermitidos.indexOf(x) === i);

                if (rolesPermitidosFiltrados.length === 0) {
                  resultado.ResultadoOperacion = false;
                  resultado.Error = 'Los permisos facilitados no coinciden';
                  resultado.Mensaje = 'El usuario no tiene el permiso requerido';
                } else {
                  resultado.Respuesta = rolesPermitidosFiltrados;
                  resultado.ResultadoOperacion = true;
                  resultado.Mensaje = 'El usuario tiene el permiso requerido';
                }
              } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'El usuario no tiene roles asignados';
              }
            } else {
              resultado.ResultadoOperacion = false;
              resultado.Error = 'Usted está desactivado. Por favor póngase en contacto con el área técnica.';
            }
          } else {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Token inválido, no existe usuario con este token';
          }
        } else {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'No se han pasado roles por parámetro o el array de roles está vacío';
        }
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'El parámetro token es indefinido o vacío';
      }
    } catch (error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = error;
    }
    return resultado;
  }

  /**
   * Método que comprueba la validez de un token y si dicho usuario tiene los permisos necesários reflejados en los roles
   * @param tokenPermisos
   * @returns ResultadoOperación true || false, Mensaje -> información adicional, Respuesta -> Permisos requeridos que tiene el usuario
   * // TODO: este metodo tiene que ser usado en el validate no en los Controllers.
   */
   async checkIfUserHavePermission(req: any, permisosAutorizados: string[], idOficinaAutorizada?: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    //let userPermisosNames = [];

    try {
      let splittedBearerToken = req.headers.authorization.split(' ');
      let token = splittedBearerToken[1];

      let tokenPermisos = new TokenPermisoDto(token, permisosAutorizados);

      // Comprobamos si la cadena es válida y no es tá vacía
      if (tokenPermisos.tokenCU && tokenPermisos.tokenCU != '') {
        let token: AuthDTO = this.jwtService.decode(tokenPermisos.tokenCU) as AuthDTO;

        // Comprobamos si el array existe o que no esté vacío
        if (Array.isArray(tokenPermisos.permisos) && tokenPermisos.permisos.length) {
          const qbUser = this.userRepo
            .createQueryBuilder('Usuario')
            .leftJoinAndMapMany('Usuario.rolesUsuarios', RolesUsuarios, 'rolesUsuarios', 'Usuario.idUsuario = rolesUsuarios.idUsuario')
            .leftJoinAndMapOne('rolesUsuarios.roles', Roles, 'roles', 'rolesUsuarios.idRol = roles.idRol')
            .leftJoinAndMapMany('roles.rolPermiso', RolPermiso, 'rolPermiso', 'roles.idRol = rolPermiso.idRol')
            .leftJoinAndMapOne('rolPermiso.permiso', Permisos, 'permiso', 'rolPermiso.idPermiso = permiso.idPermiso')
            .leftJoinAndMapMany('roles.grupoPermisosRoles', GrupoPermisosRoles, 'grupoPermisosRoles', 'roles.idRol = grupoPermisosRoles.idRol')
            .leftJoinAndMapOne('grupoPermisosRoles.grupoPermisos', GrupoPermisos,'grupoPermisos','grupoPermisosRoles.idGrupoPermiso = grupoPermisos.idGrupoPermiso')
            .leftJoinAndMapMany('grupoPermisos.grupoPermisoXPermisos', GrupoPermisoXPermisos,'grupoPermisoXPermisos','grupoPermisos.idGrupoPermiso = grupoPermisoXPermisos.idGrupoPermiso')
            .leftJoinAndMapOne('grupoPermisoXPermisos.permisosAux', Permisos,'permisosAux','grupoPermisoXPermisos.idPermiso = permisosAux.idPermiso');

          qbUser.where(
            new Brackets(subQb => {
              subQb
                .where('permiso.Codigo IN (:..._codigoPermiso)', {
                  _codigoPermiso: tokenPermisos.permisos,
                })
                .orWhere('permisosAux.Codigo IN (:..._codigoPermiso)', {
                  _codigoPermiso: tokenPermisos.permisos,
                })
                .orWhere('roles.tipoRol = 1');
            })
          );

          qbUser.andWhere('Usuario.RUN = :_RUN', { _RUN: parseInt(token.user) });

          const usuarioRoles: User = await qbUser.getOne();

          let authorizedPermissionResult : AuthorizedPermissionsResult[] = [];

          if (!usuarioRoles) {

            permisosAutorizados.forEach(pA => {
              let apr : AuthorizedPermissionsResult = new AuthorizedPermissionsResult();

              apr.PermissionRequest = pA;
              apr.HasPermission = false;

              authorizedPermissionResult.push(apr);
            });  

            resultado.ResultadoOperacion = false;
            resultado.Respuesta = authorizedPermissionResult;
            resultado.Mensaje = 'El usuario no tiene el permiso requerido.';

            return resultado;
          }

          if (usuarioRoles.Activo == false) {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Usted está desactivado. Por favor póngase en contacto con el área técnica.';

            return resultado;
          }


          // Comprobamos si el usuario tiene roles asignados
          if (Array.isArray(usuarioRoles.rolesUsuarios) && usuarioRoles.rolesUsuarios.length) {
            //const rolePermitidos: RolesUsuarios[] = [];

            
            // Con que tenga rol Superusuario, lo devolvemos sin comprobar los permisos de este
            const rolSuperUsuario = usuarioRoles.rolesUsuarios.find(x => x.roles.tipoRol == 1);

            if (rolSuperUsuario != undefined) {
              permisosAutorizados.forEach(pA => {
                let apr : AuthorizedPermissionsResult = new AuthorizedPermissionsResult();

                apr.PermissionRequest = pA;
                apr.HasPermission = true;

                authorizedPermissionResult.push(apr);
              });

              resultado.ResultadoOperacion = true;
              resultado.Mensaje = 'El usuario tiene el permiso requerido';
              resultado.Respuesta = authorizedPermissionResult;

              return resultado;

            }

            // Por cada permiso que nos viene, comprobamos si lo tenemos
            tokenPermisos.permisos.forEach(tpp => {

            // Por cada RolUsuario obtenemos los Permisos y los Grupo Permisos
            usuarioRoles.rolesUsuarios.forEach(userRol => {

              // Comparamos el Permiso necesitado con los Permisos de cada Rol y obtenemos su cogido

              // 1 Asignamos para permisos asignados directamente al rol
              let permisosEncontrados : RolPermiso = userRol.roles.rolPermiso.find(x => x.permiso.Codigo == tpp);

              if (permisosEncontrados && (!authorizedPermissionResult.find(y => y.PermissionRequest == tpp))) {

                let apr : AuthorizedPermissionsResult = new AuthorizedPermissionsResult();

                apr.PermissionRequest = tpp;
                apr.HasPermission = true;

                authorizedPermissionResult.push(apr);
              }

              // 2 Asignamos para permisos asignados mediante grupos de permisos


                userRol.roles.grupoPermisosRoles.forEach(gPermRol => {
                  if(gPermRol.grupoPermisos && gPermRol.grupoPermisos.grupoPermisoXPermisos && gPermRol.grupoPermisos.grupoPermisoXPermisos.length > 0){


                    if(gPermRol.grupoPermisos.grupoPermisoXPermisos.find(x => x.permisosAux.Codigo == tpp) && (!authorizedPermissionResult.find(y => y.PermissionRequest == tpp))){
                      let apr : AuthorizedPermissionsResult = new AuthorizedPermissionsResult();
          
                      apr.PermissionRequest = tpp;
                      apr.HasPermission = true;
      
                      authorizedPermissionResult.push(apr);
                    }
                    
                  }                
                })

              

            });

            });

            // Comprobamos si hay algún permiso que no se haya añadido y devolvemos falso
            let resultadoPermisosNoAutorizados : string[] = tokenPermisos.permisos.filter(x => !authorizedPermissionResult.map(y => y.PermissionRequest).includes(x));

            if(resultadoPermisosNoAutorizados && resultadoPermisosNoAutorizados.length > 1){
              resultadoPermisosNoAutorizados.forEach(rpna => {
                let apr : AuthorizedPermissionsResult = new AuthorizedPermissionsResult();

                apr.PermissionRequest = rpna;
                apr.HasPermission = false;
  
                authorizedPermissionResult.push(apr);                  
              })
            }

            resultado.Respuesta = authorizedPermissionResult;
            resultado.ResultadoOperacion = false;
            resultado.Error = 'El usuario tiene permisos de los facilitados.';

            return resultado;            

          } else {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'El usuario no tiene roles asignados';

            return resultado;
          }
        } else {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'No se han pasado roles por parámetro o el array de roles está vacío';

          return resultado;
        }
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'El parámetro token es indefinido o vacío';

        return resultado;
      }
    } catch (error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = error;

      return resultado;
    }
  }  
}
