import { ApiPropertyOptional } from "@nestjs/swagger";

export class AuthorizedPermissionsResult
{

    @ApiPropertyOptional()
    PermissionRequest: string;

    @ApiPropertyOptional()
    HasPermission: boolean;

}