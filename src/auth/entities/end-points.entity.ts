import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'EndPoints' )
export class EndpointsEntity
{
    @PrimaryGeneratedColumn()
    idEndPoint: number;

    @Column()
    uriEndPoint: string;

    @Column()
    nombreEndPoint: string;

    @Column()
    descripcion: string;

    @Column()
    metodo: string;

    @Column()
    idPermiso: number;
}