import { ExecutionContext, Injectable, SetMetadata } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard as PassportAuthGaurd } from '@nestjs/passport';
import { Permisos } from 'src/permisos/entity/permisos.entity';
import { getManager } from 'typeorm';
import { EndpointsEntity } from '../entities/end-points.entity';
export const Public = () => SetMetadata('isPublic', true);

@Injectable()
export class AuthGuard extends PassportAuthGaurd('jwt') {
  constructor(private readonly reflector: Reflector, private jwtService: JwtService) {
    super();
  }

  async canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.get<boolean>('isPublic', context.getHandler());

    if (isPublic) return true;

    if (await super.canActivate(context)) return this.canExecuteEndpoint(context);
  }

  private async canExecuteEndpoint(context: ExecutionContext) {
    const http = context.switchToHttp();
    const request = http.getRequest();
    const url = request.route.path.toLowerCase();
    const metodo = request.route.stack[0].method.toLowerCase();
    const permiso = await this.permisoBuscar(url, metodo);

    console.log('Permiso');

    if (permiso) return this.tienePermiso(request, permiso.Codigo);

    console.log('Crear endpoint');
    // return false;
    return await this.endpointCrear(context, url, metodo); // cambiar para que valide correctamente
  }

  private async permisoBuscar(uriEndPoint: string, metodo: string) {
    const permiso = await getManager()
      .createQueryBuilder(Permisos, 'per')
      .innerJoinAndSelect(EndpointsEntity, 'end', 'end.idPermiso = per.idPermiso')
      .where('end.uriEndPoint  = :uriEndPoint', { uriEndPoint })
      .andWhere('end.metodo       = :metodo', { metodo })
      .cache(true)
      .getOne();

    console.log('permisoBuscar(): ', permiso);

    return permiso;
  }

  private async endpointCrear(context: ExecutionContext, uriEndPoint: string, metodo: string) {
    const nombreEndPoint = context.getHandler().name;
    const descripcion = '';
    const idPermiso = 1;
    const data = { uriEndPoint, metodo, descripcion, nombreEndPoint, idPermiso };

    await getManager().save(EndpointsEntity, data);

    return true;
  }

  private decodeToken(request: Request) {
    const bearer = request.headers['authorization'];

    if (bearer) {
      const token = bearer.split(' ')[1];
      const decode = <any>this.jwtService.decode(token);

      return decode;
    }
    return null;
  }

  private tienePermiso(request: Request, codigo: string) {
    const decode = this.decodeToken(request);

    if (decode) {
      // TODO: revisar esta seccion.
      // const existe = decode.permisos.some(( permiso: any ) => permiso.Codigo == codigo );
      // return existe;
      return true;
    }
  }
}
