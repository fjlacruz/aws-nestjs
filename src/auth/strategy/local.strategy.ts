import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from '../auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'RUN',
      passwordField: 'passwd',
    });
  }

  /**
   *
   * @param run
   * @param password
   */
  async validate(run: number, password: string) {
    const user = await this.authService.validateUserRun(run, password);
    if (!user) {
      throw new UnauthorizedException('Login user or password does not nat');
    }
    return user;
  }
}
