import { PrimaryGeneratedColumn, Column, PrimaryColumn } from 'typeorm';

export abstract class BaseEntity {
  @PrimaryColumn()
  id?: number;

  @Column({ nullable: true })
  createdBy?: string;
  @Column({ nullable: true })
  createdAt?: Date;
  @Column({ nullable: true })
  modifiedBy?: string;
  @Column({ nullable: true })
  updatedAt?: Date;
}
