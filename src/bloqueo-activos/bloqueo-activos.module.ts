import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { BloqueoActivosController } from './controller/bloqueo-activos.controller';
import { BloqueoActivosEntity } from './entity/bloqueo-activos.entity';
import { BloqueoActivosService } from './service/bloqueo-activos.service';

@Module({
  imports:      [
                  TypeOrmModule.forFeature( [BloqueoActivosEntity,], "reserva" ),
                  TypeOrmModule.forFeature( [User, RolesUsuarios] )
                ],
  providers:    [BloqueoActivosService, AuthService],
  exports:      [BloqueoActivosService],
  controllers:  [BloqueoActivosController],
})
export class BloqueoActivosModule {}
