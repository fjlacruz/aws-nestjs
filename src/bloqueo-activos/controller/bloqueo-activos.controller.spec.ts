import { Test, TestingModule } from '@nestjs/testing';
import { BloqueoActivosController } from './bloqueo-activos.controller';

describe('BloqueoActivosController', () => {
  let controller: BloqueoActivosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BloqueoActivosController],
    }).compile();

    controller = module.get<BloqueoActivosController>(BloqueoActivosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
