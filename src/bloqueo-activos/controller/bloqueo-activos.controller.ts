import { Body, Controller, Get, Param, Patch, Post, Req } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { PostBloqueoActivosDTO } from '../dto/bloqueo-activos.dto';
import { BloqueoActivosService } from '../service/bloqueo-activos.service';

@ApiTags('Bloqueo-activos')
@Controller('bloqueo-activos')
export class BloqueoActivosController {
  constructor(private readonly bloqueoActivosService: BloqueoActivosService) {}

  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los bloqueos de la oficina actual' })
  async getTodos(@Req() req: any) {
    return this.bloqueoActivosService.buscarTodos(req).then(data => {
      return { code: 200, data };
    });
  }

  @Get('municipio/:idMunicipio')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los bloqueos de un municipio' })
  async getPorMunicipio(@Param('idMunicipio') idMunicipio: number, @Req() req: any) {
    return this.bloqueoActivosService.buscarPorMunicipio(req, Number(idMunicipio)).then(data => {
      return { code: 200, data };
    });
  }

  @Get('/entre-fechas/:fechaDesde/:fechaHasta/:idMunicipio')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los bloqueos entre dos fechas' })
  async getEntreFechas(
    @Param('fechaDesde') fechaDesde: Date,
    @Param('fechaHasta') fechaHasta: Date,
    @Param('idMunicipio') idMunicipio: number,
    @Req() req
  ) {
    return this.bloqueoActivosService.buscarEntreFechas(req, fechaDesde, fechaHasta, Number(idMunicipio)).then(data => {
      return { code: 200, data };
    });
  }

  /* @Post()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un bloqueo' })
  async postCreate(@Body() dto: PostBloqueoActivosDTO) {
    return this.bloqueoActivosService.crear(dto).then(data => {
      return { code: 200, data };
    });
  }*/

  @Post()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para crear un bloqueo activo',
  })
  async crear(@Body() data: any, @Req() req) {
    return this.bloqueoActivosService.crear(req, data);
  }

  @Post('validaBloqueos')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para validar bloques ya selecionados',
  })
  async validaBloqueos(@Body() data: any) {
    return this.bloqueoActivosService.validaBloqueos(data);
  }

  @Patch('/:id/:idUsuario/:Activo')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que reversa un bloqueo' })
  async patchUpdate(@Param('id') id: number, @Param('Activo') Activo: boolean, @Req() req) {
    return this.bloqueoActivosService.reversar(req, Number(id), Activo).then(data => {
      return { code: 200 };
    });
  }
}
