import { ApiPropertyOptional } from "@nestjs/swagger";

export class PostBloqueoActivosDTO
{
    @ApiPropertyOptional()
    FechaInicio: Date;

    @ApiPropertyOptional()
    FechaFin: Date;

    // @ApiPropertyOptional()
    // idUsuarioCreacion: number;

    // @ApiPropertyOptional()
    // idMunicipio: number;
}
