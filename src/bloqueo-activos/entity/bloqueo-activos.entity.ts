import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity( 'BloqueoActivos' )
export class BloqueoActivosEntity 
{
    @PrimaryGeneratedColumn()
    idBloqueoActivo: number;

    @Column()
    Activo: boolean;

    @Column({ type: 'date' })
    FechaInicio: Date;

    @Column({ type: 'date' })
    FechaFin: Date;

    @Column()
    idUsuarioCreacion: number;
    
    @Column()
    FechaReversion: Date;

    @Column()
    idUsuarioReversion: number;
    
    @Column()
    FechaCreacion: Date;

    @Column()
    idMunicipio: number;
}
