import { Test, TestingModule } from '@nestjs/testing';
import { BloqueoActivosService } from './bloqueo-activos.service';

describe('BloqueoActivosService', () => {
  let service: BloqueoActivosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BloqueoActivosService],
    }).compile();

    service = module.get<BloqueoActivosService>(BloqueoActivosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
