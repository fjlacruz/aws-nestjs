import { HttpService, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { isInteger } from 'lodash';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/users/entity/user.entity';
import { PostBloqueoActivosDTO } from '../dto/bloqueo-activos.dto';
import { BloqueoActivosEntity } from '../entity/bloqueo-activos.entity';
import { getConnection, Repository, UpdateResult } from 'typeorm';
import { PermisosNombres } from 'src/constantes';

@Injectable()
export class BloqueoActivosService {
  auth = '';

  constructor(
    @InjectRepository(BloqueoActivosEntity, 'reserva')
    private readonly bloqueoActivosEntityRepository: Repository<BloqueoActivosEntity>,

    @InjectRepository(User)
    private readonly userRepository: Repository<User>,

    private readonly authService: AuthService
  ) {}
  async buscarTodos(req) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ReservadeHorasBloqueosActivosBuscarBloqueoActivo);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	

    const idMunicipio = this.authService.oficina().idOficina;
    return await this.buscarPorMunicipio(req, idMunicipio);
  }
  async buscarPorMunicipio(req, idMunicipio: number) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ReservadeHorasBloqueosActivosBuscarBloqueoActivo);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	

    const data = await this.bloqueoActivosEntityRepository
      .createQueryBuilder('bloqueo')
      .where('bloqueo.idMunicipio = :idMunicipio', { idMunicipio })
      .getMany();

    return this.joinUsuario(data);
  }
  async buscarEntreFechas(req, fechaDesde: Date, fechaHasta: Date, idMunicipio: number) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ReservadeHorasBloqueosActivosBuscarBloqueoActivo);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	    

    const data = await this.bloqueoActivosEntityRepository
      .createQueryBuilder('bloqueo')
      .andWhere('bloqueo.idMunicipio =  :idMunicipio', { idMunicipio })
      .andWhere('bloqueo.FechaInicio >= :fechaDesde', { fechaDesde })
      .andWhere('bloqueo.FechaFin    <= :fechaHasta', { fechaHasta })
      .getMany();

    return this.joinUsuario(data);
  }
  private async joinUsuario(data: BloqueoActivosEntity[]) {
    const join = data.map(async (bloque: BloqueoActivosEntity) => {
      const UsuarioCreacion = await this.getUsuario(bloque.idUsuarioCreacion);
      const UsuarioReversion = await this.getUsuario(bloque.idUsuarioReversion);

      return { ...bloque, UsuarioCreacion, UsuarioReversion };
    });
    return Promise.all(join);
  }

  async validaBloqueos(data): Promise<any> {
    let FechaInicio = data.FechaInicio;
    let FechaFin = data.FechaFin;
    let idMunicipio = data.idMunicipio;
    let validaBolueRes = await getConnection('reserva').query(
      `select valida_bloque_reserva` + '( ' + " '" + FechaInicio + "'" + ', ' + " '" + FechaFin + "'" + ', ' + idMunicipio + ')'
    );

    return validaBolueRes[0].valida_bloque_reserva;
  }

  async crear(req, data): Promise<any> {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ReservadeHorasBloqueosActivosCrearNuevoBloqueo);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }    

    let idUsuarioCreacion = data.idUsuarioCreacion;
    let FechaInicio = data.FechaInicio;
    let FechaFin = data.FechaFin;
    let idMunicipio = data.idMunicipio;
    let motivo = data.MotivoBloqueo;

    console.log(' ================================= data que llega ==========================================');
    console.log(data);

    let consulta = await getConnection('reserva').query(
      `select insert_update_bloqueos_activos` +
        '(' +
        idUsuarioCreacion +
        ' , ' +
        " '" +
        FechaInicio +
        "'" +
        ',' +
        "'" +
        FechaFin +
        "'" +
        ',' +
        idMunicipio +
        ',' +
        "'" +
        motivo +
        "'" +
        ')'
    );
    let respuesta = consulta[0].insert_update_bloqueos_activos;
    console.log(
      '¿ ======================================================================================================================='
    );
    console.log(respuesta);
    return { respuesta };
  }

  async crear_antiguo(dto: PostBloqueoActivosDTO) {
    const FechaCreacion = new Date();
    const Activo = true;
    const idUsuarioCreacion = (await this.authService.usuario()).idUsuario;
    const idMunicipio = this.authService.oficina().idOficina;
    const data = { ...dto, FechaCreacion, Activo, idUsuarioCreacion, idMunicipio };

    const fechaDesde = new Date();
    const fechaHasta = new Date();
    const datas = await this.bloqueoActivosEntityRepository
      .createQueryBuilder('bloqueo')
      .andWhere('bloqueo.idMunicipio =  :idMunicipio', { idMunicipio })
      //.andWhere(  "bloqueo.FechaFin     >= :fechaHasta",  { fechaHasta })
      .orderBy('bloqueo.idBloqueoActivo', 'DESC')
      .getMany();

    const fechaD = new Date();
    const fechaH = new Date();
    const dataActivo = await this.bloqueoActivosEntityRepository
      .createQueryBuilder('bloqueo')
      .andWhere('bloqueo.idMunicipio =  :idMunicipio', { idMunicipio })
      .andWhere('bloqueo.FechaFin     > :fechaH', { fechaH })
      .andWhere('bloqueo.Activo   = :Activo', { Activo })
      .orderBy('bloqueo.idBloqueoActivo', 'DESC')
      .getMany();

    const fechaTermino = String(datas[0].FechaFin);
    const hoy = new Date();
    const fecha_actual = String(hoy.getFullYear() + '-' + hoy.getMonth() + '-' + hoy.getDate());

    if (datas[0].Activo == true) {
      // }else if(fecha_actual < fechaTermino){
      return 'isBlocked';
      //    return await this.bloqueoActivosEntityRepository.save( data );
    } else if (datas[0].Activo == false) {
      return await this.bloqueoActivosEntityRepository.save(data);
    }
  }
  async reversar(req, idBloqueoActivo: number, Activo: boolean) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ReservadeHorasBloqueosActivosSeleccionarAccionesRevertir);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }

    var f = new Date();
    let fecha = f.getFullYear() + '-' + (f.getMonth() + 1) + '-' + f.getDate();
    const FechaReversion = fecha;
    const idUsuarioReversion = (await this.authService.usuario()).idUsuario;
    const data = { idUsuarioReversion, FechaReversion, Activo };

    // return await this.bloqueoActivosEntityRepository.update(idBloqueoActivo, data);

    let ejecutaReversar = await getConnection('reserva').query(
      `select reversar` +
        '( ' +
        idBloqueoActivo +
        ', ' +
        idUsuarioReversion +
        ', ' +
        " '" +
        FechaReversion +
        "'" +
        ', ' +
        " '" +
        Activo +
        "'" +
        ')'
    );
  }

  private async getUsuario(idUsuario: number) {
    if (idUsuario) {
      const usuario = await this.userRepository.findOne({ idUsuario });

      if (usuario) {
        const nombres = usuario.Nombres || '';
        const materno = usuario.ApellidoMaterno || '';
        const paterno = usuario.ApellidoPaterno || '';

        return nombres + ' ' + materno + ' ' + paterno;
      } else return 'Usuario borrado';
    }
    return '';
  }
  // private async apiCall( urlMetodo: string  )
  // {
  //     const headers = { 'Accept': 'application/json' };
  //     const url     = process.env.API_URL + urlMetodo;

  //     if( this.auth )
  //         headers['Bearer'] = this.auth;

  //     return this.httpService.get( url, { headers } ).pipe( map( response => response.data ) ).toPromise()
  //         .then(  response  => response.data )
  //         .catch( error     => error );
  // }
}
