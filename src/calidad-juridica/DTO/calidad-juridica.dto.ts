export class CalidadJuridicaDTO {

    idCalidadJuridica: number;
    nombre: string;
    descripcion: string;
}