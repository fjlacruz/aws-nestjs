import { ApiPropertyOptional } from "@nestjs/swagger";

export class createCalidadJuridica {

    @ApiPropertyOptional()
    idCalidadJuridica: number;
    @ApiPropertyOptional()
    nombre: string;
    @ApiPropertyOptional()
    descripcion: string;
}