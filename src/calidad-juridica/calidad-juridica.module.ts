import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CalidadJuridicaController } from './controller/calidad-juridica.controller';
import { CalidadJuridicaEntity } from './entity/calidad-juridica.entity';
import { CalidadJuridicaService } from './services/calidad-juridica.service';

@Module({
  imports: [TypeOrmModule.forFeature([CalidadJuridicaEntity])],
  providers: [CalidadJuridicaService],
  exports: [CalidadJuridicaService],
  controllers: [CalidadJuridicaController]
})
export class CalidadJuridicaModule {}
