import { Test, TestingModule } from '@nestjs/testing';
import { CalidadJuridicaController } from './calidad-juridica.controller';

describe('CalidadJuridicaController', () => {
  let controller: CalidadJuridicaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CalidadJuridicaController],
    }).compile();

    controller = module.get<CalidadJuridicaController>(CalidadJuridicaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
