import { Controller, Get } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CalidadJuridicaService } from '../services/calidad-juridica.service';

@ApiTags('Calidad-Juridica')
@Controller('Calidad-Juridica')
export class CalidadJuridicaController
{
    constructor( private calidadJuridicaService: CalidadJuridicaService )
    {}

    @Get('/CalidadJuridica')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos las opciones de calidad juridica' })
    async getAll()
    {
        return this.calidadJuridicaService.getAll().then(data =>
        {
            return { code: 200, data };
        });
    }

}
