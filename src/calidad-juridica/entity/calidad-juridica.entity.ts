import { ApiPropertyOptional } from "@nestjs/swagger";
import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity('CalidadJuridica')
export class CalidadJuridicaEntity {

    @PrimaryColumn()
    idCalidadJuridica: number;

    @Column()
    nombre: string;
    
    @Column()
    descripcion: string;

}