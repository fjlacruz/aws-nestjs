import { Test, TestingModule } from '@nestjs/testing';
import { CalidadJuridicaService } from './calidad-juridica.service';

describe('CalidadJuridicaService', () => {
  let service: CalidadJuridicaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CalidadJuridicaService],
    }).compile();

    service = module.get<CalidadJuridicaService>(CalidadJuridicaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
