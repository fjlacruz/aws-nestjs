import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { CalidadJuridicaEntity } from '../entity/calidad-juridica.entity';
import { CalidadJuridicaDTO } from '../DTO/calidad-juridica.dto';

@Injectable()
export class CalidadJuridicaService {
    constructor(@InjectRepository(CalidadJuridicaEntity) private readonly CalidadJuridicaRespository: Repository<CalidadJuridicaEntity>) { }

    async getAll(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoCalidadJuridica: CalidadJuridicaEntity[] = [];

        try {
            resultadoCalidadJuridica = await this.CalidadJuridicaRespository.find();

            if (Array.isArray(resultadoCalidadJuridica) && resultadoCalidadJuridica.length) {
                resultado.Respuesta = resultadoCalidadJuridica;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Opciones obtenidas correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron opciones';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo opciones de Calidad Jurídica';
        }

        return resultado;
    }
}
