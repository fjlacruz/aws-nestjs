import { Module } from '@nestjs/common';
import { CambioestadorceiController } from './controller/cambioestadorcei/cambioestadorcei.controller';
import { CambioestadorceiService } from './services/cambioestadorcei/cambioestadorcei.service';
import { AuthService } from 'src/auth/auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';

@Module({
  imports: [TypeOrmModule.forFeature([Postulantes, ComunasEntity, User, RolesUsuarios, ConductoresEntity])],
  providers: [CambioestadorceiService, AuthService, IngresoPostulanteService],
  exports: [CambioestadorceiService],
  controllers: [CambioestadorceiController],
})
export class CambioEstadorceiModule {}
