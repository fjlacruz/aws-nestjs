import { Test, TestingModule } from '@nestjs/testing';
import { CambioestadorceiController } from './cambioestadorcei.controller';

describe('CambioestadorceiController', () => {
  let controller: CambioestadorceiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CambioestadorceiController],
    }).compile();

    controller = module.get<CambioestadorceiController>(CambioestadorceiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
