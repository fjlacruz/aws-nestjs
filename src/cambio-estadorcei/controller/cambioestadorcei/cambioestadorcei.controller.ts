import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CambioestadorceiService } from 'src/cambio-estadorcei/services/cambioestadorcei/cambioestadorcei.service';

@ApiTags('Cambio-Estados-RCeI')
@Controller('cambioestadorcei')
export class CambioestadorceiController {
  constructor(private readonly cambioestadorceiService: CambioestadorceiService) {}
  @UseGuards(JwtAuthGuard)
  @Post('cambioEstadoInformarOtorgamiento')
  @ApiBearerAuth()
  @ApiOperation({
    summary:
      'Servicio para realizar el cambio de estado en el registro civil por run (run+dv) y clase de licencia para ingreso de postulantes',
  })
  async cambioEstadoInformarOtorgamiento(@Body() data: any) {
    return this.cambioestadorceiService.cambioEstadoInformarOtorgamiento(data);
  }

  @Post('consultaSuspension')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar suspensiones de un postulante',
  })
  async consultaSuspension(@Body() data: any) {
    return this.cambioestadorceiService.consultaSuspension(data);
  }
  @Post('validaEstadosLic')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para validar que las licencias esten en (PCL/EDF/EDD): 102/201/301 o 111/202/302',
  })
  async validaEstadosLic(@Body() data: any) {
    return this.cambioestadorceiService.validaEstadosLic(data);
  }

  @Post('actualizaEstadoSGL1111')
  @ApiBearerAuth()
  @ApiOperation({
    summary:
      'Servicio cambiar el estado de la solicitud de A la espera de informar otorgamiento" (1103) a "En espera de respuesta de Registro Civil" (1111).',
  })
  async actualizaEstadoSGL1111(@Body() data: any) {
    return this.cambioestadorceiService.actualizaEstadoSGL1111(data);
  }

  @Post('consultaRestriccionesEmitirLic')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar si el postulante tiene restricciones en el SGL e informar al RC',
  })
  async consultaRestriccionesEmitirLic(@Body() data: any) {
    return this.cambioestadorceiService.consultaRestriccionesEmitirLic(data);
  }

  @Post('getDataRestricciones')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar si el postulante tiene restricciones en el SGL',
  })
  async getDataRestricciones(@Body() data: any) {
    return this.cambioestadorceiService.getDataRestricciones(data);
  }

  @Post('actualizar_estado_sgl_a_1104')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio cambiar el estado del tramite de A " Otorgamiento informado y en impresión" (1104).',
  })
  async actualizar_estado_sgl_a_1104(@Body() data: any) {
    return this.cambioestadorceiService.actualizar_estado_sgl_a_1104(data);
  }

  @Post('consultaPagoEmitirLic')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar si el postulante tiene pagos pedientes',
  })
  async consultaPago(@Body() data: any) {
    return this.cambioestadorceiService.consultaPago(data);
  }
  @Post('consultaRPI')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar si el postulante tiene registros en RPI pedientes',
  })
  async consultaRPI(@Body() data: any) {
    return this.cambioestadorceiService.consultaRPI(data);
  }

  @Post('actualizar_estado_sgl_a_1303')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio cambiar el estado del tramite  "recepcion no conforma" (1303).',
  })
  async actualizar_estado_sgl_a_1303(@Body() data: any) {
    return this.cambioestadorceiService.actualizar_estado_sgl_a_1303(data);
  }

  @Post('cambio_estadoRNC_EDF203')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio cambiar el estado del tramite  en RNC a EDF 203',
  })
  async cambio_estadoRNC_EDF203(@Body() data: any) {
    return this.cambioestadorceiService.cambio_estadoRNC_EDF203(data);
  }

  @Post('validaReglasNegocioDenegacion')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para validar las reglas de negocio de denegaciones',
  })
  async validaReglasNegocioDenegacion(@Body() data: any) {
    return this.cambioestadorceiService.validaReglasNegocioDenegacion(data);
  }

  @Post('consultaSentenciasPostulante')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar las sentencias de un postulante',
  })
  async consulSen(@Body() data: any) {
    return this.cambioestadorceiService.consulSen(data);
  }

  @Post('consultaPlazosSentencias')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar los plazos de las sentencias del postulante',
  })
  async consultaPlazosSentencias(@Body() data: any) {
    return this.cambioestadorceiService.consultaPlazosSentencias(data);
  }

  @Post('informarDenegacion')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para informar denegaciones del postulante',
  })
  async informarDenegacion(@Body() data: any) {
    return this.cambioestadorceiService.informarDenegacion(data);
  }

  @Post('cambioEstadoDenegacion')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para informar cambio de estados por denegacion',
  })
  async cambioEstadoDenegacion(@Body() data: any) {
    return this.cambioestadorceiService.cambioEstadoDenegacion(data);
  }

  @Post('cambioEstadoTramiteDenegacion')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para cambiar el estado del tramite en el SGL por denegacion',
  })
  async cambioEstadoTramiteDenegacion(@Body() data: any) {
    return this.cambioestadorceiService.cambioEstadoTramiteDenegacion(data);
  }
  @Post('consultaEstadoSolicitud')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar el estado de las solicitudes del postulante',
  })
  async consultaEstadoSolicitud(@Body() data: any) {
    return this.cambioestadorceiService.consultaEstadoSolicitud(data);
  }

  @Post('consulLicIngresoPostulante')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar en la LIC que el estado PCL sea distinto a 102',
  })
  async consulLicIngresoPostulante(@Body() data: any) {
    return this.cambioestadorceiService.consulLicIngresoPostulante(data);
  }

  @Post('consultaSentenciasIngresoPostulante')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar las sentencias para el ingreso del postulante',
  })
  async consultaSentenciasIngresoPostulante(@Body() data: any) {
    return this.cambioestadorceiService.consulSenIngresoPostulante(data);
  }
  @Post('consultaDenegacionesIngresoPostulante')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar las sentencias para el ingreso del postulante',
  })
  async consultaDenegacionesIngresoPostulante(@Body() data: any) {
    return this.cambioestadorceiService.consultaDenegacionesIngPost(data);
  }

  @Post('verificaLicIngresoPostulante')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para verificar que la licencia que se va a registrar no exista en el RC',
  })
  async verificaLicIngresoPostulante(@Body() data: any) {
    return this.cambioestadorceiService.verificaLicIngresoPostulante(data);
  }
  @Post('actualizaEstadoSGL1101')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio cambiar el estado de los tramites de una solicitud a 1101 (trámite iniciado)',
  })
  async actualizaEstadoSGL1101(@Body() data: any) {
    return this.cambioestadorceiService.actualizaEstadoSGL1101(data);
  }

  @Post('actualizaSolicitudSGL1205')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio cambiar el estado de una solicitud a 1205 (Abierta)',
  })
  async actualizaSolicitudSGL1205(@Body() data: any) {
    return this.cambioestadorceiService.actualizaSolicitudSGL1205(data);
  }
  @Post('recepcionConforme')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio cambiar el estado de una solicitud a 1205 (Abierta)',
  })
  async recepcionConforme(@Body() data: any) {
    return this.cambioestadorceiService.recepcionConforme(data);
  }
}
