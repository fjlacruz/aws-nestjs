import { Test, TestingModule } from '@nestjs/testing';
import { CambioestadorceiService } from './cambioestadorcei.service';

describe('CambioestadorceiService', () => {
  let service: CambioestadorceiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CambioestadorceiService],
    }).compile();

    service = module.get<CambioestadorceiService>(CambioestadorceiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
