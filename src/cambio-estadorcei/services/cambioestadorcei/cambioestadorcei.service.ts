import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository, UpdateResult } from 'typeorm';
import axios from 'axios';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';

const schedule = require('node-schedule');
@Injectable()
export class CambioestadorceiService {
  salidaUpdate;
  salidaInsert;
  salidaHis;
  salidaLID;
  salidaDOM;
  validaInsertDom;
  validaUpdateDom;
  flagLID;
  flagInsertLic;
  flagUpadteLic;
  flagSalidaProceso;
  flagInserDOM;
  respSuspension;
  validaEstadoLic;
  flagInserLID;
  salidaValidacionrestricciones;
  salidaInsertrestricciones;
  salidaValRest;
  comunaTramite;
  newSec;

  constructor(private readonly ingresoPostulanteService: IngresoPostulanteService) {}

  async cambio_estadoRNC_EDF203(data): Promise<any> {
    try {
      let conexion = await this.ingresoPostulanteService.validaStatus();
      if (conexion === 'OK') {
        let Licencias = data[1].Licencias;
        let idUsuario = data[0].idUsuario;
        let dataUpdate = [];
        dataUpdate = [...dataUpdate, { idUsuario }, { Licencias }];

        let updateLics = await this.ingresoPostulanteService.updateLic(dataUpdate);

        if (updateLics.respuesta.codigo === '0') {
          return { codigo: 200, mensaje: 'Datos Actualizados' };
        } else {
          return { codigo: 400, mensaje: 'Error en actualizacion de datos' };
        }
      } else {
        return {
          host: 'privqa.srcei.cl',
          estatus: 'NOK',
          mensaje: 'Sin conexion con Registro Civil',
        };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async cambioEstadoInformarOtorgamiento(data): Promise<any> {
    try {
      //return data[0];
      // ******************************************************************************************************** //
      // ******************************** 1.- evaluar conexion con registro civil ******************************* //
      // ******************************************************************************************************** //
      let conexion = await this.ingresoPostulanteService.validaStatus();
      if (conexion === 'OK') {
        let dataConsulta = data[0];
        let dataLic = data[1].Licencias;
        let tipoLic = data[0].tipoLic;
        let tipoTramite = data[0].tipoTramite;
        let run = data[0].run;
        let RUN = run.substring(0, run.length - 1);
        let _RUN = parseInt(RUN);
        let idUsuario = { idUsuario: dataConsulta.idUsuario };
        let proceso = data[2].proceso;

        console.log('========================================================================================== ');
        console.log('==================================== tipoTramite que se envia    ==============================');
        console.log('========================================================================================== ');
        console.log(tipoTramite);

        //***se obtiene comuna para el domicilio  ***********//
        if (tipoTramite != 4) {
          let datosDom = await this.ingresoPostulanteService.getPostulanteByRUN(_RUN);
          this.comunaTramite = datosDom[0].idComuna[0].codRNC.toString();
        }
        // **** inserta en el historico ********//

        //let insertHis = await this.ingresoPostulanteService.insertatHistoricoCambiEstado(dataLic, dataConsulta);

        // ********************************************************************************************************************** //
        // *********************************************** 2.- Tramites a procesar ********************************************** //
        // ********************************************************************************************************************** //
        /*
        tipo1:
        1er otorgamiento profesional:1; 1er otorgamiento no profesional:2, Control:3, Canje:7, Cambio de Restricciones médicas:12
        tipo2:
        Duplicado:4
        tipo3:
        Cambio de domicilio:5
        tipo4:
        Diplomatico:9
        Procesocomunes=>tipo1-tipo3-tipo4
         */

        //if (tipoTramite === 1 || tipoTramite === 2 || tipoTramite === 3 || tipoTramite === 7 || tipoTramite === 12) {
        // ********************************************************************************************************************** //
        // **************** 3.- consulta las clases de licencia que posee (Lic interSGL)***************************************** //
        // ********************************************************************************************************************** //
        let consulLic;
        if (tipoTramite == 5) {
          consulLic = dataLic;
          if (consulLic == undefined) {
            consulLic = [];
          }
        } else {
          consulLic = await this.ingresoPostulanteService.consulLic(dataConsulta);
          if (consulLic == undefined) {
            consulLic = [];
          }
        }

        let lic = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));
        let longitud = data[0].tipoLic.length;

        for (let i = 0; i < longitud; i++) {
          let licTipo = data[0].tipoLic[i];

          // ************************************************************************************************************************** //
          // ********* 4.- filtra las claces de licencias obtenidas en (3) por los licTipo a procesar)********************************* //
          // ************************************************************************************************************************** //
          let claseLics = lic.filter(claseLic => claseLic.licTipo == licTipo.toUpperCase());
          let respuesta = { tipo: licTipo, claseLic: claseLics };

          // ************************************************************************************************************************** //
          // **************************** 5.- en caso de poseer liciencia actualiza la lic en el RC *********************************** //
          // ************************************************************************************************************************** //

          if (respuesta.claseLic != '') {
            console.log('posee lic ' + claseLics[0].licTipo + ' , se ejecuta updateLic');
            this.salidaInsert = {};
            this.salidaLID = {};
            this.salidaDOM = {};
            let Licencias = { Licencias: dataLic };
            let dataLicUpdate = [];
            dataLicUpdate.push(idUsuario);
            dataLicUpdate.push(Licencias);

            let actualizaLic = await this.ingresoPostulanteService.updateLic(dataLicUpdate);
            console.log('********************** update Lic ************************************************* ');
            console.log(actualizaLic);
            if (actualizaLic) {
              if (actualizaLic.respuesta.codigo === '0') {
                this.validaUpdateDom = 1;
                this.validaInsertDom = 0;
                this.flagInsertLic = 0;
                this.flagUpadteLic = 1;
                let datos_enviados_update = actualizaLic.datos_enviados.Licencias;

                this.salidaUpdate = {
                  codigoUpdate_LIC: 200,
                  statusProcesoUpdate_LIC: 'OK',
                  datosEnviadosUpdate_LIC: datos_enviados_update,
                  respuestaRCeI_LIC: actualizaLic.respuesta,
                };
              } else {
                this.salidaUpdate = { codigoUpdate_LIC: 403, statusProcesoUpdate_lic: 'NOK', respuestaRCeI_LIC: actualizaLic.respuesta };
                this.flagUpadteLic = 0;
                this.flagInsertLic = 0;
              }
            }
          }
          // ************************************************************************************************************************** //
          //***************************** 6.- en caso de NO poseer liciencia inserta en la lic en el RC ******************************* //
          // ************************************************************************************************************************** //
          if (respuesta.claseLic == '') {
            console.log('NO Posee lic ' + respuesta.tipo + ' , se ejecuta insertLic');
            let dat = JSON.parse(JSON.stringify(dataLic).replace(/"\s+|\s+"/g, '"'));
            this.salidaUpdate = {};
            this.salidaLID = {};
            this.salidaDOM = {};

            let licPoseeIns = dat.filter(claseLic => claseLic.licTipo == respuesta.tipo.toUpperCase());
            let licPoseeInsert = JSON.parse(JSON.stringify(licPoseeIns).trim());

            let Licencias = { Licencias: licPoseeInsert };
            let dataLicInsert = [];
            dataLicInsert.push(idUsuario);
            dataLicInsert.push(Licencias);

            let insertaLic = await this.ingresoPostulanteService.insertLic(dataLicInsert);
            console.log('********************** insert Lic ************************************************* ');
            console.log(insertaLic);
            console.log('*********************************************************************************** ');
            if (insertaLic) {
              if (insertaLic.respuesta.codigo === '0') {
                this.validaUpdateDom = 0;
                this.validaInsertDom = 1;
                this.flagInsertLic = 1;
                this.flagUpadteLic = 0;

                let datos_enviados_insert = insertaLic.datos_enviados.Licencias;
                this.salidaInsert = {
                  codigoInsert_LIC: 200,
                  statusProcesoInsert_LIC: 'OK',
                  datosEnviadosInsert_LIC: datos_enviados_insert,
                  respuestaRCeI_LIC: insertaLic.respuesta,
                };
              } else {
                this.salidaInsert = { codigoInsert_LIC: 403, statusProcesoInsert_LIC: 'NOK', respuestaRCeI_LIC: insertaLic.respuesta };
                this.flagInsertLic = 0;
                this.flagUpadteLic = 0;
              }
            }
          }

          //***************************** Validacion para ejecutar insertLid ******************************* //
          if (tipoTramite === 4) {
            this.flagInserDOM = 0;
            console.log('ejecuta insertLid');
            let consultaLid = await this.ingresoPostulanteService.consulLid(dataConsulta);
            let lid = JSON.parse(JSON.stringify(consultaLid).replace(/"\s+|\s+"/g, '"'));
            let lidPosse = lid.filter(claseLid => claseLid.lidTipoLic == licTipo.toUpperCase());

            var f = new Date();
            let fecha = f.getFullYear() + '' + (f.getMonth() + 1) + '' + f.getDate();

            if (lidPosse == '') {
              console.log('NO posee duplicados: ' + licTipo);

              let d;
              for (d of dataLic) {
                let duplicados = [
                  {
                    lidRun: d.licRun,
                    lidSec: '1',
                    lidFecha: fecha,
                    lidVigencia: 'S',
                    lidEstado: ' ',
                    lidTipoLic: d.licTipo,
                    lidFolio: d.licFolio,
                    lidFechaIng: fecha,
                    lidObservacion: d.licObservaciones,
                    lidFecRecepcion: fecha,
                    lidCodOrigen: '3',
                    lidComuna: this.comunaTramite,
                    lidRunEscuela: '0',
                    lidLetraFolio: d.licLetraFolio,
                  },
                ];

                let Duplicados = { Duplicados: duplicados };
                let dataLidInsert = [];
                dataLidInsert.push(idUsuario);
                dataLidInsert.push(Duplicados);

                await this.insertarLID(dataLidInsert);
              }
            } else {
              let D;
              for (D of lidPosse) {
                let nevaSec = parseInt(D.lidSec) + 1;
                let duplicados = [
                  {
                    lidRun: D.lidRun,
                    lidSec: nevaSec,
                    lidFecha: fecha,
                    lidVigencia: 'S',
                    lidEstado: ' ',
                    lidTipoLic: D.lidTipoLic,
                    lidFolio: D.lidFolio,
                    lidFechaIng: fecha,
                    lidObservacion: D.lidObservacion,
                    lidFecRecepcion: fecha,
                    lidCodOrigen: '3',
                    lidComuna: this.comunaTramite,
                    lidRunEscuela: '0',
                    lidLetraFolio: D.lidLetraFolio,
                  },
                ];

                let Duplicados = { Duplicados: duplicados };
                let dataLidInsert = [];
                dataLidInsert.push(idUsuario);
                dataLidInsert.push(Duplicados);

                await this.insertarLID(dataLidInsert);
              }
            }
          }
        }

        if (
          tipoTramite === 1 ||
          tipoTramite === 2 ||
          tipoTramite === 3 ||
          tipoTramite === 7 ||
          tipoTramite === 12 ||
          tipoTramite === 5 ||
          tipoTramite === 9
        ) {
          // ***** consulta datos de domicilio del postulate ingresados previamente al sgl ******* //
          let datosDom = await this.ingresoPostulanteService.getPostulanteByRUN(_RUN);

          let DOM = [
            {
              domRun: datosDom[0].RUN.toString(),
              domComuna: this.comunaTramite,
              domCalle: datosDom[0].Calle,
              domNumero: datosDom[0].CalleNro,
              domLetra: datosDom[0].Letra,
              domResto: datosDom[0].RestoDireccion,
              domFechaIngresoNac: '0',
              domFechaIngresoMat: '0',
              domFechaIngresoDef: '0',
              domFechaIngresoFil: '0',
              domCodigoPostal: '',
            },
          ];

          let dataDom = [];
          let Domicilio = { Domicilio: DOM };
          dataDom.push(idUsuario);
          dataDom.push(Domicilio);

          console.log('===================== dataDom =====================');
          console.log(this.comunaTramite);
          // //***************************** Validacion para ejecutar insertDomicilio ******************************* //
          console.log('ejecuta domicilio' + this.validaUpdateDom + ' ' + this.validaInsertDom);
          let validaEjecutaDOM = this.validaUpdateDom + this.validaInsertDom;
          if (validaEjecutaDOM == 1 || validaEjecutaDOM == 2) {
            console.log('ejecuta domicilio interno');
            await this.insertarDomicilio(dataDom);
          }
        }

        let statusSalida = this.flagInsertLic + this.flagUpadteLic;
        console.log('salida statusSalida: ' + statusSalida);
        console.log('insert: ' + this.flagInsertLic);
        console.log('update: ' + this.flagUpadteLic);
        console.log('inserLID: ' + this.flagInserLID);

        if (tipoTramite === 4) {
          if ((statusSalida == 1 && this.flagUpadteLic == 1) || (statusSalida == 2 && this.flagUpadteLic == 1)) {
            this.flagSalidaProceso = 'OK';
          } else {
            this.flagSalidaProceso = 'NOK';
          }
        } else {
          if (statusSalida == 1 || statusSalida == 2) {
            this.flagSalidaProceso = 'OK';
          } else {
            this.flagSalidaProceso = 'NOK';
          }
        }

        //***************************** Valida ejecucion general del proceso  ******************************* //
        let salidRespuestas = Object.assign(this.salidaInsert, this.salidaUpdate, this.salidaLID, this.salidaDOM);
        //***************************** respuesta del la ejecucion del proceso ******************************* //
        let H = await this.ingresoPostulanteService.insertatHistoricoCambiEstado(dataLic, dataConsulta, proceso);
        return { respuestaProceso: this.flagSalidaProceso, tipoTramite: tipoTramite, salidRespuestas };
      } else {
        return {
          host: 'privqa.srcei.cl',
          estatus: 'NOK',
          mensaje: 'Sin conexion con Registro Civil',
        };
      }
    } catch (err) {
      console.log(err);
      return 'Error en peticion al RCeI';
    }
  }

  async insertarDomicilio(dataDom) {
    try {
      let conexion = await this.ingresoPostulanteService.validaStatus();
      if (conexion === 'OK') {
        let url = this.ingresoPostulanteService.BASE_URL_REGISTRO_CIVIL;
        const domicilio = await axios.post(url + '/api/insertardomicilio', dataDom);

        if (domicilio.data.respuesta.codigo === '0') {
          this.flagInserDOM = 1;
          console.log('salida dom: ' + this.flagInserDOM);
          this.salidaDOM = {
            codigoInsert_DOM: 200,
            statusProcesoInsert_DOM: 'OK',
            datosEnviadosInsert_DOM: domicilio.data.datos_enviados,
            respuestaRCeI_DOM: domicilio.data.respuesta,
          };
        } else {
          this.flagInserDOM = 0;
          this.salidaDOM = {
            codigoInsert_DOM: 403,
            statusProcesoInsert_DOM: 'NOK',
            datosEnviadosInsert_DOM: domicilio.data.datos_enviados,
            respuestaRCeI_DOM: domicilio.data.respuesta,
          };
        }
      } else {
        return {
          host: 'privqa.srcei.cl',
          estatus: 'NOK',
          mensaje: 'Sin conexion con Registro Civil',
        };
      }
    } catch (e) {
      //console.log(e);
      this.ingresoPostulanteService.errorExcesoPeticiones(e);
      return this.insertarDomicilio(dataDom);
    }
  }

  async insertarLID(dataLidInsert) {
    try {
      let conexion = await this.ingresoPostulanteService.validaStatus();
      if (conexion === 'OK') {
        let url = this.ingresoPostulanteService.BASE_URL_REGISTRO_CIVIL;
        const lid = await axios.post(url + '/api/duplicados', dataLidInsert);
        console.log(dataLidInsert);
        console.log('********************** insert LID ****************************************** ');
        console.log(lid.data);
        console.log('*********************************************************************************** ');
        if (lid.data.respuesta.codigo === '0') {
          this.flagInserLID = 1;
          let datos_enviados_insert = lid.data.datos_enviados.Duplicados;
          this.salidaLID = {
            codigoInsert_LID: 200,
            statusProcesoInsert_LID: 'OK',
            datosEnviadosInsert_LID: datos_enviados_insert,
            respuestaRCeI_LID: lid.data.respuesta,
          };

          console.log('salida LID: ' + this.flagInserLID);
        } else {
          this.flagInserLID = 0;
          this.salidaLID = {
            codigoInsert_LID: 403,
            statusProcesoInsert_LID: 'NOK',
            respuestaRCeI_LID: lid.data.respuesta,
          };
        }
      } else {
        return {
          host: 'privqa.srcei.cl',
          estatus: 'NOK',
          mensaje: 'Sin conexion con Registro Civil',
        };
      }
    } catch (e) {
      //console.log(e);
      this.ingresoPostulanteService.errorExcesoPeticiones(e);
      return this.insertarLID(dataLidInsert);
    }
  }

  async consultaSuspension(data) {
    try {
      let suspension = await this.ingresoPostulanteService.consulSen(data);
      let suspencionAux = await this.ingresoPostulanteService.consulSenAux(data);

      let tieneSuspension = suspension.filter(R => R.senCodRes001 == '4' || R.senCodRes002 == '4' || R.senCodRes003 == '4');
      let tieneSuspensionAux = suspencionAux.filter(RA => RA.senauxCodRes == '4');

      if (tieneSuspension.length > 0 || tieneSuspensionAux.length > 0) {
        this.respSuspension = { estadoSuspension: 1, descripcion: 'el Postulante tiene suspensiones', datos: suspension };
      } else {
        this.respSuspension = { estadoSuspension: 0, descripcion: 'el Postulante NO tiene suspensiones', datos: suspension };
      }

      return this.respSuspension;
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  async validaEstadosLic(data) {
    //====Pueden estar en (PCL/EDF/EDD): 102/201/301 o 111/202/302 =====//
    try {
      let dataConsulta = { idUsuario: data[0].idUsuario, run: data[0].run };

      let consulLic = await this.ingresoPostulanteService.consulLic(dataConsulta);
      let lic = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));
      //let longitud = data[0].tipoLic.length;

      //console.log(dataConsulta);
      console.log(lic);

      let dataLic = data[1].Licencias;
      let tipoTramite = data[0].tipoTramite;

      console.log(' =============== data para validar ======== ');
      //console.log(dataLic);
      //===== se validan los estados solo para las clase de licencias a informar ======//
      let deupraLic = lic.filter(v => v.licTipo != 'A');

      console.log(' =============== deupraLic ======== ');
      console.log(deupraLic);

      if (tipoTramite != 4) {
        let verificaEstados = deupraLic.filter(v => v.licEstadoClaLic != '102' || v.licEstadoFisLic != '201' || v.licEstadoDigLic != '301');
        if (verificaEstados.length > 0) {
          this.validaEstadoLic = {
            validacion: 'NOK',
            descripcion: 'No cumple con los estados de la licencia para el proceso. (PCL/EDF/EDD): 102/201/301',
            datos: verificaEstados,
          };
        } else {
          this.validaEstadoLic = { validacion: 'OK' };
        }
      } else {
        let verificaEstadosD = deupraLic.filter(
          v => v.licEstadoClaLic != '111' || v.licEstadoFisLic != '202' || v.licEstadoDigLic != '302'
        );
        if (verificaEstadosD.length > 0) {
          this.validaEstadoLic = {
            validacion: 'NOK',
            descripcion: 'No cumple con los estados de la licencia para el proceso. (PCL/EDF/EDD): 111/202/302',
            datos: verificaEstadosD,
          };
        } else {
          this.validaEstadoLic = { validacion: 'OK' };
        }
      }

      return this.validaEstadoLic;
    } catch (e) {
      console.log(e);
    }
  }

  async getDataRestricciones(data: any): Promise<any> {
    try {
      let idUsuario = data.idUsuario;
      let solicitud = data.idSol;
      let tramite = data.idTra;

      console.log(idUsuario + ' ' + solicitud + ' ' + tramite);

      const Datosrestricciones = await getConnection().query(`select get_datos_restricciones(${solicitud})as resp`);
      let run = Datosrestricciones[0].resp.RUNDV;
      let dataRest = {
        idUsuario: idUsuario,
        RUN: run,
      };

      await this.consultaRestriccionesEmitirLic(dataRest);
    } catch (e) {
      console.log(e);
    }
  }

  async consultaRestriccionesEmitirLic(data) {
    try {
      let _run = data.RUN.toString();
      let _RUN = _run.substring(0, _run.length - 1);
      let _idUsuario = data.idUsuario;
      let _tipoTramite = data.tipoTramite;
      let salidaValRest;
      const restricciones = await getConnection().query(`select validarestricciones_emitir_licencia(${_RUN})as resp`);
      console.log(' ========================== restricciones ================================= ');
      let restriccionesPost = restricciones[0].resp.respuestaMultiple;

      let salidaRestricciones = restricciones[0].resp;

      await this.insertaRestricciones(salidaRestricciones, _idUsuario);

      // if (respuesta === null) {
      //   return { restricciones: 0, tipoTramite: _tipoTramite };
      // } else {
      //   let salida = respuesta.map(item => {
      //     return { run: item.f1, comuna: item.f2, codrc: item.f4, descripcionRestriccion: item.f5 };
      //   });

      //   let salidaRestricciones = salida;

      //   if (_tipoTramite == 1 || _tipoTramite == 2 || _tipoTramite == 3 || _tipoTramite == 7 || _tipoTramite == 12) {
      //     await this.insertaRestricciones(salidaRestricciones, _idUsuario);
      //     this.salidaValidacionrestricciones = { restricciones: salidaRestricciones };
      //     salidaValRest = Object.assign(this.salidaValidacionrestricciones, this.salidaInsertrestricciones);
      //   }
      //   if (_tipoTramite == 4 || _tipoTramite == 5) {
      //     this.salidaValidacionrestricciones = { restricciones: salidaRestricciones };
      //     let mensaje = { reimpresion: 'SI', mensaje: 'Reimprimir la restriccion en nuevo documento' };
      //     salidaValRest = Object.assign(this.salidaValidacionrestricciones, mensaje);
      //   }
      //   if (_tipoTramite == 9) {
      //     salidaValRest = { mensaje: 'Esta validación no aplica para el tipo trámite 9 (Diplomático)' };
      //   }

      //   return salidaValRest;
      // }
    } catch (e) {
      console.log(e);
    }
  }
  async insertaRestricciones(salidaRestricciones: any, _idUsuario: number) {
    try {
      let Mes;
      let Dia;

      var f = new Date();

      let anio = f.getFullYear();
      let mes = f.getMonth() + 1;
      let dia = f.getDate();

      if ((f.getMonth() + 1).toString().length == 1) {
        Mes = '0' + (f.getMonth() + 1).toString();
      } else {
        Mes = f.getMonth() + 1;
      }
      if (f.getDate().toString().length == 1) {
        Dia = '0' + f.getDate().toString();
      } else {
        Dia = f.getDate();
      }

      let FECHARESTRICCION = Dia + '' + Mes + '' + f.getFullYear();
      let fecha = f.getFullYear() + '' + Mes + '' + Dia;

      let anotaciones;
      let sbfDatos;
      let idUsuario = { idUsuario: _idUsuario };
      let maxSec;
      let sbfSecuenciaNueva;

      let restricciones = salidaRestricciones;

      let _run_ = salidaRestricciones.RUNDV;
      let dataConsultaRes = {
        idUsuario: idUsuario.idUsuario,
        run: _run_,
      };

      let restriccionesPosee = await this.ingresoPostulanteService.consultaRestricciones(dataConsultaRes);

      if (restriccionesPosee.respuesta == '') {
        this.newSec = 0;
        sbfSecuenciaNueva = this.newSec;
        console.log(sbfSecuenciaNueva);
      } else if (restriccionesPosee.respuesta == 'Sin informacion registrada') {
        this.newSec = 0;
        sbfSecuenciaNueva = this.newSec;
      } else {
        let datosParaSecuencia = restriccionesPosee.respuesta;
        let ss;
        let secu = [];
        for (ss of datosParaSecuencia) {
          secu = [...secu, ss.sbfSecuencia];
        }
        let secuenciarestriccion = restriccionesPosee.respuesta;

        let arrSec = [];
        let sec;
        for (sec of secuenciarestriccion) {
          let secu = parseInt(sec.sbfSecuencia);
          arrSec = [...arrSec, secu];
        }
        maxSec = Math.max.apply(Math, arrSec);

        sbfSecuenciaNueva = parseInt(maxSec) + 1;
        this.newSec = sbfSecuenciaNueva;
      }

      let R1;
      let R2;
      let R3;
      let R4;
      let R5;
      let R6;
      let R7;
      let R8;

      if (salidaRestricciones.respuestaMultiple[0] != undefined) {
        if (salidaRestricciones.respuestaMultiple[0].toString().length == 1) {
          R1 = '000' + salidaRestricciones.respuestaMultiple[0];
        } else {
          R1 = '00' + salidaRestricciones.respuestaMultiple[0];
        }
      } else {
        R1 = '0000';
      }

      if (salidaRestricciones.respuestaMultiple[1] != undefined) {
        R2 = '000' + salidaRestricciones.respuestaMultiple[1];
      } else {
        R2 = '0000';
      }

      if (salidaRestricciones.respuestaMultiple[2] != undefined) {
        R3 = '000' + salidaRestricciones.respuestaMultiple[2];
      } else {
        R3 = '0000';
      }
      if (salidaRestricciones.respuestaMultiple[3] != undefined) {
        R4 = '000' + salidaRestricciones.respuestaMultiple[3];
      } else {
        R4 = '0000';
      }
      if (salidaRestricciones.respuestaMultiple[4] != undefined) {
        R5 = '000' + salidaRestricciones.respuestaMultiple[4];
      } else {
        R5 = '0000';
      }
      if (salidaRestricciones.respuestaMultiple[5] != undefined) {
        R6 = '000' + salidaRestricciones.respuestaMultiple[5];
      } else {
        R6 = '0000';
      }
      if (salidaRestricciones.respuestaMultiple[6] != undefined) {
        R7 = '000' + salidaRestricciones.respuestaMultiple[6];
      } else {
        R7 = '0000';
      }
      if (salidaRestricciones.respuestaMultiple[7] != undefined) {
        R8 = '000' + salidaRestricciones.respuestaMultiple[7];
      } else {
        R8 = '0000';
      }

      let idComuna = salidaRestricciones.codRNC.toString();
      let COMUNA;

      if (idComuna.length === 1) {
        COMUNA = '000' + idComuna;
      } else if (idComuna.length === 2) {
        COMUNA = '00' + idComuna;
      } else if (idComuna.length === 3) {
        COMUNA = '0' + idComuna;
      } else {
        COMUNA = idComuna;
      }

      let sbfRun = salidaRestricciones.RUNDV.substring(0, salidaRestricciones.RUNDV.length - 1);
      let RUNCONDV = salidaRestricciones.RUNDV;

      sbfDatos =
        RUNCONDV +
        '|' +
        COMUNA +
        '|' +
        FECHARESTRICCION +
        '|' +
        R1 +
        '|' +
        R2 +
        '|' +
        R3 +
        '|' +
        R4 +
        '|' +
        R5 +
        '|' +
        R6 +
        '|' +
        R7 +
        '|' +
        R8 +
        '|';
      anotaciones = [
        {
          sbfTipoReg: 'C',
          sbfSubtipo: ' ',
          sbfRun: sbfRun,
          sbfFechaSub: fecha,
          sbfSecuencia: sbfSecuenciaNueva.toString(),
          sbfCodigo: '1089',
          sbfEstado: 'L',
          sbfDisponible: ' ',
          sbfDatos: sbfDatos,
          sbfNroDoc: '0',
        },
      ];

      let Anotaciones = { Anotaciones: anotaciones };

      let dataInsertAnotaciones = [];

      dataInsertAnotaciones.push(idUsuario);
      dataInsertAnotaciones.push(Anotaciones);

      console.log(Anotaciones);

      await this.insertarRestricciones(dataInsertAnotaciones);
    } catch (e) {
      console.log(e);
    }
  }

  async insertarRestricciones(dataInsertAnotaciones): Promise<any> {
    try {
      let conexion = await this.ingresoPostulanteService.validaStatus();
      if (conexion === 'OK') {
        console.log(' ============ data para informar restriccion: ============');
        console.log(dataInsertAnotaciones);
        console.log(dataInsertAnotaciones[1].Anotaciones);
        console.log(' ============ data para informar restriccion: ============');
        let url = this.ingresoPostulanteService.BASE_URL_REGISTRO_CIVIL;
        const restricciones = await axios.post(url + '/api/anotaciones', dataInsertAnotaciones);
        console.log('============ insert de restriccion ===============');

        if (restricciones.data.respuesta.codigo === '0') {
          console.log(restricciones.data.respuesta);
          this.salidaInsertrestricciones = {
            codigoInsert_restriccion: 200,
            statusProcesoInsert_restriccion: 'OK',
            datosEnviadosInsert_restriccion: dataInsertAnotaciones.Anotaciones,
            respuestaRCeI_restriccion: restricciones.data.respuesta,
          };
        } else {
          this.salidaInsertrestricciones = {
            codigoInsert_restriccion: 403,
            statusProcesoInsert_restriccion: 'NOK',
            datosEnviadosInsert_restriccion: dataInsertAnotaciones[1].Anotaciones,
            respuestaRCeI_restriccion: restricciones.data.respuesta,
          };
        }
        return this.salidaInsertrestricciones;
      } else {
        return {
          host: 'privqa.srcei.cl',
          estatus: 'NOK',
          mensaje: 'Sin conexion con Registro Civil',
        };
      }
    } catch (e) {
      console.log(e);
      this.ingresoPostulanteService.errorExcesoPeticiones(e);
      return this.insertarRestricciones(dataInsertAnotaciones);
    }
  }

  async actualizaEstadoSGL1111(data) {
    try {
      let idTramite = data.idTramite;
      const actualiza = await getConnection().query(`select actualizar_estado_sgl_a_1111(${idTramite}) as resp`);
      if (actualiza[0].resp === 0) {
        return { codigo: 400, mensaje: 'El tramite: ' + idTramite + ' no se encuentra registrado' };
      }
      return { codigo: 200, mensaje: 'El tramite: ' + idTramite + ' fue actualizado a estado 1111' };
    } catch (e) {
      console.log(e);
    }
  }

  async actualizar_estado_sgl_a_1104(data) {
    try {
      let idTramite = data.idTramite;
      const actualiza = await getConnection().query(`select actualizar_estado_sgl_a_1104(${idTramite}) as resp`);
      if (actualiza[0].resp === 0) {
        return { mensaje: 'El tramite: ' + idTramite + ' no se encuentra registrado' };
      }
      return { codigo: 200, mensaje: 'El tramite: ' + idTramite + ' fue actualizado a estado 1104' };
    } catch (e) {
      console.log(e);
    }
  }

  async consultaPago(data) {
    try {
      let idTramite = parseInt(data.idTramite);

      const pagpsPendientes = await getConnection().query(`select validapagotramite(${idTramite}) as resp`);
      console.log('salida: ' + pagpsPendientes[0]);

      if (pagpsPendientes[0] == '' || pagpsPendientes[0] == undefined) {
        return { pagosPendientes: false, mensaje: 'NO Posee pagos pendientes' };
      } else {
        return { pagosPendientes: true, mensaje: 'Posee pagos pendientes' };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async consultaRPI(data) {
    try {
      let RUN = data.run;
      return { registrosRPI: false };
    } catch (e) {
      console.log(e);
    }
  }

  async actualizar_estado_sgl_a_1303(data) {
    try {
      let idTramite = data.idTramite;
      const actualiza = await getConnection().query(`select actualizar_estado_sgl_a_1303(${idTramite}) as resp`);
      if (actualiza[0].resp === 0) {
        return { codigo: 400, mensaje: 'El tramite: ' + idTramite + ' no se encuentra registrado' };
      }
      return { codigo: 200, mensaje: 'El tramite: ' + idTramite + ' fue actualizado a estado  1303' };
    } catch (e) {
      console.log(e);
    }
  }

  async validaReglasNegocioDenegacion(data): Promise<any> {
    try {
      let idTramite = data.idTramite;
      let respHis;

      const valida = await getConnection().query(`select validarTramiteDenegacion(${idTramite}) as resp`);
      if (valida[0] === '' || valida[0] === undefined) {
        return { codigo: 400, respuesta: 'El tramite: ' + idTramite + ' no se encuentra registrado' };
      } else {
        let resp = valida[0].resp;
        let _run = valida[0].resp.RUN;
        let _idUsuario = data.idUsuario;
        let dataActualiza = { idUsuario: _idUsuario, run: _run };

        let actualizaHis = await this.ingresoPostulanteService.consulHis(dataActualiza);
        console.log(actualizaHis.codigoRespuesta);
        if (actualizaHis.codigoRespuesta === 200) {
          respHis = { ActualizacionHistorico: 'OK', mensaje: 'El historico se actualizo correctamente' };
        } else {
          respHis = { ActualizacionHistorico: 'NOK', mensaje: 'El historico NO se pudo actualizadar correctamente' };
        }
        return { codigo: 200, respuesta: resp, Hitorico: respHis };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async consulSen(data): Promise<any> {
    try {
      let sen = await this.ingresoPostulanteService.consulSen(data);
      let senA = await this.ingresoPostulanteService.consulSenAux(data);

      if (sen == '' || sen == undefined) {
        return { poseeSentencias: false, mensaje: 'EL postulate NO tiene sentencias registradas', respuesta: [{ esSuspension: false }] };
      }

      var f = new Date();

      let salida = [];
      let salidaA = [];
      let suspensionCumplida;
      let suspensionCumplidaA;
      let esSuspension;
      let esSuspensionA;

      let S;
      for (S of sen) {
        let senCodRes001 = S.senCodRes001;
        let senCodRes002 = S.senCodRes002;
        let senCodRes003 = S.senCodRes003;
        let senSuspencionTot = S.senSuspencionTot;
        let senFechaResol = S.senFechaResol;
        let dia = senFechaResol.substr(0 - 2);
        let mes = senFechaResol.substr(-4, 2);
        let anio = senFechaResol.substr(-8, 4);
        let fechaSuspencion = anio + '-' + mes + '-' + dia;

        var fFecha1 = Date.UTC(anio, mes - 1, dia);
        var fFecha2 = Date.UTC(f.getFullYear(), f.getMonth() - 1, f.getDate());
        var dif = fFecha2 - fFecha1;
        var dias_trancuridos = Math.floor(dif / (1000 * 60 * 60 * 24));
        let dt = dias_trancuridos;

        if (dt > senSuspencionTot) {
          suspensionCumplida = true;
        } else {
          suspensionCumplida = false;
        }

        if (senCodRes001 == 4 || senCodRes001 == 8 || senCodRes002 == 4 || senCodRes002 == 8 || senCodRes003 == 4 || senCodRes003 == 8) {
          esSuspension = true;
        } else {
          esSuspension = false;
        }

        salida = [
          ...salida,
          {
            esSuspension,
            codigoRes1: senCodRes001,
            codigoRes2: senCodRes002,
            codigoRes3: senCodRes003,
            diasSuspencion: senSuspencionTot,
            fechaRosolucion: senFechaResol,
            diasTranscurridos: dias_trancuridos,
            suspensionCumplida,
          },
        ];
      }

      let filtroSuspension = salida.filter(
        F => F.codigoRes1 == 4 || F.codigoRes1 == 8 || F.codigoRes2 == 4 || F.codigoRes2 == 8 || F.codigoRes3 == 4 || F.codigoRes3 == 8
      );

      let SA;
      for (SA of senA) {
        let senauxFechaResol = SA.senauxFechaResol;
        let senauxCodRes = SA.senauxCodRes;
        let dia = senauxFechaResol.substr(0 - 2);
        let mes = senauxFechaResol.substr(-4, 2);
        let anio = senauxFechaResol.substr(-8, 4);
        let fechaSuspencion = anio + '-' + mes + '-' + dia;

        var fFecha1 = Date.UTC(anio, mes - 1, dia);
        var fFecha2 = Date.UTC(f.getFullYear(), f.getMonth() - 1, f.getDate());
        var dif = fFecha2 - fFecha1;
        var dias_trancuridos = Math.floor(dif / (1000 * 60 * 60 * 24));
      }

      if (sen === 'Sin informacion registrada' && senA === 'Sin informacion registrada') {
        return { poseeSentencias: false, mensaje: 'EL postulate tiene sentencias registradas' };
      } else {
        let sentencias = [];
        sentencias = [...sentencias, { Sentencias: sen }, { SentenciasAux: senA }];

        return { poseeSentencias: true, mensaje: 'EL postulate tiene sentencias registradas con suspension', respuesta: filtroSuspension };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async consultaPlazosSentencias(data): Promise<any> {
    try {
      let _run = data.run;
      const consulta = await getConnection().query(`select consultaplazossen(${_run}) as resp`);
      if (consulta == '') {
        return { codigo: 0, respuesta: 'No posee sentencias registradas' };
      } else {
        return { codigo: 1, respuesta: consulta };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async informarDenegacion(data): Promise<any> {
    try {
      let _idTramite = data.idTramite;
      let _idUsuario = data.idUsuario;
      let getData = await getConnection().query(`select obtenerdatosdenegacion(${_idTramite}) as resp`);
      let secuencia;
      let Mes;
      let Dia;
      let Lic;
      let comuna;
      let longMax = 40;
      let Denegacion;
      let CantidadDiasDenegacion;

      var f = new Date();

      if ((f.getMonth() + 1).toString().length == 1) {
        Mes = '0' + (f.getMonth() + 1).toString();
      } else {
        Mes = f.getMonth() + 1;
      }
      if (f.getDate().toString().length == 1) {
        Dia = '0' + f.getDate().toString();
      } else {
        Dia = f.getDate();
      }

      let fechaSBF_DATOS = Dia + '' + Mes + '' + f.getFullYear();
      let FechaSub = f.getFullYear() + '' + Mes + '' + Dia;

      console.log(getData[0].resp);

      if (getData == '') {
        return { codigo: 0, respuesta: 'No posee registros para denegar' };
      } else {
        let run = getData[0].resp.RUN + getData[0].resp.DV;
        let RUN = getData[0].resp.RUN.toString();

        let dataConsulLic = { idUsuario: _idUsuario, run: run };
        let consulLic = await this.ingresoPostulanteService.consulLic(dataConsulLic);

        if (consulLic == undefined) {
          consulLic = [];
        }
        let licenciasPostulante = [];
        let L;
        for (L of consulLic) {
          licenciasPostulante = [...licenciasPostulante, L.licTipo];
        }

        if (run.toString().length === 8) {
          run = '0' + run;
        } else {
          run = run;
        }

        if (getData[0].resp.claseLicencia.toString().length == 1) {
          Lic = getData[0].resp.claseLicencia + '   ';
        }
        if (getData[0].resp.claseLicencia.toString().length == 2) {
          Lic = getData[0].resp.claseLicencia.toString() + '  ';
        }
        if (getData[0].resp.claseLicencia.toString().length == 3) {
          Lic = getData[0].resp.claseLicencia.toString() + ' ';
        }
        if (getData[0].resp.claseLicencia.toString().length == 4) {
          Lic = getData[0].resp.claseLicencia.toString();
        }

        if (getData[0].resp.comuna.toString().length == 1) {
          comuna = '000' + getData[0].resp.comuna.toString();
        }

        if (getData[0].resp.comuna.toString().length == 2) {
          comuna = '00' + getData[0].resp.comuna.toString();
        }
        if (getData[0].resp.comuna.toString().length == 3) {
          comuna = '0' + getData[0].resp.comuna.toString();
        }

        if (getData[0].resp.MotivoDenegacion == null || getData[0].resp.MotivoDenegacion == '') {
          return { codigo: 400, mensaje: 'No existe motivo de denegacion' };
        }

        let mot = getData[0].resp.codigoSRCeI;
        if (getData[0].resp.codigoSRCeI.toString().length == 1) {
          Denegacion = '000' + getData[0].resp.codigoSRCeI.toString();
        }
        if (getData[0].resp.codigoSRCeI.toString().length == 2) {
          Denegacion = '00' + getData[0].resp.codigoSRCeI.toString();
        }
        console.log(getData[0].resp.plazoDenegacion);

        let CantidadDiasDenegacion = getData[0].resp.plazoDenegacion.toString();

        //variables para el campo V_SBF_DATOS
        let sbfFechaDenegacion = fechaSBF_DATOS + '|';
        let sbfTipo = Lic + '|';
        let sbfRun = run + '|';
        let sbfCodigoComuna = comuna + '|';
        let sbfDenegacion = Denegacion.toString() + '|';

        let sbfCantidadDiasDenegacion = CantidadDiasDenegacion + '|';
        let SbfDatos = sbfFechaDenegacion + sbfTipo + sbfRun + sbfCodigoComuna + sbfDenegacion + sbfCantidadDiasDenegacion;

        let dataConsulta = { idUsuario: _idUsuario, run };
        let consulDenegaciones = await this.ingresoPostulanteService.consultaDenegaciones(dataConsulta);

        if (consulDenegaciones.respuesta == 'Sin informacion registrada') {
          secuencia = 0;
        } else if (consulDenegaciones.respuesta == '') {
          secuencia = 0;
        } else {
          let secu = consulDenegaciones.respuesta;
          let ss;
          let newSec = [];
          for (ss of secu) {
            newSec = [...newSec, ss.sbfSecuencia];
          }
          secuencia = parseInt(Math.max.apply(null, newSec)) + 1;
        }
        let anotaciones = [
          {
            sbfTipoReg: 'C',
            sbfSubtipo: ' ',
            sbfRun: RUN,
            sbfFechaSub: FechaSub,
            sbfSecuencia: secuencia,
            sbfCodigo: '1149',
            sbfEstado: 'L',
            sbfDisponible: ' ',
            sbfDatos: `${SbfDatos}`,
            sbfNroDoc: '0',
          },
        ];

        let Anotaciones = { Anotaciones: anotaciones };
        let idUsuario = { idUsuario: _idUsuario };

        let dataInsertAnotaciones = [];

        dataInsertAnotaciones.push(idUsuario);
        dataInsertAnotaciones.push(Anotaciones);

        //console.log('secuencia ' + secuencia);
        let url = this.ingresoPostulanteService.BASE_URL_REGISTRO_CIVIL;
        const insertAnotaciones = await axios.post(url + '/api/anotaciones', dataInsertAnotaciones);
        console.log(insertAnotaciones.data.respuesta);
        if (insertAnotaciones.data.respuesta.codigo == '0') {
          return {
            codigo: 200,
            respuesta: 'Denegación registrada',
            informacionPostulante: getData[0].resp,
            registroDeLicencias: licenciasPostulante,
            datosEnviados: anotaciones,
          };
        } else {
          return {
            codigo: 400,
            respuesta: 'No se pupo insertar la denegacion',
            informacionPostulante: getData[0].resp,
            registroDeLicencias: licenciasPostulante,
            datosEnviados: anotaciones,
          };
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  async cambioEstadoDenegacion(data): Promise<any> {
    try {
      console.log('data para denegar');
      console.log(data);
      let _idUsuario = data[0].idUsuario;
      let run = data[0].run;
      let dataConsulLic = { idUsuario: _idUsuario, run };
      let tipoLic = data[0].tipoLic;
      let dataLic = data[1].Licencias;
      let flagsSalidaInsert;
      let flagSalidaUpdate;
      let salidaUpdate;
      let salidaInsert;
      let flagSalidaProceso;
      let idUsuario = { idUsuario: _idUsuario };

      let consulLic = await this.ingresoPostulanteService.consulLic(dataConsulLic);

      console.log(consulLic);

      if (consulLic == undefined) {
        consulLic = [];
      }
      let lic = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));
      let longitud = data[0].tipoLic.length;

      for (let i = 0; i < longitud; i++) {
        let licTipo = data[0].tipoLic[i];
        //console.log(licTipo);
        let claseLics = lic.filter(claseLic => claseLic.licTipo == licTipo.toUpperCase());
        let respuesta = { tipo: licTipo, claseLic: claseLics };
        if (respuesta.claseLic != '') {
          console.log('posee lic ' + claseLics[0].licTipo + ' , se ejecuta updateLic');
          let licPosee = dataLic.filter(claseLic => claseLic.licTipo == claseLics[0].licTipo.toUpperCase());

          let Licencias = { Licencias: licPosee };

          let dataLicUpdate = [];
          dataLicUpdate = [...dataLicUpdate, idUsuario, Licencias];

          let actualizaLic = await this.ingresoPostulanteService.updateLic(dataLicUpdate);
          console.log(actualizaLic.respuesta);

          if (actualizaLic) {
            if (actualizaLic.respuesta.codigo === '0') {
              let codigoRespuesta = actualizaLic.respuesta.codigo;
              let idPeticion = actualizaLic.datos_enviados.Licencias[0].idPeticion;
              let datos_enviados_update = actualizaLic.datos_enviados.Licencias;
              flagsSalidaInsert = 0;
              flagSalidaUpdate = 1;
              salidaUpdate = { codigoUpdate: 200, statusProcesoUpdate: 'OK', datosEnviadosUpdate: datos_enviados_update };
            } else {
              salidaUpdate = { codigoUpdate: 403, statusProcesoUpdate: 'NOK', respuestaRCeI: actualizaLic.respuesta };
            }
          } else {
            flagsSalidaInsert = 0;
            flagSalidaUpdate = 0;
          }
        }

        if (respuesta.claseLic == '') {
          console.log('NO Posee lic ' + respuesta.tipo + ' , se ejecuta insertLic');
          let dat = JSON.parse(JSON.stringify(dataLic).replace(/"\s+|\s+"/g, '"'));
          //this.salidaUpdate = {};

          let licPoseeIns = dat.filter(claseLic => claseLic.licTipo == respuesta.tipo.toUpperCase());

          //let licPoseeInsert = JSON.parse(JSON.stringify(licPoseeIns).replace(/"\s+|\s+"/g, '"'));
          let licPoseeInsert = JSON.parse(JSON.stringify(licPoseeIns).trim());
          console.log(licPoseeInsert);

          let Licencias = { Licencias: licPoseeInsert };
          let dataLicInsert = [];
          dataLicInsert = [...dataLicInsert, idUsuario, Licencias];
          let insertaLic = await this.ingresoPostulanteService.insertLic(dataLicInsert);
          console.log(insertaLic);
          if (insertaLic) {
            if (insertaLic.respuesta.codigo === '0') {
              let datos_enviados_insert = insertaLic.datos_enviados.Licencias;
              flagsSalidaInsert = 1;
              flagSalidaUpdate = 0;
              salidaInsert = { codigoInsert: 200, statusProcesoInsert: 'OK', datosEnviadosInsert: datos_enviados_insert };
            } else {
              salidaInsert = { codigoInsert: 403, statusProcesoInsert: 'NOK', respuestaRCeI: insertaLic.respuesta };
            }
          } else {
            flagsSalidaInsert = 0;
            flagSalidaUpdate = 0;
          }
        }
      }

      let statusSalida = flagsSalidaInsert + flagSalidaUpdate;
      if (statusSalida == 1 || statusSalida == 2) {
        flagSalidaProceso = 'OK';
      } else {
        flagSalidaProceso = 'NOK';
      }

      return { ejecucionProceso: flagSalidaProceso, salidaInsert, salidaUpdate };
    } catch (e) {
      console.log(e);
    }
  }
  async cambioEstadoTramiteDenegacion(data): Promise<any> {
    try {
      let _idTramite = data.idTramite;
      const consulta = await getConnection().query(`select cambio_estado_tramite_denegacion(${_idTramite}) as resp`);

      if (consulta[0].resp === -1) {
        return { codigo: 400, respuesta: 'Tramite No registrado' };
      } else {
        return { codigo: 200, respuesta: 'Estado de Tramite Actualizado' };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async consultaEstadoSolicitud(data): Promise<any> {
    try {
      let respHis;
      let _R = data.run;
      let idUsuario = data.idUsuario;
      const consulta = await getConnection().query(`select consulta_estado_solicitud(${_R}) as resp`);
      let dv = data.dv;
      let run = _R.toString() + dv;
      let dataActualiza = { idUsuario, run };

      if (consulta[0] == undefined || consulta[0] == '' || consulta == '') {
        let actualizaHis = await this.ingresoPostulanteService.consulHis(dataActualiza);
        console.log(actualizaHis.codigoRespuesta);
        if (actualizaHis.codigoRespuesta == 200) {
          respHis = 'Historico actualizado correctamente';
        } else {
          respHis = 'Historico No se pudo actualizar correctamente';
        }
        return { codigo: 200, solicitudes: false, respuesta: 'El Postlante no tiene solicitudes', actualizacionHIS: respHis };
      } else {
        return { codigo: 200, solicitudes: true, respuesta: consulta[0].resp };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async consulLicIngresoPostulante(data): Promise<any> {
    try {
      let _R = data.run;
      let dv = data.dv;
      let idUsuario = data.idUsuario;
      let run = _R.toString() + dv;
      let dataConsulta = { idUsuario, run };

      let consulLic = await this.ingresoPostulanteService.consulLic(dataConsulta);
      let lic = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));
      let filtraLic_102 = lic.filter(claseLic => claseLic.licEstadoClaLic == 102);

      if (filtraLic_102 == '' || filtraLic_102 == undefined) {
        return { tramites_102: false, mensaje: 'No posee licencias con PCL 102' };
      } else {
        return { tramites_102: true, mensaje: 'Posee licencias con PCL 102', Licencias: lic };
      }
    } catch (e) {
      console.log(e);
      return { e };
    }
  }

  async consulSenIngresoPostulante(data): Promise<any> {
    /* codigos resolucion
1: ABSUELTO
2: MULTA 
3: CANCELACION ****
4: SUSPENSION ****
5: AMONESTACION
6: INCOMPETENTE
7: Inhabilitación   10373762 1
8: SUSPENSION Y MULTA*/
    try {
      let _R = data.run;
      let dv = data.dv;
      let idUsuario = data.idUsuario;
      let run = _R.toString() + dv;
      let dataConsulta = { idUsuario, run };
      let suspension = [];
      let multa = [];
      let cancelacion = [];
      let sentencias = [];
      let inhabilitacion = [];
      let sentenciaJudicialVigente;
      let ts;

      let sen = await this.ingresoPostulanteService.consulSen(dataConsulta);
      console.log(' ================================================================================================== ');
      console.log(' ================================================================================================== ');
      console.log(' ================================================================================================== ');
      console.log(sen);
      if (sen == 'Sin informacion registrada' || sen == '') {
        return {
          sentenciaJudicialVigente: false,
          mensaje: 'No Posee registros de sentencias',
          multa: [],
          suspension: [],
          cancelacion: [],
          inhabilitacion: [],
        };
      }

      let senA = await this.ingresoPostulanteService.consulSenAux(dataConsulta);

      console.log(' ================================================================================================== ');
      console.log(' ================================================================================================== ');
      console.log(' ================================================================================================== ');
      console.log(senA);

      let s;
      sentenciaJudicialVigente = false;
      for (s of sen) {
        let res1 = s.senCodRes001;
        let res2 = s.senCodRes002;
        let res3 = s.senCodRes003;
        ts = s.senSuspencionTot;
        let fr = s.senFechaResol;

        var f = new Date();
        let dia = fr.substr(0 - 2);
        let mes = fr.substr(-4, 2);
        let anio = fr.substr(-8, 4);

        var fFecha1 = Date.UTC(anio, mes - 1, dia);
        var fFecha2 = Date.UTC(f.getFullYear(), f.getMonth() - 1, f.getDate());
        var dif = fFecha2 - fFecha1;
        var dias_trancuridos = Math.floor(dif / (1000 * 60 * 60 * 24));
        let dt = dias_trancuridos;
        console.log('dias_trancuridos: ' + dias_trancuridos);

        if (dt <= ts) {
          sentenciaJudicialVigente = true;
        }

        let mensajeS = {
          suspension: true,
          mensaje: 'Posee suspension',
          fechaResolucion: fr,
          tiempoSuspension: ts,
        };
        let mensajeM = { multa: true, mensaje: 'Posee multa', fechaResolucion: fr, tiempoSuspension: ts };
        let mensajeC = {
          cancelacion: true,
          mensaje: 'Posee cancelacion',
          fechaResolucion: fr,
          tiempoSuspension: ts,
        };
        let mensajeI = { inhabilitacion: true, mensaje: 'Posee Inhabilitacion' };

        /* codigos resolucion
1: ABSUELTO
2: MULTA 
3: CANCELACION ****
4: SUSPENSION ****
5: AMONESTACION
6: INCOMPETENTE
7: Inhabilitación  
8: SUSPENSION Y MULTA*/
        if (res1 == 2 || res2 == 2 || res3 == 2) {
          multa = [...multa, mensajeM];
        }
        if (res1 == 3 || res2 == 3 || res3 == 3) {
          cancelacion = [...cancelacion, mensajeC];
        }
        if (res1 == 4 || res2 == 4 || res3 == 4 || res1 == 8 || res2 == 8 || res3 == 8) {
          suspension = [...suspension, mensajeS];
        }
        if (res1 == 7 || res2 == 7 || res3 == 7) {
          inhabilitacion = [...inhabilitacion, mensajeI];
        }
      }
      let sa;
      for (sa of senA) {
        let res = sa.senauxCodRes;
        let fr = s.senauxFechaResol;
        console.log(sentenciaJudicialVigente);
        let mensajeSA = {
          suspensionAux: true,
          mensajeAux: 'Posee suspension',
          fechaResolucionAux: fr,
        };
        let mensajeMA = { multaAux: true, mensajeAux: 'Posee multa', fechaResolucionAux: fr };
        let mensajeCA = {
          cancelacionAux: true,
          mensajeAux: 'Posee cancelacion',
          fechaResolucionAux: fr,
        };

        let mensajeIA = { inhabilitacionAux: true, mensajeAux: 'Posee inhabilitacion' };
        if (res == 2) {
          multa = [...multa, mensajeMA];
        }
        if (res == 3) {
          cancelacion = [...cancelacion, mensajeCA];
        }
        if (res == 4 || res == 8) {
          suspension = [...suspension, mensajeSA];
        }
        if (res == 7) {
          inhabilitacion = [...inhabilitacion, mensajeIA];
        }
      }

      sentencias = [
        ...sentencias,
        { run: _R, dv: dv, sentenciaJudicialVigente },
        { multas: multa },
        { suspensiones: suspension },
        { cancelaciones: cancelacion },
        { inhabilitacion: inhabilitacion },
      ];
      /*console.log(sentencias[0].sentenciaJudicialVigente);
      console.log(sentencias[1].multas);
      console.log(sentencias[2].suspensiones);
      console.log(sentencias[3].cancelaciones);*/
      sentenciaJudicialVigente = sentencias[0].sentenciaJudicialVigente;
      multa = sentencias[1].multas;
      suspension = sentencias[2].suspensiones;
      cancelacion = sentencias[3].cancelaciones;
      inhabilitacion = sentencias[4].inhabilitacion;

      return { sentenciaJudicialVigente, multa, suspension, cancelacion, inhabilitacion };
    } catch (e) {
      console.log(e);
      return e;
    }
  }
  async consultaDenegacionesIngPost(data): Promise<any> {
    try {
      let _R = data.run;
      let dv = data.dv;
      let idUsuario = data.idUsuario;
      let tipoLic = data.tipoLic;
      let run = _R.toString() + dv;
      let dataConsulta = { idUsuario, run };
      let denegacionVigente;
      let denegacionesClaseLic;
      let licDen;
      let salidaLic = [];

      let consulDenegaciones = await this.ingresoPostulanteService.consultaDenegaciones(dataConsulta);

      if (consulDenegaciones.respuesta == 'Sin informacion registrada') {
        return { respuesta: 'Sin informacion registrada' };
      } else {
        let DEN = consulDenegaciones.respuesta;
        let filtro = DEN;
        if (filtro == '') {
          return { denegaciones: false, mensaje: 'No posee denegaciones' };
        } else {
          let s;
          let denegaciones = [];
          for (s of filtro) {
            let fd = s.sbfFechaSub;
            let td = parseInt(s.sbfDatos.substr(-40, 2));
            let lic = s.sbfDatos.substr(-141, 2);

            var f = new Date();
            let dia = fd.substr(0 - 2);
            let mes = fd.substr(-4, 2);
            let anio = fd.substr(-8, 4);

            var fFecha1 = Date.UTC(anio, mes - 1, dia);
            var fFecha2 = Date.UTC(f.getFullYear(), f.getMonth() - 1, f.getDate());
            var dif = fFecha2 - fFecha1;
            var dias_trancuridos = Math.floor(dif / (1000 * 60 * 60 * 24));
            let dt = dias_trancuridos;

            if (dt > td) {
              denegacionVigente = false;
            } else {
              denegacionVigente = true;
            }

            denegaciones = [...denegaciones, { fechaDenegacion: fd, tiempoDenegacion: td, claseLicencia: lic, denegacionVigente }];
            licDen = JSON.parse(JSON.stringify(denegaciones).replace(/"\s+|\s+"/g, '"'));
          }
          let CL;
          for (CL of tipoLic) {
            let clase = CL;
            let filtroCL = licDen.filter(f => f.claseLicencia == clase);
            let salida = filtroCL[0];
            if (salida == null) {
              console.log(salida);
              salida = { mensaje: 'No posee denegaciones para la clase ' + clase };
              console.log(salida);
              salidaLic = [...salidaLic, salida];
            } else {
              salidaLic = [...salidaLic, salida];
            }
          }
        }
      }
      return salidaLic;
    } catch (e) {
      console.log(e);
    }
  }

  async verificaLicIngresoPostulante(data): Promise<any> {
    try {
      let _R = data.run;
      let dv = data.dv;
      let idUsuario = data.idUsuario;
      let tipoLic = data.tipoLic;
      let run = _R.toString() + dv;
      let dataConsulta = { idUsuario, run };
      let L;
      let filtraLic;
      let salidaLic = [];

      for (L of tipoLic) {
        let consulLic = await this.ingresoPostulanteService.consulLic(dataConsulta);
        let lic = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));
        filtraLic = lic.filter(Lic => Lic.licTipo == L);
        let salida = filtraLic[0];

        if (salida == null) {
          console.log(salida);
          salida = { mensaje: 'No posee registros de la clase ' + L + ' en el RNC' };
          console.log(salida);
          salidaLic = [...salidaLic, salida];
        } else {
          salidaLic = [...salidaLic, salida];
        }
      }
      return { Licencias: salidaLic };
    } catch (e) {
      console.log(e);
    }
  }
  async actualizaEstadoSGL1101(data) {
    try {
      let idTramite = data.idTramite;
      const actualiza = await getConnection().query(`select actualizar_estado_sgl_a_1101(${idTramite}) as resp`);
      if (actualiza[0].resp === 0) {
        return { codigo: 400, mensaje: 'El tramite: ' + idTramite + ' no se encuentra registrado' };
      }
      return { codigo: 200, mensaje: 'El tramite: ' + idTramite + ' fue actualizado a estado 1101' };
    } catch (e) {
      console.log(e);
    }
  }
  async actualizaSolicitudSGL1205(data) {
    try {
      let idTramite = data.idTramite;
      const actualiza = await getConnection().query(`select actualizar_estado_sgl_Solicitud_1205(${idTramite}) as resp`);
      if (actualiza[0].resp === 0) {
        return { codigo: 400, mensaje: 'El tramite: ' + idTramite + ' no se encuentra registrado' };
      }
      return { codigo: 200, mensaje: 'La solicitud con el tramite: ' + idTramite + ' fue actualizada a estado 1205(Abierta)' };
    } catch (e) {
      console.log(e);
    }
  }
  async recepcionConforme(data): Promise<any> {
    try {
      let idTramite = data.idTramite;
      let idUs = data.idUsuario;
      let conforme = data.conforme;
      let app = data.app;
      let licTipo = [];
      let EDF;
      let EDD;
      let PCL;
      let respuestaProceso;

      const datos = await getConnection().query(`select get_datos_recepcion_conforme(${idTramite}) as resp`);

      if (datos[0].resp == null || datos[0].resp == '' || datos[0].resp == undefined) {
        return { mensaje: 'No existe el id del tramite', codigo: 400 };
      } else {
        let salida = datos[0].resp.map(item => {
          return {
            idTramite: item.f1,
            idSolicitud: item.f2,
            idClaseLicencia: item.f3,
            Abreviacion: item.f4,
            RUN: item.f6,
            DV: item.f7,
            idTipoTramite: item.f8,
            Nombre: item.f9,
          };
        });
        let datosrecepcionConf = salida;

        console.log(salida);

        let RC;
        for (RC of datosrecepcionConf) {
          licTipo = [...licTipo, RC.Abreviacion];
        }

        licTipo = [...licTipo, 'A'];

        let _run = salida[0].RUN.toString() + '' + salida[0].DV.toString();
        let dataConsulta = {
          idUsuario: idUs,
          run: _run,
        };

        let licenciasPosee = await this.ingresoPostulanteService.consulLic(dataConsulta);
        let lic = JSON.parse(JSON.stringify(licenciasPosee).replace(/"\s+|\s+"/g, '"'));
        let tipoTramite = salida[0].idTipoTramite;

        let L;
        for (L of licTipo) {
          let filtraLic = lic.filter(Lic => Lic.licTipo == L);

          if (conforme == true) {
            if (tipoTramite == 4) {
              EDF = '205';
              PCL = '103';
              if (app == 'SGL') {
                EDD = '305';
              } else {
                EDD = '303';
              }
            } else {
              EDF = '203';
              PCL = '103';

              if (app == 'SGL') {
                EDD = '302';
              } else {
                EDD = '303';
              }
            }
          } else {
            EDF = '207';
            EDD = '301';
            PCL = '102';
          }

          let lics = [
            {
              licComunaTraLic: filtraLic[0].licComunaTraLic,
              licLetraFolio: filtraLic[0].licLetraFolio,
              licEstadoDigLic: EDD,
              licEstadoFisLic: EDF,
              licRun: filtraLic[0].licRun,
              licFechaUltLic: filtraLic[0].licFechaUltLic,
              licFechaTraLic: filtraLic[0].licFechaTraLic,
              licLetraFolioIni: filtraLic[0].licLetraFolioIni,
              licLetraFolioTra: filtraLic[0].licLetraFolioTra,
              licFolio: filtraLic[0].licFolio,
              licComunaPriLic: filtraLic[0].licComunaPriLic,
              licRunEscuela: filtraLic[0].licRunEscuela,
              licComunaUltLic: filtraLic[0].licComunaUltLic,
              licFechaPrxCtl: filtraLic[0].licFechaPrxCtl,
              licTipo: filtraLic[0].licTipo,
              licFechaPriLic: filtraLic[0].licFechaPriLic,
              licObservaciones: ' ',
              licFolioTra: filtraLic[0].licFolioTra,
              licEstadoClaLic: PCL,
              licFolioIni: filtraLic[0].licFolioIni,
            },
          ];

          console.log(
            '==========================================================================================================================='
          );
          console.log(
            '=============================================== lics que se envian ============================================================================'
          );
          console.log(
            '==========================================================================================================================='
          );
          console.log(lics);

          let idUsuario = { idUsuario: idUs };
          let Licencias = { Licencias: lics };

          let dataLicUpdate = [];
          dataLicUpdate = [...dataLicUpdate, idUsuario, Licencias];

          //actualiza estados en RNC
          let actualizalic = await this.ingresoPostulanteService.updateLic(dataLicUpdate);
          if (actualizalic.respuesta.codigo == 0) {
            respuestaProceso = { respuestaProceso: 'OK' };
            let proceso = 3;
            await this.ingresoPostulanteService.insertatHistoricoCambiEstado(lics, dataConsulta, proceso);
          } else {
            respuestaProceso = { respuestaProceso: 'NOK' };
          }
        }

        console.log(respuestaProceso);

        return { datosrecepcionConf, respuestaProceso };
      }
    } catch (e) {
      console.log(e);
    }
  }
}
