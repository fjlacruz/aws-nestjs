import { ServiciosComunesService } from './../shared/services/ServiciosComunes/ServiciosComunes.services';
import { Module, forwardRef } from '@nestjs/common';
import { CambiosEstadoService } from './services/cambios-estado.service';
import { CambiosEstadoController } from './controller/cambios-estado.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocRespaldoCambioEstadoEntity } from './entities/doc-respaldo-cambio-estado.entity';
import { RegistroCambiosEstadoSolicitudEntity } from './entities/registro-cambios-estado-solicitud.entity';
import { RegistroCambiosEstadoTramiteEntity } from './entities/registro-cambios-estado-tramite.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { MatrizEstadosSolicitudEntity } from './entities/matriz-estados-solicitud.entity';
import { MatrizEstadosTramiteEntity } from './entities/matriz-estados-tramite.entity';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { AuthService } from 'src/auth/auth.service';
import { SharedModule } from 'src/shared/shared.module';
import { MatrizEstadosExaminacionEntity } from './entities/matriz-estados-examinacion.entity';
import { EstadosExaminacionEntity } from 'src/tramites/entity/estados-examinacion.entity';

@Module({
  imports:      [
                  SharedModule,
                  TypeOrmModule.forFeature([ 
                    DocRespaldoCambioEstadoEntity, 
                    RegistroCambiosEstadoSolicitudEntity, 
                    RegistroCambiosEstadoTramiteEntity, 
                    Solicitudes, 
                    TramitesEntity, 
                    TramitesClaseLicencia,
                    EstadosSolicitudEntity,
                    EstadosTramiteEntity,
                    MatrizEstadosSolicitudEntity,
                    MatrizEstadosTramiteEntity,
                    MatrizEstadosExaminacionEntity,
                    User,
                    RolesUsuarios,
                    EstadosTramiteEntity,
                    EstadosExaminacionEntity
                  ]),
                ],
  providers:    [CambiosEstadoService, AuthService],
  controllers:  [CambiosEstadoController],
  exports:      [CambiosEstadoService],
})
export class CambiosEstadoModule {}
