import { Test, TestingModule } from '@nestjs/testing';
import { CambiosEstadoController } from './cambios-estado.controller';

describe('CambiosEstadoController', () => {
  let controller: CambiosEstadoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CambiosEstadoController],
    }).compile();

    controller = module.get<CambiosEstadoController>(CambiosEstadoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
