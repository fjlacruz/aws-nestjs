import { Body, Controller, DefaultValuePipe, Delete, Get, Param, ParseIntPipe, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { DocRespaldoCambioEstadoDTO } from '../dto/doc-respaldo-cambio-estado.dto';
import { ExaminacionCambioEstado } from '../dto/examinacion-cambio-estado';
import { RegistroSolicitudCambioEstadoDto } from '../dto/registro-solicitud-cambio-estado.dto';
import { SolicitudCambioEstadoDTO } from '../dto/solicitud-cambio-estado.dto';
import { TramiteCambioEstadoDTO } from '../dto/tramite-cambio-estado.dto';
import { CambiosEstadoService } from '../services/cambios-estado.service';

@ApiTags('Cambios-Estado')
@ApiBearerAuth()
@Controller('cambios-estado')
export class CambiosEstadoController {
  constructor(private cambioEstadoService: CambiosEstadoService) {}

  @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes' })
  @Get('solicitudes')
  async solicitudes() {
    return await this.cambioEstadoService.getSolicitudes().then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes de prórrogas disponibles' })
  @Get('getSolicitudes')
  async getSolicitudesProrrogas(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {

    const data = await this.cambioEstadoService.getAllSolicitudes(
      req,
      query.idTipoExaminacion,
      query.RUN,
      query.Nombres,
      query.Nomclaselic,
      query.nombreTramite,
      query.estadoTramite,
      query.idEstadoSolicitud,
      query.fechaIngresoResultado,
      query.idSolicitud,
      query.idTramite,
      query.tipoTramite,
      {
        page,
        limit,
        route: '/cambios-estado/getAllProrrogas',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    return data;

  }

  @ApiOperation({ summary: 'Servicio que modifica el estado de la solicitud' })
  @Post('solicitud')
  async solicitudCambiarEstado(@Body() dto: any) {
    return await this.cambioEstadoService.postCambioEstado(dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que modifica el estado de la solicitud' })
  @Post('solicitudCambioEstado')
  async solicitudCambioEstadoEspecial(@Body() dto: RegistroSolicitudCambioEstadoDto, @Req() req: any) {
    return await this.cambioEstadoService.solicitudCambioEstadoEspecial(req, dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @Post('consultaNuevoEstadoSolicitudCambioEstado')
  async consultaNuevoEstadoSolicitudCambioEstadoEspecial(@Body() dto: RegistroSolicitudCambioEstadoDto, @Req() req: any) {
    return await this.cambioEstadoService.consultaNuevoEstadoSolicitudCambioEstadoEspecial(req, dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que trae los estados posibles para cambiar posibles de una solicitud' })
  @Get('solicitud/estados/:idEstado')
  async solicitudEstados(@Param('idEstado') id: number) {
    return await this.cambioEstadoService.getSolicitudEstados(Number(id)).then((data: any) => {
      return { data, code: 200 };
    });
  }
  @ApiOperation({ summary: 'Servicio que trae los estados posibles para cambiar posibles de un tramite' })
  @Get('tramite/estados/:idEstado')
  async tramiteEstados(@Param('idEstado') id: number) {
    return await this.cambioEstadoService.getTramiteEstados(Number(id)).then((data: any) => {
      return { data, code: 200 };
    });
  }
  @ApiOperation({ summary: 'Servicio que trae los estados posibles para cambiar posibles de un tramite' })
  @Get('examinacion/estados/:idEstado')
  async examinacionEstados(@Param('idEstado') id: number) {
    return await this.cambioEstadoService.getExaminacionEstados(Number(id)).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que modifica el estado de un tramite' })
  @Post('tramite')
  async tramiteCambiarEstado(@Body() dto: TramiteCambioEstadoDTO) {
    return await this.cambioEstadoService.postTramiteCambiarEstado(dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que modifica el estado de una examinacion' })
  @Post('examinacion')
  async examinacionCambiarEstado(@Body() dto: ExaminacionCambioEstado) {
    return await this.cambioEstadoService.postExaminacionCambiarEstado(dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que trae los tramites de una solicitud' })
  @Get('tramites/:idSolicitud')
  async tramites(@Param('idSolicitud') id: number) {
    return await this.cambioEstadoService.getTramites(Number(id)).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que adjunta un archivo de para respaldar el cambio de estado de la solicitud' })
  @Post('documento')
  async documentoAgregar(@Body() dto: DocRespaldoCambioEstadoDTO) {
    return await this.cambioEstadoService.postDocumento(dto).then((data: any) => {
      return { data, code: 200 };
    });
  }

  @ApiOperation({ summary: 'Servicio que elimina un archivo para respaldar el cambio de estado' })
  @Delete('documento/:idDocumento')
  async documentoEliminar(@Param('idDocumento') id: number) {
    return await this.cambioEstadoService.deleteDocumento(id).then((data: any) => {
      return { data, code: 200 };
    });
  }
}
