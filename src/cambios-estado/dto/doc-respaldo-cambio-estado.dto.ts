import { ApiPropertyOptional } from "@nestjs/swagger";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

export class DocRespaldoCambioEstadoDTO
{
    @ApiPropertyOptional()
    IdRegistroCambiosEstadoSolicitud: number;

    @ApiPropertyOptional()
    nombreDoc: string;
    
    @ApiPropertyOptional()
    archivo: Buffer;
    
    // @ApiPropertyOptional()
    // idUsuario: number;
}