import { ApiPropertyOptional } from "@nestjs/swagger";

export class ExaminacionCambioEstado 
{
  @ApiPropertyOptional()
  idColaExaminacion: number;
  
  @ApiPropertyOptional()
  idEstadosExaminacionPrevio: number;
  
  @ApiPropertyOptional()
  idEstadosExaminacionNuevo: number;
  
  // @ApiPropertyOptional()
  // idUsuario: number;
  
  @ApiPropertyOptional()
  observacion: string;
  
  // @ApiPropertyOptional()
  // informadoSRCeI: boolean;
 
  // @ApiPropertyOptional()
  // manual: boolean;
}

