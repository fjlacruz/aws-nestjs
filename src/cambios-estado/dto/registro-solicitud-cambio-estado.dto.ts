export class RegistroSolicitudCambioEstadoDto {
    IdSolicitudCambioEstado: number;
    Observaciones: string;

    CambiosEstadosTramites: SolicitudCambioEstadoTramites[];
    CambiosEstadosExaminaciones: SolicitudCambioEstadoExaminaciones[];
    CambiosEstadosDocumentos: SolicitudCambioEstadoDocumentos[];
}

export class SolicitudCambioEstadoTramites{
    IdTramiteParaCambiar: number;
    IdNuevoEstadoTramite?: number;
}

export class SolicitudCambioEstadoExaminaciones{
    IdExaminacionParaCambiar: number;
    IdNuevoEstadoExaminacionParaCambiar?: number;
}

export class SolicitudCambioEstadoDocumentos{
    NombreDocumento: string;
    Binario: Buffer;
}