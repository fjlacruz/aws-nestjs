import { Column } from "typeorm";

export class SolicitudCambioEstadoValidacionDto
{
    idSolicitud: number;

    @Column()
    idTipoSolicitud: number;
  
    @Column()
    idPostulante: number;
  
    @Column({
      nullable: true,
    })
    idUsuario: number;
  
    @Column({
      nullable: true,
    })
    idOficina: number;
  
    @Column({
      nullable: true,
    })
    idEstado: number;

    idEstadoNuevo: number;
  
    @Column({
        nullable: true,
      })
    codigoEstado: string;

    @Column({
      nullable: true,
    })
    idInstitucion: number;
  
    @Column({
      nullable: true,
    })
    FechaCreacion: Date;
  
    @Column({
      nullable: true,
    })
    updated_at: Date;
  
    @Column({
      nullable: true,
    })
    fechaVencimiento: Date;
  
    @Column({
      nullable: true,
    })
    FechaCierre: Date;

    @Column({
      nullable: true,
    })
    DescripcionEstado: string;    

    CambioEstadoTramitesValidacion : SolicitudCambioEstadoTramiteValidacionDto[];   
}

export class SolicitudCambioEstadoTramiteValidacionDto{
    @Column({
        nullable: true,
    })
    idTramite: number;

    @Column({
        nullable: true,
    })

    idEstado: number;

    idEstadoPrevio: number;
    
    @Column({
        nullable: true,
    })
    codigoEstado: string;

    @Column({
      nullable: true,
    })
    codigoEstadoPrevio: string;

    CambioEstadoExaminacionValidacion : SolicitudCambioEstadoExaminacionValidacionDto[];
}

export class SolicitudCambioEstadoExaminacionValidacionDto{
    @Column({
        nullable: true,
    })
    idExaminacion: number;  

    @Column({
        nullable: true,
    })
    idEstado: number;

    @Column({
      nullable: true,
    })
    idEstadoPrevio: number;

    @Column({
        nullable: true,
    })
    codigoEstado: string;  

    @Column({
        nullable: true,
    })
    codigoEstadoPrevio: string;    
}