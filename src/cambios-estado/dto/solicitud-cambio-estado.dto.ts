import { ApiPropertyOptional } from '@nestjs/swagger';

export class SolicitudCambioEstadoDTO {
  @ApiPropertyOptional()
  idSolicitud: number;

  @ApiPropertyOptional()
  idEstadoSolicitudPrevio: number;

  @ApiPropertyOptional()
  idEstadoSolicitud: number;

  // @ApiPropertyOptional()
  // idUsuario: number;

  @ApiPropertyOptional()
  observacion: string;

  // @ApiPropertyOptional()
  // informadoSRCeI: boolean;

  // @ApiPropertyOptional()
  // manual: boolean;
}
