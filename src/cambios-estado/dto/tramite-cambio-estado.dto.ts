import { ApiPropertyOptional } from "@nestjs/swagger";

export class TramiteCambioEstadoDTO 
{
  @ApiPropertyOptional()
  idTramite: number;
  
  @ApiPropertyOptional()
  idEstadoTramitePrevio: number;
  
  @ApiPropertyOptional()
  idEstadoTramite: number;
  
  // @ApiPropertyOptional()
  // idUsuario: number;
  
  @ApiPropertyOptional()
  observacion: string;
  
  // @ApiPropertyOptional()
  // informadoSRCeI: boolean;
 
  // @ApiPropertyOptional()
  // manual: boolean;
}

