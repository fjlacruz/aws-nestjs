import { ApiPropertyOptional } from "@nestjs/swagger";
import { SolicitudExaminacionesDTO, SolicitudTramitesDTO } from "src/prorrogas/dto/prorrogas-listado.dto";

export class CambioEstadoSolicitudEntity {
    constructor(){}

    @ApiPropertyOptional()
    IdSolicitud: number

    @ApiPropertyOptional()
    Run: Number;

    @ApiPropertyOptional()
    RunFormateado: string;    

    @ApiPropertyOptional()
    NombrePostulante: string;

    @ApiPropertyOptional()
    Tramites: SolicitudTramitesDTO[];

    @ApiPropertyOptional()
    Examinaciones: SolicitudExaminacionesDTO[];

    @ApiPropertyOptional()
    ClasesLicencias: string[];

    @ApiPropertyOptional()
    FechaInicio: Date;

    @ApiPropertyOptional()
    FechaModificacion: Date;

    @ApiPropertyOptional()
    FechaVencimiento: Date;

    @ApiPropertyOptional()
    EstadoExaminacion: boolean;

    @ApiPropertyOptional()
    EstadoAlerta: string;

    @ApiPropertyOptional()
    UltimaModificacion: Date;

    @ApiPropertyOptional()
    IdEstadoSolicitud: number;

    @ApiPropertyOptional()
    EstadoSolicitudString: string;
}

