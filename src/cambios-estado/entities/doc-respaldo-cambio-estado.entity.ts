import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'docRespaldoCambioEstado' )
export class DocRespaldoCambioEstadoEntity
{
    @PrimaryGeneratedColumn()
    idDocRespaldoCambioEstado: number;

    @Column()
    IdRegistroCambiosEstadoSolicitud: number;

    @Column()
    nombreDoc: string;
    
    @Column({ type: 'bytea' })
    archivo: Buffer;
    
    @Column()
    created_at: Date;
    
    @Column()
    idUsuario: number;
    
}