import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'MatrizEstadosExaminacion' )
export class MatrizEstadosExaminacionEntity {
    @PrimaryGeneratedColumn()
    idMatrizEstadosExaminacion: number;

    @Column()
    idEstadoExaminacionOrigen: number;

    @Column()
    idEstadoExaminacionDestino: number;

    @Column()
    permitido: boolean;
}
