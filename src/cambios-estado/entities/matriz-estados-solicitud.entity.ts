import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'MatrizEstadosSolicitud' )
export class MatrizEstadosSolicitudEntity
{
    @PrimaryGeneratedColumn()
    idMatrizEstadosSolicitud: number;

    @Column()
    IdEstadoSolicitudOrigen: number;

    @Column()
    IdEstadoSolicitudDestino: number;

    @Column()
    permitido: boolean;
}