import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'MatrizEstadosTramite' )
export class MatrizEstadosTramiteEntity
{
    @PrimaryGeneratedColumn()
    idMatrizEstadosTramite: number;

    @Column()
    IdEstadoTramiteOrigen: number;

    @Column()
    IdEstadoTramiteDestino: number;

    @Column()
    permitido: boolean;
}