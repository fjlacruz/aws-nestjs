import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'RegistroCambiosEstadoExaminaciones' )
export class RegistroCambiosEstadoExaminacion {
    @PrimaryGeneratedColumn()
    idRegistroCambiosEstadoExaminacion: number;
    
    @Column()
    idColaExaminacion: number;
    
    @Column()
    RealizadoPor: number;

    @Column()
    idEstadosExaminacionPrevio: number;
    
    @Column()
    idEstadosExaminacionNuevo: number;
        
    @Column()
    idRegistroCambiosEstadoTramite: string;

    @Column()
    created_at: Date;
}
