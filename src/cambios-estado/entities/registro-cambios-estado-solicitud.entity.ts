import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('RegistroCambiosEstadoSolicitud')
export class RegistroCambiosEstadoSolicitudEntity {
  @PrimaryGeneratedColumn()
  IdRegistroCambiosEstadoSolicitud: number;

  @Column()
  idSolicitud: number;

  @Column()
  idEstadoSolicitudPrevio: number;

  @Column()
  idEstadoSolicitud: number;

  @Column()
  idUsuario: number;

  @Column()
  created_at: Date;

  @Column()
  observacion: string;

  @Column()
  informadoSRCeI: boolean;

  @Column()
  manual: boolean;
}
