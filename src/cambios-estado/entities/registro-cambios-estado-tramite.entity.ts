import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('RegistroCambiosEstadoTramite')
export class RegistroCambiosEstadoTramiteEntity {
  @PrimaryGeneratedColumn()
  IdRegistroCambiosEstadoTramite: number;

  @Column()
  IdRegistroCambiosEstadoSolicitud: number;

  @Column()
  idTramite: number;

  @Column()
  idEstadoTramitePrevio: number;

  @Column()
  idEstadoTramite: number;

  @Column()
  idUsuario: number;

  @Column()
  created_at: Date;

  @Column()
  observacion: string;

  @Column()
  informadoSRCeI: boolean;

  @Column()
  fechaRespuestaSRCeI: Date;

  @Column()
  manual: boolean;
}
