import { Test, TestingModule } from '@nestjs/testing';
import { CambiosEstadoService } from './cambios-estado.service';

describe('CambiosEstadoService', () => {
  let service: CambiosEstadoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CambiosEstadoService],
    }).compile();

    service = module.get<CambiosEstadoService>(CambiosEstadoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
