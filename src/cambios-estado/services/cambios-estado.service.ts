import { RegistroCambiosEstadoExaminacion } from './../entities/registro-cambios-estado-examinacion.entity';
import { ColaExaminacionEntity } from './../../cola-examinacion/entity/cola-examinacion.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { AuthService } from 'src/auth/auth.service';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { SolicitudExaminacionesDTO, SolicitudTramitesDTO } from 'src/prorrogas/dto/prorrogas-listado.dto';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { validarStringEsEntero } from 'src/shared/Common/ValidadoresFormato';
import { ServiciosComunesService } from 'src/shared/services/ServiciosComunes/ServiciosComunes.services';
import { formatearRun } from 'src/shared/ValidarRun';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { ColaExaminacionTramiteNMEntity } from 'src/tramites/entity/cola-examinacion-tramite-n-m.entity';
import { ColaExaminacion } from 'src/tramites/entity/cola-examinacion.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { Resultado } from 'src/utils/resultado';
import { EntityManager, getManager, Repository } from 'typeorm';
import { DocRespaldoCambioEstadoDTO } from '../dto/doc-respaldo-cambio-estado.dto';
import { ExaminacionCambioEstado } from '../dto/examinacion-cambio-estado';
import { SolicitudCambioEstadoDTO } from '../dto/solicitud-cambio-estado.dto';
import { TramiteCambioEstadoDTO } from '../dto/tramite-cambio-estado.dto';
import { CambioEstadoSolicitudEntity } from '../entities/cambio-estado-solicitud-entity';
import { DocRespaldoCambioEstadoEntity } from '../entities/doc-respaldo-cambio-estado.entity';
import { MatrizEstadosSolicitudEntity } from '../entities/matriz-estados-solicitud.entity';
import { MatrizEstadosTramiteEntity } from '../entities/matriz-estados-tramite.entity';
import { RegistroCambiosEstadoSolicitudEntity } from '../entities/registro-cambios-estado-solicitud.entity';
import { RegistroCambiosEstadoTramiteEntity } from '../entities/registro-cambios-estado-tramite.entity';
import { EstadosExaminacionEntity } from 'src/tramites/entity/estados-examinacion.entity';
import { MatrizEstadosExaminacionEntity } from '../entities/matriz-estados-examinacion.entity';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import {
  SolicitudCambioEstadoExaminacionValidacionDto,
  SolicitudCambioEstadoTramiteValidacionDto,
  SolicitudCambioEstadoValidacionDto,
} from '../dto/solicitud-cambio-estado-validacion';
import { RegistroSolicitudCambioEstadoDto } from '../dto/registro-solicitud-cambio-estado.dto';
import {
  ESTADO_EXAM_ALERTA_PRE_IDON_MORAL_CODE,
  ESTADO_EXAM_APROBADO_IDON_MORAL_CODE,
  ESTADO_EXAM_APROBADO_IDON_MORAL_ID,
  ESTADO_EXAM_APROBADO_IDON_MORAL_JPL_CODE,
  ESTADO_EXAM_APROBADO_IDON_MORAL_JPL_ID,
  ESTADO_EXAM_APROBADO_MEDICA_CODE,
  ESTADO_EXAM_APROBADO_MEDICA_ID,
  ESTADO_EXAM_APROBADO_MEDICA_SML_CODE,
  ESTADO_EXAM_APROBADO_MEDICA_SML_ID,
  ESTADO_EXAM_APROBADO_PRACTICA_CODE,
  ESTADO_EXAM_APROBADO_PRACTICA_ID,
  ESTADO_EXAM_APROBADO_TEORICA_CODE,
  ESTADO_EXAM_APROBADO_TEORICA_ID,
  ESTADO_EXAM_EN_CURSO_IDON_MORAL_CODE,
  ESTADO_EXAM_EN_CURSO_IDON_MORAL_ID,
  ESTADO_EXAM_EN_CURSO_MEDICA_CODE,
  ESTADO_EXAM_EN_CURSO_MEDICA_ID,
  ESTADO_EXAM_EN_CURSO_PRACTICA_CODE,
  ESTADO_EXAM_EN_CURSO_PRACTICA_ID,
  ESTADO_EXAM_EN_CURSO_PRE_IDON_MORAL_CODE,
  ESTADO_EXAM_EN_CURSO_TEORICA_CODE,
  ESTADO_EXAM_EN_CURSO_TEORICA_ID,
  ESTADO_EXAM_EN_EVALUACION_IDON_MORAL_CODE,
  ESTADO_EXAM_EN_EVALUACION_IDON_MORAL_ID,
  ESTADO_EXAM_MAS_ANTECEDENTES_MEDICA_CODE,
  ESTADO_EXAM_MAS_ANTECEDENTES_MEDICA_ID,
  ESTADO_EXAM_NO_ALERTA_PRE_IDON_MORAL_CODE,
  ESTADO_EXAM_PENDIENTE_IDON_MORAL_CODE,
  ESTADO_EXAM_PENDIENTE_IDON_MORAL_ID,
  ESTADO_EXAM_PENDIENTE_MEDICA_CODE,
  ESTADO_EXAM_PENDIENTE_MEDICA_ID,
  ESTADO_EXAM_PENDIENTE_PRACTICA_CODE,
  ESTADO_EXAM_PENDIENTE_PRACTICA_ID,
  ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE,
  ESTADO_EXAM_PENDIENTE_TEORICA_CODE,
  ESTADO_EXAM_PENDIENTE_TEORICA_ID,
  ESTADO_EXAM_REPROBADO_IDON_MORAL_CODE,
  ESTADO_EXAM_REPROBADO_IDON_MORAL_ID,
  ESTADO_EXAM_REPROBADO_MEDICA_CODE,
  ESTADO_EXAM_REPROBADO_MEDICA_ID,
  ESTADO_EXAM_REPROBADO_PRACTICA_1_OPORT_CODE,
  ESTADO_EXAM_REPROBADO_PRACTICA_1_OPORT_ID,
  ESTADO_EXAM_REPROBADO_PRACTICA_2_OPORT_CODE,
  ESTADO_EXAM_REPROBADO_PRACTICA_2_OPORT_ID,
  ESTADO_EXAM_REPROBADO_PRACTICA_NO_ASIST_2_OPORT_CODE,
  ESTADO_EXAM_REPROBADO_PRACTICA_NO_ASIST_2_OPORT_ID,
  ESTADO_EXAM_REPROBADO_TEORICA_1_OPORT_CODE,
  ESTADO_EXAM_REPROBADO_TEORICA_1_OPORT_ID,
  ESTADO_EXAM_REPROBADO_TEORICA_2_OPORT_CODE,
  ESTADO_EXAM_REPROBADO_TEORICA_2_OPORT_ID,
  ESTADO_EXAM_REPROBADO_TEORICA_NO_ASIST_2_OPORT_CODE,
  ESTADO_EXAM_REPROBADO_TEORICA_NO_ASIST_2_OPORT_ID,
  ESTADO_SOLICITUD_ABIERTA_CODE,
  ESTADO_SOLICITUD_BORRADOR_CODE,
  ESTADO_SOLICITUD_CERRADA_CODE,
  ESTADO_SOLICITUD_HORA_RESERVADA_CODE,
  ESTADO_SOLICITUD_HORA_RESERVAD_FI_ATC_ENVDOS_CODE,
  ESTADO_SOLICITUD_INCUMPLIMIENTO_REQUISITOS_CODE,
  ESTADO_SOLICITUD_MESON_CODE,
  ESTADO_SOLICITUD_REABIERTA_RECONSID_JPL_SML_CODE,
  ESTADO_TRAMITE_ABANDONADO_CODE,
  ESTADO_TRAMITE_BORRADOR_CODE,
  ESTADO_TRAMITE_CANCELADO_CERRADO_CODE,
  ESTADO_TRAMITE_DENEGACION_INFORMADA,
  ESTADO_TRAMITE_DENEGACION_INFORMADA_JPL_CODE,
  ESTADO_TRAMITE_DENEGACION_INFORMADA_SML_CODE,
  ESTADO_TRAMITE_DESISTIDO_CODE,
  ESTADO_TRAMITE_ENVIO_CORREO_DENEG_EXAM_TEORC_CODE,
  ESTADO_TRAMITE_EN_ESPERA_REGISTRO_CIVIL_CODE,
  ESTADO_TRAMITE_ESPERA_INFORMAR_DENEGACION_CODE,
  ESTADO_TRAMITE_ESPERA_INFORMAR_ORTORGAMIENTO_CODE,
  ESTADO_TRAMITE_INICIADO_CODE,
  ESTADO_TRAMITE_OTORGAMIENTO_INFORMADO_IMPRES_CODE,
  ESTADO_TRAMITE_RECEPCION_CONFORME,
  ESTADO_TRAMITE_RECEPCION_NO_CONFORME,
  ESTADO_TRAMITE_REINICIADO_RECONSIDER_JPL_SML_CODE,
} from 'src/shared/Constantes/constantesEstadosExaminacion';
import { PermisosNombres } from 'src/constantes';

@Injectable()
export class CambiosEstadoService {
  constructor(
    private readonly authService: AuthService,

    @InjectRepository(RegistroCambiosEstadoSolicitudEntity)
    private readonly cambioEstadoSolicitudRepository: Repository<RegistroCambiosEstadoSolicitudEntity>,

    @InjectRepository(RegistroCambiosEstadoTramiteEntity)
    private readonly cambioEstadoTramiteRepository: Repository<RegistroCambiosEstadoTramiteEntity>,

    @InjectRepository(DocRespaldoCambioEstadoEntity)
    private readonly docCambioEstadoRepository: Repository<DocRespaldoCambioEstadoEntity>,

    @InjectRepository(Solicitudes) private readonly solicitudesEntityRespository: Repository<Solicitudes>,

    @InjectRepository(TramitesEntity)
    private readonly tramitesRepository: Repository<TramitesEntity>,

    @InjectRepository(TramitesClaseLicencia)
    private readonly tramitesLicenciaRepository: Repository<TramitesClaseLicencia>,

    @InjectRepository(EstadosSolicitudEntity)
    private readonly estadoSolicitudRepository: Repository<EstadosSolicitudEntity>,

    @InjectRepository(EstadosTramiteEntity)
    private readonly estadoTramiteRepository: Repository<EstadosTramiteEntity>,

    @InjectRepository(EstadosExaminacionEntity)
    private readonly estadoExaminacionRepository: Repository<EstadosExaminacionEntity>,

    @InjectRepository(MatrizEstadosSolicitudEntity)
    private readonly matrizEstadoSolicitudRepository: Repository<MatrizEstadosSolicitudEntity>,

    @InjectRepository(MatrizEstadosTramiteEntity)
    private readonly matrizEstadoTramiteRepository: Repository<MatrizEstadosTramiteEntity>,
    private serviciosComunesService: ServiciosComunesService,

    @InjectRepository(MatrizEstadosExaminacionEntity)
    private readonly matrizEstadoExaminacionRepository: Repository<MatrizEstadosExaminacionEntity>
  ) {}

  async getAllSolicitudes(
    req:any,
    idTipoExaminacion: string,
    run?: string,
    nombres?: string,
    claseLicencia?: string,
    nombreTramite?: string,
    estadoTramite?: string,
    estadoSolicitud?: string,
    fechaCreacion?: Date,
    idSolicitud?: string,
    idTramite?: string,
    tipoTramite?: string,
    options?: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ): Promise<any> {
    let res: Resultado = new Resultado();

    try {
      
      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaModificacionesEspecialesCambiosDeEstados);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      // Comprobación de tipos numéricos
      if (
        (idTipoExaminacion && !validarStringEsEntero(idTipoExaminacion)) ||
        (claseLicencia && !validarStringEsEntero(claseLicencia)) ||
        (nombreTramite && !validarStringEsEntero(nombreTramite)) ||
        (estadoTramite && !validarStringEsEntero(estadoTramite)) ||
        (tipoTramite && !validarStringEsEntero(tipoTramite)) ||
        (idSolicitud && !validarStringEsEntero(idSolicitud))
      ) {
        res.Error = 'Existe un error en los datos enviados, por favor revise los tipos de datos enviados.';
        res.ResultadoOperacion = false;
        res.Respuesta = [];

        return res;
      }

      //let resultadoHistorial: PostulanteHistorialIdoneidadMoral[] = []

      const qbSolicitudes = this.solicitudesEntityRespository
        .createQueryBuilder('Solicitudes')
        .innerJoinAndMapOne('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne(
          'Solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'estadosSolicitud.idEstado = Solicitudes.idEstado'
        )
        .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        )
        .innerJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .innerJoinAndMapOne(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacion,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne(
          'colaExaminacion.estadosExaminacion',
          estadosExaminacionEntity,
          'estadosExaminaciones',
          'colaExaminacion.idEstadosExaminacion = estadosExaminaciones.idEstadosExaminacion'
        )
        .innerJoinAndMapOne(
          'colaExaminacion.tipoExaminaciones',
          TipoExaminacionesEntity,
          'tipoExaminaciones',
          'colaExaminacion.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion'
        )
        .andWhere('(Solicitudes.idEstado != 1)');

      // Filtros
      if (run) {
        const rut = run.split('-');
        let runsplited: string = rut[0];
        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');
        const div = rut[1];
        qbSolicitudes.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbSolicitudes.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }
      if (nombres) {
        qbSolicitudes.andWhere(
          '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${nombres}%`,
          }
        );
      }

      if (claseLicencia)
        qbSolicitudes.andWhere('clasesLicencias.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: claseLicencia,
        });

      if (fechaCreacion) qbSolicitudes.andWhere('DATE(Solicitudes.FechaCreacion) = :created_at', { created_at: fechaCreacion });

      //Filtro por estado de trámite
      if (estadoTramite)
        qbSolicitudes.andWhere('tramites.idEstadoTramite = :_idEstadoTramite', {
          _idEstadoTramite: estadoTramite,
        });

      if (estadoSolicitud)
        qbSolicitudes.andWhere('estadosSolicitud.idEstado = :estadoSolicitud', {
          estadoSolicitud: estadoSolicitud,
        });

      //Filtro por tipo de tramite
      if (nombreTramite)
        qbSolicitudes.andWhere('tramites.idTipoTramite = :_idEstadosExaminacion', {
          _idEstadosExaminacion: nombreTramite,
        });

      // Filtrar por id solicitud
      if (idSolicitud)
        qbSolicitudes.andWhere('Solicitudes.idSolicitud = :_idSolicitud', {
          _idSolicitud: idSolicitud,
      });

      if (idTramite)
        qbSolicitudes.andWhere('tramites.idTramite = :idTramite', {
          idTramite: idTramite,
      });

      if (tipoTramite)
        qbSolicitudes.andWhere('tramites.idTipoTramite = :tipoTramite', {
          tipoTramite: tipoTramite,
      });      

      if (orden === 'idSolicitud' && ordenarPor === 'ASC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'ASC');
      if (orden === 'idSolicitud' && ordenarPor === 'DESC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'DESC');
      if (orden === 'estadoSolicitud' && ordenarPor === 'ASC') qbSolicitudes.orderBy('estadosSolicitud.Nombre', 'ASC');
      if (orden === 'estadoSolicitud' && ordenarPor === 'DESC') qbSolicitudes.orderBy('estadosSolicitud.Nombre', 'DESC');
      if (orden === 'run' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.RUN', 'ASC');
      if (orden === 'run' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.RUN', 'DESC');
      if (orden === 'Nombre' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.Nombres', 'ASC');
      if (orden === 'Nombre' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.Nombres', 'DESC');
      if (orden === 'idTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('colaExaminacion.idTramite', 'ASC');
      if (orden === 'idTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('colaExaminacion.idTramite', 'DESC');
      if (orden === 'TipoTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('tramites.idTipoTramite', 'ASC');
      if (orden === 'TipoTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('tramites.idTipoTramite', 'DESC');
      if (orden === 'estadoTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('estadoTramite.Nombre', 'ASC');
      if (orden === 'estadoTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('estadoTramite.Nombre', 'DESC');
      if (orden === 'clase' && ordenarPor === 'ASC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'ASC');
      if (orden === 'clase' && ordenarPor === 'DESC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'DESC');
      if (orden === 'fechaTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('Solicitudes.FechaCreacion', 'ASC');
      if (orden === 'fechaTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('Solicitudes.FechaCreacion', 'DESC');

      //const resultadoSolicitudes: Solicitudes[] = await qbSolicitudes.getMany();
      const resultadoSolicitudes: any = await paginate<Solicitudes>(qbSolicitudes, options);

      // Inicializamos el listado
      let listadoSolicitudesDTO: CambioEstadoSolicitudEntity[] = [];

      // Transformacion al DTO
      resultadoSolicitudes.items.forEach(sol => {
        let resRecorrido: Solicitudes = sol as Solicitudes;

        let plDTO: CambioEstadoSolicitudEntity = new CambioEstadoSolicitudEntity();

        plDTO.IdSolicitud = resRecorrido.idSolicitud;
        plDTO.EstadoSolicitudString = resRecorrido.estadosSolicitud.Nombre;
        plDTO.IdEstadoSolicitud = resRecorrido.idEstado;

        plDTO.Tramites = resRecorrido.tramites.map((t: TramitesEntity) => {
          const temp: SolicitudTramitesDTO = {
            IdTramite: t.idTramite,
            IdTipoTramite: t.idTipoTramite,
            idEstadoTramite: t.estadoTramite.idEstadoTramite,
            EstadoTramiteString: t.estadoTramite.Nombre,
            TipoTramiteString: t.tiposTramite.Nombre,
            FechaTramite: t.created_at,
            IdEstadoTramite: t.idEstadoTramite,
            UrgenciaTramite: null,
            FechaExpiracionTramite: null,
            FechaUltimaModificacion: null,
          };

          return temp;
        });

        plDTO.Run = +(resRecorrido.postulante.RUN + '' + resRecorrido.postulante.DV);
        plDTO.RunFormateado = formatearRun(resRecorrido.postulante.RUN + '-' + resRecorrido.postulante.DV);
        plDTO.ClasesLicencias = resRecorrido.tramites.map(t => t.tramitesClaseLicencia.clasesLicencias.Abreviacion);
        plDTO.NombrePostulante =
          resRecorrido.postulante.Nombres + ' ' + resRecorrido.postulante.ApellidoPaterno + ' ' + resRecorrido.postulante.ApellidoMaterno;

        //Guardamos todos los ids de las colas de examinación, dentro se repetirán los que estén más de una vez (se usará para asignar abreviación de exámen y comprobar
        // si ya se ha insertado)
        let colasExaminacionesIds: number[] = [];

        resRecorrido.tramites.forEach(z => {
          z.colaExaminacionNM.forEach(y => {
            if (y.colaExaminacion.idTipoExaminacion != 6) {
              // Descartamos preidoneidad moral
              colasExaminacionesIds.push(y.idColaExaminacion);
            }
          });
        });

        //Se inicializa el array de cola examinación dtos
        let listadoSolExamDTO: SolicitudExaminacionesDTO[] = [];

        // Booleano para indicar si se debe asociar las clases de licencia
        let asignarAbreviacionClaseExaminacion: Boolean = false;

        resRecorrido.tramites.forEach(x => {
          // Primero sacamos la clase de licencia
          let claseAbreviacionTramite = x.tramitesClaseLicencia.clasesLicencias.Abreviacion;

          // Segundo las examinaciones asociadas al trámite
          x.colaExaminacionNM.forEach((cExam: any) => {
            // Inicializamos el DTO
            let solExamDTO: SolicitudExaminacionesDTO = new SolicitudExaminacionesDTO();

            // Comprobamos primero si no existe en el listado para agregarlo (en caso contrario ya fue agregado)
            if (
              cExam.colaExaminacion.idTipoExaminacion != 6 &&
              (listadoSolExamDTO.length == 0 || !listadoSolExamDTO.some(x => x.IdExaminacion == cExam.idColaExaminacion))
            ) {
              solExamDTO.IdExaminacion = cExam.idColaExaminacion;
              solExamDTO.EstadoExaminacionString = cExam.colaExaminacion.estadosExaminacion.nombreEstado;
              solExamDTO.IdEstadoExaminacion = cExam.colaExaminacion.idEstadosExaminacion;
              solExamDTO.TipoExaminacionString = cExam.colaExaminacion.tipoExaminaciones.nombreExaminacion;
              solExamDTO.AbreviacionClaseLicenciaAsocString = claseAbreviacionTramite;
              solExamDTO.IdTramitesAsociados = [];
              solExamDTO.IdTramitesAsociados.push(x.idTramite);

              //En el caso de existir más de una cola examinación asociada, procedemos a añadir la distinción
              if (colasExaminacionesIds.filter(x => x == cExam.idColaExaminacion).length > 1) {
                solExamDTO.AbreviacionClaseLicenciaAsocString = null;
                asignarAbreviacionClaseExaminacion = true;
              }

              // Agregamos a la lista
              listadoSolExamDTO.push(solExamDTO);
            }
            // En caso de que lo hayamos encontrado, procedemos sólo a agregar un trámite adicional asociado a la examinación
            else if (listadoSolExamDTO.some(x => x.IdExaminacion == cExam.idColaExaminacion)) {
              let solExamAgregarTramite: SolicitudExaminacionesDTO = listadoSolExamDTO.find(
                x => x.IdExaminacion == cExam.idColaExaminacion
              );
              solExamAgregarTramite.IdTramitesAsociados.push(x.idTramite);
            }
          });
        });

        //En esta linea se terminó la inserción de examinaciones de un trámite, comprobamos para resetear a null si no es necesario indicar la abreviación
        if (asignarAbreviacionClaseExaminacion) {
          listadoSolExamDTO.forEach(lseDTO => {
            lseDTO.AbreviacionClaseLicenciaAsocString = null;
          });
        }

        plDTO.Examinaciones = listadoSolExamDTO;

        plDTO.FechaInicio = resRecorrido.FechaCreacion;
        plDTO.FechaVencimiento = resRecorrido.fechaVencimiento;
        plDTO.UltimaModificacion = resRecorrido.updated_at;

        // Se agrega el elemento a la lista
        listadoSolicitudesDTO.push(plDTO);
      });

      // Se devuelve resultado correcto
      resultadoSolicitudes.items = listadoSolicitudesDTO;

      res.ResultadoOperacion = true;
      res.Respuesta = resultadoSolicitudes;

      return res;
    } catch (Error) {
      res.Error = 'Ha ocurrido un error en la respuesta, por favor consulte con el administrador de la aplicación.';
      res.ResultadoOperacion = false;
      res.Respuesta = [];

      return res;
    }
  }

  async getSolicitudes() {
    // const relations =   ["clasesLicencias", "tramite", "tramite.tiposTramite", "tramite.estadoTramite", "tramite.solicitudes", "tramite.solicitudes.postulante",
    //                     "tramite.solicitudes.estadosSolicitud"];
    // return await this.tramitesLicenciaRepository.find({ relations });
    const idOficina = this.authService.oficina().idOficina;
    const solicitudes = await this.tramitesLicenciaRepository
      .createQueryBuilder('traLic')
      .innerJoinAndSelect('traLic.clasesLicencias', 'clasesLicencias')
      .innerJoinAndSelect('traLic.tramite', 'tramite')
      .innerJoinAndSelect('tramite.tiposTramite', 'tiposTramite')
      .innerJoinAndSelect('tramite.solicitudes', 'solicitudes')
      .innerJoinAndSelect('tramite.estadoTramite', 'estadoTramite')
      .innerJoinAndSelect('solicitudes.postulante', 'postulante')
      .innerJoinAndSelect('solicitudes.estadosSolicitud', 'estadosSolicitud')
      .where('solicitudes.idOficina = :idOficina', { idOficina })
      .getMany();

    return solicitudes;
  }

  async getSolicitudEstados(idEstado: number) {
    const estados = await this.matrizEstadoSolicitudRepository
      .createQueryBuilder('matriz')
      .select('estado.idEstado', 'idEstado')
      .addSelect('estado.Nombre', 'Nombre')
      .addSelect('estado.Descripcion', 'Descripcion')
      .innerJoin(EstadosSolicitudEntity, 'estado', 'estado.idEstado = matriz.IdEstadoSolicitudDestino')
      .where('matriz.IdEstadoSolicitudOrigen = :idEstado', { idEstado })
      .andWhere("matriz.permitido = 'true'")
      .getRawMany();

    return estados;
  }
  async getTramiteEstados(idEstado: number) {
    const estados = await this.matrizEstadoTramiteRepository
      .createQueryBuilder('matriz')
      .select('estado.idEstadoTramite', 'idEstadoTramite')
      .addSelect('estado.Nombre', 'Nombre')
      .addSelect('estado.Descripcion', 'Descripcion')
      .innerJoin(EstadosTramiteEntity, 'estado', 'estado.idEstadoTramite = matriz.IdEstadoTramiteDestino')
      .where('matriz.IdEstadoTramiteOrigen = :idEstado', { idEstado })
      .andWhere("matriz.permitido = 'true'")
      .getRawMany();

      return estados;
      // const estadoDesistido = await this.estadoTramiteRepository
      // .createQueryBuilder('estado')
      // .select('estado.idEstadoTramite', 'idEstadoTramite')
      // .addSelect('estado.Nombre', 'Nombre')
      // .addSelect('estado.Descripcion', 'Descripcion')
      // .where('estado.idEstadoTramite = 20')
      // .getRawMany();
      // if(estados.length != 0){ //Si el estado no es terminal, añade el estadoDesistido
      //   const allEstados = estados.concat(estadoDesistido)
      //   return allEstados;
      // }else{
      //   return estados;
      // }
  }

  async getExaminacionEstados(idEstado: number) {
    const estados = await this.matrizEstadoExaminacionRepository
      .createQueryBuilder('matriz')
      .select('estado.idEstadosExaminacion', 'idEstadoExaminacion')
      .addSelect('estado.nombreEstado', 'Nombre')
      .addSelect('estado.descripcion', 'Descripcion')
      .innerJoin(estadosExaminacionEntity, 'estado', 'matriz.idEstadoExaminacionDestino = estado.idEstadosExaminacion')
      .where('matriz.idEstadoExaminacionOrigen = :idEstado', { idEstado })
      .andWhere("matriz.permitido = 'true'")
      .getRawMany();

    return estados;
  }

  async getTramites(idSolicitud: number) {
    const options = {
      relations: ['tiposTramite', 'estadoTramite', 'tramitesClaseLicencia', 'tramitesClaseLicencia.clasesLicencias'],
      where: { idSolicitud },
    };
    return await this.tramitesRepository.find(options);
  }

  async postSolicitudCambiarEstado(dto: SolicitudCambioEstadoDTO) {
    const created_at = new Date();
    const updated_at = created_at;
    var idEstado = dto.idEstadoSolicitud;
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const manual = true;
    if (idEstado == null) {
      idEstado = dto.idEstadoSolicitudPrevio;
      dto.idEstadoSolicitud = dto.idEstadoSolicitudPrevio;
    }
    const data = { ...dto, created_at, idUsuario, manual };
    console.log('******************************************************************');
    console.log(data);

    return await getManager().transaction(async (manager: EntityManager) => {
      await manager.update(Solicitudes, dto.idSolicitud, { idEstado, updated_at });
      return await manager.save(RegistroCambiosEstadoSolicitudEntity, data);
    });
  }

  async postCambioEstado(dto: any) {
    const created_at = new Date();
    const updated_at = created_at;
    var idEstado = dto.idEstadoSolicitud;
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const observacion = dto.observacion;
    const manual = true;
    // ! Guarda el estado Solicitud Nulo por ahora
    if (idEstado == null) {
      idEstado = dto.idEstadoSolicitudPrevio;
      dto.idEstadoSolicitud = dto.idEstadoSolicitudPrevio;
    }
    const data = { ...dto, created_at, idUsuario, manual };

    await getManager()
      .transaction(async (manager: EntityManager) => {
        await manager.update(Solicitudes, dto.idSolicitud, { idEstado, updated_at });
        return await manager.save(RegistroCambiosEstadoSolicitudEntity, data);
      })
      .then(res => {
        const idRegistroSolicitud = res.IdRegistroCambiosEstadoSolicitud;

        // Guarda todos los tramites
        dto.dataTramites.forEach(tramite => {
          const dataTramite = {
            ...tramite,
            observacion,
            IdRegistroCambiosEstadoSolicitud: idRegistroSolicitud,
            idUsuario,
            created_at,
            manual,
          };
          this.postTramiteCambiarEstado(dataTramite);
        });

        // Guarda todas las examinaciones
        dto.dataExaminaciones.forEach(examinacion => {
          const dataExaminacion = {
            ...examinacion,
            observacion,
            IdRegistroCambiosEstadoSolicitud: idRegistroSolicitud,
            RealizadoPor: idUsuario,
            created_at,
            manual,
          };
          this.postExaminacionCambiarEstado(dataExaminacion);
        });

        // Guarda todos los documentos
        dto.dataDocumentos.forEach(documento => {
          const dataDocumento = {
            nombreDoc: documento.nombreOriginal,
            archivo: documento.base64,
            IdRegistroCambiosEstadoSolicitud: idRegistroSolicitud,
          };
          this.postDocumento(dataDocumento);
        });
      });
  }

  async solicitudCambioEstadoEspecial(req:any, registroSolicitudCambiodto: RegistroSolicitudCambioEstadoDto) {
    let res: Resultado = new Resultado();

    try {

      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ModificacionesEspecialesCambiosdeEstadosModificarEstadodeTramitesExaminaciones);
    
      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }	 

      // 2 Recuperamos datos de la solicitud original
      let solicitudOriginal: Solicitudes = await this.GetSolicitudTramitesExaminaciones(registroSolicitudCambiodto.IdSolicitudCambioEstado);

      // 3 Mapeamos datos de la solicitud original junto a los datos que nos vienen
      let solicitudMapeadaConOriginal: SolicitudCambioEstadoValidacionDto = await this.MapearSolicitudTramitesExaminacionesToDto(
        solicitudOriginal,
        registroSolicitudCambiodto
      );

      // 4 Se realiza la validación de los datos conforme a los estados (Reglas de negocio necesarias) y se devuelve un nuevo estado de solicitud
      // si es necesario
      let resultadoValidacion: Resultado = this.ValidacionEstadosTramitesXExaminaciones(solicitudMapeadaConOriginal);

      // 5 Realizamos validaciones previas, permisos y verificación de los datos ( validación estados examinaciones y trámites correctos )
      // matriz
      let resultadoMatriz: Resultado = resultadoValidacion.Respuesta
        ? await this.comprobarMatrizTramites(solicitudMapeadaConOriginal)
        : null;

      // 6 Cambiamos el nuevo estado de solicitud si es necesario
      if (resultadoMatriz?.ResultadoOperacion == true) {
        resultadoMatriz = await this.ObtenerNuevoEstadoSolicitud(solicitudMapeadaConOriginal);
      }

      if (resultadoMatriz?.ResultadoOperacion == true) {
        // 7 Por último guardamos los datos una vez verificado todo lo demás
        await this.guardarCambioEstado(registroSolicitudCambiodto, resultadoMatriz.Respuesta, resultadoMatriz.Respuesta.idEstadoNuevo);

        res.ResultadoOperacion = true;
        res.Mensaje = 'Los datos fueron guardados correctamente.';

        return res;
      } else {
        res.ResultadoOperacion = false;
        res.Mensaje =
          'Los datos insertados no cumple con las reglas de estados de trámites y examinaciones, por favor revise la información seleccionada.';

        return res;
      }
    } catch (Error) {
      res.ResultadoOperacion = false;
      res.Mensaje = 'Se ha producido un error en el tratamiento de los datos, por favor consulte al administrador para más información.';
      return res;
    }
  }

  async consultaNuevoEstadoSolicitudCambioEstadoEspecial(req:any, registroSolicitudCambiodto: RegistroSolicitudCambioEstadoDto) {
    let res: Resultado = new Resultado();

    try {

      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ModificacionesEspecialesCambiosdeEstadosModificarEstadodeTramitesExaminaciones);
    
      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }	 

      // 2 Recuperamos datos de la solicitud original
      let solicitudOriginal: Solicitudes = await this.GetSolicitudTramitesExaminaciones(registroSolicitudCambiodto.IdSolicitudCambioEstado);

      // 3 Mapeamos datos de la solicitud original junto a los datos que nos vienen
      let solicitudMapeadaConOriginal: SolicitudCambioEstadoValidacionDto = await this.MapearSolicitudTramitesExaminacionesToDto(
        solicitudOriginal,
        registroSolicitudCambiodto
      );

      // 4 Se realiza la validación de los datos conforme a los estados (Reglas de negocio necesarias) y se devuelve un nuevo estado de solicitud
      // si es necesario
      //let resultadoValidacion: Resultado = this.ValidacionEstadosTramitesXExaminaciones(solicitudMapeadaConOriginal);

      // 5 Realizamos validaciones previas, permisos y verificación de los datos ( validación estados examinaciones y trámites correctos )
      // matriz
      // let resultadoMatriz: Resultado = resultadoValidacion.Respuesta
      //   ? await this.comprobarMatrizTramites(solicitudMapeadaConOriginal)
      //   : null;

      // 6 Cambiamos el nuevo estado de solicitud si es necesario
      //if (resultadoMatriz?.ResultadoOperacion == true) {
      let resultadoMatriz = await this.ObtenerNuevoEstadoSolicitud(solicitudMapeadaConOriginal);
      //}
      res.Respuesta = resultadoMatriz.Respuesta;
      res.ResultadoOperacion = true;
      res.Mensaje = "Se devuelve el nuevo estado de solicitud";

      return res;

    } catch (Error) {
      res.ResultadoOperacion = false;
      res.Mensaje = 'Se ha producido un error en el tratamiento de los datos, por favor consulte al administrador para más información.';
      return res;
    }
  }


  async postTramiteCambiarEstado(dto: any) {
    const idEstadoTramite = dto.idEstadoTramite;
    const created_at = new Date();
    const update_at = new Date();
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const manual = true;
    const data = { ...dto, created_at, idUsuario, manual };

    return await getManager().transaction(async (manager: EntityManager) => {
      try {
        await manager.update(TramitesEntity, dto.idTramite, { idEstadoTramite, update_at });
        await manager.save(RegistroCambiosEstadoTramiteEntity, data);
        manager.queryRunner.commitTransaction();
      } catch (err) {
        manager.queryRunner.rollbackTransaction();
      }

      return true;
    });
  }

  async postExaminacionCambiarEstado(dto: any) {
    return await getManager().transaction(async (manager: EntityManager) => {
      try {
        await manager.update(ColaExaminacionEntity, dto.IdExaminacionParaCambiar, {
          idEstadosExaminacion: dto.IdNuevoEstadoExaminacionParaCambiar,
        });
        await manager.save(RegistroCambiosEstadoExaminacion, dto);
        manager.queryRunner.commitTransaction();
      } catch (err) {
        manager.queryRunner.rollbackTransaction();
      }

      return true;
    });
  }

  async postExaminacionCambiarEstadoModificacionesEspeciales(dto: any) {
    return await getManager().transaction(async (manager: EntityManager) => {
      try {
        // Se asigna el nuevo estado de la examinación
        let nuevoEstadoAprobadoColaExam : boolean = this.AsignarNuevoFlagPorEstadoExaminacion(dto.IdNuevoEstadoExaminacionParaCambiar);

        // Se asigna el nuevo estado para el histórico (si aprobado es null está pendiente, por tanto histórico false, en otro caso true)
        let nuevoEstadoHistoricoColaExam : boolean = (nuevoEstadoAprobadoColaExam || nuevoEstadoAprobadoColaExam == null)?false:true;

        await manager.update(ColaExaminacionEntity, dto.IdExaminacionParaCambiar, {
          idEstadosExaminacion: dto.IdNuevoEstadoExaminacionParaCambiar,
          aprobado: nuevoEstadoAprobadoColaExam,
          historico: nuevoEstadoHistoricoColaExam
        });
        await manager.save(RegistroCambiosEstadoExaminacion, dto);
        manager.queryRunner.commitTransaction();
      } catch (err) {
        manager.queryRunner.rollbackTransaction();
      }

      return true;
    });
  }

  async postDocumento(dto: DocRespaldoCambioEstadoDTO) {
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const created_at = new Date();
    const data = { ...dto, idUsuario, created_at };
    return await this.docCambioEstadoRepository.save(data);
  }

  async deleteDocumento(id: number) {
    this.docCambioEstadoRepository.delete(id);
    return true;
  }

  public async getEstadosExaminacion() {
    // Estados de examinación admitidos : 11 - ""Examen médico reprobado""
    //                                    36 - ""Reprobado por idoneidad moral""

    const filtroIds: number[] = [11, 36];

    return this.serviciosComunesService.getEstadosExaminacion();
  }

  private async GetSolicitudTramitesExaminaciones(idSolicitud: number): Promise<Solicitudes> {
    const qbSolicitudes = this.solicitudesEntityRespository
      .createQueryBuilder('Solicitudes')
      //.innerJoinAndMapOne('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
      .innerJoinAndMapOne(
        'Solicitudes.estadosSolicitud',
        EstadosSolicitudEntity,
        'estadosSolicitud',
        'estadosSolicitud.idEstado = Solicitudes.idEstado'
      )
      .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
      .innerJoinAndMapOne(
        'tramites.tiposTramite',
        TiposTramiteEntity,
        'tiposTramite',
        'tramites.idTipoTramite = tiposTramite.idTipoTramite'
      )
      .innerJoinAndMapOne(
        'tramites.estadoTramite',
        EstadosTramiteEntity,
        'estadoTramite',
        'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
      )
      .innerJoinAndMapOne(
        'tramites.tramitesClaseLicencia',
        TramitesClaseLicencia,
        'tramitesClaseLicencia',
        'tramites.idTramite = tramitesClaseLicencia.idTramite'
      )
      .innerJoinAndMapOne(
        'tramitesClaseLicencia.clasesLicencias',
        ClasesLicenciasEntity,
        'clasesLicencias',
        'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
      )
      .innerJoinAndMapMany(
        'tramites.colaExaminacionNM',
        ColaExaminacionTramiteNMEntity,
        'colaExaminacionNM',
        'tramites.idTramite = colaExaminacionNM.idTramite'
      )
      .innerJoinAndMapOne(
        'colaExaminacionNM.colaExaminacion',
        ColaExaminacion,
        'colaExaminacion',
        'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
      )
      .innerJoinAndMapOne(
        'colaExaminacion.estadosExaminacion',
        EstadosExaminacionEntity,
        'estadosExaminacion',
        'estadosExaminacion.idEstadosExaminacion = colaExaminacion.idEstadosExaminacion'
      )
      .innerJoinAndMapOne(
        'colaExaminacion.tipoExaminaciones',
        TipoExaminacionesEntity,
        'tipoExaminaciones',
        'colaExaminacion.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion'
      )

      // Recogemos la solicitud correspondiente
      .andWhere('(Solicitudes.idSolicitud = :_idSolicitud)', { _idSolicitud: idSolicitud });

    const solicitudRetorno: Solicitudes = await qbSolicitudes.getOne();

    return solicitudRetorno;
  }

  private async MapearSolicitudTramitesExaminacionesToDto(
    solicitud: Solicitudes,
    registroSolicitudCambiodto: RegistroSolicitudCambioEstadoDto
  ): Promise<SolicitudCambioEstadoValidacionDto> {
    let solce: SolicitudCambioEstadoValidacionDto = new SolicitudCambioEstadoValidacionDto();

    solce.idSolicitud = solicitud.idSolicitud;
    solce.idEstado = solicitud.idEstado;
    solce.codigoEstado = solicitud.estadosSolicitud.codigo;

    solce.CambioEstadoTramitesValidacion = [];

    //Recuperamos primeramente los códigos de examinaciones y los de trámites para luego poder asignar
    const estadosExaminacionResult: EstadosExaminacionEntity[] = await this.estadoExaminacionRepository
      .createQueryBuilder('estadosExaminacion')
      .getMany();
    const estadosTramitesResult: EstadosTramiteEntity[] = await this.estadoTramiteRepository.createQueryBuilder('Tramites').getMany();

    solicitud.tramites.forEach(tram => {
      //En este momento comprobamos si hay algún nuevo estado de trámite para cambiar, lo cogemos de la nueva clase y no de la existente
      let tramce: SolicitudCambioEstadoTramiteValidacionDto = new SolicitudCambioEstadoTramiteValidacionDto();
      tramce.idTramite = tram.idTramite;

      // Si se encuentra asignamos, en otro caso dejamos el que estaba anteriormente
      let regSolC = registroSolicitudCambiodto.CambiosEstadosTramites.find(x => x.IdTramiteParaCambiar == tram.idTramite);

      tramce.idEstadoPrevio = tram.idEstadoTramite;
      tramce.idEstado = regSolC?.IdNuevoEstadoTramite ? regSolC.IdNuevoEstadoTramite : tram.idEstadoTramite;
      tramce.codigoEstado = estadosTramitesResult.find(x => x.idEstadoTramite == tramce.idEstado).codigo;
      tramce.codigoEstadoPrevio = tram.estadoTramite.codigo;
      tramce.idEstadoPrevio = tram.estadoTramite.idEstadoTramite;
      tramce.CambioEstadoExaminacionValidacion = [];

      tram.colaExaminacionNM.forEach(cexam => {
        // En este momento comprobamos si hay algún nuevo estado de examinación, si lo hay, lo recuperamos para el estado de examinación,
        // en otro caso dejamos el que estaba
        let colaexamce: SolicitudCambioEstadoExaminacionValidacionDto = new SolicitudCambioEstadoExaminacionValidacionDto();

        colaexamce.idExaminacion = cexam.idColaExaminacion;

        let regExamc = registroSolicitudCambiodto.CambiosEstadosExaminaciones.find(
          x => x.IdExaminacionParaCambiar == cexam.idColaExaminacion
        );

        colaexamce.idEstadoPrevio = cexam.colaExaminacion.idEstadosExaminacion;

        colaexamce.idEstado = regExamc?.IdNuevoEstadoExaminacionParaCambiar
          ? regExamc.IdNuevoEstadoExaminacionParaCambiar
          : cexam.colaExaminacion.idEstadosExaminacion;

        colaexamce.codigoEstado = estadosExaminacionResult.find(x => x.idEstadosExaminacion == colaexamce.idEstado).codigo;

        tramce.CambioEstadoExaminacionValidacion.push(colaexamce);
      });

      solce.CambioEstadoTramitesValidacion.push(tramce);
    });

    // Devolvemos el resultado del mapeado
    return solce;
  }

  private async guardarCambioEstado(
    solicitudCambioEstado: RegistroSolicitudCambioEstadoDto,
    solicitudMapeadaConOriginal: SolicitudCambioEstadoValidacionDto,
    idNuevoEstadoSolicitud?: number
  ) {
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const observacion = solicitudCambioEstado.Observaciones;

    try {
      await getManager()
        .transaction(async (manager: EntityManager) => {
          // Si tenemos que actualizar el estado de la solicitud, lo actualizamos
          if (idNuevoEstadoSolicitud) {
            await manager.update(Solicitudes, solicitudCambioEstado.IdSolicitudCambioEstado, {
              idEstado: idNuevoEstadoSolicitud,
              updated_at: new Date(),
            });

            let registroCambioEstado: RegistroCambiosEstadoSolicitudEntity = new RegistroCambiosEstadoSolicitudEntity();
            let registroCambioEstadoGuardado;

            registroCambioEstado.idSolicitud = solicitudCambioEstado.IdSolicitudCambioEstado;
            registroCambioEstado.idEstadoSolicitudPrevio = solicitudMapeadaConOriginal.idEstado;
            registroCambioEstado.idEstadoSolicitud = idNuevoEstadoSolicitud ? idNuevoEstadoSolicitud : solicitudMapeadaConOriginal.idEstado;
            registroCambioEstado.idUsuario = idUsuario;
            registroCambioEstado.created_at = new Date();
            registroCambioEstado.observacion = solicitudCambioEstado.Observaciones;
            registroCambioEstado.informadoSRCeI = false;
            registroCambioEstado.manual = true;
            //registroCambioEstado.IdRegistroCambiosEstadoSolicitud:0

            try {
              registroCambioEstadoGuardado = await manager.save(RegistroCambiosEstadoSolicitudEntity, registroCambioEstado);
              manager.queryRunner.commitTransaction();
            } catch (err) {
              manager.queryRunner.rollbackTransaction();
            }

            return registroCambioEstadoGuardado;
          }
        })
        .then(res => {
          const idRegistroSolicitud = res?.IdRegistroCambiosEstadoSolicitud;

          // Guarda todos los tramites
          solicitudCambioEstado.CambiosEstadosTramites.forEach(async tramite => {
            // Buscamos el estado previo
            let idEstadoTramitePrevio;
            let encontrado = solicitudMapeadaConOriginal.CambioEstadoTramitesValidacion.find(
              y => y.idTramite == tramite.IdTramiteParaCambiar
            );

            encontrado ? (idEstadoTramitePrevio = encontrado.idEstadoPrevio) : null;

            const dataTramite = {
              observacion,
              idTramiteParaCambiar: tramite.IdTramiteParaCambiar,
              idTramite: tramite.IdTramiteParaCambiar,
              idEstadoTramite: tramite.IdNuevoEstadoTramite,
              idEstadoTramitePrevio: idEstadoTramitePrevio,
              IdRegistroCambiosEstadoSolicitud: idRegistroSolicitud,
              idUsuario: idUsuario,
              created_at: new Date(),
              manual: true,
            };
            await this.postTramiteCambiarEstado(dataTramite);
          });

          // Guarda todas las examinaciones
          solicitudCambioEstado.CambiosEstadosExaminaciones.forEach(async examinacion => {
            let idEstadoExaminacionPrevio;
            solicitudMapeadaConOriginal.CambioEstadoTramitesValidacion.forEach(x => {
              let encontrado = x.CambioEstadoExaminacionValidacion.find(y => y.idExaminacion == examinacion.IdExaminacionParaCambiar);

              encontrado ? (idEstadoExaminacionPrevio = encontrado.idEstadoPrevio) : null;
            });

            

            const dataExaminacion = {
              ...examinacion,
              observacion,
              RealizadoPor: idUsuario,
              created_at: new Date(),
              manual: true,
              idColaExaminacion: examinacion.IdExaminacionParaCambiar,
              idEstadosExaminacionPrevio: idEstadoExaminacionPrevio,
              idEstadosExaminacionNuevo: examinacion.IdNuevoEstadoExaminacionParaCambiar,
            };

            await this.postExaminacionCambiarEstadoModificacionesEspeciales(dataExaminacion);
          });

          // Guarda todos los documentos
          solicitudCambioEstado.CambiosEstadosDocumentos.forEach(async documento => {
            const dataDocumento: DocRespaldoCambioEstadoDTO = {
              nombreDoc: documento.NombreDocumento,
              archivo: documento.Binario,
              IdRegistroCambiosEstadoSolicitud: idRegistroSolicitud,
            };
            await this.postDocumento(dataDocumento);
          });

          return res;
        });
    } catch (err) {}
  }

  private ValidacionEstadosTramitesXExaminaciones(solicitudCambioEstados: SolicitudCambioEstadoValidacionDto) {
    // Recorremos según la solicitud, los trámites recorridos para comprobar que están en un estado correcto
    let res: Resultado = new Resultado();

    // res = this.ValidarSolicitudXTramites(
    //   solicitudCambioEstados.codigoEstado,
    //   solicitudCambioEstados.CambioEstadoTramitesValidacion.map(x => x.codigoEstado)
    // );

    // if (res.Respuesta == false) {
    //   return res;
    // }

    solicitudCambioEstados.CambioEstadoTramitesValidacion.forEach(cetv => {
      res = this.ValidarTramiteXExaminaciones(
        cetv.codigoEstado,
        cetv.CambioEstadoExaminacionValidacion.map(x => x.codigoEstado)
      );

      if (res.Respuesta == false) {
        res.Mensaje = 'No se han podido confirmar los cambios, revise los estados.';
        return res;
      }
    });

    // En caso de que haya ido todo bien llegaremos a este punto
    if (res.Respuesta != false) {
      res.ResultadoOperacion = true;
      res.Respuesta = true;
      res.Mensaje = 'Validación realizada con éxito';
    }

    return res;
  }

  private ValidarSolicitudXTramites(codigoSolicitud: string, codigosTramites: string[]) {
    let res: Resultado = new Resultado();
    res.Respuesta = false;

    switch (codigoSolicitud) {
      case ESTADO_SOLICITUD_HORA_RESERVADA_CODE: {
        break;
      }

      case ESTADO_SOLICITUD_HORA_RESERVAD_FI_ATC_ENVDOS_CODE: {
        break;
      }

      case ESTADO_SOLICITUD_MESON_CODE: {
        break;
      }

      // Para que esté la solicitud en estado borrador, todos sus trámites deben estar en borrador
      case ESTADO_SOLICITUD_BORRADOR_CODE: {
        let codigosTemp = codigosTramites.filter(x => x == ESTADO_TRAMITE_BORRADOR_CODE);

        codigosTemp.length == codigosTramites.length ? (res.Respuesta = true) : (res.Respuesta = false);

        break;
      }

      //
      case ESTADO_SOLICITUD_ABIERTA_CODE: {
        break;
      }

      case ESTADO_SOLICITUD_CERRADA_CODE: {
        break;
      }

      case ESTADO_SOLICITUD_INCUMPLIMIENTO_REQUISITOS_CODE: {
        break;
      }

      case ESTADO_SOLICITUD_REABIERTA_RECONSID_JPL_SML_CODE: {
        break;
      }
    }

    res.ResultadoOperacion = true;
    return res;
  }

  private ValidarTramiteXExaminaciones(codigoTramite: string, codigosExaminacion: string[]): Resultado {

    //Filtramos las que vengan con estado asociado a preidoneidad moral (eliminamos las que tengan que ver con preidoneidad moral)
    let codigosExaminacionFiltrados = codigosExaminacion.filter(x =>  x != ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE
                                                                      && x != ESTADO_EXAM_EN_CURSO_PRE_IDON_MORAL_CODE
                                                                      && x != ESTADO_EXAM_ALERTA_PRE_IDON_MORAL_CODE
                                                                      && x != ESTADO_EXAM_NO_ALERTA_PRE_IDON_MORAL_CODE) 

    let res: Resultado = new Resultado();
    res.Respuesta = true;

    switch (codigoTramite) {
      case ESTADO_TRAMITE_INICIADO_CODE:
      case ESTADO_TRAMITE_REINICIADO_RECONSIDER_JPL_SML_CODE: {
        // Buscar que tengan almenos una examinacion que no esté en un estado terminal
        let encontrada = codigosExaminacion.find(
          x =>
            x == ESTADO_EXAM_PENDIENTE_IDON_MORAL_CODE ||
            x == ESTADO_EXAM_EN_CURSO_IDON_MORAL_CODE ||
            x == ESTADO_EXAM_PENDIENTE_MEDICA_CODE ||
            x == ESTADO_EXAM_EN_CURSO_MEDICA_CODE ||
            x == ESTADO_EXAM_MAS_ANTECEDENTES_MEDICA_CODE ||
            x == ESTADO_EXAM_PENDIENTE_TEORICA_CODE ||
            x == ESTADO_EXAM_PENDIENTE_PRACTICA_CODE ||
            x == ESTADO_EXAM_EN_CURSO_PRACTICA_CODE ||
            x == ESTADO_EXAM_EN_EVALUACION_IDON_MORAL_CODE
        );

        encontrada.length > 0 ? (res.Respuesta = true) : (res.Respuesta = false);

        break;
      }

      case ESTADO_TRAMITE_DENEGACION_INFORMADA:
      case ESTADO_TRAMITE_DENEGACION_INFORMADA_JPL_CODE:
      case ESTADO_TRAMITE_DENEGACION_INFORMADA_SML_CODE:
      case ESTADO_TRAMITE_ESPERA_INFORMAR_DENEGACION_CODE: {
        // Si no encontramos uno que sea en estado rechazado, entonces devolvemos falso
        let examinacionesEncontradas = codigosExaminacion.filter(
          x =>
            x == ESTADO_EXAM_REPROBADO_IDON_MORAL_CODE ||
            x == ESTADO_EXAM_REPROBADO_MEDICA_CODE ||
            x == ESTADO_EXAM_REPROBADO_TEORICA_1_OPORT_CODE ||
            x == ESTADO_EXAM_REPROBADO_TEORICA_NO_ASIST_2_OPORT_CODE ||
            x == ESTADO_EXAM_REPROBADO_TEORICA_2_OPORT_CODE ||
            x == ESTADO_EXAM_REPROBADO_PRACTICA_1_OPORT_CODE ||
            x == ESTADO_EXAM_REPROBADO_PRACTICA_NO_ASIST_2_OPORT_CODE ||
            x == ESTADO_EXAM_REPROBADO_PRACTICA_2_OPORT_CODE
        );

        examinacionesEncontradas.length > 0 ? (res.Respuesta = true) : (res.Respuesta = false);

        break;
      }

      case ESTADO_TRAMITE_RECEPCION_NO_CONFORME:
      case ESTADO_TRAMITE_RECEPCION_CONFORME:
      case ESTADO_TRAMITE_ESPERA_INFORMAR_ORTORGAMIENTO_CODE:
      case ESTADO_TRAMITE_OTORGAMIENTO_INFORMADO_IMPRES_CODE: {
        // Tienen que estar todos los tramites aprobados

        let examinacionesEncontradas = codigosExaminacionFiltrados.filter(
          x =>
            x == ESTADO_EXAM_APROBADO_IDON_MORAL_CODE ||
            x == ESTADO_EXAM_APROBADO_IDON_MORAL_JPL_CODE ||
            x == ESTADO_EXAM_APROBADO_MEDICA_CODE ||
            x == ESTADO_EXAM_APROBADO_MEDICA_SML_CODE ||
            x == ESTADO_EXAM_APROBADO_PRACTICA_CODE ||
            x == ESTADO_EXAM_APROBADO_TEORICA_CODE
        );

        examinacionesEncontradas.length >= codigosExaminacionFiltrados.length ? (res.Respuesta = true) : (res.Respuesta = false);

        break;
      }

      case ESTADO_TRAMITE_DESISTIDO_CODE: {
        break;
      }

      case ESTADO_TRAMITE_ABANDONADO_CODE: {
        break;
      }

      case ESTADO_TRAMITE_ENVIO_CORREO_DENEG_EXAM_TEORC_CODE: {
        break;
      }

      case ESTADO_TRAMITE_EN_ESPERA_REGISTRO_CIVIL_CODE: {
        break;
      }

      case ESTADO_TRAMITE_BORRADOR_CODE: {
        break;
      }

      case ESTADO_TRAMITE_CANCELADO_CERRADO_CODE: {
        break;
      }
    }

    res.ResultadoOperacion = true;
    return res;
  }

  async comprobarMatrizTramites(solicitudCambioEstados: SolicitudCambioEstadoValidacionDto) {
    let resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    let matrices: MatrizEstadosTramiteEntity[] = [];

    // Iteramos los posibles cambios de estado
    await Promise.all(
      solicitudCambioEstados.CambioEstadoTramitesValidacion.map(async t => {
        // Solo comprobamos los que si llevan un cambio de estado
        if (t.idEstadoPrevio != t.idEstado) {
          // Buscamos en la matriz si esta permitido el cambio de estado
          let matriz: MatrizEstadosTramiteEntity = await this.matrizEstadoTramiteRepository
            .createQueryBuilder('MatrizEstadoTramite')
            .where(
              'MatrizEstadoTramite.IdEstadoTramiteOrigen = :idOrigen AND MatrizEstadoTramite.IdEstadoTramiteDestino = :idDestino AND permitido = :permitido',
              {
                idOrigen: t.idEstadoPrevio,
                idDestino: t.idEstado,
                permitido: true,
              }
            )
            .getOne();

          matrices.push(matriz);
        }
      })
    ).then(
      res => {
        // Buscamos algun cambio de estado invalido
        let cambioEstadoInvalido = matrices.find(m => {
          return m == null;
        });

        resultado.ResultadoOperacion = cambioEstadoInvalido ? false : true;
      },
      err => {
        console.log(err);
      }
    );

    return resultado;
  }

  async ObtenerNuevoEstadoSolicitud(solicitudCambioEstadoValidacionDto: SolicitudCambioEstadoValidacionDto) {
    let resultado: Resultado = new Resultado();

    try {
      let tramitesCerrados = solicitudCambioEstadoValidacionDto.CambioEstadoTramitesValidacion.filter(x => 

             (x.codigoEstado == ESTADO_TRAMITE_DESISTIDO_CODE)
          || (x.codigoEstado == ESTADO_TRAMITE_ABANDONADO_CODE)
          || (x.codigoEstado == ESTADO_TRAMITE_DENEGACION_INFORMADA)
          || (x.codigoEstado == ESTADO_TRAMITE_DENEGACION_INFORMADA_SML_CODE)
          || (x.codigoEstado == ESTADO_TRAMITE_DENEGACION_INFORMADA_JPL_CODE)
          || (x.codigoEstado == ESTADO_TRAMITE_CANCELADO_CERRADO_CODE)

      );

      // Si todos los tramites se encuentran en estados finales, cerramos la solicitud
      if (tramitesCerrados.length == solicitudCambioEstadoValidacionDto.CambioEstadoTramitesValidacion.length) {
        
        let nuevoEstadoSolicitud : EstadosSolicitudEntity = (await this.estadoSolicitudRepository.findOne({ where: { codigo: ESTADO_SOLICITUD_CERRADA_CODE } }));

        solicitudCambioEstadoValidacionDto.idEstadoNuevo = nuevoEstadoSolicitud.idEstado;
        solicitudCambioEstadoValidacionDto.DescripcionEstado = nuevoEstadoSolicitud.Nombre;
      }

      let tramitesAbiertos = solicitudCambioEstadoValidacionDto.CambioEstadoTramitesValidacion.filter(x => 
           (x.codigoEstado == ESTADO_TRAMITE_INICIADO_CODE)
        || (x.codigoEstado == ESTADO_TRAMITE_ESPERA_INFORMAR_DENEGACION_CODE)
        || (x.codigoEstado == ESTADO_TRAMITE_ESPERA_INFORMAR_ORTORGAMIENTO_CODE)
        || (x.codigoEstado == ESTADO_TRAMITE_OTORGAMIENTO_INFORMADO_IMPRES_CODE)
        || (x.codigoEstado == ESTADO_TRAMITE_REINICIADO_RECONSIDER_JPL_SML_CODE)
        || (x.codigoEstado == ESTADO_TRAMITE_ENVIO_CORREO_DENEG_EXAM_TEORC_CODE)
      );

      // Si todos los tramites se encuentran en estados iniciales, reabrimos la solicitud
      if (tramitesAbiertos.length == solicitudCambioEstadoValidacionDto.CambioEstadoTramitesValidacion.length) {
          let nuevoEstadoSolicitud : EstadosSolicitudEntity = (await this.estadoSolicitudRepository.findOne({ where: { codigo: ESTADO_SOLICITUD_REABIERTA_RECONSID_JPL_SML_CODE } }));

          solicitudCambioEstadoValidacionDto.idEstadoNuevo = nuevoEstadoSolicitud.idEstado; 
          solicitudCambioEstadoValidacionDto.DescripcionEstado = nuevoEstadoSolicitud.Nombre;   
      }

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = solicitudCambioEstadoValidacionDto;
    } catch (err) {
      resultado.Error = err;
      resultado.Respuesta = 'Ha ocurrido un error';
    }

    return resultado;
  }

  AsignarNuevoFlagPorEstadoExaminacion(IdNuevoEstadoExaminacionParaCambiar: number): boolean {
    let nuevoEstadoFlagParaCambiar : boolean = null;
  
    switch(IdNuevoEstadoExaminacionParaCambiar){
  // Estados examinación idoneidad moral 5 
      case ESTADO_EXAM_PENDIENTE_IDON_MORAL_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_EN_CURSO_IDON_MORAL_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_EN_EVALUACION_IDON_MORAL_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_APROBADO_IDON_MORAL_ID:{nuevoEstadoFlagParaCambiar = true};break;
      case ESTADO_EXAM_REPROBADO_IDON_MORAL_ID:{nuevoEstadoFlagParaCambiar = false};break;
      case ESTADO_EXAM_APROBADO_IDON_MORAL_JPL_ID:{nuevoEstadoFlagParaCambiar = true};break;
  
      // Estados examinación médica 4
      case ESTADO_EXAM_PENDIENTE_MEDICA_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_EN_CURSO_MEDICA_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_APROBADO_MEDICA_ID:{nuevoEstadoFlagParaCambiar = true};break;
      case ESTADO_EXAM_REPROBADO_MEDICA_ID:{nuevoEstadoFlagParaCambiar = false};break;
      case ESTADO_EXAM_MAS_ANTECEDENTES_MEDICA_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_APROBADO_MEDICA_SML_ID:{nuevoEstadoFlagParaCambiar = true};break;
  
      // Estados examinación teórica 3
      case ESTADO_EXAM_PENDIENTE_TEORICA_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_EN_CURSO_TEORICA_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_APROBADO_TEORICA_ID:{nuevoEstadoFlagParaCambiar = true};break;
      case ESTADO_EXAM_REPROBADO_TEORICA_1_OPORT_ID:{nuevoEstadoFlagParaCambiar = false};break;
      case ESTADO_EXAM_REPROBADO_TEORICA_NO_ASIST_2_OPORT_ID:{nuevoEstadoFlagParaCambiar = false};break;
      case ESTADO_EXAM_REPROBADO_TEORICA_2_OPORT_ID:{nuevoEstadoFlagParaCambiar = false};break;
  
      // Estados examinación práctica 2
      case ESTADO_EXAM_PENDIENTE_PRACTICA_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_EN_CURSO_PRACTICA_ID:{nuevoEstadoFlagParaCambiar = null};break;
      case ESTADO_EXAM_APROBADO_PRACTICA_ID:{nuevoEstadoFlagParaCambiar = true};break;
      case ESTADO_EXAM_REPROBADO_PRACTICA_1_OPORT_ID:{nuevoEstadoFlagParaCambiar = false};break;
      case ESTADO_EXAM_REPROBADO_PRACTICA_NO_ASIST_2_OPORT_ID:{nuevoEstadoFlagParaCambiar = false};break;
      case ESTADO_EXAM_REPROBADO_PRACTICA_2_OPORT_ID:{nuevoEstadoFlagParaCambiar = false};break; 
    }
  
    return nuevoEstadoFlagParaCambiar;
  }

}



