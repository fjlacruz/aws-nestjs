import { ApiPropertyOptional } from "@nestjs/swagger";

export class CarpetaConductorDTO {

    @ApiPropertyOptional()
    NombreDocumento:string;
    @ApiPropertyOptional()
    descripcion:string;
    // @ApiPropertyOptional()
    // created_at:Date;
    // @ApiPropertyOptional()
    // update_at:Date;
    // @ApiPropertyOptional()
    // idUsuario:number;
    @ApiPropertyOptional()
    idPostulante:number;
    @ApiPropertyOptional()
    archivo:Buffer;
}


