import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { CarpetaConductorController } from './controller/carpeta-conductor.controller';
import { CarpetaConductorEntity } from './entity/carpeta-conductor.entity';
import { CarpetaConductorService } from './services/carpeta-conductor.service';


@Module({
  imports: [TypeOrmModule.forFeature([CarpetaConductorEntity, User, RolesUsuarios])],
  providers: [CarpetaConductorService, AuthService],
  exports: [CarpetaConductorService],
  controllers: [CarpetaConductorController]
})
export class CarpetaConductorModule {}
