import { Test, TestingModule } from '@nestjs/testing';
import { CarpetaConductorController } from './carpeta-conductor.controller';

describe('CarpetaConductorController', () => {
  let controller: CarpetaConductorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CarpetaConductorController],
    }).compile();

    controller = module.get<CarpetaConductorController>(CarpetaConductorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
