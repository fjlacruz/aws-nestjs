import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CarpetaConductorDTO } from '../DTO/carpeta-conductor.dto';
import { CarpetaConductorService } from '../services/carpeta-conductor.service';


@ApiTags('Carpeta-Conductor')
@Controller('carpeta-conductor')
export class CarpetaConductorController {

    constructor(private readonly carpetaConductorService: CarpetaConductorService) { }

    @Get('carpetasConductor')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las carpetas del conductor' })
    async getCarpetasConductor() {
        const data = await this.carpetaConductorService.getCarpetasConductor;
        return { data };
    }


    @Get('carpetaConductorById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una carpeta del conductor por Id' })
    async carpetaConductorById(@Param('id') id: number) {
        const data = await this.carpetaConductorService.getCarpetaConductor(id);
        return { data };
    }


    @Post('uploadCarpetaConductor')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Registro Carpeta Conductor' })
    async uploadCarpetaConductor(
        @Body() dto: CarpetaConductorDTO) {

        const data = await this.carpetaConductorService.createCarpetaConductor(dto);
        return {data};

    }


}
