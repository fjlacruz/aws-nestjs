import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn} from "typeorm";

@Entity('CarpetaConductor')
export class CarpetaConductorEntity {

    @PrimaryColumn()
    idCarpetaConductor:number;

    @Column()
    NombreDocumento:string;

    @Column()
    created_at:Date;

    @Column()
    update_at:Date;

    @Column()
    descripcion:string;

    @Column()
    idUsuario:number;

    @Column()
    idPostulante:number;

    @Column({
        name: 'File',
        type: 'bytea',
        nullable: false,
    })
    archivo:Buffer;
}

