import { Test, TestingModule } from '@nestjs/testing';
import { CarpetaConductorService } from './carpeta-conductor.service';

describe('CarpetaConductor', () => {
  let service: CarpetaConductorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CarpetaConductorService],
    }).compile();

    service = module.get<CarpetaConductorService>(CarpetaConductorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
