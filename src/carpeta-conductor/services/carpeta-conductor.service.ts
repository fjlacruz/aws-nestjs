import { Injectable , NotFoundException} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Repository, getConnection, getRepository } from 'typeorm';
import { CarpetaConductorDTO } from '../DTO/carpeta-conductor.dto';
import { CarpetaConductorEntity } from '../entity/carpeta-conductor.entity';


@Injectable()
export class CarpetaConductorService {

    constructor
    (
        private authService: AuthService,

        @InjectRepository(CarpetaConductorEntity) 
        private readonly carpetaConductorService: Repository<CarpetaConductorEntity>
    )
    {}

    async getCarpetasConductor() {
        return await this.carpetaConductorService.find();
    }

    async getCarpetaConductor(id:number){
        const carpeta= await this.carpetaConductorService.findOne(id);
        if (!carpeta)throw new NotFoundException("documneto dont exist");
        
        return carpeta;
    }

    async createCarpetaConductor(dto: CarpetaConductorDTO): Promise<CarpetaConductorEntity> 
    {
        const idUsuario   = (await this.authService.usuario()).idUsuario;
        const created_at  = new Date();
        const update_at   = created_at;
        const data        = { ...dto, idUsuario, created_at, update_at };

        return await this.carpetaConductorService.save( data );
    }


}
