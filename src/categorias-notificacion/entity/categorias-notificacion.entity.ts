import { ApiProperty } from "@nestjs/swagger";
import { NotificacionesEntity } from "src/notificaciones/entity/notificaciones.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, ManyToOne, JoinColumn, OneToMany} from "typeorm";

@Entity('CategoriasNotificacion')
export class CategoriasNotificacionEntity {

    @PrimaryGeneratedColumn()
    idCategoria:number;

    @Column()
    NombreCategoria:string;

    @Column()
    Descripcion:string;
    
    @ApiProperty()
    @OneToMany(() => NotificacionesEntity, notificacion => notificacion.categoriasNotificacion)
    readonly notificacion: NotificacionesEntity[];
}
