import { ApiPropertyOptional } from "@nestjs/swagger";
export class CertificadoAntecConducObtenRenovLicenciaDTO {
    
    @ApiPropertyOptional()
    idCertificadoEscuela:number;

    @ApiPropertyOptional()
    Folio:string;

    @ApiPropertyOptional()
    FechaEmision:Date;

    @ApiPropertyOptional()
    idPostulante:number;

    @ApiPropertyOptional()
    idEscuelaConductor:number;

    @ApiPropertyOptional()
    archivo:Buffer;

    @ApiPropertyOptional()
    NombreArchivo:string;

    @ApiPropertyOptional()
    idSolicitud:number;

    @ApiPropertyOptional()
    idClaseLicencia:number;
}