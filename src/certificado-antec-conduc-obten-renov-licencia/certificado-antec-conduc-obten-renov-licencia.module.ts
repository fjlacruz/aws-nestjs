import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { CarpetaConductorEntity } from 'src/carpeta-conductor/entity/carpeta-conductor.entity';
import { CertificadoEscuelaConductorEntity } from 'src/certificado-escuela-conductor/entity/certificado-escuela-conductor.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { InstitucionesService } from 'src/instituciones/services/instituciones.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { jwtConstants } from 'src/users/constants';
import { User } from 'src/users/entity/user.entity';
import { CertificadoAntecConducObtenRenovLicenciaController } from './controller/certificado-antec-conduc-obten-renov-licencia.controller';
import { CertificadoAntecConducObtenRenovLicenciaService } from './services/certificado-antec-conduc-obten-renov-licencia.service';


@Module({
  imports: 
  [
    TypeOrmModule.forFeature([PostulantesEntity,ComunasEntity,OficinasEntity,InstitucionesEntity,CertificadoEscuelaConductorEntity,RolesUsuarios,User,Solicitudes]),
  ],
  providers: [CertificadoAntecConducObtenRenovLicenciaService,InstitucionesService, AuthService],
  exports: [CertificadoAntecConducObtenRenovLicenciaService,InstitucionesService],
  controllers: [CertificadoAntecConducObtenRenovLicenciaController]
})
export class CertificadoAntecConducObtenRenovLicenciaModule {}
