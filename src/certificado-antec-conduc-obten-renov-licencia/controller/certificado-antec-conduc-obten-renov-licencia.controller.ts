import { Controller, Req } from '@nestjs/common';
import { Get, Param } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { InstitucionesService } from 'src/instituciones/services/instituciones.service';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { CertificadoAntecConducObtenRenovLicenciaService } from '../services/certificado-antec-conduc-obten-renov-licencia.service';

@ApiTags('Certificado-Antecedente')
@Controller('certificadoAntecedente')
export class CertificadoAntecConducObtenRenovLicenciaController {

    constructor(private readonly certificadoAntecConducObtenRenovLicenciaService: CertificadoAntecConducObtenRenovLicenciaService,
                private readonly institucionesService : InstitucionesService,
                private readonly authService: AuthService) { }

    @Get('certificados')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve listado de municipios' })
    async getMunicipios() {
        const data = await this.institucionesService.getMunicipalidades();
        return { data };
    }


    @Get('getCertificadoAntecedentes/:run/:idMunicipio')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Certificado por run e id de municipio' })
    async getCertificadoAntecedentes(@Param('run') run: string, @Param('idMunicipio') idMunicipio: number, @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.AccesoConsultaHistoricaUsoExclusivoObtencion
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {
            const data = await this.certificadoAntecConducObtenRenovLicenciaService.getCertificadoAntecedentes(run,idMunicipio);
            return { data };
        } else {

            return { data: validarPermisos };
        }
    }


    // @Post('uploadCertificado')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que crea un nuevo Certificado Escuela Conductor' })
    // async uploadDocumento(
    //     @Body() certificadoAntecConducObtenRenovLicenciaDTO: CertificadoAntecConducObtenRenovLicenciaDTO) {
    //     const data = await this.certificadoAntecConducObtenRenovLicenciaService.create(certificadoAntecConducObtenRenovLicenciaDTO);
    //     return {data};

    // }

    // @Post('updateCertificado')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que modifica la metadatos de un Certificado' })
    // async updateDocumento(
    //     @Body() certificadoAntecConducObtenRenovLicenciaDTO: CertificadoAntecConducObtenRenovLicenciaDTO) {
    //     const data = await this.certificadoAntecConducObtenRenovLicenciaService.update(certificadoAntecConducObtenRenovLicenciaDTO.idCertificadoEscuela, certificadoAntecConducObtenRenovLicenciaDTO);
    //     return {data};

    // }
        

    // @Get('deleteCertificadoById/:id')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que elimina un Certificado por Id' })
    // async deleteDocumentoById(@Param('id') id: number) {
    //     const data = await this.certificadoAntecConducObtenRenovLicenciaService.delete(id);
    //     return data;
    // }
}
