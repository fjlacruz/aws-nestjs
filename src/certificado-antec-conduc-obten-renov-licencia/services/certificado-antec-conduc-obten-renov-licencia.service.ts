import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CertificadoEscuelaConductorEntity } from 'src/certificado-escuela-conductor/entity/certificado-escuela-conductor.entity';
import { Repository } from 'typeorm';
import { CertificadoAntecConducObtenRenovLicenciaDTO } from '../DTO/certificado-antec-conduc-obten-renov-licencia.dto';
import { relacionCertificadoAntecedentes } from 'src/constantes';
import { Resultado } from 'src/utils/resultado';
import { certificadoMock } from '../DTO/MockCertificado'
import { CertificadoDTO } from '../DTO/certificado';
import { validarRut } from 'src/shared/ValidarRun';


@Injectable()
export class CertificadoAntecConducObtenRenovLicenciaService {

    constructor(
        @InjectRepository(CertificadoEscuelaConductorEntity) private readonly certificadoEscuelaConductorRepository: Repository<CertificadoEscuelaConductorEntity>,
        ) { }


    async getCertificadoAntecedentes(run: any, idMunicipalidad: any): Promise<Resultado>{

        const resultado: Resultado = new Resultado();
        let documentoAntecedentes: CertificadoEscuelaConductorEntity;

        try{

            // if(!validarRut(run)){
            //     resultado.Error = "El formato del rut es incorrecto"
            //     return resultado;
            // }

            // documentoAntecedentes = await this.certificadoEscuelaConductorRepository.findOne( {
            //     relations: relacionCertificadoAntecedentes,
            //     join: { alias: 'certificadoEscuelaConductor', innerJoinAndSelect: {
            //         postulante: 'certificadoEscuelaConductor.postulante', conductor: 'postulante.conductor',
            //         comunas: 'postulante.comunas', oficinas: 'comunas.oficinas', instituciones: 'oficinas.instituciones', 
            //       }
            //     },
            //     where: qb => {
            //         qb.where(
            //             'postulante.RUN || \'-\' || postulante.DV = :RUN', { RUN: run }
            //         ).andWhere(
            //             'instituciones.idInstitucion = :Codigo', { Codigo: idMunicipalidad}
            //         )
            //     }
            // });

            // MOCK
            let certificadoDTO : CertificadoDTO = new CertificadoDTO();
                certificadoDTO.archivo = certificadoMock.fileBinary.toString();
                certificadoDTO.NombreArchivo = certificadoMock.NombreDocumento;
                resultado.Respuesta = certificadoDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Certificado obtenido correctamente';

            // if (documentoAntecedentes) {

            //     let certificadoDTO : CertificadoAntecConducObtenRenovLicenciaDTO = new CertificadoAntecConducObtenRenovLicenciaDTO();
            //     certificadoDTO.archivo = documentoAntecedentes.archivo?.toString();
            //     certificadoDTO.NombreArchivo = documentoAntecedentes.NombreArchivo;

            //     resultado.Respuesta = certificadoDTO;
            //     resultado.ResultadoOperacion = true;
            //     resultado.Mensaje = 'Certificado obtenido correctamente';

            // } else {
            //     resultado.ResultadoOperacion = false;
            //     resultado.Error = 'No se encontro el Certificado';
            // }

        }catch(error){
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo el Certificado';
        }
        return resultado;
    }

}