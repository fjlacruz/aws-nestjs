import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CertificadoEscuelaConductorController } from './controller/certificado-escuela-conductor.controller';
import { CertificadoEscuelaConductorEntity } from './entity/certificado-escuela-conductor.entity';
import { CertificadoEscuelaConductorService } from './services/certificado-escuela-conductor.service';

@Module({
  imports: [TypeOrmModule.forFeature([CertificadoEscuelaConductorEntity])],
  providers: [CertificadoEscuelaConductorService],
  exports: [CertificadoEscuelaConductorService],
  controllers: [CertificadoEscuelaConductorController]
})
export class CertificadoEscuelaConductorModule {}
