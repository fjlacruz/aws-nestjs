import { Test, TestingModule } from '@nestjs/testing';
import { CertificadoEscuelaConductorController } from './certificado-escuela-conductor.controller';

describe('CertificadoEscuelaConductorController', () => {
  let controller: CertificadoEscuelaConductorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CertificadoEscuelaConductorController],
    }).compile();

    controller = module.get<CertificadoEscuelaConductorController>(CertificadoEscuelaConductorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
