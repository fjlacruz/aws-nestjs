import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CertificadoEscuelaConductorDTO } from '../DTO/certificado-escuela-conductor.dto';
import { CertificadoEscuelaConductorService } from '../services/certificado-escuela-conductor.service';

@ApiTags('Certificado-escuela-conductor')
@Controller('certificado-escuela-conductor')
export class CertificadoEscuelaConductorController {

    constructor(private readonly certificadoEscuelaConductorService: CertificadoEscuelaConductorService) { }

    @Get('certificados')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Certificados Escuela Conductor' })
    async getDocumentos() {
        const data = await this.certificadoEscuelaConductorService.getCertificadosEscuelaConductor();
        return { data };
    }


    @Get('certificadoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Certificado por Id' })
    async getDocumentoById(@Param('id') id: number) {
        const data = await this.certificadoEscuelaConductorService.getCertificadoEscuelaConductor(id);
        return { data };
    }


    @Post('uploadCertificado')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Certificado Escuela Conductor' })
    async uploadDocumento(
        @Body() certificadoEscuelaConductorDTO: CertificadoEscuelaConductorDTO) {
        const data = await this.certificadoEscuelaConductorService.create(certificadoEscuelaConductorDTO);
        return {data};

    }

    @Post('updateCertificado')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que modifica la metadatos de un Certificado' })
    async updateDocumento(
        @Body() certificadoEscuelaConductorDTO: CertificadoEscuelaConductorDTO) {
        const data = await this.certificadoEscuelaConductorService.update(certificadoEscuelaConductorDTO.idCertificadoEscuela, certificadoEscuelaConductorDTO);
        return {data};

    }
        

    @Get('deleteCertificadoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Certificado por Id' })
    async deleteDocumentoById(@Param('id') id: number) {
        const data = await this.certificadoEscuelaConductorService.delete(id);
        return data;
    }
}
