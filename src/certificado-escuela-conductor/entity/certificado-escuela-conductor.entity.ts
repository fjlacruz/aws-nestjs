import { Postulante } from "src/registro-pago/entity/postulante.entity";
import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn, OneToOne, JoinColumn} from "typeorm";

@Entity('CertificadosEscuelaConductor')
export class CertificadoEscuelaConductorEntity {

    @PrimaryGeneratedColumn()
    idCertificadoEscuela:number;

    @Column()
    Folio:string;

    @Column()
    FechaEmision:Date;

    @Column()
    idPostulante:number;
    @OneToOne(() => PostulantesEntity, postulante => postulante.certificadoEscuelaConductor)
    @JoinColumn({ name: 'idPostulante' })
    readonly postulante: PostulantesEntity;

    @Column()
    idEscuelaConductor:number;

    @Column({
        name: 'archivo',
        type: 'bytea',
        nullable: false,
    })
    archivo:Buffer;

    @Column()
    NombreArchivo:string;

    @Column()
    idSolicitud:number;

    @Column()
    idClaseLicencia:number;
}
