import { Test, TestingModule } from '@nestjs/testing';
import { CertificadoEscuelaConductorService } from './certificado-escuela-conductor.service';

describe('CertificadoEscuelaConductorService', () => {
  let service: CertificadoEscuelaConductorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CertificadoEscuelaConductorService],
    }).compile();

    service = module.get<CertificadoEscuelaConductorService>(CertificadoEscuelaConductorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
