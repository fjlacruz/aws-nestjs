import { Injectable , NotFoundException} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection, getRepository } from 'typeorm';
import { CertificadoEscuelaConductorDTO } from '../DTO/certificado-escuela-conductor.dto';
import { CertificadoEscuelaConductorEntity } from '../entity/certificado-escuela-conductor.entity';

@Injectable()
export class CertificadoEscuelaConductorService {

    constructor(@InjectRepository(CertificadoEscuelaConductorEntity) private readonly certificadoEscuelaConductorEntity: Repository<CertificadoEscuelaConductorEntity>) { }

    async getCertificadosEscuelaConductor() {
 
        const documentos = await getConnection().createQueryBuilder() 
            .select(['CertificadosEscuelaConductor.idCertificadoEscuela', 'CertificadosEscuelaConductor.Folio', 'CertificadosEscuelaConductor.FechaEmision', 'CertificadosEscuelaConductor.idPostulante', 'CertificadosEscuelaConductor.idEscuelaConductor', 'CertificadosEscuelaConductor.NombreArchivo', 'CertificadosEscuelaConductor.idSolicitud', 'CertificadosEscuelaConductor.idClaseLicencia'])
            .from(CertificadoEscuelaConductorEntity, "CertificadosEscuelaConductor")
            .getRawMany();
       
        if (!documentos)throw new NotFoundException("Certificado dont exist");
        
        return documentos;
  
    }
      
    async getCertificadoEscuelaConductor(id:number){
        const documento= await this.certificadoEscuelaConductorEntity.findOne(id);
        if (!documento)throw new NotFoundException("Certificado dont exist");
        
        return documento;
    }

    async create(certificadoEscuelaConductorDTO: CertificadoEscuelaConductorDTO): Promise<CertificadoEscuelaConductorEntity> {
        return await this.certificadoEscuelaConductorEntity.save(certificadoEscuelaConductorDTO);
    }

    async update(idCertificadoEscuela: number, data: Partial<CertificadoEscuelaConductorDTO>) {
        await this.certificadoEscuelaConductorEntity.update({ idCertificadoEscuela }, data);
        return await this.certificadoEscuelaConductorEntity.findOne({ idCertificadoEscuela });
    }

    async delete(id: number) {
        try {
            await getConnection()
            .createQueryBuilder()
            .delete()
            .from(CertificadoEscuelaConductorEntity)
            .where("idCertificadoEscuela = :id", { id: id })
            .execute();
            return {"code":200, "message":"success"};
        } catch (error) {
            return {"code":400, "error":error};
        }    
    }

}
