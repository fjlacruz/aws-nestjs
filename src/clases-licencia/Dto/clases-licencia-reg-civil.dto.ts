import { ApiPropertyOptional } from "@nestjs/swagger";

export class ClasesLicenciaRegistroCivilDto
{

    @ApiPropertyOptional()
    licComunaTraLic: string;
    @ApiPropertyOptional()
    licComunaUltLic: string;
    @ApiPropertyOptional()
    licEstadoClaLic: string;
    @ApiPropertyOptional()
    licEstadoDigLic: string;
    @ApiPropertyOptional()
    licEstadoFisLic: string;
    @ApiPropertyOptional()
    licFechaPriLic: Date;
    @ApiPropertyOptional()
    licFechaPrxCtl: Date;
    @ApiPropertyOptional()
    licFechaTraLic: string;
    @ApiPropertyOptional()
    licFechaUltLic: Date;
    @ApiPropertyOptional()
    licFolio: string;
    @ApiPropertyOptional()
    licFolioIni: string;
    @ApiPropertyOptional()
    licFolioTra:string;
    @ApiPropertyOptional()
    licLetraFolio: string;
    @ApiPropertyOptional()
    licLetraFolioIni: string;
    @ApiPropertyOptional()
    licLetraFolioTra: string;
    @ApiPropertyOptional()
    licObservaciones: string;
    @ApiPropertyOptional()
    licRun: string;
    @ApiPropertyOptional()
    licRunEscuela: string;
    @ApiPropertyOptional()
    licTipo: string;

}