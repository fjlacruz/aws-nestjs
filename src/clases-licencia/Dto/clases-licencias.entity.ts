import { ApiPropertyOptional } from "@nestjs/swagger";

export class ClasesLicenciasDTO {

    @ApiPropertyOptional()
    idClaseLicencia:number;

    @ApiPropertyOptional()
    Nombre:string;

    @ApiPropertyOptional()
    Profesional:boolean;

    @ApiPropertyOptional()
    Abreviacion:string;

    @ApiPropertyOptional()
    Descripcion:string;

    @ApiPropertyOptional()
    Restricciones:string;

    @ApiPropertyOptional()
    Folio:string;

    @ApiPropertyOptional()
    LetrasFolio:string;
}
