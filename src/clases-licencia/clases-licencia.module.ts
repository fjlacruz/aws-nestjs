import { UsersModule } from './../users/users.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { ClasesLicenciaController } from './controller/clases-licencia.controller';
import { ClasesLicenciasEntity } from './entity/clases-licencias.entity';
import { ClasesLicenciaService } from './services/clases-licencia.service';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { jwtConstants } from 'src/users/constants';
import { User } from 'src/users/entity/user.entity';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { DocumentosLicenciaRestriccionEntity } from './entity/documentos-licencia-restriccion.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { ServicioGenerarLicenciasService } from 'src/shared/services/ServiciosComunes/ServicioGenerarLicencias.services';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ClasesLicenciasEntity,
      DocumentosLicencia,
      TramitesClaseLicencia,
      User,
      RolesUsuarios,
      DocumentosLicenciaRestriccionEntity,
      PostulantesEntity,
      TiposTramiteEntity,
      ConductoresEntity,
      Postulantes,
      ComunasEntity,
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '360d' },
    }),
  ],
  providers: [ClasesLicenciaService, AuthService, ServicioGenerarLicenciasService, IngresoPostulanteService],
  exports: [ClasesLicenciaService, ServicioGenerarLicenciasService],
  controllers: [ClasesLicenciaController],
})
export class ClasesLicenciaModule {}
