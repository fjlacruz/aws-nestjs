import { Test, TestingModule } from '@nestjs/testing';
import { ClasesLicenciaController } from './clases-licencia.controller';

describe('ClasesLicenciaController', () => {
  let controller: ClasesLicenciaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClasesLicenciaController],
    }).compile();

    controller = module.get<ClasesLicenciaController>(ClasesLicenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
