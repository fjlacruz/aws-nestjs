import { Controller } from '@nestjs/common';
import { Get, Param, Req } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { ClasesLicenciaService } from '../services/clases-licencia.service';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { UsersService } from 'src/users/services/users.service';

@ApiTags('clases-licencias')
@Controller('clases-licencias')
export class ClasesLicenciaController {
  constructor(private readonly clasesLicenciaService: ClasesLicenciaService, private readonly authService: AuthService) {}

  @Get('getClasesLicenciasByConductor/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las clases de licencias de un conductor' })
  async getClasesLicenciasByConductor(@Param('id') id: number) {
    const data = await this.clasesLicenciaService.getClasesLicenciasByConductor(id);
    return { data };
  }

  @Get('getClasesLicenciasByPostulante/:RUN')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve clases de licencias de un postulante' })
  async getClasesLicenciasByPostulante(@Param('RUN') run: string) {
    const data = await this.clasesLicenciaService.getClasesLicenciasByPostulante(run);
    return { data };
  }

  @Get('getLicenciasByPostulante/:RUN')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve clases de licencias de un postulante con el Id Tramite al que pertenece' })
  async getLicenciasByPostulante(@Param('RUN') run: number, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];
    
    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.PerfildeUsuarioLicenciasAntiguedadVisualizarLicenciasAntiguedad]);
    
    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);
    
    if (validarPermisos.ResultadoOperacion) {

      const data = await this.clasesLicenciaService.getLicenciasByPostulante(run);
      return { data };

    } else {
      return { data: validarPermisos };
    }
  }

  //Añadir comprobacion token rol
  @Get('getAllClasesLicencias')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todas las clases de licencias' })
  async getConsolidados(@Req() req: any) {
    // let splittedBearerToken = req.headers.authorization.split(' ');
    // let token = splittedBearerToken[1];
    
    // let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaLicencias]);
    
    // let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);
    
    // if (validarPermisos.ResultadoOperacion) {

      const data = await this.clasesLicenciaService.getAllClasesLicencias();
      return { data };

    // } else {
    //   return { data: validarPermisos };
    // }
  }

  @Get('getClasesLicenciasListaNombres')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todas las clases de licencias, lista de id y nombre' })
  async getClasesLicenciasListaNombres(@Req() req: any) {
    // let splittedBearerToken = req.headers.authorization.split(" ");
    // let token = splittedBearerToken[1];

    // let tokenPermiso = new TokenPermisoDto(token, [
    //     PermisosNombres.VisualizarTiposTramitesLicencias
    // ]);
    
    // let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    // if (validarPermisos.ResultadoOperacion) {
        
        const data = await this.clasesLicenciaService.getClasesLicenciasListaNombres();
        return { data };


    // } else {
    //   return { data: validarPermisos };
    // }
  }

  @Get('getById/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todas las clases de licencias' })
  async getById(@Param('id') id: number) {
    return { data: await this.clasesLicenciaService.getById(Number(id)) };
  }

  @Get('allClaseLicenciaWithDocumentoLicencia/:RUN')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las clase de licencia asociado a los documentos de licencia creados por RUN postulante' })
  async getClaseLicenciaWithDocumentoLicencia(@Param('RUN') run: number) {
    const data = await this.clasesLicenciaService.fetch(run);

    return { data };
  }

  @Get('getFotosLicenciaByTramiteId/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las 2 imagenes de una licencias por id del Tramite' })
  async getFotosLicenciaByTramiteId(@Param('id') id: number, @Req() req: any) {

      const data = await this.clasesLicenciaService.getFotosLicenciaByTramiteId(req, id);

      return { data };
  }

  @Get('documentos-completos/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el documento completo de la licencia por run' })
  async getDocumentosCompletosPorRUN(@Param('run') run: string) {
    var separaRUT = run.split('-');
    let rut = +separaRUT[0];
    const data = await this.clasesLicenciaService.getDocumentosCompletosPorRUN(rut);
    return { data };
  }
}
