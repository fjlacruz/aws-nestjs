import { ApiProperty } from '@nestjs/swagger';
import { ClaseLicenciaRequeridaTipoTramite } from 'src/ClaseLicenciaRequeridaTipoTramite/entity/claseLicenciaRequeridaTipoTramite.entity';
import { DocumentosLicenciaClaseLicenciaEntity } from 'src/documento-licencia/entity/documento-licencia.entity';
import { TipoDeTramiteClaseLicenciaInstitucionEntity } from 'src/tipo-de-tramite-clase-licencia-institucion/entity/tipo-de-tramte-clase-licencia-institucion.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { Entity, Column, PrimaryColumn, OneToOne, OneToMany, ManyToOne } from 'typeorm';
import { DocumentosLicenciaRestriccionEntity } from './documentos-licencia-restriccion.entity';

@Entity('ClasesLicencias')
export class ClasesLicenciasEntity {
  @PrimaryColumn()
  idClaseLicencia: number;

  @ApiProperty()
  @OneToMany(() => DocumentosLicenciaClaseLicenciaEntity, documentoLicenciaClaseLicencia => documentoLicenciaClaseLicencia.clasesLicencias)
  readonly documentoLicenciaClaseLicencia: DocumentosLicenciaClaseLicenciaEntity[];

  @Column()
  Nombre: string;

  @Column()
  Profesional: boolean;

  @Column()
  Abreviacion: string;

  @Column()
  Descripcion: string;

  @ApiProperty()
  @OneToOne(() => TramitesClaseLicencia, tramitesClaseLicencia => tramitesClaseLicencia.clasesLicencias)
  readonly tramitesClaseLicencia: TramitesClaseLicencia;

  @ApiProperty()
  @OneToMany(() => ClaseLicenciaRequeridaTipoTramite, licenciaRequerida => licenciaRequerida.clasesLicencias)
  readonly licenciaRequerida: ClaseLicenciaRequeridaTipoTramite[];

  @ApiProperty()
  @OneToMany(
    () => TipoDeTramiteClaseLicenciaInstitucionEntity,
    tipoTramiteLicenciaInstitucion => tipoTramiteLicenciaInstitucion.clasesLicencias
  )
  readonly tipoTramiteLicenciaInstitucion: TipoDeTramiteClaseLicenciaInstitucionEntity[];
}
