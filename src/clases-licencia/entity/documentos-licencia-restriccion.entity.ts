import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { RestriccionesEntity } from './restricciones.entity';

@Entity('DocumentosLicenciaRestriccion')
export class DocumentosLicenciaRestriccionEntity {
  @PrimaryGeneratedColumn()
  idDocumentosLicenciaRestriccion: number;

  @Column()
  idDocumentoLicencia: number;

  @Column()
  idrestriccion: number;

  @ManyToOne(() => RestriccionesEntity, restriccion => restriccion.idrestriccion)
  @JoinColumn({ name: 'idrestriccion' })
  readonly restricciones: RestriccionesEntity;

  @ManyToOne(() => DocumentosLicencia, documentoLicencia => documentoLicencia.documentoLicenciaRestricciones)
  @JoinColumn({ name: 'idDocumentoLicencia' })
  readonly documentoLicencia: DocumentosLicencia;
}
