import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { DocumentosLicenciaRestriccionEntity } from "./documentos-licencia-restriccion.entity";
import { InfraccionEntity } from '../../infraccion/infraccion.entity';
import { EstadosEDDEntity } from '../../estados-EDD/entity/estados-EDD.entity';


@Entity('DocumentosLicencia')
export class DocumentosLicenciaEntity {

    @PrimaryColumn()
    idDocumentoLicencia:number;

    @Column()
    created_at:Date;

    @Column()
    update_at:Date;

    @Column()
    idClaseLicencia:number;

    @Column()
    idPapelSeguridad:number;

    @Column()
    Folio:string;

    @Column()
    idUsuario:number;

    @Column()
    idConductor:number;

    @Column()
    idPostulante:number;

    @Column()
    IdEstadoEDF:number;

    @Column()
    IdEstadoEDD:number;

    @JoinColumn({name: 'IdEstadoEDD'})
    @ManyToOne(() => EstadosEDDEntity, roles => roles.IdEstadoEDD, { eager: false })
    readonly estadosEDDEntity: EstadosEDDEntity;

    @Column()
    idEstadoPCL:number;

    @Column()
    idImagenLicencia:number;

    @Column()
    idInstitucion: number;

    @Column()
    fechaVencimiento: Date;

    @Column()
    fechacaducidad: Date;

    @Column()
    fechaPrimerOtorgamiento: Date;

    @Column()
    fechaOtorgamientoActual: Date;

    @OneToMany(() => DocumentosLicenciaRestriccionEntity, restriccion => restriccion.idDocumentoLicencia)
    restricciones: DocumentosLicenciaRestriccionEntity[]; 

}
