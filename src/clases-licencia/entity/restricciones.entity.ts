import { ApiProperty } from "@nestjs/swagger";
import { DocumentosLicenciaRestriccionEntity } from "src/documento-licencia-restriccion/entity/documento-licencia-restriccion.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'restricciones' )
export class RestriccionesEntity
{
    @PrimaryGeneratedColumn()
    idrestriccion: number;

    @Column()
    nombre: string;

    @Column()
    descripcion: string;

    @Column()
    codrc: string;

    @ApiProperty()
    @OneToMany(() => DocumentosLicenciaRestriccionEntity, documentoLicenciaRestriccion => documentoLicenciaRestriccion.restricciones)
    readonly documentoLicenciaRestricciones: DocumentosLicenciaRestriccionEntity[];
}