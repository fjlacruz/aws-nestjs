import { Test, TestingModule } from '@nestjs/testing';
import { ClasesLicenciaService } from './clases-licencia.service';

describe('ClasesLicenciaService', () => {
  let service: ClasesLicenciaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClasesLicenciaService],
    }).compile();

    service = module.get<ClasesLicenciaService>(ClasesLicenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
