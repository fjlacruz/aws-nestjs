import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { ClasesLicenciasEntity } from '../entity/clases-licencias.entity';
import { DocumentosLicenciaEntity } from '../entity/documentos-licencia.entity';
import { getConnection } from 'typeorm';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { FotosTramiteLicenciaDto } from 'src/tramites-clase-licencia/DTO/fotosTramiteLicencia.dto';
import { Resultado } from 'src/utils/resultado';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { EstadosEDFEntity } from 'src/estados-EDF/entity/estados-EDF.entity';
import { EstadosEDDEntity } from 'src/estados-EDD/entity/estados-EDD.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { EstadosPCLEntity } from 'src/estados-PCL/entity/estados-PCL.entity';
import { DocumentosLicenciaRestriccionEntity } from '../entity/documentos-licencia-restriccion.entity';
import { RestriccionesEntity } from '../entity/restricciones.entity';
import { ObjetoListado } from 'src/utils/objetoListado';
import { DocumentosLicenciaClaseLicenciaEntity } from 'src/documento-licencia/entity/documento-licencia.entity';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { PapelSeguridadEntity } from 'src/papel-seguridad/entity/papel-seguridad.entity';
import { DocumentoLicenciaTramiteEntity } from 'src/documento-licencia-tramite/entity/documento-licencia-tramite.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { ComunasEntity } from '../../comunas/entity/comunas.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { OficinaEntity } from 'src/oficina/oficina.entity';
import { ServicioGenerarLicenciasService } from 'src/shared/services/ServiciosComunes/ServicioGenerarLicencias.services';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { RUTA_IMAGENES_FUENTES_LICENCIA, RUTA_IMAGENES_LICENCIA } from 'src/approutes.config';
import { ClasesLicenciasIds, PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { ClasesLicenciaRegistroCivilDto } from '../Dto/clases-licencia-reg-civil.dto';
import moment from 'moment';
import { ClasesLicenciasDTO } from '../Dto/clases-licencias.entity';
import { DatosClasesLicenciasConFechas } from 'src/tramites/DTO/datosClasesLicenciasConFechas.dto';

@Injectable()
export class ClasesLicenciaService {
  constructor(
    @InjectRepository(ClasesLicenciasEntity) private readonly clasesLicenciasRespository: Repository<ClasesLicenciasEntity>,
    @InjectRepository(TramitesClaseLicencia) private readonly tramitesClaseLicenciaRespository: Repository<TramitesClaseLicencia>,
    @InjectRepository(DocumentosLicencia) private readonly documentosLicenciaRespository: Repository<DocumentosLicencia>,
    @InjectRepository(PostulantesEntity)
    private readonly postulanteRespository: Repository<PostulantesEntity>,
    @InjectRepository(DocumentosLicenciaRestriccionEntity)
    private readonly documentosLicenciaRestriccionEntityRepository: Repository<DocumentosLicenciaRestriccionEntity>,
    private servicioGenerarLicenciasService: ServicioGenerarLicenciasService,
    private readonly authService: AuthService,
    private readonly ingresoAspiranteService: IngresoPostulanteService,
  ) {}

  async getClasesLicenciasByConductor(id: number) {
    const documentosLicenciaList = await getConnection()
      .createQueryBuilder()
      .select('DocumentosLicencia')
      .from(DocumentosLicenciaEntity, 'DocumentosLicencia')
      .where('DocumentosLicencia.idConductor = :id', { id: id })
      .getMany();

    if (!documentosLicenciaList) throw new NotFoundException('Clase Licencia dont exist');

    var aDocumentosLicencias = [];
    for (var i in documentosLicenciaList) {
      let documentosLicencia = documentosLicenciaList[i];
      const clasesLicencias = await this.getClasesLicencias(documentosLicencia.idClaseLicencia);
      aDocumentosLicencias.push(clasesLicencias);
    }

    return aDocumentosLicencias;
  }

  async getClasesLicenciasByPostulante(run: string) {
    //console.log(run);

    const postulante = await getConnection()
      .createQueryBuilder()
      .select('Postulantes')
      .from(Postulante, 'Postulantes')
      .where("Postulantes.RUN  || '-' || Postulantes.DV = :run", { run: run })
      .getOne();

    if (!postulante) throw new NotFoundException('Clase Licencia dont exist');

    const documentos = await this.documentosLicenciaRespository.find({
      where: { idPostulante: postulante.idPostulante },
      relations: ['documentoLicenciaClaseLicencia', 'estadosDD', 'estadosPCL', 'estadosDF', 'papelSeguridad', 'documentoLicenciaTramite'],
    });
    //console.log(documentos);

    if (!documentos) throw new NotFoundException('Clase Licencia dont exist');

    var aDocumentosLicencias = [];
    for (const i in documentos) {
      const documentosLicencia = documentos[i];
      const clasesLicencias = await this.getClasesLicencias(documentosLicencia.idClaseLicencia);
      clasesLicencias['documentoLicencia'] = documentosLicencia;
      const tipoDocumentoRestriccion = await this.documentosLicenciaRestriccionEntityRepository.find({
        where: { idDocumentoLicencia: documentos[i].idDocumentoLicencia },
        relations: ['restriccionesEntity'],
      });
      clasesLicencias['restricciones'] = tipoDocumentoRestriccion;

      aDocumentosLicencias.push(clasesLicencias);
    }

    return aDocumentosLicencias;
  }

  async getFotosLicenciaByTramiteId(req, idTramite: number) {
    const resultado: Resultado = new Resultado();

      try{

        // Comprobamos permisos
        let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaTramitesEnVisualizacionLicencia);
      
        // En caso de que el usuario no sea correcto
        if (!usuarioValidado.ResultadoOperacion) {
          return usuarioValidado;
        } 
        
        const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];  
      
        const tramiteClaseLicencia = await this.tramitesClaseLicenciaRespository.findOne({
          relations: [
            'tramite',
            'tramite.solicitudes',
            'tramite.solicitudes.estadosSolicitud',
            'tramite.solicitudes.postulante',
            'tramite.solicitudes.postulante.conductor',
            'tramite.solicitudes.postulante.comunas',
            'tramite.solicitudes.postulante.imagenesPostulante',
          ],
          where: qb => {
            qb.where('TramitesClaseLicencia.idTramite = :id', { id: idTramite });
          },
        });

        if (!tramiteClaseLicencia) {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'No se encontraron los datos para el trámite seleccionado.';

          return resultado;
        }

        // Comprobación de datos en registro civil
        //console.log(data);
        const runCompleto: string = tramiteClaseLicencia.tramite.solicitudes.postulante.RUN + '' + tramiteClaseLicencia.tramite.solicitudes.postulante.DV;

        const data = {idUsuario:rolesUsuarioRecuperados[0].idUsuario,run:runCompleto};

        let respuestaRegCivil = await this.ingresoAspiranteService.consulHis(data);

        if (respuestaRegCivil.respuesta.mensaje === 'Error en formato de datos' || respuestaRegCivil.respuesta.mensaje === 'TypeError') {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'Se ha producido un error en la recuperación de datos desde registro civil.';

          return resultado;         
        }

        // Recogemos la licencias que vienen de registro civil
        let licRegistroCivil = respuestaRegCivil.respuesta.Licencias;

        const mapeoLicenciasRegistroCivil : ClasesLicenciaRegistroCivilDto [] = this.mapearRespuestaLicenciasRegistroCivil(licRegistroCivil);

        const docLicencia: DocumentosLicencia = await this.documentosLicenciaRespository.findOne({
          relations: [
            'documentoLicenciaRestricciones',
            'documentoLicenciaRestricciones.restricciones',
            'imagenLicencia',
            'imagenLicencia.tipoImagenLicencia',
            'papelSeguridad',
            'papelSeguridad.lotepapelseguridad',
            'papelSeguridad.lotepapelseguridad.proveedor',
            'documentoLicenciaTramite',
            'documentoLicenciaTramite.tramites',
            'documentoLicenciaTramite.tramites.colaExaminacionNM',
            'documentoLicenciaTramite.tramites.colaExaminacionNM.colaExaminacion',
            'documentoLicenciaTramite.tramites.colaExaminacionNM.colaExaminacion.resultadoExaminacionCola',
            'documentoLicenciaTramite.tramites.colaExaminacionNM.colaExaminacion.resultadoExaminacionCola.resultadoExaminacion',
            'documentoLicenciaTramite.tramites.colaExaminacionNM.colaExaminacion.resultadoExaminacionCola.resultadoExaminacion.usuarioAprob',
            'usuario',
          ],
          where: qb => {
            qb.where('DocumentosLicencia.idConductor = :id AND DocumentosLicencia.fechacaducidad is null', {
              id: tramiteClaseLicencia.tramite.solicitudes.postulante.conductor.idConductor,
            });
          },
        });

        if (!docLicencia) {
          resultado.Error = 'Se ha producido un error al obtener el documento de licencia.';
          resultado.ResultadoOperacion = false;

          return resultado;
        }

        let fotosLicencia: FotosTramiteLicenciaDto = new FotosTramiteLicenciaDto();

        docLicencia.imagenLicencia.forEach(imagenLicencia => {
          if (imagenLicencia.tipoImagenLicencia.Nombre == 'Física' && imagenLicencia.numeroImagen == 0)
            fotosLicencia.fotoFrontalLicencia = imagenLicencia.fileBinary.toString();
          else if (imagenLicencia.tipoImagenLicencia.Nombre == 'Física' && imagenLicencia.numeroImagen == 1)
            fotosLicencia.fotoTraseraLicencia = imagenLicencia.fileBinary.toString();
        });

        fotosLicencia.letrasFolio = docLicencia?.papelSeguridad?.LetrasFolio;
        fotosLicencia.folio = docLicencia.papelSeguridad?.Folio;

        // Obtenemos el postulante para recoger los datos de la licencia
        const postulante: PostulantesEntity = await this.postulanteRespository.findOne({
          relations: [
            'comunas',
            'imagenesPostulante',
            'conductor',
            'conductor.documentosLicencia',
            'conductor.documentosLicencia.documentoLicenciaClaseLicencia',
            'conductor.documentosLicencia.documentoLicenciaClaseLicencia.clasesLicencias',
          ],
          where: qb => {
            qb.where('PostulantesEntity.idPostulante = :_idPostulante', {
              _idPostulante: tramiteClaseLicencia.tramite.solicitudes.postulante.idPostulante,
            }).andWhere('PostulantesEntity__conductor__documentosLicencia.fechacaducidad is null');
          },
        });

        if (!postulante) {
          resultado.Error = 'Se ha producido un error al obtener el postulante.';
          resultado.ResultadoOperacion = false;

          return resultado;
        }

        // Obtenemos los datos de la última licencia anterior (puede no haber)
        const documentoLicenciaAnterior: DocumentosLicencia = await this.documentosLicenciaRespository.findOne({
          relations: [
            'documentoLicenciaClaseLicencia',
            'documentoLicenciaClaseLicencia.clasesLicencias',
            'papelSeguridad',
            'papelSeguridad.lotepapelseguridad',
            'papelSeguridad.lotepapelseguridad.proveedor',
            'documentoLicenciaTramite',
            'documentoLicenciaTramite.tramites',
            'documentoLicenciaTramite.tramites.tramitesClaseLicencia',
            'documentoLicenciaTramite.tramites.tramitesClaseLicencia.clasesLicencias',
          ],
          where: qb => {
            qb.where('DocumentosLicencia.idConductor = :_idConductor', { _idConductor: postulante.conductor.idConductor }).andWhere(
              'DocumentosLicencia.fechacaducidad IS NOT NULL'
            );
          },
          order: { fechacaducidad: 'DESC' },
        });

        let papelSeguridadCompleto = docLicencia.papelSeguridad.LetrasFolio + ' ' + docLicencia.papelSeguridad.Folio;

        let datosClasesLicenciasConFechas: DatosClasesLicenciasConFechas[] = await this.servicioGenerarLicenciasService.obtenerDatosClasesLicenciasFechas(
          postulante,
          papelSeguridadCompleto,
          mapeoLicenciasRegistroCivil
        );

        docLicencia.imagenLicencia.forEach(imagenLicencia => {
          if (imagenLicencia.tipoImagenLicencia.Nombre == 'Física' && imagenLicencia.numeroImagen == 0)
            fotosLicencia.fotoFrontalLicencia = imagenLicencia.fileBinary.toString();
          else if (imagenLicencia.tipoImagenLicencia.Nombre == 'Física' && imagenLicencia.numeroImagen == 1)
            fotosLicencia.fotoTraseraLicencia = imagenLicencia.fileBinary.toString();
        });

        fotosLicencia.letrasFolio = docLicencia.papelSeguridad.LetrasFolio;
        fotosLicencia.folio = docLicencia.papelSeguridad.Folio;

        // Recogiendo datos para el QR
        const cadenaDatosConHash: string = this.construirQRLicencia(tramiteClaseLicencia.tramite, docLicencia);
        const resultadoQR = await this.crearCodigoQR(cadenaDatosConHash);

        // Mapeamos las clases de licencia
        let clasesLicenciasAnterioresDto : ClasesLicenciasDTO [] = this.mapearClasesLicenciasAnteriores(documentoLicenciaAnterior, mapeoLicenciasRegistroCivil, datosClasesLicenciasConFechas);

        if (resultadoQR.ResultadoOperacion == true) {
          // Recuperamos la imagen origen del F8
          var Jimp = require('jimp');
          console.log('>>>>>>>>', __dirname);
          const image = await Jimp.read(RUTA_IMAGENES_LICENCIA + 'FORMF8ENBLANCOEXPORTADO.png');

          // Rutas para las fuentes
          const imageFontFGB28Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'FRABK_28.fnt';
          // const imageFontFGB30Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'FRABK_30.fnt';
          // const imageFontFGB32Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'FRABK_32.fnt';
          const imageFontCourier34Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'cour_32.fnt';
          const imageRobotoRegular32Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'Roboto-Regular_32.fnt';
          //const imageCourierNew42Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'Courier-New_42.fnt';
          const imageCourierNew34Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'Courier-New_34.fnt';
          const imageCourierNew14Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'Courier-New_14.fnt';
          const imageCourierNew38Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'Courier-New_38.fnt';
          const imageCourierNew18Route = RUTA_IMAGENES_FUENTES_LICENCIA + 'Courier-New_18.fnt';


          let formf8 = this.servicioGenerarLicenciasService.obtenerFormularioF8(
            tramiteClaseLicencia,
            docLicencia,
            resultadoQR.Respuesta.toString(),
            datosClasesLicenciasConFechas,
            clasesLicenciasAnterioresDto,
            image,
            Jimp,
            imageFontFGB28Route,
            imageFontCourier34Route,
            imageRobotoRegular32Route,
            imageCourierNew34Route,
            imageCourierNew14Route,
            imageCourierNew38Route,
            imageCourierNew18Route
          );

          const imageNegativo = await Jimp.read(RUTA_IMAGENES_LICENCIA + 'FORMF8NEGATIVOENBLANCOEXPORTADO.png');

          let formf8negativo = this.servicioGenerarLicenciasService.obtenerFormularioF8(
            tramiteClaseLicencia,
            docLicencia,
            resultadoQR.Respuesta.toString(),
            datosClasesLicenciasConFechas,
            clasesLicenciasAnterioresDto,
            imageNegativo,
            Jimp,
            imageFontFGB28Route,
            imageFontCourier34Route,
            imageRobotoRegular32Route,
            imageCourierNew34Route,
            imageCourierNew14Route,
            imageCourierNew38Route,
            imageCourierNew18Route
          );

          const imageAnversoLicencia = await Jimp.read(RUTA_IMAGENES_LICENCIA + 'FORMANVERSOLICENCIAENBLANCO.png');

          let formAnversoLicencia = this.servicioGenerarLicenciasService.obtenerAnversoLicencia(
            Jimp,
            imageAnversoLicencia,
            tramiteClaseLicencia,
            resultadoQR.Respuesta.toString(),
            imageFontFGB28Route,
            imageFontCourier34Route,
            docLicencia
          );

          const imageReversoLicencia = await Jimp.read(RUTA_IMAGENES_LICENCIA + 'FORMREVERSOLICENCIAENBLANCO.png');

          let formReversoLicencia = this.servicioGenerarLicenciasService.obtenerReversoLicencia(
            Jimp,
            imageReversoLicencia,
            datosClasesLicenciasConFechas,
            imageFontFGB28Route,
            docLicencia,
            imageCourierNew34Route,
            imageCourierNew14Route
          );

          fotosLicencia.fotoF8LicenciaBase64 = await formf8;
          fotosLicencia.fotoF8LicenciaNegativoBase64 = await formf8negativo;
          fotosLicencia.fotoAnversoLicenciaBase64 = await formAnversoLicencia;
          fotosLicencia.fotoReversoLicenciaBase64 = await formReversoLicencia;

          fotosLicencia.codigoQR = resultadoQR.Respuesta.toString();
          resultado.Respuesta = fotosLicencia;
          resultado.ResultadoOperacion = true;
        } else {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'Error creado QR Code';
        }

        return resultado;
    }
    catch(Error){
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Ha ocurrido un error, por favor, contacte con el administrador.'; 

      return resultado;       
    }
  }

  private construirQRLicencia(tramite: TramitesEntity, docLicencia: DocumentosLicencia): string {
    const RUN = tramite.solicitudes.postulante.RUN + '-' + tramite.solicitudes.postulante.DV;
    const folio = docLicencia.papelSeguridad.Folio;
    const llave = '';
    const cadenaDatos = RUN + ';' + folio + ';' + docLicencia.idConductor + ';' + llave;

    // Se codifican los datos
    var crypto = require('crypto');
    const hash = crypto.createHash('sha256').update(cadenaDatos).digest('hex');

    // Se preparan los datos para el codigo QR
    const cadenaDatosConHash = RUN + ';' + folio + ';' + hash;

    return cadenaDatosConHash;
  }

  async crearCodigoQR(datosConHash: string) {
    const resultado: Resultado = new Resultado();

    var QRCode = require('qrcode');
    await QRCode.toDataURL(datosConHash, {
      errorCorrectionLevel: 'M',
    })
      .then(imgSRC => {
        resultado.Respuesta = imgSRC;
        resultado.Mensaje = 'Código QR generado correctamente';
        resultado.ResultadoOperacion = true;
      })
      .catch(err => {
        //console.error(err);

        resultado.Error = 'Error creando código QR';
        resultado.ResultadoOperacion = false;
      });

    return resultado;
  }

  async getLicenciasByPostulante(run: number) {
    const resultado: Resultado = new Resultado();

    try {
      //console.log(run);

      const tramitesConLicencias = await this.tramitesClaseLicenciaRespository.find({
        join: {
          alias: 'tramitesClaseLicencia',
          innerJoinAndSelect: {
            tramites: 'tramitesClaseLicencia.tramite',
            clasesLicencias: 'tramitesClaseLicencia.clasesLicencias',
            solicitudes: 'tramites.solicitudes',
            postulante: 'solicitudes.postulante',
            tiposTramite: 'tramites.tiposTramite',
            estadoTramite: 'tramites.estadoTramite',
          },
        },
        where: qb => {
          qb.where("postulante.RUN || '-' || postulante.DV = :RUN", {
            RUN: run,
          });
        },
      });

      //console.log(tramitesConLicencias);
    } catch (error) {
      //console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }

  async getClasesLicencias(id: number) {
    const clasesLicencias = await this.clasesLicenciasRespository.findOne(id);
    if (!clasesLicencias) throw new NotFoundException('Clase Licencia dont exist');

    return clasesLicencias;
  }

  async getAllClasesLicencias() {
    return await this.clasesLicenciasRespository.find();
  }

  async getClasesLicenciasListaNombres() {
    const resultado: Resultado = new Resultado();
    let resultadoLicencias: ClasesLicenciasEntity[] = [];
    let resultadosDTO: ObjetoListado[] = [];

    try {
      resultadoLicencias = await this.clasesLicenciasRespository.find();

      if (Array.isArray(resultadoLicencias) && resultadoLicencias.length) {
        resultadoLicencias.forEach(item => {
          let nuevoElemento: ObjetoListado = {
            id: item.idClaseLicencia,
            Nombre: item.Abreviacion,
          };

          resultadosDTO.push(nuevoElemento);
        });
      }

      if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
        resultado.Respuesta = resultadosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Clases Licencias obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Clases Licencias';
      }
    } catch (error) {
      //console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Clases Licencias';
    }

    return resultado;
  }

  async fetch(run: number): Promise<any> {
    return await this.clasesLicenciasRespository
      .createQueryBuilder('ClasesLicencias')
      .innerJoinAndMapMany(
        'ClasesLicencias.idClaseLicencia',
        DocumentosLicenciaEntity,
        'doc',
        'ClasesLicencias.idClaseLicencia = doc.idClaseLicencia'
      )
      .innerJoinAndMapMany('doc.idPostulante', Postulante, 'pos', 'doc.idPostulante = pos.idPostulante')
      .where('pos.RUN = :run', {
        run,
      })
      .getMany();
  }

  async getById(id: number) {
    return await this.clasesLicenciasRespository.findOne(id);
  }

  async getDocumentosCompletosPorRUN(run: number): Promise<any> {
    const join = [];
    const repo = getRepository(DocumentosLicenciaEntity);
    const licencias = await repo
      .createQueryBuilder('doc')
      .innerJoinAndMapMany(
        'doc.idClaseLicencia',
        DocumentosLicenciaClaseLicenciaEntity,
        'dlcl',
        'doc.idDocumentoLicencia = dlcl.idDocumentoLicencia'
      )
      .innerJoinAndMapOne('dlcl.idClaseLicencia', ClasesLicenciasEntity, 'cla', 'dlcl.idClaseLicencia = cla.idClaseLicencia')
      .innerJoinAndMapOne('doc.idConductor', ConductoresEntity, 'cond', 'cond.idConductor    = doc.idConductor')
      .innerJoinAndMapOne('cond.idPostulante', Postulante, 'pos', 'pos.idPostulante    = cond.idPostulante')
      .innerJoinAndMapOne('doc.IdEstadoEDF', EstadosEDFEntity, 'edf', 'doc.IdEstadoEDF     = edf.IdEstadoEDF')
      .innerJoinAndMapOne('doc.IdEstadoEDD', EstadosEDDEntity, 'edd', 'doc.IdEstadoEDD     = edd.IdEstadoEDD')
      .innerJoinAndMapOne('doc.idEstadoPCL', EstadosPCLEntity, 'pcl', 'doc.idEstadoPCL     = pcl.idEstadoPCL')
      .leftJoinAndMapOne('doc.idPapelSeguridad', PapelSeguridadEntity, 'ps', 'doc.idPapelSeguridad     = ps.idPapelSeguridad')
      .leftJoinAndMapOne('doc.idInstitucion', InstitucionesEntity, 'mun', 'doc.idInstitucion   = mun.idInstitucion')
      .leftJoinAndMapOne('pos.idComuna', ComunasEntity, 'comu', 'pos.idComuna   = comu.idComuna')
      .leftJoinAndMapMany(
        'doc.restricciones',
        DocumentosLicenciaRestriccionEntity,
        'res',
        'doc.idDocumentoLicencia = res.idDocumentoLicencia'
      )

      .innerJoinAndMapMany(
        'doc.documentoLicenciaTramite',
        DocumentoLicenciaTramiteEntity,
        'documentoLicenciaTramite',
        'doc.idDocumentoLicencia = documentoLicenciaTramite.idDocumentoLicencia'
      )

      .innerJoinAndMapOne(
        'documentoLicenciaTramite.tramites',
        TramitesEntity,
        'tramites',
        'documentoLicenciaTramite.idTramite = tramites.idTramite'
      )

      .innerJoinAndMapOne('tramites.idSolicitud', Solicitudes, 'soli', 'tramites.idSolicitud = soli.idSolicitud')
      .innerJoinAndMapOne('soli.idOficina', OficinaEntity, 'ofi', 'soli.idOficina = ofi.idOficina')

      .innerJoinAndMapOne('doc.comuna', ComunasEntity, 'comuna', 'ofi.idComuna = comuna.idComuna')

      .where('pos.RUN = :run ', {
        run,
      })
      .andWhere('tramites.idEstadoTramite = 14') // 14 Recepción conforme
      .getMany();

    for (let licencia of licencias) {
      const restricciones = licencia.restricciones;
      const datos = [];

      for (let restriccion of restricciones) {
        datos.push(await getRepository(RestriccionesEntity).findOne(restriccion.idrestriccion));
      }
      licencia.restricciones = datos;
    }

    //const licenciasActivas = licencias.filter((licencia) =>{ return licencia.fechacaducidad === null});
    const licenciasActivas = [];
    licencias.forEach(licencia => {
      if (licencia.fechacaducidad == null) {
        licenciasActivas.push(licencia);
      }
    });
    licenciasActivas.forEach(lic => {
      // console.log("-------------------------------------------------")
      // console.log(lic.documentoLicenciaTramite)
      // console.log("-------------------------------------------")
      //lic.documentoLicenciaTramite[0].tramites.idSolicitud
    });

    return licenciasActivas;
  }

  mapearRespuestaLicenciasRegistroCivil(licRegistroCivil:any) : ClasesLicenciaRegistroCivilDto[]{
    let clasesLicRegistroCivil : ClasesLicenciaRegistroCivilDto[] = [];

    if(licRegistroCivil && licRegistroCivil.length > 0){
      licRegistroCivil.forEach(lrc => {
        if((lrc.licFechaPriLic && lrc.licFechaPriLic.length == 8) || (lrc.licFechaPrxCtl && lrc.licFechaPrxCtl.length == 8) || (lrc.licFechaUltLic && lrc.licFechaUltLic.length == 8)){
          let claseLicRegCivil : ClasesLicenciaRegistroCivilDto = new ClasesLicenciaRegistroCivilDto();

          claseLicRegCivil.licComunaTraLic = (lrc.licComunaTraLic)?lrc.licComunaTraLic:'';
          claseLicRegCivil.licComunaUltLic = (lrc.licComunaUltLic)?lrc.licComunaUltLic:'';
          claseLicRegCivil.licEstadoClaLic = (lrc.licEstadoClaLic)?lrc.licEstadoClaLic:'';
          claseLicRegCivil.licEstadoDigLic = (lrc.licEstadoDigLic)?lrc.licEstadoDigLic:'';
          claseLicRegCivil.licEstadoFisLic = (lrc.licEstadoFisLic)?lrc.licEstadoFisLic:'';
          claseLicRegCivil.licFechaPriLic = (lrc.licFechaPriLic && lrc.licFechaPriLic.length == 8)?this.convertirStringRegistroCivilEnFecha(lrc.licFechaPriLic):null;
          claseLicRegCivil.licFechaPrxCtl = (lrc.licFechaPrxCtl && lrc.licFechaPrxCtl.length == 8)?this.convertirStringRegistroCivilEnFecha(lrc.licFechaPrxCtl):null;
          claseLicRegCivil.licFechaTraLic = (lrc.licFechaTraLic)?lrc.licFechaTraLic:'';
          claseLicRegCivil.licFechaUltLic = (lrc.licFechaUltLic && lrc.licFechaUltLic.length == 8)?this.convertirStringRegistroCivilEnFecha(lrc.licFechaUltLic):null;
          claseLicRegCivil.licFolio = (lrc.licFolio)?lrc.licFolio:'';
          claseLicRegCivil.licFolioIni = (lrc.licFolioIni)?lrc.licFolioIni:'';
          claseLicRegCivil.licFolioTra = (lrc.licFolioTra)?lrc.licFolioTra:'';
          claseLicRegCivil.licLetraFolio = (lrc.licLetraFolio)?lrc.licLetraFolio:'';
          claseLicRegCivil.licLetraFolioIni = (lrc.licLetraFolioIni)?lrc.licLetraFolioIni:'';
          claseLicRegCivil.licLetraFolioTra = (lrc.licLetraFolioTra)?lrc.licLetraFolioTra.trim():'';
          claseLicRegCivil.licObservaciones = (lrc.licObservaciones)?lrc.licObservaciones:'';
          claseLicRegCivil.licRun = (lrc.licRun)?lrc.licRun:'';
          claseLicRegCivil.licRunEscuela = (lrc.licRunEscuela)?lrc.licRunEscuela:'';
          claseLicRegCivil.licTipo = (lrc.licTipo)?lrc.licTipo.trim():'';

          clasesLicRegistroCivil.push(claseLicRegCivil);
        }
      })
    }

    return clasesLicRegistroCivil;
  }

  convertirStringRegistroCivilEnFecha(fechaString:string):Date{
    let fechaParaAsignar : Date = null;

    if(fechaString.length != 8){
      return fechaParaAsignar;
    }

    try{
      const anyoFecha : number = parseInt(fechaString.substring(0,4));
      const mesFecha : number = parseInt(fechaString.substring(4,6));
      const diaFecha : number = parseInt(fechaString.substring(6,8));

      fechaParaAsignar = new Date(anyoFecha, mesFecha, diaFecha)

      return fechaParaAsignar;
    }
    catch(Error){
      return null;
    }

  }

  mapearClasesLicenciasAnteriores(documentoLicenciaAnterior : DocumentosLicencia, mapeoLicenciasRegistroCivil : ClasesLicenciaRegistroCivilDto [], datosClasesLicenciasConFechas: DatosClasesLicenciasConFechas[]) : ClasesLicenciasDTO[]{
    // Inciamos la respuesta
    let respuestaClasesLicencias : ClasesLicenciasDTO[] = [];

    if(documentoLicenciaAnterior && documentoLicenciaAnterior.documentoLicenciaTramite && documentoLicenciaAnterior.documentoLicenciaTramite.length > 0){
      
      documentoLicenciaAnterior.documentoLicenciaTramite.forEach(dlt => {
        let clAux : ClasesLicenciasDTO = new ClasesLicenciasDTO();

        clAux.Abreviacion = dlt.tramites.tramitesClaseLicencia.clasesLicencias.Abreviacion;
        clAux.Nombre = dlt.tramites.tramitesClaseLicencia.clasesLicencias.Abreviacion;
        clAux.Folio = documentoLicenciaAnterior.papelSeguridad.Folio;
        clAux.LetrasFolio = documentoLicenciaAnterior.papelSeguridad.LetrasFolio;

        respuestaClasesLicencias.push(clAux);
      })

    }

    // En caso de que no exista la licencia incluída por el sgl la incluímos desde registro civil
    mapeoLicenciasRegistroCivil.forEach(lrc => {

      let recuperacionLicenciaSGL = respuestaClasesLicencias.filter(clf => clf.Abreviacion.toUpperCase() == lrc.licTipo.toUpperCase());

      // Recoger valores de las clases de licencias para luego comprobar si esta se va a asignar
      const keysClasesLicencias = Object.keys(ClasesLicenciasIds);

      if(keysClasesLicencias.includes(lrc.licTipo.toUpperCase())){

        let idClaseLicenciaAsociada = ClasesLicenciasIds[lrc.licTipo.toUpperCase()];

        // Si no existe en el sgl procedemos
        if(datosClasesLicenciasConFechas.filter(dclcf => dclcf.idClaseLicencia == idClaseLicenciaAsociada && dclcf.asignadoRC == true).length > 0)
          if(!recuperacionLicenciaSGL || recuperacionLicenciaSGL.length == 0){
        
            let clAux : ClasesLicenciasDTO = new ClasesLicenciasDTO();

            clAux.Abreviacion = lrc.licTipo.toUpperCase();
            clAux.Nombre = lrc.licTipo.toUpperCase();
            clAux.Folio = lrc.licFolio;
            clAux.LetrasFolio = lrc.licLetraFolio;

            respuestaClasesLicencias.push(clAux);

          }
      }

    })

    return respuestaClasesLicencias;
  }

}
