import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { TiposDocumentoEntity } from 'src/tipos-documento/entity/tipos-documento.entity';
import { User } from 'src/users/entity/user.entity';
import { ClaveUnicaController } from './controller/clave-unica.controller';
import { ClaveUnicaService } from './services/clave-unica.service';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([User, RolesUsuarios, TiposDocumentoEntity])],
  controllers: [ClaveUnicaController],
  providers: [ClaveUnicaService, AuthService], 
})
export class ClaveUnicaModule{}
