import { Test, TestingModule } from '@nestjs/testing';
import { ClaveUnicaController } from './clave-unica.controller';

describe('ClaveUnicaController', () => {
  let controller: ClaveUnicaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClaveUnicaController],
    }).compile();

    controller = module.get<ClaveUnicaController>(ClaveUnicaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
