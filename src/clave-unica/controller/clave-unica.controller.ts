import { Controller, Get, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/auth/guards/auth.guard';
import { ClaveUnicaService } from '../services/clave-unica.service';

@Controller('clave-unica')
@ApiTags('Clave-Unica')
export class ClaveUnicaController 
{
    constructor( private claveUnicaService: ClaveUnicaService )
    {}

    // @Get( 'token/:code/:state' )
    // @Public()
    // @ApiOperation({ summary: 'Servicio que trae el token de clave unica' })
    // async getToken( @Param( 'code' ) code: string, @Param( 'state' ) state: string )
    // {
    //     return await this.claveUnicaService.getAccessToken( code, state );
    // }

    // @Get( 'user-info/:token' )
    // @Public()
    // @ApiOperation({ summary: 'Servicio que trae los datos del postulante de clave unica' })
    // async getUserInfo( @Param( 'token' ) token: string )
    // {
    //     return await this.claveUnicaService.getUserInfo( token );
    // }
    
    @Get( 'login/:code/:state' )
    @Public()
    @ApiOperation({ summary: 'Servicio logea un usuario con clave unica' })
    async getLogin( @Param( 'code' ) code: string, @Param( 'state' ) state: string )
    {
        return await this.claveUnicaService.login( code, state );
    }
}
