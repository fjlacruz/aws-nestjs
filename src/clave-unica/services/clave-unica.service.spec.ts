import { Test, TestingModule } from '@nestjs/testing';
import { ClaveUnicaService } from './clave-unica.service';

describe('ClaveUnicaService', () => {
  let service: ClaveUnicaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClaveUnicaService],
    }).compile();

    service = module.get<ClaveUnicaService>(ClaveUnicaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
