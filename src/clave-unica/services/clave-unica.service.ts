import { HttpService, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/auth/auth.service';
import { cuConfig } from '../cu.config';

const API_CLIENTE_ID        = cuConfig.cliente_id;
const API_CLIENTE_SECRET    = cuConfig.cliente_secret;
const API_REDIRECT          = cuConfig.redirect_url;
const API_URL               = cuConfig.url;

@Injectable()
export class ClaveUnicaService 
{
    constructor( private readonly httpService: HttpService, private authService: AuthService )
    {}

    private async getAccessToken( code: string, state: string ) 
    {
      const data    = "client_id="      + API_CLIENTE_ID +
                      "&client_secret=" + API_CLIENTE_SECRET +
                      "&redirect_uri="  + encodeURIComponent(API_REDIRECT) +
                      "&grant_type="    + 'authorization_code' +
                      "&code="          + code +
                      "&state="         + state; 
      const url     = API_URL + '/token/';
      const headers = 
      {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      return await this.getResponse( this.httpService.post( url, data, { headers }));
    }
    private async getUserInfo( token: string )
    {
      const url     = API_URL + '/userinfo/';
      const headers = 
      {
        'Accept': 'application/json',
        'Content-Type': 'application/json;',
        'Authorization': 'Bearer ' + token 
      };
      return await this.getResponse( this.httpService.post( url, null, { headers } ));
    }
    private async getResponse( call: Observable<any> )
    {
        return await call.pipe( map( response => response.data ) ).toPromise().then( response => response ).catch( error => console.log( error.response.data ));
    }
    async login( code: string, state: string )
    {
      const response = await this.getAccessToken( code, state );

      if( response )
      {
        const userInfo = await this.getUserInfo( response.access_token );
  
        if( userInfo )
        {
            const user          = userInfo.RolUnico.numero
            const access_token  = await this.authService.login( user );
  
            return { access_token, user };
        }
      }
      return null;
    }
}
