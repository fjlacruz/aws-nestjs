import { ApiPropertyOptional } from '@nestjs/swagger';

export class PostulanteHistorialIdoneidadMoral {
  constructor() {}

  @ApiPropertyOptional()
  run: string;

  @ApiPropertyOptional()
  nombrePostulante;

  @ApiPropertyOptional()
  idSolicitud: number;

  @ApiPropertyOptional()
  idTramites: PostulanteTramites[];

  @ApiPropertyOptional()
  clasesLicencias: string[];

  @ApiPropertyOptional()
  fechaInicio: Date;

  @ApiPropertyOptional()
  idEstadoTramite: number;

  @ApiPropertyOptional()
  estadoExaminacion: boolean;

  @ApiPropertyOptional()
  EstadoAlerta: string;

  @ApiPropertyOptional()
  runFormateado: string;

  @ApiPropertyOptional()
  estadoExaminacionAprobado: boolean;
}

export class PostulanteTramites {
  @ApiPropertyOptional()
  idTramite: number;

  @ApiPropertyOptional()
  estadoTramiteString: string;

  @ApiPropertyOptional()
  idTipoTramite: string;

  @ApiPropertyOptional()
  tipoTramiteString: string;
}
