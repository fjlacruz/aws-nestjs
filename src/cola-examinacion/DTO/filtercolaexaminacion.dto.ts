import { ApiPropertyOptional } from '@nestjs/swagger';
import { isEmpty } from 'rxjs/operators';

export class CreateColaExaminacionDto {
  @ApiPropertyOptional()
  idTipoExaminacion: number;

  @ApiPropertyOptional()
  RUN: string;

  @ApiPropertyOptional()
  Nombres: string;

  @ApiPropertyOptional()
  idColaExaminacion: string;

  @ApiPropertyOptional()
  idMunicipalidad: number;

  @ApiPropertyOptional()
  Nomclaselic: string;

  @ApiPropertyOptional()
  Nomet: string;

  @ApiPropertyOptional()
  idTipoExam: number;

  @ApiPropertyOptional()
  fechaIngresoResultado: any;

  @ApiPropertyOptional()
  fechaRendicionExamen: Date;

  @ApiPropertyOptional()
  nomExaminadorPractico: number;

  // @ApiPropertyOptional()
  // idMunicipalidad: number;

  @ApiPropertyOptional()
  resultadoExamen: string;

  @ApiPropertyOptional()
  idEstadosExaminacion: number;

  @ApiPropertyOptional()
  numeroSolicitud: string;

  @ApiPropertyOptional()
  numeroTramite: string;

  @ApiPropertyOptional()
  oportunidad: string;

  @ApiPropertyOptional()
  idResultadoExam: string;
}
