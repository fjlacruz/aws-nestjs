export class ResultadoExaminacionTramiteDTO {
    idTramite: number;
    claseLicencia: string;

    resultadoExaminacionPreexamIdonMoral: string;
    resultadoExaminacionIdonMoral: string;
    resultadoExaminacionMedica: string;
    resultadoExaminacionTeorica: string;
    resultadoExaminacionPractica: string;
}