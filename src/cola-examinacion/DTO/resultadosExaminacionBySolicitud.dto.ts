import { ResultadoExaminacionTramiteDTO } from "./resultadoExaminacionTramite.dto";

export class ResultadosExaminacionBySolicitudDTO {
    run: string;
    nombrePostultante: string;
    runFormateado:string;

    retDTO : ResultadoExaminacionTramiteDTO[]; // Dentro de Trámite, aprobado cada una de las examinaciones
}