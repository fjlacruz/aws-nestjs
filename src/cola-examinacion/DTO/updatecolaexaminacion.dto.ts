import { ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateColaExaminacionDto {

  @ApiPropertyOptional()
  idColaExaminacion: number;

  @ApiPropertyOptional()
  idEstado: number;

}