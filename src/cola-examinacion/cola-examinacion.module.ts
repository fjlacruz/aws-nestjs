import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { FormularioExaminacionClaseLicenciaEntity } from 'src/formularios-examinaciones/entity/FormularioExaminacionClaseLicencia.entity';
import { FormulariosExaminacionesEntity } from 'src/formularios-examinaciones/entity/formularios-examinaciones.entity';
import { OpcionEstadosCivilEntity } from 'src/opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { OpcionesNivelEducacional } from 'src/opciones-nivel-educacional/entity/nivel-educacional.entity';
import { OpcionSexoEntity } from 'src/opciones-sexo/entity/sexo.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { User } from 'src/users/entity/user.entity';
import { ColaExaminacionController } from './controller/cola-examinacion.controller';
import { ColaExaminacionEntity } from './entity/cola-examinacion.entity';
import { estadosExaminacionEntity } from './entity/estadosExaminacion.entity';
import { ColaExaminacionService } from './services/cola-examinacion.service';

@Module({
  imports: [TypeOrmModule.forFeature([ColaExaminacionEntity,
    TramitesEntity,
    TiposTramiteEntity,
    TramitesClaseLicencia,
    EstadosTramiteEntity,
    ClasesLicenciasEntity,
    Solicitudes,
    InstitucionesEntity,
    Postulante,
    OpcionSexoEntity,
    OpcionEstadosCivilEntity,
    OpcionesNivelEducacional,
    estadosExaminacionEntity,
    FormularioExaminacionClaseLicenciaEntity,
    FormulariosExaminacionesEntity,
    ResultadoExaminacionColaEntity,
    ResultadoExaminacionEntity,
    User,
    RolesUsuarios,
    PostulanteEntity]
  )],
  
  providers: [ColaExaminacionService, AuthService],
  exports: [ColaExaminacionService],
  controllers: [ColaExaminacionController]

})
export class ColaExaminacionModule {}
