import { Test, TestingModule } from '@nestjs/testing';
import { ColaExaminacionController } from './cola-examinacion.controller';

describe('ColaExaminacionController', () => {
  let controller: ColaExaminacionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ColaExaminacionController],
    }).compile();
    

    controller = module.get<ColaExaminacionController>(ColaExaminacionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
