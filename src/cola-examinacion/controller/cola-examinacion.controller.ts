import { Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { ResultadoexaminacionFiltroDto } from 'src/resultado-examinacion/DTO/ResultadoexaminacionFiltroDto';
import { CreateColaExaminacionDto } from '../DTO/filtercolaexaminacion.dto';
import { UpdateColaExaminacionDto } from '../DTO/updatecolaexaminacion.dto';
import { ColaExaminacionService } from '../services/cola-examinacion.service';
import {
  ESTADO_EXAM_EN_CURSO_IDON_MORAL_CODE,
  ESTADO_EXAM_EN_CURSO_PRE_IDON_MORAL_CODE,
  ESTADO_EXAM_PENDIENTE_IDON_MORAL_CODE,
  ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE,
  TIPOEXAMINACIONCOLA_IDONEIDADMORAL,
  TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL,
} from 'src/shared/Constantes/constantesEstadosExaminacion';
import { PermisosNombres } from 'src/constantes';
import { Resultado } from 'src/utils/resultado';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';

@ApiTags('Cola-examinacion')
@Controller('cola-examinacion')
export class ColaExaminacionController {
  [x: string]: any;
  ColaExaminacionService: any;

  constructor(private readonly colaExaminacionService: ColaExaminacionService, private readonly authService: AuthService) {}

  @Get('ColaExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todas las cola de examinacion' })
  async getColaexaminacion() {
    const data = await this.colaExaminacionService.getColaExam();
    return { data };
  }

  @Get('colaexaminacionById/:id/:idTipoExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Colas por Tipo de examinación' })
  async colaexaminacionById(@Param('id') id: number, @Param('idTipoExaminacion') idTipoExaminacion: number) {
    const data = await this.colaExaminacionService.getColaExaminacion(id);
    return { data };
  }

  @Get('colaFiltrado')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getFiltrado(@Query() query: CreateColaExaminacionDto) {
    const idMunicipalidad = this.authService.oficina().idOficina;
    const data = await this.colaExaminacionService.getFiltrado(
      query.RUN,
      query.Nombres,
      query.Nomclaselic,
      query.Nomet,
      query.idTipoExam,
      query.fechaIngresoResultado,
      query.fechaRendicionExamen,
      query.nomExaminadorPractico,
      idMunicipalidad,
      query.resultadoExamen
    );
    return { data };
  }

  @Get('getEstadoExaminaciones/:idTramite')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el estado de las examinaciones por tramite' })
  async getEstadoExaminaciones(@Param('idTramite') idTramite: number) {
    const data = await this.colaExaminacionService.getEstadoExaminaciones(idTramite);
    return { data };
  }

  @Get('getListaEstadosExaminacion')
  @ApiBearerAuth()
  //    @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve los Estados por Tipo Examinacion' })
  async getListaEstadosExaminacion() {
    const data = await this.colaExaminacionService.getListaEstadosExaminacion();
    return { data };
  }

  @Get('getListaEstadosByIdTipoExaminacion/:idTipoExaminacion')
  @ApiBearerAuth()
  //    @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve Estados de un Tipo Examinacion' })
  async getListaEstadosByIdTipoExaminacion(@Param('idTipoExaminacion') idTipoExaminacion: number) {
    const data = await this.colaExaminacionService.getListaEstadosByIdTipoExaminacion(idTipoExaminacion);
    return { data };
  }

  @Get('getEstadoExaminacionbyIdColaExaminacion/:idColaExaminacion')
  @ApiBearerAuth()
  //    @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve estado de una Examinación' })
  async getEstadoExaminacionbyIdColaExaminacion(@Param('idColaExaminacion') idColaExaminacion: number) {
    const data = await this.colaExaminacionService.getEstadoExaminacionbyIdColaExaminacion(idColaExaminacion);
    return { data };
  }

  @Post('setEstadobyIdColaExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza Estado de una Examinación' })
  async setEstadobyIdColaExaminacion(@Body() updateColaExaminacionDto: UpdateColaExaminacionDto) {
    return await this.colaExaminacionService.setEstadobyIdColaExaminacion(
      updateColaExaminacionDto.idColaExaminacion,
      updateColaExaminacionDto.idEstado
    );
  }

  @Post('setEstadoPendientePreIdonMoralColaExaminacionById')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza a Estado Pendiente una Examinación' })
  async setEstadoPendientePreIdonMoralColaExaminacionById(@Body() updateColaExaminacionDto: UpdateColaExaminacionDto) {
    return await this.colaExaminacionService.setEstadoColaExaminacionById(
      updateColaExaminacionDto.idColaExaminacion,
      ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE
    );
  }

  @Post('setEstadoPendientePreIdoneidadMoralById')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza a Estado Pendiente una Examinación' })
  async setEstadoPendientePreIdoneidadMoralByTramites(@Body() idColaExam: any) {
    return await this.colaExaminacionService.setEstadoColaExaminacionById(
      idColaExam.idColaExaminacion,
      ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE
    );
  }

  @Post('setExaminacionEsperaIdoneidadMoral')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza Estado de una Examinación "A la espera de evaluación de idoneidad moral" ' })
  async setExaminacionEsperaIdoneidadMoral(@Body() updateColaExaminacionDto: UpdateColaExaminacionDto) {
    return await this.colaExaminacionService.setExaminacionEsperaIdoneidadMoral(updateColaExaminacionDto.idColaExaminacion);
  }

  @Get('getListaPendientesAtencionEstados')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Cola Examinacion y estados de Examin.' })
  async getListaPendientesAtencionEstadosExam(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: ResultadoexaminacionFiltroDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.colaExaminacionService.getListaPendientesAtencionEstadosExam(
      query.idTipoExaminacion,
      {
        page,
        limit,
        route: '/cola-examinacion/getListaPendientesAtencionEstados',
      },
      orden.toString(),
      ordenarPor.toString()
    );
    for (const item of data.items) {
      const formu = await this.colaExaminacionService.getHistoricoExamenesDetalle(
        item.ColaExaminacion_idColaExaminacion,
        item.tr_idTipoTramite,
        item.te_idTipoExaminacion,
        item.cl_idClaseLicencia
      );
      item['formulario'] = formu;
    }
    return data;
  }

  @Get('getListaPendientesAtencionEstadosV2')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Cola Examinacion y estados de Examin.' })
  async getListaPendientesAtencionEstadosExamV2(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: ResultadoexaminacionFiltroDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.colaExaminacionService.getListaPendientesAtencionEstadosExamV2(
      query.idTipoExaminacion,
      {
        page,
        limit,
        route: '/cola-examinacion/getListaPendientesAtencionEstadosV2',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    for (const item of data.items) {
      for (let index = 0; index < item.tramites.length; index++) {
        const formu = await this.colaExaminacionService.getHistoricoExamenesDetalle(
          item.tramites[index].colaExaminacionNM[0].idColaExaminacion,
          item.tramites[index].idTipoTramite[0].idTipoTramite,
          item.tramites[index].colaExaminacionNM[0].colaExaminacion[0].idEstadosExaminacion[0].idTipoExaminacion, //item.tramites[index].colaExaminacion[0].idTipoExaminacion[0].idTipoExaminacion,
          item.tramites[index].idTramite[0].idClaseLicencia[0].idClaseLicencia
        );
        item.tramites[index]['formulario'] = formu;
      }
    }
    return data;
  }

  @Get('getListaPendientesAtencionEstadosFiltrados') //
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Cola Examinacion y estados de Examinacion filtrados.' })
  async getListaPendientesAtencionEstadosExamFiltrados(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.colaExaminacionService.getListaPendientesAtencionEstadosExamFiltrados(
      query.idTipoExaminacion,
      {
        page,
        limit,
        route: '/cola-examinacion/getListaPendientesAtencionEstadosFiltrados',
      },
      orden.toString(),
      ordenarPor.toString(),
      query.RUN,
      query.Nombres,
      query.Nomclaselic,
      query.fechaIngresoResultado,
      query.idSolicitud
    );
    for (const item of data.items) {
      for (let index = 0; index < item.tramites.length; index++) {
        const formu = await this.colaExaminacionService.getHistoricoExamenesDetalle(
          item.tramites[index].colaExaminacionNM[0].idColaExaminacion,
          item.tramites[index].idTipoTramite[0].idTipoTramite,
          item.tramites[index].colaExaminacionNM[0].colaExaminacion[0].idEstadosExaminacion[0].idTipoExaminacion, //item.tramites[index].colaExaminacion[0].idTipoExaminacion[0].idTipoExaminacion,
          item.tramites[index].idTramite[0].idClaseLicencia[0].idClaseLicencia
        );
        item.tramites[index]['formulario'] = formu;
      }
    }
    return data;
  }

  @Get('getListaPendientesAtencionEstadosIdoniedadMoral')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Cola Examinacion y estados de Examin.' })
  async getListaPendientesAtencionEstadosIdoniedadMoral(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: ResultadoexaminacionFiltroDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.colaExaminacionService.getListaPendientesAtencionEstadosIdoniedadMoral(
      query.idTipoExaminacion,
      {
        page,
        limit,
        route: '/cola-examinacion/getListaPendientesAtencionEstadosIdoniedadMoral',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    for (const item of data.items) {
      const formu = await this.colaExaminacionService.getHistoricoExamenesDetalle(
        item.ColaExaminacion_idColaExaminacion,
        item.tr_idTipoTramite,
        item.te_idTipoExaminacion,
        item.cl_idClaseLicencia
      );
      item['formulario'] = formu;
    }
    return data;
  }

  @Get('getListaPendientesAtencionEstadosIdoniedadMoralV2')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Cola Examinacion y estados de Examin.' })
  async getListaPendientesAtencionEstadosIdoniedadMoralV2(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any, //ResultadoexaminacionFiltroDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.colaExaminacionService.getListaPendientesAtencionEstadosIdoniedadMoralV2(
      query.idTipoExaminacion,
      {
        page,
        limit,
        route: '/cola-examinacion/getListaPendientesAtencionEstadosIdoniedadMoral',
      },
      query.run,
      query.nombreApellido,
      query.claseLicencia,
      query.idSolicitud,
      query.fechaInicio,
      query.estado,
      orden.toString(),
      ordenarPor.toString()
    );

    for (const item of data.items) {
      const formu = await this.colaExaminacionService.getHistoricoExamenesDetalle(
        item.ColaExaminacion_idColaExaminacion,
        item.tr_idTipoTramite,
        item.te_idTipoExaminacion,
        item.cl_idClaseLicencia
      );
      item['formulario'] = formu;
    }
    return data;
  }

  @Get('getHistoricoExamenesSinAlertaPEIM')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getListaHistoricoExamenes(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: CreateColaExaminacionDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const idMunicipalidad = this.authService.oficina().idOficina;
    const data = await this.colaExaminacionService.getHistoricoExamenes(
      query.idTipoExaminacion,
      query.RUN,
      query.Nombres,
      query.Nomclaselic,
      query.Nomet,
      query.fechaIngresoResultado,
      query.fechaRendicionExamen,
      query.nomExaminadorPractico,
      null, // aqui agregar idMunicipalidad
      query.resultadoExamen,
      query.idEstadosExaminacion,
      {
        page,
        limit,
        route: '/cola-examinacion/getHistoricoExamenesSinAlertaPEIM',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    for (const item of data.items) {
      const formu = await this.colaExaminacionService.getHistoricoExamenesDetalle(
        item.ColaExaminacion_idColaExaminacion,
        item.tr_idTipoTramite,
        item.te_idTipoExaminacion,
        item.cl_idClaseLicencia
      );
      item['formulario'] = formu;
    }
    return data;
  }

  @Get('getHistoricoExamenesSinAlertaPEIMv2')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getListaHistoricoExamenesv2(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any, //CreateColaExaminacionDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {
    let validacionPermisos: Resultado = await this.authService.checkPermission(req, PermisosNombres.VisualizarListaSolicitudesEnProceso);
    const permisos: RolesUsuarios[] = validacionPermisos.Respuesta as RolesUsuarios[];

    if (validacionPermisos.ResultadoOperacion) {
      const data = await this.colaExaminacionService.getHistoricoExamenesv2(
        permisos,
        query.idTipoExaminacion,
        query.RUN,
        query.Nombres,
        query.Nomclaselic,
        query.nombreTramite,
        query.estadoTramite,
        query.estadoAlerta,
        query.estadoExaminacion,
        query.fechaIngresoResultado,
        query.idSolicitud,
        {
          page,
          limit,
          route: '/cola-examinacion/getHistoricoExamenesSinAlertaPEIMv2',
        },
        orden.toString(),
        ordenarPor.toString()
      );

      return data;
    } else {
      return { data: validacionPermisos };
    }
  }

  @Get('getEstadoExaminacionesByIdSolicitud/:idSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el estado de las examinaciones por tramite' })
  async getEstadoExaminacionesByIdSolicitud(@Param('idSolicitud') idSolicitud: number) {
    const data = await this.colaExaminacionService.getEstadoExaminacionesByIdSolicitud(idSolicitud);
    return { data };
  }

  @Post('setEstadoPendPreIMoralColaExBySolicitudId')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza Estado de una Examinación' })
  async setEstadoPendPreIMoralColaExBySolicitudId(@Body() updateColaExaminacionDto: UpdateColaExaminacionDto) {
    return await this.colaExaminacionService.setEstadoColaPendienteBySolicitudIdAndTipoExam(
      updateColaExaminacionDto.idColaExaminacion,
      ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE,
      TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL
    );
  }

  @Post('setEstadoEnCursoPreIMoralColaExBySolicitudId')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza Estado de una Examinación' })
  async setEstadoEnCursoPreIMoralColaExBySolicitudId(@Body() updateColaExaminacionDto: UpdateColaExaminacionDto) {
    return await this.colaExaminacionService.setEstadoColaPendienteBySolicitudIdAndTipoExam(
      updateColaExaminacionDto.idColaExaminacion,
      ESTADO_EXAM_EN_CURSO_PRE_IDON_MORAL_CODE,
      TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL
    );
  }

  @Post('setEstadoPendienteIdonMoralColaExaminacionById')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza Estado de una Examinación' })
  async setEstadoPendienteIdonMoralColaExaminacionById(@Body() updateColaExaminacionDto: UpdateColaExaminacionDto) {
    return await this.colaExaminacionService.setEstadoColaPendienteBySolicitudIdAndTipoExam(
      updateColaExaminacionDto.idColaExaminacion,
      ESTADO_EXAM_PENDIENTE_IDON_MORAL_CODE,
      TIPOEXAMINACIONCOLA_IDONEIDADMORAL
    );
  }

  @Post('setEstadoEnCursoIdonMoralColaExBySolicitudId')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza Estado de una Examinación' })
  async setEstadoEnCursoIdonMoralColaExBySolicitudId(@Body() updateColaExaminacionDto: UpdateColaExaminacionDto) {
    return await this.colaExaminacionService.setEstadoColaPendienteBySolicitudIdAndTipoExam(
      updateColaExaminacionDto.idColaExaminacion,
      ESTADO_EXAM_EN_CURSO_IDON_MORAL_CODE,
      TIPOEXAMINACIONCOLA_IDONEIDADMORAL
    );
  }
  @Get('getTipoTramites')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados por Tipo Examinacion' })
  async getTipoTramites() {
    const data = await this.colaExaminacionService.getTipoTramites();
    return { data };
  }

  @Get('getEstadosTramites')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados por Tipo Examinacion' })
  async getEstadosTramites() {
    const data = await this.colaExaminacionService.getEstadosTramites();
    return { data };
  }

  // Método para recoger las examinaciones para los apartados preidoneidad moral e idoneidad moral
  @Get('getListaPendientesEstadosV3')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getListaPendientesEstadosV3(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any, //CreateColaExaminacionDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Query('recuperarHistorial') recuperarHistorial: boolean,
    @Req() req: any
  ) {
    const data = await this.colaExaminacionService.getHistoricoExamenesv2(
      req,
      query.idTipoExaminacion,
      query.RUN,
      query.Nombres,
      query.Nomclaselic,
      query.nombreTramite,
      query.estadoTramite,
      query.estadoAlerta,
      query.estadoExaminacion,
      query.fechaIngresoResultado,
      query.idSolicitud,
      {
        page,
        limit,
        route: '/cola-examinacion/getListaPendientesEstadosV3',
      },
      orden.toString(),
      ordenarPor.toString(),
      recuperarHistorial
    );

    return data;
  }

  @Get('getColasCompartidas/:idTramite/:idSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el estado de las examinaciones por tramite' })
  async getColasCompartidas(@Param('idTramite') idTramite: number, @Param('idSolicitud') idSolicitud: number) {
    const data = await this.colaExaminacionService.getColasCompartidas(idTramite, idSolicitud);
    return { data };
  }
}
