import { ApiProperty } from '@nestjs/swagger';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { ColaExaminacionTramiteNMEntity } from 'src/tramites/entity/cola-examinacion-tramite-n-m.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm';
import { estadosExaminacionEntity } from './estadosExaminacion.entity';
import { TramitesColaEntity } from './tramitesCola.entity';
import { OportunidadEntity } from '../../oportunidad/oportunidad.entity';

@Entity('ColaExaminacion')
export class ColaExaminacionEntity {
  @PrimaryGeneratedColumn()
  idColaExaminacion: number;

  @Column()
  idTipoExaminacion: number;

  @Column()
  historico: boolean;

  @Column()
  aprobado: boolean;

  @Column()
  created_at: Date;

  @Column()
  updated_at: Date;

  @Column()
  idEstadosExaminacion: number;

  @Column()
  idTramite: number;

  @OneToOne(() => TramitesColaEntity, tramites => tramites.colaExaminacion)
  @JoinColumn({ name: 'idTramite' })
  readonly tramites: TramitesColaEntity;

  @ApiProperty()
  @OneToOne(() => TramitesClaseLicencia, tramitesClaseLicencia => tramitesClaseLicencia.tramite)
  readonly tramitesClaseLicencia: TramitesClaseLicencia;

  @ApiProperty()
  @OneToOne(() => ClasesLicenciasEntity, claseslicencias => claseslicencias.tramitesClaseLicencia)
  readonly claseslicencias: ClasesLicenciasEntity;

  @ApiProperty()
  @OneToMany(() => ColaExaminacionTramiteNMEntity, colaExaminacionNM => colaExaminacionNM.colaExaminacion)
  readonly colaExaminacionNM: ColaExaminacionTramiteNMEntity[];

  @ApiProperty()
  @OneToMany(() => ResultadoExaminacionColaEntity, resultadoexaminacioncola => resultadoexaminacioncola.colaExaminacion)
  readonly resultadoExaminacionCola: ResultadoExaminacionColaEntity[];

  @ManyToOne(() => TipoExaminacionesEntity, tipoExaminacion => tipoExaminacion.colaExaminacion)
  @JoinColumn({ name: 'idTipoExaminacion' })
  readonly tipoExaminacion: TipoExaminacionesEntity;

  @ManyToOne(() => estadosExaminacionEntity, estadoExaminacion => estadoExaminacion.colaExaminacion)
  @JoinColumn({ name: 'idEstadosExaminacion' })
  readonly estadoExaminacion: estadosExaminacionEntity;

  @OneToMany(type => OportunidadEntity, oportunidad => oportunidad.colaExaminacion)
  readonly oportunidades: OportunidadEntity[];

  oportunidadesCount: number;
}
