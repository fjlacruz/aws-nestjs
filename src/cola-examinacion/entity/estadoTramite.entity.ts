import { ApiProperty } from "@nestjs/swagger";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, JoinColumn, ManyToOne} from "typeorm";

@Entity('EstadosTramite')
export class EstadosTramiteEntity {

    @PrimaryColumn()
    idEstadoTramite:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;

    @Column()
    IdPredecesor:number;

}
