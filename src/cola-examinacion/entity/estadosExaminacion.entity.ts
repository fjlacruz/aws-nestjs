import { ApiProperty } from "@nestjs/swagger";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, JoinColumn, ManyToOne, OneToMany} from "typeorm";
import { ColaExaminacionEntity } from "./cola-examinacion.entity";

@Entity('estadosExaminacion')
export class estadosExaminacionEntity {

    @PrimaryColumn()
    idEstadosExaminacion:number;

    @Column()
    nombreEstado:string;

    @Column()
    descripcion:string;

    @Column()
    idTipoExaminacion:number;

    @Column()
    codigo:string;

    @OneToMany(() => ColaExaminacionEntity, (colaExaminacion) => colaExaminacion.estadoExaminacion)
    readonly colaExaminacion: ColaExaminacionEntity[];      

}
