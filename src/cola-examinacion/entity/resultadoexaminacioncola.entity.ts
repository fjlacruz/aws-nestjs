import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { ColaExaminacionEntity } from './cola-examinacion.entity';

@Entity('ResultadoExaminacionCola')
export class ResultadoExaminacionColaEntity {
  @PrimaryColumn()
  idResultadoExamCola: number;

  @Column()
  idResultadoExam: number;

  @Column()
  idColaExaminacion: number;

  @ManyToOne(() => ColaExaminacionEntity, colaExaminacion => colaExaminacion.idColaExaminacion)
  @JoinColumn({ name: 'idColaExaminacion' })
  readonly colaExaminacion: ColaExaminacionEntity;

  @ManyToOne(() => ResultadoExaminacionEntity, resultadoExaminacion => resultadoExaminacion.resultadoExaminacionCola)
  @JoinColumn({ name: 'idResultadoExam' })
  readonly resultadoExaminacion: ResultadoExaminacionEntity;

}
