import { ApiProperty } from "@nestjs/swagger";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToMany} from "typeorm";
import { ColaExaminacionEntity } from "./cola-examinacion.entity";

@Entity('TipoExaminaciones')
export class TipoExaminacionesEntity {

    @PrimaryColumn()
    idTipoExaminacion:number;

    @Column()
    nombreExaminacion:string;

    @Column()
    descripcionExaminacion:string;

    @ApiProperty()
    @OneToMany(() => ColaExaminacionEntity, (colaExaminacion) => colaExaminacion.estadoExaminacion)
    readonly colaExaminacion: ColaExaminacionEntity[];   
}
