import { ApiProperty } from '@nestjs/swagger';
// import { ColaExamMedicoEntity } from 'src/cola-exam-medico/entity/cola-exam-medico.entity';
// import { ColaExamPracticosEntity } from 'src/cola-exam-practicos/entity/cola-exam-practicos.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  ManyToOne,
  JoinColumn,
  OneToOne,
  OneToMany,
} from 'typeorm';
import { ColaExaminacionEntity } from './cola-examinacion.entity';

@Entity('Tramites')
export class TramitesColaEntity {
  @PrimaryGeneratedColumn()
  idTramite: number;

  @Column()
  idSolicitud: number;

  @ManyToOne(() => Solicitudes, (solicitudes) => solicitudes.tramites)
  @JoinColumn({ name: 'idSolicitud' })
  readonly solicitudes: Solicitudes;

  @Column()
  idTipoTramite: number;

  @ManyToOne(() => TiposTramiteEntity, (tiposTramite) => tiposTramite.tramites)
  @JoinColumn({ name: 'idTipoTramite' })
  readonly tiposTramite: TiposTramiteEntity;

  @Column()
  idEstadoTramite: number;

  @ManyToOne(
    () => EstadosTramiteEntity,
    (estadoTramite) => estadoTramite.tramites,
  )
  @JoinColumn({ name: 'idEstadoTramite' })
  readonly estadoTramite: EstadosTramiteEntity;

  @ApiProperty()
  @OneToOne(
    () => ColaExaminacionEntity,
    (colaExaminacion) => colaExaminacion.tramites,
  )
  readonly colaExaminacion: ColaExaminacionEntity;

  // @ApiProperty()
  // @OneToOne(
  //   () => ColaExamPracticosEntity,
  //   (colaexampracticos) => colaexampracticos.tramite,
  // )
  // readonly colaexampracticos: ColaExamPracticosEntity;
  //
  // @ApiProperty()
  // @OneToOne(
  //   () => ColaExamMedicoEntity,
  //   (colaexammedico) => colaexammedico.tramite,
  // )
  // readonly colaexammedico: ColaExamMedicoEntity;
}
