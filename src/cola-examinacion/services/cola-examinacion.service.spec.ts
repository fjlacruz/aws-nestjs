import { Test, TestingModule } from '@nestjs/testing';
import { ColaExaminacionService } from './cola-examinacion.service';

describe('ColaExaminacionService', () => {
  let service: ColaExaminacionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ColaExaminacionService],
    }).compile();

    service = module.get<ColaExaminacionService>(ColaExaminacionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
