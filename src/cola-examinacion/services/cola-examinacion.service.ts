import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { OpcionEstadosCivilEntity } from 'src/opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { OpcionesNivelEducacional } from 'src/opciones-nivel-educacional/entity/nivel-educacional.entity';
import { OpcionSexoEntity } from 'src/opciones-sexo/entity/sexo.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { getConnection, Repository } from 'typeorm';
import { ColaExaminacionEntity } from '../entity/cola-examinacion.entity';
import { ResultadoExaminacionColaEntity } from '../entity/resultadoexaminacioncola.entity';
import { FormulariosExaminacionesEntity } from '../../formularios-examinaciones/entity/formularios-examinaciones.entity';
import { TipoExaminacionesEntity } from '../entity/tipoexaminacion.entity';
import { Postulante } from '../../registro-pago/entity/postulante.entity';
import { estadosExaminacionEntity } from '../entity/estadosExaminacion.entity';
import { FormularioExaminacionClaseLicenciaEntity } from 'src/formularios-examinaciones/entity/FormularioExaminacionClaseLicencia.entity';
import { AuthService } from 'src/auth/auth.service';
import { IPaginationOptions, paginate, paginateRaw } from 'nestjs-typeorm-paginate';
import { codigoEsperaIdoneidadMoral, PermisosNombres } from 'src/constantes';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { Resultado } from 'src/utils/resultado';
import {
  ESTADO_EXAM_PENDIENTE_IDON_MORAL_CODE,
  ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE,
  TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL,
} from 'src/shared/Constantes/constantesEstadosExaminacion';
import { ColaExaminacionTramiteNMEntity } from 'src/tramites/entity/cola-examinacion-tramite-n-m.entity';
import { ResultadosExaminacionBySolicitudDTO } from '../DTO/resultadosExaminacionBySolicitud.dto';
import { ResultadoExaminacionTramiteDTO } from '../DTO/resultadoExaminacionTramite.dto';
import { ColaExaminacion } from 'src/tramites/entity/cola-examinacion.entity';
import { PostulanteHistorialIdoneidadMoral } from '../DTO/PostulanteHistorialIdoneidadMoral.dto';
import { formatearRun } from 'src/shared/ValidarRun';
import { validarStringEsEntero } from 'src/shared/Common/ValidadoresFormato';
import { EstadoTramiteDto } from 'src/shared/DTO/estadoTramites.dto';
import { TramitesColaEntity } from '../entity/tramitesCola.entity';

@Injectable()
export class ColaExaminacionService {
  [x: string]: any;

  constructor(
    private authService: AuthService,
    @InjectRepository(ColaExaminacionEntity) private readonly colaexamRespository: Repository<ColaExaminacionEntity>,
    @InjectRepository(PostulanteEntity) private readonly postulanteEntityRespository: Repository<PostulanteEntity>,
    @InjectRepository(TramitesEntity) private readonly tramitesEntityRespository: Repository<TramitesEntity>,
    @InjectRepository(TiposTramiteEntity) private readonly tipoTramitesEntityRespository: Repository<TiposTramiteEntity>,
    @InjectRepository(EstadosTramiteEntity) private readonly estadosTramitesEntityRespository: Repository<EstadosTramiteEntity>,
    @InjectRepository(estadosExaminacionEntity) private readonly estadosExamRepository: Repository<estadosExaminacionEntity>,
    @InjectRepository(FormulariosExaminacionesEntity)
    private readonly formulariosexamRespository: Repository<FormulariosExaminacionesEntity>,
    @InjectRepository(ResultadoExaminacionEntity) private readonly resulexaminRespository: Repository<ResultadoExaminacionEntity>,
    @InjectRepository(ResultadoExaminacionColaEntity)
    private readonly resulExaminacionColaRespository: Repository<ResultadoExaminacionColaEntity>,
    @InjectRepository(Solicitudes) private readonly solicitudesEntityRespository: Repository<Solicitudes>
  ) {}

  async getColaExam() {
    const idOficina = this.authService.oficina().idOficina;

    return await this.colaexamRespository
      .createQueryBuilder('cola')
      .innerJoin(TramitesEntity, 'tra', 'cola.idTramite = tra.idTramite')
      .innerJoin(Solicitudes, 'sol', 'sol.idSolicitud = tra.idSolicitud')
      .where('sol.idOficina = :idOficina', {
        idOficina,
      })
      .getMany();
  }

  async getColaExaminacion(id?: number, idTipoExaminacion?: number) {
    const colaexam = await this.colaexamRespository.findOne(id);
    if (!colaexam) throw new NotFoundException('Cola Examinacion dont exist');

    return colaexam;
  }

  async getFiltrado(
    run?: string,
    nombres?: string,
    Nomclaselic?: string,
    nomet?: string,
    idTipoExam?: number,
    fechaIngresoResultado?: Date,
    fechaRendicionExamen?: Date,
    nomExaminadorPractico?: number,
    idMunicipalidad?: number,
    idResultadoExam?: string
  ) {
    let vquery: string = '';
    let vRun: string = '';
    let vNombres: string = '';
    let vNomclaselic: string = '';
    let vNomet: string = '';
    let vidTipoExam: string = '';
    let vfechaIngresoResultado: string = '';
    let vfechaRendicionExamen: string = '';
    let vnomExaminadorPractico: string = '';
    let vidMunicipalidad: string = '';

    if (run) vRun = '("pos"."RUN" = ' + run + ')';
    if (nombres)
      vNombres = '(concat("pos"."Nombres", \' \', "pos"."ApellidoPaterno", \' \', "pos"."ApellidoMaterno") ILIKE \'%' + nombres + "%')";
    if (Nomclaselic) vNomclaselic = '("clic"."Nombre" ILIKE \'%' + Nomclaselic + "%')";
    if (nomet) vNomet = '("et"."Nombre" ILIKE \'%' + nomet + "%')";
    if (idTipoExam) vidTipoExam = '("cex"."idTipoExaminacion" = ' + idTipoExam + ')';
    if (fechaIngresoResultado) vfechaIngresoResultado = '("cex"."created_at" = \'' + fechaIngresoResultado + "')";
    if (nomExaminadorPractico)
      vnomExaminadorPractico =
        '(concat("usr"."Nombres", \' \', "usr"."ApellidoPaterno", \' \', "usr"."ApellidoMaterno") ILIKE \'%' +
        nomExaminadorPractico +
        "%')";
    if (idMunicipalidad) vidMunicipalidad = '("insti"."idInstitucion" = ' + idMunicipalidad + ')';

    if (vRun != '') vquery = vRun;
    if (vNombres != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vNombres;
      } else {
        vquery = vNombres;
      }
    }
    if (vNomclaselic != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vNomclaselic;
      } else {
        vquery = vNomclaselic;
      }
    }
    if (vNomet != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vNomet;
      } else {
        vquery = vNomet;
      }
    }
    if (vidTipoExam != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidTipoExam;
      } else {
        vquery = vidTipoExam;
      }
    }
    if (vfechaIngresoResultado != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vfechaIngresoResultado;
      } else {
        vquery = vfechaIngresoResultado;
      }
    }
    if (vfechaRendicionExamen != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vfechaRendicionExamen;
      } else {
        vquery = vfechaRendicionExamen;
      }
    }
    if (vnomExaminadorPractico != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vnomExaminadorPractico;
      } else {
        vquery = vnomExaminadorPractico;
      }
    }
    if (vidMunicipalidad != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidMunicipalidad;
      } else {
        vquery = vidMunicipalidad;
      }
    }
    if (vquery != '') {
      vquery = vquery + ' AND ' + '("cex"."historico" = true)';
    } else {
      vquery = '("cex"."historico" = true)';
    }

    return await getConnection()
      .createQueryBuilder(ColaExaminacionEntity, 'cex')
      .select('cex.idColaExaminacion', 'idColaExaminacion')
      .addSelect('cex.aprobado', 'ColaexamAprobado')
      .addSelect('cex.historico', 'ColaexamHistorico')
      .addSelect('sol.idSolicitud', 'idSolicitud')
      .addSelect('tra.idTramite', 'idTramite')
      .addSelect('tra.idTipoTramite', 'idTipoTramite')
      .addSelect('titr.Nombre', 'NomTipoTramite')
      .addSelect('pos.RUN', 'RUN')
      .addSelect('pos.DV', 'DV')
      .addSelect('pos.idPostulante', 'idPostulante')
      .addSelect('pos.Nombres', 'PosNombres')
      .addSelect('pos.ApellidoPaterno', 'PosApellidoPaterno')
      .addSelect('pos.ApellidoMaterno', 'PosApellidoMaterno')
      .addSelect('pos.idOpcionSexo', 'PosidSexo')
      .addSelect('pos.idOpcionEstadosCivil', 'PosidEstadoCivil')
      .addSelect('pos.idOpNivelEducacional', 'PosidNivelEducacional')
      .addSelect('pos.Profesion', 'PosProfesion')
      .addSelect('pos.Email', 'PosEmail')
      .addSelect('pos.Telefono', 'PosTelefono')
      .addSelect('pos.Nacionalidad', 'PosNacionalidad')
      .addSelect('pos.FechaNacimiento', 'PosFechaNacimiento')
      .addSelect('osex.Nombre', 'NomSexo')
      .addSelect('oeciv.Nombre', 'NomEstadoCivil')
      .addSelect('onioc.Nombre', 'NomNivelEducacional')
      .addSelect('clic.idClaseLicencia', 'idClaseLicencia')
      .addSelect('clic.Nombre', 'LicNombre')
      .addSelect('clic.Abreviacion', 'LicAbreviacion')
      .addSelect('tra.idEstadoTramite', 'idEstadoTramite')
      .addSelect('et.Nombre', 'Nombre')
      .addSelect('sol.idInstitucion', 'idInstitucion')
      .addSelect('insti.Nombre', 'InstNombre')
      .addSelect('cex.idTipoExaminacion', 'TipoExaminacion')
      .addSelect('cex.created_at', 'FechaIngResultado')

      .innerJoin(TramitesEntity, 'tra', 'tra.idTramite = cex.idTramite')
      .innerJoin(EstadosTramiteEntity, 'et', 'tra.idEstadoTramite = et.idEstadoTramite')
      .innerJoin(Solicitudes, 'sol', 'tra.idSolicitud = sol.idSolicitud')
      .innerJoin(Postulantes, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoin(InstitucionesEntity, 'insti', 'sol.idInstitucion = insti.idInstitucion')
      .innerJoin(TramitesClaseLicencia, 'tcla', 'tra.idTramite = tcla.idTramite')
      .innerJoin(ClasesLicenciasEntity, 'clic', 'tcla.idClaseLicencia = clic.idClaseLicencia')
      .innerJoin(TiposTramiteEntity, 'titr', 'tra.idTipoTramite = titr.idTipoTramite')
      .leftJoin(OpcionSexoEntity, 'osex', 'pos.idOpcionSexo = osex.idOpcionSexo')
      .leftJoin(OpcionEstadosCivilEntity, 'oeciv', 'pos.idOpcionEstadosCivil = oeciv.idOpcionEstadosCivil')
      .leftJoin(OpcionesNivelEducacional, 'onioc', 'pos.idOpNivelEducacional = onioc.idOpNivelEducacional')

      .andWhere(vquery)

      .getRawMany();
  }

  /**
   * Metodo para obtener las colas compartidas por id tramite e id solicitud.
   * @param idTramite
   * @param idSolicitud
   */
  async getColasCompartidas(idTramite: number, idSolicitud: number) {
    const qb = this.colaexamRespository
      .createQueryBuilder('ce')
      .leftJoinAndMapMany('ce.idTipoExaminacion', TipoExaminacionesEntity, 'te', 'ce.idTipoExaminacion = te.idTipoExaminacion')
      .leftJoinAndMapMany('ce.idTramite', TramitesColaEntity, 'tram', 'ce.idTramite = tram.idTramite')
      .leftJoinAndMapOne('ce.idColaExaminacion', ResultadoExaminacionColaEntity, 'rec', 'ce.idColaExaminacion = rec.idColaExaminacion')
      .leftJoinAndMapOne('re.idResultadoExam', ResultadoExaminacionEntity, 're', 'rec.idResultadoExam = re.idResultadoExam')
      .leftJoinAndMapMany('tram.idSolicitud', Solicitudes, 'sol', 'tram.idSolicitud = sol.idSolicitud');
    qb.andWhere('te.idTipoExaminacion = 4');
    qb.andWhere('sol.idSolicitud = :idSolicitud', { idSolicitud: idSolicitud });
    qb.andWhere('tram.idTramite not in (:idTramite)', { idTramite: idTramite });
    qb.andWhere('re.idFormularioExaminaciones = 2');

    const estados: any = await qb.getRawMany();
    return estados;
  }

  async getEstadoExaminaciones(idTramite: number) {
    const qb = this.colaexamRespository
      .createQueryBuilder('ColaExaminacion')
      .innerJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .innerJoinAndMapMany(
        'ColaExaminacion.idColaExaminacion',
        ResultadoExaminacionColaEntity,
        'rec',
        'ColaExaminacion.idColaExaminacion = rec.idColaExaminacion'
      )
      .innerJoinAndMapMany('re.idResultadoExam', ResultadoExaminacionEntity, 're', 'rec.idResultadoExam = re.idResultadoExam')
      .innerJoinAndMapMany(
        're.idFormularioExaminaciones',
        FormulariosExaminacionesEntity,
        'form',
        're.idFormularioExaminaciones = form.idFromularioExaminaciones'
      );
    qb.andWhere('ColaExaminacion.idTramite = :idTramite', { idTramite: idTramite });

    const estados: any = await qb.getRawMany();
    const estadosFinal: any[] = [];
    const teoricas = estados.filter(item => item.te_idTipoExaminacion === 2);
    const practicas = estados.filter(item => item.te_idTipoExaminacion === 3);
    const medicas = estados.filter(item => item.te_idTipoExaminacion === 4);
    const im = estados.filter(item => item.te_idTipoExaminacion === 5);
    const impre = estados.filter(item => item.te_idTipoExaminacion === 6);

    if (teoricas.length > 0) {
      estadosFinal.push(this.returnStatus('Teórica', teoricas));
    }
    if (practicas.length > 0) {
      estadosFinal.push(this.returnStatus('Práctica', practicas));
    }
    if (medicas.length > 0) {
      estadosFinal.push(this.returnStatus('Médica', medicas));
    }
    if (im.length > 0) {
      estadosFinal.push(this.returnStatus('Idoneidad Moral', im));
    }
    if (impre.length > 0) {
      estadosFinal.push(this.returnStatus('Idoneidad Moral - PreEval', impre));
    }

    return estadosFinal;
  }

  private returnStatus(tipo: string, estadoList: any[]) {
    const aprobados = estadoList.filter(item => item.re_aprobado === true);
    const reprobados = estadoList.filter(item => item.re_aprobado === false);
    const nulos = estadoList.filter(item => item.re_aprobado === null);
    const estadoExamen = {
      tipoExaminacion: tipo,
      status: nulos.length > 0 ? 'Pendiente' : aprobados.length === estadoList.length ? 'Aprobado' : 'Reprobado',
    };
    return estadoExamen;
  }

  async getListaEstadosExaminacion() {
    const qb = this.estadosExamRepository
      .createQueryBuilder('estadosExaminacion')
      .innerJoinAndMapMany(
        'estadosExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'estadosExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      );

    return qb.getMany();
  }

  async getListaEstadosByIdTipoExaminacion(idTipoExaminacion: number) {
    const qb = this.estadosExamRepository
      .createQueryBuilder('estadosExaminacion')
      .innerJoinAndMapMany(
        'estadosExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'estadosExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      );
    qb.andWhere('estadosExaminacion.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: idTipoExaminacion });

    return qb.getMany();
  }

  async getEstadoExaminacionbyIdColaExaminacion(idColaExaminacion: number) {
    let consulta: string = '';
    consulta = '("cex"."idColaExaminacion" = ' + idColaExaminacion + ')';
    return await getConnection()
      .createQueryBuilder(ColaExaminacionEntity, 'cex')
      .addSelect('cex.idColaExaminacion', 'idColaExaminacion')
      .addSelect('cex.idTipoExaminacion', 'TipoExaminacion')
      .addSelect('cex.aprobado', 'ColaexamAprobado')
      .addSelect('cex.historico', 'ColaexamHistorico')
      .addSelect('eex.nombreEstado', 'NombreEstadoExaminacion')
      .addSelect('eex.codigo', 'CodigoEstadoExaminacion')
      .addSelect('tipex.nombreExaminacion', 'TiExnombreExaminacion')

      .innerJoin(
        estadosExaminacionEntity,
        'eex',
        'cex.idEstadosExaminacion = eex.idEstadosExaminacion and cex.idTipoExaminacion = eex.idTipoExaminacion'
      )
      .innerJoin(TipoExaminacionesEntity, 'tipex', 'tipex.idTipoExaminacion = cex.idTipoExaminacion')
      .where(consulta)
      .getRawMany();
  }

  async setEstadobyIdColaExaminacion(idColaExaminacion: number, idEstado: number) {
    let res: Resultado = new Resultado();

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const aprobadoPor = (await this.authService.usuario()).idUsuario;

      const qbColaEx = this.colaexamRespository
        .createQueryBuilder('colaExaminacion')
        .innerJoinAndMapOne(
          'colaExaminacion.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne('colaExaminacionNM.tramites', TramitesEntity, 'tramites', 'colaExaminacionNM.idTramite = tramites.idTramite')

        .andWhere('colaExaminacion.idColaExaminacion = :_idColaExaminacion', { _idColaExaminacion: idColaExaminacion });

      const colaExaminacion: ColaExaminacionEntity = await qbColaEx.getOne();

      // const colaExaminacion: ColaExaminacionEntity = await this.colaexamRespository.findOne({
      //   idColaExaminacion: idColaExaminacion
      // });

      if (colaExaminacion) {
        const resultadoExaminacionCola: ResultadoExaminacionColaEntity = await this.resulExaminacionColaRespository.findOne({
          idColaExaminacion: idColaExaminacion,
        });

        const resultadoExaminacion: ResultadoExaminacionEntity = await this.resulexaminRespository.findOne({
          idResultadoExam: resultadoExaminacionCola.idResultadoExam,
        });

        let resultadoAprobado: boolean = false;

        if (resultadoExaminacion) {
          if (idEstado == 35) {
            // En caso de ser aprobado el resultado
            resultadoAprobado = true;
          } else if (idEstado == 36) {
            // En caso de ser reprobado asignamos resultado
            resultadoAprobado = false;
          }

          // Actualizamos resultado de examinación, cola examinación y el trámite en el caso de denegación
          queryRunner.manager.update(ResultadoExaminacionEntity, resultadoExaminacion.idResultadoExam, {
            aprobado: resultadoAprobado,
          });

          queryRunner.manager.update(ColaExaminacionEntity, colaExaminacion.idColaExaminacion, {
            idEstadosExaminacion: idEstado,
            aprobado: resultadoAprobado,
            historico: true,
          });

          if (resultadoAprobado == false) {
            queryRunner.manager.update(TramitesEntity, colaExaminacion.colaExaminacionNM[0].idTramite, {
              idEstadoTramite: 2, // Trámite en espera de denegación
            });
          }

          queryRunner.commitTransaction();

          res.Mensaje = 'Se ha guardado la información correctamente';
          res.ResultadoOperacion = true;

          return res;
        }
      }
    } catch (Error) {
      res.Error = 'Ha ocurrido un error guardando la información.';
      res.ResultadoOperacion = false;
      queryRunner.rollbackTransaction();
    } finally {
      queryRunner.release();
    }
  }

  async setEstadoColaPendienteBySolicitudIdAndTipoExam(idSolicitud: number, nuevoEstadoColaExam: string, tipoExaminacion: number) {
    let resultado = new Resultado();

    try {
      // 1 Recuperar el estado de examinación
      const estadoPendienteExaminacion: estadosExaminacionEntity = await this.estadosExamRepository.findOne({
        codigo: nuevoEstadoColaExam,
      });

      // 2. Nos traemos la información de los trámites asociados junto a sus examinaciones
      const qbTramites = this.tramitesEntityRespository
        .createQueryBuilder('tr')
        .innerJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
        .innerJoinAndMapMany(
          'tr.idEstadoTramite',
          EstadosTramiteEntity,
          'EstadoTramite',
          'tr.idEstadoTramite = EstadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapMany(
          'tr.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'ColaExaminacionNM',
          'ColaExaminacionNM.idTramite = tr.idTramite'
        )
        .innerJoinAndMapMany(
          'ColaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'ColaExaminacion',
          'ColaExaminacion.idColaExaminacion = ColaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne(
          'tr.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tr.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'tramitesClaseLicencia.idClaseLicencia = clasesLicencias.idClaseLicencia'
        );
      qbTramites.andWhere('tr.idSolicitud = ' + idSolicitud);

      const resultadoTramites: TramitesEntity[] = await qbTramites.getMany();

      // Variable para guardar los ids de las examinaciones
      let idsColaExaminacion: number[] = [];

      resultadoTramites.forEach((tramite, i) => {
        tramite.colaExaminacionNM.forEach((colaNM, j) => {
          idsColaExaminacion.push(colaNM.idColaExaminacion);
        });
      });

      // 3. Nos traemos los resultados de examinación asociados
      const qbResultadosExaminacion = this.colaexamRespository
        .createQueryBuilder('ColaExaminacion')
        .innerJoinAndMapMany(
          'ColaExaminacion.TipoExaminacion',
          TipoExaminacionesEntity,
          'te',
          'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.ResultadoExaminacionCola',
          ResultadoExaminacionColaEntity,
          'ResultadoExaminacionCola',
          'ColaExaminacion.idColaExaminacion = ResultadoExaminacionCola.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'ResultadoExaminacion.idResultadoExam',
          ResultadoExaminacionEntity,
          'ResultadoExaminacion',
          'ResultadoExaminacionCola.idResultadoExam = ResultadoExaminacion.idResultadoExam'
        );

      qbResultadosExaminacion
        .andWhere('ColaExaminacion.idColaExaminacion IN (:...idsColaExaminacion)', {
          idsColaExaminacion: idsColaExaminacion,
        })
        .andWhere('ColaExaminacion.idTipoExaminacion = :idTipoExaminacion', {
          idTipoExaminacion: tipoExaminacion,
        });

      const resultadoColaExaminacion: ColaExaminacionEntity[] = await qbResultadosExaminacion.getMany();

      // const resultadoColaExaminacion: ColaExaminacionEntity[] = await this.colaexamRespository.find({
      //   //idResultadoExam: resultadoExaminacionCola.idResultadoExam,
      //   where: qb => {
      //     qb.where(
      //         'ColaExaminacionEntity.idColaExaminacion IN (:...idsColaExam)', { idsColaExam: idsColaExaminacion }
      //     )
      //   }
      // });

      if (resultadoColaExaminacion) {
        resultadoColaExaminacion.forEach(ce => {
          ce.idEstadosExaminacion = estadoPendienteExaminacion.idEstadosExaminacion;
        });
      }

      // this.resulexaminRespository.save(resultadosExam);
      this.colaexamRespository.save(resultadoColaExaminacion);

      resultado.Mensaje = 'Se han guardado los resultados de examinación correctamente';
      resultado.ResultadoOperacion = true;
    } catch (Error) {
      resultado.Error = 'Se ha producido un error, no se pudo asignar los resultados de examinación';
      resultado.ResultadoOperacion = true;
    }

    return resultado;
  }

  async setEstadoPendientePreIdoneidadMoralByTramites(idTramite: number, idEstado: number) {
    const resultado: Resultado = new Resultado();

    try {
      const idsColaExaminaciones: number[] = [1, 2, 3];

      const colaExaminacion: ColaExaminacionEntity[] = await this.colaexamRespository
        .createQueryBuilder('colaexam')
        .where('colaexam.idColaExaminacion IN (:... idsColaExaminaciones)', {
          idsColaExaminaciones,
        })
        .getMany();

      if (colaExaminacion) {
        colaExaminacion.forEach(async x => {
          x.idEstadosExaminacion = idEstado;

          await this.colaexamRespository.save(x);
        });

        return {};
      } else {
        resultado.Error = 'No existe el id de examinación especificado.';
        resultado.ResultadoOperacion = false;
      }
    } catch (Error) {
      resultado.Error = 'Se ha producido un error al modificar el estado de la examinación.';
      resultado.ResultadoOperacion = false;
    }
  }

  async setEstadoColaExaminacionById(idColaExam: number, nuevoEstadoColaExam: string) {
    const resultado: Resultado = new Resultado();

    try {
      const estadoPendienteExaminacion: estadosExaminacionEntity = await this.estadosExamRepository.findOne({
        codigo: nuevoEstadoColaExam,
      });

      const colaExaminacion: ColaExaminacionEntity = await this.colaexamRespository.findOne({
        idColaExaminacion: idColaExam,
      });

      if (colaExaminacion) {
        colaExaminacion.idEstadosExaminacion = estadoPendienteExaminacion.idEstadosExaminacion;

        await this.colaexamRespository.save(colaExaminacion);

        if (nuevoEstadoColaExam == ESTADO_EXAM_PENDIENTE_PRE_IDON_MORAL_CODE) {
          resultado.Mensaje = 'Se ha modificado el estado de la examinación a Espera de Pre evaluación de Id. Moral correctamente';
        }
        if (nuevoEstadoColaExam == ESTADO_EXAM_PENDIENTE_IDON_MORAL_CODE) {
          resultado.Mensaje = 'Se ha modificado el estado de la examinación a Espera de Evaluación de Id. Moral correctamente';
        }

        resultado.ResultadoOperacion = true;

        return resultado;
      } else {
        resultado.Error = 'No existe el id de examinación especificado.';
        resultado.ResultadoOperacion = false;
      }
    } catch (Error) {
      resultado.Error = 'Se ha producido un error al modificar el estado de la examinación.';
      resultado.ResultadoOperacion = false;
    }
  }

  async setExaminacionEsperaIdoneidadMoral(idColaExaminacion: number) {
    const colaExaminacion: ColaExaminacionEntity = await this.colaexamRespository.findOne({
      idColaExaminacion: idColaExaminacion,
    });
    const resultadoExaminacionCola: ResultadoExaminacionColaEntity = await this.resulExaminacionColaRespository.findOne({
      idColaExaminacion: idColaExaminacion,
    });

    const estadosExaminacionEntity: estadosExaminacionEntity = await this.estadosExamRepository.findOne({
      codigo: codigoEsperaIdoneidadMoral,
    });

    colaExaminacion.idEstadosExaminacion = estadosExaminacionEntity.idEstadosExaminacion;
    await this.colaexamRespository.save(colaExaminacion);
    return {};
  }

  async getListaPendientesAtencionEstadosExam(idTipoExaminacion: number, options: IPaginationOptions, orden?: string, ordenarPor?: string) {
    const qb = this.colaexamRespository
      .createQueryBuilder('ColaExaminacion')
      .leftJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesEntity, 'tr', 'ColaExaminacion.idTramite = tr.idTramite')
      .leftJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesClaseLicencia, 'tc', 'ColaExaminacion.idTramite = tc.idTramite')
      .leftJoinAndMapMany('tr.idEstadoTramite', EstadosTramiteEntity, 'EstadoTramite', 'tr.idEstadoTramite = EstadoTramite.idEstadoTramite')
      .leftJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
      .leftJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
      .leftJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')
      .leftJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante')
      .leftJoinAndMapMany('so.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .leftJoinAndMapMany(
        'so.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .leftJoinAndMapMany(
        'so.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      )
      .leftJoinAndMapMany(
        'ColaExaminacion.idEstadosExaminacion',
        estadosExaminacionEntity,
        'esexa',
        'ColaExaminacion.idEstadosExaminacion = esexa.idEstadosExaminacion'
      );

    qb.andWhere('ColaExaminacion.historico = false');
    qb.andWhere('ColaExaminacion.idEstadosExaminacion IN (31,4,3,5,2,1)');
    if (idTipoExaminacion)
      qb.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
        idTipoExaminacion: idTipoExaminacion,
      });

    if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
    if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
    if (orden === 'run' && ordenarPor === 'ASC') qb.orderBy('po.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qb.orderBy('po.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qb.orderBy('po.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qb.orderBy('po.Nombres', 'DESC');
    if (orden === 'claseLicencia' && ordenarPor === 'ASC') qb.orderBy('cl.Abreviacion', 'ASC');
    if (orden === 'claseLicencia' && ordenarPor === 'DESC') qb.orderBy('cl.Abreviacion', 'DESC');
    if (orden === 'fechaInicioTramite' && ordenarPor === 'ASC') qb.orderBy('tr.update_at', 'ASC');
    if (orden === 'fechaInicioTramite' && ordenarPor === 'DESC') qb.orderBy('tr.update_at', 'DESC');
    if (orden === 'idSolicitud' && ordenarPor === 'ASC') qb.orderBy('tr.idSolicitud', 'ASC');
    if (orden === 'idSolicitud' && ordenarPor === 'DESC') qb.orderBy('tr.idSolicitud', 'DESC');
    if (orden === 'idTramite' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.idTramite', 'ASC');
    if (orden === 'idTramite' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.idTramite', 'DESC');

    return paginateRaw<any>(qb, options);
  }

  async getListaPendientesAtencionEstadosExamV2(
    idTipoExaminacion: number,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ) {
    const qbSolicitudes = this.postulanteEntityRespository
      .createQueryBuilder('po')
      .innerJoinAndMapMany(
        'po.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .innerJoinAndMapMany(
        'po.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      )
      .innerJoinAndMapMany('po.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .innerJoinAndMapMany('po.solicitudes', Solicitudes, 'so', 'po.idPostulante = so.idPostulante');
    //.innerJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')

    if (orden === 'run' && ordenarPor === 'ASC') qbSolicitudes.orderBy('po.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qbSolicitudes.orderBy('po.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qbSolicitudes.orderBy('po.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qbSolicitudes.orderBy('po.Nombres', 'DESC');
    if (orden === 'idSolicitud' && ordenarPor === 'ASC') qbSolicitudes.orderBy('so.idSolicitud', 'ASC');
    if (orden === 'idSolicitud' && ordenarPor === 'DESC') qbSolicitudes.orderBy('so.idSolicitud', 'DESC');
    // if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qbSolicitudes.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
    // if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qbSolicitudes.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
    // if (orden === 'claseLicencia' && ordenarPor === 'ASC') qbSolicitudes.orderBy('cl.Abreviacion', 'ASC');
    // if (orden === 'claseLicencia' && ordenarPor === 'DESC') qbSolicitudes.orderBy('cl.Abreviacion', 'DESC');
    // if (orden === 'fechaInicioTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('tr.update_at', 'ASC');
    // if (orden === 'fechaInicioTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('tr.update_at', 'DESC');
    // if (orden === 'idTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('ColaExaminacion.idTramite', 'ASC');
    // if (orden === 'idTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('ColaExaminacion.idTramite', 'DESC');

    let resultadoSolicitudes: any = await paginateRaw<any>(qbSolicitudes, options);

    for (let indexSolicitudes = 0; indexSolicitudes < resultadoSolicitudes.items.length; indexSolicitudes++) {
      const qbTramites = this.tramitesEntityRespository
        .createQueryBuilder('tr')
        .innerJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
        .innerJoinAndMapMany(
          'tr.idEstadoTramite',
          EstadosTramiteEntity,
          'EstadoTramite',
          'tr.idEstadoTramite = EstadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapMany(
          'tr.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'ColaExaminacionNM',
          'ColaExaminacionNM.idTramite = tr.idTramite'
        )
        .innerJoinAndMapMany(
          'ColaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'ColaExaminacion',
          'ColaExaminacionNM.idColaExaminacion = ColaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.idTipoExaminacion',
          TipoExaminacionesEntity,
          'te',
          'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.idEstadosExaminacion',
          estadosExaminacionEntity,
          'esexa',
          'ColaExaminacion.idEstadosExaminacion = esexa.idEstadosExaminacion'
        )
        .innerJoinAndMapMany('tr.idTramite', TramitesClaseLicencia, 'tc', 'tr.idTramite = tc.idTramite')
        .innerJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
        .innerJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
        .innerJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante');

      qbTramites.andWhere('tr.idSolicitud = ' + resultadoSolicitudes.items[indexSolicitudes].so_idSolicitud);
      qbTramites.andWhere('ColaExaminacion.historico = false');
      qbTramites.andWhere('ColaExaminacion.idEstadosExaminacion IN (31,5,2,1)');
      if (idTipoExaminacion)
        qbTramites.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
          idTipoExaminacion: idTipoExaminacion,
        });

      const resultadoTramites: any = await qbTramites.getMany();
      resultadoSolicitudes.items[indexSolicitudes]['tramites'] = resultadoTramites;
    }

    // V1
    //resultadoSolicitudes.items = resultadoSolicitudes.items.filter(x => { x.tramites.length > 0})

    // V2
    let tempItems = [];
    resultadoSolicitudes.items.forEach(element => {
      if (element.tramites.length > 0) {
        tempItems.push(element);
      }
    });
    resultadoSolicitudes.items = tempItems;

    return resultadoSolicitudes;
  }

  async getListaPendientesAtencionEstadosExamFiltrados(
    idTipoExaminacion: number,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: string,
    RUN?,
    Nombres?,
    Nomclaselic?,
    fechaIngresoResultado?,
    idSolicitud?
  ) {
    const qbSolicitudes = this.postulanteEntityRespository
      .createQueryBuilder('po')
      .innerJoinAndMapMany(
        'po.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .innerJoinAndMapMany(
        'po.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      )
      .innerJoinAndMapMany('po.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .innerJoinAndMapMany('po.solicitudes', Solicitudes, 'so', 'po.idPostulante = so.idPostulante');
    //.innerJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')

    if (orden === 'run' && ordenarPor === 'ASC') qbSolicitudes.orderBy('po.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qbSolicitudes.orderBy('po.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qbSolicitudes.orderBy('po.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qbSolicitudes.orderBy('po.Nombres', 'DESC');
    if (orden === 'idSolicitud' && ordenarPor === 'ASC') qbSolicitudes.orderBy('so.idSolicitud', 'ASC');
    if (orden === 'idSolicitud' && ordenarPor === 'DESC') qbSolicitudes.orderBy('so.idSolicitud', 'DESC');
    // if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qbSolicitudes.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
    // if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qbSolicitudes.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
    // if (orden === 'claseLicencia' && ordenarPor === 'ASC') qbSolicitudes.orderBy('cl.Abreviacion', 'ASC');
    // if (orden === 'claseLicencia' && ordenarPor === 'DESC') qbSolicitudes.orderBy('cl.Abreviacion', 'DESC');
    // if (orden === 'fechaInicioTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('tr.update_at', 'ASC');
    // if (orden === 'fechaInicioTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('tr.update_at', 'DESC');
    // if (orden === 'idTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('ColaExaminacion.idTramite', 'ASC');
    // if (orden === 'idTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('ColaExaminacion.idTramite', 'DESC');

    let resultadoSolicitudes: any = await paginateRaw<any>(qbSolicitudes, options);

    for (let indexSolicitudes = 0; indexSolicitudes < resultadoSolicitudes.items.length; indexSolicitudes++) {
      const qbTramites = this.tramitesEntityRespository
        .createQueryBuilder('tr')
        .innerJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
        .innerJoinAndMapMany(
          'tr.idEstadoTramite',
          EstadosTramiteEntity,
          'EstadoTramite',
          'tr.idEstadoTramite = EstadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapMany(
          'tr.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'ColaExaminacionNM',
          'ColaExaminacionNM.idTramite = tr.idTramite'
        )
        .innerJoinAndMapMany(
          'ColaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'ColaExaminacion',
          'ColaExaminacion.idColaExaminacion = ColaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.idTipoExaminacion',
          TipoExaminacionesEntity,
          'te',
          'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.idEstadosExaminacion',
          estadosExaminacionEntity,
          'esexa',
          'ColaExaminacion.idEstadosExaminacion = esexa.idEstadosExaminacion'
        )
        .innerJoinAndMapMany('tr.idTramite', TramitesClaseLicencia, 'tc', 'tr.idTramite = tc.idTramite')
        .innerJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
        .innerJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
        .innerJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante');

      qbTramites.andWhere('tr.idSolicitud = ' + resultadoSolicitudes.items[indexSolicitudes].so_idSolicitud);
      qbTramites.andWhere('ColaExaminacion.historico = false');
      qbTramites.andWhere('ColaExaminacion.idEstadosExaminacion IN (31,4,3,5,2,1)');
      if (idTipoExaminacion)
        qbTramites.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
          idTipoExaminacion: idTipoExaminacion,
        });

      if (RUN) {
        const rut = RUN.split('-');
        let runsplited: string = rut[0];
        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');
        const div = rut[1];
        qbTramites.andWhere('po.RUN = :run', {
          run: runsplited,
        });

        qbTramites.andWhere('po.DV = :dv', {
          dv: div,
        });
      }

      if (Nombres)
        qbTramites.andWhere(
          '(lower(unaccent("po"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("po"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("po"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${Nombres}%`,
          }
        );
      if (Nomclaselic)
        qbTramites.andWhere('cl.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: Nomclaselic,
        });
      if (fechaIngresoResultado)
        qbTramites.andWhere(`DATE_TRUNC('day',"so"."FechaCreacion") = :created_at`, {
          created_at: fechaIngresoResultado,
        });
      if (idSolicitud)
        qbTramites.andWhere('so.idSolicitud = :idSolicitud', {
          idSolicitud: idSolicitud,
        });

      const resultadoTramites: any = await qbTramites.getMany();
      resultadoSolicitudes.items[indexSolicitudes]['tramites'] = resultadoTramites;
    }

    // V1
    //resultadoSolicitudes.items = resultadoSolicitudes.items.filter(x => { x.tramites.length > 0})

    // V2
    let tempItems = [];
    resultadoSolicitudes.items.forEach(element => {
      if (element.tramites.length > 0) {
        tempItems.push(element);
      }
    });
    resultadoSolicitudes.items = tempItems;

    return resultadoSolicitudes;
  }

  async getListaPendientesAtencionEstadosIdoniedadMoral(
    idTipoExaminacion: number,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ) {
    const qb = this.colaexamRespository
      .createQueryBuilder('ColaExaminacion')
      .leftJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesEntity, 'tr', 'ColaExaminacion.idTramite = tr.idTramite')
      .leftJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesClaseLicencia, 'tc', 'ColaExaminacion.idTramite = tc.idTramite')
      .leftJoinAndMapMany('tr.idEstadoTramite', EstadosTramiteEntity, 'EstadoTramite', 'tr.idEstadoTramite = EstadoTramite.idEstadoTramite')
      .leftJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
      .leftJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
      .leftJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')
      .leftJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante')
      .leftJoinAndMapMany('so.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .leftJoinAndMapMany(
        'so.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .leftJoinAndMapMany(
        'so.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      )
      .leftJoinAndMapMany(
        'ColaExaminacion.idEstadosExaminacion',
        estadosExaminacionEntity,
        'esexa',
        'ColaExaminacion.idEstadosExaminacion = esexa.idEstadosExaminacion'
      );

    qb.andWhere('ColaExaminacion.historico = false');
    qb.andWhere('ColaExaminacion.idEstadosExaminacion in(4,3,2,31,32,33,34,6)');

    if (idTipoExaminacion)
      qb.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
        idTipoExaminacion: idTipoExaminacion,
      });
    if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
    if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
    if (orden === 'run' && ordenarPor === 'ASC') qb.orderBy('po.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qb.orderBy('po.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qb.orderBy('po.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qb.orderBy('po.Nombres', 'DESC');
    if (orden === 'claseLicencia' && ordenarPor === 'ASC') qb.orderBy('cl.Abreviacion', 'ASC');
    if (orden === 'claseLicencia' && ordenarPor === 'DESC') qb.orderBy('cl.Abreviacion', 'DESC');
    if (orden === 'fechaInicioTramite' && ordenarPor === 'ASC') qb.orderBy('tr.update_at', 'ASC');
    if (orden === 'fechaInicioTramite' && ordenarPor === 'DESC') qb.orderBy('tr.update_at', 'DESC');
    if (orden === 'idSolicitud' && ordenarPor === 'ASC') qb.orderBy('tr.idSolicitud', 'ASC');
    if (orden === 'idSolicitud' && ordenarPor === 'DESC') qb.orderBy('tr.idSolicitud', 'DESC');
    if (orden === 'idTramite' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.idTramite', 'ASC');
    if (orden === 'idTramite' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.idTramite', 'DESC');
    return paginateRaw<any>(qb, options);
  }

  async getListaPendientesAtencionEstadosIdoniedadMoralV2(
    idTipoExaminacion: number,
    options: IPaginationOptions,
    run?,
    nombreApellido?,
    claseLicenicia?,
    idSolicitud?,
    fechaInicio?,
    estado?,
    orden?: string,
    ordenarPor?: string
  ) {
    try {
      const qbSolicitudes = this.postulanteEntityRespository
        .createQueryBuilder('po')
        .innerJoinAndMapMany(
          'po.estadoCivil',
          OpcionEstadosCivilEntity,
          'EstadosCivil',
          'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
        )
        .innerJoinAndMapMany(
          'po.nuivelEducacional',
          OpcionesNivelEducacional,
          'NivelEducacional',
          'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
        )
        .innerJoinAndMapMany('po.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
        .innerJoinAndMapMany('po.solicitudes', Solicitudes, 'so', 'po.idPostulante = so.idPostulante');
      //.innerJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')

      if (orden === 'run' && ordenarPor === 'ASC') qbSolicitudes.orderBy('po.RUN', 'ASC');
      if (orden === 'run' && ordenarPor === 'DESC') qbSolicitudes.orderBy('po.RUN', 'DESC');
      if (orden === 'postulante' && ordenarPor === 'ASC') qbSolicitudes.orderBy('po.Nombres', 'ASC');
      if (orden === 'postulante' && ordenarPor === 'DESC') qbSolicitudes.orderBy('po.Nombres', 'DESC');
      if (orden === 'idSolicitud' && ordenarPor === 'ASC') qbSolicitudes.orderBy('so.idSolicitud', 'ASC');
      if (orden === 'idSolicitud' && ordenarPor === 'DESC') qbSolicitudes.orderBy('so.idSolicitud', 'DESC');
      // if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qbSolicitudes.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
      // if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qbSolicitudes.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
      // if (orden === 'claseLicencia' && ordenarPor === 'ASC') qbSolicitudes.orderBy('cl.Abreviacion', 'ASC');
      // if (orden === 'claseLicencia' && ordenarPor === 'DESC') qbSolicitudes.orderBy('cl.Abreviacion', 'DESC');
      // if (orden === 'fechaInicioTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('tr.update_at', 'ASC');
      // if (orden === 'fechaInicioTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('tr.update_at', 'DESC');
      // if (orden === 'idTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('ColaExaminacion.idTramite', 'ASC');
      // if (orden === 'idTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('ColaExaminacion.idTramite', 'DESC');

      let resultadoSolicitudes: any = await paginateRaw<any>(qbSolicitudes, options);

      for (let indexSolicitudes = 0; indexSolicitudes < resultadoSolicitudes.items.length; indexSolicitudes++) {
        const qbTramites = this.tramitesEntityRespository
          .createQueryBuilder('tr')
          .innerJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
          .innerJoinAndMapMany(
            'tr.idEstadoTramite',
            EstadosTramiteEntity,
            'EstadoTramite',
            'tr.idEstadoTramite = EstadoTramite.idEstadoTramite'
          )
          .innerJoinAndMapMany(
            'tr.colaExaminacionNM',
            ColaExaminacionTramiteNMEntity,
            'ColaExaminacionNM',
            'ColaExaminacionNM.idTramite = tr.idTramite'
          )
          .innerJoinAndMapMany(
            'ColaExaminacionNM.colaExaminacion',
            ColaExaminacionEntity,
            'ColaExaminacion',
            'ColaExaminacionNM.idColaExaminacion = ColaExaminacion.idColaExaminacion'
          )
          .innerJoinAndMapMany(
            'ColaExaminacion.colaExaminacionNM',
            ColaExaminacionTramiteNMEntity,
            'ColaExaminacionNMEstadoAlerta',
            'ColaExaminacionNMEstadoAlerta.idTramite = ColaExaminacion.idTramite'
          )
          .leftJoinAndMapMany(
            'ColaExaminacionNMEstadoAlerta.colaExaminacionEstadoAlerta',
            ColaExaminacionEntity,
            'colaExaminacionEstadoAlerta',
            'ColaExaminacionNMEstadoAlerta.idColaExaminacion = colaExaminacionEstadoAlerta.idColaExaminacion'
          )
          //.innerJoinAndMapMany('ColaExaminacionNMEstadoAlerta.tramiteAlerta', TramitesEntity, 'tramiteAlerta', 'ColaExaminacionNMEstadoAlerta.idTramite = tramiteAlerta.idTramite')
          .innerJoinAndMapMany(
            'ColaExaminacion.idTipoExaminacion',
            TipoExaminacionesEntity,
            'te',
            'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
          )
          .innerJoinAndMapMany(
            'ColaExaminacion.idEstadosExaminacion',
            estadosExaminacionEntity,
            'esexa',
            'ColaExaminacion.idEstadosExaminacion = esexa.idEstadosExaminacion'
          )
          .innerJoinAndMapMany('tr.idTramite', TramitesClaseLicencia, 'tc', 'tr.idTramite = tc.idTramite')
          .innerJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
          .innerJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
          .innerJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante');

        qbTramites.andWhere('tr.idSolicitud = ' + resultadoSolicitudes.items[indexSolicitudes].so_idSolicitud);
        qbTramites.andWhere('ColaExaminacion.historico = false');
        qbTramites.andWhere('ColaExaminacion.idEstadosExaminacion IN (31,34,6,7)');

        //qbTramites.andWhere('colaExaminacionEstadoAlerta.idEstadosExaminacion IN (3,4)');

        if (idTipoExaminacion)
          qbTramites.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
            idTipoExaminacion: idTipoExaminacion,
          });

        // Filtros
        if (run) {
          const rut = run.split('-');
          let runsplited: string = rut[0];
          runsplited = runsplited.replace('.', '');
          runsplited = runsplited.replace('.', '');
          const div = rut[1];
          qbTramites.andWhere('po.RUN = :run', {
            run: runsplited,
          });

          qbTramites.andWhere('po.DV = :dv', {
            dv: div,
          });
        }
        if (nombreApellido)
          qbTramites.andWhere(
            '(lower(unaccent("po"."Nombres")) like lower(unaccent(:nombre))' +
              ' or lower(unaccent("po"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
              ' or lower(unaccent("po"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
            {
              nombre: `%${nombreApellido}%`,
            }
          );
        if (claseLicenicia)
          qbTramites.andWhere('cl.idClaseLicencia = :idClaseLicencia', {
            idClaseLicencia: claseLicenicia,
          });
        if (fechaInicio)
          qbTramites.andWhere(`DATE_TRUNC('day',"so"."FechaCreacion") = :created_at`, {
            created_at: fechaInicio,
          });
        if (idSolicitud)
          qbTramites.andWhere('so.idSolicitud = :idSolicitud', {
            idSolicitud: idSolicitud,
          });
        //TODO: if (estado) qbTramites.andWhere('......... = :resultadoExamen', { estado: estado });

        const resultadoTramites: any = await qbTramites.getMany();
        resultadoSolicitudes.items[indexSolicitudes]['tramites'] = resultadoTramites;
      }

      // V1
      //resultadoSolicitudes.items = resultadoSolicitudes.items.filter(x => { x.tramites.length > 0})

      // V2
      let tempItems = [];
      resultadoSolicitudes.items.forEach(element => {
        if (element.tramites.length > 0) {
          tempItems.push(element);
        }
      });

      // Después de guardar los items, procedemos a asignar si tiene alguna cola con alguna alerta
      let contador = 0;
      tempItems.forEach(solicitudes => {
        if (solicitudes?.tramites[0]?.colaExaminacionNM[0]?.colaExaminacion[0]?.colaExaminacionNM) {
          let colasExaminacionesAsociadas: any = solicitudes.tramites[0].colaExaminacionNM[0].colaExaminacion[0].colaExaminacionNM;

          let colasExaminacionConAlerta = colasExaminacionesAsociadas.filter(
            x => x.colaExaminacionEstadoAlerta[0].idEstadosExaminacion == 3 || x.colaExaminacionEstadoAlerta[0].idEstadosExaminacion == 4
          );

          if (colasExaminacionConAlerta.length < 1) tempItems[contador].EstadoAlerta = 'SINALERTA';
          else {
            if (colasExaminacionConAlerta[0].colaExaminacionEstadoAlerta[0].idEstadosExaminacion == 3) {
              tempItems[contador].EstadoAlerta = 'ALERTADO';
            } else {
              tempItems[contador].EstadoAlerta = 'NOALERTADO';
            }
          }
        }

        contador = contador + 1;
      });

      resultadoSolicitudes.items = tempItems;

      return resultadoSolicitudes;
    } catch (Error) {
      return [];
    }
  }

  async getFormularioExaminacionClaseLicencia(idTipoTramite: number, idTipoExaminacion: number, idClaseLicencia: number) {
    const formulario = await this.formulariosexamRespository
      .createQueryBuilder('FormulariosExaminaciones')
      .innerJoinAndMapMany(
        'FormularioExaminacionClaseLicencia',
        FormularioExaminacionClaseLicenciaEntity,
        'fexcla',
        'FormulariosExaminaciones.idFromularioExaminaciones = fexcla.idFormularioExaminaciones'
      )
      .innerJoinAndMapMany(
        'FormulariosExaminaciones.idTipoTramite',
        TiposTramiteEntity,
        'titra',
        'FormulariosExaminaciones.idTipoTramite = titra.idTipoTramite'
      )
      .innerJoinAndMapMany('ClasesLicencias', ClasesLicenciasEntity, 'clic', 'fexcla.idClaseLicencia = clic.idClaseLicencia');

    formulario.where('FormulariosExaminaciones.idTipoTramite = :idTipoTramite', {
      idTipoTramite: idTipoTramite,
    });
    formulario.where('FormulariosExaminaciones.idTipoExaminacion = :idTipoExaminacion', {
      idTipoExaminacion: idTipoExaminacion,
    });
    formulario.andWhere('fexcla.idClaseLicencia = :idClaseLicencia', {
      idClaseLicencia: idClaseLicencia,
    });
    return formulario.getMany();
  }

  async getResultadoExaminacionByFormulario(idFormularioExaminaciones: number, idColaExaminacion: number) {
    const resultadoExaminacion = await this.resulexaminRespository
      .createQueryBuilder('ResultadoExaminacion')
      .innerJoinAndMapMany(
        'ResultadoExaminacionCola',
        ResultadoExaminacionColaEntity,
        'rec',
        'ResultadoExaminacion.idResultadoExam = rec.idResultadoExam'
      );
    resultadoExaminacion.where('ResultadoExaminacion.idFormularioExaminaciones = :idFormularioExaminaciones', {
      idFormularioExaminaciones: idFormularioExaminaciones,
    });
    resultadoExaminacion.andWhere('rec.idColaExaminacion = :idColaExaminacion', {
      idColaExaminacion: idColaExaminacion,
    });
    return resultadoExaminacion.getMany();
  }

  async getExamenesPendientesHistorico(
    idTipoExaminacion: number,
    run?: string,
    nombres?: string,
    Nomclaselic?: string,
    nomet?: string,
    fechaIngresoResultado?: Date,
    fechaRendicionExamen?: Date,
    nomExaminadorPractico?: number,
    idMunicipalidad?: number,
    resultadoExamen?: number,
    idEstadosExaminacion?: number,
    options?: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ) {
    const qb = this.colaexamRespository
      .createQueryBuilder('ColaExaminacion')
      .leftJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesEntity, 'tr', 'ColaExaminacion.idTramite = tr.idTramite')
      .leftJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesClaseLicencia, 'tc', 'ColaExaminacion.idTramite = tc.idTramite')
      .leftJoinAndMapMany('tr.idEstadoTramite', EstadosTramiteEntity, 'EstadoTramite', 'tr.idEstadoTramite = EstadoTramite.idEstadoTramite')
      .leftJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
      .leftJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
      .leftJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')
      .leftJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante')
      .leftJoinAndMapMany('so.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .leftJoinAndMapMany(
        'so.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .leftJoinAndMapMany(
        'so.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      );

    qb.andWhere('ColaExaminacion.historico = true');

    qb.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
      idTipoExaminacion: idTipoExaminacion,
    });

    qb.andWhere('ColaExaminacion.idEstadosExaminacion in (2,3,4,5,31,35,36)');

    if (run) {
      const rut = run.split('-');
      let runsplited: string = rut[0];
      runsplited = runsplited.replace('.', '');
      runsplited = runsplited.replace('.', '');
      const div = rut[1];
      qb.andWhere('po.RUN = :run', {
        run: runsplited,
      });

      qb.andWhere('po.DV = :dv', {
        dv: div,
      });
    }
    if (nombres)
      qb.andWhere(
        '(lower(unaccent("po"."Nombres")) like lower(unaccent(:nombre))' +
          ' or lower(unaccent("po"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
          ' or lower(unaccent("po"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
        {
          nombre: `%${nombres}%`,
        }
      );
    if (Nomclaselic)
      qb.andWhere('cl.idClaseLicencia = :idClaseLicencia', {
        idClaseLicencia: Nomclaselic,
      });
    if (fechaIngresoResultado)
      qb.andWhere(`DATE_TRUNC('day',"so"."FechaCreacion") = :created_at`, {
        created_at: fechaIngresoResultado,
      });
    if (idMunicipalidad)
      qb.andWhere('municipalidad.idInstitucion = :idInstitucion', {
        idInstitucion: idMunicipalidad,
      });
    if (resultadoExamen)
      qb.andWhere('ColaExaminacion.aprobado = :resultadoExamen', {
        resultadoExamen: resultadoExamen,
      });
    // const data = await qb.getRawMany();

    if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
    if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
    if (orden === 'run' && ordenarPor === 'ASC') qb.orderBy('po.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qb.orderBy('po.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qb.orderBy('po.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qb.orderBy('po.Nombres', 'DESC');
    if (orden === 'claseLicencia' && ordenarPor === 'ASC') qb.orderBy('cl.Abreviacion', 'ASC');
    if (orden === 'claseLicencia' && ordenarPor === 'DESC') qb.orderBy('cl.Abreviacion', 'DESC');

    return paginateRaw<any>(qb, options);
  }

  async getHistoricoExamenesv2(
    req: any,
    idTipoExaminacion: string,
    run?: string,
    nombres?: string,
    claseLicencia?: string,
    nombreTramite?: string,
    estadoTramite?: string,
    estadoAlerta?: string,
    estadoExaminacion?: string,
    fechaCreacion?: Date,
    idSolicitud?: string,
    options?: IPaginationOptions,
    orden?: string,
    ordenarPor?: string,
    recogerHistorico?: boolean
  ) {
    let res: Resultado = new Resultado();

    try {
      // Comprobación de tipos numéricos
      if (
        (idTipoExaminacion && !validarStringEsEntero(idTipoExaminacion)) ||
        (claseLicencia && !validarStringEsEntero(claseLicencia)) ||
        (nombreTramite && !validarStringEsEntero(nombreTramite)) ||
        (estadoTramite && !validarStringEsEntero(estadoTramite)) ||
        (estadoExaminacion && !validarStringEsEntero(estadoExaminacion)) ||
        (idSolicitud && !validarStringEsEntero(idSolicitud))
      ) {
        res.Error = 'Existe un error en los datos enviados, por favor revise los tipos de datos enviados.';
        res.ResultadoOperacion = false;
        res.Respuesta = [];

        return res;
      }

      let resultadoHistorial: PostulanteHistorialIdoneidadMoral[] = [];

      const qbSolicitudes = this.solicitudesEntityRespository
        .createQueryBuilder('Solicitudes')
        .innerJoinAndMapMany('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        )
        .innerJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .innerJoinAndMapMany(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacion,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'colaExaminacion.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNMEstadoAlerta',
          'colaExaminacionNMEstadoAlerta.idTramite = colaExaminacion.idTramite'
        )
        .leftJoinAndMapMany(
          'colaExaminacionNMEstadoAlerta.colaExaminacionEstadoAlerta',
          ColaExaminacionEntity,
          'colaExaminacionEstadoAlerta',
          'colaExaminacionNMEstadoAlerta.idColaExaminacion = colaExaminacionEstadoAlerta.idColaExaminacion'
        )

        // Recogemos las solicitudes que no estén en estado borrador ni en estado cerrada
        .andWhere('(Solicitudes.idEstado != 1)')
        .andWhere('(Solicitudes.idEstado != 5)');

      // Comprobamos permisos
      let permisoAComprobar: string = PermisosNombres.VisualizarListaPostulantesEvaluacionIdoneidadMoral;

      if (idTipoExaminacion == '5') {
        permisoAComprobar = PermisosNombres.VisualizarListaPostulantesEvaluacionIdoneidadMoral;
      } else if (idTipoExaminacion == '6') {
        permisoAComprobar = PermisosNombres.VisualizarListaPostulantesPreEvaluacionIdoneidadMoral;
      }
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, permisoAComprobar);

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      // 3.b Añadimos condición para filtrar por oficina si se da el caso
      // Filtramos por las oficinas en caso de que el usuario no sea administrador
      //Comprobamos si es superUsuario por si es necesario filtrar
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null) {
          res.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          res.ResultadoOperacion = false;
          res.Respuesta = [];

          return res;
        } else {
          qbSolicitudes.andWhere('Solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
        }
      }

      if (recogerHistorico == undefined || recogerHistorico == true) {
        qbSolicitudes.andWhere('colaExaminacion.historico = true');

        if (idTipoExaminacion) {
          qbSolicitudes.andWhere('colaExaminacion.idTipoExaminacion = :idTipoExaminacion', {
            idTipoExaminacion: idTipoExaminacion,
          });

          if (idTipoExaminacion == '5') {
            qbSolicitudes.andWhere('colaExaminacion.idEstadosExaminacion IN (35,36,37,38)');
          } else if (idTipoExaminacion == '6') {
            qbSolicitudes.andWhere('colaExaminacion.idEstadosExaminacion IN (3,4,35,36,37,38)');
          }
        }
      } else {
        qbSolicitudes.andWhere('colaExaminacion.historico = false');

        if (idTipoExaminacion) {
          qbSolicitudes.andWhere('colaExaminacion.idTipoExaminacion = :idTipoExaminacion', {
            idTipoExaminacion: idTipoExaminacion,
          });

          if (idTipoExaminacion == '5') {
            qbSolicitudes.andWhere('colaExaminacion.idEstadosExaminacion IN (31,7,6)');
          } else if (idTipoExaminacion == '6') {
            qbSolicitudes.andWhere('colaExaminacion.idEstadosExaminacion IN (31,2,1)');
          }
        }
      }

      // Filtros
      if (run) {
        const rut = run.split('-');
        let runsplited: string = rut[0];
        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');
        const div = rut[1];
        qbSolicitudes.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbSolicitudes.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }
      if (nombres) {
        qbSolicitudes.andWhere(
          '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${nombres}%`,
          }
        );
      }

      if (claseLicencia)
        qbSolicitudes.andWhere('clasesLicencias.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: claseLicencia,
        });

      if (fechaCreacion)
        qbSolicitudes.andWhere('"Solicitudes"."FechaCreacion"::date = :created_at::date', {
          //"FechaCreacion" > '2022-07-20'
          created_at: fechaCreacion,
        });

      //En el caso de recuperar los alertados
      if (estadoAlerta == '1') {
        qbSolicitudes.andWhere('colaExaminacionEstadoAlerta.idTipoExaminacion = :_idTipoExaminacion', {
          _idTipoExaminacion: TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL,
        });

        qbSolicitudes.andWhere('colaExaminacionEstadoAlerta.idEstadosExaminacion = :_idEstadosExaminacion', {
          _idEstadosExaminacion: 3, // Estado examinación alertado
        });
      }

      //En el caso de recuperar los no alertados
      if (estadoAlerta == '2') {
        qbSolicitudes.andWhere('colaExaminacionEstadoAlerta.idTipoExaminacion = :_idTipoExaminacion', {
          _idTipoExaminacion: TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL,
        });

        qbSolicitudes.andWhere('colaExaminacionEstadoAlerta.idEstadosExaminacion = :_idEstadosExaminacion', {
          _idEstadosExaminacion: 4, // Estado examinación no alertado
        });
      }

      //Filtro por estado de trámite
      if (estadoTramite)
        qbSolicitudes.andWhere('tramites.idEstadoTramite = :_idEstadoTramite', {
          _idEstadoTramite: estadoTramite,
        });

      //Filtro por tipo de tramite
      if (nombreTramite)
        qbSolicitudes.andWhere('tramites.idTipoTramite = :_idEstadosExaminacion', {
          _idEstadosExaminacion: nombreTramite,
        });

      // Filtrar por id solicitud
      if (idSolicitud)
        qbSolicitudes.andWhere('Solicitudes.idSolicitud = :_idSolicitud', {
          _idSolicitud: idSolicitud,
        });

      // Filtrar por estado examinación
      if (estadoExaminacion)
        qbSolicitudes.andWhere('colaExaminacion.idEstadosExaminacion = :_idEstadosExaminacion', {
          _idEstadosExaminacion: estadoExaminacion,
        });

      if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qbSolicitudes.orderBy('colaExaminacion.idColaExaminacion', 'ASC');
      if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qbSolicitudes.orderBy('colaExaminacion.idColaExaminacion', 'DESC');
      if (orden === 'run' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.RUN', 'ASC');
      if (orden === 'run' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.RUN', 'DESC');
      if (orden === 'idSolicitud' && ordenarPor === 'ASC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'ASC');
      if (orden === 'idSolicitud' && ordenarPor === 'DESC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'DESC');
      if (orden === 'fechaInicioTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('Solicitudes.FechaCreacion', 'ASC');
      if (orden === 'fechaInicioTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('Solicitudes.FechaCreacion', 'DESC');
      if (orden === 'estadoTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('tramites.idEstadoTramite', 'ASC');
      if (orden === 'estadoTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('tramites.idEstadoTramite', 'DESC');
      if (orden === 'postulante' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.Nombres', 'ASC');
      if (orden === 'postulante' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.Nombres', 'DESC');
      if (orden === 'claseLicencia' && ordenarPor === 'ASC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'ASC');
      if (orden === 'claseLicencia' && ordenarPor === 'DESC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'DESC');
      if (orden === 'idTramite' && ordenarPor === 'ASC') qbSolicitudes.orderBy('colaExaminacion.idTramite', 'ASC');
      if (orden === 'idTramite' && ordenarPor === 'DESC') qbSolicitudes.orderBy('colaExaminacion.idTramite', 'DESC');

      //const resultadoSolicitudes: Solicitudes[] = await qbSolicitudes.getMany();
      const resultadoSolicitudes: any = await paginate<Solicitudes>(qbSolicitudes, options);

      // Transformacion al DTO
      resultadoSolicitudes.items.forEach(res => {
        let historial: PostulanteHistorialIdoneidadMoral = new PostulanteHistorialIdoneidadMoral();

        historial.run = res.postulante[0].RUN + '' + res.postulante[0].DV;
        historial.nombrePostulante =
          res.postulante[0].Nombres + ' ' + res.postulante[0].ApellidoPaterno + ' ' + res.postulante[0].ApellidoMaterno;
        historial.idSolicitud = res.idSolicitud;
        historial.runFormateado = formatearRun(res.postulante[0].RUN + '-' + res.postulante[0].DV);

        historial.idTramites = res.tramites.map((t: any) => {
          const temp = {
            idTramite: t.idTramite,
            idTipoTramite: t.idTipoTramite,
            estadoTramiteString: t.estadoTramite.Nombre,
            tipoTramiteString: t.tiposTramite.Nombre,
          };
          return temp;
        });

        historial.clasesLicencias = res.tramites.map(t => t.tramitesClaseLicencia.clasesLicencias.Abreviacion);
        historial.fechaInicio = res.FechaCreacion;
        historial.estadoExaminacion = res.tramites[0].colaExaminacionNM[0].colaExaminacion[0].aprobado;
        historial.idEstadoTramite = res.tramites[0].colaExaminacionNM[0].colaExaminacion[0].idEstadosExaminacion;

        // Asignamos los estados de alerta
        //historial.estadoExaminacionDescripcion = res.tramites[0].colaExaminacionNM[0].colaExaminacionestadoAlerta[0]

        if (res.tramites[0].colaExaminacionNM[0].colaExaminacion[0].colaExaminacionNM) {
          let colasExaminacionesAsociadas: any = res.tramites[0].colaExaminacionNM[0].colaExaminacion[0].colaExaminacionNM;

          let colasExaminacionConAlerta = colasExaminacionesAsociadas.filter(
            x => x.colaExaminacionEstadoAlerta[0].idEstadosExaminacion == 3 || x.colaExaminacionEstadoAlerta[0].idEstadosExaminacion == 4
          );

          // Se recupera el estado en idoneidad Moral
          let colaExaminacionEstadoIdoneidadMoral = colasExaminacionesAsociadas.filter(
            x => x.colaExaminacionEstadoAlerta[0].idTipoExaminacion == 5
          );

          if (colasExaminacionConAlerta.length < 1) historial.EstadoAlerta = 'SINALERTA';
          else {
            if (colasExaminacionConAlerta[0].colaExaminacionEstadoAlerta[0].idEstadosExaminacion == 3) {
              historial.EstadoAlerta = 'ALERTADO';
            } else {
              historial.EstadoAlerta = 'NOALERTADO';
            }
          }

          if (
            colaExaminacionEstadoIdoneidadMoral &&
            colaExaminacionEstadoIdoneidadMoral.length > 0 &&
            colaExaminacionEstadoIdoneidadMoral[0].colaExaminacionEstadoAlerta[0].idEstadosExaminacion == 35
          ) {
            historial.estadoExaminacionAprobado = true;
          } else {
            historial.estadoExaminacionAprobado = false;
          }
        }

        resultadoHistorial.push(historial);
      });

      resultadoSolicitudes.items = resultadoHistorial;

      res.ResultadoOperacion = true;
      res.Respuesta = resultadoSolicitudes;

      return res;
    } catch (Error) {
      res.Error = 'Ha ocurrido un error en la respuesta, por favor consulte con el administrador de la aplicación.';
      res.ResultadoOperacion = false;
      res.Respuesta = [];

      return res;
    }
  }

  async getHistoricoExamenesDetalle(idColaExaminacion: number, idTipoTramite: number, idTipoExaminacion: number, idClaseLicencia: number) {
    const formularios = await this.getFormularioExaminacionClaseLicencia(idTipoTramite, idTipoExaminacion, idClaseLicencia);
    for (const form of formularios) {
      const resultadoExaminacion = await this.getResultadoExaminacionByFormulario(form.idFromularioExaminaciones, idColaExaminacion);
      form['resultadoExaminacion'] = resultadoExaminacion;
    }
    return formularios;
  }

  async getEstadoExaminacionesByIdSolicitud(idSolicitud: number) {
    let resultado: Resultado = new Resultado();

    try {
      // 0. Comprobación de permisos

      // 1. Nos traemos la información de la solicitud y el postulante asociado

      const qbSolicitudXPostulante = this.solicitudesEntityRespository
        .createQueryBuilder('Solicitudes')
        .innerJoinAndMapOne(
          'Solicitudes.postulante',
          PostulanteEntity,
          'Postulantes',
          'Solicitudes.idPostulante = Postulantes.idPostulante'
        );

      qbSolicitudXPostulante.andWhere('Solicitudes.idSolicitud = ' + idSolicitud);

      const infoSolicitudXPostulante: Solicitudes = await qbSolicitudXPostulante.getOne();

      // 2. Nos traemos la información de los trámites asociados junto a sus examinaciones
      const qbTramites = this.tramitesEntityRespository
        .createQueryBuilder('tr')
        .innerJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
        .innerJoinAndMapMany(
          'tr.idEstadoTramite',
          EstadosTramiteEntity,
          'EstadoTramite',
          'tr.idEstadoTramite = EstadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapMany(
          'tr.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'ColaExaminacionNM',
          'ColaExaminacionNM.idTramite = tr.idTramite'
        )
        .innerJoinAndMapMany(
          'ColaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'ColaExaminacion',
          'ColaExaminacion.idColaExaminacion = ColaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne(
          'tr.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tr.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'tramitesClaseLicencia.idClaseLicencia = clasesLicencias.idClaseLicencia'
        );
      qbTramites.andWhere('tr.idSolicitud = ' + idSolicitud);

      const resultadoTramites: TramitesEntity[] = await qbTramites.getMany();

      // Variable para guardar los ids de las examinaciones
      let idsColaExaminacion: number[] = [];

      resultadoTramites.forEach((tramite, i) => {
        tramite.colaExaminacionNM.forEach((colaNM, j) => {
          idsColaExaminacion.push(colaNM.idColaExaminacion);
        });
      });

      // 3. Nos traemos los resultados de examinación asociados
      const qbResultadosExaminacion = this.colaexamRespository
        .createQueryBuilder('ColaExaminacion')
        .innerJoinAndMapMany(
          'ColaExaminacion.TipoExaminacion',
          TipoExaminacionesEntity,
          'te',
          'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.ResultadoExaminacionCola',
          ResultadoExaminacionColaEntity,
          'ResultadoExaminacionCola',
          'ColaExaminacion.idColaExaminacion = ResultadoExaminacionCola.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'ResultadoExaminacion.idResultadoExam',
          ResultadoExaminacionEntity,
          'ResultadoExaminacion',
          'ResultadoExaminacionCola.idResultadoExam = ResultadoExaminacion.idResultadoExam'
        );

      qbResultadosExaminacion.andWhere('ColaExaminacion.idColaExaminacion IN (:...idsColaExaminacion)', {
        idsColaExaminacion: idsColaExaminacion,
      });

      let resultadosExam = await qbResultadosExaminacion.getMany();

      let resdto = new ResultadosExaminacionBySolicitudDTO();

      resdto.retDTO = [];

      resultadoTramites.forEach((tramite, i) => {
        resdto.run = infoSolicitudXPostulante.postulante.RUN + '-' + infoSolicitudXPostulante.postulante.DV;
        resdto.runFormateado = formatearRun(infoSolicitudXPostulante.postulante.RUN + '-' + infoSolicitudXPostulante.postulante.DV);
        resdto.nombrePostultante =
          infoSolicitudXPostulante.postulante.Nombres +
          ' ' +
          infoSolicitudXPostulante.postulante.ApellidoPaterno +
          ' ' +
          infoSolicitudXPostulante.postulante.ApellidoMaterno;

        let colaExaminacionTeorica: ColaExaminacionEntity[] = [];
        let colaExaminacionPractica: ColaExaminacionEntity[] = [];
        let colaExaminacionesMedica: ColaExaminacionEntity[] = [];
        let colaExaminacionIdonMoral: ColaExaminacionEntity[] = [];
        let colaExaminacionesPreIdonMoral: ColaExaminacionEntity[] = [];

        let ret = new ResultadoExaminacionTramiteDTO();

        // Asignar datos de trámite
        tramite.colaExaminacionNM.forEach(colNM => {
          //Consultamos las examinaciones por tipo y su estado ç(hay que sacarlos en arrays para poder establecer luego los estados fuera del segundo for)
          colaExaminacionTeorica.push(
            ...resultadosExam.filter(x => x.idColaExaminacion == colNM.idColaExaminacion && x.idTipoExaminacion == 2)
          );
          colaExaminacionPractica.push(
            ...resultadosExam.filter(x => x.idColaExaminacion == colNM.idColaExaminacion && x.idTipoExaminacion == 3)
          );
          colaExaminacionesMedica.push(
            ...resultadosExam.filter(x => x.idColaExaminacion == colNM.idColaExaminacion && x.idTipoExaminacion == 4)
          );
          colaExaminacionIdonMoral.push(
            ...resultadosExam.filter(x => x.idColaExaminacion == colNM.idColaExaminacion && x.idTipoExaminacion == 5)
          );
          colaExaminacionesPreIdonMoral.push(
            ...resultadosExam.filter(x => x.idColaExaminacion == colNM.idColaExaminacion && x.idTipoExaminacion == 6)
          );
        });

        // Aquí estableceríamos por cada trámite cada nota según lo acumulado en los arrays de colas de examinación
        ret.idTramite = tramite.idTramite;
        ret.claseLicencia = tramite.tramitesClaseLicencia.clasesLicencias.Abreviacion;

        ret.resultadoExaminacionTeorica = this.devolverResultadoColaExaminaciones(colaExaminacionTeorica);
        ret.resultadoExaminacionPractica = this.devolverResultadoColaExaminaciones(colaExaminacionPractica);
        ret.resultadoExaminacionMedica = this.devolverResultadoColaExaminaciones(colaExaminacionesMedica);
        ret.resultadoExaminacionIdonMoral = this.devolverResultadoColaExaminaciones(colaExaminacionIdonMoral);
        ret.resultadoExaminacionPreexamIdonMoral = this.devolverResultadoColaExaminaciones(colaExaminacionesPreIdonMoral);

        resdto.retDTO.push(ret);
      });

      resultado.Respuesta = resdto;
      resultado.ResultadoOperacion = true;
    } catch (Error) {
      resultado.Error = 'Se ha producido un error recuperando los trámites asociados a la solicitud';
      resultado.ResultadoOperacion = false;
    }

    return resultado;
  }

  public async getTipoTramites() {
    const qbTramites = this.tipoTramitesEntityRespository.createQueryBuilder('TiposTramite');

    let res = await qbTramites.getMany();

    return res;
  }

  public async getEstadosTramites() {
    let res: Resultado = new Resultado();

    try {
      let arrayEstadoTramiteDto: EstadoTramiteDto[] = [];

      const qbTramites = this.estadosTramitesEntityRespository.createQueryBuilder('EstadosTramite');

      let tipost: EstadosTramiteEntity[] = await qbTramites.getMany();

      tipost.forEach(x => {
        let estadosTramiteDto: EstadoTramiteDto = new EstadoTramiteDto();

        estadosTramiteDto.IdEstadoTramite = x.idEstadoTramite;
        estadosTramiteDto.Nombre = x.Nombre;
        estadosTramiteDto.Descripcion = x.Descripcion;

        arrayEstadoTramiteDto.push(estadosTramiteDto);
      });

      res.Respuesta = arrayEstadoTramiteDto;
      res.ResultadoOperacion = true;

      return res;
    } catch (Error) {
      res.ResultadoOperacion = true;
      res.Error = 'Ha ocurrido un error al obtener los estados de examinación';
    }
  }

  // Función para devolver el resultado de la cola examinación
  private devolverResultadoColaExaminaciones(colaExaminacionArray: ColaExaminacionEntity[]): string {
    let resultado: string = '';

    let aprobados: ColaExaminacionEntity[] = colaExaminacionArray.filter(x => x.aprobado == true);
    let suspensos: ColaExaminacionEntity[] = colaExaminacionArray.filter(x => x.aprobado == false);
    let sinContestar: ColaExaminacionEntity[] = colaExaminacionArray.filter(x => x.aprobado == null);

    if (suspensos.length > 0) {
      resultado = 'Reprobado';
    } else if (sinContestar.length > 0) {
      resultado = 'Pendiente';
    } else if (aprobados.length == colaExaminacionArray.length) {
      resultado = 'Aprobado';
    }

    return resultado;
  }
}
