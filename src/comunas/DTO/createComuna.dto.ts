import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateComunaDTO {

    @ApiPropertyOptional()
    idComuna:number;

    @ApiPropertyOptional()
    Nombre:string;

    @ApiPropertyOptional()
    Descripcion:string;

    @ApiPropertyOptional()
    idRegion:number;

    @ApiPropertyOptional()
    codigoComuna:number;
}
