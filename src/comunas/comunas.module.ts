import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComunasController } from './controller/comunas.controller';
import { ComunasEntity } from './entity/comunas.entity';
import { ComunasService } from './services/comunas.service';

@Module({
  imports: [TypeOrmModule.forFeature([ComunasEntity])],
  providers: [ComunasService],
  exports: [ComunasService],
  controllers: [ComunasController]
})
export class ComunasModule {}
