import { Test, TestingModule } from '@nestjs/testing';
import { ComunasController } from './comunas.controller';

describe('ComunasController', () => {
  let controller: ComunasController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ComunasController],
    }).compile();

    controller = module.get<ComunasController>(ComunasController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
