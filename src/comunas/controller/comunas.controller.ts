import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CreateComunaDTO } from '../DTO/createComuna.dto';
import { ComunasEntity } from '../entity/comunas.entity';
import { ComunasService } from '../services/comunas.service';


@ApiTags('Comunas')
@Controller('Comunas')
export class ComunasController {

    constructor(private readonly comunasService: ComunasService) { }

    @Get('comunas')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las comunas' })
    async getComunas() {
        const data = await this.comunasService.getComunas();
        return { data };
    }

    @Get('comunaById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Comuna por Id' })
    async comunaById(@Param('id') id: number) {
        const data = await this.comunasService.getComuna(id);
        return { data };
    }

    @Patch('comunaUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de una Comuna' })
    async comunaUpdate(@Param('id') id: number, @Body() data: Partial<CreateComunaDTO>) {
        return await this.comunasService.update(id, data);
    }

    @Post('crearComuna')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una nueva Region' })
    async addComuna(
        @Body() createComunaDTO: CreateComunaDTO) {
        const data = await this.comunasService.create(createComunaDTO);
        return {data};
    }

    @Get('comunaByRegion/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Comuna por Id region' })
    async comunaByRegion(@Param('id') id: number) {
        const data = await this.comunasService.getComunaByRegion(id);
        return { data };
    }

    @Get('comunasListado')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las comunas' })
    async comunasListado() {
        const data = await this.comunasService.getComunasListado();
        return { data };
    }

    @Get('comunaDTO/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Comuna por Id' })
    async comunaDTO(@Param('id') id: number) {
        const data = await this.comunasService.getComunaDTO(id);
        return { data };
    }

    @Get('comunaByRegionListado/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve Comunas por Id region' })
    async comunaByRegionListado(@Param('id') id: number) {
        const data = await this.comunasService.getComunaByRegionListado(id);
        return { data };
    }

}
