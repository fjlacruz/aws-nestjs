import { Proveedor } from './../../proveedor/entities/proveedor.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { User } from 'src/users/entity/user.entity';
import { Entity, Column, PrimaryColumn, OneToOne, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { RegionesEntity } from '../../escuela-conductores/entity/regiones.entity';
import { AudienciaNotificacionesComuna } from 'src/notificaciones/entity/audienciaComunasNotificaciones.entity';
import { MunicipalidadEntity } from 'src/municipalidad/entity/municipalidad.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';

@Entity('Comunas')
export class ComunasEntity {
  @PrimaryColumn()
  idComuna: number;

  @Column()
  Nombre: string;

  @Column()
  Descripcion: string;

  @Column()
  idRegion: number;
  @ManyToOne(() => RegionesEntity, regiones => regiones.comunas)
  @JoinColumn({ name: 'idRegion' })
  readonly regiones: RegionesEntity;

  @ApiProperty()
  @OneToOne(() => OficinasEntity, oficinas => oficinas.comunas)
  readonly oficinas: OficinasEntity;

  @Column()
  codigoComuna: number;

  @Column()
  codRNC: number;

  @ApiProperty()
  @OneToOne(() => Postulante, postulante => postulante.comunas)
  readonly postulante: Postulante;

  @ApiProperty()
  @OneToOne(() => User, user => user.comunas)
  readonly user: User;

  @OneToMany(() => Proveedor, proveedores => proveedores.comuna)
  readonly proveedores: Proveedor[];

  @OneToMany(() => AudienciaNotificacionesComuna, audicenciaComunaNotificaciones => audicenciaComunaNotificaciones.comuna)
  readonly AudienciaNotificacionesComuna: AudienciaNotificacionesComuna[];

  @ApiProperty()
  @OneToOne(() => MunicipalidadEntity, municipalidad => municipalidad.comuna)
  readonly municipalidad: MunicipalidadEntity;

  @ApiProperty()
  @OneToOne(() => InstitucionesEntity, institucion => institucion.comunas)
  readonly instituciones: InstitucionesEntity;

  @ApiProperty()
  @OneToOne(() => InstitucionesEntity, institucionAuxPermisos => institucionAuxPermisos.comunaAuxPermisos)
  readonly institucionesAuxPermisos: InstitucionesEntity;

  @ManyToOne(() => RegionesEntity, regionesAuxPermisos => regionesAuxPermisos.comunasAuxPermisos)
  @JoinColumn({ name: 'idRegion' })
  readonly regionesAuxPermisos: RegionesEntity;
}
