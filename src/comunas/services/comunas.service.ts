import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { CreateComunaDTO } from '../DTO/createComuna.dto';
import { ComunasEntity } from '../entity/comunas.entity';
import { getConnection } from "typeorm";
import { Resultado } from 'src/utils/resultado';
import { ComunasDTO } from '../DTO/comunas.dto';

@Injectable()
export class ComunasService {

    constructor(@InjectRepository(ComunasEntity) private readonly comunasRespository: Repository<ComunasEntity>) { }

    async getComunas() {
        return await this.comunasRespository.find();
    }

    async getComuna(id:number){
        const region= await this.comunasRespository.findOne(id);
        if (!region)throw new NotFoundException("comuna dont exist");
        
        return region;
    }

    async getComunaByRegion(id:number){
        const region = await getConnection() .createQueryBuilder() 
                .select("Comunas") 
                .from(ComunasEntity, "Comunas") 
                .where("Comunas.idRegion = :id", { id: id}).getMany();
                if (!region)throw new NotFoundException("comuna dont exist");        
        return region;
    }

    async getComunasListado(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoComunas: ComunasEntity[] = [];
        let resultadosDTO: ComunasDTO[] = [];

        try {
            resultadoComunas = await this.comunasRespository.find();

            if (Array.isArray(resultadoComunas) && resultadoComunas.length) {

                resultadoComunas.forEach(item => {
                    let nuevoElemento: ComunasDTO = {
                        idComuna: item.idComuna,
                        nombre: item.Nombre,
                        descripcion: item.Descripcion
                    }

                    resultadosDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Comunas obtenidas correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Comunas';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Comunas';
        }

        return resultado;
    }

    async getComunaDTO(id: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoComuna: ComunasEntity = null;
        let resultadoDTO: ComunasDTO = null;

        try {
            resultadoComuna = await this.comunasRespository.findOne(id);

            if (resultadoComuna != null && resultadoComuna != undefined) {
                resultadoDTO = {
                    idComuna: resultadoComuna.idComuna,
                    nombre: resultadoComuna.Nombre,
                    descripcion: resultadoComuna.Descripcion
                }
            }

            if (resultadoDTO != null && resultadoDTO != undefined) {
                resultado.Respuesta = resultadoDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Comuna obtenida correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontró la Comuna';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Comuna';
        }

        return resultado;
    }

    async getComunaByRegionListado(id: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoComunas: ComunasEntity[] = [];
        let resultadosDTO: ComunasDTO[] = [];

        try {
            resultadoComunas = await getConnection().createQueryBuilder()
                .select("Comunas")
                .from(ComunasEntity, "Comunas")
                .where("Comunas.idRegion = :id", { id: id }).getMany();

            if (Array.isArray(resultadoComunas) && resultadoComunas.length) {

                resultadoComunas.forEach(item => {
                    let nuevoElemento: ComunasDTO = {
                        idComuna: item.idComuna,
                        nombre: item.Nombre,
                        descripcion: item.Descripcion
                    }

                    resultadosDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
                // resultado.Respuesta = resultadoComunas;
                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Comunas obtenidas correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Comunas';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Comunas';
        }

        return resultado;
    }

    async update(idComuna: number, data: Partial<CreateComunaDTO>) {
        await this.comunasRespository.update({ idComuna }, data);
        return await this.comunasRespository.findOne({ idComuna });
    }

    async create(data: CreateComunaDTO): Promise<ComunasEntity> {
        return await this.comunasRespository.save(data);
    }
}
