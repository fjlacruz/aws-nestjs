import {ApiPropertyOptional} from '@nestjs/swagger';
import {Column} from 'typeorm';
import {DocumentsDto} from '../../documentmanager/DTO/documents.dto';

export class ComunicadosDTO {

    @ApiPropertyOptional()
    idComunicado: string;

    @ApiPropertyOptional()
    titulo: string;

    @ApiPropertyOptional()
    bajada: string;

    @ApiPropertyOptional()
    cuerpo: string;

    @ApiPropertyOptional()
    horaCreacion: string;

    @ApiPropertyOptional()
    fechaCreacion: Date;

    @ApiPropertyOptional()
    imagenCabeceraFileId: string;

    @ApiPropertyOptional()
    created_at: Date;

    @ApiPropertyOptional()
    files: DocumentsDto[]

    @ApiPropertyOptional()
    status: Boolean;


}
