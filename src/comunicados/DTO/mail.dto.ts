import {ApiPropertyOptional} from '@nestjs/swagger';


export class EmailDTO {

    @ApiPropertyOptional()
    html: string;

    @ApiPropertyOptional()
    replyTo: string;

    @ApiPropertyOptional()
    from: string;

    @ApiPropertyOptional()
    files: any[];

    @ApiPropertyOptional()
    subject: string;

    @ApiPropertyOptional()
    comunicado: string;

    @ApiPropertyOptional()
    fromName: string;

}
