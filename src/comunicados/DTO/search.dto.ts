import {ApiPropertyOptional} from '@nestjs/swagger';


export class SearchDTO {

    @ApiPropertyOptional()
    text: string;

    @ApiPropertyOptional()
    page: number;

    @ApiPropertyOptional()
    limit: number;

}
