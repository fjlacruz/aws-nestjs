import {Module} from '@nestjs/common';
import {ComunicadosService} from './service/comunicados.service';
import {ComunicadosController} from './controller/comunicados.controller';
import {ComunicadoEntity} from './entity/comunicados.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DocumentmanagerModule} from '../documentmanager/documentmanager.module';
import {PreguntasModule} from '../preguntas/preguntas.module';
import {JwtModule} from '@nestjs/jwt';
import {jwtConstants} from '../users/constants';
import {ReadStatusComunicadoEntity} from './entity/ReadStatusComunicado.entity';
import {PreguntaEntity} from '../preguntas/entity/preguntas.entity';
import {PreguntasService} from '../preguntas/service/preguntas.service';
import {ReadStatusPreguntaEntity} from '../preguntas/entity/ReadStatusPregunta.entity';
import {ParametrosGeneral} from '../parametros/entity/parametros.entity';
import {ParametrosService} from '../parametros/services/parametros.service';
import { ParametrosModule } from 'src/parametros/parametros.module';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';


@Module({

    imports: [DocumentmanagerModule, ParametrosModule,PreguntasModule, TypeOrmModule.forFeature([ComunicadoEntity,PreguntaEntity, ReadStatusComunicadoEntity, ReadStatusPreguntaEntity, ParametrosGeneral, User, RolesUsuarios]),

        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: '360d'}
        })
    ],
    providers: [ComunicadosService, PreguntasService, ParametrosService, AuthService],
    controllers: [ComunicadosController],
    exports: [ComunicadosService]
})
export class ComunicadosModule {
}
