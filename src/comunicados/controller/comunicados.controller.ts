import {Controller, Delete, Request, UploadedFile, UploadedFiles, UseInterceptors} from '@nestjs/common';
import {Get, Param, Post, Body, Put, Patch} from '@nestjs/common';
import {ApiBearerAuth, ApiOperation, ApiTags} from '@nestjs/swagger';
import {ComunicadosDTO} from '../DTO/comunicados.dto';
import {ComunicadosService} from '../service/comunicados.service';
import {v4 as uuidv4} from 'uuid';
import {FileInterceptor, FilesInterceptor} from '@nestjs/platform-express';
import {DocumentsService} from '../../documentmanager/service/documents.service';
import {DocumentsDto} from '../../documentmanager/DTO/documents.dto';
import * as _ from 'lodash';
import {JwtService} from '@nestjs/jwt';
import {AuthDTO} from '../../auth/auth.dto';
import {SearchDTO} from '../DTO/search.dto';
import {EmailDTO} from '../DTO/mail.dto';
import {PreguntasService} from '../../preguntas/service/preguntas.service';

@ApiTags('Comunicados-Manager')
@Controller('comunicados')
@ApiBearerAuth()
export class ComunicadosController {
    constructor(
        private comunicadosService: ComunicadosService,
        private preguntasService: PreguntasService,
        private fileService: DocumentsService,
        private jwtService: JwtService
    ) {
    }


    @Post('/')
    @ApiOperation({summary: 'Servicio para subir documentos asociado a un modulo'})
    @UseInterceptors(FileInterceptor('imagenCabecera'))

    async createComunicado(@UploadedFile() imagenCabecera = null, @Body() comunicadoDTO: ComunicadosDTO) {
        comunicadoDTO.idComunicado = uuidv4();
        if (imagenCabecera) {
            let optionsImagenCabecera = {width: 800, moduleName: 'comunicaciones'};
            const imagenCabeceraSaved = await this.fileService.createFile(imagenCabecera, optionsImagenCabecera);
            comunicadoDTO.imagenCabeceraFileId = imagenCabeceraSaved.idFile;
        }

        let data = await this.comunicadosService.createComunicado(comunicadoDTO);
        let response: any = {
            ...data
        }
        return response;

    }

    @Patch('/publish/:idComunicado')
    @ApiOperation({summary: 'Servicio que activa un comunicado'})
    async publish(@Param('idComunicado') idComunicado: string) {
        const data = await this.comunicadosService.publishComunicado(idComunicado);
        return data;
    }

    @Get('/')
    @ApiOperation({summary: 'Servicio que devuelve todos los comunicados'})
    async getComunicado() {
        const data = await this.comunicadosService.getComunicados();
        return data;
    }

    @Get('/all')
    @ApiOperation({summary: 'Servicio que devuelve todos los comunicados'})
    async getAllComunicado() {
        const data = await this.comunicadosService.getAllComunicados();
        return data;
    }

    @Get('/byMonth/:page?/:limit?')
    @ApiOperation({summary: 'Servicio que devuelve todos los comunicados agrupados por mes y año'})
    async getComunicadosByMonth(@Param('page') page: number, @Param('limit') limit: number, @Request() req) {
        let userData : AuthDTO = this.jwtService.decode(req.headers.authorization.split(" ")[1]) as AuthDTO;
        const data = await this.comunicadosService.getComunicadoByMonth(req, userData.user, page, limit);
        return data;
    }

    @Get('/:idComunicado')
    @ApiOperation({summary: 'Servicio que devuelve un comunicado'})
    async getComunicadoById(@Param('idComunicado') idComunicado: string, @Request() req) {
        const data = await this.comunicadosService.getComunicado(idComunicado);
        return data;
    }

    @Post('readComunicado/:idComunicado')
    @ApiOperation({summary: 'Servicio que marca comunicado como leido para un usuario'})
    async readComunicado(@Param('idComunicado') idComunicado: string, @Request() req) {
        let userData : AuthDTO = this.jwtService.decode(req.headers.authorization.split(" ")[1]) as AuthDTO;
        const data = await this.comunicadosService.readComunicado(idComunicado, userData.user);
    }


    @Post('buscar')
    @ApiOperation({summary: 'Servicio que busca publicaciones en comunicados oficiales'})
    async buscar(@Body() searchParams: SearchDTO, @Request() req) {

        const results = await this.comunicadosService.search(req, searchParams);
        return results;
    }

    @Post('sendMail')
    @ApiOperation({summary: 'Servicio que envia mail de contacto'})
    async sendMail(@Body() mail: EmailDTO, @Request() req) {
        let userData : AuthDTO = this.jwtService.decode(req.headers.authorization.split(" ")[1]) as AuthDTO;
        const results = await this.comunicadosService.sendMail(req, mail, userData.user);
        const resultsUsuario = await this.comunicadosService.sendEmailUsuario(req, mail, userData.user);
        return results;
    }

    @Get('/unreaded/count')
    @ApiOperation({summary: 'Servicio que devuelve la cantidad de  comunicados no leidos'})
    async getUnreaded(@Request() req) {
        let userData : AuthDTO = this.jwtService.decode(req.headers.authorization.split(" ")[1]) as AuthDTO;
        const unreadedComunicados = await this.comunicadosService.getCountUnreaded(userData.user);
        const unreadedPreguntas = await this.preguntasService.getCountUnreaded(userData.user);
        return {
            preguntas : unreadedPreguntas.count,
            comunicados : unreadedComunicados.count
        }
    }

    @Delete('/:idComunicado')
    @ApiOperation({summary: 'Servicio que elimina un comunicado que no ha sido publicado'})
    async deleteComunicado(@Param('idComunicado') idComunicado: string, @Request() req) {
        return this.comunicadosService.delete(idComunicado);
    }

}
