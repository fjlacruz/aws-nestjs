import {Entity, Column, PrimaryColumn, OneToMany, JoinColumn, Index, BaseEntity} from 'typeorm';

@Entity('ReadStatusComunicado')
export class ReadStatusComunicadoEntity extends BaseEntity {

    @PrimaryColumn()
    readStatusId: string;

    @Column()
    @Index()
    comunicadoId: string;

    @Column()
    @Index()
    userId: string;

    @Column({
        type: 'timestamp', default: () => `CURRENT_TIMESTAMP`})
    created_at: Date;
}
