import {Entity, Column, PrimaryColumn, OneToMany, JoinColumn, Index, BaseEntity} from 'typeorm';


@Entity('Comunicados')
export class ComunicadoEntity extends BaseEntity{

    @PrimaryColumn()
    idComunicado: string;

    @Index()
    @Column({
        nullable: true
    })
    imagenCabeceraFileId: string;

    @Column()
    titulo: string;

    @Column()
    cuerpo: string;

    @Column()
    bajada: string;

    @Column()
    horaCreacion: string;

    @Column()
    fechaCreacion: Date;

    @Column()
    status: Boolean;

    @Column({
        type: 'timestamp', default: () => `CURRENT_TIMESTAMP`
    })
    created_at: Date;

}
