import {Injectable, NotFoundException} from '@nestjs/common';
import {ComunicadoEntity} from '../entity/comunicados.entity';
import {ComunicadosDTO} from '../DTO/comunicados.dto';
import {DeepPartial, getConnection, LessThan, MoreThan, Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {DocumentsService} from '../../documentmanager/service/documents.service';
import {PreguntasService} from '../../preguntas/service/preguntas.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import {ReadStatusComunicadoEntity} from '../entity/ReadStatusComunicado.entity';
import {v4 as uuidv4} from 'uuid';
import {SearchDTO} from '../DTO/search.dto';
import {PreguntaEntity} from '../../preguntas/entity/preguntas.entity';
import {MailerService} from '@nestjs-modules/mailer';
import {ParametrosService} from '../../parametros/services/parametros.service';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';

@Injectable()
export class ComunicadosService {

    constructor(
        @InjectRepository(ComunicadoEntity) private readonly comunicadosEntity: Repository<ComunicadoEntity>,
        @InjectRepository(PreguntaEntity) private readonly preguntasEntity: Repository<PreguntaEntity>,
        @InjectRepository(ReadStatusComunicadoEntity) private readonly readStatusComunicadoEntity: Repository<ReadStatusComunicadoEntity>,
        private readonly fileService: DocumentsService,
        private preguntasService: PreguntasService,
        private readonly mailerService: MailerService,
        private readonly parametroService: ParametrosService,
        private readonly authService: AuthService
    ) {
    }

    async createComunicado(comunicado: ComunicadosDTO): Promise<ComunicadoEntity> {
        comunicado.status = true;
        return await this.comunicadosEntity.save(comunicado);
    }

    async publishComunicado(idComunicado: string): Promise<ComunicadoEntity> {
        const comunicado = await this.comunicadosEntity.findOne(idComunicado);
        comunicado.status = true;
        return await comunicado.save();
    }

    async getComunicados() {
        let now = moment()
        var utcOffset = moment().utcOffset();
        now.add(utcOffset, 'minutes').toISOString();
        return await this.comunicadosEntity.find({where: {status: true, fechaCreacion: LessThan(now)}});
    }

    async getAllComunicados() {
        return await this.comunicadosEntity.find({
            select: ["idComunicado", "titulo"],
            where: {status: true}});
    }

    async getReadStatus(userId: string) {
        let data = await this.readStatusComunicadoEntity.find({select: ['comunicadoId'], where: {userId: userId}});
        let response = [];
        if (data.length > 0) {
            _.forEach(data, id => {
                response.push(id.comunicadoId);
            })
        }
        return response;
    }

    async getCountUnreaded(userId: string) {
        let now = moment()
        var utcOffset = moment().utcOffset();
        now.add(utcOffset, 'minutes').toISOString();
        let comunicados = await this.comunicadosEntity.find({
            select: ['idComunicado'],
            where: {status: true, fechaCreacion: LessThan(now)},
            order: {fechaCreacion: 'DESC'}
        });
        let comunicadosReaded = await this.getReadStatus(userId);
        let unreaded = 0;
        _.forEach(comunicados, (comunicado: any) => {
            if (!comunicadosReaded.includes(comunicado.idComunicado)) {
                ++unreaded;
            }
        });
        return {count: unreaded};
    }

    async getComunicadoByMonth(req, userId, page = 1, limit = 10) {

        let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ComunicadosOficialesComunicadosOficialesVisualizarunComunicadoyunaPregunta);
    
        // En caso de que el usuario no sea correcto
        if (!usuarioValidado.ResultadoOperacion) {
          return usuarioValidado;
        }

        let loadMoreComunicados = false;
        let now = moment()
        var utcOffset = moment().utcOffset();
        now.add(utcOffset, 'minutes').toISOString();
        limit = page * limit;

        let countComunicados = await this.comunicadosEntity.count({
                status: true,
                fechaCreacion: LessThan(now)
            }
        );

        let comunicados = await this.comunicadosEntity.find({
            where: {status: true, fechaCreacion: LessThan(now)},
            order: {fechaCreacion: 'DESC'},
            take: limit
        });

        if (comunicados.length < countComunicados) {
            loadMoreComunicados = true;
        }
        let comunicadosReaded = await this.getReadStatus(userId);

        let response = {}
        const monthDictionary =
            {
                0: 'Enero',
                1: 'Febrero',
                2: 'Marzo',
                3: 'Abril',
                4: 'Mayo',
                5: 'Junio',
                6: 'Julio',
                7: 'Agosto',
                8: 'Septiembre',
                9: 'Octubre',
                10: 'Noviembre',
                11: 'Diciembre'
            };
        _.forEach(comunicados, (comunicado: any) => {
            comunicado.dateString = (new Date(comunicado.fechaCreacion)).toLocaleDateString('es-CL');
            comunicado.readed = comunicadosReaded.includes(comunicado.idComunicado);
            const [month, year] = [moment(comunicado.fechaCreacion).month(), moment(comunicado.fechaCreacion).year()];
            comunicado.month = month;
            comunicado.year = year;
        })
        comunicados = _.orderBy(comunicados, ['year', 'month'], ['desc', 'desc'])
        _.forEach(comunicados, (comunicado) => {
            const [month, year] = [moment(comunicado.fechaCreacion).month(), moment(comunicado.fechaCreacion).year()];
            if (!response[year.toString() + ' ' + monthDictionary[month]]) {
                response[year.toString() + ' ' + monthDictionary[month]] = [];
            }
            response[year.toString() + ' ' + monthDictionary[month]].push(comunicado);
        })
        return {comunicados: response, loadMoreComunicados: loadMoreComunicados};
    }

    reverseObject(object) {
        let newObject = {};
        let keys = [];

        for (let key in object) {
            keys.push(key);
        }

        for (let i = keys.length - 1; i >= 0; i--) {
            let value = object[keys[i]];
            newObject[keys[i]] = value;
        }

        return newObject;
    }

    async readComunicado(idComunicado: string, userId: string) {
        const readEntry = {
            readStatusId: uuidv4(),
            comunicadoId: idComunicado,
            userId: userId
        }
        let data: any = await this.readStatusComunicadoEntity.find({
            where: {
                comunicadoId: idComunicado,
                userId: userId
            }
        });
        if (data.length == 0) {
            data = await this.readStatusComunicadoEntity.save(readEntry);
        }

        return data;
    }

    async getComunicado(id: string, filesIncluded = true) {
        const comunicado = await this.comunicadosEntity.findOne(id);
        let filesFilter = {
            limit: 100,
            where: {
                entityId: id,
                moduleName: 'comunicaciones'
            }
        }
        let files;
        if (filesIncluded) {
            files = await this.fileService.getDocuments(filesFilter);
        }
        const preguntas = await this.preguntasService.getPreguntas(id);
        if (!comunicado) throw new NotFoundException('Comunicado dostn exists');
        let response = {
            ...comunicado,
            files: files,
            preguntas: preguntas
        }
        return response;
    }


    async search(req: any, searchParam: SearchDTO) {



        let skip = searchParam.page == 1 ? 0 : searchParam.limit * (searchParam.page - 1)

        let comunicados: any = await this.comunicadosEntity.createQueryBuilder('Comunicados')
            .select('Comunicados.idComunicado')
            .orWhere('LOWER(bajada) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .orWhere('LOWER(titulo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .orWhere('LOWER(cuerpo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .andWhere('status = :status', {status: true})
            .orderBy('created_at', 'DESC')
            .limit(searchParam.limit)
            .offset(skip)
            .skip(skip)
            .getMany()

        let preguntas: any = await this.preguntasEntity.createQueryBuilder('Preguntas')
            .select('Preguntas.idComunicado')
            .orWhere('LOWER(titulo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .orWhere('LOWER(cuerpo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .andWhere('status = :status', {status: true})
            .orderBy('created_at', 'DESC')
            .limit(searchParam.limit)
            .skip(skip)
            .offset(skip)
            .getMany()
        let comunicadosToSearch = [];

        _.forEach(preguntas, (pregunta) => {
            comunicadosToSearch.push(pregunta.idComunicado);
        });
        _.forEach(comunicados, (comunicado) => {
            comunicadosToSearch.push(comunicado.idComunicado);
        });
        comunicadosToSearch = [...new Set(comunicadosToSearch)];
        comunicadosToSearch = comunicadosToSearch.slice(0, searchParam.limit)
        let promisesArray = [];
        _.forEach(comunicadosToSearch, (comunicadoId) => {
            promisesArray.push(this.getComunicado(comunicadoId, false))
        })
        let comunicadosResults = await Promise.all(promisesArray);
        let total = await this.countResults(searchParam);
        return {comunicados: comunicadosResults, count: total};
    }

    async countResults(searchParam: SearchDTO) {
        let comunicados: any = await this.comunicadosEntity.createQueryBuilder('Comunicados')
            .select('Comunicados.idComunicado')
            .orWhere('LOWER(bajada) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .orWhere('LOWER(titulo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .orWhere('LOWER(cuerpo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .andWhere('status = :status', {status: true})
            .orderBy('created_at', 'DESC')
            .getMany()

        let preguntas: any = await this.preguntasEntity.createQueryBuilder('Preguntas')
            .select('Preguntas.idComunicado')
            .orWhere('LOWER(titulo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .orWhere('LOWER(cuerpo) ILIKE :searchQuery', {searchQuery: `%${searchParam.text.toLowerCase()}%`})
            .andWhere('status = :status', {status: true})
            .orderBy('created_at', 'DESC')
            .getMany()
        let comunicadosToSearch = [];

        _.forEach(preguntas, (pregunta) => {
            comunicadosToSearch.push(pregunta.idComunicado);
        });
        _.forEach(comunicados, (comunicado) => {
            comunicadosToSearch.push(comunicado.idComunicado);
        });
        comunicadosToSearch = [...new Set(comunicadosToSearch)];
        return comunicadosToSearch.length;
    }

    async getInfoUsuarioConMunicipalidadPorRun(run: string) {
        run = run.toString();
        let infoUsuario: any = await getConnection()
            .createQueryBuilder()
            .select('US.Nombres', 'nombres')
            .addSelect('US.ApellidoPaterno', 'ApellidoPaterno')
            .addSelect('US.ApellidoMaterno', 'ApellidoMaterno')
            /*.addSelect('OF.Nombre', 'oficina')*/
            /*.addSelect('INS.Nombre', 'municipalidad')*/
            .from('Usuarios', 'US')
           /* .leftJoin('UsuarioOficina', 'UO', 'UO.idUsuario = US.idUsuario')
            .leftJoin('Oficinas', 'OF', 'OF.idOficina = UO.idOficina')
            .leftJoin('Instituciones', 'INS', 'INS.idInstitucion = OF.idInstitucion')*/
            .where('US.RUN = :run', {run: run})
            .execute();
        infoUsuario = infoUsuario && infoUsuario[0] ? infoUsuario [0] : null;
        return infoUsuario
    }

    formatHtml(dataMail, infoUsuario) {
        let html = 'Ha recibido un mensaje a través del formulario de contacto de Comunicaciones Oficiales '
        html += `<hr>`;
        html += `<b>Nombre</b> : ${infoUsuario.nombres} ${infoUsuario.ApellidoPaterno} ${infoUsuario.ApellidoMaterno} <br>`;
        html += `<b>Comunicado</b> : ${dataMail.comunicado} <br>`;
       /* html += `<b>Municipalidad</b> : ${infoUsuario.municipalidad} <br>`;
        html += `<b>Oficina</b> : ${infoUsuario.oficina} <br>`;*/
        html += `<hr>`;
        html += `<b>Mensaje :</b>`;
        html += `${dataMail.html}`;
        return html;
    }

    formatHtmlUsuario(dataMail, infoUsuario) {
        let html = `<p>Estimad@ ${infoUsuario.nombres} ${infoUsuario.ApellidoPaterno} :  </p>`;
        html += `<p>Junto con saludar, comunicamos que hemos recibido su consulta sobre ${dataMail.comunicado}, la cual está siendo procesada por nuestras expertas y expertos.</p>`;
        html += `<p>Nos pondremos en contacto con usted a la brevedad</p>`;
        html += `<p>Agradeciendo su paciencia.</p>`;
        return html;
    }

    async sendEmailUsuario (req, dataMail, run) {
        let infoUsuario = await this.getInfoUsuarioConMunicipalidadPorRun(run);
        let htmlBody = this.formatHtmlUsuario(dataMail, infoUsuario);
        let headerHtml: any = await this.parametroService.getParametro(35);
        let footerHtml: any = await this.parametroService.getParametro(36);
        let html = headerHtml.Value + htmlBody +footerHtml.Value;

        try {

            let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ComunicadosOficialesComunicateconNosotrosEnviarPregunta);
    
            // En caso de que el usuario no sea correcto
            if (!usuarioValidado.ResultadoOperacion) {
              return usuarioValidado;
            }	

            let mailOptions: any = {
                to: dataMail.replyTo,
                from: `SGLC <sglc@sglc-mail.ingeniaglobal.cl>`, // sender address
                subject : '[No responder] - Comunícate con nosotros',
                html: html
            };
            let response = await this.mailerService.sendMail(mailOptions);
            return response;
        } catch (e) {
            return e;
        }
    }
    async sendMail(req, dataMail, run) {
        let infoUsuario = await this.getInfoUsuarioConMunicipalidadPorRun(run);
        let htmlBody = this.formatHtml(dataMail, infoUsuario);
        let toMailObj: any = await this.parametroService.getParametro(26);
        let headerHtml: any = await this.parametroService.getParametro(35);
        let footerHtml: any = await this.parametroService.getParametro(36);
        let html = headerHtml.Value + htmlBody +footerHtml.Value;
        let toMail = toMailObj.Value;

        try {

            let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ComunicadosOficialesComunicateconNosotrosEnviarPregunta);
    
            // En caso de que el usuario no sea correcto
            if (!usuarioValidado.ResultadoOperacion) {
              return usuarioValidado;
            }	

            let mailOptions: any = {
                to: toMail,
                from: `${dataMail.fromName} <sglc@sglc-mail.ingeniaglobal.cl>`, // sender address
                subject: dataMail.subject,
                html: html
            };
            if (dataMail.replyTo) {
                mailOptions.replyTo = dataMail.replyTo;
            }
            if (dataMail.files) {
                mailOptions.attachments = [];
                _.forEach(dataMail.files, file => {
                    mailOptions.attachments.push({filename: file.filename, path: file.path})
                })
            }
            let response = await this.mailerService.sendMail(mailOptions);
            return response;
        } catch (e) {
            return e;
        }
    }

    async delete(id: string) {
        const comunicado = await this.comunicadosEntity.findOne(id);

        try {
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(ComunicadoEntity)
                .where('idComunicado = :id', {id: id})
                .execute();
            return {'code': 200, 'message': 'success'};
        } catch (error) {
            return {'code': 400, 'error': error};
        }
    }

}




