
export class Bloqueo {
    fechaHora: Date;
    tipo: string;
    codigoCta: string;
    causa: string;
    numeroBloqueo: string;
    origen: string;
    fechaHoraDesbloqueo: Date;
    letraF: string;
    rutEsc: string;
}


