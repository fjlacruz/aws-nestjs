import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConductorMobileDTO {

    @ApiPropertyOptional()
    RUN: number;
    @ApiPropertyOptional()
    DV: string;
    @ApiPropertyOptional()
    Nombres: string;
    @ApiPropertyOptional()
    ApellidoPaterno: string;
    @ApiPropertyOptional()
    ApellidoMaterno: string;
    @ApiPropertyOptional()
    Telefono: string;
    @ApiPropertyOptional()
    Email: string;
    @ApiPropertyOptional()
    Comuna: string;
    @ApiPropertyOptional()
    Calle: string;
    @ApiPropertyOptional()
    CalleNro: number;
    @ApiPropertyOptional()
    Letra: string;
    @ApiPropertyOptional()
    RestroDireccion: string;
    @ApiPropertyOptional()
    Sexo: string;
    @ApiPropertyOptional()
    EstadoCivil: string;
    @ApiPropertyOptional()
    Nacionalidad: string;
    @ApiPropertyOptional()
    NivelEducacional: string;
    @ApiPropertyOptional()
    Profesion: string;
    @ApiPropertyOptional()
    Diplomatico: string;
    @ApiPropertyOptional()
    Discapacidad: string;
    @ApiPropertyOptional()
    DetalleDiscapacidad: string;
    @ApiPropertyOptional()
    FechaNacimiento: Date;
    @ApiPropertyOptional()
    FechaDefuncion: Date;
    @ApiPropertyOptional()
    idSGLConductor: number;
    @ApiPropertyOptional()
    municipalidad: string;
    @ApiPropertyOptional()
    fotoUsuarioPath: string;
    @ApiPropertyOptional()
    fotoUsuarioBinary: Buffer;
    @ApiPropertyOptional()
    IdComunaSGL: number;
}


