import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConductorDTO {

    @ApiPropertyOptional()
    idConductor: number;
    @ApiPropertyOptional()
    idPostulante:number;
    @ApiPropertyOptional()
    RUN:number;
    @ApiPropertyOptional()
    DV:string;
    @ApiPropertyOptional()
    created_at:Date;
    @ApiPropertyOptional()
    update_at:Date;
    @ApiPropertyOptional()
    idUsuario:number;
}


