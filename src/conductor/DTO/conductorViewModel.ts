import { Anotacion } from "./anotacion.dto";
import { Duplicado } from "./duplicado.dto";
import { Bloqueo } from "./bloqueo.dto";
import { Licencia } from "./licencia.dto";
import { TipoLicenciaRegistradoLIC } from "./tipoLicenciaRegistradoLIC.dto";

export class ConductorViewModel {

    run: string;
    nombreConductor: string;
    tipoLicenciaRegistradoLic: TipoLicenciaRegistradoLIC;
    duplicadosInformados: Duplicado;
    anotacionesRegistradas: Anotacion;
    denegaciones: Anotacion;
    bloqueos: Bloqueo;
    tipoLicenciaRegistradoHis: Licencia;
}


