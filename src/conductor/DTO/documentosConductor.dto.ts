
export class DocumentosConductorDTO {

    run: string;
    nombreConductor: string; // Nombre + Apellidos
    licenciaAbreviatura: string;
    nombreDocumento:string;
    documentoId: number;
}


