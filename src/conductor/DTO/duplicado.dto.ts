
export class Duplicado {
    secuencia: string;
    fecha: Date;
    vigencia: Date;
    estado: string;
    tipo: string;
    letra: string;
    folio: string;
    fechaIngreso: Date;
    observaciones: string;
    recepcion: string;
    codOrigen: string;
    codColumnan: string;
    proxControl: Date;
    edFisico: string;
    edDigital: string;
    eClase: string;
    rutEsc: string;
}


