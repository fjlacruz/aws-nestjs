
export class Licencia {
    clase: string;
    fecha: Date;
    fProxControl: Date;
    edFisico: string;
    edDigital: string;
    eClase: string;
    fPrimeraLic: string;
    letraFolioLic: string;
    runRutEscConductores: string;
    opciones1: string;
    opciones2: string;
}


