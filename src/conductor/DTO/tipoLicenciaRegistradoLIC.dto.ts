import { Licencia } from "./licencia.dto";

export class TipoLicenciaRegistradoLIC {
    primeraLicencia: Licencia;
    ultimaLicencia: Licencia;
    inicioTramiteLicencia: Licencia;
}


