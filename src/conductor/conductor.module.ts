import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { DocumentosLicenciaClaseLicenciaEntity } from 'src/documento-licencia/entity/documento-licencia.entity';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { User } from 'src/users/entity/user.entity';
import { ConductorController } from './controller/conductor.controller';
import { ConductoresEntity } from './entity/conductor.entity';
import { ConductorService } from './services/conductor.service';


@Module({
  imports: 
  [
    TypeOrmModule.forFeature([ConductoresEntity,
    TramitesEntity,
    TramitesClaseLicencia,
    DocumentosLicenciaClaseLicenciaEntity,
    DocsRolesUsuarioEntity,
    User,
    RolesUsuarios]),
  ],
  providers: [ConductorService, AuthService],
  exports: [ConductorService],
  controllers: [ConductorController]
})
export class ConductorModule {}
