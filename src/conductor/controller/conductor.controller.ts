import { Controller, Req } from '@nestjs/common';
import { Get, Param, Post, Body } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { ConductorDTO } from '../DTO/conductor.dto';
import { ConductorService } from '../services/conductor.service';
import { PostulanteEntity } from '../../postulante/postulante.entity';
import { ConductoresEntity } from '../entity/conductor.entity';


@ApiTags('Conductor')
@Controller('conductor')
export class ConductorController {

    constructor(private readonly conductorService: ConductorService,
        private readonly authService: AuthService) { }

    @Get('Conductor')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los conductores' })
    async getConductor() {
        const data = await this.conductorService.getConductores();
        return { data };
    }


    @Get('ConductorById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un conductor por Id' })
    async ConductorById(@Param('id') id: number) {
        const data = await this.conductorService.getConductor(id);
        return { data };
    }


    @Post('uploadConductor')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Conductor' })
    async uploadConductor(
        @Body() ConductorDTO: ConductorDTO) {

        const data = await this.conductorService.createConductor(ConductorDTO);
        return {data};

    }

    @Post('CarpetaConductor')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los tramites con documentos del conductor por RUN del conductor' })
    async CarpetaConductor(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {

        const data = await this.conductorService.getCarpetaConductor(req, paginacionArgs);
        return { data };
        
    }

    @Post('DatosHistoricosRNCVM')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los datos históricos del RNCVM' })
    async datosHistoricosRNCVM(@Body() run: any, @Req() req: any) {

        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.AccesoConsultaHistoricaAlRNCVM
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.conductorService.datosHistoricosRNCVM(run);
            return { data };
        
        } else {

            return { data: validarPermisos };
        }
    }

  @Get('/run-postulante/:run')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'The found Postulante by Run',
  })
  async getConductorByRun(@Param('run') run: number): Promise<any> {
    return await this.conductorService.findOne({ where: { RUN: run } });
  }

  @Get('/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un conductor por Id' })
  async getConductorById(@Param('id') id: number) {
    return await this.conductorService.findOne({ where: { idConductor: id } });
  }
}
