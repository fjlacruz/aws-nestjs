import { ApiProperty } from '@nestjs/swagger';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { Entity, Column, PrimaryColumn, OneToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';
import { ResolucionesJudicialesEntity } from '../../resoluciones-judiciales/entity/resoluciones-judiciales.entity';

@Entity('Conductores')
export class ConductoresEntity {
  @PrimaryGeneratedColumn()
  idConductor: number;

  @Column()
  idPostulante: number;

  @OneToOne(() => PostulantesEntity, postulante => postulante.conductor)
  @JoinColumn({ name: 'idPostulante' })
  readonly postulante: PostulantesEntity;

  // @Column()
  // RUN: number;

  @Column()
  created_at: Date;

  @Column()
  update_at: Date;

  @Column()
  ingresadoPor: number;

  @ApiProperty()
  @OneToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.conductor)
  readonly documentosLicencia: DocumentosLicencia;

  @ApiProperty()
  @OneToOne(() => ResolucionesJudicialesEntity, resolucion => resolucion.conductor)
  readonly resolucion: ResolucionesJudicialesEntity;
}
