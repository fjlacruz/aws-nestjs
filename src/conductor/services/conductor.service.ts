import { Injectable , NotFoundException} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { Resultado } from 'src/utils/resultado';
import { Repository, getConnection, getRepository, Brackets, FindManyOptions } from 'typeorm';
import { ConductorDTO } from '../DTO/conductor.dto';
import { FiltroConductorDTO } from '../DTO/filtroConductor.dto';
import { ConductoresEntity } from '../entity/conductor.entity';
import { PermisosNombres, relacionLicenciaRegistradaLIC, relacionTramiteConExamenes, relacionTramiteDocumentos } from 'src/constantes';
import { DocumentosConductorDTO } from '../DTO/documentosConductor.dto';
import { DocumentosLicenciaClaseLicenciaEntity } from 'src/documento-licencia/entity/documento-licencia.entity';
import { TipoLicenciaRegistradoLIC } from '../DTO/tipoLicenciaRegistradoLIC.dto';
import { Licencia } from '../DTO/licencia.dto';
import { ConductorViewModel } from '../DTO/conductorViewModel';
import { validarRut } from 'src/shared/ValidarRun';
import { AuthService } from 'src/auth/auth.service';


@Injectable()
export class ConductorService {

    constructor(@InjectRepository(ConductoresEntity) private readonly conductorService: Repository<ConductoresEntity>,
                @InjectRepository(TramitesClaseLicencia) private readonly tramiteLicenciaRespository: Repository<TramitesClaseLicencia>,
                @InjectRepository(DocumentosLicenciaClaseLicenciaEntity) private readonly documentoLicenciaClaseLicenciaRespository: Repository<DocumentosLicenciaClaseLicenciaEntity>,
                private readonly authService: AuthService
                ) { }

    async getConductores() {
        return await this.conductorService.find();
    }

    async getConductor(id:number){
        const conductor= await this.conductorService.findOne(id);
        if (!conductor)throw new NotFoundException("conductor dont exist");
        
        return conductor;
    }

    async createConductor(conductorDTO: ConductorDTO): Promise<ConductoresEntity> {
        return await this.conductorService.save(conductorDTO);
    }




    async getCarpetaConductor(req, paginacionArgs: PaginacionArgs): Promise<Resultado>{

        const resultado: Resultado = new Resultado();
        let solicitudesParseadas = new PaginacionDtoParser<DocumentosConductorDTO>();
        const filtro: FiltroConductorDTO = paginacionArgs.filtro;

        let tramitesClaseLicenciaPag: Pagination<TramitesClaseLicencia>;

        try {


            // Comprobamos permisos
            let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AccesoConsultaCarpetaConductor);
            
            // En caso de que el usuario no sea correcto
            if (!usuarioValidado.ResultadoOperacion) {
                return usuarioValidado;
            }



            if(!validarRut(filtro.run)){
                resultado.Error = "El formato del rut es incorrecto"
                return resultado;
            }

            tramitesClaseLicenciaPag = await paginate<TramitesClaseLicencia>(this.tramiteLicenciaRespository, paginacionArgs.paginationOptions, {
                relations: relacionTramiteDocumentos,
                where: qb => {
                    qb.where(
                        'TramitesClaseLicencia__tramite__solicitudes__postulante.RUN || \'-\' || TramitesClaseLicencia__tramite__solicitudes__postulante.DV = :RUN', { RUN: filtro.run }
                    ).andWhere(
                        'TramitesClaseLicencia__tramite__documentosTramites.idDocumentosTramite > 0'
                    ).andWhere( new Brackets( subQb => {

                        subQb.where(filtro.texto != null && filtro.texto != '' ?
                            'lower(unaccent(TramitesClaseLicencia__tramite__documentosTramites.NombreDocumento)) like lower(unaccent(:nombre))': 'TRUE', { nombre: `%${filtro.texto}%` }

                        ).orWhere(filtro.texto != null && filtro.texto != '' ?
                            'TramitesClaseLicencia__clasesLicencias.Abreviacion like :claseLicencia': 'TRUE', { claseLicencia: filtro.texto }
                        )}
                    ))
                },
                order: {
                    idClaseLicencia: "DESC",
                    idTramite: "DESC"
                }
            });

            let carpetaConductorDTO : DocumentosConductorDTO[] = [];

            if (Array.isArray(tramitesClaseLicenciaPag.items) && tramitesClaseLicenciaPag.items.length) {

                tramitesClaseLicenciaPag.items.forEach((item, index) => {
                    let documentoConductorDTO : DocumentosConductorDTO = new DocumentosConductorDTO();

                    documentoConductorDTO.run = item.tramite.solicitudes.postulante.RUN + "-" +
                                                item.tramite.solicitudes.postulante.DV;
                    documentoConductorDTO.nombreConductor = item.tramite.solicitudes.postulante.Nombres + " " +
                                                            item.tramite.solicitudes.postulante.ApellidoPaterno + " " +
                                                            item.tramite.solicitudes.postulante.ApellidoMaterno;
                    documentoConductorDTO.documentoId = item.tramite.documentosTramites.idDocumentosTramite;
                    documentoConductorDTO.nombreDocumento = item.tramite.documentosTramites.NombreDocumento;
                    documentoConductorDTO.licenciaAbreviatura = item.clasesLicencias.Abreviacion;

                    if (carpetaConductorDTO.length > 0) {
                        if (carpetaConductorDTO[index - 1].licenciaAbreviatura != item.clasesLicencias.Abreviacion) {
                            let elementoFinalGrupo : DocumentosConductorDTO = new DocumentosConductorDTO();
                            elementoFinalGrupo.licenciaAbreviatura = tramitesClaseLicenciaPag.items[index - 1].clasesLicencias.Abreviacion;
                            elementoFinalGrupo.nombreDocumento = 'Todos los Documentos (Clase ' + tramitesClaseLicenciaPag.items[index - 1].clasesLicencias.Abreviacion + ')';
                            elementoFinalGrupo.documentoId = 0;

                            carpetaConductorDTO.push(elementoFinalGrupo);
                        }
                    }
                    carpetaConductorDTO.push(documentoConductorDTO);
                });

                // AGREGAMOS EL ULTIMO ELEMENTO DE FIN DE GRUPO DE LICENCIAS
                let elementoFinalGrupo : DocumentosConductorDTO = new DocumentosConductorDTO();
                elementoFinalGrupo.licenciaAbreviatura = tramitesClaseLicenciaPag.items[tramitesClaseLicenciaPag.items.length -1].clasesLicencias.Abreviacion;
                elementoFinalGrupo.nombreDocumento = 'Todos los Documentos (Clase ' + tramitesClaseLicenciaPag.items[tramitesClaseLicenciaPag.items.length -1].clasesLicencias.Abreviacion + ')';
                carpetaConductorDTO.push(elementoFinalGrupo);
    
                solicitudesParseadas.items = carpetaConductorDTO;
                solicitudesParseadas.meta = tramitesClaseLicenciaPag.meta;
                solicitudesParseadas.links = tramitesClaseLicenciaPag.links;
    
                resultado.Respuesta = solicitudesParseadas;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Carpeta del conductor obtenida correctamente';
                
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron los tramites';
            }

        } catch (error) {

            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo tramites';
        }

        return resultado;
    }

    async datosHistoricosRNCVM(filtro: any): Promise<Resultado>{
        let run :string = filtro.run.split('.').join("");
        console.log(run);
        const resultado: Resultado = new Resultado();
        let documentoLicenciaClaseLicencia: DocumentosLicenciaClaseLicenciaEntity[];

        try {

            if(!validarRut(filtro.run)){
                resultado.Error = "El formato del rut es incorrecto"
                return resultado;
            }

            documentoLicenciaClaseLicencia = await this.documentoLicenciaClaseLicenciaRespository.find( {
                relations: relacionLicenciaRegistradaLIC,
                join: { alias: 'docLicenciaClaseLicencia', innerJoinAndSelect: {
                    documentosLicencia: 'docLicenciaClaseLicencia.documentosLicencia', estadosDF: 'documentosLicencia.estadosDF', estadosDD: 'documentosLicencia.estadosDD', estadosPCL: 'documentosLicencia.estadosPCL', papelSeguridad: 'documentosLicencia.papelSeguridad',
                    conductor: 'documentosLicencia.conductor', postulante: 'conductor.postulante', solicitudes: 'postulante.solicitudes', tramites: 'solicitudes.tramites',
                    clasesLicencias: 'docLicenciaClaseLicencia.clasesLicencias', 
                  }
                },
                where: qb => {
                    qb.where(
                        'postulante.RUN || \'-\' || postulante.DV = :RUN', { RUN: run }
                    // ).andWhere(
                    //     'TramitesClaseLicencia__tramite__documentosTramites.idDocumentosTramite > 0'
                    )
                    .orderBy('documentosLicencia.created_at', 'ASC')
                }
            });

            if (documentoLicenciaClaseLicencia.length > 0) {

                let informacionCondutor: ConductorViewModel = new ConductorViewModel();
                informacionCondutor.run= run;
                informacionCondutor.nombreConductor =   documentoLicenciaClaseLicencia[0].documentosLicencia.conductor.postulante.Nombres + " " +
                                                        documentoLicenciaClaseLicencia[0].documentosLicencia.conductor.postulante.ApellidoPaterno + " " +
                                                        documentoLicenciaClaseLicencia[0].documentosLicencia.conductor.postulante.ApellidoMaterno;

                let tipoLicenciaRegistradoLIC : TipoLicenciaRegistradoLIC = new TipoLicenciaRegistradoLIC();

                documentoLicenciaClaseLicencia.forEach((documento,index) => {

                    if (index == 0) {
                        let licencia : Licencia = new Licencia();

                        licencia.clase= documento.clasesLicencias.Nombre;
                        licencia.fecha= documento.documentosLicencia.created_at;
                        licencia.fProxControl= documento.caducidadOtorgamiento;
                        licencia.edFisico= documento.documentosLicencia.estadosDF.Nombre;
                        licencia.edDigital= documento.documentosLicencia.estadosDD.Nombre;
                        licencia.eClase= documento.documentosLicencia.estadosPCL.Nombre;
                        licencia.fPrimeraLic= documento.documentosLicencia.papelSeguridad.Folio;
                        //licencia.letraFolioLic= documento.documentosLicencia.papelSeguridad.LetrasFolio; // Falta en la BBDD
                        licencia.runRutEscConductores= run;
                        licencia.opciones1= documento.documentosLicencia.conductor.postulante.solicitudes[0].tramites[0].observacion; // Observacion del Tramite Primero?
                        licencia.opciones2= documento.documentosLicencia.conductor.postulante.solicitudes[1].tramites[1].observacion; // Observacion del Tramite Segundo?

                        tipoLicenciaRegistradoLIC.primeraLicencia = licencia;
                    }

                    if (documentoLicenciaClaseLicencia.length -1 == index) {
                        let licencia : Licencia = new Licencia();

                        licencia.clase= documento.clasesLicencias.Nombre;
                        licencia.fecha= documento.documentosLicencia.created_at;
                        licencia.fProxControl= documento.caducidadOtorgamiento;
                        licencia.edFisico= documento.documentosLicencia.estadosDF.Nombre;
                        licencia.edDigital= documento.documentosLicencia.estadosDD.Nombre;
                        licencia.eClase= documento.documentosLicencia.estadosPCL.Nombre;
                        licencia.fPrimeraLic= documento.documentosLicencia.papelSeguridad.Folio;
                        //licencia.letraFolioLic= documento.documentosLicencia.papelSeguridad.LetrasFolio; // Falta en la BBDD
                        licencia.runRutEscConductores= run;
                        licencia.opciones1= documento.documentosLicencia.conductor.postulante.solicitudes[0].tramites[0].observacion; // Observacion del Tramite?
                        licencia.opciones2= documento.documentosLicencia.conductor.postulante.solicitudes[1].tramites[1].observacion; // Observacion del Tramite?

                        tipoLicenciaRegistradoLIC.ultimaLicencia = licencia;
                    }

                });

                informacionCondutor.tipoLicenciaRegistradoLic = tipoLicenciaRegistradoLIC;
    
                resultado.Respuesta = informacionCondutor;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Tipos de Licencia Registrados en LIC obtenidos correctamente';
                
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron los tramites';
            }

        } catch (error) {

            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo tramites';
        }

        return resultado;
    }

  async findOne(options: FindManyOptions<ConductoresEntity>): Promise<ConductoresEntity> {
    return await this.conductorService.findOne(options);
  }
}
