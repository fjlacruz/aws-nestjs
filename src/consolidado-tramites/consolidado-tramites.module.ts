import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { ConsolidadoTramitesController } from './controller/consolidado-tramites.controller';
import { ConsolidadoTramites } from './entity/consolidad-tramite.entity';
import { ConsolidadoTramitesService } from './services/consolidado-tramites.service';


@Module({
  imports: [TypeOrmModule.forFeature([ConsolidadoTramites, User, RolesUsuarios])],
  controllers: [ConsolidadoTramitesController],
  exports: [ConsolidadoTramitesService],
  providers: [ConsolidadoTramitesService, AuthService]
})
export class ConsolidadoTramitesModule {}
