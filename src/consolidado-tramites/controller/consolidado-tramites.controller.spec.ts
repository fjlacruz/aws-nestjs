import { Test, TestingModule } from '@nestjs/testing';
import { ConsolidadoTramitesController } from './consolidado-tramites.controller';

describe('ConsolidadoTramitesController', () => {
  let controller: ConsolidadoTramitesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConsolidadoTramitesController],
    }).compile();

    controller = module.get<ConsolidadoTramitesController>(ConsolidadoTramitesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
