import { Controller, Get } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ConsolidadoTramitesService } from '../services/consolidado-tramites.service';

@ApiTags('Consolidado-Tramites')
@Controller('consolidado-tramites')
export class ConsolidadoTramitesController {

    constructor(private readonly consolidadoService: ConsolidadoTramitesService) { }

    @Get('allConsolidados')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Tramites Consoliados' })
    async getConsolidados() {
        const data = await this.consolidadoService.getConsolidados();
        return { data };
    }

    @Get('allConsolidadosWithSolicitud')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los Tramites Consolidados con detalle de  Solicitud' })
    async getConsolidadosWithSolicitu() {
        
        const data =  await this.consolidadoService.fetch();

        return { data };
    }


}
