import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('ConsolidadoTramites')
export class ConsolidadoTramites {

    @PrimaryGeneratedColumn()
    idConsolidadoExaminaciones: number;

    @Column()
    idSolicitud: number;

    @Column()
    idTramite: number;

    @Column()
    idExamenTeorico: number;

    @Column()
    idExamPractico: number;

    @Column()
    idExamIMPreEval:number;

    @Column()
    idExamenIdoneidadMoral:number;

    @Column()
    idExamMedico: number;
    
    @Column()
    idRegistroPago: number;
    
    @Column()
    created_at: Date;

}