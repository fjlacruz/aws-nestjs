import { Test, TestingModule } from '@nestjs/testing';
import { ConsolidadoTramitesService } from './consolidado-tramites.service';

describe('ConsolidadoTramitesService', () => {
  let service: ConsolidadoTramitesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConsolidadoTramitesService],
    }).compile();

    service = module.get<ConsolidadoTramitesService>(ConsolidadoTramitesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
