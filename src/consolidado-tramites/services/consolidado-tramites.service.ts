import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { Repository } from 'typeorm';
import { ConsolidadoTramites } from '../entity/consolidad-tramite.entity';

@Injectable()
export class ConsolidadoTramitesService {

    constructor
    (
        private authService: AuthService,

        @InjectRepository(ConsolidadoTramites) 
        private readonly consolidadoRespository: Repository<ConsolidadoTramites>
    ) { }

    async getConsolidados() {
        const idOficina = this.authService.oficina().idOficina;
        return await this.consolidadoRespository
                .createQueryBuilder( "cons" )
                .innerJoin(          Solicitudes, "sol", "sol.idSolicitud = cons.idSolicitud ")
                .where(              "sol.idOficina = :idOficina", { idOficina })
                .getMany();
    }

    public fetch(): Promise<any> {
        const idOficina = this.authService.oficina().idOficina;
        return this.consolidadoRespository.createQueryBuilder('ConsolidadoTramites')
          .innerJoinAndMapMany('ConsolidadoTramites.idSolicitud', Solicitudes, 'sol', 'ConsolidadoTramites.idSolicitud = sol.idSolicitud',)
          .innerJoinAndMapMany('sol.idPostulante', Postulantes, 'pos', 'sol.idPostulante = pos.idPostulante',)
          .innerJoinAndMapMany('ConsolidadoTramites.idTramite', TramitesEntity, 'tra', 'ConsolidadoTramites.idTramite = tra.idTramite',)
          .where( "sol.idOficina = :idOficina", { idOficina })
          // .innerJoinAndMapMany('ConsolidadoTramites.idExamenTeorico', ColaExamTeoricos, 'col1', 'ConsolidadoTramites.idExamenTeorico = col1.idExamenTeorico',)
          // .innerJoinAndMapMany('ConsolidadoTramites.idExamIMPreEval', ColaExamPreEvalIdoneidadMoral, 'col2', 'ConsolidadoTramites.idExamIMPreEval = col2.idExamIMPreEval',)
          // .innerJoinAndMapMany('ConsolidadoTramites.idExamenIdoneidadMoral', ColaExamIdoneidadMoral, 'col3', 'ConsolidadoTramites.idExamenIdoneidadMoral = col3.idExamenIdoneidadMoral',)
          // .innerJoinAndMapMany('ConsolidadoTramites.idExamMedico', ColaExamMedicoEntity, 'col4', 'ConsolidadoTramites.idExamMedico = col4.idExamMedico',)
          // .innerJoinAndMapMany('ConsolidadoTramites.idExamPractico', ColaExamPracticosEntity, 'col5', 'ConsolidadoTramites.idExamPractico = col5.idExamPractico',)

          .getMany();
      }
}
