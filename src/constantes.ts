// ESTADOS SOLICITUDES //
export const EstadosSolicitudesActivas = ['Hora Reservada', 'Hora reservada, FI y ACT enviados', 'Mesón', 'Borrador', 'Abierta'];
export const EstadosSolicitudesSINUSAR = ['Cerrada', 'Incumplimiento de requisitos'];
export const EstadosSolicitudesNoActivas = ['Recepción Conforme Recibida', 'Solicitud Desistida', 'Solicitud Abandonada'];
export const EstadoSolicitudAbierta = ['Abierta'];
export const EstadoSolicitudCerrada = ['Cerrada'];
export const CodigoSolicitudAbierta = '1205';
export const CodigoSolicitudCerrada = '1206';

// ESTADOS TRAMITES //
export const estadoTramitesIniciado = ['Trámite Iniciado'];
export const estadoTramitesEsperaDenegacionLicencia = ['A la Espera de Informar Denegación'];
export const estadoTramitesEsperaDenegacionLicenciaId = 2;
export const estadosTramitesOtorgamiento = ['A la Espera de Informar Otorgamiento'];
export const estadosTramitesOtorgamientoId = 3;
export const estadosTramitesOtorgado = ['Otorgamiento informado y en impresión'];
export const estadosTramitesOtorgadoEnImpresionId = 9;
export const estadosTramitesEsperaRegistro = ['Pendiente Registro Civil'];
export const estadoTramitesDesistido = ['Trámite Desistido'];
export const CodigoEstadoTramitesDesistido = '1105';
export const estadoTramitesAbandonado = ['Trámite Abandonado'];
export const CodigoEstadoTramitesAbandonado = '1106';
export const estadoTramitesDenegacion = ['Denegación Informada'];
export const estadoTramitesDenegacionSML = ['Denegación Informada SML'];
export const CodigoEstadoTramitesDenegacionSML = '1107';
export const estadoTramitesDenegacionJPL = ['Denegación Informada JPL'];
export const CodigoEstadoTramitesDenegacionJPL = '1108';
export const estadoTramitesEsperaRecepcionConforme = ['Acusar Recibo Conforme'];
export const estadoTramitesEsperaRecepcionConformeId = 5;
export const estadoTramitesRecepcionConforme = ['Recepción conforme Documento físico'];
export const CodigoEstadoTramitesRecepcionConformeFisico = '1302';
export const CodigoEstadoTramitesRecepcionConformeFisicoDigital = '1304';
export const estadoTramitesRecepcionNoConforme = ['Recepción no conforme'];
export const CodigoEstadoTramitesCanceladoCerrado = '1113';

export const estadosTramitesEnProceso = ['Trámite Iniciado', 'A la Espera de Informar Denegación', 'A la Espera de Informar Otorgamiento'];
export const estadosTramitesEspera = [
  'A la Espera de Informar Denegación',
  'A la Espera de Informar Otorgamiento',
  'Otorgamiento informado y en impresión',
  'Acusar Recibo Conforme',
];
export const estadosTramitesOtorgamientoDenegacion = ['A la Espera de Informar Denegación', 'A la Espera de Informar Otorgamiento'];

// TIPO DE DOCUMENTOS //
export const tipoDocumentoRecepcionConforme = 'Documento Recepción Conforme';
export const tipoDocumentoOtorgamiento = 'Documento Otorgamiento de Licencia';
export const tipoDocumentoDenegacion = 'Documento de Denegación de Licencia';

// TIPO DE Roles //
//export const tipoRolSuperUsuario = 'Superusuario';
export const tipoRolSuperUsuarioID = 1;
export const tipoRolRegion = 'Region';
export const tipoRolOficina = 'Oficina';


export const tipoRolRegionID = 2;
export const tipoRolOficinaID = 3;
export const tipoRolGeneralID = 4;

export const plazoDenegacionOtro = 'Otro';
export const plazoDenegacion = ['30 días', '6 meses', plazoDenegacionOtro];
export const motivoDenegacionJPL = ['NO REÚNE REQUISITO DE IDONEIDAD MORAL'];
export const motivoDenegacionSML = [
  'NO REÚNE REQUISITO DE IDONEIDAD FISICA',
  'NO REÚNE REQUISITO DE IDONEIDAD PSIQUICA',
  'REPROBAR EXAMEN MÉDICO',
  'NO PRESENTARSE A RENDIR EXAMEN MÉDICO',
];
export const motivoDenegacionOtros = [
  'REPROBAR EXAMEN TEÓRICO',
  'NO PRESENTARSE A RENDIR EXAMEN TEÓRICO',
  'COMPORTAMIENTO NO IDONEO',
  'REPROBAR EXAMEN PRÁCTICO',
  'NO PRESENTARSE A RENDIR EXAMEN PRÁCTICO',
  'Reservado SGL',
  'NO SABER LEER NI ESCRIBIR',
];
export const motivosDenegacion = motivoDenegacionJPL.concat(motivoDenegacionSML.concat(motivoDenegacionOtros));

export const relacionTramiteConExamenes = [
  'tramite',
  'clasesLicencias',
  'tramite.solicitudes',
  'tramite.solicitudes.postulante',
  'tramite.solicitudes.estadosSolicitud',
  'tramite.tiposTramite',
  'tramite.estadoTramite',
  'tramite.colaExaminacion',
  'tramite.colaExaminacionNM',
  'tramite.colaExaminacionNM.colaExaminacion',
  'tramite.colaExaminacionNM.colaExaminacion.tipoExaminaciones',
  'tramite.solicitudes.oficinas',
  'tramite.solicitudes.oficinas.instituciones',
];

export const relacionTramiteSinExamenes = [
  'tramite',
  'clasesLicencias',
  'tramite.solicitudes',
  'tramite.solicitudes.estadosSolicitud',
  'tramite.solicitudes.postulante',
  'tramite.tiposTramite',
  'tramite.estadoTramite',
  'tramite.motivoDenegacion',
];
export const relacionTramiteSinExamenesLicencia = [
  'tramite',
  'clasesLicencias',
  'clasesLicencias.documentoLicenciaClaseLicencia',
  'tramite.solicitudes',
  'tramite.solicitudes.estadosSolicitud',
  'tramite.solicitudes.postulante',
  'tramite.tiposTramite',
  'tramite.estadoTramite',
];

export const relacionTramiteConductor = [
  'tramite',
  'clasesLicencias',
  'tramite.solicitudes',
  'tramite.solicitudes.postulante',
  'tramite.tiposTramite',
  'tramite.estadoTramite',
  'tramite.colaExaminacionNM',
  'tramite.colaExaminacionNM.colaExaminacion',
  'tramite.solicitudes.postulante.conductor',
  'tramite.solicitudes.postulante.comunas',
  'tramite.solicitudes.postulante.comunas.instituciones',
  //'tramite.solicitudes.postulante.comunas.oficinas',
  //'tramite.solicitudes.postulante.comunas.oficinas.instituciones',
  'tramite.solicitudes.postulante.opcionSexo',
  'tramite.solicitudes.postulante.opcionesNivelEducacional',
  'tramite.solicitudes.postulante.opcionEstadosCivil',
  'tramite.solicitudes.postulante.imagenesPostulante',
];
export const relacionClaseLicenciaTramite = ['documentosLicencia', 'clasesLicencias', 'clasesLicencias.tramitesClaseLicencia'];

export const relacionTramiteDocumentos = [
  'tramite',
  'clasesLicencias',
  'tramite.solicitudes',
  'tramite.solicitudes.postulante',
  'tramite.documentosTramites',
];
export const relacionLicenciaRegistradaLIC = [
  'clasesLicencias',
  'documentosLicencia',
  'documentosLicencia.conductor',
  'documentosLicencia.conductor.postulante',
  'documentosLicencia.conductor.postulante.solicitudes',
  'documentosLicencia.conductor.postulante.solicitudes.tramites',
  'documentosLicencia.papelSeguridad',
  'documentosLicencia.estadosDF',
  'documentosLicencia.estadosPCL',
  'documentosLicencia.estadosDD',
];
export const relacionCertificadoAntecedentes = [
  'postulante',
  'postulante.conductor',
  'postulante.comunas',
  'postulante.comunas.oficinas',
  'postulante.comunas.oficinas.instituciones',
];

export const joinTramitesConExamenes = {
  tramites: 'tramitesClaseLicencia.tramite',
  clasesLicencias: 'tramitesClaseLicencia.clasesLicencias',
  solicitudes: 'tramites.solicitudes',
  postulante: 'solicitudes.postulante',
  tiposTramite: 'tramites.tiposTramite',
  estadoTramite: 'tramites.estadoTramite',
  // colaExamTeorico: 'tramites.colaExamTeorico', colaExamIdoneidadMoral: 'tramites.colaExamIdoneidadMoral',
  // colaExamPractico: 'tramites.colaExamPractico', colaExamMedico: 'tramites.colaExamMedico',
};
export const relacionPostulanteOficina = [
  'comunas',
  'comunas.oficinas',
  'comunas.oficinas.instituciones',
  'comunas.oficinas.instituciones.papelSeguridad',
];

export const relacionSolicitudes = [
  'estadosSolicitud',
  'postulante',
  'tramites',
  'tramites.tiposTramite',
  'tramites.tramitesClaseLicencia',
  'tramites.tramitesClaseLicencia.clasesLicencias',
];

export const relacionProveedores: string[] = ['region', 'comuna'];

export enum ColumnasProveedores {
  nombre = 'Proveedor.nombre',
  codigo = 'Proveedor.codigo',
  direccion = 'Proveedor.direccion',
  nrodireccion = 'Proveedor.nrodireccion',
  region = 'Proveedor__region.Nombre',
  comuna = 'Proveedor__comuna.Nombre',
}

export enum ColumnasMunicipalidades {
  nombre = 'Nombre',
  descripcion = 'Descripcion',
}

export enum ColumnasOficinas {
  nombre = 'OficinasEntity.Nombre',
  descripcion = 'OficinasEntity.Descripcion',
  direccion = 'OficinasEntity.Direccion',
  calleDireccion = 'OficinasEntity.calleDireccion',
  nroDireccion = 'OficinasEntity.nroDireccion',
  letraDireccion = 'OficinasEntity.letraDireccion',
  restoDireccion = 'OficinasEntity.restoDireccion',
  comuna = 'OficinasEntity__comunas.Nombre',
  region = 'OficinasEntity__comunas__regiones.Nombre',
}

export enum ColumnasFolios {
  folioRango = 'LotePapelSeguridadEntity.descripcion',
  letra = 'papelSeguridad.LetrasFolio',
  fabricante = 'proveedor.nombre',
  estado = 'LotePapelSeguridadEntity.habilitado',
  fechaCarga = 'LotePapelSeguridadEntity.created_at',
  // nFinal = 'papelSeguridadMax.Folio',
  // nInicial = 'papelSeguridadMin.Folio'
}

export enum ColumnasSolicitudes {
  Id = 'Solicitudes.idSolicitud',
  RUN = 'Solicitudes__postulante.RUN',
  NombrePostulante = 'Solicitudes__postulante.Nombres',
  FechaSolicitud = 'Solicitudes.FechaCreacion',
  EstadoSolicitud = 'Solicitudes__estadosSolicitud.Nombre',
  TipoTramite = 'Solicitudes__tramites__tiposTramite.Nombre',
  ClaseLicencia = 'Solicitudes__tramites__tramitesClaseLicencia__clasesLicencias.Abreviacion',
}

export enum ColumnasSolicitudes2 {
  Id = 'Solicitudes.idSolicitud',
  RUN = 'postulante.RUN',
  NombrePostulante = 'postulante.Nombres',
  FechaSolicitud = 'Solicitudes.FechaCreacion',
  EstadoSolicitud = 'estadosSolicitud.Nombre',
  TipoTramite = 'tiposTramite.Nombre',
  ClaseLicencia = 'clasesLicencias.Abreviacion',
  IdTramite = 'tramites.idTramite',
  EstadoTramite = 'estadoTramite.Nombre',
}

//tambien sirve para pagos cancelados
export enum ColumnasSolicitudesProrroga {
  idRegistroPago = 'RegistroPago.idRegistroPago',
  Id = 'Solicitudes.idSolicitud',
  IdTramite = 'tramites.idTramite',
  TipoTramite = 'tiposTramite.Nombre',
  EstadoTramite = 'estadoTramite.Nombre',
  RUN = 'postulante.RUN',
  ClaseLicencia = 'clasesLicencias.Abreviacion',
  NombrePostulante = 'postulante.Nombres',
  InicioTramite = 'tramites.created_at',
  UltimaModTramite = 'tramites.update_at',
  FechaExpiracionTramite = 'FechaExpiracionTramite',
  FechaCreacion = 'Solicitudes.FechaCreacion',
  VencimientoTramite = 'tramites.created_at'
}

export enum ColumnasTramites2 {
  IdSolicitud = 'solicitudes.idSolicitud',
  IdTramite = 'tramites.idTramite',
  TipoTramite = 'tiposTramite.Nombre',
  EstadoTramite = 'estadoTramite.Nombre',
  RUN = 'postulante.RUN',
  ClaseLicencia = 'clasesLicencias.Abreviacion',
  tipoExamiancion = 'tipoExaminaciones.nombreExaminacion',
  estadoExaminacion = 'estadoExaminaciones.nombreEstado',
  fechaTramite = 'tramites.created_at',
}

export enum ColumnasColaExaminacion {
  Id = 'ColaExaminacion.idColaExaminacion',
  RUN = 'po.RUN',
  Nombres = 'po.Nombres',
  Examinador = 'user.Nombres',
  ClaseLicencia = 'cl.Abreviacion',
  FechaCreacion = 'so.FechaCreacion',
  FechaRendicion = 'res.fechaAprobacion',
  IdTramite = 'tr.idTramite',
  Aprobacion = 'ColaExaminacion.aprobado',
  IdSolicitud = 'so.idSolicitud',
  TipoTramite = 'tt.Nombre',
}

export enum ColumnasSolicitudesHistorial {
  Id = 'TramitesClaseLicencia__tramite__solicitudes.idSolicitud',
  RUN = 'TramitesClaseLicencia__tramite__solicitudes__postulante.RUN',
  NombrePostulante = 'TramitesClaseLicencia__tramite__solicitudes__postulante.Nombres',
  FechaSolicitud = 'TramitesClaseLicencia__tramite__solicitudes.FechaCreacion',
  EstadoSolicitud = 'TramitesClaseLicencia__tramite__solicitudes__estadosSolicitud.Nombre',
  EstadoTramite = 'TramitesClaseLicencia__tramite__estadoTramite.Nombre',
  TipoTramite = 'TramitesClaseLicencia__tramite__tiposTramite.Nombre',
  idTramite = 'TramitesClaseLicencia__tramite.idTramite',
  ClaseLicencia = 'TramitesClaseLicencia__clasesLicencias.Abreviacion',
}

export enum ColumnasTramites {
  Id = 'TramitesClaseLicencia__tramite.idSolicitud',
  IdTramite = 'TramitesClaseLicencia__tramite.idTramite',
  RUN = 'TramitesClaseLicencia__tramite__solicitudes__postulante.RUN',
  NombrePostulante = 'TramitesClaseLicencia__tramite__solicitudes__postulante.Nombres',
  EstadoTramite = 'TramitesClaseLicencia__tramite__estadoTramite.Nombre',
  TipoTramite = 'TramitesClaseLicencia__tramite__tiposTramite.Nombre',
  ClaseLicencia = 'TramitesClaseLicencia__clasesLicencias.Abreviacion',
  // Moral = 'TramitesClaseLicencia__tramite__colaExamIdoneidadMoral.Aprobado',
  // Medico = 'TramitesClaseLicencia__tramite__colaExamMedico.Aprobado',
  // Teorico = 'TramitesClaseLicencia__tramite__colaExamTeorico.Aprobado',
  // Practico = 'TramitesClaseLicencia__tramite__colaExamPractico.Aprobado'
}

export enum ColumnasUsuarios {
  RUN = 'usuarios.RUN',
  Nombre = 'usuarios.Nombres',
  Rol = 'usuarios__rolesUsuarios__roles.Nombre',
  Region = 'usuarios__comunas__regiones.Nombre',
}

export enum ColumnasTiposDocumento {
  Nombre = 'TiposDocumentoEntity.Nombre',
  Descripcion = 'TiposDocumentoEntity.Descripcion',
}

export enum ColumnasDocsRequeridoRol {
  Nombre = 'DocsRequeridoRol.nombre',
  Descripcion = 'DocsRequeridoRol.descripcion',
}

export enum ColumnasRoles {
  Nombre = 'Roles.Nombre',
  Descripcion = 'Roles.Descripcion',
  GrupoPermisos = 'Roles__grupoPermisosRoles__grupoPermisos.nombreGrupo',
  Creador = 'Roles__usuario.Nombres',
  Documentos = 'Roles.Nombre',
  Asignado = 'Roles.Nombre',
}

export enum ColumnasGrupoPermisos {
  Nombre = 'GrupoPermisos.nombreGrupo',
  Descripcion = 'GrupoPermisos.descripcion',
  RolesAsociados = 'GrupoPermisos__grupoPermisosRoles__roles.Nombre',
}

export enum RoleNames {
  JefeLicencias = 'Jefe de Licencias',
  ExaminadorPractico = 'Examinador Práctico',
  ExaminadorTeorico = 'Examinador Teórico',
  AdministrativoMunicipal = 'Administrativo Municipal',
  ExaminadorMedico = 'Examinador Médico',
  AdministradorSGLC = 'Administrador SGLC',
  DirectorTransito = 'Director de Tránsito',
  FiscalizacionMTT = 'Fiscalización MTT',
  Medico = 'Médico',
  AsistenteMedico = 'Asistente médico',
  Juez = 'Juez',
  SecretarioJPL = 'Secretario JPL',
  ComunicadorMTT_CONASET = 'Comunicador MTT_CONASET',
  AdministradorSEREMITT = 'Administrador SEREMITT',
}

export enum TipoExaminaciones {
  ExamenTeorico = 'Examinación Teórica',
  ExamenPractico = 'Examinación Práctica',
  ExamenMedico = 'Examinación Médica',
  ExamenIM = 'Examinación Idoneidad Moral',
  ExamenPreIM = 'Examinación IM Pre Eval',
}

export enum ColumnasNotificaciones {
  Titulo = 'NotificacionesEntity.Titulo',
  Categoria = 'NotificacionesEntity.idCategoria',
  Tipo = 'NotificacionesEntity.idNotificacionesTipoAudiencia',
  FechaCreacion = 'NotificacionesEntity.Fecha',
}

export enum PermisosNombres {
  // VisualizarTiposEstadosDeSolicitudLicencias = 'VisualizarTiposEstadosDeSolicitudLicencias',

  // MenuVerPerfilDeUsuario = 'MenuPerfilUsuario',

  // MenuVerModificacionesEspecialesSolicitudesPendientes = 'MenuModificacionesEspecialesSolicitudesPendientes',
  // MenuVerModificacionesEspecialesCambiosDeEstados = 'MenuModificacionesEspecialesCambiosDeEstados',
  // MenuReservadeHoras = 'MenuModificacionesEspecialesCambiosDeEstados',
  // VisualizarModuloOtorgamientosDenegaciones = 'VisualizarModuloOtorgamientosDenegaciones',
  // VisualizarListaPostulantes = 'VisualizarListaPostulantes',
  // VisualizarIngresoPostulante = 'VisualizarIngresoPostulante',
  
  // EditarOtorgamientosDenegaciones = 'EditarOtorgamientosDenegaciones',
  // ValidadorDenegarLicencia = 'ValidadorDenegarLicencia',
  // DenegacionesEnviarEmail = 'DenegacionesEnviarEmail',
  // ImprimirDenegacion = 'ImprimirDenegacion',
  // ValidadorOtorgarLicencia = 'ValidadorOtorgarLicencia',
  // VisualizarListaLicencias = 'VisualizarListaLicencias',
  // ImprimirLicencias = 'ImprimirLicencias',
  
  // ValidadorConfirmarRecepcion = 'ValidadorConfirmarRecepcion',
  // VisualizarModuloConsultaHistorica = 'VisualizarModuloConsultaHistorica',
  // VisualizarCHalRNCVM = 'VisualizarCHalRNCVM',
  // VisualizarConsultaCarpetaDelConductor = 'VisualizarConsultaCarpetaDelConductor',
  // VisualizarConsultaAntecedentesObtencionRenovacion = 'VisualizarConsultaAntecedentesObtencionRenovacion',
  // VisualizarConsultaAntecedentesJuzgadoMunicipalidades = 'VisualizarConsultaAntecedentesJuzgadoMunicipalidades',
  // VisualizarModuloSolicitudesEnProceso = 'VisualizarModuloSolicitudesEnProceso',
  // VisualizarTramitesEnProceso = 'VisualizarTramitesEnProceso',
  // VisualizarSolicitudesEnProceso = 'VisualizarSolicitudesEnProceso',
  // VisualizarHistorialSolicitudesPostulantes = 'VisualizarHistorialSolicitudesPostulantes',
  // VisualizarDetalleHistorialSolicitud = 'VisualizarDetalleHistorialSolicitud',
  // VisualizarModuloAdministracionSGLC = 'VisualizarModuloAdministracionSGLC',
  // VisualizarListadoRoles = 'VisualizarListadoRoles',
  // CrearNuevosRoles = 'CrearNuevosRoles',
  // EditarRoles = 'EditarRoles',
  // EliminarRoles = 'EliminarRoles',
  // VisualizarDetalleRoles = 'VisualizarDetalleRoles',
  // VisualizarListadoGrupoPermisos = 'VisualizarListadoGrupoPermisos',
  // CrearNuevosGrupoPermisos = 'CrearNuevosGrupoPermisos',
  // EditarGrupoPermisos = 'EditarGrupoPermisos',
  // EliminarGrupoPermisos = 'EliminarGrupoPermisos',
  // VisualizarDetalleGrupoPermisos = 'VisualizarDetalleGrupoPermisos',
  // VisualizarListadoPermisos = 'VisualizarListadoPermisos',
  // VisualizarListadoTipoDocumentos = 'VisualizarListadoTipoDocumentos',
  // CrearTipoDocumentos = 'CrearTipoDocumentos',
  // EditarTipoDocumentos = 'EditarTipoDocumentos',
  // EliminarTipoDocumentos = 'EliminarTipoDocumentos',
  // VisualizarDetalleTipoDocumentos = 'VisualizarDetalleTipoDocumentos',
  // VisualizarListadoUsuarios = 'VisualizarListadoUsuarios',
  // CrearUsuario = 'CrearUsuario',
  // EditarUsuario = 'EditarUsuario',
  // EliminarUsuario = 'EliminarUsuario',
  // VisualizarDetalleUsuario = 'VisualizarDetalleUsuario',
  // ActivarUsuario = 'ActivarUsuario',
  // RecepcionActivacionUsuario = 'RecepcionActivacionUsuario',
  // VisualizarListadoMunicipalidades = 'VisualizarListadoMunicipalidades',
  // CrearMunicipalidad = 'CrearMunicipalidad',
  // EditarMunicipalidad = 'EditarMunicipalidad',
  // EliminarMunicipalidad = 'EliminarMunicipalidad',
  // VisualizarDetalleMunicipalidad = 'VisualizarDetalleMunicipalidad',
  // ActivarMunicipalidad = 'ActivarMunicipalidad',
  // VisualizarTiposMunicipalidades = 'VisualizarTiposMunicipalidades',
  // VisualizarTiposClasesLicencias = 'VisualizarTiposClasesLicencias',
  // VisualizarTiposTramitesLicencias = 'VisualizarTiposTramitesLicencias',
  // visualizarTiposEstadosDeSolicitudLicencias = 'visualizarTiposEstadosDeSolicitudLicencias',
  // VisualizarTiposSexo = 'VisualizarTiposSexo',
  // VisualizarTiposComunas = 'VisualizarTiposComunas',
  // VisualizarTiposRegiones = 'VisualizarTiposRegiones',
  // VisualizarTiposEstadoCivil = 'VisualizarTiposEstadoCivil',
  // VisualizarTiposNacionalidad = 'VisualizarTiposNacionalidad',
  // VisualizarTiposCalidadJuridica = 'VisualizarTiposCalidadJuridica',
  // VisualizarTiposEstamento = 'VisualizarTiposEstamento',
  // VisualizarTiposGrado = 'VisualizarTiposGrado',
  // GestionarNotificaciones = 'GestionarNotificaciones',
  // GestionarFolios = 'GestionarFolios',
  // GestionarProveedor = 'GestionarProveedor',
  // MenuVerModificacionesEspeciales = 'MenuModificacionesEspeciales',
  // MenuVerModificacionesEspecialesApelaciones = 'MenuModificacionesEspecialesApelaciones',
  // MenuVerModificacionesEspecialesProrrogasOportunidades = 'MenuModificacionesEspecialesProrrogasOportunidades',
  // AsignarDecisionIdoneidadMoral = 'AsignarDecisionIdoneidadMoral',
  // comunicadosOficiales = 'comunicadosOficiales',
  // crearescuelaconductores = 'crearescuelaconductores',
  // examinacionMedicaEndpoint = 'examinacionMedicaEndpoint',
  // ListaPostulantesesperadeotorgardenegar = 'ListaPostulantesesperadeotorgardenegar',
  // MenuAdministraciondeRolesyPermisos = 'MenuAdministraciondeRolesyPermisos',
  // MenuAdministracionSGLC = 'MenuAdministracionSGLC',
  // MenuAdministracionyParametrizaciondeTramites = 'MenuAdministracionyParametrizaciondeTrámites',
  // MenuComunicadosOficiales = 'MenuComunicadosOficiales',
  // MenuComunicateconNosotros = 'MenuComunicateconNosotros',
  // MenuConsultaCertificadodeAntecedentesdelConductorusoexclusivoJuzgadosPoliciaLocalyMunicipalidades = 'MenuConsultaCertificadodeAntecedentesdelConductorusoexclusivoJuzgadosPoliciaLocalyMunicipalidades',
  // MenuConsultaCertificadodeAntecedentesdelConductorusoexclusivoobtencionoenovaciondeLicenciasdeConductor = 'MenuConsultaCertificadodeAntecedentesdelConductorusoexclusivoobtencionorenovaciondeLicenciasdeConductor',
  // MenuConsultaHistorica = 'MenuConsultaHistorica',
  // MenuConsultaHistoricaalRNCVM = 'MenuConsultaHistoricaalRNCVM',
  // MenuCreaciondeComunicadoOficial = 'MenuCreaciondeComunicadoOficial',
  // MenuCreaciondePreguntaFrecuente = 'MenuCreaciondePreguntaFrecuente',

  // menuCreacionyAdministraciondeMunicipalidades = 'menuCreacionyAdministraciondeMunicipalidades',
  // MenuDescargadeDocumentos = 'MenuDescargadeDocumentos',

  // menuEmpresasFabricantesdePapeldeSeguridad = 'menuEmpresasFabricantesdePapeldeSeguridad',
  // MenuFiscalizacionyReportes = 'MenuFiscalizacionyReportes',
  // MenuGestionyAdministracion = 'MenuGestionyAdministracion',
  // menuHistorialdeSolicitudes = 'menuHistorialdeSolicitudes',
  // MenuIdoneidadMoralPreevaluacion = 'MenuIdoneidadMoralPreevaluacion',
  // MenuInformaciondeFolios = 'MenuInformaciondeFolios',
  // MenuIngresodeResolucionesJudiciales = 'MenuIngresodeResolucionesJudiciales',
  // MenuInscripcionyAdministraciondeUsuarios = 'MenuInscripcionyAdministraciondeUsuarios',
  // MenuJuzgadodePoliciaLocal = 'MenuJuzgadodePoliciaLocal',
  // MenuModificacionesEspeciales = 'MenuModificacionesEspeciales',
  // MenuModificacionesEspecialesApelaciones = 'MenuModificacionesEspecialesApelaciones',
  // MenuModificacionesEspecialesCambiosDeEstados = 'MenuModificacionesEspecialesCambiosDeEstados',

  // MenuModificacionesEspecialesProrrogasOportunidades = 'MenuModificacionesEspecialesProrrogasOportunidades',
  // MenuModificacionesEspecialesSolicitudesPendientes = 'MenuModificacionesEspecialesSolicitudesPendientes',
  // MenuModuloPrincipaldeComunicadosOficiales = 'MenuModuloPrincipaldeComunicadosOficiales',
  // MenuOtorgamientoyDenegaciones = 'MenuOtorgamientoyDenegaciones',
  // MenuPerfilUsuario = 'MenuPerfilUsuario',
  // MenuRecepcioConforme = 'MenuRecepcioConforme',
  // MenuReportes = 'MenuReportes',
  // MenuRepositoriodeInformacion = 'MenuRepositoriodeInformacion',
  // MenuReservadeHorasAdministraciondeBloquesHorarioyBloqueActivos = 'MenuReservadeHorasAdministraciondeBloquesHorarioyBloqueActivos',
  // MenuSolicitudesenProceso = 'MenuSolicitudesenProceso',
  // MenuSubSolicitudesEnProceso = 'MenuSubSolicitudesEnProceso',
  // MenuTramitesenProceso = 'MenuTramitesenProceso',
  // MenuVisualizaciondeLicencias = 'MenuVisualizaciondeLicencias',
  // ModificaciondeDatosPersonalesPostulante = 'ModificaciondeDatosPersonalesPostulante',
  // protecpago = 'protecpago',
  // VisualizarIdoneidadMoral = 'VisualizarIdoneidadMoral',
  // VisualizarIdoneidadMoralPreEvaluacion = 'VisualizarIdoneidadMoralPreEvaluacion',
  // VisualizarListaExamenPractico = 'VisualizarListaExamenPractico',
  // VisualizarListaExamenTeorico = 'VisualizarListaExamenTeorico',
  // MenuIngresoPostulante = 'MenuIngresoPostulante',

  // GestorDocumentalEditarDocumento = 'GestorDocumentalEditarDocumento',
  // GestorDocumentalVisualizarDocumentos = 'GestorDocumentalVisualizarDocumentos',

  


  // Permisos nuevos 23/05/2022

  // INGRESO Y EVALUACIÓN
  
  // Ingreso Postulante
  AccesoIngresoPostulante = 'AccesoIngresoPostulante',
  IngresoyEvaluacionIngresoyEvaluacionBuscarPostulante = "IngresoyEvaluacion-IngresoyEvaluacion-BuscarPostulante",
  IngresoyEvaluacionIngresoyEvaluacionIniciarunasolicitud	 = "IngresoyEvaluacion-IngresoyEvaluacion-Iniciarunasolicitud",
  IngresoyEvaluacionIngresoyEvaluacionAgregarTramites	 = "IngresoyEvaluacion-IngresoyEvaluacion-AgregarTramites",
  IngresoyEvaluacionIngresoyEvaluacionEliminarTramite	 = "IngresoyEvaluacion-IngresoyEvaluacion-EliminarTramite",
  IngresoyEvaluacionIngresoyEvaluacionCompletarSolicitud	 = "IngresoyEvaluacion-IngresoyEvaluacion-CompletarSolicitud",

  //Ingreso Postulante-Solicitud
  IngresoyEvaluacionIngresoyEvaluacionModificacionPostulante	 = "IngresoyEvaluacion-IngresoyEvaluacion-ModificacionPostulante",
  IngresoyEvaluacionIngresoyEvaluacionAgregarotroTramite	 = "IngresoyEvaluacion-IngresoyEvaluacion-AgregarotroTramite",
  IngresoyEvaluacionIngresoyEvaluacionvertodoslostramitesborradoroabierto	 = "IngresoyEvaluacion-IngresoyEvaluacion-vertodoslostramites-borradoroabierto",
  IngresoyEvaluacionIngresoyEvaluacioneliminartramitesdemismaofina	 = "IngresoyEvaluacion-IngresoyEvaluacion-eliminartramitesdemismaofina",
  IngresoyEvaluacionIngresoyEvaluacionverclasesdelicenciadelpostulante	 = "IngresoyEvaluacion-IngresoyEvaluacion-verclasesdelicenciadelpostulante",
  IngresoyEvaluacionIngresoyEvaluacionAgregarescueladeconductores	 = "IngresoyEvaluacion-IngresoyEvaluacion-Agregarescueladeconductores",
  IngresoyEvaluacionIngresoyEvaluacionAgregarcertificadoconductor	 = "IngresoyEvaluacion-IngresoyEvaluacion-Agregarcertificadoconductor",
  IngresoyEvaluacionIngresoyEvaluacionDocumentosasociadosaTramites	 = "IngresoyEvaluacion-IngresoyEvaluacion-DocumentosasociadosaTramites",
  
  // Idoneidad Moral pre-evaluación
  VisualizarListaPostulantesPreEvaluacionIdoneidadMoral= 'VisualizarListaPostulantesPreEvaluacionIdoneidadMoral',
  IngresoyEvaluacionIdoneidadMoralPreEvaluacionDescargaHVC = 'IngresoyEvaluacion-IdoneidadMoralPre-Evaluacion-DescargaHVC',
  IngresoyEvaluacionIdoneidadMoralPreEvaluacionAlertarNoAlertar = 'IngresoyEvaluacion-IdoneidadMoralPre-Evaluacion-Alertar-NoAlertar',

  // Idoneidad Moral
  VisualizarListaPostulantesEvaluacionIdoneidadMoral= 'VisualizarListaPostulantesEvaluacionIdoneidadMoral',
  IngresoyEvaluacionIdoneidadMoralAprobarRechazar = 'IngresoyEvaluacion-IdoneidadMoral-Aprobar-Rechazar',
  IngresoyEvaluacionIdoneidadMoralDescargaHVC = "IngresoyEvaluacion-IdoneidadMoral-DescargaHVC",

  // Exámen Médico
  VisualizarListaPostulantesExamenMedico= 'VisualizarListaPostulantesExamenMedico',
  IngresoyEvaluacionExamenMedicoVisualizar	 = "IngresoyEvaluacion-ExamenMedico-Visualizar",
  IngresoyEvaluacionExamenMedicootrasexaminaciones	 = "IngresoyEvaluacion-ExamenMedico-otrasexaminaciones",
  IngresoyEvaluacionExamenMedicoCompletarEntrevistaMedica	 = "IngresoyEvaluacion-ExamenMedico-CompletarEntrevistaMedica",
  IngresoyEvaluacionExamenMedicoCompletarSicometrico	 = "IngresoyEvaluacion-ExamenMedico-CompletarSicometrico",
  IngresoyEvaluacionExamenMedicoVerFormularioResumen	 = "IngresoyEvaluacion-ExamenMedico-VerFormularioResumen",
  IngresoyEvaluacionExamenMedicoCompletarCalificacion	 = "IngresoyEvaluacion-ExamenMedico-CompletarCalificacion",
  IngresoyEvaluacionExamenMedicoModificar	 = "IngresoyEvaluacion-ExamenMedico-Modificar",
  
  // Exámen Teórico
  VisualizarListaPostulantesExamenTeorico= 'VisualizarListaPostulantesExamenTeorico',

  // Exámen Práctico
  VisualizarListaPostulantesExamenPractico= 'VisualizarListaPostulantesExamenPractico',
  VisualizarResultadosErrorExamenPracticoIngresoPostulante = 'VisualizarResultadosErrorExamenPracticoIngresoPostulante',
  IngresoyEvaluacionExamenPracticoCompletarExamen	 = "IngresoyEvaluacion-ExamenPractico-CompletarExamen",
  IngresoyEvaluacionExamenPracticoModificar	 = "IngresoyEvaluacion-ExamenPractico-Modificar",
  IngresoyEvaluacionExamenPracticoConsultarFichaResumen	 = "IngresoyEvaluacion-ExamenPractico-ConsultarFichaResumen",
  IngresoyEvaluacionExamenPracticoAnular	 = "IngresoyEvaluacion-ExamenPractico-Anular",
  IngresoyEvaluacionExamenPracticootrasexaminaciones	 = "IngresoyEvaluacion-ExamenPractico-otrasexaminaciones",
  
  // Caja
  VisualizarListaPostulantesModuloCaja= 'VisualizarListaPostulantesModuloCaja',
  IngresoyEvaluacionModuloCajaVisualizarSolicitudesPendientesdePagoyHistorialdeSolicitudes	 = "IngresoyEvaluacion-ModuloCaja-VisualizarSolicitudesPendientesdePagoyHistorialdeSolicitudes",
  IngresoyEvaluacionModuloCajaModificarEstadodeSolicitudesPendientesdePago	 = "IngresoyEvaluacion-ModuloCaja-ModificarEstadodeSolicitudesPendientesdePago",
  IngresoyEvaluacionModuloCajaModificarEstadodeSolicitudesdelHistorialdeSolicitudes	 = "IngresoyEvaluacion-ModuloCaja-ModificarEstadodeSolicitudesdelHistorialdeSolicitudes",
  
  // Datos Personales
  AccesoModificacionDatosPersonales= 'AccesoModificacionDatosPersonales',
  IngresoyEvaluacionDatosPersonalesVisualizarInformaciondelCiudadano	 = "IngresoyEvaluacion-DatosPersonales-VisualizarInformaciondelCiudadano",
  IngresoyEvaluacionDatosPersonalesModificarDatosPersonalesdelCiudadano	 = "IngresoyEvaluacion-DatosPersonales-ModificarDatosPersonalesdelCiudadano",
  IngresoyEvaluacionDatosPersonalesVisualizarLicenciasAutorizadas	 = "IngresoyEvaluacion-DatosPersonales-VisualizarLicenciasAutorizadas",
  
  // OTORGAMIENTO Y DENEGACIONES

  //Postulantes a la espera
  VisualizarListaTramitesEnListaDeEspera = 'VisualizarListaTramitesEnListaDeEspera',
  OtorgamientoyDenegacionesListadeEsperaVisualizar	 = "OtorgamientoyDenegaciones-ListadeEspera-Visualizar",

  //Otorgamiento y Denegaciones
  VisualizarListaOtorgamientosDenegaciones = 'VisualizarListaOtorgamientosDenegaciones',
  OtorgamientoyDenegacionesOtorgamientosDenegacionesVisualizar	 = "OtorgamientoyDenegaciones-OtorgamientosDenegaciones-Visualizar",
  OtorgamientoyDenegacionesOtorgamientosDenegacionesOtorgarlicencia	 = "OtorgamientoyDenegaciones-OtorgamientosDenegaciones-Otorgarlicencia",
  OtorgamientoyDenegacionesOtorgamientosDenegacionesDenegarLicencia	 = "OtorgamientoyDenegaciones-OtorgamientosDenegaciones-DenegarLicencia",

  // Visualizar Licencias
  VisualizarListaTramitesEnVisualizacionLicencia = 'VisualizarListaTramitesEnVisualizacionLicencia',
  OtorgamientoyDenegacionesVisualizarLicenciasVisualizar	 = "OtorgamientoyDenegaciones-VisualizarLicencias-Visualizar",
  OtorgamientoyDenegacionesVisualizarLicenciasImprimirLicencia	 = "OtorgamientoyDenegaciones-VisualizarLicencias-ImprimirLicencia",

  // Recepción Conforme
  VisualizarListaRecepcionConforme = 'VisualizarListaRecepcionConforme',
  OtorgamientoyDenegacionesRecepcionConformeVisualizar	 = "OtorgamientoyDenegaciones-RecepcionConforme-Visualizar",
  OtorgamientoyDenegacionesRecepcionConformeRecepcionarConforme	 = "OtorgamientoyDenegaciones-RecepcionConforme-RecepcionarConforme",
  OtorgamientoyDenegacionesRecepcionConformeRecepcionNoConforme	 = "OtorgamientoyDenegaciones-RecepcionConforme-RecepcionNoConforme",


  // CONSULTA HISTÓRICA

  // Consulta Histórica RNC
  AccesoConsultaHistoricaAlRNCVM= 'AccesoConsultaHistoricaAlRNCVM',
  ConsultaHistoricaConsultaHistRNCVisualizar	 = "ConsultaHistorica-ConsultaHistRNC-Visualizar",

  // Carpeta de Conductor
  AccesoConsultaCarpetaConductor= 'AccesoConsultaCarpetaConductor',
  ConsultaHistoricaCarpetaConductorDescargartodo	 = "ConsultaHistorica-CarpetaConductor-Descargartodo",
  ConsultaHistoricaCarpetaConductorDescargarArchivo	 = "ConsultaHistorica-CarpetaConductor-DescargarArchivo",
  ConsultaHistoricaCarpetaConductorSolicitarTraspasodeArchivos	 = "ConsultaHistorica-CarpetaConductor-SolicitarTraspasodeArchivos",

  // Certificado de Antecedentes (Obtención/Renovación)
  AccesoConsultaHistoricaUsoExclusivoObtencion = 'AccesoConsultaHistoricaUsoExclusivoObtencion',
  ConsultaHistoricaCertAntecedentesORVisualizar	 = "ConsultaHistorica-CertAntecedentesOR-Visualizar",
  ConsultaHistoricaCertAntecedentesORObtenerAntecedentes	 = "ConsultaHistorica-CertAntecedentesOR-ObtenerAntecedentes",

  // Certificado de Antecedentes (JPL/Muni)
  AccesoConsultaHistoricaUsoExclusivoObtencionJPL= 'AccesoConsultaHistoricaUsoExclusivoObtencionJPL',
  ConsultaHistoricaCertAntecedentesJPLMVisualizar	 = "ConsultaHistorica-CertAntecedentesJPLM-Visualizar",
  ConsultaHistoricaCertAntecedentesJPLMObtenerAntecedentes	 = "ConsultaHistorica-CertAntecedentesJPLM-ObtenerAntecedentes",


  // SOLICITUDES EN PROCESO

  // Trámites en Proceso
  VisualizarListaTramitesEnProceso= 'VisualizarListaTramitesEnProceso',

  // Solicitudes en Proceso
  VisualizarListaSolicitudesEnProceso= 'VisualizarListaSolicitudesEnProceso',

  // Historial de Solicitudes
  VisualizarListaHistorialDeSolicitudes= 'VisualizarListaHistorialDeSolicitudes',


  // MODIFICACIONES ESPECIALES

  // Modificaciones por Apelaciones
  VisualizarListaModificacionesEspecialesPorApelaciones= 'VisualizarListaModificacionesEspecialesPorApelaciones',
  ModificacionesEspecialesModificacionesporApelacionesFiltrarSolicitudes	 = "ModificacionesEspeciales-ModificacionesporApelaciones-FiltrarSolicitudes",
  ModificacionesEspecialesModificacionesporApelacionesVisualizarSolicitudes	 = "ModificacionesEspeciales-ModificacionesporApelaciones-VisualizarSolicitudes",
  ModificacionesEspecialesModificacionesporApelacionesModificarEstadoSolicitudes	 = "ModificacionesEspeciales-ModificacionesporApelaciones-ModificarEstadoSolicitudes",
  
  // Prórrogas/Oportunidades
  VisualizarListaModificacionesEspecialesProrrogasOportunidades= 'VisualizarListaModificacionesEspecialesProrrogasOportunidades',
  ModificacionesEspecialesProrrogasOportunidadesVisualizarListadodeExaminacionesPendientesdeunaNuevaOportunidadProrroga	 = "ModificacionesEspeciales-ProrrogasOportunidades-VisualizarListadodeExaminacionesPendientesdeunaNuevaOportunidadProrroga",
  ModificacionesEspecialesProrrogasOportunidadesSolicitarAprobacion	 = "ModificacionesEspeciales-ProrrogasOportunidades-SolicitarAprobacion",
  
  // Solicitudes Esp. Pendientes (Validaciones de Prórrogas Director de Tránsito)
  VisualizarListaModificacionesEspecialesSolicitudesEspecialesPendientes= 'VisualizarListaModificacionesEspecialesSolicitudesEspecialesPendientes',
  ModificacionesEspecialesSolicitudesEspPendientesVisualizarSolicitudes	 = "ModificacionesEspeciales-SolicitudesEspPendientes-VisualizarSolicitudes",
  ModificacionesEspecialesSolicitudesEspPendientesModificarEstadoProrrogasOportunidades	 = "ModificacionesEspeciales-SolicitudesEspPendientes-ModificarEstadoProrrogasOportunidades",
  
  // Cambios de Estados
  VisualizarListaModificacionesEspecialesCambiosDeEstados= 'VisualizarListaModificacionesEspecialesCambiosDeEstados',
  ModificacionesEspecialesCambiosdeEstadosVisualizarListadosdeSolicitudes	 = "ModificacionesEspeciales-CambiosdeEstados-VisualizarListadosdeSolicitudes",
  ModificacionesEspecialesCambiosdeEstadosModificarEstadodeTramitesExaminaciones	 = "ModificacionesEspeciales-CambiosdeEstados-ModificarEstadodeTramites-Examinaciones",

  // PERFIL DE USUARIO

  // Datos del Usuario
  AccesoDatosPersonalesDelUsuario= 'AccesoDatosPersonalesDelUsuario',
  PerfildeUsuarioDatosdelUsuarioModificarDatosdelUsuario	 = "PerfildeUsuario-DatosdelUsuario-ModificarDatosdelUsuario",

  // Consulta Rol
  PerfildeUsuarioConsultaRolVisualizarListadodeRol	 = "PerfildeUsuario-ConsultaRol-VisualizarListadodeRol",
  PerfildeUsuarioConsultaRolDescargaDocumentos	 = "PerfildeUsuario-ConsultaRol-DescargaDocumentos",
  PerfildeUsuarioConsultaRolSeleccionarunRol	 = "PerfildeUsuario-ConsultaRol-SeleccionarunRol",

  //Licencias/Antigüedad
  PerfildeUsuarioLicenciasAntiguedadVisualizarLicenciasAntiguedad	 = "PerfildeUsuario-LicenciasAntiguedad-VisualizarLicenciasAntiguedad",

  
  // MENÚ JPL

  //Resoluciones Judiciales
  VisualizarListaJuzgadoPoliciaLocalIngresoDeResolucionesJudiciales= 'VisualizarListaJuzgadoPoliciaLocalIngresoDeResolucionesJudiciales',
  JuzgadodePoliciaLocalResolucionesJudicialesVisualizarResoluciones	 = "JuzgadodePoliciaLocal-ResolucionesJudiciales-VisualizarResoluciones",
  JuzgadodePoliciaLocalResolucionesJudicialesSeleccionarAcciones	 = "JuzgadodePoliciaLocal-ResolucionesJudiciales-SeleccionarAcciones",
  JuzgadodePoliciaLocalResolucionesJudicialesExportarListadoResoluciones	 = "JuzgadodePoliciaLocal-ResolucionesJudiciales-ExportarListadoResoluciones",
  JuzgadodePoliciaLocalResolucionesJudicialesCrearNuevaResolucion	 = "JuzgadodePoliciaLocal-ResolucionesJudiciales-CrearNuevaResolucion",
  
  // COMUNICADOS OFICIALES

  // Módulo Principal de Comunicados Oficiales
  AccesoComunicadosOficiales= 'AccesoComunicadosOficiales',
  ComunicadosOficialesComunicadosOficialesVisualizarunComunicadoyunaPregunta	 = "ComunicadosOficiales-ComunicadosOficiales-VisualizarunComunicadoyunaPregunta",
  ComunicadosOficialesComunicadosOficialesBotonCargarmasComunicadosymasPreguntas	 = "ComunicadosOficiales-ComunicadosOficiales-BotonCargarmasComunicadosymasPreguntas",
  ComunicadosOficialesComunicadosOficialesCrearunComunicadoyunaPregunta	 = "ComunicadosOficiales-ComunicadosOficiales-CrearunComunicadoyunaPregunta",

  // Creación de Comunicado Oficial
  ComunicadosOficialesCreacionComOficialesCrearunComunicado	 = "ComunicadosOficiales-CreacionComOficiales-CrearunComunicado",
  ComunicadosOficialesCreacionComOficialesVisualizarpreviamenteunComunicado	 = "ComunicadosOficiales-CreacionComOficiales-VisualizarpreviamenteunComunicado",
  ComunicadosOficialesCreacionComOficialesPublicarunComunicado	 = "ComunicadosOficiales-CreacionComOficiales-PublicarunComunicado",
  ComunicadosOficialesCreacionComOficialesAdjuntarunarchivo	 = "ComunicadosOficiales-CreacionComOficiales-Adjuntarunarchivo",
  
  // Creación de Preguntas Frecuentes
  ComunicadosOficialesCreacionPregFrecuentesCrearunComunicado	 = "ComunicadosOficiales-CreacionPregFrecuentes-CrearunComunicado",
  ComunicadosOficialesCreacionPregFrecuentesVisualizarpreviamenteunComunicado	 = "ComunicadosOficiales-CreacionPregFrecuentes-VisualizarpreviamenteunComunicado",
  ComunicadosOficialesCreacionPregFrecuentesPublicarunComunicado	 = "ComunicadosOficiales-CreacionPregFrecuentes-PublicarunComunicado",
  ComunicadosOficialesCreacionPregFrecuentesAdjuntarunarchivo	 = "ComunicadosOficiales-CreacionPregFrecuentes-Adjuntarunarchivo",
  ComunicadosOficialesCreacionPregFrecuentesCargarmasPreguntas	 = "ComunicadosOficiales-CreacionPregFrecuentes-CargarmasPreguntas",
  
  // Comunícate con nosotros
  ComunicadosOficialesComunicateconNosotrosCrearunFormulariodeContacto	 = "ComunicadosOficiales-ComunicateconNosotros-CrearunFormulariodeContacto",
  ComunicadosOficialesComunicateconNosotrosVisualizarFormulariodeContacto	 = "ComunicadosOficiales-ComunicateconNosotros-VisualizarFormulariodeContacto",
  ComunicadosOficialesComunicateconNosotrosEnviarPregunta	 = "ComunicadosOficiales-ComunicateconNosotros-EnviarPregunta",
  ComunicadosOficialesComunicateconNosotrosAdjuntarunarchivo	 = "ComunicadosOficiales-ComunicateconNosotros-Adjuntarunarchivo",
  ComunicadosOficialesComunicateconNosotrosLimpiarFormulario	 = "ComunicadosOficiales-ComunicateconNosotros-LimpiarFormulario",
  

  // DESCARGA DE DOCUMENTOS

  // Repositorio de información
  VisualizarListaDescargaDocumentosRepositorioInformacion= 'VisualizarListaDescargaDocumentosRepositorioInformacion',
  DescargadeDocumentosRepositoriodeInformacionVisualizarDocumento	 = "DescargadeDocumentos-RepositoriodeInformacion-VisualizarDocumento",
  DescargadeDocumentosRepositoriodeInformacionSeleccionarAccionesPDF	 = "DescargadeDocumentos-RepositoriodeInformacion-SeleccionarAccionesPDF",
  DescargadeDocumentosRepositoriodeInformacionCrearNuevoDocumento	 = "DescargadeDocumentos-RepositoriodeInformacion-CrearNuevoDocumento",
  DescargadeDocumentosRepositoriodeInformacionSeleccionarAccionesEditarEliminar	 = "DescargadeDocumentos-RepositoriodeInformacion-SeleccionarAccionesEditarEliminar",
  
  // ADMINISTRACIÓN SGL

  // Roles - Permisos - Tipo de Documentos
  VisualizarListaAdminitracionRolesYPermisos= 'VisualizarListaAdminitracionRolesYPermisos',
  AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos	 = "AdministracionSGLC-Roles-Permisos-TipodeDocumentos-VisualizarRoles-GruposdePermisos-TipodeDocumentos",
  AdministracionSGLCRolesPermisosTipodeDocumentosConsultarListadePermisos	 = "AdministracionSGLC-Roles-Permisos-TipodeDocumentos-ConsultarListadePermisos",
  AdministracionSGLCRolesPermisosTipodeDocumentosCrearRolesGruposdePermisosTipodeDocumentos	 = "AdministracionSGLC-Roles-Permisos-TipodeDocumentos-CrearRoles-GruposdePermisos-TipodeDocumentos",
  AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones	 = "AdministracionSGLC-Roles-Permisos-TipodeDocumentos-SeleccionarAcciones",
  
  // Administración de Usuarios
  VisualizarListaIncripcionAdministracionUsuarios= 'VisualizarListaIncripcionAdministracionUsuarios',
  AdministracionSGLCAdministraciondeUsuariosVisualizarDetalledeUsuario	 = "AdministracionSGLC-AdministraciondeUsuarios-VisualizarDetalledeUsuario",
  AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario	 = "AdministracionSGLC-AdministraciondeUsuarios-Activar-DesactivarUsuario",
  AdministracionSGLCAdministraciondeUsuariosCrearNuevoUsuario	 = "AdministracionSGLC-AdministraciondeUsuarios-CrearNuevoUsuario",
  AdministracionSGLCAdministraciondeUsuariosModificarDatosUsuario	 = "AdministracionSGLC-AdministraciondeUsuarios-ModificarDatosUsuario",

  // Administración de Municipalidades - Oficinas
  AdministracionSGLCAdministraciondeMunicipalidadesOficinasBuscarOficina	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Oficinas-BuscarOficina",
  AdministracionSGLCAdministraciondeMunicipalidadesOficinasEditarOficina	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Oficinas-EditarOficina",
  AdministracionSGLCAdministraciondeMunicipalidadesOficinasCrearNuevaOficina	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Oficinas-CrearNuevaOficina",
  AdministracionSGLCAdministraciondeMunicipalidadesOficinasVisualizarListaOficinas	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Oficinas-VisualizarListaOficinas",
  AdministracionSGLCAdministraciondeMunicipalidadesOficinasVerDetalleOficina	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Oficinas-VerDetalleOficina",
  AdministracionSGLCAdministraciondeMunicipalidadesOficinasActivarRechazarInhabilitarOficina	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Oficinas-Activar-RechazarInhabilitarOficina",

  // Administración de Municipalidades - Municipalidades
  VisualizarListaCreacionAdministracionMunicipalidades= 'VisualizarListaCreacionAdministracionMunicipalidades',
  AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesCrearNuevaMunicipalidad	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Municipalidades-CrearNuevaMunicipalidad",
  AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesEditarMunicipalidad	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Municipalidades-EditarMunicipalidad",
  AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesVisualizarListaMunicipalidades	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Municipalidades-VisualizarListaMunicipalidades",
  AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesVerDetalleMunicipalidad	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Municipalidades-VerDetalleMunicipalidad",
  AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesActivarRechazarInhabilitarMunicipalidad	 = "AdministracionSGLC-AdministraciondeMunicipalidades-Municipalidades-Activar-RechazarInhabilitarMunicipalidad",
      

  // Fabricante de Papel de Seguridad
  VisualizarListaAdministracionFabricantesPapelSeguridad= 'VisualizarListaAdministracionFabricantesPapelSeguridad',
  AdministracionSGLCFabricantedePapeldeSeguridadVisualizarFabricante	 = "AdministracionSGLC-FabricantedePapeldeSeguridad-VisualizarFabricante",
  AdministracionSGLCFabricantedePapeldeSeguridadConsultarFabricante	 = "AdministracionSGLC-FabricantedePapeldeSeguridad-ConsultarFabricante",
  AdministracionSGLCFabricantedePapeldeSeguridadCrearNuevoFabricante	 = "AdministracionSGLC-FabricantedePapeldeSeguridad-CrearNuevoFabricante",
  AdministracionSGLCFabricantedePapeldeSeguridadEditarFabricante	 = "AdministracionSGLC-FabricantedePapeldeSeguridad-EditarFabricante",
    

  // Administracion y Parametrización de Trámites
  VisualizarListaAdministracionParametrizacionDeTramites= 'VisualizarListaAdministracionParametrizacionDeTramites',
  AdministracionSGLCAdministracionyParametrizaciondeTramitesBuscarParametros	 = "AdministracionSGLC-AdministracionyParametrizaciondeTramites-BuscarParametros",
  AdministracionSGLCAdministracionyParametrizaciondeTramitesVisualizarTramites	 = "AdministracionSGLC-AdministracionyParametrizaciondeTramites-VisualizarTramites",
  AdministracionSGLCAdministracionyParametrizaciondeTramitesVisualizarListadoParametros	 = "AdministracionSGLC-AdministracionyParametrizaciondeTramites-VisualizarListadoParametros",
  AdministracionSGLCAdministracionyParametrizaciondeTramitesCrearNuevoTramite	 = "AdministracionSGLC-AdministracionyParametrizaciondeTramites-CrearNuevoTramite",
  AdministracionSGLCAdministracionyParametrizaciondeTramitesEditarTramite	 = "AdministracionSGLC-AdministracionyParametrizaciondeTramites-EditarTramite",
  AdministracionSGLCAdministracionyParametrizaciondeTramitesActivarDesactivarTramite	 = "AdministracionSGLC-AdministracionyParametrizaciondeTramites-ActivarDesactivarTramite",
  AdministracionSGLCAdministracionyParametrizaciondeTramitesEditarParametros	 = "AdministracionSGLC-AdministracionyParametrizaciondeTramites-EditarParametros",
  
  // Administración de Folios
  VisualizarListaAdministracionInformacionDeFolios= 'VisualizarListaAdministracionInformacionDeFolios',
  AdministracionSGLCAdministraciondeFoliosVisualizarFolios	 = "AdministracionSGLC-AdministraciondeFolios-VisualizarFolios",
  AdministracionSGLCAdministraciondeFoliosCrearNuevoFolio	 = "AdministracionSGLC-AdministraciondeFolios-CrearNuevoFolio",
  AdministracionSGLCAdministraciondeFoliosAnularFolio	 = "AdministracionSGLC-AdministraciondeFolios-AnularFolio",
  
  // Administración y Parametrización  app Mobile
  VisualizarListaAppMobileAdminYParam= 'VisualizarListaAppMobileAdminYParam',
  AdministracionSGLCAdministracionyParametrizacionAppMobileVisualizarNotificacionesGenerales	 = "AdministracionSGLC-AdministracionyParametrizacionAppMobile-VisualizarNotificacionesGenerales",
  AdministracionSGLCAdministracionyParametrizacionAppMobileCrearNotificacionGeneral	 = "AdministracionSGLC-AdministracionyParametrizacionAppMobile-CrearNotificacionGeneral",
  AdministracionSGLCAdministracionyParametrizacionAppMobileDetalleNotificacionGeneral	 = "AdministracionSGLC-AdministracionyParametrizacionAppMobile-DetalleNotificacionGeneral",
  AdministracionSGLCAdministracionyParametrizacionAppMobileBuscarParametros	 = "AdministracionSGLC-AdministracionyParametrizacionAppMobile-BuscarParametros",
  AdministracionSGLCAdministracionyParametrizacionAppMobileVisualizarListadoParametros	 = "AdministracionSGLC-AdministracionyParametrizacionAppMobile-VisualizarListadoParametros",
  AdministracionSGLCAdministracionyParametrizacionAppMobileEditarParametros	 = "AdministracionSGLC-AdministracionyParametrizacionAppMobile-EditarParametros",
  
  // Fiscalización y Reportes
  AccesoAdministracionFiscalizacionYReportes= 'AccesoAdministracionFiscalizacionYReportes',
  AccesoAdministracionReportes= 'AccesoAdministracionReportes',

  // RESERVA DE HORA ADMINISTRATIVO

  // Administración de bloques horarios
  VisualizarListaReservaHorasYBloques = 'VisualizarListaReservaHorasYBloques',
  ReservadeHorasAdministracionBloquesHorariosBuscarBloquesHorario	 = "ReservadeHoras-AdministracionBloquesHorarios-BuscarBloquesHorario",
  ReservadeHorasAdministracionBloquesHorariosCrearBloqueHorario	 = "ReservadeHoras-AdministracionBloquesHorarios-CrearBloqueHorario",
  ReservadeHorasAdministracionBloquesHorariosEliminarBloqueHorario	 = "ReservadeHoras-AdministracionBloquesHorarios-EliminarBloqueHorario",
  ReservadeHorasBloqueosActivosBuscarBloqueoActivo	 = "ReservadeHoras-BloqueosActivos-BuscarBloqueoActivo",
  ReservadeHorasBloqueosActivosVisualizarBloqueoActivo	 = "ReservadeHoras-BloqueosActivos-VisualizarBloqueoActivo",
  ReservadeHorasBloqueosActivosCrearNuevoBloqueo	 = "ReservadeHoras-BloqueosActivos-CrearNuevoBloqueo",
  ReservadeHorasBloqueosActivosSeleccionarAccionesRevertir	 = "ReservadeHoras-BloqueosActivos-SeleccionarAcciones(Revertir)",
  ReservadeHorasReservarHorasBuscarReservas	 = "ReservadeHoras-ReservarHoras-BuscarReservas",
  ReservadeHorasReservarHorasVisualizarReservas	 = "ReservadeHoras-ReservarHoras-VisualizarReservas",
  ReservadeHorasReservarHorasSeleccionarReservar	 = "ReservadeHoras-ReservarHoras-SeleccionarReservar",
  ReservadeHorasReservarHorasAtenderHora	 = "ReservadeHoras-ReservarHoras-AtenderHora",
  ReservadeHorasReservarHorasAusentar	 = "ReservadeHoras-ReservarHoras-Ausentar",
  ReservadeHorasReservarHorasReprogramar	 = "ReservadeHoras-ReservarHoras-Reprogramar",
  ReservadeHorasReservarHorasCancelar	 = "ReservadeHoras-ReservarHoras-Cancelar",
  
  

}

export const config_email = {
  host: 'email-smtp.sa-east-1.amazonaws.com',
  port: 25,
  secure: true,
  auth: {
    user: 'AKIARA72SJBHPCAY23WQ',
    pass: 'BMbM1VccFRZFQWf15rtz0XsCmxT42kTUxe+zQFPJUSLv',
  },
};

// Para comprobar si una cadena de texto tiene solo letras y espacios
export const pattern = /^[a-zA-Z ]+$/;

// Registros de Usuario
export const registroUsuarioActivacion = 'Activacion';
export const registroUsuarioDesactivacion = 'Desactivacion';
export const registroUsuarioRechazo = 'Rechazo Activacion';
export const registroUsuarioCreacionUsuario = 'Creacion Usuario';
export const registroUsuarioEliminacion = 'Eliminacion';
export const registroUsuarioAsignacionRol = 'Asinacion Rol';
export const registroUsuarioEliminacionRol = 'Eliminacion Rol';
export const registroUsuarioEdicion = 'Edicion';

//Asuntos para Correos
export const asuntoActivacionUsuario = 'Peticion para activar usuario';
export const asuntoDesactivacionUsuario = 'Peticion para desactivar usuario';
export const asuntoRechazoUsuario = 'Peticion para rechazar usuario';

export const asuntoUsuarioActivado = 'Un usuario ha sido activado';
export const asuntoUsuarioDesactivado = 'Un usuario ha sido desactivado';
export const asuntoUsuarioRechazado = 'Una activacion de usuario ha sido rechazada';
export const asuntoUsuarioActivadoToActivatedUser = 'Su usuario ha sido activado';
export const asuntoActivacionOficina = 'Petición para activar oficina';
export const asuntoUsuarioPendienteActivacion = 'Usuario pendiente de activación';

export const MAIL_FROM = 'aparca@ingenia.es';

// Tipo Fabricante Papel Seguridad
export const TipoProveedor_FabricaPapelSeguridad = 1;

//Tipo de Institucion
export const TipoInstitucion_Municipalidad = 1;

//#region  roles Usuarios

export const directorTransito = 8; // falta quitar las referencias a esta constante

export const AdministradorSGLC = 7;

//#endregion

export enum motivosAnulacionFolios {
  CargaIncorrecta = 1,
  RoboInformacion = 2,
  RoboFormularios = 3,
  FormulariosDañados = 4,
  Otro = 5,
}

export const codigoEsperaIdoneidadMoral = '1001';

export const NotificacionesTipoAudienciaComuna = 1;
export const NotificacionesTipoAudienciaConductor = 2;

export const CATEGORIANOTIFTRAMITESOLICITUDESLICCOND: number = 0;
export const CATEGORIANOTIFSEGURIDADVIAL: number = 1;
export const CATEGORIANOTIFMINISTERIOTRANSPORTESTELEC: number = 2;
export const CATEGORIANOTIFCONTROLESVENCCAMRNC: number = 3;

export const TOKEN_NOTIFICACION = 'EPMSD3BQSG6Y323HUE6VTA2UC53MBOEFTU3D7QWVQIFOAZU42BGIK3SIXKGEWYPK33GUFLCLH35ZBMPPQIB6DCP2NAT7HQ108TLRQ7A';

export const SECRET_PASS_CRYPTO = 'ifuyasnd-di3/21oi-998cs';

export const SECRET_KEY_FRONT = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI';

export enum ClasesLicenciasIds {
  B = 1,
  C = 2,
  A1 = 4,
  A2 = 6,
  A3 = 7,
  A4 = 8,
  A5 = 9,
  D = 11,
  E = 10,
  F = 12,
  CR = 13,
  A1P = 14,
  A2P = 15,
}

export enum TipoExaminacionesIds {
  ExamenTeorico =   2,
  ExamenPractico =  3,
  ExamenMedico =    4,
  ExamenIM =        5,
  ExamenPreIM =     6
}

export const imagenBarraRUNBase64 = 'iVBORw0KGgoAAAANSUhEUgAAAWcAAAAfCAIAAAC03JvCAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADNSURBVHhe7ddRjoIwGIVR6P53M2xB44IcwYeCZSSOMTFyH03OaaA/8P6l9IfjaRrHWut1vEzTuCxL98bjU9vv66GN/d/9WXvTrrb6dVr9T8DX6oef4fJ73p4APin3I8LLMQHgnVLr3DZ/DsBOZZ7rNgLs0KqxnjUAdlINIFMW1QASZdsB9lENIKMaQEY1gIxqABnVADKqAWRUA8ioBpBRDSCjGkBGNYCMagAZ1QAyqgFkVAPIqAaQUQ0goxpARjWAjGoAGdUAMqoBJLruBonwMRB3MIXSAAAAAElFTkSuQmCC';

export enum TipoRolesIds {
  Superusuario =    1,
  Regional =        2,
  Oficina =         3,
  Generico =        4
}

export enum TipoInstitucionesIds {
  Municipalidad =     1,
  JPL =               2,
  Conaset =           3,
  Carabineros =       4,
  PDI =               5
}
