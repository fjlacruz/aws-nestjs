import { ApiPropertyOptional } from "@nestjs/swagger";

export class CertificadosPostulanteDto {

    @ApiPropertyOptional()
    idPostulante: number;
    
    @ApiPropertyOptional()
    RUN: string;

    @ApiPropertyOptional()
    archivo: string;

    @ApiPropertyOptional()
    documento: string;

    @ApiPropertyOptional()
    art29: string;

    @ApiPropertyOptional()
    RUNCompleto: string;

    @ApiPropertyOptional()
    isArt29: boolean;
}