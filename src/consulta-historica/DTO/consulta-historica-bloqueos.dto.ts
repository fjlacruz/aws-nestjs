import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConsultaHistoricaBloqueosDto {

    @ApiPropertyOptional()
    bloRun: string;

    @ApiPropertyOptional()
    bloFechaHoraB: string;

    @ApiPropertyOptional()
    bloTipoB: string;

    @ApiPropertyOptional()
    bloCuenta: string;

    @ApiPropertyOptional()
    bloMotivo: string;

    @ApiPropertyOptional()
    bloNumero: string;

    @ApiPropertyOptional()
    bloOrigen: string;

    @ApiPropertyOptional()
    bloFechaHoraD: string;

}