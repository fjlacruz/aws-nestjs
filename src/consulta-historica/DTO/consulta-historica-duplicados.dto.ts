import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConsultaHistoricaDuplicadosDto {

    @ApiPropertyOptional()
    lidRun: string;

    @ApiPropertyOptional()
    lidSec: string;

    @ApiPropertyOptional()
    lidFecha: string;

    @ApiPropertyOptional()
    lidVigencia: string;

    @ApiPropertyOptional()
    lidEstado: string;

    @ApiPropertyOptional()
    lidTipoLic: string;

    @ApiPropertyOptional()
    lidFolio: string;

    @ApiPropertyOptional()
    lidFechaIng: string;

    @ApiPropertyOptional()
    lidObservacion: string;

    @ApiPropertyOptional()
    lidFecRecepcion: string;

    @ApiPropertyOptional()
    lidCodOrigen: string;

    @ApiPropertyOptional()
    lidComuna: string;

    @ApiPropertyOptional()
    lidRunEscuela: string;

    @ApiPropertyOptional()
    lidLetraFolio: string;

    @ApiPropertyOptional()
    lidEstadoClaLic: string;

    @ApiPropertyOptional()
    lidEstadoFisLic: string;

    @ApiPropertyOptional()
    lidEstadoDigLic: string;

    @ApiPropertyOptional()
    lidFechaPrxCtl: string;

}