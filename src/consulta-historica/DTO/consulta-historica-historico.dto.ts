import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConsultaHistoricaHistoricoDto {
    
    @ApiPropertyOptional()
    hisRun: string;

    @ApiPropertyOptional()
    hisTipo: string;

    @ApiPropertyOptional()
    hisFechaOtorgada: string;

    @ApiPropertyOptional()
    hisComunaLic: string;

    @ApiPropertyOptional()
    hisFechaIngLic: string;

    @ApiPropertyOptional()
    hisFolio: string;

    @ApiPropertyOptional()
    hisFecRecepcion: string;

    @ApiPropertyOptional()
    hisCodOrigen: string;

    @ApiPropertyOptional()
    hisRunEscuela: string;

    @ApiPropertyOptional()
    hisLetraFolio: string;

    @ApiPropertyOptional()
    hisEstadoClaCli: string;

    @ApiPropertyOptional()
    hisEstadoFisLic: string;
    
    @ApiPropertyOptional()
    hisEstadoDigLic: string;
    
    @ApiPropertyOptional()
    hisFechaPrxCtl: string;
    
    @ApiPropertyOptional()
    hisObservaciones: string;

}