import { ApiPropertyOptional } from "@nestjs/swagger";


export class ConsultaHistoricaHVCDto {
    
    @ApiPropertyOptional()
    art29: boolean;

    @ApiPropertyOptional()
    RUN: string;

    @ApiPropertyOptional()
    idOficina?: number;

    @ApiPropertyOptional()
    idMunicipalidad?: number;

}