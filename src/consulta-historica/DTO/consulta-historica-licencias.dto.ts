import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConsultaHistoricaLicenciasDto {
    
    @ApiPropertyOptional()
    licRun: string;

    @ApiPropertyOptional()
    licTipo: string;

    @ApiPropertyOptional()
    licComunaPriLic: string;

    @ApiPropertyOptional()
    licComunaUltLic: string;

    @ApiPropertyOptional()
    licComunaTraLic: string;

    @ApiPropertyOptional()
    licFechaPriLic: string;

    @ApiPropertyOptional()
    licFechaUltLic: string;

    @ApiPropertyOptional()
    licFechaTraLic: string;

    @ApiPropertyOptional()
    licFolio: string;

    @ApiPropertyOptional()
    licFolioIni: string;

    @ApiPropertyOptional()
    licFolioTra: string;

    @ApiPropertyOptional()
    licLetraFolio: string;

    @ApiPropertyOptional()
    licLetraFolioIni: string;

    @ApiPropertyOptional()
    licLetraFolioTra: string;

    @ApiPropertyOptional()
    licEstadoClaLic: string;

    @ApiPropertyOptional()
    licEstadoFisLic: string;

    @ApiPropertyOptional()
    licEstadoDigLic: string;

    @ApiPropertyOptional()
    licFechaPrxCtl: string;

    @ApiPropertyOptional()
    licRunEscuela: string;

    @ApiPropertyOptional()
    licObservaciones: string;
}