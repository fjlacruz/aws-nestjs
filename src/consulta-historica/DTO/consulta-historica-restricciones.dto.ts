import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConsultaHistoricaRestriccionesDto {
    
    @ApiPropertyOptional()
    sbfTipoReg: string;

    @ApiPropertyOptional()
    licTipo: string;

    @ApiPropertyOptional()
    sbfSubTipo: string;

    @ApiPropertyOptional()
    sbfRun: string;

    @ApiPropertyOptional()
    sbfFechaSub: string;

    @ApiPropertyOptional()
    sbfSecuencia: string;

    @ApiPropertyOptional()
    sbfCodigo: string;

    @ApiPropertyOptional()
    sbfEstado: string;

    @ApiPropertyOptional()
    sbfDisponible: string;

    @ApiPropertyOptional()
    sbfDatos: string;

    @ApiPropertyOptional()
    sbfNroDoc: string;
}