import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConsultaHistoricaSentenciasDto {
    
    @ApiPropertyOptional()
    senJuzgado: string;

    @ApiPropertyOptional()
    senFechaResol: string;

    @ApiPropertyOptional()
    senNroProceso: string;

    @ApiPropertyOptional()
    senAnoProceso: string;

    @ApiPropertyOptional()
    senRun: string;

    @ApiPropertyOptional()
    senUnidadPolicial: string;

    @ApiPropertyOptional()
    senNumeroParte: string;

    @ApiPropertyOptional()
    senFechaDenuncia: string;

    @ApiPropertyOptional()
    senCodInf001: string;

    @ApiPropertyOptional()
    senCodInf002: string;

    @ApiPropertyOptional()
    senCodInf003: string;

    @ApiPropertyOptional()
    senCodRes001: string;
    
    @ApiPropertyOptional()
    senCodRes002: string;
    
    @ApiPropertyOptional()
    senCodRes003: string;
    
    @ApiPropertyOptional()
    senSuspencionTot: string;
    
    @ApiPropertyOptional()
    senPatente: string;
    
    @ApiPropertyOptional()
    senOrdenElim: string;
    
    @ApiPropertyOptional()
    senFechaOs: string;
    
    @ApiPropertyOptional()
    senFlagEliminado: string;
    
    @ApiPropertyOptional()
    senFechaIngreso: string;
    
    @ApiPropertyOptional()
    senFechaRecepDoc: string;
    
    @ApiPropertyOptional()
    senFechaIngSent: string;
    
    @ApiPropertyOptional()
    senTipoIngreso: string;

}