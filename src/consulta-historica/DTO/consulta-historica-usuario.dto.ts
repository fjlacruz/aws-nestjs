import { ApiPropertyOptional } from "@nestjs/swagger";

export class ConsultaHistoricaUsuarioDto {

    @ApiPropertyOptional()
    run: string;

    @ApiPropertyOptional()
    comuna: string;

    @ApiPropertyOptional()
    idUsuario: number;
}