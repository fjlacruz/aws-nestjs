import { ApiPropertyOptional } from "@nestjs/swagger";
import { ConsultaHistoricaBloqueosDto } from "./consulta-historica-bloqueos.dto";
import { ConsultaHistoricaDenegacionesDto } from "./consulta-historica-denegaciones.dto";
import { ConsultaHistoricaDuplicadosDto } from "./consulta-historica-duplicados.dto";
import { ConsultaHistoricaHistoricoDto } from "./consulta-historica-historico.dto";
import { ConsultaHistoricaLicenciasDto } from "./consulta-historica-licencias.dto";
import { ConsultaHistoricaRestriccionesDto } from "./consulta-historica-restricciones.dto";
import { ConsultaHistoricaSentenciasDto } from "./consulta-historica-sentencias.dto";

export class ConsultaHistoricaDto {

    @ApiPropertyOptional()
    idDocsRequeridoRol: number;
    
    @ApiPropertyOptional()
    nombres: string;

    @ApiPropertyOptional()
    RUN: string;

    @ApiPropertyOptional()
    Licencias: ConsultaHistoricaLicenciasDto[];

    @ApiPropertyOptional()
    Duplicados: ConsultaHistoricaDuplicadosDto[];
    
    @ApiPropertyOptional()
    Restricciones: ConsultaHistoricaRestriccionesDto[];

    @ApiPropertyOptional()
    Denegaciones: ConsultaHistoricaDenegacionesDto[];

    // @ApiPropertyOptional()
    // AnotacionesJudiciales: [];
    
    @ApiPropertyOptional()
    Bloqueos: ConsultaHistoricaBloqueosDto[];

    @ApiPropertyOptional()
    Historico: ConsultaHistoricaHistoricoDto[];
    
    @ApiPropertyOptional()
    Sentencias: ConsultaHistoricaSentenciasDto[];
       
    // @ApiPropertyOptional()
    // SentenciasAux: ConsultaHistoricaSentenciasDto[];

    @ApiPropertyOptional()
    codigo: number;
    
    @ApiPropertyOptional()
    mensaje: string;
}