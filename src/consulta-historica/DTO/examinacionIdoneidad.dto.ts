import { ApiPropertyOptional } from "@nestjs/swagger";

export class ExaminacionIdoneidadDto {

    @ApiPropertyOptional()
    idPostulante: number;
    
    @ApiPropertyOptional()
    RUN: string;

    @ApiPropertyOptional()
    archivo: string;

    @ApiPropertyOptional()
    art29:boolean;
}