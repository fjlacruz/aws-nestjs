import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { jwtConstants } from 'src/auth/constants';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { EstadosEDDEntity } from 'src/estados-EDD/entity/estados-EDD.entity';
import { EstadosEDFEntity } from 'src/estados-EDF/entity/estados-EDF.entity';
import { EstadosPCLEntity } from 'src/estados-PCL/entity/estados-PCL.entity';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { RegistroCivilConsultasService } from 'src/shared/externalServices/registroCivil/registroCivilConsultas.services';
import { SharedModule } from 'src/shared/shared.module';
import { User } from 'src/users/entity/user.entity';
import { UtilService } from 'src/utils/utils.service';
import { ConsultaHistoricaController } from './controller/consulta-historica.controller';
import { CertificadosPostulante } from './entity/certificadosPostulante.entity';
import { HVC } from './entity/hvc.entity';
import { ConsultaHistoricaService } from './service/consulta-historica.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, RolesUsuarios, Postulante, ComunasEntity, CertificadosPostulante, HVC, EstadosPCLEntity, EstadosEDDEntity, EstadosEDFEntity]),
    SharedModule,
  ],
  controllers: [ConsultaHistoricaController],
  providers: [ConsultaHistoricaService, AuthService, UtilService],
  exports: [ConsultaHistoricaService],
})
export class ConsultaHistoricaModule {}
