import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { ConsultaHistoricaService } from '../service/consulta-historica.service';
import { ConsultaHistoricaUsuarioDto } from '../DTO/consulta-historica-usuario.dto';
import { CertificadosPostulanteDto } from '../DTO/certificadoPostulante.dto';
import { ConsultaHistoricaHVCDto } from '../DTO/consulta-historica-hvc.dto';

@ApiTags('Consulta-Historica')
@Controller('consulta-historica')
export class ConsultaHistoricaController {
  constructor(private readonly docsRequeridosService: ConsultaHistoricaService, private readonly authService: AuthService) {}

  @Post('getHistorico')
  async getHistorico(@Body() datosUsuario: ConsultaHistoricaUsuarioDto, @Req() req: any) {

      const data = await this.docsRequeridosService.getHistorico(req, datosUsuario);
      return { data };

  }

  @Get('getCertificadoAntecedentes/:run/:comunaNombre')
  async getCertificadoAntecedentes(@Param('run') run: string, @Param('comunaNombre') comunaNombre: string, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AccesoConsultaHistoricaAlRNCVM]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.getCertificadoAntecedentes(run, comunaNombre);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('getCertificadoAntecedentesJuzgado/:run/:comunaNombre')
  async getCertificadoAntecedentesJuzgado(@Param('run') run: string, @Param('comunaNombre') comunaNombre: string, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AccesoConsultaHistoricaAlRNCVM]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.getCertificadoAntecedentesJuzgado(run, comunaNombre);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('getHVC/:run/:art29')
  async getHVC(@Param('run') run: string, @Param('art29') art29: boolean, @Req() req: any) {
    // let splittedBearerToken = req.headers.authorization.split(' ');
    // let token = splittedBearerToken[1];

    // let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.IngresoyEvaluacionIdoneidadMoralPreEvaluacionDescargaHVC]);

    // let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    // if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.getHVC(run, art29);
      return { data };
    // } else {
    //   return { data: validarPermisos };
    // }
  }

  @Post('guardarHVC')
  async guardarHVC(@Body() certificadosPostulanteDto: CertificadosPostulanteDto, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.IngresoyEvaluacionIdoneidadMoralPreEvaluacionDescargaHVC]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.guardarHVC(certificadosPostulanteDto);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('getHVCByPostulante/:idPostulante/:art29')
  async getHVCByPostulante(@Param('idPostulante') idPostulante: string, @Param('art29') art29: boolean, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.IngresoyEvaluacionIdoneidadMoralPreEvaluacionDescargaHVC]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.getHVCByPostulante(idPostulante, art29);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('getCertificadoObtencionRenovacion/:idPostulante/:art29')
  async getCertificadoObtencionRenovacion(@Param('idPostulante') idPostulante: string, @Param('art29') art29: boolean, @Req() req: any) {

      const data = await this.docsRequeridosService.obtenerLocalHVC(req, idPostulante, art29);

      return { data };

  }

  @Post('getCertificadoObtencionRenovacionMuniPost')
  async getCertificadoObtencionRenovacionMuniPost(@Body() consultaHistoricaHVCDto: ConsultaHistoricaHVCDto, @Req() req: any) {

      const data = await this.docsRequeridosService.getHVCPostObtencionRenovacionMuni(consultaHistoricaHVCDto, req);

      return { data };

  }

  @Post('getCertificadoObtencionRenovacionJPLPost')
  async getCertificadoObtencionRenovacionJPLPost(@Body() consultaHistoricaHVCDto: ConsultaHistoricaHVCDto, @Req() req: any) {

      const data = await this.docsRequeridosService.getHVCPostObtencionRenovacionJPL(consultaHistoricaHVCDto, req);

      return { data };

  }

}
