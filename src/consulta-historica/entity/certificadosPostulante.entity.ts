import { ApiProperty } from "@nestjs/swagger";
import { RegistroUsuarios } from "src/registro-usuarios/entity/registro-usuario.entity";
import { Roles } from "src/roles/entity/roles.entity";
import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import {Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne} from "typeorm";

@Entity('CertificadosPostulante')
export class CertificadosPostulante {

    @PrimaryGeneratedColumn()
    idCertificadosPostulante: number;

    @Column()
    idPostulante: number;
    @ManyToOne(() => PostulantesEntity, postulante => postulante.certificadosPostulante)
    @JoinColumn({ name: 'idPostulante' })
    readonly postulante: PostulantesEntity;

    @Column({
        type: 'bytea',
        nullable: false,
    })
    archivo: Buffer;
    
      
    @Column()
    art29:boolean;

    @Column()
    FechaEmision: Date;

}