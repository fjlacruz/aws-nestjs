import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, JoinColumn} from "typeorm";

@Entity('HVC')
export class HVC {

    @PrimaryGeneratedColumn()
    idHVC: number;

    @Column()
    idPostulante: number;
    //@ManyToOne(() => PostulantesEntity, postulante => postulante.HVC) -> deberia de mapearse correctamente.
    @JoinColumn({ name: 'idPostulante' })
    readonly postulante: PostulantesEntity;

    @Column()
    solicitadoPor: number;
    //@ManyToOne(() => User, usuario => usuario.HVC) -> deberia de mapearse correctamente.
    @JoinColumn({ name: 'solicitadoPor' })
    readonly userSolicitante: User;

    @Column({
        type: 'bytea',
        nullable: false,
    })
    certificado: Buffer;

    @Column()
    fechaSolicitud: Date;

    @Column()
    fechaExpiracion: Date;

    @Column()
    HVC:boolean;

    @Column()
    HVCArt29:boolean;
}