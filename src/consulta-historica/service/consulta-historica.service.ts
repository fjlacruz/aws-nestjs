import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { DeleteResult, getConnection, InsertResult, Repository, UpdateResult } from 'typeorm';
import { UtilService } from 'src/utils/utils.service';
import { ConsultaHistoricaDto } from '../DTO/consulta-historica.dto';
import { ConsultaHistoricaUsuarioDto } from '../DTO/consulta-historica-usuario.dto';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { CertificadosPostulanteDto } from '../DTO/certificadoPostulante.dto';
import { HVC } from '../entity/hvc.entity';
import { AuthService } from 'src/auth/auth.service';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { CertificadosPostulante } from '../entity/certificadosPostulante.entity';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';
import { RegistroCivilConsultasService } from 'src/shared/externalServices/registroCivil/registroCivilConsultas.services';
import { EstadosPCLEntity } from 'src/estados-PCL/entity/estados-PCL.entity';
import { EstadosEDFEntity } from 'src/estados-EDF/entity/estados-EDF.entity';
import { EstadosEDDEntity } from 'src/estados-EDD/entity/estados-EDD.entity';
import { PermisosNombres, tipoRolOficinaID } from 'src/constantes';
import { ConsultaHistoricaHVCDto } from '../DTO/consulta-historica-hvc.dto';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { BASE_URL_REGISTRO_CIVIL, URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES, URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES_JUZGADO } from 'src/approutes.config';

@Injectable()
export class ConsultaHistoricaService {
  constructor(
    private readonly utilService: UtilService,
    @InjectRepository(Postulante) private readonly postulanteRepository: Repository<Postulante>,
    @InjectRepository(EstadosPCLEntity) private readonly EstadosPCLRepository: Repository<EstadosPCLEntity>,
    @InjectRepository(EstadosEDFEntity) private readonly EstadosDocFisicoRepository: Repository<EstadosEDFEntity>,
    @InjectRepository(EstadosEDDEntity) private readonly EstadosDocDigitalRepository: Repository<EstadosEDDEntity>,
    @InjectRepository(ComunasEntity) private readonly ComunasRepository: Repository<ComunasEntity>,
    @InjectRepository(HVC) private readonly HVCRepository: Repository<HVC>,
    @InjectRepository(CertificadosPostulante) private readonly certificadosPostulanteRepository: Repository<CertificadosPostulante>,
    private authService: AuthService,
    private registroCivilConsultasService: RegistroCivilConsultasService
  ) {}

  async getHistorico(req:any, datosUsuario: ConsultaHistoricaUsuarioDto): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    try {

      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AccesoConsultaHistoricaAlRNCVM);
    
      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }


      const idUsuario = (await this.authService.usuario()).idUsuario;
      const estadosPCL = await this.EstadosPCLRepository.find();
      const estadosDocFisico = await this.EstadosDocFisicoRepository.find();
      const estadosDocDigital = await this.EstadosDocDigitalRepository.find();
      const comunas = await this.ComunasRepository.find();

      const RUN = datosUsuario.run;
      datosUsuario.run = datosUsuario.run.replace('-', '');
      datosUsuario.idUsuario = idUsuario;

      // LLamada a Registro Civil
      const respuestaRegCivil = await this.registroCivilConsultasService.consulHis(datosUsuario);

      if (respuestaRegCivil.ResultadoOperacion) {
        const consultaHistoricaDto = respuestaRegCivil.Respuesta;

        // Transformacion de los datos
        consultaHistoricaDto.RUN = RUN;
        consultaHistoricaDto.respuesta.Licencias?.forEach((licencia, i) => {
          // Fechas
          if (licencia.licFechaTraLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licFechaTraLic = this.transformarFecha(licencia.licFechaTraLic);
          }

          if (licencia.licFechaUltLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licFechaUltLic = this.transformarFecha(licencia.licFechaUltLic);
          }

          if (licencia.licFechaPriLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licFechaPriLic = this.transformarFecha(licencia.licFechaPriLic);
          }

          // PCL
          if (licencia.licEstadoClaLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licEstadoClaLic = this.transformarEstado(licencia.licEstadoClaLic, estadosPCL);
          }

          // EDF
          if (licencia.licEstadoDigLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licEstadoDigLic = this.transformarEstado(
              licencia.licEstadoDigLic,
              estadosDocDigital
            );
          }

          // EDD
          if (licencia.licEstadoFisLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licEstadoFisLic = this.transformarEstado(
              licencia.licEstadoFisLic,
              estadosDocFisico
            );
          }

          // Comunas
          if (licencia.licComunaPriLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licComunaPriLic = this.transformarComuna(licencia.licComunaPriLic, comunas);
          }

          if (licencia.licComunaTraLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licComunaTraLic = this.transformarComuna(licencia.licComunaTraLic, comunas);
          }

          if (licencia.licComunaUltLic > 0) {
            consultaHistoricaDto.respuesta.Licencias[i].licComunaUltLic = this.transformarComuna(licencia.licComunaUltLic, comunas);
          }
        });

        consultaHistoricaDto.respuesta.Historico?.forEach((historico, i) => {
          // Comunas
          if (historico.hisComunaLic) {
            consultaHistoricaDto.respuesta.Historico[i].hisComunaLic = this.transformarComuna(historico.hisComunaLic, comunas);
          }

          // Fechas
          if (historico.hisFechaIngLic) {
            consultaHistoricaDto.respuesta.Historico[i].hisFechaIngLic = this.transformarFecha(historico.hisFechaIngLic);
          }

          if (historico.hisComunaLic) {
            consultaHistoricaDto.respuesta.Historico[i].hisFechaOtorgada = this.transformarFecha(historico.hisFechaOtorgada);
          }

          // PCL
          if (historico.hisEstadoClaCli) {
            consultaHistoricaDto.respuesta.Historico[i].hisEstadoClaCli = this.transformarEstado(historico.hisEstadoClaCli, estadosPCL);
          }

          if (historico.hisEstadoDigLic) {
            consultaHistoricaDto.respuesta.Historico[i].hisEstadoDigLic = this.transformarEstado(
              historico.hisEstadoDigLic,
              estadosDocDigital
            );
          }

          if (historico.hisEstadoFisLic) {
            consultaHistoricaDto.respuesta.Historico[i].hisEstadoFisLic = this.transformarEstado(
              historico.hisEstadoFisLic,
              estadosDocFisico
            );
          }
        });

        consultaHistoricaDto.respuesta.Restricciones?.forEach((restriccion, i) => {
          if (restriccion.sbfFechaSub > 0) {
            consultaHistoricaDto.respuesta.Restricciones[i].sbfFechaSub = this.transformarFecha(restriccion.sbfFechaSub);
          }
        });

        consultaHistoricaDto.respuesta.Sentencias?.forEach((sentencia, i) => {
          if (sentencia.senFechaResol > 0) {
            consultaHistoricaDto.respuesta.Sentencias[i].senFechaResol = this.transformarFecha(sentencia.senFechaResol);
          }

          if (sentencia.senFechaDenuncia > 0) {
            consultaHistoricaDto.respuesta.Sentencias[i].senFechaDenuncia = this.transformarFecha(sentencia.senFechaDenuncia);
          }
        });

        consultaHistoricaDto.respuesta.AnotacionesJudiciales?.forEach((sentencia, i) => {
          if (sentencia.sbfFechaSub > 0) {
            consultaHistoricaDto.respuesta.AnotacionesJudiciales[i].sbfFechaSub = this.transformarFecha(sentencia.sbfFechaSub);
          }
        });

        resultado.Respuesta = consultaHistoricaDto;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Obtenido correctamente el historico';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error obteniendo el historico';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Ha ocurrido un error';
    }

    return resultado;
  }

  async getCertificadoAntecedentes(run: string, comunaNombre: string): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    try {
      const idUsuario = (await this.authService.usuario()).idUsuario;

      let datosUsuario = new ConsultaHistoricaUsuarioDto();
      datosUsuario.run = run.replace('-', '');
      datosUsuario.comuna = comunaNombre;
      datosUsuario.idUsuario = idUsuario;

      // LLamada a Registro Civil
      const respuestaRegCivil = await this.historicoRegistroCivil(datosUsuario, URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES);

      if (respuestaRegCivil.ResultadoOperacion) {
        const documentoBase64 = respuestaRegCivil.Respuesta;

        resultado.Respuesta = documentoBase64;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Obtenido correctamente el certificado de antecedentes';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error obteniendo el certificado de antecedentes';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo el certificado de antecedentes';
    }

    return resultado;
  }

  async getCertificadoAntecedentesJuzgado(run: string, comunaNombre: string): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    try {
      const idUsuario = (await this.authService.usuario()).idUsuario;

      let datosUsuario = new ConsultaHistoricaUsuarioDto();
      datosUsuario.run = run.replace('-', '');
      datosUsuario.comuna = comunaNombre;
      datosUsuario.idUsuario = idUsuario;

      // LLamada a Registro Civil
      const respuestaRegCivil = await this.historicoRegistroCivil(
        datosUsuario,
        URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES_JUZGADO
      );

      if (respuestaRegCivil.ResultadoOperacion) {
        const documentoBase64 = respuestaRegCivil.Respuesta;

        resultado.Respuesta = documentoBase64;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Obtenido correctamente el certificado de antecedentes de juzgado';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error obteniendo el certificado de antecedentes de juzgado';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo el certificado de antecedentes de juzgado';
    }

    return resultado;
  }

  // Llamada al back de Registro Civil, se le pasa por parametros la URL a la que llamará
  async historicoRegistroCivil(consultaHistoricaUsuarioDto: ConsultaHistoricaUsuarioDto, pathURL: string): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    const request = require('request');

    const tokenEncriptado = this.utilService.encriptar();

    try {
      return new Promise(function (resolve, reject) {
        request.post(
          BASE_URL_REGISTRO_CIVIL + pathURL,
          { json: consultaHistoricaUsuarioDto, headers: { Authorization: tokenEncriptado } },
          function (error, res, body) {
            if (!error && res.statusCode == 200) {
              if (
                pathURL == URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES ||
                pathURL == URL_REGISTRO_CIVIL_CERTIFICADO_ANTECEDENTES_JUZGADO
              ){
                if(body.errorMessage){
                  resultado.Error = 'Error conectando a registro civil';
                  resultado.ResultadoOperacion = false;

                  resolve(resultado);
                }
                else{
                  resultado.Respuesta = body.respuesta.documento;
                  resultado.ResultadoOperacion = true;
                  
                  resolve(resultado);
                }
              }

              else {
                resultado.Respuesta = body.respuesta;
                resultado.ResultadoOperacion = true;

                resolve(resultado);
              }
            } else {
              console.log(error);
              resultado.Error = error ? error : 'Error conectando a registro civil';
              resultado.ResultadoOperacion = false;

              resolve(resultado);
            }
          }
        );
      });
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error conectando a registro civil';
      resultado.ResultadoOperacion = false;
      return resultado;
    }
  }

  async getHVC(run: string, art29: any) {
    const resultado: Resultado = new Resultado();
    var pathURL = '';
    if (art29 == 'true') {
      pathURL = '/api/certificadoantecedentesjuzgado';
    } else {
      pathURL = '/api/certificadoantecedentes';
    }
    try {
      // Buscamos la comuna del postulante
      const postulante = await this.postulanteRepository
        .createQueryBuilder('Postulante')
        .innerJoinAndMapOne('Postulante.comunas', ComunasEntity, 'comunas', 'Postulante.idComuna = comunas.idComuna')
        .andWhere('Postulante.RUN = :run', { run: run.substring(0, run.length - 1) })
        .andWhere('Postulante.DV = :dv', { dv: run.substring(run.length - 1, run.length) })
        .getOne();

      let consultaHistoricaDto: ConsultaHistoricaUsuarioDto = {
        run: run,
        comuna: postulante.comunas.Nombre,
        idUsuario: 0,
      };

      // Obtenemos el documento de Registro Civil
      let resDocumentoRegistroCivil = await this.historicoRegistroCivil(consultaHistoricaDto, pathURL);

      // Si se ha encontrado el documento, lo guardamos en nuestra bbdd
      if (
        resDocumentoRegistroCivil.ResultadoOperacion &&
        resDocumentoRegistroCivil.Respuesta != undefined &&
        resDocumentoRegistroCivil.Respuesta != ''
      ) {
        const certificadosPostulanteDto: CertificadosPostulanteDto = {
          idPostulante: postulante.idPostulante,
          archivo: resDocumentoRegistroCivil.Respuesta,
          RUN: run,
          art29: art29,
          isArt29: art29.toString(),
          RUNCompleto: run,
          documento: null,
        };

        let resGuardarDocumento = await this.guardarHVC(certificadosPostulanteDto);

        if (resGuardarDocumento.ResultadoOperacion) {
          resultado.ResultadoOperacion = true;
          resultado.Respuesta = { documento: resDocumentoRegistroCivil.Respuesta };
        } else {
          resultado.Mensaje = 'La conexion con  Registro Civil fué exitosa pero no se ha podido guardar el documento correctamente.';
        }
      } else {
        resultado.Mensaje = 'La conexion con  Registro Civil fué exitosa pero no se ha podido recuperar el documento correctamente.';
      }
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error obteniendo certificado para el postulante';
    }

    return resultado;
  }

  async obtenerLocalHVC(req: any, run: string, art29: any) {
    var isArt29 = false;
    if (art29 == 'true') {
      var isArt29 = true;
    }
    const resultado: Resultado = new Resultado();

    try {
      // Comprobamos permisos
      // let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.IngresoyEvaluacionIdoneidadMoralPreEvaluacionDescargaHVC);

      // // En caso de que el usuario no sea correcto
      // if (!usuarioValidado.ResultadoOperacion) {
      //     return usuarioValidado;
      // }
      


      const hace30Dias = new Date(new Date().setDate(new Date().getDate() - 30));

      const postulante = await this.postulanteRepository
        .createQueryBuilder('Postulantes')
        .andWhere('Postulantes.RUN = :run', { run: run.substring(0, run.length - 1) })
        .andWhere('Postulantes.DV = :dv', { dv: run.substring(run.length - 1, run.length) })
        .getOne();

      const certificadoEncontrado: CertificadosPostulante = await this.certificadosPostulanteRepository
        .createQueryBuilder('Certificados')
        .innerJoinAndMapMany('Certificados.postulante', Postulante, 'postulante', 'Certificados.idPostulante = postulante.idPostulante')
        .andWhere('postulante.idPostulante = :id', { id: postulante.idPostulante })
        .andWhere('Certificados.art29 = :art29', { art29: isArt29 })
        .andWhere('Certificados.FechaEmision > :hace30Dias', { hace30Dias: hace30Dias })
        .orderBy('Certificados.FechaEmision', 'DESC')
        .getOne();

      let certificadoPostulanteDto: CertificadosPostulanteDto = new CertificadosPostulanteDto();

      if (certificadoEncontrado) {
        certificadoPostulanteDto.RUNCompleto =
          certificadoEncontrado.postulante[0].RUN.toString() + '-' + certificadoEncontrado.postulante[0].DV;
        //certificadoPostulanteDto.archivo = certificadoEncontrado.archivo.toString();
        certificadoPostulanteDto.isArt29 = certificadoEncontrado.art29;
        certificadoPostulanteDto.documento = certificadoEncontrado.archivo.toString();
        resultado.Respuesta = certificadoPostulanteDto;
      } else {
        resultado.Respuesta = null;
        resultado.Mensaje = 'No se ha encontrado el documento';
      }

      resultado.ResultadoOperacion = true;
    } catch (err) {
      resultado.Error = err;
    }

    return resultado;
  }

  async obtenerLocalHVCIdonMoral(req: any, run: string, art29: any) {
    var isArt29 = false;
    if (art29 == 'true') {
      var isArt29 = true;
    }
    const resultado: Resultado = new Resultado();

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.IngresoyEvaluacionIdoneidadMoralDescargaHVC);

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
          return usuarioValidado;
      }
      


      const hace30Dias = new Date(new Date().setDate(new Date().getDate() - 30));

      const postulante = await this.postulanteRepository
        .createQueryBuilder('Postulantes')
        .andWhere('Postulantes.RUN = :run', { run: run.substring(0, run.length - 1) })
        .andWhere('Postulantes.DV = :dv', { dv: run.substring(run.length - 1, run.length) })
        .getOne();

      const certificadoEncontrado: CertificadosPostulante = await this.certificadosPostulanteRepository
        .createQueryBuilder('Certificados')
        .innerJoinAndMapMany('Certificados.postulante', Postulante, 'postulante', 'Certificados.idPostulante = postulante.idPostulante')
        .andWhere('postulante.idPostulante = :id', { id: postulante.idPostulante })
        .andWhere('Certificados.art29 = :art29', { art29: isArt29 })
        .andWhere('Certificados.FechaEmision > :hace30Dias', { hace30Dias: hace30Dias })
        .orderBy('Certificados.FechaEmision', 'DESC')
        .getOne();

      let certificadoPostulanteDto: CertificadosPostulanteDto = new CertificadosPostulanteDto();

      if (certificadoEncontrado) {
        certificadoPostulanteDto.RUNCompleto =
          certificadoEncontrado.postulante[0].RUN.toString() + '-' + certificadoEncontrado.postulante[0].DV;
        //certificadoPostulanteDto.archivo = certificadoEncontrado.archivo.toString();
        certificadoPostulanteDto.isArt29 = certificadoEncontrado.art29;
        certificadoPostulanteDto.documento = certificadoEncontrado.archivo.toString();
        resultado.Respuesta = certificadoPostulanteDto;
      } else {
        resultado.Respuesta = null;
        resultado.Mensaje = 'No se ha encontrado el documento';
      }

      resultado.ResultadoOperacion = true;
    } catch (err) {
      resultado.Error = err;
    }

    return resultado;
  }  

  async guardarHVC(certificadosPostulanteDto: CertificadosPostulanteDto) {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      //let resultadoCertificadoLocal = await this.obtenerLocalHVC(certificadosPostulanteDto.RUN, certificadosPostulanteDto.art29)

      // if (resultadoCertificadoLocal.Respuesta != undefined) {

      //   const certificadoBorrado = await queryRunner.manager.delete(CertificadosPostulante, resultadoCertificadoLocal.Respuesta.idCertificadosPostulante);

      //   if (certificadoBorrado.affected == 0) {
      //     resultado.Error = 'Error borrando el certificado anterior';
      //     await queryRunner.rollbackTransaction();
      //     await queryRunner.release();
      //     return resultado;
      //   }
      // }

      // No hace falta borrar el certificado anterior

      const certificadosPostulante = new CertificadosPostulante();
      certificadosPostulante.archivo = Buffer.from(certificadosPostulanteDto.archivo);
      if (certificadosPostulanteDto.art29 == 'true') {
        certificadosPostulante.art29 = true;
      } else {
        certificadosPostulante.art29 = false;
      }
      certificadosPostulante.FechaEmision = new Date();
      certificadosPostulante.idPostulante = certificadosPostulanteDto.idPostulante;

      const certificadoNuevo = await queryRunner.manager.insert(CertificadosPostulante, certificadosPostulante);

      if (certificadoNuevo.identifiers.length > 0) {
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Certificado guardado correctamente';
        await queryRunner.commitTransaction();
        await queryRunner.release();
      } else {
        resultado.Mensaje = 'Error guardando el documento';
        resultado.Error = 'Error guardando el documento';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
      }
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error obteniendo certificado para el postulante';
      await queryRunner.rollbackTransaction();
      await queryRunner.release();
    }

    return resultado;
  }

  async getHVCByPostulante(idPostulante: string, art29: boolean) {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      const certificado = await this.HVCRepository.findOne({
        where: qb => {
          qb.where('HVC.idPostulante = :idPostulante', { idPostulante: idPostulante }).andWhere('HVC.HVCArt29 = :Art29', { Art29: art29 });
        },
        order: { fechaExpiracion: 'DESC' },
      });

      if (certificado == undefined) {
        resultado.Error = 'No se encontro certificado para el postulante';
      } else {
        resultado.ResultadoOperacion = true;
        resultado.Respuesta = { documento: certificado.certificado.toString() };
      }
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error obteniendo certificado para el postulante';
    }

    return resultado;
  }

  private transformarFecha(fecha) {
    let anho = fecha.substring(0, 4);
    let mes = fecha.substring(4, 6);
    let dia = fecha.substring(6, 8);

    return dia + '/' + mes + '/' + anho;
  }

  private transformarEstado(codigo, ListPCL: any[]) {
    let estado = ListPCL.find(x => x.CodigoRC == codigo)?.Nombre;

    return estado ? estado : 'Estado no registrado';
  }

  private transformarComuna(codigo, ListComunas: any[]) {
    let comuna = ListComunas.find(x => x.CodigoRCN == codigo)?.Nombre;

    return comuna ? comuna : 'Comuna no registrada en el sistema';
  }

  async getHVCPostObtencionRenovacionMuni(consultaHVC : ConsultaHistoricaHVCDto, req){
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AccesoConsultaHistoricaUsoExclusivoObtencion);

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
          return usuarioValidado;
      }

      return this.getHVCPost(consultaHVC, usuarioValidado);
  }

  async getHVCPostObtencionRenovacionJPL(consultaHVC : ConsultaHistoricaHVCDto, req){
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AccesoConsultaHistoricaUsoExclusivoObtencionJPL);

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
          return usuarioValidado;
      }  
      
      return this.getHVCPost(consultaHVC, usuarioValidado);
  }

  private async getHVCPost(consultaHVC : ConsultaHistoricaHVCDto, usuarioValidado: Resultado) {
    const resultado: Resultado = new Resultado();


    try {
      
      // // Comprobamos permisos
      // let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.IngresoyEvaluacionIdoneidadMoralPreEvaluacionDescargaHVC);

      // // En caso de que el usuario no sea correcto
      // if (!usuarioValidado.ResultadoOperacion) {
      //     return usuarioValidado;
      // }

      const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];
      
      let nombreComunaConsulta : string = '';
      
      // En el caso de que el usuario sea administrador, no hacemos comprobación de oficina,
      // En el caso de no ser administrador, debemos hacer la comprobación de que la oficina pertenece
      // a la municipalidad que se consulta
      if(!this.authService.checkUserSuperAdmin(rolesUsuarioRecuperados)){
        // Se entraría a consultar si la oficina pertenece al usuario para la consulta que se realiza

        //En este momento se va a recuperar para la primera oficina recuperada del usuario según sus roles
        const rolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRol == tipoRolOficinaID);

        if(!rolOficina && rolOficina.length < 1){
          resultado.Mensaje = 'Error, no existe rol asociado a oficina.';
          resultado.Error = 'Error, no existe rol asociado a oficina.';
          resultado.ResultadoOperacion = false;
    
          return resultado;  
        }

        const comunaXMunicipalidadXOficina = await this.ComunasRepository
        .createQueryBuilder('Comunas')
        .innerJoinAndMapOne('Comunas.instituciones', InstitucionesEntity, 'instituciones', 'Comunas.idComuna = instituciones.idComuna')
        .innerJoinAndMapOne('instituciones.oficinas', OficinasEntity, 'oficinas', 'instituciones.idInstitucion = oficinas.idInstitucion')
        .where('oficinas.idOficina = :_idOficina', {_idOficina: rolOficina[0].idOficina})
        .getOne();   
        
        nombreComunaConsulta = comunaXMunicipalidadXOficina.instituciones.Nombre;

      }
      else{
        // En el caso de administrador, debemos recuperar la comuna mediante la municipalidad facilitada
        if(!consultaHVC.idMunicipalidad){
          resultado.Mensaje = 'Error, la municipalidad no ha sido informada.';
          resultado.Error = 'Error, la municipalidad no ha sido informada.';
          resultado.ResultadoOperacion = false;
    
          return resultado;        
        }        

        const comunaXMunicipalidad = await this.ComunasRepository
        .createQueryBuilder('Comunas')
        .innerJoinAndMapOne('Comunas.instituciones', InstitucionesEntity, 'instituciones', 'Comunas.idComuna = instituciones.idComuna')
        .where('instituciones.idInstitucion = :_idInstitucion', {_idInstitucion: consultaHVC.idMunicipalidad})
        .getOne();

        if(!comunaXMunicipalidad){
          resultado.Mensaje = 'Error, la municipalidad no existe en el sistema.';
          resultado.Error = 'Error, la municipalidad no existe en el sistema.';
          resultado.ResultadoOperacion = false;
    
          return resultado;        
        }

        nombreComunaConsulta = comunaXMunicipalidad.instituciones.Nombre;
      }

      // Buscamos en bbdd el documento

      const hace30Dias = new Date(new Date().setDate(new Date().getDate() - 30));

      const postulante = await this.postulanteRepository
        .createQueryBuilder('Postulantes')
        .innerJoinAndMapOne('Postulantes.comunas', ComunasEntity, 'comunas', 'Postulantes.idComuna = comunas.idComuna')
        .andWhere('Postulantes.RUN = :run', { run: consultaHVC.RUN.substring(0, consultaHVC.RUN.length - 1) })
        .andWhere('Postulantes.DV = :dv', { dv: consultaHVC.RUN.substring(consultaHVC.RUN.length - 1, consultaHVC.RUN.length) })
        .getOne();

      if(!postulante){
        resultado.Mensaje = 'Error, el postulante no está registrado en el sistema.';
        resultado.Error = 'Error, el postulante no está registrado en el sistema.';
        resultado.ResultadoOperacion = false;
  
        return resultado;        
      }

      const certificadoEncontrado: CertificadosPostulante = await this.certificadosPostulanteRepository
        .createQueryBuilder('Certificados')
        .innerJoinAndMapMany('Certificados.postulante', Postulante, 'postulante', 'Certificados.idPostulante = postulante.idPostulante')
        .andWhere('postulante.idPostulante = :id', { id: postulante.idPostulante })
        .andWhere('Certificados.art29 = :art29', { art29: consultaHVC.art29 })
        .andWhere('Certificados.FechaEmision > :hace30Dias', { hace30Dias: hace30Dias })
        .orderBy('Certificados.FechaEmision', 'DESC')
        .getOne();

      let certificadoPostulanteDto: CertificadosPostulanteDto = new CertificadosPostulanteDto();

      if (certificadoEncontrado) {

        certificadoPostulanteDto.RUNCompleto =
        certificadoEncontrado.postulante[0].RUN.toString() + '-' + certificadoEncontrado.postulante[0].DV;

        certificadoPostulanteDto.isArt29 = certificadoEncontrado.art29;
        certificadoPostulanteDto.documento = certificadoEncontrado.archivo.toString();

        resultado.Respuesta = certificadoPostulanteDto;

        resultado.ResultadoOperacion = true;

        return resultado;
      } 

            

      let consultaHistoricaDto: ConsultaHistoricaUsuarioDto = {
        run: consultaHVC.RUN,
        comuna: nombreComunaConsulta,
        idUsuario: rolesUsuarioRecuperados[0].idUsuario,
      };

      var pathURL = '';

      if (consultaHVC.art29 == true) {
        pathURL = '/api/certificadoantecedentesjuzgado';
      } else {
        pathURL = '/api/certificadoantecedentes';
      }      

      // Obtenemos el documento de Registro Civil
      let resDocumentoRegistroCivil = await this.historicoRegistroCivil(consultaHistoricaDto, pathURL);

      // Si se ha encontrado el documento, lo guardamos en nuestra bbdd
      if (
        resDocumentoRegistroCivil.ResultadoOperacion &&
        resDocumentoRegistroCivil.Respuesta != undefined &&
        resDocumentoRegistroCivil.Respuesta != ''
      ) {
        const certificadosPostulanteDto: CertificadosPostulanteDto = {
          idPostulante: postulante.idPostulante,
          archivo: resDocumentoRegistroCivil.Respuesta,
          RUN: consultaHVC.RUN,
          art29: consultaHVC.art29.toString(),
          isArt29: consultaHVC.art29,
          RUNCompleto: consultaHVC.RUN,
          documento: null,
        };

        let resGuardarDocumento = await this.guardarHVC(certificadosPostulanteDto);

        if (resGuardarDocumento.ResultadoOperacion) {
          resultado.ResultadoOperacion = true;
          resultado.Respuesta = { documento: resDocumentoRegistroCivil.Respuesta };

          return resultado;
        } else {
          resultado.Mensaje = 'La conexion con Registro Civil fué exitosa pero no se ha podido guardar el documento correctamente.';
          resultado.ResultadoOperacion = false;

          return resultado;
        }
      } else {
        resultado.Mensaje = 'No se pudo realizar la conexión con Registro Civil para la recuperación de los datos del postulante.';
        resultado.ResultadoOperacion = false;

        return resultado;
      }
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error obteniendo certificado para el postulante';
      resultado.ResultadoOperacion = false;

      return resultado;
    }

  }  
}
