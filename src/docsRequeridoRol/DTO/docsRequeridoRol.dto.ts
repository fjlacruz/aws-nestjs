import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocsRolesUsuarioEntity } from "src/documentos-usuarios/entity/DocsRolesUsuario.entity";
import { RolesUsuarios } from "src/roles-usuarios/entity/roles-usuarios.entity";

export class DocsRequeridoRolDto {

    @ApiPropertyOptional()
    idDocsRequeridoRol: number;
    
    @ApiPropertyOptional()
    nombreDoc: string;

    @ApiPropertyOptional()
    descripcion: string;

    @ApiPropertyOptional()
    obligatorio: boolean;

    @ApiPropertyOptional()
    asignado: boolean;

    @ApiPropertyOptional()
    activo: boolean;
}