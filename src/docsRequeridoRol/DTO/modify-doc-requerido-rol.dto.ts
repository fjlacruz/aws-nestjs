import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class ModifyDocRequeridoRol {
  @ApiProperty({ name: 'nombre', required: true, nullable: false, example: 'Prueba' })
  @IsNotEmpty({ message: 'nombre es obligatorio' })
  @IsString({ message: 'nombre debe ser un string' })
  nombre: string;

  @ApiProperty({ name: 'descripción', required: true, nullable: false, example: 'Descripción de prueba' })
  @IsNotEmpty({ message: 'descripción es obligatorio' })
  @IsString({ message: 'descripción debe ser un string' })
  descripcion: string;

  @ApiProperty({ name: 'obligatorio', required: true, nullable: false, example: 'false' })
  @IsNotEmpty({ message: 'obligatorio es obligatorio' })
  @IsBoolean({ message: 'obligatorio debe ser un boolean' })
  obligatorio: boolean;

  @ApiProperty({ name: 'activo', required: false, nullable: true, example: 'false' })
  @IsBoolean({ message: 'activo debe ser un boolean' })
  activo: boolean;


}
