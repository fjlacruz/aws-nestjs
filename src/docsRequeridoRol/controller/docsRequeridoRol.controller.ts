import { Resultado } from './../../utils/resultado';
import { Body, Controller, Get, Param, Post, Req, UsePipes, ValidationPipe, ParseIntPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { ModifyTipoDocumentoDto } from 'src/tipos-documento/DTO/modify-tipo-documento.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { DocsRequeridoRolService } from '../service/docsRequeridoRol.service';
import { ModifyDocRequeridoRol } from '../DTO/modify-doc-requerido-rol.dto';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';

@ApiTags('Servicios-DocsRequeridoRol')
@Controller('docsRequeridoRol')
export class DocsRequeridoRolController {
  constructor(private readonly docsRequeridosService: DocsRequeridoRolService, private readonly authService: AuthService) {}

  @Get('getDocsRequeridos')
  async getDocsRequeridos(@Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.getDocsRequeridos();
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }
    @Get('getDocsRol/:id')
  async getDocsRol(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    // let splittedBearerToken = req.headers.authorization.split(' ');
    // let token = splittedBearerToken[1];

    // let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    // let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    // if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.getDocsRol(id);
      return { data };
    // } else {
    //   return { data: validarPermisos };
    // }
  }

  @Get(':id')
  @UsePipes(ValidationPipe)
  public async getDocRequeridoById(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.docsRequeridosService.getDocRequeridoById(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('getDocsRequeridosPaginados')
  async getDocsRequeridosPaginados(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.docsRequeridosService.getDocsRequeridosPaginados(req, paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('crearDocRequerido')
  @UsePipes(ValidationPipe)
  public async create(@Req() req, @Body() createDocRequerido: ModifyDocRequeridoRol): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosCrearRolesGruposdePermisosTipodeDocumentos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.docsRequeridosService.create(createDocRequerido);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('editarDocRequerido/:id')
  @UsePipes(ValidationPipe)
  public async update(
    @Req() req,
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDocRequerido: ModifyDocRequeridoRol
  ): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.docsRequeridosService.update(id, updateDocRequerido);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('eliminarDocRequerido/:id')
  @UsePipes(ValidationPipe)
  public async remove(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.docsRequeridosService.remove(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }
}
