import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { jwtConstants } from 'src/auth/constants';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { SharedModule } from 'src/shared/shared.module';
import { User } from 'src/users/entity/user.entity';
import { DocsRequeridoRolController } from './controller/docsRequeridoRol.controller';
import { DocsRequeridoRol } from './entity/docsRequeridoRol.entity';
import { DocsRequeridoRolService } from './service/docsRequeridoRol.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([DocsRequeridoRol, User, RolesUsuarios]),
    SharedModule,
  ],
  controllers: [DocsRequeridoRolController],
  providers: [DocsRequeridoRolService, AuthService],
  exports: [DocsRequeridoRolService],
})
export class DocsRequeridoRolModule {}
