import { Roles } from "src/roles/entity/roles.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne} from "typeorm";
import { DocsRequeridoRol } from "./docsRequeridoRol.entity";

@Entity('DocsRolesUsuario')
export class DocsRolesUsuario {

    @PrimaryGeneratedColumn()
    idDocsRolesUsuario: number;

    @Column()
    idRol: number;
    @ManyToOne(() => Roles, roles => roles.docsRolesUsuario)
    @JoinColumn({ name: 'idRol' })
    readonly roles: Roles;

    @Column()
    idUsuario: number;
    @ManyToOne(() => User, usuario => usuario.docsRolesUsuario)
    @JoinColumn({ name: 'idUsuario' })
    readonly usuario: User;

    @Column()
    idDocsRequeridoRol: number;
    @JoinColumn({ name: 'idDocsRequeridoRol' })
    @ManyToOne(() => DocsRequeridoRol, docsRequeridoRol => docsRequeridoRol.docsRolesUsuario)
    readonly docsRequeridoRol: DocsRequeridoRol;

    @Column()
    nombreDoc:string;

    @Column({
        name: 'archivo',
        type: 'bytea',
        nullable: false,
    })
    archivo: Buffer;

    @Column()
    created_at:Date;
}
