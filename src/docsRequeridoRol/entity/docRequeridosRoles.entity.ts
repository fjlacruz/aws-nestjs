import { ApiProperty } from "@nestjs/swagger";
import { RegistroUsuarios } from "src/registro-usuarios/entity/registro-usuario.entity";
import { Roles } from "src/roles/entity/roles.entity";
import {Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne} from "typeorm";
import { DocsRequeridoRol } from "./docsRequeridoRol.entity";

@Entity('DocRequeridosRoles')
export class DocRequeridosRoles {

    @PrimaryGeneratedColumn()
    idDocRequeridosRoles: number;

    @Column()
    idRol: number;
    @JoinColumn({ name: 'idRol' })
    @ManyToOne(() => Roles, roles => roles.docRequeridosRoles)
    readonly roles: Roles;

    @Column()
    idDocsRequeridoRol: number;
    @JoinColumn({ name: 'idDocsRequeridoRol' })
    @ManyToOne(() => DocsRequeridoRol, docsRequeridoRol => docsRequeridoRol.docRequeridosRoles)
    readonly docsRequeridoRol: DocsRequeridoRol;
}