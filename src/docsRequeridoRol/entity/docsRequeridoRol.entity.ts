import { ApiProperty } from '@nestjs/swagger';
import { RegistroUsuarios } from 'src/registro-usuarios/entity/registro-usuario.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { DocRequeridosRoles } from './docRequeridosRoles.entity';
import { DocsRolesUsuario } from './DocsRolesUsuario.entity';

@Entity('DocsRequeridoRol')
export class DocsRequeridoRol {
  @PrimaryGeneratedColumn()
  idDocsRequeridoRol: number;

  @Column({ name: 'nombreDoc' })
  nombre: string;

  @Column()
  descripcion: string;

  @Column()
  obligatorio: boolean;

  @Column()
  activo: boolean;

  @ApiProperty()
  @OneToMany(() => DocsRolesUsuario, docsRolesUsuario => docsRolesUsuario.docsRequeridoRol)
  readonly docsRolesUsuario: DocsRolesUsuario[];

  @ApiProperty()
  @OneToMany(() => DocRequeridosRoles, docRequeridosRoles => docRequeridosRoles.docsRequeridoRol)
  readonly docRequeridosRoles: DocRequeridosRoles[];
}
