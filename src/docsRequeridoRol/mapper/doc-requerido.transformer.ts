import { Exclude, Expose } from 'class-transformer';
import { DocRequeridosRoles } from '../entity/docRequeridosRoles.entity';
import { DocsRolesUsuario } from '../entity/DocsRolesUsuario.entity';

@Exclude()
export class DocRequeridoTransformer {
  @Expose({ groups: ['all', 'toShow'] })
  idDocsRequeridoRol: number;

  @Expose({ groups: ['all', 'toShow'] })
  nombre: string;

  @Expose({ groups: ['all', 'toShow'] })
  descripcion: string;

  @Expose({ groups: ['all', 'toShow'] })
  obligatorio: boolean;

  @Expose({ groups: ['all', 'toShow'] })
  activo: boolean;

  @Expose({ groups: ['all'] })
  docsRolesUsuario: DocsRolesUsuario[];

  @Expose({ groups: ['all'] })
  docRequeridosRoles: DocRequeridosRoles[];
}
