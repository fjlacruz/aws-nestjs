import { plainToClass } from 'class-transformer';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { DocsRequeridoRol } from '../entity/docsRequeridoRol.entity';
import { DocsRequeridoRolDto } from '../DTO/docsRequeridoRol.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { ColumnasDocsRequeridoRol, PermisosNombres } from 'src/constantes';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { SetResultadoService } from 'src/shared/services/set-resultado/set-resultado.service';
import { DocRequeridoTransformer } from '../mapper/doc-requerido.transformer';
import { ModifyDocRequeridoRol } from '../DTO/modify-doc-requerido-rol.dto';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class DocsRequeridoRolService {
  constructor(
    @InjectRepository(DocsRequeridoRol) private readonly docsRequeridoRespository: Repository<DocsRequeridoRol>,
    private readonly setResultadoService: SetResultadoService,
    private readonly authService: AuthService
  ) {}

  async getDocsRequeridos(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let docsRequeridos: DocsRequeridoRol[] = [];

    try {
      docsRequeridos = await this.docsRequeridoRespository.find({ relations: ['docRequeridosRoles', 'docsRolesUsuario'] });

      let docsRequeridosDTO: DocsRequeridoRolDto[] = [];

      if (Array.isArray(docsRequeridos) && docsRequeridos.length) {
        docsRequeridos.forEach(item => {
          let nuevoDocRequerido: DocsRequeridoRolDto = {
            idDocsRequeridoRol: item.idDocsRequeridoRol,
            nombreDoc: item.nombre,
            descripcion: item.descripcion,
            obligatorio: item.obligatorio,
            activo: item.activo,
            // Comprobar si esta asignado aun Rol o a un DocRolUsuario
            asignado: item.docRequeridosRoles.length > 0 || item.docsRolesUsuario.length > 0 ? true : false,
          };

          docsRequeridosDTO.push(nuevoDocRequerido);
        });

        resultado.Respuesta = docsRequeridosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Obtenidos correctamente los Documentos Requeridos';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Documentos Requeridos';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Documentos Requeridos';
    }

    return resultado;
  }

  async getDocsRol(id: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let docsRequeridos: DocsRequeridoRol[] = [];

    try {
      docsRequeridos = await this.docsRequeridoRespository.find({
        relations: ['docRequeridosRoles'],
        where: qb => {
          qb.where('DocsRequeridoRol__docRequeridosRoles.idRol = :id', { id: id });
        },
      });
      let docsRequeridosDTO: DocsRequeridoRolDto[] = [];

      if (Array.isArray(docsRequeridos) && docsRequeridos.length) {
        docsRequeridos.forEach(item => {
          let nuevoDocRequerido: DocsRequeridoRolDto = {
            idDocsRequeridoRol: item.idDocsRequeridoRol,
            nombreDoc: item.nombre,
            descripcion: item.descripcion,
            obligatorio: item.obligatorio,
            activo: item.activo,
            // Comprobar si esta asignado aun Rol o a un DocRolUsuario
            asignado: item.docRequeridosRoles.length > 0 || item.docsRolesUsuario.length > 0 ? true : false,
          };

          docsRequeridosDTO.push(nuevoDocRequerido);
        });

        resultado.Respuesta = docsRequeridosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Obtenidos correctamente los Documentos Requeridos';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Documentos Requeridos';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Documentos Requeridos';
    }

    return resultado;
  }
  public async getDocRequeridoById(id: number, group: string = 'toShow'): Promise<Resultado> {
    const resultado = new Resultado();

    await this.docsRequeridoRespository
      .createQueryBuilder('dr')
      .leftJoinAndSelect('dr.docsRolesUsuario', 'dru')
      .leftJoinAndSelect('dr.docRequeridosRoles', 'drr')
      .where({ idDocsRequeridoRol: id })
      .getOneOrFail()
      .then((res: DocsRequeridoRol) => {
        // * Mapeamos la respuesta
        const docRequerido = plainToClass(DocRequeridoTransformer, res, { groups: [group] });

        // * Modificamos resultado para respuesta
        this.setResultadoService.setResponse<DocRequeridoTransformer>(resultado, docRequerido);
      })
      .catch((err: Error) => {
        // *  Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'No se ha encontrado Documento Requerido');
      });

    return resultado;
  }

  async getDocsRequeridosPaginados(req:any, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    const orderBy = ColumnasDocsRequeridoRol[paginacionArgs.orden.orden];
    let docsRequeridosPaginados: Pagination<DocsRequeridoRol>;
    let docsRequeridosParseados = new PaginacionDtoParser<DocsRequeridoRolDto>();
    let filtro = null;

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaAdminitracionRolesYPermisos);	
          
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
          return usuarioValidado;	
      }

      if (paginacionArgs.filtro != null && paginacionArgs.filtro != '') {
        filtro = paginacionArgs.filtro;
      }

      docsRequeridosPaginados = await paginate<DocsRequeridoRol>(this.docsRequeridoRespository, paginacionArgs.paginationOptions, {

        where: qb => {
          if (filtro?.nombre != null && filtro.nombre != '') {
            qb.andWhere('lower(unaccent(DocsRequeridoRol.nombre)) like lower(unaccent(:nombre))', { nombre: `%${filtro.nombre}%` });
          }
          if (filtro?.descripcion != null && filtro.descripcion != '') {
            qb.andWhere('lower(unaccent(DocsRequeridoRol.descripcion)) like lower(unaccent(:descripcion))', {
              descripcion: `%${filtro.descripcion}%`,
            });
          }

          if(filtro?.estado != null && filtro.estado != ''){
            if(filtro.estado == 'null') {
                qb.andWhere('DocsRequeridoRol.activo IS NULL')
            } 
            else if(filtro.estado == 'true'){
                qb.andWhere('DocsRequeridoRol.activo = true')
            } 
            else if(filtro.estado == 'false'){
                qb.andWhere('DocsRequeridoRol.activo = false')
            }     
          }

          qb.orderBy(orderBy, paginacionArgs.orden.ordenarPor);
        },
      });

      let docsRequeridosDTO: DocsRequeridoRolDto[] = [];

      if (Array.isArray(docsRequeridosPaginados.items) && docsRequeridosPaginados.items.length) {
        docsRequeridosPaginados.items.forEach(item => {
          let nuevoDocRequerido: DocsRequeridoRolDto = {
            idDocsRequeridoRol: item.idDocsRequeridoRol,
            nombreDoc: item.nombre,
            descripcion: item.descripcion,
            obligatorio: item.obligatorio,
            activo: item.activo,
            asignado : null
            // Comprobar si esta asignado aun Rol o a un DocRolUsuario
            //asignado: item.id.length > 0 || item.docsRolesUsuario.length > 0 ? true : false,
          };

          docsRequeridosDTO.push(nuevoDocRequerido);
        });

        docsRequeridosParseados.items = docsRequeridosDTO;
        docsRequeridosParseados.meta = docsRequeridosPaginados.meta;
        docsRequeridosParseados.links = docsRequeridosPaginados.links;

        resultado.Respuesta = docsRequeridosParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Obtenidos correctamente los Documentos Requeridos';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Documentos Requeridos';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Documentos Requeridos';
    }

    return resultado;
  }

  public async create(createDocRequerido: ModifyDocRequeridoRol): Promise<Resultado> {
    const resultado = new Resultado();

    try {
      const newDocRequerido = this.docsRequeridoRespository.create(createDocRequerido);

      if (!newDocRequerido) throw new Error('Error al crear Documento Requerido');

      await this.docsRequeridoRespository
        .createQueryBuilder()
        .insert()
        .into(DocsRequeridoRol)
        .values(newDocRequerido)
        .execute()
        .then((res: InsertResult) => {
          if (res.identifiers.length <= 0) throw new Error('No se ha podido insertar Documento Requerido');
        })
        .catch((err: Error) => {
          throw new Error('Error al insertar Doc Requerido');
        });

      // *  Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Documento Requerido creado con éxito');
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido crear Documento Requerido');
    } finally {
      return resultado;
    }
  }

  public async update(id: number, updateDocRequerido: ModifyDocRequeridoRol): Promise<Resultado> {
    const resultado = new Resultado();

    try {
      const searchDocRequerido = await this.getDocRequeridoById(id);

      if (!searchDocRequerido.ResultadoOperacion) throw new Error(searchDocRequerido.Mensaje);

      await this.docsRequeridoRespository
        .createQueryBuilder()
        .update()
        .set(updateDocRequerido)
        .where({ idDocsRequeridoRol: id })
        .execute()
        .then((res: UpdateResult) => {
          if (res.affected <= 0) throw new Error('No se ha actualizado ninguna fila');
        })
        .catch((err: Error) => {
          throw new Error('No se ha podido actualizar Documento Requerido');
        });

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Documento Requerido actualizado con éxito');
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido actualizar Documento Requerido');
    } finally {
      return resultado;
    }
  }

  public async remove(id: number): Promise<Resultado> {
    const resultado = new Resultado();

    try {
      const searchDocRequerido = await this.getDocRequeridoById(id, 'all');

      if (!searchDocRequerido.ResultadoOperacion) throw new Error(searchDocRequerido.Mensaje);

      const respuesta = searchDocRequerido.Respuesta as DocsRequeridoRol;

      if (respuesta.docsRolesUsuario.length > 1 || respuesta.docRequeridosRoles.length > 1)
        throw new Error('No se puede eliminar Documento Requerido si esta asignado');

      await this.docsRequeridoRespository
        .createQueryBuilder()
        .delete()
        .from(DocsRequeridoRol)
        .where({ idDocsRequeridoRol: id })
        .execute()
        .then((res: DeleteResult) => {
          if (res.affected <= 0) throw new Error('No se ha eliminado ninguna fila Documento Requerido');
        })
        .catch((err: Error) => {
          throw new Error('Error al eliminar Documento Requerido');
        });

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse(resultado, null, true, 'Documento Rquerido eliminado con éxito');
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido eliminar Documento Requerido');
    } finally {
      return resultado;
    }
  }
}
