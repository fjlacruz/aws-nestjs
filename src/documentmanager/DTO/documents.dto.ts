import { ApiPropertyOptional } from "@nestjs/swagger";
import {Column} from 'typeorm';

export class DocumentsDto {

    @ApiPropertyOptional()
    idFile: string;

    @ApiPropertyOptional()
    fileName:string;

    @ApiPropertyOptional()
    buffer: Buffer;

    @ApiPropertyOptional()
    thumbnail?: Buffer;

    @ApiPropertyOptional()
    created_at:Date;

    @ApiPropertyOptional()
    moduleName: String;

    @ApiPropertyOptional()
    mimetype: String;

    @ApiPropertyOptional()
    originalName: String;

    @ApiPropertyOptional()
    entityId?: String;


}
