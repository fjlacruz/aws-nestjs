import {Controller, Res, UploadedFile, UseInterceptors} from '@nestjs/common';
import {Get, Param, Post, Body, Put, Patch} from '@nestjs/common';
import {ApiOperation, ApiTags} from '@nestjs/swagger';
import {DocumentsDto} from '../DTO/documents.dto';
import {DocumentsService} from '../service/documents.service';
import {v4 as uuidv4} from 'uuid';
import {FileInterceptor} from '@nestjs/platform-express';
import {Public} from '../../auth/guards/auth.guard';

@ApiTags('Documents-manager')
@Controller('document-manager')
export class DocumentManagerController {
    constructor(private readonly documentService: DocumentsService) {
    }


    @Post('/')
    @ApiOperation({summary: 'Servicio para subir documentos asociado a un modulo'})
    @UseInterceptors(FileInterceptor('file'))

    async uploadFile(
        @UploadedFile() file, @Body() extraOptions) {
        const data = await this.documentService.createFile(file, extraOptions);
        return data;
    }

    @Get('/:options?')
    @ApiOperation({summary: 'Servicio que retorna todos los archivos'})
    async getDocuments(@Param('options') options: any) {
        options = JSON.parse(options);
        const data = await this.documentService.getDocuments(options);
        return {data};
    }

    @Get('/id/:idFile')
    @ApiOperation({summary: 'Servicio que devuelve un documento'})
    async getFileById(@Param('idFile') idFile: string) {
        const data = await this.documentService.getFile(idFile, false);
        return data;
    }

    @Public()
    @Get('download/:idFile/:thumb?')
    @ApiOperation({summary: 'Servicio que devuelve una imagen o archivo y su thumnbnail asociado'})
    async downloadFileById(@Param('idFile') idFile: string, @Param('thumb') thumb: Boolean, @Res() res) {
        const fileData = await this.documentService.getFile(idFile);
        let fileExtension = fileData.originalName.split('.').pop();
        let file;
        if (thumb && fileData.thumbnail) {
            file = Buffer.from(fileData.thumbnail);
        } else {
            file = Buffer.from(fileData.buffer);
        }

        res.setHeader('Content-Disposition', 'attachment; filename=' + fileData.fileName + '.' + fileExtension);
        res.setHeader('Content-Transfer-Encoding', 'binary');
        res.setHeader('Content-Type', fileData.mimetype);
        res.send(file);
    }
}
