import { Module } from '@nestjs/common';
import { DocumentsService } from './service/documents.service';
import { DocumentManagerController } from './controller/documents.controller';
import {DocumentEntity} from './entity/documents.entity';
import {TypeOrmModule} from '@nestjs/typeorm';

@Module({

  imports: [TypeOrmModule.forFeature([DocumentEntity])],
  providers: [DocumentsService],
  controllers: [DocumentManagerController],
  exports: [DocumentsService],
})
export class DocumentmanagerModule {}
