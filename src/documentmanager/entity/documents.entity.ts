import {Entity, Column, PrimaryColumn, ManyToOne, JoinColumn, Index} from 'typeorm';
import {ComunicadoEntity} from '../../comunicados/entity/comunicados.entity';
const uuid = require('uuid');

@Entity('Documents')
export class DocumentEntity {

    @PrimaryColumn()
    idFile: string;

    @Column()
    fileName: string;

    @Column({
        name: 'buffer',
        type: 'bytea',
        nullable: true
    })
    buffer: Buffer;

    @Column({
        name: 'thumbnail',
        type: 'bytea',
        nullable: true
    })
    thumbnail: Buffer;

    @Column()
    created_at: Date;

    @Column({
        nullable: true
    })
    mimetype: String;

    @Column({
        nullable: true
    })
    originalName: String;

    @Column({
        nullable: true
    })
    entityId: String;

    @Column({
        nullable: true
    })
    moduleName: String;

}
