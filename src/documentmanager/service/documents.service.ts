import {Injectable, NotFoundException} from '@nestjs/common';
import {DocumentEntity} from '../entity/documents.entity';
import {DocumentsDto} from '../DTO/documents.dto';
import {getConnection, Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import * as _ from 'lodash';
import {FindOneOptions} from 'typeorm/find-options/FindOneOptions';
import {v4 as uuidv4} from 'uuid';

const imageThumbnail = require('image-thumbnail');


@Injectable()
export class DocumentsService {

    constructor(@InjectRepository(DocumentEntity) private readonly documentEntity: Repository<DocumentEntity>) {
    }

    async createFile(file, extraOptions: any = null): Promise<DocumentEntity> {
        let {fileToSave, thumbOptions} = this.formatFileToUpload(file, extraOptions);

        if (this.isImage(fileToSave.originalName) && thumbOptions) {
            const thumbnail = await this.generateThumbnail(fileToSave.buffer, thumbOptions)
            fileToSave.thumbnail = thumbnail;
        }
        let fileSaved = await this.documentEntity.save(fileToSave);
        return this.formatFileResponse(fileSaved);

    }

    formatFileResponse(fileSaved: DocumentsDto) {
        let response: any = fileSaved;
        delete response.buffer;
        delete response.thumbnail;
        response.url = 'document-manager/download/' + response.idFile
        if (this.isImage(response.originalName)) {
            response.thumb = 'document-manager/download/' + response.idFile + '/true'
        }
        return response;
    }

    formatFileToUpload(file, extraOptions) {
        let fileToSave: DocumentsDto = {
            buffer: file.buffer,
            fileName: extraOptions.fileName || file.originalname,
            created_at: new Date(),
            moduleName: extraOptions.moduleName,
            mimetype: file.mimetype,
            originalName: file.originalname,
            entityId: extraOptions.entityId || null,
            idFile: uuidv4()
        };
        let thumbOptions = null;
        if (extraOptions.thumbWidth || extraOptions.thumbHeight) {
            thumbOptions = {
                width: extraOptions.thumbWidth ? parseInt(extraOptions.thumbWidth) : null,
                height: extraOptions.thumbHeight ? parseInt(extraOptions.thumbHeight) : null
            }
        }
        return {fileToSave, thumbOptions}
    }

    async getDocuments(options: any = {}) {
        let filter: any = {
            take: options.limit || 10,
            order: {
                created_at: 'DESC'
            }
        }
        if (!options.select) {
            filter.select = ['idFile', 'fileName', 'moduleName', 'mimetype', 'originalName', 'created_at', 'entityId']
        }
        if (options.page > 1) {
            filter.skip = filter.take * (options.page - 1)
        }
        if (options.where) {
            filter.where = options.where;
        }
        let result = await this.documentEntity.find(filter);
        _.map(result, (file: any) => {
            file.url = 'document-manager/download/' + file.idFile
            if (this.isImage(file.originalName)) {
                file.thumb = 'document-manager/download/' + file.idFile + '/true'
            }
            return file;
        })
        return result;
    }


    async getFile(id: string, withBufferData: boolean = true) {
        let filter: FindOneOptions = {
            select: ['idFile', 'fileName', 'moduleName', 'mimetype', 'originalName', 'created_at']
        }
        const file = withBufferData ? await this.documentEntity.findOne(id) : await this.documentEntity.findOne(id, filter);
        let response: any = file;

        response.url = 'document-manager/download/' + file.idFile
        if (this.isImage(file.originalName)) {
            response.thumb = 'document-manager/download/' + file.idFile + '/true'
        }
        if (!response) throw new NotFoundException('Document dostn exists');
        return response;
    }

    async getFileThumbnail(id: string) {
        const file = await this.documentEntity.findOne(id);
        if (!file) throw new NotFoundException('Document dostn exists');
        return file;
    }


    async delete(id: number) {
        try {
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(DocumentEntity)
                .where('idFile = :id', {id: id})
                .execute();
            return {'code': 200, 'message': 'success'};
        } catch (error) {
        }
    }

    async generateThumbnail(image: any, options: any = {width: 800}): Promise<any> {
        try {
            options.responseType = 'buffer';
            let thumbnail = await imageThumbnail(image, options);
            return thumbnail;
        } catch (error) {
            return {'code': 400, 'error': error};
        }
    }


    isImage(name: any): Boolean {
        try {
            return name.match(/.(jpg|jpeg|png|gif)$/i);
        } catch (error) {
            return false;
        }
    }
}




