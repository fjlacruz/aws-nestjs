import { ClasesLicenciasEntity } from "src/clases-licencia/entity/clases-licencias.entity";
import { RestriccionesEntity } from "src/clases-licencia/entity/restricciones.entity";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import {Entity, Column,PrimaryColumn, JoinColumn, OneToOne, ManyToOne} from "typeorm";

@Entity('DocumentosLicenciaRestriccion')
export class DocumentosLicenciaRestriccionEntity {
  @PrimaryColumn()
  idDocumentosLicenciaRestriccion: number;

  @Column()
  idDocumentoLicencia: number;
  @ManyToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.documentoLicenciaRestricciones)
  @JoinColumn({ name: 'idDocumentoLicencia' })
  readonly documentosLicencia: DocumentosLicencia;

  @Column()
  idrestriccion: number;
  @ManyToOne(() => RestriccionesEntity, restricciones => restricciones.documentoLicenciaRestricciones)
  @JoinColumn({ name: 'idrestriccion' })
  readonly restricciones: RestriccionesEntity;

}
