import { ClasesLicenciasEntity } from "src/clases-licencia/entity/clases-licencias.entity";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import { TramitesEntity } from "src/tramites/entity/tramites.entity";
import {Entity, Column,PrimaryColumn, JoinColumn, OneToOne, ManyToOne} from "typeorm";

@Entity('DocumentoLicenciaTramite')
export class DocumentoLicenciaTramiteEntity {

    @PrimaryColumn()
    idDocumentoLicenciaTramite:number;

    @Column()
    created_at:Date;

    @Column()
    idDocumentoLicencia:number;
    @ManyToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.documentoLicenciaTramite)
    @JoinColumn({ name: 'idDocumentoLicencia' })
    readonly documentosLicencia: DocumentosLicencia;
    
    @Column()
    idTramite:number;
    @ManyToOne(() => TramitesEntity, tramites => tramites.documentoLicenciaTramite)
    @JoinColumn({ name: 'idTramite' })
    readonly tramites: TramitesEntity;
}
