export class DocumentoLicenciaClaseLicenciaMobileDto {
    idClaseLicencia: string;
    descClaseLicencia: string;
    fechaPrimerOtorgamiento: string; 
    fechaActualOtorgamiento: string; 
    fechaProximoControl: string;
  }