import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocumentoLicenciaClaseLicenciaMobileDto } from "./documento-licencia-clase-licencia-mobile";

export class DocumentoLicenciaMobileDto {
    @ApiPropertyOptional()
    folioPapelSeguridad:string;
    @ApiPropertyOptional()
    idFolioSGL:number;
    @ApiPropertyOptional()
    estadoPCL:string;
    @ApiPropertyOptional()
    estadoEDF:string;
    @ApiPropertyOptional()
    estadoEDD:string;

    @ApiPropertyOptional()
    clasesLicencia:string;
    @ApiPropertyOptional()
    municipalidad:string;
    @ApiPropertyOptional()
    restricciones:string;
    @ApiPropertyOptional()
    fechaCaducidad:Date;

}
