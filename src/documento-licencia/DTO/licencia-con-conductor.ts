import { ApiPropertyOptional } from "@nestjs/swagger";
import { ConductorMobileDTO } from "src/conductor/DTO/conductor-mobile.dto";
import { ImagenLicenciaDto } from "src/imagen-licencia/DTO/imagen-licencia.dto";
import { DocumentoLicenciaClaseLicenciaMobileDto } from "./documento-licencia-clase-licencia-mobile";
import { DocumentoLicenciaMobileDto } from "./documento-licencia-mobile";

export class LicenciaConConductorDto {
    @ApiPropertyOptional()
    documentosLicencia: DocumentoLicenciaMobileDto[];
    
    @ApiPropertyOptional()
    conductor: ConductorMobileDTO;

    @ApiPropertyOptional()
    datosClasesLicencias: DocumentoLicenciaClaseLicenciaMobileDto[];

}
