import { ClasesLicenciasEntity } from "src/clases-licencia/entity/clases-licencias.entity";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import {Entity, Column,PrimaryColumn, JoinColumn, OneToOne, ManyToOne} from "typeorm";

@Entity('DocumentosLicenciaClaseLicencia')
export class DocumentosLicenciaClaseLicenciaEntity {
  @PrimaryColumn()
  idDocumentosLicenciaClaseLicencia: number;

  @Column()
  primerOtorgamiento: Date;

  @Column()
  otorgamientoActual: Date;

  @Column()
  caducidadOtorgamiento: Date;

  @Column()
  idDocumentoLicencia: number;
  @ManyToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.documentoLicenciaClaseLicencia)
  @JoinColumn({ name: 'idDocumentoLicencia' })
  readonly documentosLicencia: DocumentosLicencia;

  @Column()
  idClaseLicencia: number;
  @ManyToOne(() => ClasesLicenciasEntity, clasesLicencias => clasesLicencias.documentoLicenciaClaseLicencia)
  @JoinColumn({ name: 'idClaseLicencia' })
  readonly clasesLicencias: ClasesLicenciasEntity;
}
