import { ApiPropertyOptional } from "@nestjs/swagger";

export class DocumentoSolicitudPostulanteDTO {

    @ApiPropertyOptional()
    idDocSolicitudPostu:number;

    @ApiPropertyOptional()
    idPostulante:number;

    @ApiPropertyOptional()
    idSolicitud:number;

    @ApiPropertyOptional()
    nombreArchivo:string;

    @ApiPropertyOptional()
    archivo:Buffer;

    @ApiPropertyOptional()
    created_at:Date;

    @ApiPropertyOptional()
    idTipoDocumento: number;

}
