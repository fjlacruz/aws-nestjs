import { Test, TestingModule } from '@nestjs/testing';
import { DocumentoSolicitudPostulanteController } from './documento-solicitud-postulante.controller';

describe('DocumentoSolicitudPostulanteController', () => {
  let controller: DocumentoSolicitudPostulanteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocumentoSolicitudPostulanteController],
    }).compile();

    controller = module.get<DocumentoSolicitudPostulanteController>(DocumentoSolicitudPostulanteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
