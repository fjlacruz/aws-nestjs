import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { DocumentoSolicitudPostulanteDTO } from '../DTO/documento-solicitud-postulante.dto';
import { DocumentoSolicitudPostulanteService } from '../services/documento-solicitud-postulante.service';

@ApiTags('Documento-solicitud-postulante')
@Controller('documento-solicitud-postulante')
export class DocumentoSolicitudPostulanteController {

    constructor(private readonly documentoSolicitudPostulanteService: DocumentoSolicitudPostulanteService) { }

    @Get('documentos')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los documentos solicitud postulante' })
    async getDocumentos() {
        const data = await this.documentoSolicitudPostulanteService.getDocumentos();
        return { data };
    }


    @Get('documentoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por Id' })
    async getDocumentoById(@Param('id') id: number) {
        const data = await this.documentoSolicitudPostulanteService.getDocumento(id);
        return { data };
    }


    @Post('uploadDocumento')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Documento Solicitud Postulante' })
    async uploadDocumento(
        @Body() documentoSolicitudPostulanteDTO: DocumentoSolicitudPostulanteDTO) {
        const data = await this.documentoSolicitudPostulanteService.create(documentoSolicitudPostulanteDTO);
        return {data};

    }

    @Post('updateDocumento')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que modifica la metadatos de un Documento' })
    async updateDocumento(
        @Body() documentoSolicitudPostulanteDTO: DocumentoSolicitudPostulanteDTO) {
        const data = await this.documentoSolicitudPostulanteService.update(documentoSolicitudPostulanteDTO.idDocSolicitudPostu, documentoSolicitudPostulanteDTO);
        return {data};

    }
        

    @Get('deleteDocumentoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Docuemneto por Id' })
    async deleteDocumentoById(@Param('id') id: number) {
        const data = await this.documentoSolicitudPostulanteService.delete(id);
        return data;
    }
}
