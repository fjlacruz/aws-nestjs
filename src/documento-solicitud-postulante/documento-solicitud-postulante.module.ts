import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentoSolicitudPostulanteController } from './controller/documento-solicitud-postulante.controller';
import { DocumentoSolicitudPostulanteEntity } from './entity/documento-solicitud-postulante.entity';
import { DocumentoSolicitudPostulanteService } from './services/documento-solicitud-postulante.service';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentoSolicitudPostulanteEntity])],
  providers: [DocumentoSolicitudPostulanteService],
  exports: [DocumentoSolicitudPostulanteService],
  controllers: [DocumentoSolicitudPostulanteController]
})
export class DocumentoSolicitudPostulanteModule {}
