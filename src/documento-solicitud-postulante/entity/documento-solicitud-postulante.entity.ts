import { Solicitudes } from "src/solicitudes/entity/solicitudes.entity";
import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn, JoinColumn, ManyToOne} from "typeorm";

@Entity('DocumentoSolicitudPostulante')
export class DocumentoSolicitudPostulanteEntity {

    @PrimaryGeneratedColumn()
    idDocSolicitudPostu:number;

    @Column()
    idPostulante:number;

    @Column()
    idSolicitud:number;
    @ManyToOne(() => Solicitudes, solicitudes => solicitudes.documentosSolicitudPostulante)
    @JoinColumn({ name: 'idSolicitud' })
    readonly solicitudes: Solicitudes;
    
    @Column()
    nombreArchivo:string;

    @Column({
        name: 'archivo',
        type: 'bytea',
        nullable: false,
    })
    archivo:Buffer;

    @Column()
    created_at:Date;

    @Column()
    idTipoDocumento: number;

}
