import { Test, TestingModule } from '@nestjs/testing';
import { DocumentoSolicitudPostulanteService } from './documento-solicitud-postulante.service';

describe('DocumentoSolicitudPostulanteService', () => {
  let service: DocumentoSolicitudPostulanteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocumentoSolicitudPostulanteService],
    }).compile();

    service = module.get<DocumentoSolicitudPostulanteService>(DocumentoSolicitudPostulanteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
