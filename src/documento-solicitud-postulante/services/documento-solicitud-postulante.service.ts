import { Injectable , NotFoundException} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection, getRepository } from 'typeorm';
import { DocumentoSolicitudPostulanteDTO } from '../DTO/documento-solicitud-postulante.dto';
import { DocumentoSolicitudPostulanteEntity } from '../entity/documento-solicitud-postulante.entity';

@Injectable()
export class DocumentoSolicitudPostulanteService {

    constructor(@InjectRepository(DocumentoSolicitudPostulanteEntity) private readonly documentoSolicitudPostulanteEntity: Repository<DocumentoSolicitudPostulanteEntity>) { }

    async getDocumentos() {
 
        const documentos = await getConnection().createQueryBuilder() 
            .select(['DocumentoSolicitudPostulante.idDocSolicitudPostu',  'DocumentoSolicitudPostulante.idPostulante', 'DocumentoSolicitudPostulante.idSolicitud', 'DocumentoSolicitudPostulante.nombreArchivo', 'DocumentoSolicitudPostulante.created_at'])
            .from(DocumentoSolicitudPostulanteEntity, "DocumentoSolicitudPostulante")
            .getRawMany();
       
        if (!documentos)throw new NotFoundException("Documentos dont exist");
        
        return documentos;
  
    }
     
    async getDocumento(id:number){
        const documento= await this.documentoSolicitudPostulanteEntity.findOne(id);
        if (!documento)throw new NotFoundException("Documento dont exist");
        
        return documento;
    }

    async create(documentoSolicitudPostulanteDTO: DocumentoSolicitudPostulanteDTO): Promise<DocumentoSolicitudPostulanteEntity> {
        return await this.documentoSolicitudPostulanteEntity.save(documentoSolicitudPostulanteDTO);
    }

    async update(idDocSolicitudPostu: number, data: Partial<DocumentoSolicitudPostulanteDTO>) {
        await this.documentoSolicitudPostulanteEntity.update({ idDocSolicitudPostu }, data);
        return await this.documentoSolicitudPostulanteEntity.findOne({ idDocSolicitudPostu });
    }

    async delete(id: number) {
        try {
            await getConnection()
            .createQueryBuilder()
            .delete()
            .from(DocumentoSolicitudPostulanteEntity)
            .where("idDocSolicitudPostu = :id", { id: id })
            .execute();
            return {"code":200, "message":"success"};
        } catch (error) {
            return {"code":400, "error":error};
        }    
    }

}
