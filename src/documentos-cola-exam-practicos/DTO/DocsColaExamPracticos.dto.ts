import { ApiPropertyOptional } from "@nestjs/swagger";

export class DocsColaExamPracticosDTO {

    @ApiPropertyOptional()
    idDocsColaExamPracticos:number;

    @ApiPropertyOptional()
    idExamPractico:number;

    @ApiPropertyOptional()
    nombreDoc:string;

    @ApiPropertyOptional()
    archivo: Buffer;

    @ApiPropertyOptional()
    created_at:Date;
}
