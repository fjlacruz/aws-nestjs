import { Test, TestingModule } from '@nestjs/testing';
import { DocsColaExamPracticosController } from './documentos-cola-exam-practicos.controller';

describe('DocsColaExamPracticosController', () => {
  let controller: DocsColaExamPracticosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocsColaExamPracticosController],
    }).compile();

    controller = module.get<DocsColaExamPracticosController>(DocsColaExamPracticosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
