import { Controller, UseGuards } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { DocsColaExamPracticosDTO } from '../DTO/DocsColaExamPracticos.dto';
import { DocsColaExamPracticosService } from '../services/documentos-cola-exam-practicos.service';

@ApiTags('Documentos-cola-exam-practicos')
@Controller('documentos-cola-exam-practicos')
export class DocsColaExamPracticosController {

    constructor(private readonly docsColaExamPracticosService: DocsColaExamPracticosService) { }

    @Post('uploadDocumentosColaExamPracticos')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio para subir documentos asociados a una cola de examen practico' })
    async uploadDocumentosColaExamPracticos(
        @Body() docsColaExamPracticosDTO: DocsColaExamPracticosDTO) {
        const data = await this.docsColaExamPracticosService.createDocumento(docsColaExamPracticosDTO);
        return {data};
    }

    @Get('documentosColaExamPracticos')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Documentos de cola examen practicos' })
    async documentosColaExamPracticos() {
        const data = await this.docsColaExamPracticosService.getDocumentos();
        return { data };
    }


    @Get('documentosColaExamPracticosById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por Id' })
    async documentosColaExamPracticosById(@Param('id') id: number) {
        const data = await this.docsColaExamPracticosService.getDocumento(id);
        return { data };
    }


    @Post('updateDocumentosColaExamPracticos')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que modifica la metadatos de un documento cola examen pratico' })
    async updateDocumentosColaExamPracticos(
        @Body() docsColaExamPracticosDTO: DocsColaExamPracticosDTO) {
        const data = await this.docsColaExamPracticosService.update(docsColaExamPracticosDTO.idDocsColaExamPracticos, docsColaExamPracticosDTO);
        return {data};
    }
        

    @Get('deleteDocumentosColaExamPracticosById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Documento por Id' })
    async deleteDocumentoById(@Param('id') id: number) {
        const data = await this.docsColaExamPracticosService.delete(id);
        return data;
    }


    @Get('documentosByidExamPractico/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por idExamPractico' })
    async comunaByRegion(@Param('id') id: number) {
        const data = await this.docsColaExamPracticosService.getDocumentoByidExamPractico(id);
        return { data };
    }
}
