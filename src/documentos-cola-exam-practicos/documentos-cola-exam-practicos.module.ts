import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocsColaExamPracticosController } from './controller/documentos-cola-exam-practicos.controller';
import { DocsColaExamPracticosEntity } from './entity/DocsColaExamPracticos.entity';
import { DocsColaExamPracticosService } from './services/documentos-cola-exam-practicos.service';

@Module({
  imports: [TypeOrmModule.forFeature([DocsColaExamPracticosEntity])],
  providers: [DocsColaExamPracticosService],
  exports: [DocsColaExamPracticosService],
  controllers: [DocsColaExamPracticosController]
})
export class DocumentosColaExamPracticosModule {}
