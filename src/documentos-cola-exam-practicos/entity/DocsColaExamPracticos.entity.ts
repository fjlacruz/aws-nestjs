import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn} from "typeorm";

@Entity('DocsColaExamPracticos')
export class DocsColaExamPracticosEntity {

    @PrimaryColumn()
    idDocsColaExamPracticos:number;

    @Column()
    idExamPractico:number;

    @Column()
    nombreDoc:string;

    @Column({
        name: 'archivo',
        type: 'bytea',
        nullable: false,
    })
    archivo: Buffer;

    @Column()
    created_at:Date;
}
