import { Test, TestingModule } from '@nestjs/testing';
import { DocsColaExamPracticosService } from './documentos-cola-exam-practicos.service';

describe('DocsColaExamPracticosService', () => {
  let service: DocsColaExamPracticosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocsColaExamPracticosService],
    }).compile();

    service = module.get<DocsColaExamPracticosService>(DocsColaExamPracticosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
