import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import { DocsColaExamPracticosDTO } from '../DTO/DocsColaExamPracticos.dto';
import { DocsColaExamPracticosEntity } from '../entity/DocsColaExamPracticos.entity';
import { NotFoundException } from '@nestjs/common';

@Injectable()
export class DocsColaExamPracticosService {

    constructor(@InjectRepository(DocsColaExamPracticosEntity) private readonly docsColaExamPracticosEntity: Repository<DocsColaExamPracticosEntity>) { }


    async createDocumento(docsColaExamPracticosDTO: DocsColaExamPracticosDTO): Promise<DocsColaExamPracticosDTO> {
        return await this.docsColaExamPracticosEntity.save(docsColaExamPracticosDTO);
    }

    async getDocumentos() {
        return await this.docsColaExamPracticosEntity.find();
    }


    async getDocumento(id:number){
        const documento= await this.docsColaExamPracticosEntity.findOne(id);
        if (!documento)throw new NotFoundException("documneto dont exist");
        
        return documento;
    }

    async getDocumentoByidExamPractico(id:number){
        const documento = await getConnection() .createQueryBuilder() 
                .select("DocsColaExamPracticos") 
                .from(DocsColaExamPracticosEntity, "DocsColaExamPracticos") 
                .where("DocsColaExamPracticos.idExamPractico = :id", { id: id}).getMany();
                if (!documento)throw new NotFoundException("documento dont exist");        
        return documento;
    }


    async update(idDocsColaExamPracticos: number, data: Partial<DocsColaExamPracticosEntity>) {
        await this.docsColaExamPracticosEntity.update({ idDocsColaExamPracticos }, data);
        return await this.docsColaExamPracticosEntity.findOne({ idDocsColaExamPracticos });
    }

    async delete(id: number) {
        try {
            await getConnection()
            .createQueryBuilder()
            .delete()
            .from(DocsColaExamPracticosEntity)
            .where("idDocsColaExamPracticos = :id", { id: id })
            .execute();
            return {"code":200, "message":"success"};
        } catch (error) {
            return {"code":400, "error":error};
        }    
    }

}
