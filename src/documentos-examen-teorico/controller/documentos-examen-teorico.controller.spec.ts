import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosExamenTeoricoController } from './documentos-examen-teorico.controller';

describe('DocumentosExamenTeoricoController', () => {
  let controller: DocumentosExamenTeoricoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocumentosExamenTeoricoController],
    }).compile();

    controller = module.get<DocumentosExamenTeoricoController>(DocumentosExamenTeoricoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
