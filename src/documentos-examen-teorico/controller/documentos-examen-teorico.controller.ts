import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { DocumentoExamenTeoricoDTO } from '../dto/documento-examen-teorico.dto';
import { DocumentosExamenTeoricoService } from '../service/documentos-examen-teorico.service';

@ApiTags('Documentos-examen-teorico')
@Controller('documentos-examen-teorico')
export class DocumentosExamenTeoricoController
{
    constructor( private readonly documentosService: DocumentosExamenTeoricoService ) { }

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos documentos de los examenes teoricos' })
    async getAll()
    {
        return await this.documentosService.getAll()
            .then( data => { return { status: 200, data }; });
    }
    
    @Get('/:idDocumentoExamenTeorico')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un documento de examenen teorico por idDocumentoExamenTeorico' })
    async getById( @Param( 'idDocumentoExamenTeorico' ) id: number )
    {
        return await this.documentosService.getById( id )
            .then( data => { return { status: 200, data }; });
    }

    @Get('/por-examen/:idExamenTeorico')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los documentos de un examenen teorico por idExamenTeorico' })
    async getAllByIdExamenTeorico( @Param( 'idExamenTeorico' ) id: number )
    {
        return await this.documentosService.getAllByIdExamenTeorico( id )
            .then( data => 
            {
                console.log( data ); 
                return { status: 200, data }; 
            });
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un documento de examen teorico' })
    async create( @Body() dto: DocumentoExamenTeoricoDTO  )
    {
        return await this.documentosService.post( dto )
            .then( data => { return { status: 200, data }; });
    }

    @Delete('/:idDocumentoExamenTeorico' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un documento de examen teorico por idDocumentoExamenTeorico' })
    async delete( @Param( 'idDocumentoExamenTeorico' ) id: number  )
    {
        return await this.documentosService.delete( id )
            .then( data => { return { status: 200 }; });
    }
}
