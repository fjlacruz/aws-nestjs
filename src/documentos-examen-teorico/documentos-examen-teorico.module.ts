import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentosExamenTeoricoController } from './controller/documentos-examen-teorico.controller';
import { DocumentosExamenTeoricoEntity } from './entity/documentos-examen-teorico.entity';
import { DocumentosExamenTeoricoService } from './service/documentos-examen-teorico.service';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentosExamenTeoricoEntity])],
  controllers: [DocumentosExamenTeoricoController],
  providers: [DocumentosExamenTeoricoService],
  exports: [DocumentosExamenTeoricoService]
})
export class DocumentosExamenTeoricoModule {}
