import { ApiPropertyOptional } from "@nestjs/swagger";

export class DocumentoExamenTeoricoDTO
{
    @ApiPropertyOptional()
    NombreDocumento:string;

    @ApiPropertyOptional()
    fileBinary: Buffer;

    @ApiPropertyOptional()
    idExamenTeorico:number;

}

