import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn} from "typeorm";

@Entity('DocumentosExamenTeorico')
export class DocumentosExamenTeoricoEntity {

    @PrimaryGeneratedColumn()
    idDocumentoExamenTeorico:number;

    @Column()
    NombreDocumento:string;

    @Column({name: 'fileBinary', type: 'bytea', nullable: false })
    fileBinary: Buffer;

    @Column()
    created_at:Date;

    @Column()
    updated_at:Date;

    @Column()
    idExamenTeorico:number;
}
