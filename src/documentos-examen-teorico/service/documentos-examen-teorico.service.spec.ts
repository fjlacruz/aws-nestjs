import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosExamenTeoricoService } from './documentos-examen-teorico.service';

describe('DocumentosExamenTeoricoService', () => {
  let service: DocumentosExamenTeoricoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocumentosExamenTeoricoService],
    }).compile();

    service = module.get<DocumentosExamenTeoricoService>(DocumentosExamenTeoricoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
