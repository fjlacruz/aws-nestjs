import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DocumentoExamenTeoricoDTO } from '../dto/documento-examen-teorico.dto';
import { DocumentosExamenTeoricoEntity } from '../entity/documentos-examen-teorico.entity';

@Injectable()
export class DocumentosExamenTeoricoService
{
    constructor
    (
        @InjectRepository(DocumentosExamenTeoricoEntity) private readonly documentosRepository: Repository<DocumentosExamenTeoricoEntity>
    )
    {}
    async getAll()
    {
        return await this.documentosRepository.find();
    }
    async getAllByIdExamenTeorico( idExamen: number )
    {
        const id   = Number( idExamen );
        const data = await this.documentosRepository
                        .createQueryBuilder( "documento" )
                        .where( "documento.idExamenTeorico = :id", { id } )
                        .getMany();

        return data;
    }
    async getById( idDocumento: number )
    {
        const id = Number( idDocumento );
        return await this.documentosRepository.findOne( id );
    }
    async post( dto: DocumentoExamenTeoricoDTO )
    {
        dto['created_at'] = new Date();

        return await this.documentosRepository.save( dto );
    }
    async delete( idDocumento: number )
    {
        const id = Number( idDocumento );
        return await this.documentosRepository.delete( id );
    }

}
