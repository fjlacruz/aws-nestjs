import { ApiPropertyOptional } from "@nestjs/swagger";

export class DocumentosRespuestaDTO {

    @ApiPropertyOptional()
    idArchivosRespuesta:number;

    @ApiPropertyOptional()
    nombreArchivo:string;

    @ApiPropertyOptional()
    archivo:Buffer;

    @ApiPropertyOptional()
    idRespuestaFormExam:number;
}
