import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosRespuestaController } from './documentos-respuesta.controller';

describe('DocumentosRespuestaController', () => {
  let controller: DocumentosRespuestaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocumentosRespuestaController],
    }).compile();

    controller = module.get<DocumentosRespuestaController>(DocumentosRespuestaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
