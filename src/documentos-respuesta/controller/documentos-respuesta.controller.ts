import { Controller, UseGuards } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { DocumentosRespuestaDTO } from '../DTO/documentos-respuesta.dto';
import { DocumentosRespuestaService } from '../services/documentos-respuesta.service';

@ApiTags('Documentos-respuesta')
@Controller('documentos-respuesta')
export class DocumentosRespuestaController {

    constructor(private readonly documentosRespuestaService: DocumentosRespuestaService) { }

    @Get('documentos')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Documentos' })
    async getDocumentos() {
        const data = await this.documentosRespuestaService.getDocumentos();
        return { data };
    }


    @Get('documentoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por Id' })
    async getDocumentoById(@Param('id') id: number) {
        const data = await this.documentosRespuestaService.getDocumento(id);
        return { data };
    }


    @Post('uploadDocumento')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Documento' })
    async uploadDocumento(
        @Body() documentosRequest: DocumentosRespuestaDTO) {
        const data = await this.documentosRespuestaService.createDocumento(documentosRequest);
        return {data};

    }

    @Post('updateDocumento')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que modifica la metadatos de documentos' })
    async updateDocumento(
        @Body() documentosRequest: DocumentosRespuestaDTO) {
        const data = await this.documentosRespuestaService.update(documentosRequest.idArchivosRespuesta, documentosRequest);
        return {data};

    }
        

    @Get('deleteDocumentoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Documento por Id' })
    async deleteDocumentoById(@Param('id') id: number) {
        const data = await this.documentosRespuestaService.delete(id);
        return data;
    }

    @Get('documentoById/respuesta/:idRespuestaFormExam')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por Id' })
    async getDocumentoByIdRespuestaFormExam(@Param('idRespuestaFormExam') idRespuestaFormExam: number) {
        const data = await this.documentosRespuestaService.getDocumentosbyidRespuestaFormExam(idRespuestaFormExam);
        return { data };
    }
}
