import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentosRespuestaController } from './controller/documentos-respuesta.controller';
import { DocumentosRespuestaEntity } from './entity/documentos-respuesta.entity';
import { DocumentosRespuestaService } from './services/documentos-respuesta.service';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentosRespuestaEntity])],
  providers: [DocumentosRespuestaService],
  exports: [DocumentosRespuestaService],
  controllers: [DocumentosRespuestaController]
})
export class DocumentosRespuestaModule {}
