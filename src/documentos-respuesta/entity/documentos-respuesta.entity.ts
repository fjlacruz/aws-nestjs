import { TramitesEntity } from "src/tramites/entity/tramites.entity";
import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn, OneToOne, JoinColumn} from "typeorm";

@Entity('ArchivosRespuesta')
export class DocumentosRespuestaEntity {

    @PrimaryGeneratedColumn()
    idArchivosRespuesta:number;

    @Column()
    nombreArchivo:string;

    @Column({
        type: 'bytea',
        nullable: false
    })
    archivo:Buffer;

    @Column()
    idRespuestaFormExam:number;
}
