import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosRespuestaService } from './documentos-respuesta.service';

describe('DocumentosRespuestaService', () => {
  let service: DocumentosRespuestaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocumentosRespuestaService],
    }).compile();

    service = module.get<DocumentosRespuestaService>(DocumentosRespuestaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
