import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { getConnection } from "typeorm";
import { DocumentosRespuestaDTO } from '../DTO/documentos-respuesta.dto';
import { DocumentosRespuestaEntity } from '../entity/documentos-respuesta.entity';

@Injectable()
export class DocumentosRespuestaService {

    
    constructor(@InjectRepository(DocumentosRespuestaEntity) private readonly documentosRespuestaEntity: Repository<DocumentosRespuestaEntity>) { }

    async getDocumentosbyidRespuestaFormExam(id: number) {

        const documentosLicenciaList = await getConnection().createQueryBuilder()
            .select("ArchivosRespuesta")
            .from(DocumentosRespuestaEntity, "ArchivosRespuesta")
            .where("ArchivosRespuesta.idRespuestaFormExam = :id", { id: id }).getMany();
        if (!documentosLicenciaList) throw new NotFoundException("Documentos  dont exist");

        return documentosLicenciaList;
    }

    async createDocumento(documentosDTO: DocumentosRespuestaDTO): Promise<DocumentosRespuestaEntity> {
        return await this.documentosRespuestaEntity.save(documentosDTO);
    }

    async getDocumentos() {
        return await this.documentosRespuestaEntity.find();
    }


    async getDocumento(id:number){
        const documento= await this.documentosRespuestaEntity.findOne(id);
        if (!documento)throw new NotFoundException("documneto dont exist");
        
        return documento;
    }


    async update(idArchivosRespuesta: number, data: Partial<DocumentosRespuestaDTO>) {
        await this.documentosRespuestaEntity.update({ idArchivosRespuesta }, data);
        return await this.documentosRespuestaEntity.findOne({ idArchivosRespuesta });
    }

    async delete(id: number) {
        try {
            await getConnection()
            .createQueryBuilder()
            .delete()
            .from(DocumentosRespuestaEntity)
            .where("idArchivosRespuesta = :id", { id: id })
            .execute();
            return {"code":200, "message":"success"};
        } catch (error) {
            return {"code":400, "error":error};
        }    
    }

}
