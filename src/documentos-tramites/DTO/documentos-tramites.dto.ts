
export class DocumentosTramitesDTO {

    idDocumentosTramite:number;
    NombreDocumento:string;
    fileBinary: string;
    created_at: Date;
    updated_at: Date;
    idTipoDocumento: number;
    idUsuario: number;
    idTramite: number;
}
