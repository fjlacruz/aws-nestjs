import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosTramitesController } from './documentos-tramites.controller';

describe('DocumentosTramitesController', () => {
  let controller: DocumentosTramitesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocumentosTramitesController],
    }).compile();

    controller = module.get<DocumentosTramitesController>(DocumentosTramitesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
