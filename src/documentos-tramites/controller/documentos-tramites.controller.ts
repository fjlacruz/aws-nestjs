import { Controller, Delete, Req, UseGuards } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres, RoleNames } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { DocumentosTramitesService } from '../services/documentos-tramites.service';

@ApiTags('Documentos-tramites')
@Controller('documentos-tramites')
export class DocumentosTramitesController {

    constructor(private readonly documentosTramitesService: DocumentosTramitesService,
        private readonly authService: AuthService) { }

    @Get('getDocumentosbyIdTramite/:idTramite')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los los documentos asociado a un tramite por su id' })
    async getDocumentosbyIdTramite(@Param('idTramite') idTramite: number) {
        const data = await this.documentosTramitesService.getDocumentosbyIdTramite(idTramite);
        return { data };
    }


    @Get('getDocumentosTramitebyRUNPostulante/:RUN')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los los documentos asociado a un tramite por RUN Postulante' })
    async getDocumentosTramitebyRUNPostulante(@Param('RUN') RUN: number) {
        const data = await this.documentosTramitesService.getDocumentosTramitebyRUNPostulante(RUN);
        return { data };
    }

    @Delete('deleteDocumentosTramiteById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que borra un documento trámite por id trámite' })
    async deleteDocumentosTramitebyTramiteId(@Param('id') id: number, @Req() req: any) {

        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaOtorgamientosDenegaciones
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.documentosTramitesService.deleteDocumentosTramiteByTramiteId(id);
            return { data };

        } else {

            return { data: validarPermisos };

        }

    }

    @Get('getDocumentoTramiteById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un documentoTramite por su id' })
    async getDocumentoTramiteById(@Param('id') id: number, @Req() req: any) {

        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.AccesoConsultaCarpetaConductor
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.documentosTramitesService.getDocumentoTramiteById(id);
            return { data };

        } else {

            return { data: validarPermisos };
        }
    }

    @Post('getDocumentosTramitesByIds')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un documentoTramite por su id' })
    async getDocumentosTramitesByIds(@Body() ids: number[], @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.AccesoConsultaCarpetaConductor
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.documentosTramitesService.getDocumentosTramitesByIds(ids);
            return { data };

        } else {

            return { data: validarPermisos };
        }
    }
}
