import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentosTramitesEntity } from './entity/documentos-tramites.entity';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { DocumentosTramitesService } from './services/documentos-tramites.service';
import { DocumentosTramitesController } from './controller/documentos-tramites.controller';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Roles } from 'src/roles/entity/roles.entity';
import { User } from 'src/users/entity/user.entity';
import { jwtConstants } from 'src/users/constants';
import { AuthService } from 'src/auth/auth.service';

@Module({
  imports: 
  [
    TypeOrmModule.forFeature([DocumentosTramitesEntity,Postulante,RolesUsuarios,User]),
  ],
  providers: [DocumentosTramitesService, AuthService],
  exports: [DocumentosTramitesService],
  controllers: [DocumentosTramitesController]
})
export class DocumentosTramitesModule { }
