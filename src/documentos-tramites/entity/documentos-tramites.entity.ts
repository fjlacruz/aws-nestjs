import { TiposDocumentoEntity } from 'src/tipos-documento/entity/tipos-documento.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { ManyToOne } from 'typeorm';
import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, JoinColumn } from 'typeorm';

@Entity('DocumentosTramites')
export class DocumentosTramitesEntity {
  @PrimaryColumn()
  idDocumentosTramite: number;

  @Column()
  NombreDocumento: string;

  @Column({
    type: 'bytea',
    nullable: false,
  })
  fileBinary: Buffer;

  @Column()
  created_at: Date;

  @Column()
  updated_at: Date;

  @Column()
  idTipoDocumento: number;

  @Column()
  idUsuario: number;

  @Column()
  idTramite: number;

  @OneToOne(() => TramitesEntity, tramite => tramite.documentosTramites)
  @JoinColumn({ name: 'idTramite' })
  readonly tramite: TramitesEntity;

  @ManyToOne(() => TiposDocumentoEntity, tipoDocumento => tipoDocumento.documentos)
  @JoinColumn({ name: 'idTipoDocumento' })
  readonly tipoDocumento: TiposDocumentoEntity;
}
