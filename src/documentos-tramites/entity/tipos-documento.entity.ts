import {Entity, Column,PrimaryColumn} from "typeorm";

@Entity('TiposDocumento')
export class TiposDocumentoEntity {

    @PrimaryColumn()
    idTipoDocumento:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;

    @Column()
    created_at:Date;

    @Column()
    update_at:Date;

    @Column()
    idUsuario:number;
}
