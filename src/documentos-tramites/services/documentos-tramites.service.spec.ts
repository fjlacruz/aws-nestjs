import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosTramitesService } from './documentos-tramites.service';

describe('DocumentosTramitesService', () => {
  let service: DocumentosTramitesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocumentosTramitesService],
    }).compile();

    service = module.get<DocumentosTramitesService>(DocumentosTramitesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
