import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DocumentosTramitesEntity } from '../entity/documentos-tramites.entity';
import { getConnection } from "typeorm";
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { Resultado } from 'src/utils/resultado';
import { DocumentosTramitesDTO } from '../DTO/documentos-tramites.dto';


@Injectable()
export class DocumentosTramitesService {

    constructor(@InjectRepository(DocumentosTramitesEntity) private readonly documentosTramitesRepository: Repository<DocumentosTramitesEntity>,
        @InjectRepository(Postulante) private readonly postulanteRespository: Repository<Postulante>) { }

    async getDocumentosbyIdTramite(id: number) {

        const documentosLicenciaList = await getConnection().createQueryBuilder()
            .select("DocumentosTramites")
            .from(DocumentosTramitesEntity, "DocumentosTramites")
            .where("DocumentosTramites.idTramite = :id", { id: id }).getMany();
        if (!documentosLicenciaList) throw new NotFoundException("Documentos Tramite dont exist");

        return documentosLicenciaList;
    }


    async getDocumentosTramitebyRUNPostulante(RUN: number) {

        const postulante = await this.postulanteRespository.findOne({ where: { RUN } });
        if (!postulante) throw new NotFoundException("Documentos Tramite dont exist");


        const documentosLicenciaList = await getConnection().createQueryBuilder()
            .select("DocumentosTramites")
            .from(DocumentosTramitesEntity, "DocumentosTramites")
            .where("DocumentosTramites.idUsuario = :id", { id: postulante.idUsuario }).getMany();
        if (!documentosLicenciaList) throw new NotFoundException("Documentos Tramite dont exist");

        return documentosLicenciaList;
    }

    async deleteDocumentosTramiteByTramiteId(id: number) {

        let resultado: Resultado = new Resultado();

        try {

            if (id != null || id != undefined) {

                this.documentosTramitesRepository.delete({idDocumentosTramite: id});
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Documento trámite borrado correctamente'
                
            } else {

                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se ha pasado idDocumentosTramite por parámetro'
                
            }
            
        } catch (error) {

            resultado.ResultadoOperacion = false;
            resultado.Error = error.message;
            
        }

        return resultado;

    }

    async getDocumentoTramiteById(id:number){

        const documentoTramite: DocumentosTramitesEntity = await this.documentosTramitesRepository.findOne(id);
        if (!documentoTramite) throw new NotFoundException("No se ha encontrado el Tramite Clase Licencia");

        let documentoTramiteDTO: DocumentosTramitesDTO = new DocumentosTramitesDTO();
        documentoTramiteDTO.fileBinary = documentoTramite.fileBinary?.toString();
        documentoTramiteDTO.NombreDocumento = documentoTramite.NombreDocumento;

        return documentoTramiteDTO;
    }

    
    async getDocumentosTramitesByIds(ids: number[]){

        const documentoTramite: DocumentosTramitesEntity[] = await this.documentosTramitesRepository.find({
            where: qb => {
                qb.where(
                    'DocumentosTramitesEntity.idDocumentosTramite IN (:...RUN)', { RUN: ids }
                )
            }
        });
        if (documentoTramite.length <= 0) throw new NotFoundException("No se han encontrado los Tramite Clase Licencia");

        let documentosTramitesDTO: DocumentosTramitesDTO[] = [];

        documentoTramite.forEach((item) => {
            let documentoTramiteDTO: DocumentosTramitesDTO = new DocumentosTramitesDTO();
            documentoTramiteDTO.fileBinary = item.fileBinary?.toString();
            documentoTramiteDTO.NombreDocumento = item.NombreDocumento;
            documentosTramitesDTO.push(documentoTramiteDTO);
        });

        return documentosTramitesDTO;
    }
}
