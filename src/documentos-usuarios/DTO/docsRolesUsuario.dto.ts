import { ApiPropertyOptional } from "@nestjs/swagger";

export class DocsRolesUsuarioDTO {

    @ApiPropertyOptional()
    idDocsRolesUsuario: number;

    @ApiPropertyOptional()
    idRol: number;


    @ApiPropertyOptional()
    nombreDoc:string;

    @ApiPropertyOptional()
    archivo:Buffer;

    @ApiPropertyOptional()
    idUsuario: number;

    // @ApiPropertyOptional()
    // created_at: Date;

    @ApiPropertyOptional()
    idDocsRequeridoRol: number
}
