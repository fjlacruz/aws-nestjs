import { ApiPropertyOptional } from "@nestjs/swagger";

export class RolesUsuarioDto {

    @ApiPropertyOptional()
    idRol:number;

    @ApiPropertyOptional()
    idUsuario:number;

    @ApiPropertyOptional()
    idInstitucion:number;
}
