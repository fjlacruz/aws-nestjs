import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosUsuariosController } from './documentos-usuarios.controller';

describe('DocumentosUsuariosController', () => {
  let controller: DocumentosUsuariosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocumentosUsuariosController],
    }).compile();

    controller = module.get<DocumentosUsuariosController>(DocumentosUsuariosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
