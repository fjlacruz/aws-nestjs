import { Controller, UseGuards } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { DocsRolesUsuarioDTO } from '../DTO/docsRolesUsuario.dto';
import { DocumentosUsuariosService } from '../services/documentos-usuarios.service';


@ApiTags('Documentos-usuarios')
@Controller('documentos-usuarios')
export class DocumentosUsuariosController {

    constructor(private readonly documentosUsuariosService: DocumentosUsuariosService) { }

    @Get('getDocumentosRolesUsuario/:idRol/:idUsuario/:idInstitucion')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los los documentos por Usuario SGL y Rol' })
    async getDocumentosRolesUsuario(@Param('idRol') idRol: number, @Param('idUsuario') idUsuario: number, @Param('idInstitucion') idInstitucion: number) {
        const data = await this.documentosUsuariosService.getDocumentosRolesUsuario(idRol, idUsuario, idInstitucion);
        return { data };
    }

    @Get('getDocumentosUsuario/:run')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los los documentos por Usuario SGL y Rol' })
    async getDocumentosUsuario(@Param('run') run: number) {
        const data = await this.documentosUsuariosService.getDocumentosUsuario(run);
        return { data };
    }

    @Get('getDocumentosByRolesUsuario/:idRol/:idUsuario')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los los documentos por Usuario SGL y Rol' })
    async getDocumentosByRolesUsuario(@Param('idRol') idRol: number, @Param('idUsuario') idUsuario: number) {
        const data = await this.documentosUsuariosService.getDocumentosByRolesUsuario(idRol, idUsuario);
        return { data };
    }


    @Post('uploadDocumentoRolUsuario')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio para subir documentos asociados a un rol de un usuario' })
    async uploadDocumento(
        @Body() dto: DocsRolesUsuarioDTO) {
        const data = await this.documentosUsuariosService.createDocumento(dto);
        return {data};
    }

    @Get('documentosRolUsuario')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Documentos' })
    async getDocumentosByRolUsuario() {
        const data = await this.documentosUsuariosService.getDocumentos();
        return { data };
    }


    @Get('documentoRolUsuarioById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por Id' })
    async getDocumentoById(@Param('id') id: number) {
        const data = await this.documentosUsuariosService.getDocumento(id);
        return { data };
    }


    @Post('updateDocumentoRolUsuario')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que modifica la metadatos de documentos' })
    async updateDocumento(
        @Body() docsRolesUsuarioDTO: DocsRolesUsuarioDTO) {
        const data = await this.documentosUsuariosService.update(docsRolesUsuarioDTO.idDocsRolesUsuario, docsRolesUsuarioDTO);
        return {data};
    }
        

    @Get('deleteDocumentoRolUsuarioById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Documento por Id' })
    async deleteDocumentoById(@Param('id') id: number) {
        const data = await this.documentosUsuariosService.delete(id);
        return data;
    }

}
