import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { DocumentosEntity } from 'src/file-manager/entity/documentos.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { DocumentosUsuariosController } from './controller/documentos-usuarios.controller';
import { DocsRolesUsuarioEntity } from './entity/DocsRolesUsuario.entity';
import { DocumentosUsuariosService } from './services/documentos-usuarios.service';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentosEntity, DocsRolesUsuarioEntity, User, RolesUsuarios])],
  providers: [DocumentosUsuariosService, AuthService],
  exports: [DocumentosUsuariosService],
  controllers: [DocumentosUsuariosController]
})
export class DocumentosUsuariosModule {}
