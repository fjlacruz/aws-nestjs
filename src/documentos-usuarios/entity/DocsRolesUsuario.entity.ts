import { Roles } from "src/roles/entity/roles.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne} from "typeorm";

@Entity('DocsRolesUsuario')
export class DocsRolesUsuarioEntity {

    @PrimaryGeneratedColumn()
    idDocsRolesUsuario: number;

    @Column()
    idRol: number;
    @ManyToOne(() => Roles, roles => roles.docsRolesUsuario)
    @JoinColumn({ name: 'idRol' })
    readonly roles: Roles;

    @Column()
    idUsuario: number;
    @ManyToOne(() => User, usuario => usuario.docsRolesUsuario)
    @JoinColumn({ name: 'idUsuario' })
    readonly usuario: User;

    @Column()
    nombreDoc:string;

    @Column({
        name: 'archivo',
        type: 'bytea',
        nullable: false,
    })
    archivo: Buffer;

    @Column()
    created_at:Date;

    @Column()
    idDocsRequeridoRol: number;
}
