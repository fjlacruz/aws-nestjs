export class DocumentosUsuarioDTO {
    idDocsRolesUsuario: number;
    idRol: number;
    idUsuario: number;
    nombreDoc:string;
    archivo: any;
    created_at:Date;
    idDocsRequeridoRol: number;
}
