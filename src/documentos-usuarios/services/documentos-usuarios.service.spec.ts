import { Test, TestingModule } from '@nestjs/testing';
import { DocumentosUsuariosService } from './documentos-usuarios.service';

describe('DocumentosUsuariosService', () => {
  let service: DocumentosUsuariosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocumentosUsuariosService],
    }).compile();

    service = module.get<DocumentosUsuariosService>(DocumentosUsuariosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
