import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import { DocumentosEntity } from 'src/file-manager/entity/documentos.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { DocsRolesUsuarioEntity } from '../entity/DocsRolesUsuario.entity';
import { DocsRolesUsuarioDTO } from '../DTO/docsRolesUsuario.dto';
import { NotFoundException } from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';
import { DocsRolesUsuario } from '../../docsRequeridoRol/entity/DocsRolesUsuario.entity';
import { datosDocsRolesUsuarioDTO } from '../../users/DTO/datosDocsRolesUsuario.dto';

@Injectable()
export class DocumentosUsuariosService {
  constructor(
    private readonly authService: AuthService,

    @InjectRepository(DocumentosEntity)
    private readonly documentosEntity: Repository<DocumentosEntity>,

    @InjectRepository(DocsRolesUsuarioEntity)
    private readonly docsRolesUsuarioEntity: Repository<DocsRolesUsuarioEntity>
  ) {}

  async getDocumentosRolesUsuario(idRol: number, idUsuario: number, idInstitucion: number) {
    return this.documentosEntity
      .createQueryBuilder('DocsRolesUsuario')
      .innerJoinAndMapMany('Documentos.idUsuario', RolesUsuarios, 'rol', 'Documentos.idUsuario = rol.idUsuario')
      .where('Documentos.idUsuario = :idUsuario', { idUsuario: idUsuario })
      .andWhere('rol.idRol = :idRol', { idRol: idRol })
      .andWhere('rol.idInstitucion = :idInstitucion', { idInstitucion: idInstitucion })
      .getMany();
  }

  async getDocumentosUsuario(run: number) {
    return this.documentosEntity
      .createQueryBuilder('Documentos')
      .where('Documentos.idUsuario = :idUsuario', { idUsuario: 15 }) //! Cambiar id por el correspondiente a RUN
      .getMany();
  }

  async getDocumentosByRolesUsuario(idRol: number, idUsuario: number) {
    const documentos = await this.docsRolesUsuarioEntity
      .createQueryBuilder('DocsRolesUsuario')
      .innerJoinAndMapMany('DocsRolesUsuario.idUsuario', RolesUsuarios, 'rol', 'DocsRolesUsuario.idUsuario = rol.idUsuario')
      .where('DocsRolesUsuario.idUsuario = :idUsuario', { idUsuario: idUsuario })
      .andWhere('DocsRolesUsuario.idRol = :idRol', { idRol: idRol })
      .getMany();
    // console.log(documentos);
    const documentosSaved = [];
    documentos.forEach(docRol => {
      const docsRolesUsuarioDto = new datosDocsRolesUsuarioDTO();
      docsRolesUsuarioDto.idDocsRolesUsuario = docRol.idDocsRolesUsuario;
      docsRolesUsuarioDto.idUsuario = docRol.idUsuario;
      docsRolesUsuarioDto.idRol = docRol.idRol;
      docsRolesUsuarioDto.archivo = docRol.archivo?.toString();
      docsRolesUsuarioDto.nombreDoc = docRol.nombreDoc;
      docsRolesUsuarioDto.created_at = docRol.created_at;
      docsRolesUsuarioDto.idDocsRequeridoRol = docRol.idDocsRequeridoRol;
      documentosSaved.push(docsRolesUsuarioDto);
      // console.log(docsRolesUsuarioDto);
    });
    return documentosSaved;
  }

  async createDocumento(dto: DocsRolesUsuarioDTO): Promise<DocsRolesUsuarioEntity> {
    const created_at = new Date();
    const data = { ...dto, created_at };

    return await this.docsRolesUsuarioEntity.save(data);
  }

  async getDocumentos() {
    return await this.docsRolesUsuarioEntity.find();
  }

  async getDocumento(id: number) {
    const documento = await this.docsRolesUsuarioEntity.findOne(id);
    if (!documento) throw new NotFoundException('documneto dont exist');

    return documento;
  }

  async update(idDocsRolesUsuario: number, data: Partial<DocsRolesUsuarioEntity>) {
    await this.docsRolesUsuarioEntity.update({ idDocsRolesUsuario }, data);
    return await this.docsRolesUsuarioEntity.findOne({ idDocsRolesUsuario });
  }

  async delete(id: number) {
    try {
      await getConnection()
        .createQueryBuilder()
        .delete()
        .from(DocsRolesUsuarioEntity)
        .where('idDocsRolesUsuario = :id', { id: id })
        .execute();
      return { code: 200, message: 'success' };
    } catch (error) {
      return { code: 400, error: error };
    }
  }
}
