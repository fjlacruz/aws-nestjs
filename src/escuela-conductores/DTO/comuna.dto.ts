import { ApiPropertyOptional } from "@nestjs/swagger";
import { RegionDto } from "./region.dto";


export class ComunaDTO {

    @ApiPropertyOptional()
    idComuna:number;

    @ApiPropertyOptional()
    Nombre:string;

    @ApiPropertyOptional()
    Descripcion:string;

    @ApiPropertyOptional()
    idRegion:number;

    @ApiPropertyOptional()
    region:RegionDto;
}
