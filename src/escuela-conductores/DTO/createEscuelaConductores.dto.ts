import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateEscuelaConductoresDTO {

    @ApiPropertyOptional()
    idEscuelaConductor:number;

    @ApiPropertyOptional()
    RUT:number;

    @ApiPropertyOptional()
    DV:string;

    @ApiPropertyOptional()
    NombreEscuela:string;

    @ApiPropertyOptional()
    idComuna:number;

    @ApiPropertyOptional()
    Calle:string;

    @ApiPropertyOptional()
    CalleNro:number;

    @ApiPropertyOptional()
    Letra:string;

    @ApiPropertyOptional()
    RestoDireccion:string;

    // @ApiPropertyOptional()
    // created_at:Date;

    // @ApiPropertyOptional()
    // updated_at:Date;

    // @ApiPropertyOptional()
    // idUsuario:number;
}
