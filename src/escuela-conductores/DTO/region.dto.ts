import { ApiPropertyOptional } from "@nestjs/swagger";

export class RegionDto {

    @ApiPropertyOptional()
    idRegion:number;

    @ApiPropertyOptional()
    Nombre:string;

    @ApiPropertyOptional()
    Descripcion:string;
}
