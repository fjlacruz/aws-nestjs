import { Test, TestingModule } from '@nestjs/testing';
import { EscuelaConductoresController } from './escuela-conductores.controller';

describe('EscuelaConductoresController', () => {
  let controller: EscuelaConductoresController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EscuelaConductoresController],
    }).compile();

    controller = module.get<EscuelaConductoresController>(EscuelaConductoresController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
