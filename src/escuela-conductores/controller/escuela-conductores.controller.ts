import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CreateEscuelaConductoresDTO } from '../DTO/createEscuelaConductores.dto';
import { EscuelaConductoresEntity } from '../entity/escuela-conductores.entity';
import { EscuelaConductoresService } from '../services/escuela-conductores.service';


@ApiTags('escuela-conductores')
@Controller('escuela-conductores')
export class EscuelaConductoresController {

    constructor(private readonly escuelaConductoresService: EscuelaConductoresService) { }

    @Get('getEscuelasConductores')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las escuelas de conductores' })
    async getEscuelasConductores() {
        const data = await this.escuelaConductoresService.getEscuelasConductores();
        return { data };
    }

    @Get('getEscuelaConductoresById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Escuela de Conductores por Id' })
    async getEscuelaConductoresById(@Param('id') id: number) {
        const data = await this.escuelaConductoresService.getEscuelaConductores(id);
        return { data };
    }

    @Patch('escuelaConductoresUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de una Escuela de Conductores' })
    async escuelaConductoresUpdate(@Param('id') id: number, @Body() dto: Partial<CreateEscuelaConductoresDTO>) {
        return await this.escuelaConductoresService.update(id, dto);
    }

    @Post('crearEscuelaConductores')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una nueva Escuela de Conductores' })
    async crearEscuelaConductores(
        @Body() dto: CreateEscuelaConductoresDTO) {
        const data = await this.escuelaConductoresService.create(dto);
        return {data};
    }


}
