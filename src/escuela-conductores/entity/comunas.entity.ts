import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('Comunas')
export class ComunasEntity {

    @PrimaryColumn()
    idComuna:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;

    @Column()
    idRegion:number;
}
