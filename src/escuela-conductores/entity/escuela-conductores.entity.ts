import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";

@Entity('EscuelaConductores')
export class EscuelaConductoresEntity {

    @PrimaryColumn()
    idEscuelaConductor:number;

    @Column()
    RUT:number;

    @Column()
    DV:string;

    @Column()
    NombreEscuela:string;

    @Column()
    idComuna:number;

    @Column()
    Calle:string;

    @Column()
    CalleNro:number;

    @Column()
    Letra:string;

    @Column()
    RestoDireccion:string;

    @Column()
    created_at:Date;

    @Column()
    updated_at:Date;

    @Column()
    idUsuario:number;
}
