
import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, OneToMany } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';

@Entity('Regiones')
export class RegionesEntity {

    @PrimaryColumn()
    idRegion:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;

    @ApiProperty()
    @OneToMany(() => ComunasEntity, comunas => comunas.regiones)
    readonly comunas: ComunasEntity[];

    @ApiProperty()
    @OneToMany(() => RolesUsuarios, rolesUsuarios => rolesUsuarios.regiones)
    readonly rolesUsuarios: RolesUsuarios[];

    @ApiProperty()
    @OneToMany(() => ComunasEntity, comunas => comunas.regiones)
    readonly comunasAuxPermisos: ComunasEntity[];
}
