import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { EscuelaConductoresController } from './controller/escuela-conductores.controller';
import { ComunasEntity } from './entity/comunas.entity';
import { EscuelaConductoresEntity } from './entity/escuela-conductores.entity';
import { RegionesEntity } from './entity/regiones.entity';
import { EscuelaConductoresService } from './services/escuela-conductores.service';

@Module({
  imports: [TypeOrmModule.forFeature([EscuelaConductoresEntity, ComunasEntity, RegionesEntity, User, RolesUsuarios])],
  providers: [EscuelaConductoresService, AuthService],
  exports: [EscuelaConductoresService],
  controllers: [EscuelaConductoresController]
})
export class EscuelaConductoresModule {}
