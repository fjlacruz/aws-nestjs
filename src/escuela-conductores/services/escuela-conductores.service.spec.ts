import { Test, TestingModule } from '@nestjs/testing';
import { EscuelaConductoresService } from './escuela-conductores.service';

describe('EscuelaConductoresService', () => {
  let service: EscuelaConductoresService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EscuelaConductoresService],
    }).compile();

    service = module.get<EscuelaConductoresService>(EscuelaConductoresService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
