import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { CreateEscuelaConductoresDTO } from '../DTO/createEscuelaConductores.dto';
import { EscuelaConductorDto } from '../DTO/escuelaConductor.dto';
import { RegionDto } from '../DTO/region.dto';
import { ComunaDTO } from '../DTO/comuna.dto';
import { ComunasEntity } from '../entity/comunas.entity';
import { EscuelaConductoresEntity } from '../entity/escuela-conductores.entity';
import { RegionesEntity } from '../entity/regiones.entity';
import { AuthService } from 'src/auth/auth.service';


@Injectable()
export class EscuelaConductoresService {

    constructor
    (
        private authService: AuthService,

        @InjectRepository(EscuelaConductoresEntity) 
        private readonly escuelaConductoresRespository: Repository<EscuelaConductoresEntity>,
        
        @InjectRepository(ComunasEntity) 
        private readonly comunasRespository: Repository<ComunasEntity>,

        @InjectRepository(RegionesEntity) 
        private readonly regionesRespository: Repository<RegionesEntity>
    )
    {}

    async getEscuelasConductores() {
        return await this.escuelaConductoresRespository.find();
    }


    async getEscuelaConductores(id:number){
        const escuela= await this.escuelaConductoresRespository.findOne(id);
        if (!escuela)throw new NotFoundException("Escuela Conductor dont exist");

        const escuelaConductor = new EscuelaConductorDto();
        escuelaConductor.idEscuelaConductor =  escuela.idEscuelaConductor;
        escuelaConductor.RUT =  escuela.RUT;
        escuelaConductor.DV = escuela.DV;
        escuelaConductor.NombreEscuela = escuela.NombreEscuela;
        escuelaConductor.Calle = escuela.Calle;
        escuelaConductor.CalleNro = escuela.CalleNro;
        escuelaConductor.Letra = escuela.Letra;
        escuelaConductor.RestoDireccion = escuela.RestoDireccion;
        escuelaConductor.created_at = escuela.created_at;
        escuelaConductor.updated_at = escuela.updated_at;
        escuelaConductor.idUsuario = escuela.idUsuario;

        const comuna= await this.getComuna(escuelaConductor.idComuna);

        const region= await this.getRegion(escuelaConductor.idComuna);
        const regionDto = new RegionDto;
        regionDto.idRegion = region.idRegion;
        regionDto.Nombre = region.Nombre;
        regionDto.Descripcion = region.Descripcion;

        regionDto.idRegion=region.idRegion;
        regionDto.Nombre=region.Nombre;
        regionDto.Descripcion=region.Descripcion;

        const comunaDTO = new ComunaDTO;
        comunaDTO.idComuna=comuna.idComuna;
        comunaDTO.Nombre=comuna.Nombre;
        comunaDTO.Descripcion=comuna.Descripcion;
        comunaDTO.idRegion=comuna.idRegion;
        comunaDTO.region=regionDto;

        escuelaConductor.comuna = comunaDTO;
    
        return escuelaConductor;
    }

    async update(idEscuelaConductor: number, dto: Partial<CreateEscuelaConductoresDTO> ) 
    {
        const idUsuario   = (await this.authService.usuario()).idUsuario;
        const updated_at  = new Date();
        const data        = { ...dto, updated_at, idUsuario  };

        await this.escuelaConductoresRespository.update({ idEscuelaConductor }, data );
    
        return await this.escuelaConductoresRespository.findOne({ idEscuelaConductor });
      }
      
    async create(dto: CreateEscuelaConductoresDTO): Promise<EscuelaConductoresEntity> 
    {
        const idUsuario   = (await this.authService.usuario()).idUsuario;
        const created_at  = new Date();
        const updated_at  = created_at;
        const data        = { ...dto, created_at, updated_at, idUsuario  };

        return await this.escuelaConductoresRespository.save( data );
    }


    async getComuna(id:number){
        const region= await this.comunasRespository.findOne(id);
        if (!region)throw new NotFoundException("comuna dont exist");
        
        return region;
    }

    async getRegion(id:number){
        const region= await this.regionesRespository.findOne(id);
        if (!region)throw new NotFoundException("region dont exist");
        
        return region;
    }

}
