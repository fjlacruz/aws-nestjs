import { ApiProperty } from "@nestjs/swagger";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('EstadosDocumentoDigital')
export class EstadosEDDEntity {

    @PrimaryGeneratedColumn()
    IdEstadoEDD: number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;
    
    @Column()
    CodigoRC:string;
    
    @Column()
    idEstadoCarabinero:number;

    @Column()
    estadoIncial:boolean;

    @ApiProperty()
    @OneToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.estadosDD)
    readonly documentosLicencia: DocumentosLicencia;
}
