import { ApiProperty } from "@nestjs/swagger";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('EstadosDocumentoFisico')
export class EstadosEDFEntity {

    @PrimaryGeneratedColumn()
    IdEstadoEDF: number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;
    
    @Column()
    CodigoRC:string;
    
    @Column()
    idEstadoCarabinero:number;

    @Column()
    estadoIncial:boolean;
    
    @ApiProperty()
    @OneToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.estadosDF)
    readonly documentosLicencia: DocumentosLicencia;
}
