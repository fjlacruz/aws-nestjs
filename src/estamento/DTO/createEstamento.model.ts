import { ApiPropertyOptional } from "@nestjs/swagger";

export class createEstamento {

    @ApiPropertyOptional()
    idEstamentos: number;
    @ApiPropertyOptional()
    nombre: string;
    @ApiPropertyOptional()
    descripcion: string;
}