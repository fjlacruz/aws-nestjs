export class EstamentoDTO {
    idEstamento: number;
    nombre: string;
    descripcion: string;
}