import { Test, TestingModule } from '@nestjs/testing';
import { EstamentoController } from './estamento.controller';

describe('EstamentoController', () => {
  let controller: EstamentoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EstamentoController],
    }).compile();

    controller = module.get<EstamentoController>(EstamentoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
