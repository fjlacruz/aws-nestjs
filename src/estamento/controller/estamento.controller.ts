import { Controller, Get } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { EstamentoService } from '../services/estamento.service';

@ApiTags('Estamento')
@Controller('Estamento')
export class EstamentoController
{
    constructor( private estamentoService: EstamentoService )
    {}

    @Get('/Estamento')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los estamentos' })
    async getAll()
    {
        return this.estamentoService.getAll().then(data =>
        {
            return { code: 200, data };
        });
    }

}
