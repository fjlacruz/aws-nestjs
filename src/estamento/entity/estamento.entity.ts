import { ApiPropertyOptional } from "@nestjs/swagger";
import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity('Estamentos')
export class EstamentoEntity {

    @PrimaryColumn()
    idEstamentos: number;

    @Column()
    nombre: string;
    
    @Column()
    descripcion: string;

}