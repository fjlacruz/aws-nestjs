import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EstamentoController } from './controller/estamento.controller';
import { EstamentoEntity } from './entity/estamento.entity';
import { EstamentoService } from './services/estamento.service';

@Module({
  imports: [TypeOrmModule.forFeature([EstamentoEntity])],
  providers: [EstamentoService],
  exports: [EstamentoService],
  controllers: [EstamentoController]
})
export class EstamentoModule {}
