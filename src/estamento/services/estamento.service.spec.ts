import { EstamentoService } from './estamento.service';
import { Test, TestingModule } from '@nestjs/testing';

describe('EstamentoService', () => {
  let service: EstamentoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EstamentoService],
    }).compile();

    service = module.get<EstamentoService>(EstamentoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
