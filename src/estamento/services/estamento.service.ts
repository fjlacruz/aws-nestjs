import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { EstamentoEntity } from '../entity/estamento.entity';
import { EstamentoDTO } from '../DTO/estamento.dto';

@Injectable()
export class EstamentoService {
    constructor(@InjectRepository(EstamentoEntity) private readonly estamentoRespository: Repository<EstamentoEntity>) { }

    async getAll(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoEstamento: EstamentoEntity[] = [];

        try {
            resultadoEstamento = await this.estamentoRespository.find();

            if (Array.isArray(resultadoEstamento) && resultadoEstamento.length) {
                resultado.Respuesta = resultadoEstamento;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Estamentos obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Estamentos';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Estamentos';
        }

        return resultado;
    }
}
