import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateExamenDto {
    
    @ApiPropertyOptional()
    idExamenTeorico: number;
    @ApiPropertyOptional()
    idPostulante: number;
    @ApiPropertyOptional()
    idSolicitud: number;
    @ApiPropertyOptional()
    FolioRendicion:string;
    @ApiPropertyOptional()
    ClaseLicencia:string;
    @ApiPropertyOptional()
    FechaRendicion: Date;
    @ApiPropertyOptional()
    Aprobado: boolean;
    @ApiPropertyOptional()
    Historico: boolean;
    @ApiPropertyOptional()
    Puntaje: number;
    @ApiPropertyOptional()
    ExaminadorID: number;
    @ApiPropertyOptional()
    timepoRendicionMin: number;
    // @ApiPropertyOptional()
    // idUsuario: number;
    // @ApiPropertyOptional()
    // created_at: Date;
    // @ApiPropertyOptional()
    // updated_at: Date;

}