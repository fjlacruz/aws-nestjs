import { Test, TestingModule } from '@nestjs/testing';
import { ExamenTeoricoController } from './examen-teorico.controller';

describe('ExamenTeoricoController', () => {
  let controller: ExamenTeoricoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExamenTeoricoController],
    }).compile();

    controller = module.get<ExamenTeoricoController>(ExamenTeoricoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
