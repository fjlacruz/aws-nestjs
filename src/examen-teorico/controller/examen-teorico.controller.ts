import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateExamenDto } from '../DTO/createExamen.dto';
import { ExamenTeoricoService } from '../services/examen-teorico.service';

@ApiTags('Examenes-Teoricos')
@Controller('examen-teorico')
export class ExamenTeoricoController {

    constructor(private readonly examenService: ExamenTeoricoService) { }

    @Get('allExamenes')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Examenes' })
    async getExamenes() {
        const data = await this.examenService.getExamenes();
        return { data };
    }

    @Get('exmanenById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Examen por Id' })
    async getExamen(@Param('id') id: number) {
        const data = await this.examenService.getExamen(id);
        return { data };
    }

    @Get('examenByidPostulante/:idPostulante')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Exmanen por Postulante' })
    async getExamenByidPostulante(@Param('idPostulante') idPostulante: number) {
        
        return await this.examenService.getExamenByidPostulante(idPostulante);
    }

    @Post('creaExamenTeorico')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Examen Teorico' })
    async addExamenTeorico(
        @Body() createExamenDto: CreateExamenDto) {
    
        const data = await this.examenService.create(createExamenDto);
        return {data};

    }

    @Get('allExamenesJoinPostulante')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Examenes con datos del Postulante' })
    async getExamenesWhitPostulante() {
        const data = await this.examenService.fetch();
        return { data };
    }

    @Patch('examenUpdate/:idExamenTeorico')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos Aprobado de Examen Teorico' })
    async updateExamenById(
        @Param('idExamenTeorico') idExamenTeorico: number, 
        @Body() createExamenDto: CreateExamenDto) {
        const data = await this.examenService.updateExamen(idExamenTeorico,createExamenDto.Aprobado);        
        
        return { data };
  }
}
