import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('ExamenesTeoricos')
export class ExamenTeorico {

    @PrimaryGeneratedColumn()
    idExamenTeorico: number;
    
    @Column()
    idPostulante: number;

    @Column()
    idSolicitud: number;

    @Column()
    FolioRendicion:string;

    @Column()
    ClaseLicencia:string;

    @Column()
    FechaRendicion: Date;
    
    @Column()
    Aprobado: boolean;

    @Column()
    Historico: boolean;

    @Column()
    Puntaje: number;

    @Column()
    ExaminadorID: number;

    @Column()
    timepoRendicionMin: number;

    @Column()
    idUsuario: number;
        
    @Column()
    created_at: Date;

    @Column()
    updated_at: Date;

}