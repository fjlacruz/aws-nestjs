import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { ExamenTeoricoController } from './controller/examen-teorico.controller';
import { ExamenTeorico } from './entity/examen.entity';
import { ExamenTeoricoService } from './services/examen-teorico.service';

@Module({
  imports: [TypeOrmModule.forFeature([ExamenTeorico, User, RolesUsuarios])],
  controllers: [ExamenTeoricoController],
  exports: [ExamenTeoricoService],
  providers: [ExamenTeoricoService, AuthService]
})
export class ExamenTeoricoModule {}
