import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { Repository } from 'typeorm';
import { CreateExamenDto } from '../DTO/createExamen.dto';
import { ExamenTeorico } from '../entity/examen.entity';

@Injectable()
export class ExamenTeoricoService 
{

    constructor
    (
        private readonly authService: AuthService,

        @InjectRepository(ExamenTeorico) 
        private readonly examenRespository: Repository<ExamenTeorico>
    )
    {}

    async getExamenes() 
    {
        const idOficina = this.authService.oficina().idOficina;

        return await this.examenRespository
            .createQueryBuilder( "exam" )
            .innerJoin( Solicitudes, "sol", "sol.idSolicitud = exam.idSolicitud" )
            .where(     "sol.idOficina = :idOficina", { idOficina } )
            .getMany();
    }

    async getExamen(id:number){
        const examen= await this.examenRespository.findOne(id);
        if (!examen)throw new NotFoundException("examen teorico dont exist");
        
        return examen;
    }

    async getExamenByidPostulante(idPostulante: number): Promise<ExamenTeorico> {
        return await this.examenRespository.findOne({ where: { idPostulante } });
    }

    async create( dto: CreateExamenDto ): Promise<ExamenTeorico>
    {
        const idUsuario  = (await this.authService.usuario()).idUsuario;
        const created_at = new Date();
        const updated_at = created_at;
        const data       = { ...dto, idUsuario, created_at, updated_at }; 

        return await this.examenRespository.save( data );
    }

    public fetch(): Promise<any> {
        const idOficina = this.authService.oficina().idOficina; 

        return this.examenRespository
            .createQueryBuilder('ExamenTeorico')
            .innerJoinAndMapOne('ExamenTeorico.idPostulante', Postulantes, 'post', 'ExamenTeorico.idPostulante = post.idPostulante')
            .innerJoin( Solicitudes, "sol", "sol.idSolicitud = ExamenTeorico.idSolicitud" )
            .where(     "sol.idOficina = :idOficina", { idOficina })       
            .getMany(); 
      }

    async updateExamen(idExamenTeorico: number, Aprobado:boolean) {
        console.log()
        await this.examenRespository.createQueryBuilder()
            .update(ExamenTeorico)
            .set({Aprobado: Aprobado})
            .where({idExamenTeorico})            
            .execute();
        
        return await this.examenRespository.findOne({ idExamenTeorico });
        
      }

}
