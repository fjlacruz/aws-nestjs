import { ApiPropertyOptional } from "@nestjs/swagger";
import { Column } from 'typeorm';

export class DocumentosDto {
    
    @ApiPropertyOptional()
    idDocumento: number;
    @ApiPropertyOptional()
    NombreDocumento:string;
    @ApiPropertyOptional()
    DocFolio:string;
    @ApiPropertyOptional()
    created_at:Date;
    @ApiPropertyOptional()
    update_at:Date;
    @ApiPropertyOptional()
    File:Buffer;
    @ApiPropertyOptional()
    FileLargeObject:Buffer;
    @ApiPropertyOptional()
    VirtualPath:string;
    @ApiPropertyOptional()
    idTipoDocumento:number;
    @ApiPropertyOptional()
    idUsuario:number;
    @ApiPropertyOptional()
    idOficina:number;
    @ApiPropertyOptional()
    ResumenDocumento:string;
    @ApiPropertyOptional()
    FechaPublicacion:any;
    @ApiPropertyOptional()
    estado:any;
}
