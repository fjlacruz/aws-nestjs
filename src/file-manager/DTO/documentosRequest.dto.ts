import { ApiPropertyOptional } from '@nestjs/swagger';

export class DocumentosRequest {
  @ApiPropertyOptional()
  idDocumento: number;
  @ApiPropertyOptional()
  NombreDocumento: string;
  @ApiPropertyOptional()
  DocFolio: string;
  @ApiPropertyOptional()
  File: Buffer;
  @ApiPropertyOptional()
  FileLargeObject: Buffer;
  @ApiPropertyOptional()
  VirtualPath: string;
  @ApiPropertyOptional()
  idTipoDocumento: number;
  @ApiPropertyOptional()
  ResumenDocumento: string;
  @ApiPropertyOptional()
  FechaPublicacion: Date;
  @ApiPropertyOptional()
  tipo: number;
  @ApiPropertyOptional()
  estado: boolean;
}
