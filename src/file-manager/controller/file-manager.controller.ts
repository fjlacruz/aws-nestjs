import { Controller, DefaultValuePipe, ParseIntPipe, Query, Req } from '@nestjs/common';
import { Get, Param, Post, Body } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { FileManagerService } from '../services/file-manager.service';
import { ApiBearerAuth } from '@nestjs/swagger';
import { DocumentosRequest } from '../DTO/documentosRequest.dto';
import { DocumentosDto } from '../DTO/documentos.dto';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';

@ApiTags('file-manager')
@Controller('file-manager')
export class FileManagerController {
  constructor(private readonly fileManagerService: FileManagerService, private readonly authService: AuthService) { }

  @Get('documentos')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los Documentos' })
  async getDocumentos(
    @Req() req: any,
    @Query('NombreDocumento') NombreDocumento: string,
    @Query('FechaPublicacion') FechaPublicacion: string,
    @Query('ResumenDocumento') ResumenDocumento: string,
    @Query('estado') estado: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {

    let splittedBearerToken = req.headers.authorization.split(" ");
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [
      PermisosNombres.VisualizarListaDescargaDocumentosRepositorioInformacion
    ]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    const data: any = await this.fileManagerService.getDocumentos(
      NombreDocumento,
      FechaPublicacion,
      ResumenDocumento,
      validarPermisos.ResultadoOperacion ? estado : 'true', // si tiene permisos de edicion manda el estado recibido del front, de lo contrario solo listara los publicados
      {
        page,
        limit,
        route: '/resultado-examinacion/getListaPendientesAtencion',
      },
      orden.toString(),
      ordenarPor.toString()
    );
    data.editarDocs = validarPermisos.ResultadoOperacion
    return { data };
  }

  @Get('documentoById/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Documento por Id' })
  async getDocumentoById(@Param('id') id: number, @Req() req: any) {
    const data = await this.fileManagerService.getDocumento(req, id);
    return { data };
  }

  @Post('uploadDocumento')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Registro Pago' })
  async uploadDocumento(@Body() documentosRequest: DocumentosRequest, @Req() req: any) {
    const dto = new DocumentosDto();

    dto.NombreDocumento = documentosRequest.NombreDocumento;
    dto.DocFolio = documentosRequest.DocFolio;
    dto.VirtualPath = documentosRequest.VirtualPath;
    dto.idTipoDocumento = documentosRequest.idTipoDocumento;
    dto.FechaPublicacion = documentosRequest.FechaPublicacion;
    dto.ResumenDocumento = documentosRequest.ResumenDocumento;
    dto.File = documentosRequest.File;
    dto.estado = documentosRequest.estado;

    if (documentosRequest.tipo == 3) {
      const filePath = await this.fileManagerService.getFilePath();
      dto.VirtualPath = filePath.Value;
      dto.File = null;
      dto.FileLargeObject = null;

      var path = filePath.Value + '/' + dto.NombreDocumento;
      const buffer = dto.File;

      const fsPromises = require('fs').promises;
      await fsPromises.writeFile(path, buffer);
    }

    if (documentosRequest.tipo == 2) {
      dto.File = documentosRequest.File;
      dto.FileLargeObject = null;
    }

    if (documentosRequest.tipo == 1) {
      dto.File = null;
      dto.FileLargeObject = documentosRequest.File;
    }
    const data = await this.fileManagerService.createDocumento(req, dto);
    return { data };
  }

  @Post('updateDocumento')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que modifica la metadatos de documentos' })
  async updateDocumento(@Body() documentosRequest: DocumentosRequest, @Req() req) {
    const dto = new DocumentosDto();
    dto.NombreDocumento = documentosRequest.NombreDocumento;
    dto.DocFolio = documentosRequest.DocFolio;
    dto.VirtualPath = documentosRequest.VirtualPath;
    dto.idTipoDocumento = documentosRequest.idTipoDocumento;
    dto.FechaPublicacion = documentosRequest.FechaPublicacion;
    dto.ResumenDocumento = documentosRequest.ResumenDocumento;
    dto.File = documentosRequest.File;
    dto.estado = documentosRequest.estado;

    if (documentosRequest.tipo == 3) {
      const filePath = await this.fileManagerService.getFilePath();
      dto.VirtualPath = filePath.Value;
      dto.File = null;
      dto.FileLargeObject = null;

      var path = filePath.Value + '/' + dto.NombreDocumento;
      const buffer = dto.File;

      const fsPromises = require('fs').promises;
      await fsPromises.writeFile(path, buffer);
    }

    if (documentosRequest.tipo == 2) {
      dto.File = documentosRequest.File;
      dto.FileLargeObject = null;
    }

    if (documentosRequest.tipo == 1) {
      dto.File = null;
      dto.FileLargeObject = documentosRequest.File;
    }

    const data = await this.fileManagerService.update(req, documentosRequest.idDocumento, dto);
    return { data };
  }

  @Get('deleteDocumentoById/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina un Documento por Id' })
  async deleteDocumentoById(@Param('id') id: number, @Req() req) {
    const data = await this.fileManagerService.delete(req, id);
    return data;
  }

  @Get('habilitar/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina un Documento por Id' })
  async habilitarDocumento(@Param('id') id: number) {
    return await this.fileManagerService.habilitarDeshabilitar(id, true);
  }

  @Get('deshabilitar/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina un Documento por Id' })
  async deshabilitarDocumento(@Param('id') id: number) {
    return await this.fileManagerService.habilitarDeshabilitar(id, false);
  }
}
