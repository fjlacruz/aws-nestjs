import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('Documentos')
export class DocumentosEntity {
  @PrimaryGeneratedColumn()
  idDocumento: number;

  @Column()
  NombreDocumento: string;

  @Column()
  DocFolio: string;

  @Column()
  created_at: Date;

  @Column()
  update_at: Date;

  @Column({
    name: 'File',
    type: 'bytea',
    nullable: false,
  })
  File: Buffer;

  @Column({
    name: 'FileLargeObject',
    type: 'bytea',
    nullable: false,
  })
  FileLargeObject: Buffer;

  @Column()
  VirtualPath: string;

  @Column()
  idTipoDocumento: number;

  @Column()
  idUsuario: number;

  @Column()
  idOficina: number;

  @Column()
  ResumenDocumento: string;

  @Column('timestamp without time zone')
  FechaPublicacion: Date;

  @Column()
  estado: boolean;
}
