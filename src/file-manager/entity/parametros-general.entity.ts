import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn} from "typeorm";

@Entity('ParametrosGeneral')
export class ParametrosGeneralEntity {

    @PrimaryColumn()
    idParametro:number;

    @Column()
    Param:string;

    @Column()
    Value:string;

    @Column()
    Descripcion:string;
}
