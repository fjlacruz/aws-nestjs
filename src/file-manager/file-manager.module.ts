import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { FileManagerController } from './controller/file-manager.controller';
import { DocumentosEntity } from './entity/documentos.entity';
import { FileManagerService } from './services/file-manager.service';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentosEntity, User, RolesUsuarios])],
  providers: [FileManagerService, AuthService],
  exports: [FileManagerService],
  controllers: [FileManagerController]
})
export class FileManagerModule {}
