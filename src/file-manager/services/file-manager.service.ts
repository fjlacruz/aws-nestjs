import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Repository, getConnection, getRepository } from 'typeorm';
import { DocumentosDto } from '../DTO/documentos.dto';
import { DocumentosEntity } from '../entity/documentos.entity';
import { ParametrosGeneralEntity } from '../entity/parametros-general.entity';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import * as moment from 'moment';
import { PermisosNombres } from 'src/constantes';

@Injectable()
export class FileManagerService {
  constructor(
    private readonly authService: AuthService,
    @InjectRepository(DocumentosEntity)
    private readonly documentosRespository: Repository<DocumentosEntity>
  ) {}

  async getDocumentos(
    NombreDocumento: string,
    FechaPublicacion: string,
    ResumenDocumento: string,
    estado: string,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ) {
    const documentos = await getConnection()
      .createQueryBuilder()
      .select([
        'Documentos.idDocumento',
        'Documentos.NombreDocumento',
        'Documentos.DocFolio',
        'Documentos.VirtualPath',
        'Documentos.idTipoDocumento',
        'Documentos.idUsuario',
        'Documentos.idOficina',
        'Documentos.ResumenDocumento',
        'Documentos.FechaPublicacion',
        'Documentos.estado',
      ])
      .from(DocumentosEntity, 'Documentos');

    if (orden === 'NombreDocumento' && ordenarPor === 'ASC') documentos.orderBy('Documentos.NombreDocumento', 'ASC');
    if (orden === 'NombreDocumento' && ordenarPor === 'DESC') documentos.orderBy('Documentos.NombreDocumento', 'DESC');
    if (orden === 'FechaPublicacion' && ordenarPor === 'ASC') documentos.orderBy('Documentos.FechaPublicacion', 'ASC');
    if (orden === 'FechaPublicacion' && ordenarPor === 'DESC') documentos.orderBy('Documentos.FechaPublicacion', 'DESC');
    if (orden === 'ResumenDocumento' && ordenarPor === 'ASC') documentos.orderBy('Documentos.ResumenDocumento', 'ASC');
    if (orden === 'ResumenDocumento' && ordenarPor === 'DESC') documentos.orderBy('Documentos.ResumenDocumento', 'DESC');

    if (NombreDocumento)
      documentos.andWhere('LOWER(Documentos.NombreDocumento) like LOWER(:NombreDocumento)', { NombreDocumento: `%${NombreDocumento}%` });
    if (ResumenDocumento)
      documentos.andWhere('LOWER(Documentos.ResumenDocumento) like LOWER(:ResumenDocumento)', {
        ResumenDocumento: `%${ResumenDocumento}%`,
      });
    if (FechaPublicacion) {
      documentos.andWhere('DATE(Documentos.FechaPublicacion) = :FechaPublicacion', { FechaPublicacion: FechaPublicacion });
    }
    if (estado === 'true') documentos.andWhere('Documentos.estado = :estado', { estado: true });
    if (estado === 'false') documentos.andWhere('Documentos.estado = :estado', { estado: false });
    if (estado === 'null') documentos.andWhere('Documentos.estado IS NULL');
    return paginate<any>(documentos, options);
  }

  async getDocumento(req, id: number) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.DescargadeDocumentosRepositoriodeInformacionVisualizarDocumento);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	  

    const documento = await this.documentosRespository.findOne(id);
    if (!documento) throw new NotFoundException('documneto dont exist');
    return documento;
  }

  async createDocumento(req, dto: DocumentosDto): Promise<DocumentosEntity> {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.DescargadeDocumentosRepositoriodeInformacionCrearNuevoDocumento);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return null;
    }	

    const idUsuario = (await this.authService.usuario()).idUsuario;
    const idOficina = null; // this.authService.oficina().idOficina; // comentado hasta resolver el tema de id de oficina
    const created_at = new Date();
    const update_at = created_at;
    const data: any = { ...dto, idOficina, idUsuario, created_at, update_at };
    data.FechaPublicacion = moment(dto.FechaPublicacion.toString(), 'YYYY-MM-DD').toDate();
    return await this.documentosRespository.save(data);
  }

  async getFilePath() {
    const parametro = await getRepository(ParametrosGeneralEntity)
      .createQueryBuilder('ParametrosGeneral')
      .where("ParametrosGeneral.Param = 'pathFile'")
      .getOne();
    if (!parametro) throw new NotFoundException('parametro dont exist');
    return parametro;
  }

  async update(req, idDocumento: number, dto: Partial<DocumentosDto>) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.DescargadeDocumentosRepositoriodeInformacionSeleccionarAccionesEditarEliminar);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return null;
    }	

    const idUsuario = (await this.authService.usuario()).idUsuario;
    const idOficina = null; // this.authService.oficina().idOficina; // comentado hasta resolver el tema de id de oficina
    const update_at = new Date();
    const data = { ...dto, idOficina, idUsuario, update_at };
    await this.documentosRespository.update({ idDocumento }, data);
    return await this.documentosRespository.findOne({ idDocumento });
  }

  async delete(req, id: number) {
    try {

      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.DescargadeDocumentosRepositoriodeInformacionSeleccionarAccionesEditarEliminar);
    
      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return null;
      }	

      await getConnection().createQueryBuilder().delete().from(DocumentosEntity).where('idDocumento = :id', { id: id }).execute();
      return { code: 200, message: 'success' };
    } catch (error) {
      return { code: 400, error: error };
    }
  }

  async habilitarDeshabilitar(id: number, habilitar: boolean) {
    try {
      const documento = await this.documentosRespository.findOne({ idDocumento: id });
      documento.estado = habilitar;
      await this.documentosRespository.save(documento);
      return { code: 200, message: 'success' };
    } catch (error) {
      return { code: 400, error: error };
    }
  }
}
