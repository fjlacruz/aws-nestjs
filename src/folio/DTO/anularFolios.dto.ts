import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Exclude, Expose } from "class-transformer";
import {IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { DocumentoDTO } from "src/prorrogas/dto/documento.dto";
import { Column, IsNull } from "typeorm";



export class AnularFolioDTO {
  

@IsNotEmpty()   
fechaAnulacion:Date;

@IsNumber()
@IsNotEmpty()
idlotePapelSeguridad:number;

@IsNumber()
@IsNotEmpty()
motivo:number;

@IsString()
observacionMotivo:string;

@IsString()
@IsOptional()
RangoInicio:string;

@IsString()
@IsOptional()
RangoFin:string;

@IsOptional()
documentos?: DocumentoDTO[];

@IsNumber({},{each: true})
@IsOptional()
folios:number[];


}