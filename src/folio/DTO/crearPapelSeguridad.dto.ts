import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Column, IsNull } from 'typeorm';

export class CrearPapelSeguridadDTO {
  @IsNotEmpty()
  @IsNumber()
  idInstitucion: number;

  @IsNotEmpty()
  @IsNumber()
  idProveedor: number;

  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  LetrasFolio: string;

  @IsOptional()
  @IsNumber({ allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0 }, { message: 'RangoInicio debe ser un número' })
  RangoInicio: number;

  @IsOptional()
  @IsNumber({ allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0 }, { message: 'RangoFin debe ser un número' })
  RangoFin: number;

  @IsOptional()
  @IsNumber({ allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0 }, { message: 'numFolio debe ser un número' })
  numFolio: number;
}
