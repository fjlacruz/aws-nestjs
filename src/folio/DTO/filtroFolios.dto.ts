export class FiltroFoliosDTO
{
    nombre: string;
    letra: string;
    fabricante:number;
    estado:boolean;
    rangoDesde:string;
    rangoHasta:string;
    fechaCarga:Date;
}