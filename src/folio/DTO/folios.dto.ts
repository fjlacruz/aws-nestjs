import { Expose } from "class-transformer";

@Expose()
export class FoliosDTO{

    @Expose()
    idLotePapelSeguridad:number;

    @Expose()
    FoliosRango:string;
    
    @Expose()
    FabricantePapelSeguridad:string;
    
    @Expose()
    Estado:boolean;
    
    @Expose()
    FechaCarga:Date;

    @Expose()
    rangoInicial:number;

    @Expose()
    rangoFinal:number;

}

export class FolioDTO{
    
    @Expose()
    created_at:Date;

    @Expose()
    rango:string;

    @Expose()
    rangoInicial:number;

    @Expose()
    rangoFinal:number;

}