import { Exclude, Expose } from "class-transformer";

@Exclude()
export class MunicipalidadesUsuarioDTO
{
    @Expose()
    idInstitucion:number;

    @Expose()
    Nombre:string;

}