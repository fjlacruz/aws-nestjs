import { Body, Controller, Get, Param, Post, Req, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { CrearPapelSeguridadDTO } from '../DTO/crearPapelSeguridad.dto';
import { FolioService } from '../services/folio.service';
import { AuthService } from 'src/auth/auth.service';
import { AnularFolioDTO } from '../DTO/anularFolios.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { RolesUsuariosService } from 'src/roles-usuarios/services/roles-usuarios.service';

@ApiTags('Folio')
@Controller('folio')
@UsePipes(ValidationPipe)
export class FolioController {


    constructor
        (

            private readonly folioService: FolioService,
            private readonly authService: AuthService,
            private readonly rolesUsuariosService: RolesUsuariosService

        ) { }



    @Post('/guardarFolios')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio añade folios' })
    async addFolios(@Body() crearPapelSeguridad: CrearPapelSeguridadDTO, @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.AdministracionSGLCAdministraciondeFoliosCrearNuevoFolio
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.folioService.addFolios(crearPapelSeguridad, validarPermisos.Respuesta);
            return { data };

        } else {

            return { data: validarPermisos };

        }



    }

    @Post('/AnularFolios')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que anula folios' })
    async deleteFolios(@Body() anularFolios: AnularFolioDTO, @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.AdministracionSGLCAdministraciondeFoliosAnularFolio
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.folioService.anularFolio(anularFolios, validarPermisos.Respuesta);
            return { data };

        } else {

            return { data: validarPermisos };

        }

    }

    @Post('/obtenerLotesFolios')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un listado con de Folios' })
    async getLotesFolios(
        @Body() paginacionArgs: PaginacionArgs, @Req() req: any) {


        const data = await this.folioService.getListaFolios(req, paginacionArgs);

        return { data };


    }

    @Get('/obtenerMunicipalidadesUsuario/')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una municipalidad del usuario ' })
    async getMunicipalidadesUsuario(@Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaAdministracionInformacionDeFolios
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

            const data = await this.folioService.obtenerMunicipalidadesUsuario(permisos);
            return { data };

        } else {

            return { data: validarPermisos };

        }

    }

    @Get('/obtenerMunicipalidadesUsuario2/')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una municipalidad del usuario ' })
    async getMunicipalidadesUsuario2(@Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaAdministracionInformacionDeFolios
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];
            let roles = await this.rolesUsuariosService.getDatosRolesUsuario(req, validarPermisos.Respuesta[0].idUsuario)
            console.log("-------------------ROLES----------------------------")
            console.log(roles.Respuesta)
            console.log("-------------------ROLES----------------------------")
            const data = await this.folioService.obtenerMunicipalidadesUsuario2(roles.Respuesta);
            return { data };

        } else {

            return { data: validarPermisos };

        }

    }

    @Get('/getOficinaByMunicipalidad/:idMunicipalidad')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una municipalidad del usuario ' })
    async getOficinaByMunicipalidad(@Param('idMunicipalidad') idMunicipalidad: number, @Req() req: any) {
        // let splittedBearerToken = req.headers.authorization.split(" ");
        // let token = splittedBearerToken[1];

        // let tokenPermisos = new TokenPermisoDto(token, [
        //     PermisosNombres.GestionarFolios
        // ]);

        // let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        // if (validarPermisos.ResultadoOperacion) {

            //const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

            const data = await this.folioService.getOficinaByMunicipalidad(idMunicipalidad);
            return { data };

        // } else {

        //     return { data: validarPermisos };

        // }

    }


    @Post('/obtenerFoliosLoteDeshabilitados/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los folios deshabilitados para un lote ' })
    async getFoliosEstadoPorLote(@Param('id') id: number, @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaAdministracionInformacionDeFolios
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.folioService.obtenerFoliosEstadoPorLote(id);
            return { data };

        } else {

            return { data: validarPermisos };

        }

    }


}
