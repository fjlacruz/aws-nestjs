import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity('AnulacionesFolio')
export class AnulacionesFolioEntity 
{

    @PrimaryGeneratedColumn()
    idAnulacionFolio: number;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    idlotePapelSeguridad:number;
    

    @Column()
    @IsNotEmpty()
    @IsNumber()
    anuladoPor:number;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    motivo:number;

    @Column()
    @IsString()
    observacion:string;

    @Column()
    @IsNotEmpty()
    @IsDate()
    fechaAnulacion:Date;

    @Column({
        type: 'bytea',
        nullable: false
    })
    archivo?: Buffer;

}