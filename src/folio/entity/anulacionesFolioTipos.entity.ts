import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity('anulacionesFoliosTipoEntity')
export class AnulacionesFolioTipoEntity 
{

    @PrimaryGeneratedColumn()
    idAnulacionesFolioTipo: number;

    @Column()
    @IsNotEmpty()
    @IsString()
    descripcion:string;
   

}