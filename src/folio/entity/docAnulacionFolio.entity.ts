import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity('DocAnulacionFolio')
export class DocAnulacionFolioEntity
{

    @PrimaryGeneratedColumn()
    idDocAnulacionFolio: number;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    idAnulacionFolio:number;
    
    @Column()
    @IsNotEmpty()
    nombreDoc:string;

    @Column({ type: "bytea"})
    @IsNotEmpty()
    archivo: Buffer;

    @Column()
    @IsString()
    created_at: Date;

}