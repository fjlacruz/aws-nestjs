import { ApiProperty } from "@nestjs/swagger";
import { InstitucionesEntity } from "src/tramites/entity/instituciones.entity";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne, OneToMany} from "typeorm";
import { isBoolean, IsBoolean, IsDate, isDate, IsNotEmpty, IsNumber, isNumber, IsString, isString } from "class-validator";
import { PapelSeguridadFolioEntity } from "./papelSeguridad.entity";
import { Proveedor } from "src/proveedor/entities/proveedor.entity";

@Entity('lotePapelSeguridad')
export class LotePapelSeguridadEntity {

    @PrimaryGeneratedColumn()
    idlotePapelSeguridad: number;
        
    @Column()
    @IsNotEmpty()
    @IsNumber()
    idInstitucion: number;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    idProveedor:number;
    @ManyToOne(()=> Proveedor,proveedor=> proveedor.lote)
    @JoinColumn({name:'idProveedor'})
    readonly proveedor: Proveedor

    @Column()
    @IsNotEmpty()
    @IsNumber()
    ingresadoPor:number;

    @Column()
    @IsNotEmpty()
    @IsBoolean()
    habilitado:boolean;

    @Column()
    @IsNotEmpty()
    @IsString()
    descripcion:string;

    @Column()
    @IsDate()
    @IsNotEmpty()
    created_at:Date;

    @Column()
    @IsDate()
    @IsNotEmpty()
    updated_at:Date;


    @ApiProperty()
    @OneToMany(()=> PapelSeguridadFolioEntity,papelSeguridad => papelSeguridad.lote)
    readonly papelSeguridad:PapelSeguridadFolioEntity[];



}