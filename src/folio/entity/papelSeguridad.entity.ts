import { ApiProperty } from "@nestjs/swagger";
import { InstitucionesEntity } from "src/tramites/entity/instituciones.entity";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne, OneToMany} from "typeorm";
import { isBoolean, IsBoolean, IsDate, isDate, IsNotEmpty, IsNumber, isNumber, IsString, isString } from "class-validator";
import {LotePapelSeguridadEntity} from './lotePapelSeguridad.entity';

@Entity('PapelSeguridad')
export class PapelSeguridadFolioEntity {

    @PrimaryGeneratedColumn()
    idPapelSeguridad: number;
        
    @Column()
    @IsString()
    @IsNotEmpty()
    Folio:string;

    @Column()
    idInstitucion: number;
    @ManyToOne(() => InstitucionesEntity, instituciones => instituciones.papelSeguridad)
    @JoinColumn({ name: 'idInstitucion' })
    readonly instituciones: InstitucionesEntity;
    
    @Column()
    @IsString()
    @IsNotEmpty()
    LetrasFolio: string;

    @Column()
    @IsBoolean()
    @IsNotEmpty()
    Asignado: boolean;

    @Column()
    @IsDate()
    @IsNotEmpty()
    created_at:Date;

    @Column()
    @IsDate()
    @IsNotEmpty()
    updated_at:Date;

    @Column()
    @IsNotEmpty()
    @IsBoolean()
    habilitado:boolean;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    idLotePapelSeguridad:number;
    @ManyToOne(()=> LotePapelSeguridadEntity,lote => lote.papelSeguridad)
    @JoinColumn({name:'idLotePapelSeguridad'})
    readonly lote: LotePapelSeguridadEntity;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    idAnulacionFolio:number;

    
    @ApiProperty()
    @OneToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.papelSeguridad)
    readonly documentosLicencia: DocumentosLicencia;

}