import { IsDate, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('registroAnulacionPapelSeguridad')
export class RegistroAnulacionPapelSeguridadEntity
{

    @PrimaryGeneratedColumn()
    idRegistroAnulacionPapelSeguridad: number;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    idAnulacionFolio:number;

    @Column()
    @IsNotEmpty()
    @IsNumber()
    idPapelSeguridad:number;


}