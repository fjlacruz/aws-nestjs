import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { PapelSeguridadEntity } from 'src/papel-seguridad/entity/papel-seguridad.entity';
import { Proveedor } from 'src/proveedor/entities/proveedor.entity';
import { RolActivaRol } from 'src/rol-activa-rol/entity/rol-activa-rol.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { RolesUsuariosService } from 'src/roles-usuarios/services/roles-usuarios.service';
import { TipoInstitucionEntity } from 'src/tipo-institucion/entity/tipo-institucion.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { jwtConstants } from 'src/users/constants';
import { User } from 'src/users/entity/user.entity';
import { UsersModule } from 'src/users/users.module';
import { UtilService } from 'src/utils/utils.service';
import { FolioController } from '../folio/controller/folio.controller';
import { FolioService } from '../folio/services/folio.service';
import { AnulacionesFolioEntity } from './entity/anulacionesFolio.entity';
import { LotePapelSeguridadEntity } from './entity/lotePapelSeguridad.entity';
import { PapelSeguridadFolioEntity } from './entity/papelSeguridad.entity';
import { RegistroAnulacionPapelSeguridadEntity } from './entity/registroAnulacionPapelSeguridad.entity';

@Module({
  controllers: [FolioController],
  providers: [ RolesUsuariosService,FolioService,AuthService,UtilService],
  imports:
  [
    TypeOrmModule.forFeature(
    [
      Proveedor,
      OficinasEntity,
      User,
      RolesUsuarios,
      PapelSeguridadFolioEntity,
      RegistroAnulacionPapelSeguridadEntity,
      AnulacionesFolioEntity,
      LotePapelSeguridadEntity,
      RolActivaRol,
      TipoInstitucionEntity,
      
    ]),
    UsersModule,
    
  ]
})
export class FolioModule {}
