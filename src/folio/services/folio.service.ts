import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { getConnection, QueryRunner, Repository } from 'typeorm';
import { PapelSeguridadFolioEntity } from '../entity/papelSeguridad.entity';
import { CrearPapelSeguridadDTO } from '../DTO/crearPapelSeguridad.dto';
import { LotePapelSeguridadEntity } from '../entity/lotePapelSeguridad.entity';
import { classToClass, classToPlain, plainToClass } from 'class-transformer';
import _, { forEach, forIn, orderBy, result } from 'lodash';
import { AnularFolioDTO } from '../DTO/anularFolios.dto';
import { AnulacionesFolioEntity } from '../entity/anulacionesFolio.entity';
import { RegistroAnulacionPapelSeguridadEntity } from '../entity/registroAnulacionPapelSeguridad.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { FiltroFoliosDTO } from '../DTO/filtroFolios.dto';
import { paginate, paginateRaw, paginateRawAndEntities, Pagination } from 'nestjs-typeorm-paginate';
import { permisosRol } from 'src/shared/ValidarPermisosResultadoRol';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { FolioDTO, FoliosDTO } from '../DTO/folios.dto';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { Proveedor } from 'src/proveedor/entities/proveedor.entity';
import { ColumnasFolios, PermisosNombres, TipoInstitucion_Municipalidad, tipoRolOficina, tipoRolRegion, tipoRolSuperUsuarioID } from 'src/constantes';
import { In } from 'typeorm';
import { MunicipalidadesUsuarioDTO } from '../DTO/municipalidadesUsuario.dto';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { DocAnulacionFolioEntity } from '../entity/docAnulacionFolio.entity';
import { RolesUsuariosDTO } from 'src/roles-usuarios/DTO/roles-usuarios.dto';
import { AuthService } from 'src/auth/auth.service';
import { PapelSeguridadEntity } from 'src/papel-seguridad/entity/papel-seguridad.entity';
import { UtilService } from 'src/utils/utils.service';
var range = require('lodash.range');

@Injectable()
export class FolioService {

  constructor
    (
      @InjectRepository(PapelSeguridadFolioEntity) private readonly papelSeguridadRepository: Repository<PapelSeguridadFolioEntity>,
      @InjectRepository(RegistroAnulacionPapelSeguridadEntity) private readonly registroAnulacionPapelSeguridadRepository: Repository<RegistroAnulacionPapelSeguridadEntity>,
      @InjectRepository(LotePapelSeguridadEntity) private readonly LotePapelSeguiradaRepository: Repository<LotePapelSeguridadEntity>,
      @InjectRepository(OficinasEntity) private readonly OficinasRepository: Repository<OficinasEntity>,
      @InjectRepository(Proveedor) private readonly ProveedorRepositorio: Repository<Proveedor>,
      private readonly utilService: UtilService,
      private readonly authService: AuthService
    ) { }


  async addFolios(crearPapelSeguridadDTO: CrearPapelSeguridadDTO, permisos: Object): Promise<Resultado> {
    //Iniciamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    let esSuperusuario = false;


    let roles = await permisosRol(permisos);
    let userId: number = roles[0].idUsuario;

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {

      roles.forEach(rolUsuario => {
        if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID)
          esSuperusuario = true;
      });

      //Validamos que la letra es correcta para el folio proveedor
      let obtenerLetraFolio = await this.validarLetraFolio(crearPapelSeguridadDTO.LetrasFolio, crearPapelSeguridadDTO.idProveedor);

      if (!obtenerLetraFolio) {
        queryRunner.release();
        resultado.Error = "La letra folio no es correcta"

        return resultado;

      }

      let rango = null;
      //Obtenemos los rangos
      if (crearPapelSeguridadDTO.numFolio) {
        rango = await this.generarRangos(crearPapelSeguridadDTO.numFolio.toString(), null);
      } else {
        rango = await this.generarRangos(crearPapelSeguridadDTO.RangoInicio.toString(), crearPapelSeguridadDTO.RangoFin.toString());
      }

      //validamos si el rango ya existe
      let validarSiYaExisteRango = await this.validarRango(rango, crearPapelSeguridadDTO);

      if (!validarSiYaExisteRango) {
        resultado.Error = "Ya existe un lote o folios para ese rango y proveedor";
        await queryRunner.release();
        return resultado;
      }

      // if (!esSuperusuario){
      //   //Si no es SuperUsuario, validamos la institucion para comprobar si tiene permisos
      //   let validarInstitucion: Boolean = await this.validarInstitucion(roles, crearPapelSeguridadDTO.idInstitucion);

      //   if (!validarInstitucion) {
      //     resultado.Error = "No tienes permisos en la Institucion";
      //     await queryRunner.release();
      //     return resultado;
      //   }
      // }

      //Insertamos el lote
      let loteInsertar: LotePapelSeguridadEntity = new LotePapelSeguridadEntity();
      loteInsertar.idInstitucion = crearPapelSeguridadDTO.idInstitucion;
      loteInsertar.idProveedor = crearPapelSeguridadDTO.idProveedor;
      loteInsertar.descripcion = crearPapelSeguridadDTO.nombre;
      loteInsertar.created_at = new Date;
      loteInsertar.updated_at = new Date;
      loteInsertar.ingresadoPor = userId;
      loteInsertar.habilitado = true;


      await queryRunner.manager.save(LotePapelSeguridadEntity, loteInsertar)

      //Almacenamos cada uno de los folios
      let almacenarRangos = await this.almacenarRangosFolios(queryRunner, rango, crearPapelSeguridadDTO, loteInsertar.idlotePapelSeguridad, userId);

      if (!almacenarRangos.ResultadoOperacion) {

        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        resultado.Error = "Error al crear el lote";
        return resultado;
      }

      //Si todo es correcto hacemos commint
      queryRunner.commitTransaction();
      resultado.ResultadoOperacion = true;
      resultado.Mensaje = "Lote creado correctamente";


    }
    catch (error) {
      //Capturamos el error
      queryRunner.rollbackTransaction();

      resultado.Error = "Error al añadir los folios";

    }
    finally {
      queryRunner.release();
    }
    return resultado;
  }
  async validarLetraFolio(LetrasFolio: string, idProveedor: number): Promise<boolean> {
    let resultado: boolean = false;

    let codigo = await (await this.ProveedorRepositorio.findOne({ idProveedor: idProveedor })).codigo;

    if (codigo == LetrasFolio) {

      resultado = true;
    }

    return resultado;

  }



  /**
   * generamos un array de rangos de folios 
   * @param RangoInicio 
   * @param RangoFin 
   * @returns 
   */
  async generarRangos(RangoInicio: string, RangoFin: string): Promise<number[]> {

    if (RangoFin == null) {
      RangoFin = RangoInicio;
    }

    return range(+RangoInicio, +RangoFin + 1);

  }


  /**
   * Validamos si existe el rango ya en la BD
   * @param crearPapelSeguridadDTO 
   */
  async validarRango(rango: number[], crearPapelSeguridadDTO: CrearPapelSeguridadDTO): Promise<boolean> {

    let resultado: boolean = false;

    try {

      const folios = await this.papelSeguridadRepository.createQueryBuilder('papelSeguridad').leftJoinAndSelect('papelSeguridad.lote', 'lote').where("papelSeguridad.Folio IN (:...rangos)", { rangos: rango }).andWhere("lote.idProveedor= :id", { id: crearPapelSeguridadDTO.idProveedor }).getMany();
      if (folios.length <= 0 || !folios) {
        resultado = true;
      }



    } catch (error) {
      console.log(error)
    }


    return resultado;

  }

  /**
   * Validamos que el usuario pertenezca a la institucion
   * @param roles 
   * @param idInstitucion 
   * @returns 
   */
  async validarInstitucion(roles: RolesUsuarios[], idInstitucion: number): Promise<boolean> {
    let resultado: boolean = false;



    try {
      let region = roles.map(r => r.idOficina);
      let validarInstitucion = await this.OficinasRepository.createQueryBuilder().leftJoinAndSelect('OficinasEntity.instituciones', 'instituciones').where('OficinasEntity.idOficina IN (:...region)', { region: region }).andWhere('OficinasEntity.idInstitucion = :id', { id: idInstitucion }).andWhere('instituciones.idTipoInstitucion = :idTipo', { idTipo: TipoInstitucion_Municipalidad }).getMany();
      if (validarInstitucion.length > 0) {
        resultado = true;
      }


    }
    catch (error) {
      console.log(error);
    }

    return resultado;
  }


  //Creamos el papel y el rango de seguridada
  /**
   * Método para crear lo distintos folios de papel seguridad según el rango
   * @param queryRunner la conexión de la transaccion
   * @param crearPapelSeguridadDTO el objeto creación con los atributos necesarios
   * @param idLotePapelSeguridad id del lote a los que se asignan lo folios papel seguridad
   * @param idUsuario usuario que crea los documentos
   * @returns 
   */
  async almacenarRangosFolios(queryRunner: QueryRunner, rango: number[], crearPapelSeguridadDTO: CrearPapelSeguridadDTO, idLotePapelSeguridad: number, idUsuario: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {

      const listaPapelSeguridad: PapelSeguridadFolioEntity[] = [];
      //let rango = range(+crearPapelSeguridadDTO.RangoInicio, +crearPapelSeguridadDTO.RangoFin);


      for (const page of rango) {
        let folioAdd: PapelSeguridadFolioEntity = new PapelSeguridadFolioEntity();
        folioAdd.Asignado = false;
        folioAdd.habilitado = true;
        folioAdd.created_at = new Date;
        folioAdd.updated_at = new Date;
        folioAdd.LetrasFolio = crearPapelSeguridadDTO.LetrasFolio;
        //folioAdd.idInstitucion = crearPapelSeguridadDTO.idInstitucion;
        folioAdd.Folio = page.toString();
        folioAdd.idLotePapelSeguridad = idLotePapelSeguridad;

        listaPapelSeguridad.push(folioAdd);


      }

      const loteInsert = await queryRunner.manager.save(PapelSeguridadFolioEntity, listaPapelSeguridad);
      if (loteInsert) {
        resultado.ResultadoOperacion = true;
      }

    }
    catch (error) {

      console.log(error);

    }

    return resultado

  }


  async anularFolio(anularFolio: AnularFolioDTO, permisos: Object): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = true;

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    let roles = await permisosRol(permisos);
    let userId = roles[0].idUsuario;

    try {

      //Iniciamos el objeto de rangos
      let rangos: number[] = [];

      //Comprobamos si son folios concreto o un rango de de ellos
      if (anularFolio.folios) {

        rangos.push.apply(rangos, anularFolio.folios);


      }
      if (anularFolio.RangoInicio && anularFolio.RangoFin) {

        //Obtenemos los rangos
        const listaRangos = await this.generarRangos(anularFolio.RangoInicio, anularFolio.RangoFin);

        rangos.push.apply(rangos, listaRangos);


      }

      //Obtenemos los folios segun rango y lote
      const folios = await this.papelSeguridadRepository.createQueryBuilder('papelSeguridad').where("papelSeguridad.Folio IN (:...rangos)", { rangos: rangos }).andWhere("papelSeguridad.habilitado = true").andWhere("papelSeguridad.idLotePapelSeguridad =" + anularFolio.idlotePapelSeguridad).getMany();

      //Si no obtenemos ninguno no se puede realizar la operacion
      if (folios.length <= 0) {
        resultado.Error = "No existen los folios o ya están anulados";
        return resultado;
      }

      //guardamos las anulaciones
      let guardaAnulaciones = await this.guardarAnulaciones(queryRunner, anularFolio, userId);

      if (!guardaAnulaciones.ResultadoOperacion) {
        queryRunner.rollbackTransaction();
        queryRunner.release();
        resultado.Error = "Error al guardar el registro de anulaciones";
        return resultado;
      }


      //conseguimos el id anulacion de la respuesta
      var idAnulacion = +guardaAnulaciones.Respuesta["idAnulacionFolio"];



      for (const itemFolio of folios) {

        if (itemFolio.habilitado == true) {
          let itemActualizar: Partial<PapelSeguridadFolioEntity> = new PapelSeguridadFolioEntity();
          itemActualizar.updated_at = anularFolio.fechaAnulacion;
          itemActualizar.habilitado = false;
          itemActualizar.idAnulacionFolio = idAnulacion;
          await queryRunner.manager.update(PapelSeguridadFolioEntity, itemFolio.idPapelSeguridad, itemActualizar);
          await this.generarRegistroAnulacion(queryRunner, itemFolio.idPapelSeguridad, idAnulacion);


        }

      }

      //comprobamos si queda algún rango de folio del lote activado
      const loteRestante = await queryRunner.manager.getRepository(PapelSeguridadFolioEntity)
        .createQueryBuilder("papelSeguridad").where("papelSeguridad.idLotePapelSeguridad =" + anularFolio.idlotePapelSeguridad)
        .andWhere("papelSeguridad.habilitado = true").getOne();

      //Si no lo actualizamos al estado deshabilitado
      if (!loteRestante) {

        const updateLote = await queryRunner.manager.getRepository(LotePapelSeguridadEntity).createQueryBuilder().useTransaction(true)
          .update().set({ habilitado: false, updated_at: anularFolio.fechaAnulacion }).where("idlotePapelSeguridad= :id", { id: anularFolio.idlotePapelSeguridad }).execute();

        if (updateLote.affected < 0) {

          await queryRunner.rollbackTransaction()
          await queryRunner.release();
          resultado.Error = "Error al anular el Lote";
          return resultado;
        }

      }

      //enviamos el commit
      await queryRunner.commitTransaction();
      resultado.Mensaje = "Folios Anulados correctamente";
      resultado.ResultadoOperacion = true;


    }
    catch (error) {
      //Capturamos el error
      await queryRunner.rollbackTransaction();
      console.log(error)
      resultado.Error = "Error al anular folios";

    }
    finally {
      await queryRunner.release();
    }

    return resultado;
  }

  /**
   * Método para guardar las anulaciones folio
   * @param queryRunner transaccion conexion
   * @param anularFolio los datos del folio para anular
   * @param userId el id del usuario logeado
   * @returns 
   */
  async guardarAnulaciones(queryRunner: QueryRunner, anularFolio: AnularFolioDTO, userId: number): Promise<Resultado> {

    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      let anulacionFolio: AnulacionesFolioEntity = new AnulacionesFolioEntity();
      anulacionFolio.anuladoPor = userId;
      anulacionFolio.fechaAnulacion = anularFolio.fechaAnulacion;
      anulacionFolio.idlotePapelSeguridad = anularFolio.idlotePapelSeguridad;
      anulacionFolio.motivo = anularFolio.motivo;
      anulacionFolio.observacion = anularFolio.observacionMotivo ?? null;
      //anulacionFolio.archivo = anularFolio.archivo ? Buffer.from(anularFolio.archivo) : null;

      await queryRunner.manager.save(AnulacionesFolioEntity, anulacionFolio);

      anularFolio.documentos.forEach(async doc => {
        let docEntity: DocAnulacionFolioEntity = new DocAnulacionFolioEntity()

        docEntity.idAnulacionFolio = anulacionFolio.idAnulacionFolio
        docEntity.nombreDoc = doc.nombreDoc
        docEntity.archivo = doc.archivo
        docEntity.created_at = new Date()

        await queryRunner.manager.save(DocAnulacionFolioEntity, docEntity)
      })


      resultado.ResultadoOperacion = true;
      resultado.Respuesta = anulacionFolio;

    }
    catch (error) {
      console.log(error);
    }
    return resultado;
  }

  /**
   * método para regenerar el registro de los lotes papeles anulados
   * @param queryRunner conexion de la transaccion 
   * @param idPapelSeguridad id del papel a deshabilitar
   * @param idAnulacion id de la anulacion del lote
   */
  async generarRegistroAnulacion(queryRunner: QueryRunner, idPapelSeguridad: number, idAnulacion: number) {

    const registroAnulacion: RegistroAnulacionPapelSeguridadEntity = new RegistroAnulacionPapelSeguridadEntity();
    registroAnulacion.idAnulacionFolio = idAnulacion;
    registroAnulacion.idPapelSeguridad = idPapelSeguridad;

    await queryRunner.manager.save(RegistroAnulacionPapelSeguridadEntity, registroAnulacion);

  }

  async getListaFolios(req: any, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    //Iniciamos la respuesta
    console.log(paginacionArgs.orden.orden)
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    const orderBy = ColumnasFolios[paginacionArgs.orden.orden];
    let foliosParseados = new PaginacionDtoParser<FoliosDTO>();
    const filtro: FiltroFoliosDTO = paginacionArgs.filtro;
    let filtroBuscar = paginacionArgs.filtroBusqueda;

    let foliosPags: Pagination<LotePapelSeguridadEntity>;

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaAdministracionInformacionDeFolios);

      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }


      const foliosPagsQuery = this.LotePapelSeguiradaRepository.
        createQueryBuilder('LotePapelSeguridadEntity')
        .innerJoinAndMapMany('LotePapelSeguridadEntity.papelSeguridad', PapelSeguridadFolioEntity, 'papelSeguridad', 'LotePapelSeguridadEntity.idlotePapelSeguridad = papelSeguridad.idLotePapelSeguridad')
        .innerJoinAndMapOne('LotePapelSeguridadEntity.proveedor', Proveedor, 'proveedor', 'LotePapelSeguridadEntity.idProveedor = proveedor.idProveedor')
        

        // // Inner join según order by para no provocar lentitud en la consulta
        // if(paginacionArgs && paginacionArgs.orden && paginacionArgs.orden.orden == "nFinal"){
        //   foliosPagsQuery.innerJoinAndMapOne('LotePapelSeguridadEntity.papelSeguridadMax', PapelSeguridadFolioEntity, 'papelSeguridadMax', 'LotePapelSeguridadEntity.idlotePapelSeguridad = papelSeguridadMax.idLotePapelSeguridad')
        
        //   // Para obtener entidad con el máximo del valor de papel seguridad
        //   foliosPagsQuery.andWhere(' CAST("papelSeguridadMax"."Folio" AS INTEGER) IN ' +
        //                                       '(SELECT MAX(CAST("papelSeguridadFiltroMaximo"."Folio" AS INTEGER)) ' +
        //                                       'FROM "PapelSeguridad" "papelSeguridadFiltroMaximo" ' + 
        //                                       'WHERE "papelSeguridadFiltroMaximo"."idLotePapelSeguridad" = "LotePapelSeguridadEntity"."idlotePapelSeguridad" ' +
        //                                       'GROUP BY "papelSeguridadFiltroMaximo"."idLotePapelSeguridad") ');         
        // }

        // // Inner join según order by para no provocar lentitud en la consulta
        // if(orderBy && orderBy != null && paginacionArgs.orden.orden == "nInicial"){
        //   foliosPagsQuery.innerJoinAndMapOne('LotePapelSeguridadEntity.papelSeguridadMin', PapelSeguridadFolioEntity, 'papelSeguridadMin', 'LotePapelSeguridadEntity.idlotePapelSeguridad = papelSeguridadMin.idLotePapelSeguridad')
        
        //   // Para obtener entidad con el mínimo del valor de papel seguridad
        //   foliosPagsQuery.andWhere(' CAST("papelSeguridadMin"."Folio" AS INTEGER) IN ' +
        //                                       '(SELECT MIN(CAST("papelSeguridadFiltroMaximo"."Folio" AS INTEGER)) ' +
        //                                       'FROM "PapelSeguridad" "papelSeguridadFiltroMaximo" ' + 
        //                                       'WHERE "papelSeguridadFiltroMaximo"."idLotePapelSeguridad" = "LotePapelSeguridadEntity"."idlotePapelSeguridad" ' +
        //                                       'GROUP BY "papelSeguridadFiltroMaximo"."idLotePapelSeguridad") ');         
        // }        

                                            

        if(filtro && filtro != null && filtro.nombre != null){
          foliosPagsQuery.where(
            'lower(unaccent("LotePapelSeguridadEntity"."descripcion")) LIKE lower(unaccent(:nombre)) ', { nombre: `%${filtro.nombre}%` })
        }

        if(filtro && filtro != null && filtro.letra != null){
          foliosPagsQuery.andWhere(
            'lower(unaccent("proveedor"."codigo"))  LIKE lower(unaccent(:letra)) ', { letra: `%${filtro.letra}%` })
        }        

        if(filtro && filtro != null && filtro.fabricante > 0){
          foliosPagsQuery.andWhere(
            '"LotePapelSeguridadEntity"."idProveedor" = :id', { id: filtro.fabricante })
        } 

        if(filtro && filtro != null && filtro.estado != null){
          foliosPagsQuery.andWhere(
            '"LotePapelSeguridadEntity"."habilitado" = :habilitado ', { habilitado: filtro.estado })
        } 

        if(filtro && filtro != null && filtro.fechaCarga != null){
          foliosPagsQuery.andWhere(
            `DATE_TRUNC('day', "LotePapelSeguridadEntity"."created_at") = :fechaFinSolicitud`, { fechaFinSolicitud: filtro.fechaCarga })
        } 


        if(filtro && filtro != null  && filtro.rangoDesde && filtro.rangoHasta ){
            const rangoMinQuery = await getConnection().manager.query('SELECT "papelSeguridadFiltroMinimo"."idLotePapelSeguridad" FROM "PapelSeguridad" "papelSeguridadFiltroMinimo" GROUP BY "papelSeguridadFiltroMinimo"."idLotePapelSeguridad" HAVING MIN(CAST("papelSeguridadFiltroMinimo"."Folio" AS INTEGER)) = $1', [
              (filtro.rangoDesde) ? filtro.rangoDesde : null
            ]);

            if(rangoMinQuery && rangoMinQuery.length > 0){
              foliosPagsQuery.andWhere('"LotePapelSeguridadEntity"."idlotePapelSeguridad" IN (:..._idsParaIncluir) ', { _idsParaIncluir: rangoMinQuery.map(x => x.idLotePapelSeguridad) })
            }
            else{
              foliosPagsQuery.andWhere('FALSE');
            }

            
            const rangoMaxQuery = await getConnection().manager.query('SELECT "papelSeguridadFiltroMaximo"."idLotePapelSeguridad" FROM "PapelSeguridad" "papelSeguridadFiltroMaximo" GROUP BY "papelSeguridadFiltroMaximo"."idLotePapelSeguridad" HAVING MAX(CAST("papelSeguridadFiltroMaximo"."Folio" AS INTEGER)) = $1', [
              (filtro.rangoHasta) ? filtro.rangoHasta : null
            ]);

            if(rangoMaxQuery && rangoMaxQuery.length > 0){
              foliosPagsQuery.andWhere('"LotePapelSeguridadEntity"."idlotePapelSeguridad" IN (:..._idsParaIncluir) ', { _idsParaIncluir: rangoMaxQuery.map(x => x.idLotePapelSeguridad) })
            }
            else{
              foliosPagsQuery.andWhere('FALSE');
            }  


          console.log('');
        }

        if(filtro && filtro != null  && ((filtro.rangoDesde && !filtro.rangoHasta) || (!filtro.rangoDesde && filtro.rangoHasta))){  
            let rangoBusqueda = (filtro.rangoDesde)?filtro.rangoDesde:filtro.rangoHasta;

            foliosPagsQuery.innerJoinAndMapOne('LotePapelSeguridadEntity.papelSeguridadMax', PapelSeguridadFolioEntity, 'papelSeguridadMax', 'LotePapelSeguridadEntity.idlotePapelSeguridad = papelSeguridadMax.idLotePapelSeguridad')            
            foliosPagsQuery.andWhere('"papelSeguridadMax"."Folio" like :_idParaIncluir ', { _idParaIncluir: rangoBusqueda })

        }             



        foliosPagsQuery.orderBy(orderBy, paginacionArgs.orden.ordenarPor);


        foliosPags = await paginate<LotePapelSeguridadEntity>(foliosPagsQuery, paginacionArgs.paginationOptions);
      



        //Cogemos los items de la respuesta
        if (Array.isArray(foliosPags.items) && foliosPags.items.length) {

          //Creamos el dto para mostrar los datos
          let listaFoliosDTO: FoliosDTO[] = [];

          for (const element of foliosPags.items) {

            var item: FoliosDTO = new FoliosDTO();
            // Para el estado comprobamos si todos los folios asociados tienen asociada una anulación, si no es así, asignamos el valor que viene del lote
            item.Estado = (element.papelSeguridad.every(x => x.idAnulacionFolio != null)) ? false : element.habilitado;
            item.FabricantePapelSeguridad = element.proveedor.nombre;
            item.FechaCarga = element.created_at;
            item.FoliosRango = element.descripcion;
            item.idLotePapelSeguridad = element.idlotePapelSeguridad;


            let rango : number[] = element.papelSeguridad.map(x => Number(x.Folio));
            item.rangoInicial = Math.min(...rango);
            item.rangoFinal = Math.max(...rango);

            listaFoliosDTO.push(item);

          }

        // ordenar el DTO
        //ordena solo la pagina actual
        if (paginacionArgs.orden.orden == "nInicial") {
          if(paginacionArgs.orden.ordenarPor == "ASC"){

            listaFoliosDTO.sort((a, b) => {
              return a.rangoInicial - b.rangoInicial;
            })


          }else{
            
            listaFoliosDTO.sort((a, b) => {
              return b.rangoInicial - a.rangoInicial;
            })


          }
         
        }
        
                //ordena solo la pagina actual
         if (paginacionArgs.orden.orden == "nFinal") {
          if(paginacionArgs.orden.ordenarPor == "ASC"){

            listaFoliosDTO.sort((a, b) => {
              return a.rangoFinal - b.rangoFinal;
            })


          }else{
            
            listaFoliosDTO.sort((a, b) => {
              return b.rangoFinal - a.rangoFinal;
            })


          }
         
        }          

          //parsemaos los item dto
          foliosParseados.items = listaFoliosDTO;

          foliosParseados.links = foliosPags.links;
          foliosParseados.meta = foliosPags.meta;

          //asignamos la respuesta
          resultado.Respuesta = foliosParseados;

          resultado.ResultadoOperacion = true;
          resultado.Mensaje = "Lotes Folios obtenidos correctamente";

        } else {

          resultado.Error = "No se encuentran Lotes Folios"
        }

    }
    catch (error) {
      console.log(error);
      resultado.Error = "Error al listar los Folios";

    }

    return resultado;

  }

  async getOficinaByMunicipalidad(idMunicipalidad: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado()

    try {
      resultado.Respuesta = await this.OficinasRepository.findOne({ where: { idInstitucion: idMunicipalidad } })
      resultado.ResultadoOperacion = true
    } catch (err) {
      resultado.Error = "Ha ocurrido un error"
    }

    return resultado;
  }

  async obtenerMunicipalidadesUsuario(permisos: RolesUsuarios[]): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    let esSuperusuario = false;
    // console.log(permisos)
    try {
      //let roles = await permisosRol(permisos);

      permisos.forEach(rolUsuario => {
        if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID)
          esSuperusuario = true;
      });
      /*  console.log("---------------------------------------------------------------")
       console.log(permisos[0].roles) 
       console.log("---------------------------------------------------------------") */
      var oficinas: number[] = [];
      var regiones: number[] = [];

      if (!esSuperusuario) {
        oficinas = permisos.filter(x => x.roles.tipoRoles.Nombre == tipoRolOficina).map(x => x.idOficina);
        regiones = permisos.filter(x => x.roles.tipoRoles.Nombre == tipoRolRegion).map(x => x.idRegion);
      }

      var municipalidades = await this.OficinasRepository.find({
        relations: ['instituciones', 'comunas'],
        where: qb => {
          qb.where(oficinas.length > 0 && !esSuperusuario ?
            'OficinasEntity.idOficina IN (:...idsOficina)' : 'TRUE', { idsOficina: oficinas }
          )
          /* .andWhere(regiones.length > 0 && !esSuperusuario ?
            'OficinasEntity__comunas.idRegion IN (:...idsRegiones)' : 'TRUE', { idsRegiones: regiones }
          ) */
        }
      });

      var municipaliadesInstitucion = municipalidades.map(x => x.instituciones);
      //console.log(municipalidades)
      let institucionesSinDuplicados: InstitucionesEntity[] = [];

      // Se eliminan duplicados
      municipaliadesInstitucion.forEach(municipaliad => {
        if (!institucionesSinDuplicados.find(x => x.idInstitucion == municipaliad.idInstitucion))
          institucionesSinDuplicados.push(municipaliad)
      });

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = plainToClass(MunicipalidadesUsuarioDTO, institucionesSinDuplicados);

    }
    catch (error) {
      console.log(error);
      resultado.Error = "Error al obtener lista de municipalidades";
    }

    return resultado;

  }
  //Promise<Resultado>
  async obtenerMunicipalidadesUsuario2(roles) {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = true;


    resultado.Respuesta = plainToClass(RolesUsuariosDTO, roles);

    return resultado;
  }

  async obtenerFoliosEstadoPorLote(id: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = true;

    let foliosLista: FolioDTO[] = []

    try {
      const folios = await this.papelSeguridadRepository.createQueryBuilder('papelSeguridad')
        .where("papelSeguridad.idLotePapelSeguridad =" + id)
        .andWhere("papelSeguridad.habilitado = false")
        .orderBy('papelSeguridad.idPapelSeguridad', 'ASC').getMany();
      folios.forEach(folio => {
        let newFolio: FolioDTO = new FolioDTO();

        newFolio.rango = folio.Folio;
        newFolio.created_at = folio.created_at;

        foliosLista.push(newFolio)
      });

      foliosLista = await this.foliosListaAgrupados(foliosLista);

      resultado.Mensaje = "Folios obtenidos correctamente";
      resultado.Respuesta = foliosLista;
    }
    catch (error) {
      console.log(error);
      resultado.Error = "Error al obtener el estado de los folios";
    }
    return resultado;
  }

  /**
   * Método para obtener los rangos consecutivos de una lista de números
   * @param foliosLista lista de numeros
   * @returns 
   */
  async foliosListaAgrupados(foliosLista: FolioDTO[]): Promise<any> {

    try {

      //indicamos la primera posicion e iniciamos la lista
      let length = 1;
      let list: FolioDTO[] = [];

      //recorremos la lista
      for (let i = 1; i <= foliosLista.length; i++) {

        //Comprobamos al diferencia entre el elemento actual y el previo
        if (i == foliosLista.length
          || +foliosLista[i].rango - +foliosLista[i - 1].rango != 1) {

          //Si el rango solo es de 1 lo metemos en la lista
          if (length == 1) {
            //list.push((foliosLista[i - length]).toString());
            let folioTemp = new FolioDTO();
            folioTemp.rango = foliosLista[i - length].rango
            folioTemp.created_at = foliosLista[i - length].created_at

            list.push(folioTemp)
          }
          else {
            //sino construimos el primer elemento del rangoy el elemento previo
            //list.push(foliosLista[i - length].rango + " - " + foliosLista[i - 1].rango);
            let folioTemp = new FolioDTO();
            folioTemp.rango = foliosLista[i - length].rango + " - " + foliosLista[i - 1].rango
            folioTemp.created_at = foliosLista[i - length].created_at

            list.push(folioTemp)
          }

          length = 1;
        }
        else {
          length++;
        }
      }
      return list;

    }
    catch (error) {
      console.log(error);
    }
  }
}


