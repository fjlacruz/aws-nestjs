import { ApiPropertyOptional } from "@nestjs/swagger";

export class RespuestasFormularioDto {

    @ApiPropertyOptional()
    data:any;

    @ApiPropertyOptional()
    permisoVerSeccionNoAdministrativa:any;


}