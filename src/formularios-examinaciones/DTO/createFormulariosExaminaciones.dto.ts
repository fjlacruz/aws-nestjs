import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateFormulariosExaminacionesDto {

    @ApiPropertyOptional()
    idFromularioExaminaciones:number;

    @ApiPropertyOptional()
    nombreFormulario:string;

    @ApiPropertyOptional()
    created_at:Date;

    @ApiPropertyOptional()
    requiereFirmaElectronica:boolean;

    @ApiPropertyOptional()
    idTipoTramite:number;

    @ApiPropertyOptional()
    idClaseLicencia:number;

    @ApiPropertyOptional()
    idTipoExaminacion:number;

    @ApiPropertyOptional()
    requerido:boolean;

}
