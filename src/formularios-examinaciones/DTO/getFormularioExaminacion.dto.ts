import { ApiPropertyOptional } from "@nestjs/swagger";

export class getFormulariosExaminacionesDto {

    @ApiPropertyOptional()
    idTipoExaminacion:number;

    @ApiPropertyOptional()
    idClaseLicencia:number;

}
