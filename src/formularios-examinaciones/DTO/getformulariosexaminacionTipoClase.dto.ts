import { ApiPropertyOptional } from "@nestjs/swagger";

export class getFormulariosExaminacionesTipoClaseDto {

    @ApiPropertyOptional()
    idTipoTramite:number;

    @ApiPropertyOptional()
    idTipoExaminacion:number;

    @ApiPropertyOptional()
    idClaseLicencia:number;

    @ApiPropertyOptional()
    idColaExaminacion:number;
    
}
