import { Test, TestingModule } from '@nestjs/testing';
import { FormulariosExaminacionesController } from './formularios-examinaciones.controller';

describe('FormulariosExaminacionesController', () => {
  let controller: FormulariosExaminacionesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FormulariosExaminacionesController],
    }).compile();

    controller = module.get<FormulariosExaminacionesController>(FormulariosExaminacionesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
