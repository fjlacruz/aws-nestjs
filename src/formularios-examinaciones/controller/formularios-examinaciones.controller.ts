import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { InsertQueryBuilder } from 'typeorm';
import { CreateFormulariosExaminacionesDto } from '../DTO/createFormulariosExaminaciones.dto';
import { getFormulariosExaminacionesDto } from '../DTO/getFormularioExaminacion.dto';
import { getFormulariosExaminacionesTipoClaseDto } from '../DTO/getformulariosexaminacionTipoClase.dto';
import { FormulariosExaminacionesService } from '../services/formularios-examinaciones.service';

@ApiTags('Formularios-examinaciones')
@Controller('formularios-examinaciones')
export class FormulariosExaminacionesController {
  constructor(private readonly formulariosExaminacionesService: FormulariosExaminacionesService) {}

  @Get()
  async getFormulariosExaminaciones() {
    const data = await this.formulariosExaminacionesService.getFormulariosExaminaciones();
    return { data };
  }

  @Get('getFormulariobyId/:idFromularioExaminaciones')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el formularios de examinacion por id' })
  async getColaExamPracticosPendientesResultado(@Param('idFromularioExaminaciones') idFromularioExaminaciones: number) {
    const data = await this.formulariosExaminacionesService.getFormulariobyId(idFromularioExaminaciones);
    console.log('-*------------------------------------------------------');
    return { data };
  }

  @Get('getRespuestasFormulario/:idFromularioExaminaciones/:idColaExaminacion/:idResultadoExam')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el formularios de examinacion por id' })
  async getRespuestasFormulario(
    @Param('idFromularioExaminaciones') idFromularioExaminaciones: number,
    @Param('idColaExaminacion') idColaExaminacion: number,
    @Param('idResultadoExam') idResultadoExam: number,
    @Req() req: any
  ) {
    const data = await this.formulariosExaminacionesService.getRespuestasFormulario(
      idFromularioExaminaciones,
      idColaExaminacion,
      idResultadoExam,
      req
    );
    return data ;
  }

  @Get('getFormulariobyidTipoExaminacion/:idTipoExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el formularios de examinacion por idTipoExaminacion' })
  async getFormulariobyidTipoExaminacion(@Param('idTipoExaminacion') idTipoExaminacion: number) {
    const data = await this.formulariosExaminacionesService.getFormulariobyidTipoExaminacion(idTipoExaminacion);
    return { data };
  }

  @Get('getFormularioExamByTipotramClaselic')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Form.Examiancion por Clase licencia y Tipo trámite' })
  async getFiltrado(@Query() query: getFormulariosExaminacionesTipoClaseDto) {
    //    async getFormularioExamByTipotramClaselic(@Param('idTipoTramite') idTipoTramite: number, @Param('idClaseLicencia') idClaseLicencia: number) {
    const data = await this.formulariosExaminacionesService.getFormularioExaminacionId(
      query.idTipoTramite,
      query.idTipoExaminacion,
      query.idClaseLicencia,
      query.idColaExaminacion
    );
    return { data };
  }
}
