import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('FormularioExaminacionClaseLicencia')
export class FormularioExaminacionClaseLicenciaEntity {

    @PrimaryColumn()
    idFormularioExamincionClaseLicencia:number;

    @Column()
    idClaseLicencia:number;

    @Column()
    idFormularioExaminaciones:number;

}