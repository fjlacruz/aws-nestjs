import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('TiposRespuestaPregunta')
export class TiposRespuestaPreguntaEntity {

    @PrimaryColumn()
    idTipoRespuesta:number;

    @Column()
    nombreTipo:string;
}
