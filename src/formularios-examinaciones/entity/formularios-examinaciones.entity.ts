import { ApiProperty } from "@nestjs/swagger";
import { ResultadoExaminacionEntity } from "src/resultado-examinacion/entity/resultado-Examinacion.entity";
import { TiposTramiteEntity } from "src/tipos-tramites/entity/tipos-tramite.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, ManyToOne, JoinColumn, OneToMany} from "typeorm";


@Entity('FormulariosExaminaciones')
export class FormulariosExaminacionesEntity {

    @PrimaryColumn()
    idFromularioExaminaciones:number;

    @Column()
    nombreFormulario:string;

    @Column()
    created_at:Date;

    @Column()
    requiereFirmaElectronica:boolean;

    @Column()
    idTipoTramite:number;
    @JoinColumn({ name: 'idTipoTramite' })
    @ManyToOne(() => TiposTramiteEntity, tiposTramite => tiposTramite.formulariosExaminaciones)
    readonly tiposTramite: TiposTramiteEntity[];

    @Column()
    idTipoExaminacion:number;

    @Column()
    requerido:boolean;

    @Column()
    btntext:string;

    @ApiProperty()
    @OneToMany(() => ResultadoExaminacionEntity, resultadoExaminacion => resultadoExaminacion.formularioExaminacion)
    readonly resultadoExaminacionCola: ResultadoExaminacionEntity[];

}
