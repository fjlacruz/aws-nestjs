import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('PreguntaFormularioExaminaciones')
export class PreguntaFormularioExaminacionesEntity {

    @PrimaryColumn()
    idPreguntaExam:number;

    @Column()
    pregunta:string;

    @Column()
    idFromularioExaminaciones:number;

    @Column()
    idTipoRespuesta:number;

    @Column()
    idSeccionFormulario:number;

    @Column()
    created_at:Date;

    @Column()
    visible:boolean;

    @Column()
    requerida:boolean;

    @Column()
    codigo:string;

    @Column()
    anchoMax4:number;

    @Column()
    orden:number;

    @Column()
    columna:number;

}
