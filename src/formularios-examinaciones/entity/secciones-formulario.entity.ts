import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('SeccionesFormulario')
export class SeccionesFormularioEntity {

    @PrimaryColumn()
    idSeccionFormulario:number;

    @Column()
    nombreSeccion:string;

    @Column()
    idSeccionParent:number;

    @Column()
    idformularioexaminaciones:number;
}

