import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { FormulariosExaminacionesController } from './controller/formularios-examinaciones.controller';
import { FormulariosExaminacionesEntity } from './entity/formularios-examinaciones.entity';
import { FormulariosExaminacionesService } from './services/formularios-examinaciones.service';

@Module({
  imports: [TypeOrmModule.forFeature([FormulariosExaminacionesEntity, ResultadoExaminacionEntity, ResultadoExaminacionColaEntity, User, RolesUsuarios])],
  providers: [FormulariosExaminacionesService, AuthService],
  exports: [FormulariosExaminacionesService],
  controllers: [FormulariosExaminacionesController]
})
export class FormulariosExaminacionesModule {}
