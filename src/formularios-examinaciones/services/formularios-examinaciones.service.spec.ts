import { Test, TestingModule } from '@nestjs/testing';
import { FormulariosExaminacionesService } from './formularios-examinaciones.service';

describe('FormulariosExaminacionesService', () => {
  let service: FormulariosExaminacionesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FormulariosExaminacionesService],
    }).compile();

    service = module.get<FormulariosExaminacionesService>(FormulariosExaminacionesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
