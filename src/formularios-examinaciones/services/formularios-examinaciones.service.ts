import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { getConnection, Repository } from 'typeorm';
import { CreateFormulariosExaminacionesDto } from '../DTO/createFormulariosExaminaciones.dto';
import { FormularioExaminacionClaseLicenciaEntity } from '../entity/FormularioExaminacionClaseLicencia.entity';
import { FormulariosExaminacionesEntity } from '../entity/formularios-examinaciones.entity';
import { PreguntaFormularioExaminacionesEntity } from '../entity/pregunta-formulario-examinaciones.entity';
import { SeccionesFormularioEntity } from '../entity/secciones-formulario.entity';
import { TiposRespuestaPreguntaEntity } from '../entity/TiposRespuestaPregunta.entity';
import { RespuestasFormularioExaminacionEntity } from 'src/respuestas-formulario-examinacion/entity/respuestas-formulario-examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { CreateResultadoexaminacionDto } from 'src/resultado-examinacion/DTO/createResultadoexaminacion.dto.';
import { ResultadoExaminacionColaDTO } from '../DTO/ResultadoExaminacionCola.dto';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { RespuestasFormularioDto } from '../DTO/RespuestasFormulario.dto';

@Injectable()
export class FormulariosExaminacionesService {
  constructor(
    private readonly authService: AuthService,

    @InjectRepository(FormulariosExaminacionesEntity)
    private readonly formulariosexamRespository: Repository<FormulariosExaminacionesEntity>,

    @InjectRepository(ResultadoExaminacionEntity)
    private readonly resulexaminRespository: Repository<ResultadoExaminacionEntity>,

    @InjectRepository(ResultadoExaminacionColaEntity)
    private readonly resultadoExaminacionColaEntity: Repository<ResultadoExaminacionColaEntity>
  ) {}

  async getFormulariosExaminaciones() {
    return await this.formulariosexamRespository.find();
  }

  async getFormulariosExam(id: number) {
    const formulario = await this.formulariosexamRespository.findOne(id);
    if (!formulario) throw new NotFoundException('formulario examinación dont exist');

    return formulario;
  }

  async update(idFromularioExaminaciones: number, data: Partial<CreateFormulariosExaminacionesDto>) {
    await this.formulariosexamRespository.update({ idFromularioExaminaciones }, data);
    return await this.formulariosexamRespository.findOne({ idFromularioExaminaciones });
  }

  async create(data: CreateFormulariosExaminacionesDto): Promise<FormulariosExaminacionesEntity> {
    return await this.formulariosexamRespository.save(data);
  }

  async getFormulariobyId(idFromularioExaminaciones: number) {
    return this.formulariosexamRespository
      .createQueryBuilder('FormulariosExaminaciones')
      .leftJoinAndMapMany(
        'FormulariosExaminaciones.idFromularioExaminaciones',
        SeccionesFormularioEntity,
        'sec',
        'FormulariosExaminaciones.idFromularioExaminaciones = sec.idformularioexaminaciones'
      )
      .leftJoinAndMapMany(
        'sec.idSeccionFormulario',
        PreguntaFormularioExaminacionesEntity,
        'pre',
        'sec.idSeccionFormulario = pre.idSeccionFormulario'
      )
      .innerJoinAndMapMany('pre.idTipoRespuesta', TiposRespuestaPreguntaEntity, 'tp', 'pre.idTipoRespuesta = tp.idTipoRespuesta')

      .where('FormulariosExaminaciones.idFromularioExaminaciones = :idFromularioExaminaciones', {
        idFromularioExaminaciones: idFromularioExaminaciones,
      })
      .orderBy('sec.orden', 'ASC')
      .addOrderBy('pre.idSeccionFormulario', 'ASC')
      .addOrderBy('pre.orden', 'ASC')
      .getMany();
  }

  async getRespuestasFormulario(idFromularioExaminaciones: number, idColaExaminacion: number, idResultadoExam: number, req:any) {

    // Agregar respuesta de permisos para permitir/no permitir ver sección a roles como administrativo municipal
    let respuesta : RespuestasFormularioDto = new RespuestasFormularioDto();

    // Agregar respuesta de permisos para permitir/no permitir ver sección a roles como administrativo municipal
    respuesta.data = await this.formulariosexamRespository
      .createQueryBuilder('FormulariosExaminaciones')
      .leftJoinAndMapMany(
        'FormulariosExaminaciones.ResultadoExam',
        ResultadoExaminacionEntity,
        'res',
        'FormulariosExaminaciones.idFromularioExaminaciones = res.idFormularioExaminaciones'
      )
      .leftJoinAndMapMany('res.ResultadoExaminacionCola', ResultadoExaminacionColaEntity, 're', 'res.idResultadoExam = re.idResultadoExam')

      .leftJoinAndMapMany(
        'res.RespuestasFormularioExaminacio',
        RespuestasFormularioExaminacionEntity,
        'rp',
        're.idResultadoExam = rp.idResultadoExam'
      )

      .where('FormulariosExaminaciones.idFromularioExaminaciones = :idFromularioExaminaciones', {
        idFromularioExaminaciones: idFromularioExaminaciones,
      })
      .andWhere('re.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
      .andWhere('res.idResultadoExam = :idResultadoExam', { idResultadoExam: idResultadoExam })
      .getMany();

      let usuarioValidadoPermiso = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarResultadosErrorExamenPracticoIngresoPostulante);

      respuesta.permisoVerSeccionNoAdministrativa = usuarioValidadoPermiso.ResultadoOperacion;

      return respuesta;      

  } 

  async getFormulariobyidTipoExaminacion(idTipoExaminacion: number) {
    return this.formulariosexamRespository
      .createQueryBuilder('FormulariosExaminaciones')
      .innerJoinAndMapMany(
        'FormulariosExaminaciones.idFromularioExaminaciones',
        PreguntaFormularioExaminacionesEntity,
        'pre',
        'FormulariosExaminaciones.idFromularioExaminaciones = pre.idFromularioExaminaciones'
      )
      .innerJoinAndMapMany('pre.idSeccionFormulario', SeccionesFormularioEntity, 'sec', 'pre.idSeccionFormulario = sec.idSeccionFormulario')
      .where('FormulariosExaminaciones.idTipoExaminacion = :id', { id: idTipoExaminacion })
      .getMany();
  }

  async getFormularioExaminacionClaseLicencia(idTipoTramite: number, idTipoExaminacion: number, idClaseLicencia: number) {
    const formulario = await this.formulariosexamRespository
      .createQueryBuilder('FormulariosExaminaciones')
      .innerJoinAndMapMany(
        'FormularioExaminacionClaseLicencia',
        FormularioExaminacionClaseLicenciaEntity,
        'fexcla',
        'FormulariosExaminaciones.idFromularioExaminaciones = fexcla.idFormularioExaminaciones'
      )
      .innerJoinAndMapMany(
        'FormulariosExaminaciones.idTipoTramite',
        TiposTramiteEntity,
        'titra',
        'FormulariosExaminaciones.idTipoTramite = titra.idTipoTramite'
      )
      .innerJoinAndMapMany('ClasesLicencias', ClasesLicenciasEntity, 'clic', 'fexcla.idClaseLicencia = clic.idClaseLicencia');

    if (idTipoTramite) formulario.where('FormulariosExaminaciones.idTipoTramite = :idTipoTramite', { idTipoTramite: idTipoTramite });
    if (idTipoExaminacion)
      formulario.where('FormulariosExaminaciones.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: idTipoExaminacion });
    if (idClaseLicencia) formulario.andWhere('fexcla.idClaseLicencia = :idClaseLicencia', { idClaseLicencia: idClaseLicencia });
    return formulario.getMany();
  }

  async getResultadoExaminacion(id: number) {
    return await getConnection()
      .createQueryBuilder()
      .select('ResultadoExaminacion')
      .from(ResultadoExaminacionEntity, 'ResultadoExaminacion')
      .where('ResultadoExaminacion.idFormularioExaminaciones = :id', { id: id })
      .getOne();
  }

  async getResultadoExaminacionCola(idResultadoExam: number, idColaExaminacion: number) {
    return await getConnection()
      .createQueryBuilder()
      .select('ResultadoExaminacionCola')
      .from(ResultadoExaminacionColaEntity, 'ResultadoExaminacionCola')
      .where('ResultadoExaminacionCola.idResultadoExam = :idResultadoExam', { idResultadoExam: idResultadoExam })
      .andWhere('ResultadoExaminacionCola.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
      .getOne();
  }

  async createResultadoExaminacion(dto: CreateResultadoexaminacionDto): Promise<ResultadoExaminacionEntity> {
    const UsuarioAprobador = (await this.authService.usuario()).idUsuario;
    const data = { ...dto, UsuarioAprobador };
    // @ts-ignore
    return await this.resulexaminRespository.save(data);
  }

  async createResultadoExaminacionCola(data: ResultadoExaminacionColaDTO): Promise<ResultadoExaminacionColaEntity> {
    return await this.resultadoExaminacionColaEntity.save(data);
  }

  async getFormularioExamByTipotramClaselic(
    idTipoTramite: number,
    idTipoExaminacion: number,
    idClaseLicencia: number,
    idColaExaminacion: number
  ) {
    const data = await this.getFormularioExaminacionClaseLicencia(idTipoTramite, idTipoExaminacion, idClaseLicencia);
    for (const f of data) {
      const id = f.idFromularioExaminaciones;
      const resultadoExaminacion = await this.getResultadoExaminacion(id);

      if (resultadoExaminacion === undefined) {
        const dto = new CreateResultadoexaminacionDto();
        dto.idFormularioExaminaciones = id;
        dto.aprobado = false;

        const resultado = await this.createResultadoExaminacion(dto);

        const resultadoCola = new ResultadoExaminacionColaDTO();
        resultadoCola.idResultadoExam = resultado.idResultadoExam;
        resultadoCola.idColaExaminacion = idColaExaminacion;
        const resultadoExaminacioColaSave = await this.createResultadoExaminacionCola(resultadoCola);
      } else {
        const resultadoExaminacioCola = await this.getResultadoExaminacionCola(resultadoExaminacion.idResultadoExam, idColaExaminacion);
        if (resultadoExaminacioCola === undefined) {
          const resultadoCola = new ResultadoExaminacionColaDTO();
          resultadoCola.idResultadoExam = resultadoExaminacion.idResultadoExam;
          resultadoCola.idColaExaminacion = idColaExaminacion;
          const resultadoExaminacioColaSave = await this.createResultadoExaminacionCola(resultadoCola);
        }
      }
    }

    const qb = await this.formulariosexamRespository
      .createQueryBuilder('FormulariosExaminaciones')
      .innerJoinAndMapMany(
        'FormulariosExaminaciones.idFromularioExaminaciones',
        FormularioExaminacionClaseLicenciaEntity,
        'fexcla',
        'FormulariosExaminaciones.idFromularioExaminaciones = fexcla.idFormularioExaminaciones'
      )
      .innerJoinAndMapMany(
        'FormulariosExaminaciones.resultadoExaminacion',
        ResultadoExaminacionEntity,
        're',
        're.idFormularioExaminaciones = FormulariosExaminaciones.idFromularioExaminaciones'
      )
      .innerJoinAndMapMany(
        'FormulariosExaminaciones.idTipoTramite',
        TiposTramiteEntity,
        'titra',
        'FormulariosExaminaciones.idTipoTramite = titra.idTipoTramite'
      )
      .innerJoinAndMapMany('fexcla.idClaseLicencia', ClasesLicenciasEntity, 'clic', 'fexcla.idClaseLicencia = clic.idClaseLicencia');

    if (idTipoTramite) qb.where('FormulariosExaminaciones.idTipoTramite = :idTipoTramite', { idTipoTramite: idTipoTramite });
    if (idTipoExaminacion)
      qb.where('FormulariosExaminaciones.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: idTipoExaminacion });
    if (idClaseLicencia) qb.andWhere('fexcla.idClaseLicencia = :idClaseLicencia', { idClaseLicencia: idClaseLicencia });
    return qb.getMany();
  }

  /**
   * Metodo para obtener el idFormularioExaminacion
   * @param idTipoTramite
   * @param idTipoExaminacion
   * @param idClaseLicencia
   * @param idColaExaminacion
   * @author JLopez
   */
  async getFormularioExaminacionId(idTipoTramite: number, idTipoExaminacion: number, idClaseLicencia: number, idColaExaminacion: number) {
    const data = await this.formulariosexamRespository.query(
      'select * from getFormularioExaminacionId(' +
        idTipoTramite +
        ',' +
        idClaseLicencia +
        ', ' +
        idColaExaminacion +
        ',' +
        idTipoExaminacion +
        ')'
    );
    return data;
  }
}
