import { Test, TestingModule } from '@nestjs/testing';
import { FotoLicenciaController } from './foto-licencia.controller';

describe('FotoLicenciaController', () => {
  let controller: FotoLicenciaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FotoLicenciaController],
    }).compile();

    controller = module.get<FotoLicenciaController>(FotoLicenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
