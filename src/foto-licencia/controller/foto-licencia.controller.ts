import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { FotoLicenciaService } from '../service/foto-licencia.service';

@ApiTags('Foto-licencia')
@Controller('foto-licencia')
export class FotoLicenciaController
{
    constructor( private fotoLicenciaService: FotoLicenciaService )
    {}

    @Get('/:idImagenLicencia')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve la imagen de la licencia indicada' })
    async getById( @Param( 'idImagenLicencia') id: number  )
    {
        return this.fotoLicenciaService.getById( id ).then( data => { return { code: 200, data }; });
    }
}
