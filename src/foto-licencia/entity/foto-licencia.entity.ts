import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn} from "typeorm";

@Entity('FotoLicencia')
export class FotoLicenciaEntity
{
    @PrimaryGeneratedColumn()
    idImagenLicencia: number;

    @Column({ type: 'bytea'})
    fileBinary: Buffer;

    @Column()
    created_at:Date;

    @Column()
    updated_at:Date;

    @Column()
    idUsuario:number;

    @Column()
    idConductor:number;
}    
