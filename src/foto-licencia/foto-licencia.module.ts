import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FotoLicenciaController } from './controller/foto-licencia.controller';
import { FotoLicenciaEntity } from './entity/foto-licencia.entity';
import { FotoLicenciaService } from './service/foto-licencia.service';

@Module({
  imports: [TypeOrmModule.forFeature([FotoLicenciaEntity])],
  controllers: [FotoLicenciaController],
  providers: [FotoLicenciaService],
  exports: [FotoLicenciaService]
})
export class FotoLicenciaModule {}
