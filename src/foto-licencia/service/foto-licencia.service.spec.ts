import { Test, TestingModule } from '@nestjs/testing';
import { FotoLicenciaService } from './foto-licencia.service';

describe('FotoLicenciaService', () => {
  let service: FotoLicenciaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FotoLicenciaService],
    }).compile();

    service = module.get<FotoLicenciaService>(FotoLicenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
