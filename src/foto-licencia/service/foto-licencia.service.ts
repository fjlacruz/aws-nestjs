import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FotoLicenciaEntity } from '../entity/foto-licencia.entity';

@Injectable()
export class FotoLicenciaService
{
    constructor(@InjectRepository(FotoLicenciaEntity) private readonly fotoLicenciaRespository: Repository<FotoLicenciaEntity>)
    {}

    async getById( id: number )
    {
        return await this.fotoLicenciaRespository.findOne( id );
    }
}
