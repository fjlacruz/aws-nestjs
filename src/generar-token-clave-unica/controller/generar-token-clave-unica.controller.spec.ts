import { Test, TestingModule } from '@nestjs/testing';
import { GenerarTokenClaveUnicaController } from './generar-token-clave-unica.controller';

describe('GenerarTokenClaveUnicaController', () => {
  let controller: GenerarTokenClaveUnicaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GenerarTokenClaveUnicaController],
    }).compile();

    controller = module.get<GenerarTokenClaveUnicaController>(GenerarTokenClaveUnicaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
