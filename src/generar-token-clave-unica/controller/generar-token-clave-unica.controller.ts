import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { GenerarTokenClaveUnicaService } from '../services/generar-token-clave-unica.service';

@ApiTags('Generar-token-clave-unica')
@Controller('generar-token-clave-unica')
export class GenerarTokenClaveUnicaController {

    constructor(private readonly generarTokenClaveUnicaService: GenerarTokenClaveUnicaService) { }
    
    @Get('getTokenClaveUnica')
    @ApiOperation({ summary: 'Servicio que retorna un token aleatorio para clave unica' })
    async getTokenClaveUnica() {
        const data = await this.generarTokenClaveUnicaService.getTokenClaveUnica();
        return {data};
    }
}
