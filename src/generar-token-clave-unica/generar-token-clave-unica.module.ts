import { Module } from '@nestjs/common';
import { GenerarTokenClaveUnicaController } from './controller/generar-token-clave-unica.controller';
import { GenerarTokenClaveUnicaService } from './services/generar-token-clave-unica.service';

@Module({
  providers: [GenerarTokenClaveUnicaService],
  exports: [GenerarTokenClaveUnicaService],
  controllers: [GenerarTokenClaveUnicaController]
})
export class GenerarTokenClaveUnicaModule {}
