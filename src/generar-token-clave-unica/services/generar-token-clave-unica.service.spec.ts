import { Test, TestingModule } from '@nestjs/testing';
import { GenerarTokenClaveUnicaService } from './generar-token-clave-unica.service';

describe('GenerarTokenClaveUnicaService', () => {
  let service: GenerarTokenClaveUnicaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GenerarTokenClaveUnicaService],
    }).compile();

    service = module.get<GenerarTokenClaveUnicaService>(GenerarTokenClaveUnicaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
