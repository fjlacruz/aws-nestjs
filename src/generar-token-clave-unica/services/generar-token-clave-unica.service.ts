import { Injectable } from '@nestjs/common';


@Injectable()
export class GenerarTokenClaveUnicaService {

    constructor() { }

    async getTokenClaveUnica(){
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < charactersLength; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
    
        let date: Date = new Date();
        return date.getDate().toString()+date.getMonth()+date.getUTCFullYear()+date.getHours()+date.getMinutes()+"SGL"+result;
    }
}
