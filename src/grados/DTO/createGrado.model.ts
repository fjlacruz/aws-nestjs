import { ApiPropertyOptional } from "@nestjs/swagger";

export class createGrado {

    @ApiPropertyOptional()
    idGrados: number;
    @ApiPropertyOptional()
    nombre: string;
    @ApiPropertyOptional()
    descripcion: string;
}