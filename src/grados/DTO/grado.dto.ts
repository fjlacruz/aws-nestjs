export class GradoDTO {
    idGrado: number;
    nombre: string;
    descripcion: string;
}