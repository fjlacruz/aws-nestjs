import { Controller, Get } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GradoService } from '../services/grado.service';

@ApiTags('Grado')
@Controller('Grado')
export class GradoController
{
    constructor( private gradoService: GradoService )
    {}

    @Get('/Grado')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los grados' })
    async getAll()
    {
        return this.gradoService.getAll().then(data =>
        {
            return { code: 200, data };
        });
    }

}
