import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity('Grados')
export class GradoEntity {

    @PrimaryColumn()
    idGrado: number;

    @Column()
    nombre: string;
    
    @Column()
    descripcion: string;

}