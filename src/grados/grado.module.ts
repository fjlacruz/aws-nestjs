import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GradoController } from './controller/grado.controller';
import { GradoEntity } from './entity/grado.entity';
import { GradoService } from './services/grado.service';

@Module({
  imports: [TypeOrmModule.forFeature([GradoEntity])],
  providers: [GradoService],
  exports: [GradoService],
  controllers: [GradoController]
})
export class GradoModule {}
