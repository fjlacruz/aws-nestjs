import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { GradoEntity } from '../entity/grado.entity';

@Injectable()
export class GradoService {
    constructor(@InjectRepository(GradoEntity) private readonly gradoRespository: Repository<GradoEntity>) { }

    async getAll(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoGrado: GradoEntity[] = [];

        try {
            resultadoGrado = await this.gradoRespository.find();

            if (Array.isArray(resultadoGrado) && resultadoGrado.length) {
                resultado.Respuesta = resultadoGrado;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Grados obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Grados';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Grados';
        }

        return resultado;
    }
}
