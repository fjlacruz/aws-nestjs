import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateGrupoPermisosDto {
  @ApiProperty({ name: 'nombre', type: 'string', required: true, example: 'Ejemplo' })
  @IsNotEmpty({ message: 'nombre es requerido' })
  @IsString({ message: 'nombre debe ser un string' })
  nombre: string;

  @ApiProperty({ name: 'descripción', type: 'string', required: true, example: 'Descripción de ejemplo' })
  @IsNotEmpty({ message: 'descrión es requerida' })
  @IsString({ message: 'descripción debe ser un string' })
  descripcion: string;

  @ApiProperty({ name: 'isPermisos', type: 'number[]', required: true, example: '[1,7,3,2]' })
  @IsNotEmpty({ message: 'idPermisos es requerido' })
  @IsArray({ message: 'id Permisos debe ser un array' })
  @IsNumber(
    { allowNaN: false, allowInfinity: false, maxDecimalPlaces: 0 },
    { each: true, message: 'idPermisos debe ser un array de number' }
  )
  idPermisos: number[];

  @ApiProperty({ name: 'activo', type: 'boolean', required: false, example: true })
  @IsBoolean({ message: 'activo debe ser un boolean' })
  activo: boolean;
}
