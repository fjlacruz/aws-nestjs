import { ApiPropertyOptional } from "@nestjs/swagger";
import { Roles } from "src/roles/entity/roles.entity";

export class GrupoPermisosDto {

    @ApiPropertyOptional()
    idGrupoPermiso: number;

    @ApiPropertyOptional()
    nombreGrupo: string;

    @ApiPropertyOptional()
    descripcion: string;

    @ApiPropertyOptional()
    asignado: boolean;

    @ApiPropertyOptional()
    rolesAsociados: Roles[];

    @ApiPropertyOptional()
    activo: boolean;
}