import { Body, Controller, Get, Param, ParseIntPipe, Post, Req, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { Resultado } from 'src/utils/resultado';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { CreateGrupoPermisosDto } from '../DTO/create-grupo-permisos.dto';
import { GrupoPermisosService } from '../service/grupo-permisos.service';

@ApiTags('Servicios-GrupoPermisos')
@Controller('grupoPermisos')
@UsePipes(ValidationPipe) // * Habilitamos validaciones
export class GrupoPermisosController {
  constructor(private readonly grupoPermisosService: GrupoPermisosService, private readonly authService: AuthService) {}

  @Get('/getGrupoPermisos')
  async getGrupoPermisos(@Req() req: any) {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaAdminitracionRolesYPermisos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.grupoPermisosService.getGrupoPermisos();
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Get('getGrupoPermiso/:id')
  public async getgrupoPermisoById(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaAdminitracionRolesYPermisos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.grupoPermisosService.getGrupoPermisoById(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('/getGrupoPermisosPaginados')
  async getGrupoPermisosPaginados(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosConsultarListadePermisos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.grupoPermisosService.getGrupoPermisosPaginados(paginacionArgs);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('createGrupoPermisos')
  public async create(@Req() req, @Body() createGrupoPermisoDto: CreateGrupoPermisosDto): Promise<{ data: Resultado }> {
    
    console.log(createGrupoPermisoDto);
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosCrearRolesGruposdePermisosTipodeDocumentos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.grupoPermisosService.create(createGrupoPermisoDto);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('editarGrupoPermiso/:id')
  public async update(@Req() req, @Param('id', ParseIntPipe) id: number, @Body() updateGrupoPermisos: CreateGrupoPermisosDto) {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.grupoPermisosService.update(id, updateGrupoPermisos);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('eliminarGrupoPermiso/:id')
  public async remove(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.grupoPermisosService.remove(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }
}
