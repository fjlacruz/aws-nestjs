import { ApiProperty } from '@nestjs/swagger';
import { Permisos } from 'src/permisos/entity/permisos.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { GrupoPermisos } from './grupo-permisos.entity';

@Entity('GrupoPermisoXPermisos')
export class GrupoPermisoXPermisos {
  @PrimaryGeneratedColumn()
  idGrupoPermisoXPermisos: number;

  @Column()
  idPermiso: number;

  @Column()
  idGrupoPermiso: number;

  @ManyToOne(() => Permisos, permisos => permisos.grupoPermisoXPermisos)
  @JoinColumn({ name: 'idPermiso' })
  readonly permisos: Permisos;

  @ManyToOne(() => GrupoPermisos, grupoPermisos => grupoPermisos.grupoPermisoXPermisos)
  @JoinColumn({ name: 'idGrupoPermiso' })
  readonly grupoPermisos: GrupoPermisos;

  @ManyToOne(() => Permisos, permisosAux => permisosAux.grupoPermisoXPermisosAux)
  @JoinColumn({ name: 'idPermiso' })
  readonly permisosAux: Permisos;
}
