import { ApiProperty } from "@nestjs/swagger";
import { Roles } from "src/roles/entity/roles.entity";
import { RolPermiso } from "src/rolPermiso/entity/rolPermiso.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn, ManyToOne} from "typeorm";
import { GrupoPermisos } from "./grupo-permisos.entity";

@Entity('GrupoPermisosRoles')
export class GrupoPermisosRoles {

    @PrimaryGeneratedColumn()
    idGrupoPermisosRoles: number;

    @Column()
    idRol: number;

    @JoinColumn({name: 'idRol'})
    @ManyToOne(() => Roles, roles => roles.grupoPermisosRoles)
    readonly roles: Roles;

    @Column()
    idGrupoPermiso: number;
    @JoinColumn({name: 'idGrupoPermiso'})
    @ManyToOne(() => GrupoPermisos, grupoPermisos => grupoPermisos.grupoPermisosRoles)
    readonly grupoPermisos: GrupoPermisos;
}
