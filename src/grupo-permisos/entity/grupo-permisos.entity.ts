import { ApiProperty } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { GrupoPermisoXPermisos } from './grupo-permiso-permisos.entity';
import { GrupoPermisosRoles } from './grupo-permisos-roles.entity';

@Entity('GrupoPermisos')
export class GrupoPermisos {
  @PrimaryGeneratedColumn()
  idGrupoPermiso: number;

  @Column()
  nombreGrupo: string;

  @Column()
  descripcion: string;

  @Column()
  activo: boolean;

  @ApiProperty()
  @OneToMany(() => GrupoPermisoXPermisos, grupoPermisoXPermisos => grupoPermisoXPermisos.grupoPermisos)
  readonly grupoPermisoXPermisos: GrupoPermisoXPermisos[];

  @ApiProperty()
  @OneToMany(() => GrupoPermisosRoles, grupoPermisosRoles => grupoPermisosRoles.grupoPermisos)
  readonly grupoPermisosRoles: GrupoPermisosRoles[];
}
