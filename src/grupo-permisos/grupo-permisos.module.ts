import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { GrupoPermisosController } from './controller/grupo-permisos.controller';
import { GrupoPermisos } from './entity/grupo-permisos.entity';
import { GrupoPermisosService } from './service/grupo-permisos.service';
import { AuthService } from 'src/auth/auth.service';
import { GrupoPermisosRoles } from './entity/grupo-permisos-roles.entity';
import { SharedModule } from 'src/shared/shared.module';
import { GrupoPermisoXPermisos } from './entity/grupo-permiso-permisos.entity';
import { PermisosModule } from 'src/permisos/permisos.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([GrupoPermisos, GrupoPermisosRoles, User, RolesUsuarios]),
    SharedModule,
    forwardRef(() => PermisosModule),
  ],
  controllers: [GrupoPermisosController],
  providers: [GrupoPermisosService, AuthService],
  exports: [GrupoPermisosService],
})
export class GrupoPermisosModule {}
