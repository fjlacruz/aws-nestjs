import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class GrupoPermisosXPermisosTransformer {
  @Expose({ groups: ['all'] })
  idGrupoPermisoXPermisos: number;

  @Expose({ groups: ['all', 'permiso'] })
  idPermiso: number;

  @Expose({ groups: ['all'] })
  idGrupoPermiso: number;
}
