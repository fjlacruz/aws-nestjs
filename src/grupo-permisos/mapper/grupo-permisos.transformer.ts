import { Exclude, Expose, Transform } from 'class-transformer';
import { GrupoPermisosXPermisosTransformer } from './grupo-permisos-permisos.transformer';

@Exclude()
export class GrupoPermisosTransformer {
  @Expose({ groups: ['all', 'toShow'] })
  idGrupoPermiso: number;

  @Expose({ groups: ['all', 'toShow'] })
  nombreGrupo: string;

  @Expose({ groups: ['all', 'toShow'] })
  descripcion: string;

  @Expose({ name: 'grupoPermisos', groups: ['toShow'] })
  permisos: number[];

  @Expose({ groups: ['toShow'] })
  activo: boolean;



  @Expose({ name: 'grupoPermisos', groups: ['all'] })
  @Transform(
    value => {
      const grupoPermiso = value.obj.grupoPermisoXPermisos as GrupoPermisosXPermisosTransformer;
      const motivo = grupoPermiso ? grupoPermiso.idPermiso : '';
      return motivo;
    },
    { groups: ['all'] }
  )
  grupoPermisos: GrupoPermisosXPermisosTransformer[];
}
