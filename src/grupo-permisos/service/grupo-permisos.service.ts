import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { ColumnasGrupoPermisos, ColumnasUsuarios } from 'src/constantes';
import { PermisosService } from 'src/permisos/service/permisos.service';
import { Roles } from 'src/roles/entity/roles.entity';
import { SetResultadoService } from 'src/shared/services/set-resultado/set-resultado.service';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { Resultado } from 'src/utils/resultado';
import { DeleteResult, getConnection, InsertResult, Repository, UpdateResult } from 'typeorm';
import { CreateGrupoPermisosDto } from '../DTO/create-grupo-permisos.dto';
import { GrupoPermisosDto } from '../DTO/grupo-permisos.dto';
import { GrupoPermisoXPermisos } from '../entity/grupo-permiso-permisos.entity';
import { GrupoPermisosRoles } from '../entity/grupo-permisos-roles.entity';
import { GrupoPermisos } from '../entity/grupo-permisos.entity';
import { GrupoPermisosXPermisosTransformer } from '../mapper/grupo-permisos-permisos.transformer';
import { GrupoPermisosTransformer } from '../mapper/grupo-permisos.transformer';

@Injectable()
export class GrupoPermisosService {
  constructor(
    @InjectRepository(GrupoPermisos) private readonly grupoPermisosRespository: Repository<GrupoPermisos>,
    private readonly permisosService: PermisosService,
    private readonly setResultadoService: SetResultadoService
  ) {}

  // #region CRUD

  async getGrupoPermisos() {
    return await this.grupoPermisosRespository.find({ order: { nombreGrupo: 'ASC' } });
  }

  public async getGrupoPermisoById(
    id: number,
    groupPermisos: string = 'toShow',
    groupPermisosXPermisos: string = 'grupoPermisos'
  ): Promise<Resultado> {
    const resultado = new Resultado();

    await this.grupoPermisosRespository
      .createQueryBuilder('gp')
      .leftJoinAndSelect('gp.grupoPermisoXPermisos', 'gpxp')
      .where('gp.idGrupoPermiso = :id', { id })
      .getOneOrFail()
      .then((res: GrupoPermisos) => {
        // * Realizamos una copia de la respuesta
        const copiaRes = { ...res };
        // * Mapeamos las clases para obtener los datos necesarios
        const grupoPermisoXPermisosTrans: GrupoPermisosXPermisosTransformer[] = plainToClass(
          GrupoPermisosXPermisosTransformer,
          copiaRes.grupoPermisoXPermisos,
          { groups: [groupPermisosXPermisos] }
        );
        const grupoPermisosTrans = plainToClass(GrupoPermisosTransformer, copiaRes, { groups: [groupPermisos] });
        if (groupPermisos === 'all') grupoPermisosTrans.grupoPermisos = grupoPermisoXPermisosTrans;
        if (groupPermisos === 'toShow')
          grupoPermisosTrans.permisos = copiaRes.grupoPermisoXPermisos.map(grupoPermisoXPermiso => grupoPermisoXPermiso.idPermiso);

        // * Modificamos resultado para respuesta
        this.setResultadoService.setResponse<GrupoPermisosTransformer>(resultado, grupoPermisosTrans);
      })
      .catch((err: Error) => {
        // * Modificamos resultados para errores
        this.setResultadoService.setError(resultado, 'No se ha podido obtener el grupo permisos');
      });

    return resultado;
  }

  async getGrupoPermisosPaginados(paginacionArgs: PaginacionArgs) {
    const resultado: Resultado = new Resultado();
    const { filtro } = paginacionArgs;
    const orderBy = ColumnasGrupoPermisos[paginacionArgs.orden.orden];
    let grupoPermisos: Pagination<GrupoPermisos>;
    let permisosParseados = new PaginacionDtoParser<GrupoPermisosDto>();

    grupoPermisos = await paginate<GrupoPermisos>(this.grupoPermisosRespository, paginacionArgs.paginationOptions, {
      relations: [
        'grupoPermisosRoles',
        'grupoPermisosRoles.roles',
        'grupoPermisosRoles.roles.rolesUsuarios',
        'grupoPermisosRoles.roles.rolesUsuarios.user',
      ],
      where: qb => {
        qb.where()
        if(filtro?.nombre != null && filtro.nombre != '' && filtro.nombre != 'null' ){
          qb.andWhere(
              'lower(unaccent(GrupoPermisos.nombreGrupo)) like lower(unaccent(:nombre))', {nombre: `%${filtro.nombre}%`}
        )}
        if(filtro?.descripcion != null && filtro.descripcion != '' && filtro.descripcion != 'null' ){
          qb.andWhere(
              'lower(unaccent(GrupoPermisos.descripcion)) like lower(unaccent(:descripcion))', {descripcion: `%${filtro.descripcion}%`}
        )}
        if(filtro?.rolesAsociados != null && filtro.rolesAsociados != '' && filtro.rolesAsociados != 'null' ){
          qb.andWhere(
              'GrupoPermisos__grupoPermisosRoles__roles.idRol = :rolesAsociados', {rolesAsociados: filtro.rolesAsociados}
        )}
        if(filtro?.estado != null && filtro.estado != ''){
          if(filtro.estado == 'null') {
              qb.andWhere('GrupoPermisos.activo IS NULL')
          } 
          else if(filtro.estado == 'true'){
              qb.andWhere('GrupoPermisos.activo = true')
          } 
          else if(filtro.estado == 'false'){
              qb.andWhere('GrupoPermisos.activo = false')
          }     
      }
        qb.orderBy(orderBy, paginacionArgs.orden.ordenarPor);
      },
    });

    if (Array.isArray(grupoPermisos.items) && grupoPermisos.items.length) {
      let gruposPermisosDTO: GrupoPermisosDto[] = [];

      for (const grupoPermisoRol of grupoPermisos.items) {
        let grupoPermisosDTO: GrupoPermisosDto = new GrupoPermisosDto();
        grupoPermisosDTO.idGrupoPermiso = grupoPermisoRol.idGrupoPermiso;
        grupoPermisosDTO.nombreGrupo = grupoPermisoRol.nombreGrupo;
        grupoPermisosDTO.descripcion = grupoPermisoRol.descripcion;
        grupoPermisosDTO.activo = grupoPermisoRol.activo

        grupoPermisosDTO.rolesAsociados = [];
        for (const gPermisoRol of grupoPermisoRol.grupoPermisosRoles) {
          grupoPermisosDTO.rolesAsociados.push(gPermisoRol.roles);

          for (const roles of grupoPermisosDTO.rolesAsociados) {
            for (const rolUser of roles.rolesUsuarios) {
              if (rolUser.user) {
                grupoPermisosDTO.asignado = true;
              } else {
                grupoPermisosDTO.asignado = false;
              }
            }
          }
        }

        gruposPermisosDTO.push(grupoPermisosDTO);
      }

      permisosParseados.items = gruposPermisosDTO;
      permisosParseados.meta = grupoPermisos.meta;
      permisosParseados.links = grupoPermisos.links;

      resultado.Respuesta = permisosParseados;
      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'GruposPermisos obtenidos correctamente';
    } else {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'No se encontraron GruposPermisos';
    }

    return resultado;
  }

  public async create(createGrupoPermisosDto: CreateGrupoPermisosDto): Promise<Resultado> {
    const resultado = new Resultado();

    // * Obtenemos Conexion para empezar una transacción
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const { nombre, descripcion, idPermisos,activo } = createGrupoPermisosDto;
      const newGrupoPermiso = this.grupoPermisosRespository.create({ nombreGrupo: nombre, descripcion, activo });

      if (!newGrupoPermiso) throw new Error('Error al crear grupo de permisos');

      const grupoPermisoId = await queryRunner.manager
        .createQueryBuilder()
        .insert()
        .into(GrupoPermisos)
        .values(newGrupoPermiso)
        .execute()
        .then((res: InsertResult) => {
          if (res.identifiers.length <= 0) throw new Error('No se ha insertado ningun grupo de permisos');

          return res.identifiers[0].idGrupoPermiso;
        })
        .catch((err: Error) => {
          throw new Error('Error al insertar grupo de permisos');
        });

      for (const idPermiso of idPermisos) {
        const resultadoPermiso = await this.permisosService.getPermisoById(idPermiso);
        if (resultadoPermiso.ResultadoOperacion) {
          await queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into(GrupoPermisoXPermisos)
            .values({ idGrupoPermiso: grupoPermisoId, idPermiso })
            .execute()
            .then((res: InsertResult) => {
              if (res.identifiers.length <= 0) throw new Error('No se ha insertado el grupo permiso - permiso');
            })
            .catch((err: Error) => {
              throw new Error('Error al insertar grupo permiso - permiso');
            });
        } else {
          throw new Error('Error no existe permiso');
        }
      }
      // * Comiteamos la transacción
      queryRunner.commitTransaction();

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Grupo permiso creado con éxito');
    } catch (error) {
      // * Hacemos Roll back de las consutlas realizadas
      queryRunner.rollbackTransaction();

      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido crear el grupo de permisos');
    } finally {
      // * Cerrramos conexión transacción
      queryRunner.release();

      return resultado;
    }
  }

  public async update(id: number, updateGrupoPermisosDto: CreateGrupoPermisosDto): Promise<Resultado> {
    const resultado = new Resultado();

    // * Obtenemos Conexion para empezar una transacción
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const grupoPermisoResultado = await this.getGrupoPermisoById(id, 'all', 'all');
      const grupoPermiso = { ...grupoPermisoResultado.Respuesta } as GrupoPermisosTransformer;
      const { nombre, descripcion, idPermisos, activo } = updateGrupoPermisosDto;
      // * Si no existe Grupo de permiso, lanzamos error
      if (!grupoPermisoResultado.ResultadoOperacion) throw new Error(grupoPermisoResultado.Mensaje);

      const permisosToDelete = this.getGrupoPermisoXPermisosToDelete(grupoPermiso.grupoPermisos, idPermisos);
      const permisosToInsert = this.getGrupoPermisoXPermisosToInsert(grupoPermiso.grupoPermisos, idPermisos);

      // * Actualizamos el grupo de permiso {nombre, descripcion}
      await queryRunner.manager
        .createQueryBuilder()
        .update(GrupoPermisos)
        .set({ nombreGrupo: nombre, descripcion, activo })
        .where({ idGrupoPermiso: id })
        .execute()
        .then((res: UpdateResult) => {
          if (res.affected <= 0) throw new Error('No se ha actualizado ningun grupo de permisos');
        })
        .catch((err: Error) => {
          throw new Error('Error al actualizar grupo de permisos');
        });

      // * Eliminamos los permisos del grupo
      for (const idPermiso of permisosToDelete) {
        const resultadoPermiso = await this.permisosService.getPermisoById(idPermiso);
        if (resultadoPermiso.ResultadoOperacion) {
          await queryRunner.manager
            .createQueryBuilder()
            .delete()
            .from(GrupoPermisoXPermisos)
            .where({ idPermiso })
            .execute()
            .then((res: DeleteResult) => {
              if (res.affected <= 0) throw new Error('No se ha eliminado ningun permiso del grupo de permiso');
            })
            .catch((err: Error) => {
              throw new Error('Error al eliminar permiso del grupo de permisos');
            });
        }
      }

      // * Añadimos los nuevos permisos al grupo
      for (const idPermiso of permisosToInsert) {
        const resultadoPermiso = await this.permisosService.getPermisoById(idPermiso);
        if (resultadoPermiso.ResultadoOperacion) {
          await queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into(GrupoPermisoXPermisos)
            .values({ idGrupoPermiso: id, idPermiso })
            .execute()
            .then((res: InsertResult) => {
              if (res.identifiers.length <= 0) throw new Error('No se ha insertado el grupo permiso - permiso');
            })
            .catch((err: Error) => {
              throw new Error('Error al insertar grupo permiso - permiso');
            });
        } else {
          throw new Error('Error no existe permiso');
        }
      }

      // * Comiteamos la transacción
      queryRunner.commitTransaction();

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Grupo Permiso actualizado con éxito');
    } catch (error) {
      // * Hacemos Roll back de las consutlas realizadas
      queryRunner.rollbackTransaction();

      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido actualizar el grupo de permisos');
    } finally {
      // * Cerrramos conexión transacción
      queryRunner.release();

      return resultado;
    }
  }

  public async remove(id: number): Promise<Resultado> {
    const resultado = new Resultado();

    // * Obtenemos Conexion para empezar una transacción
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const grupoPermisoResultado = await this.getGrupoPermisoById(id, 'all', 'all');

      if (await this.isGrupoPermisoAsignado(id)) throw new Error('El grupo permiso está asignado');
      // * Si no existe Grupo de permiso, lanzamos error
      if (!grupoPermisoResultado.ResultadoOperacion) throw new Error(grupoPermisoResultado.Mensaje);

      await queryRunner.manager
        .createQueryBuilder()
        .delete()
        .from(GrupoPermisoXPermisos)
        .where({ idGrupoPermiso: id })
        .execute()
        .then((res: DeleteResult) => {
          if (res.affected <= 0) throw new Error('No se ha eliminado ningin grupo permiso - permiso');
        })
        .catch((err: Error) => {
          throw new Error('Error al eliminar grupo permiso - permiso');
        });

      await queryRunner.manager
        .createQueryBuilder()
        .delete()
        .from(GrupoPermisos)
        .where({ idGrupoPermiso: id })
        .execute()
        .then((res: DeleteResult) => {
          if (res.affected <= 0) throw new Error('No se ha eliminado ningin grupo permiso');
        })
        .catch((err: Error) => {
          throw new Error('Error al eliminar grupo permiso');
        });

      // * Comiteamos la transacción
      queryRunner.commitTransaction();

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Grupo de permisos eliminado con éxito');
    } catch (error) {
      // * Hacemos Roll back de las consutlas realizadas
      queryRunner.rollbackTransaction();

      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido eliminar el grupo de permisos');
    } finally {
      // * Cerrramos conexión transacción
      queryRunner.release();

      return resultado;
    }
  }

  // #endregion CRUD

  // #region private functions

  private getGrupoPermisoXPermisosToDelete(grupoPermisoXPermisos: GrupoPermisosXPermisosTransformer[], permisosIds: number[]): number[] {
    const permisosToInsert: number[] = [];

    grupoPermisoXPermisos
      .map(grupoPermisoXPermiso => grupoPermisoXPermiso.idPermiso)
      .forEach(idPermiso => {
        const containsPermiso: boolean = permisosIds.includes(idPermiso);

        if (!containsPermiso) permisosToInsert.push(idPermiso);
      });

    return permisosToInsert;
  }

  private getGrupoPermisoXPermisosToInsert(grupoPermisoXPermisos: GrupoPermisosXPermisosTransformer[], permisosIds: number[]): number[] {
    const permisosToInsert: number[] = [];

    permisosIds.forEach(permisoId => {
      const containsPermiso: boolean = grupoPermisoXPermisos
        .map(grupoPermisoXPermiso => grupoPermisoXPermiso.idPermiso)
        .includes(permisoId);

      if (!containsPermiso) permisosToInsert.push(permisoId);
    });

    return permisosToInsert;
  }

  private async isGrupoPermisoAsignado(id: number): Promise<boolean> {
    return await this.grupoPermisosRespository
      .createQueryBuilder('gp')
      .leftJoinAndSelect('gp.grupoPermisosRoles', 'gpr')
      .where('gp.idGrupoPermiso =:id', { id })
      .getOneOrFail()
      .then((res: GrupoPermisos) => {
        const grupoPermisoRoles = res.grupoPermisosRoles;
        let isAsignado = false;

        if (grupoPermisoRoles.length > 0) isAsignado = true;

        return isAsignado;
      })
      .catch((err: Error) => false);
  }

  // #endregion private functions
}
