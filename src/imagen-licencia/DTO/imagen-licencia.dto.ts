import { ApiPropertyOptional } from "@nestjs/swagger";

export class ImagenLicenciaDto {

    @ApiPropertyOptional()
    fileBinary: string;
    @ApiPropertyOptional()
    created_at:Date;
    @ApiPropertyOptional()
    updated_at:Date;
    @ApiPropertyOptional()
    numeroImagen:number;
    @ApiPropertyOptional()
    idTipoImagenLicencia:number;
    @ApiPropertyOptional()
    idDocumentoLicencia:number;
}