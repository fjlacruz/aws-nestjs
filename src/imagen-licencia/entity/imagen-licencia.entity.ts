import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { TipoImagenLicenciaEntity } from "./tipo-imagen-licencia.entity";

@Entity('ImagenLicencia')
export class ImagenLicenciaEntity {

    @PrimaryGeneratedColumn()
    idImagenLicencia: number;

    @Column({
        type: 'bytea',
        nullable: false
    })
    fileBinary: Buffer;

    @Column()
    created_at:Date;

    @Column()
    updated_at:Date;
    
    @Column()
    numeroImagen:number;

    @Column()
    idTipoImagenLicencia:number;
    @ManyToOne(() => TipoImagenLicenciaEntity, tipoImagenLicencia => tipoImagenLicencia.imagenLicencia)
    @JoinColumn({ name: 'idTipoImagenLicencia' })
    readonly tipoImagenLicencia: TipoImagenLicenciaEntity;

    @Column()
    idDocumentoLicencia:number;
    @ManyToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.imagenLicencia)
    @JoinColumn({ name: 'idDocumentoLicencia' })
    readonly documentosLicencia: DocumentosLicencia;
}
