import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ImagenLicenciaEntity } from "./imagen-licencia.entity";

@Entity('TipoImagenLicencia')
export class TipoImagenLicenciaEntity {

    @PrimaryGeneratedColumn()
    idTipoImagenLicencia: number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;
    
    @ApiProperty()
    @OneToMany(() => ImagenLicenciaEntity, imagenLicencia => imagenLicencia.tipoImagenLicencia)
    readonly imagenLicencia: ImagenLicenciaEntity[];
}
