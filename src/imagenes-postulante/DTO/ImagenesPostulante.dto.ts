import { ApiPropertyOptional } from "@nestjs/swagger";

export class ImagenesPostulanteDTO {

    @ApiPropertyOptional()
    idImagenPostulante: number;

    @ApiPropertyOptional()
    nombreArchivo:string;

    @ApiPropertyOptional()
    archivo: Buffer;

    @ApiPropertyOptional()
    created_at:Date;

    @ApiPropertyOptional()
    idPostulante: number;
}
