import { Test, TestingModule } from '@nestjs/testing';
import { ImagenesPostulanteController } from './imagenes-postulante.controller';

describe('ImagenesPostulanteController', () => {
  let controller: ImagenesPostulanteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ImagenesPostulanteController],
    }).compile();

    controller = module.get<ImagenesPostulanteController>(ImagenesPostulanteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
