import { Controller, UseGuards } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { ImagenesPostulanteDTO } from '../DTO/ImagenesPostulante.dto';
import { ImagenesPostulanteService } from '../services/imagenes-postulante.service';

@ApiTags('Imagenes-postulante')
@Controller('imagenes-postulante')
export class ImagenesPostulanteController {

    constructor(private readonly imagenesPostulanteService: ImagenesPostulanteService) { }
    
    @Post('uploadDocumentoPostulante')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio para subir documentos asociados a un postulante' })
    async uploadDocumento(
        @Body() imagenesPostulanteDTO: ImagenesPostulanteDTO) {
        const data = await this.imagenesPostulanteService.createDocumento(imagenesPostulanteDTO);
        return {data};
    }

    @Get('documentosPostulantes')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Documentos de un postulante' })
    async documentosPostulantes() {
        const data = await this.imagenesPostulanteService.getDocumentos();
        return { data };
    }


    @Get('documentoPostulanteById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por Id' })
    async getDocumentoById(@Param('id') id: number) {
        const data = await this.imagenesPostulanteService.getDocumento(id);
        return { data };
    }

    @Get('documentoPostulanteByPostulante/:idPostulante')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Documento por id Postulante' })
    async getDocumentoByPostulante(@Param('idPostulante') idPostulante: number) {
        const data = await this.imagenesPostulanteService.getDocumentoByPostulante(idPostulante);
        return { data };
    }


    @Post('updateDocumentoPostulante')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que modifica la metadatos de documentos' })
    async updateDocumento(
        @Body() imagenesPostulanteDTO: ImagenesPostulanteDTO) {
        const data = await this.imagenesPostulanteService.update(imagenesPostulanteDTO.idImagenPostulante, imagenesPostulanteDTO);
        return {data};
    }
        

    @Get('deleteDocumentoPostulanteById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Documento por Id' })
    async deleteDocumentoById(@Param('id') id: number) {
        const data = await this.imagenesPostulanteService.delete(id);
        return data;
    }

}
