import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn, ManyToOne, JoinColumn} from "typeorm";

@Entity('ImagenesPostulante')
export class ImagenesPostulanteEntity {

    @PrimaryColumn()
    idImagenPostulante: number;

    @Column()
    nombreArchivo:string;

    @Column({
        name: 'archivo',
        type: 'bytea',
        nullable: false,
    })
    archivo: Buffer;

    @Column()
    created_at:Date;

    @Column()
    idPostulante: number;
    @ManyToOne(() => PostulantesEntity, postulante => postulante.imagenesPostulante)
    @JoinColumn({ name: 'idPostulante' })
    readonly postulante: PostulantesEntity;
}
