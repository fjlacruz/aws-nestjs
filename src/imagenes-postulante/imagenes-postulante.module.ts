import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImagenesPostulanteController } from './controller/imagenes-postulante.controller';
import { ImagenesPostulanteEntity } from './entity/ImagenesPostulante.entity';
import { ImagenesPostulanteService } from './services/imagenes-postulante.service';

@Module({
  imports: [TypeOrmModule.forFeature([ImagenesPostulanteEntity])],
  providers: [ImagenesPostulanteService],
  exports: [ImagenesPostulanteService],
  controllers: [ImagenesPostulanteController]
})
export class ImagenesPostulanteModule {}
