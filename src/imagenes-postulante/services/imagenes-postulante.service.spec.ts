import { Test, TestingModule } from '@nestjs/testing';
import { ImagenesPostulanteService } from './imagenes-postulante.service';

describe('ImagenesPostulanteService', () => {
  let service: ImagenesPostulanteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImagenesPostulanteService],
    }).compile();

    service = module.get<ImagenesPostulanteService>(ImagenesPostulanteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
