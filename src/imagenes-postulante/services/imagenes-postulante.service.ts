import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import { ImagenesPostulanteDTO } from '../DTO/ImagenesPostulante.dto';
import { ImagenesPostulanteEntity } from '../entity/ImagenesPostulante.entity';

@Injectable()
export class ImagenesPostulanteService {

    constructor(@InjectRepository(ImagenesPostulanteEntity) private readonly imagenesPostulanteEntity: Repository<ImagenesPostulanteEntity>) { }

    async createDocumento(imagenesPostulanteDTO: ImagenesPostulanteDTO): Promise<ImagenesPostulanteDTO> {
        return await this.imagenesPostulanteEntity.save(imagenesPostulanteDTO);
    }

    async getDocumentos() {
        return await this.imagenesPostulanteEntity.find();
    }


    async getDocumento(id:number){
        const documento= await this.imagenesPostulanteEntity.findOne(id);
        if (!documento)throw new NotFoundException("documneto dont exist");
        
        return documento;
    }

    async getDocumentoByPostulante(idPostulante:number)
    {
        const documento = await this.imagenesPostulanteEntity
            .createQueryBuilder("imagen")
            .where( "imagen.idPostulante = :idPostulante", { idPostulante } )
            .orderBy( "imagen.idImagenPostulante", "DESC" )
            .getOne();
        
        return documento;
    }


    async update(idImagenPostulante: number, data: Partial<ImagenesPostulanteEntity>) {
        await this.imagenesPostulanteEntity.update({ idImagenPostulante }, data);
        return await this.imagenesPostulanteEntity.findOne({ idImagenPostulante });
    }

    async delete(id: number) {
        try {
            await getConnection()
            .createQueryBuilder()
            .delete()
            .from(ImagenesPostulanteEntity)
            .where("idImagenPostulante = :id", { id: id })
            .execute();
            return {"code":200, "message":"success"};
        } catch (error) {
            return {"code":400, "error":error};
        }    
    }
}
