import { Body, Controller, Delete, Get, Param, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { Page, PageRequest } from '../infraccion/pagination-infraccion.entity';
import { HeaderUtil } from '../base/base/header-util';
import { AuthGuard } from '@nestjs/passport';
import { Post as PostMethod } from '@nestjs/common/decorators/http/request-mapping.decorator';
import { InfraccionService } from './infraccion.service';
import { InfraccionEntity } from './infraccion.entity';

@ApiTags('Infracciones')
@Controller('infracciones')
export class InfraccionController {
  constructor(private readonly infraccionService: InfraccionService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all records',
    type: InfraccionEntity,
  })
  async getAll(@Req() req: Request): Promise<InfraccionEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.infraccionService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una Sentencia por Id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: InfraccionEntity,
  })
  async getOne(@Param('id') id: string): Promise<InfraccionEntity> {
    return await this.infraccionService.findById(id);
  }

  @PostMethod('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea una nueva Sentencia' })
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    type: InfraccionEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() sentenciaEntity: InfraccionEntity): Promise<InfraccionEntity> {
    const created = await this.infraccionService.save(sentenciaEntity);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: InfraccionEntity,
  })
  async put(@Req() req: Request, @Body() sentenciaEntity: InfraccionEntity): Promise<InfraccionEntity> {
    return await this.infraccionService.update(sentenciaEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: InfraccionEntity,
  })
  async putId(@Req() req: Request, @Body() sentenciaEntity: InfraccionEntity): Promise<InfraccionEntity> {
    return await this.infraccionService.update(sentenciaEntity);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Elimina una Sentencia' })
  @ApiResponse({
    status: 204,
    description: 'The record has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
    return await this.infraccionService.deleteById(id);
  }
}
