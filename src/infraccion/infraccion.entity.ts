import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { TipoInfraccionEntity } from '../tipo-infraccion/entity/tipo-infraccion.entity';

@Entity('Infracciones')
export class InfraccionEntity {
  @PrimaryGeneratedColumn()
  idInfracciones: number;
  @Column()
  @ApiModelProperty()
  idTipoInfraccion: number;

  @JoinColumn({name: 'idTipoInfraccion'})
  @ManyToOne(() => TipoInfraccionEntity, roles => roles.idTipoInfraccion, { eager: false })
  readonly tipoInfraccionEntity: TipoInfraccionEntity;

  @Column()
  @ApiModelProperty()
  fechaDenunca: Date;
  @Column()
  @ApiModelProperty()
  codigoInfraccion: string;
}
