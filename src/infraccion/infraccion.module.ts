import { Module } from '@nestjs/common';
import { InfraccionController } from './infraccion.controller';
import { InfraccionService } from './infraccion.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InfraccionEntity } from './infraccion.entity';

@Module({
  imports: [TypeOrmModule.forFeature([InfraccionEntity])],
  controllers: [InfraccionController],
  providers: [InfraccionService],
  exports: [InfraccionService],
})
export class InfraccionModule {}
