import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { InfraccionEntity } from './infraccion.entity';

const relationshipNames = [];

@Injectable()
export class InfraccionService {
  constructor(@InjectRepository(InfraccionEntity) private readonly infraccionEntityRepository: Repository<InfraccionEntity>) {}

  async findById(id: string): Promise<InfraccionEntity | undefined> {
    const options = { relations: relationshipNames };
    const result = await this.infraccionEntityRepository.findOne(id, options);
    return result;
  }

  async findByfields(options: FindOneOptions<InfraccionEntity>): Promise<InfraccionEntity | undefined> {
    const result = await this.infraccionEntityRepository.findOne(options);
    return result;
  }

  async findAndCount(options: FindManyOptions<InfraccionEntity>): Promise<[InfraccionEntity[], number]> {
    options.relations = relationshipNames;
    const resultList = await this.infraccionEntityRepository.findAndCount(options);
    const infraccionEntities: InfraccionEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(infraccion => infraccionEntities.push(infraccion));
      resultList[0] = infraccionEntities;
    }
    return resultList;
  }

  async save(sentenciaEntity: InfraccionEntity): Promise<InfraccionEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.infraccionEntityRepository.save(entity);
    return result;
  }

  async update(sentenciaEntity: InfraccionEntity): Promise<InfraccionEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.infraccionEntityRepository.save(entity);
    return result;
  }

  async deleteById(id: string): Promise<void | undefined> {
    await this.infraccionEntityRepository.delete(id);
    const entityFind = await this.findById(id);
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
