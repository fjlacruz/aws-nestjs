import { ApiPropertyOptional } from "@nestjs/swagger";

export class PostulantesDto {
    
    @ApiPropertyOptional()
    idPostulante: number;

    @ApiPropertyOptional()
    RUN: number;
    @ApiPropertyOptional()
    RUNCompleto: string;
    @ApiPropertyOptional()
    DV:string;

    @ApiPropertyOptional()
    Nombres:string;

    @ApiPropertyOptional()
    ApellidoPaterno: string;

    @ApiPropertyOptional()
    ApellidoMaterno: string;

    @ApiPropertyOptional()
    Sexo: string;

    @ApiPropertyOptional()
    EstadoCivil: string;

    @ApiPropertyOptional()
    Nacionalidad: string;

    @ApiPropertyOptional()
    Email: string;

    @ApiPropertyOptional()
    Telefono: string;

    @ApiPropertyOptional()
    FechaNacimiento: Date;

    @ApiPropertyOptional()
    Edad: string;

    @ApiPropertyOptional()
    FechaDefuncion: Date;

    @ApiPropertyOptional()
    NivelEducacional: string;

    @ApiPropertyOptional()
    Profesion: string;

    @ApiPropertyOptional()
    Diplomatico: boolean;

    @ApiPropertyOptional()
    Discapacidad: boolean;

    @ApiPropertyOptional()
    DetalleDiscapacidad: string;

    @ApiPropertyOptional()
    Region: string;

    @ApiPropertyOptional()
    Comuna: string;

    @ApiPropertyOptional()
    Calle: string;

    @ApiPropertyOptional()
    RestoDireccion: string;

    @ApiPropertyOptional()
    CalleNro: number;

    @ApiPropertyOptional()
    Letra: string;

  }