import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateAspiranteDto {
    
    @ApiPropertyOptional()
    idPostulante: number;
    @ApiPropertyOptional()
    RUN: number;
    @ApiPropertyOptional()
    DV:string;
    @ApiPropertyOptional()
    Nombres:string;
    @ApiPropertyOptional()
    ApellidoPaterno: string;
    @ApiPropertyOptional()
    ApellidoMaterno: string;
    @ApiPropertyOptional()
    Calle: string;
    @ApiPropertyOptional()
    CalleNro: number;
    @ApiPropertyOptional()
    Letra: string;
    @ApiPropertyOptional()
    RestoDireccion: string;
    @ApiPropertyOptional()
    Sexo: string;
    @ApiPropertyOptional()
    EstadoCivil: string;
    @ApiPropertyOptional()
    Nacionalidad: string;
    @ApiPropertyOptional()
    Email: string;
    @ApiPropertyOptional()
    NivelEducacional: string;
    @ApiPropertyOptional()
    Profesion: string;
    @ApiPropertyOptional()
    Diplomatico: boolean;
    @ApiPropertyOptional()
    discapacidad: boolean;
    @ApiPropertyOptional()
    DetalleDiscapacidad: string;
    // @ApiPropertyOptional()
    // updated_at: Date;
    // @ApiPropertyOptional()
    // created_at: Date;
    // @ApiPropertyOptional()
    // idUsuario: number;
    @ApiPropertyOptional()
    Telefono: string;
    @ApiPropertyOptional()
    FechaNacimiento: Date;
    @ApiPropertyOptional()
    FechaDefuncion: Date;
    
    @ApiPropertyOptional()
    idRegion: number;

    @ApiPropertyOptional()
    idComuna: number;

    @ApiPropertyOptional()
    idOpcionSexo: number;

    @ApiPropertyOptional()
    idOpcionEstadosCivil: number;

    @ApiPropertyOptional()
    idOpNivelEducacional: number;

  }
   
  export default CreateAspiranteDto;