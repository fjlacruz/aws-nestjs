import { Test, TestingModule } from '@nestjs/testing';
import { IngresoPostulanteController } from './ingreso-postulante.controller';

describe('IngresoPostulanteController', () => {
  let controller: IngresoPostulanteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IngresoPostulanteController],
    }).compile();

    controller = module.get<IngresoPostulanteController>(IngresoPostulanteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
