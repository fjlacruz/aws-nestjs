import { Controller, Req } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch, UseGuards } from '@nestjs/common';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';
import { CreateAspiranteDto } from 'src/ingreso-postulante/DTO/createAspirante.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { Resultado } from 'src/utils/resultado';

@ApiTags('Servicios-Postulante')
@Controller('ingresoPostulante')
export class IngresoPostulanteController {
  constructor(private readonly ingresoAspiranteService: IngresoPostulanteService, private readonly authService: AuthService) {}

  @UseGuards(JwtAuthGuard)
  @Get('postulantes')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los Postulantes.' })
  async getPostulantes() {
    const data = await this.ingresoAspiranteService.getPostulantes();
    return { data };
  }

  @Get('postulantesById/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Postulante por Id.' })
  async getPostulante(@Param('id') id: number) {
    const data = await this.ingresoAspiranteService.getPostulante(id);
    return { data };
  }

  @Post('creaPostulante')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Postulante' })
  async addPostulante(@Body() createAspiranteDto: CreateAspiranteDto) {
    const data = await this.ingresoAspiranteService.create(createAspiranteDto);
    return { data };
  }

  @Post('creaPostulanteByRUN')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Postulante con validacion de RUN sin duplicar' })
  async addPostulanteByRUN(@Body() createAspiranteDto: CreateAspiranteDto) {
    const data = await this.ingresoAspiranteService.createByRUNNoExist(createAspiranteDto);

    if (!data) return 'El run ingresado ya se encuentra registrado, no puede duplicar RUN';
    else return { data };
  }

  @Get('postulantesByRUN/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Postulante por RUN' })
  async getPostulanteByRUN(@Param('run') RUN: number) {
    return await this.ingresoAspiranteService.getPostulanteByRUN(RUN);
  }

  @Get('postulantesByRUNV2/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Postulante por RUN V2' })
  async getPostulanteByRUNV2(@Param('run') RUN: number) {
    return await this.ingresoAspiranteService.getPostulanteByRUNV2(RUN);
  }

  @Get('postulanteRegistroByRUN/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Postulante por RUN y DV' })
  async postulanteRegistroByRUN(@Param('run') run: string, @Param('idUsuario') idUsuario: 1) {
    return await this.ingresoAspiranteService.getPostulanteRegistroByRUN(run, idUsuario);
  }

  @Get('postulanteByRUNDV/:run/:dv')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Postulante por RUN y DV' })
  async getPostulanteByRUNDV(@Param('run') run: number, @Param('dv') dv: string, @Param('idUsuario') idUsuario: 1) {
    return await this.ingresoAspiranteService.getPostulanteByRUNDV(run, dv, idUsuario);
  }

  @Get('conductorByRUNDV/:run/:dv/:idUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Conductor por RUN y DV' })
  async getConductorByRUNDV(@Param('run') run: number, @Param('dv') dv: string, @Param('idUsuario') idUsuario: string) {
    return await this.ingresoAspiranteService.getConductorByRUNDV(run, dv, idUsuario);
  }

  /*@Put('postulantesUpdate/:id')
    async update(@Param('id') id, @Body() postulanteData: CreateAspiranteDto): Promise<any> {
        postulanteData.idPostulante = Number(id);
        console.log('Update #' + postulanteData.idPostulante)
        return this.ingresoAspiranteService.update(postulanteData);
    } */

  @Patch('postulantesUpdate/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza datos de un Postulante' })
  async updateUser(@Param('id') id: number, @Body() data: Partial<CreateAspiranteDto>, @Req() req: any) {
    return await this.ingresoAspiranteService.update(req, id, data);
  }
  @Post('consultaHis')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que consulta el historico del postulante en el registro civil y actualiza las colecciones de interSGL',
  })
  async consultaHis(@Body() data: string) {
    return this.ingresoAspiranteService.consulHis(data);
  }

  @Post('valida_tramitres_en_proceso_otra_muni')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que valida si el postulante tiene tramites en otra municipalidad',
  })
  async valida_tramitres_en_proceso_otra_muni(@Body() data: string) {
    return this.ingresoAspiranteService.valida_tramitres_en_proceso_otra_muni(data);
  }

  @Post('consulta_estados_rc')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que consulta codigo de estados del RC',
  })
  async consulta_estados_rc(@Body() data: string) {
    return this.ingresoAspiranteService.consulta_estados_rc(data);
  }

  //consulta_descrip_restricciones
  @Post('consulta_descrip_restricciones')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que consulta los nombres de las restricciones  del RC',
  })
  async consulta_descrip_restricciones(@Body() data: string) {
    return this.ingresoAspiranteService.consulta_descrip_restricciones(data);
  }

  @Post('consultaUltimosDatosRNC')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que consulta los ultimos datos del RNC del postulante',
  })
  async consulUltDatRNC(@Body() data: string) {
    return this.ingresoAspiranteService.consultaUltimosDatosRNC(data);
  }

  @Post('insertLic')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para insertar licencias del postulante',
  })
  async insertLic(@Body() data: string) {
    return this.ingresoAspiranteService.insertLic(data);
  }

  @Post('insertHistLic')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para insertar historico de licencias del postulante en el interSGL',
  })
  async insertHistLic(@Body() data: string) {
    return this.ingresoAspiranteService.insertHistLic(data);
  }

  @Post('insertHis')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para insertar registros del postulante en la tabla de historico en el registro civil',
  })
  async insertHis(@Body() data: string) {
    return this.ingresoAspiranteService.insertHis(data);
  }
  /*
  @Post('inserMovtHis')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para insertar registros del postulante en la tabla de historico en el registro civil',
  })
  async insertatHistoricoCambiEstado(@Body() data: string) {
    return this.ingresoAspiranteService.insertatHistoricoCambiEstado(data);
  }
*/
  @Post('updateLic')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para actualizar licencia del postulante en registro civil',
  })
  async updateLic(@Body() data: string) {
    return this.ingresoAspiranteService.updateLic(data);
  }
  @Post('anotaciones')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para registrar anotaciones(denegaciones y restricciones) del postulante en registro civil',
  })
  async insertAnotaciones(@Body() data: string) {
    return this.ingresoAspiranteService.anotaciones(data);
  }
  @Post('sentencias')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para registrar sentencias del postulante en registro civil',
  })
  async insertSentencias(@Body() data: string) {
    return this.ingresoAspiranteService.procesaDatosSen(data);
  }
  @Post('duplicados')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para registrar duplicados de liciencias del postulante en registro civil',
  })
  async insertDuplicados(@Body() data: string) {
    return this.ingresoAspiranteService.duplicados(data);
  }

  @Post('domicilio')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para registrar domicilio del postulante en registro civil',
  })
  async insertDomicilio(@Body() data: string) {
    console.log(data);
    return this.ingresoAspiranteService.domicilio(data);
  }

  @Post('profesion')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para obtener la profesion desde el registro civil',
  })
  async consulProf(@Body() data: string) {
    return this.ingresoAspiranteService.consulProf(data);
  }

  @Post('direccion')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para obtener la direcccion desde el registro civil',
  })
  async consulPerDir(@Body() data: string) {
    console.log(data);
    return this.ingresoAspiranteService.consulPerDir(data);
  }

  @Get('validaStatusRCel')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que valida si el Registro Civil esta activo(OK) o no actino(NOK)',
  })
  async validaStatusRcel() {
    return this.ingresoAspiranteService.validaStatus();
  }

  @Post('cambioEstadoIngresoPostulante')
  @ApiBearerAuth()
  @ApiOperation({
    summary:
      'Servicio para realizar el cambio de estado en el registro civil por run (run+dv) y clase de licencia para ingreso de postulantes',
  })
  async cambioEstadoIngPost(@Body() data: any) {
    return this.ingresoAspiranteService.cambioEstadoIngPost(data);
  }
  @Post('consulSen')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar suspensiones de un postulante',
  })
  async consulSen(@Body() data: any) {
    return this.ingresoAspiranteService.consulSen(data);
  }

  @Post('consulHis')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar suspensiones de un postulante',
  })
  async consulHis(@Body() data: any) {
    console.log(data);
    return this.ingresoAspiranteService.consulHis(data);
  }
  @Post('datosSolicitud')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para informar al registro civil el inicio de una solicitud',
  })
  async datosSolicitud(@Body() data: any) {
    return this.ingresoAspiranteService.datosSolicitud(data);
  }

  @Post('getComRNC')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que devuelve la comuna segun codigo RNC',
  })
  async getComRNC(@Body() data: string) {
    return this.ingresoAspiranteService.getComRNC(data);
  }

  @Post('updateDomicilioRC')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para actualizare los datos del domicilio del postulante',
  })
  async updateDomicilioRC(@Body() data: any) {
    return this.ingresoAspiranteService.updateDomicilioRC(data);
  }

  @Post('rpi')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para consultar el registro de pasajero infractor RPI',
  })
  async rpi(@Body() data: any) {
    return this.ingresoAspiranteService.rpi(data);
  }

  @Get('postulantesByIdPostulante/:idPostulante')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Postulante por RUN' })
  async getPostulanteByIdPostulante(@Param('idPostulante') idPostulante: number, @Req() req: any) {
    let validacionPermisos = await this.authService.checkPermission(req, PermisosNombres.VisualizarListaSolicitudesEnProceso);

    if (validacionPermisos.ResultadoOperacion) {
      return await this.ingresoAspiranteService.getPostulanteByIdPostulante(idPostulante);
    } else {
      return { data: validacionPermisos };
    }
  }

  @Post('verificaPermisos')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para actualizare los datos del domicilio del postulante',
  })
  async verificaPermisos(@Body() data: any, @Req() req: any) {
    let opc = data.opcion;
    let splittedBearerToken = data.token.split(' ');
    let token = splittedBearerToken[0];
    let tokenPermisos = new TokenPermisoDto(token, [`PermisosNombres.${opc}`]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);
    return validarPermisos;
  }

  @Post('verificaAdministrador')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para actualizare los datos del domicilio del postulante',
  })
  async verificaAdministrador(@Body() data: any, @Req() req: any) {
    let resAdmin: Resultado = new Resultado();

    try {
      // No importa el permiso que le enviemos, sólo queremos saber si es superadmin
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, '');

      resAdmin.Respuesta = this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios);
      resAdmin.ResultadoOperacion = true;
      resAdmin.Mensaje = 'Se devuelve resultado de si el usuario es administrador.';

      return resAdmin;
    } catch (Error) {
      resAdmin.ResultadoOperacion = false;
      resAdmin.Mensaje = 'Fallo al comprobar si el usuario es administrador.';

      return resAdmin;
    }
  }
}
