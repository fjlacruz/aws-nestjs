import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('Postulantes')
export class Postulantes {

    @PrimaryGeneratedColumn()
    idPostulante: number;
    
    @Column()
    RUN: number;

    @Column()
    DV:string;

    @Column()
    Nombres:string;

    @Column()
    ApellidoPaterno: string;

    @Column()
    ApellidoMaterno: string;

    @Column()
    Calle: string;
    
    @Column()
    CalleNro: number;
    
    @Column()
    Letra: string;

    @Column()
    RestoDireccion: string;

    @Column()
    Nacionalidad: string;

    @Column()
    Email: string;

    @Column()
    Profesion: string;

    @Column()
    Diplomatico: boolean;

    @Column()
    discapacidad: boolean;

    @Column()
    DetalleDiscapacidad: string;

    @Column()
    updated_at: Date;

    @Column()
    created_at: Date;

    @Column()
    idUsuario: number;

    @Column()
    Telefono: string;

    @Column()
    FechaNacimiento: Date;

    @Column()
    FechaDefuncion: Date;

    @Column()
    idRegion: number;

    @Column()
    idComuna: number;

    @Column()
    idOpcionSexo: number;

    @Column()
    idOpcionEstadosCivil: number;

    @Column()
    idOpNivelEducacional: number;
}