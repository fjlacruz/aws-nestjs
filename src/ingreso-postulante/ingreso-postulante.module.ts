import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { IngresoPostulanteController } from './controller/ingreso-postulante/ingreso-postulante.controller';
import { Postulantes } from './entity/aspirante.entity';
import { IngresoPostulanteService } from './services/ingreso-postulante/ingreso-postulante.service';
import { ConductoresEntity } from '../conductor/entity/conductor.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Postulantes, ComunasEntity, User, RolesUsuarios, ConductoresEntity])],
  controllers: [IngresoPostulanteController],
  providers: [IngresoPostulanteService, AuthService],
  exports: [IngresoPostulanteService]
})
export class IngresoPostulanteModule {}
