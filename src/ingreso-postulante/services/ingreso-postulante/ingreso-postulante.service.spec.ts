import { Test, TestingModule } from '@nestjs/testing';
import { IngresoPostulanteService } from './ingreso-postulante.service';

describe('IngresoAspiranteService', () => {
  let service: IngresoPostulanteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IngresoPostulanteService],
    }).compile();

    service = module.get<IngresoPostulanteService>(IngresoPostulanteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
