import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository, UpdateResult } from 'typeorm';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { CreateAspiranteDto } from 'src/ingreso-postulante/DTO/createAspirante.dto';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { RegionesEntity } from 'src/regiones/entity/regiones.entity';
import { OpcionSexoEntity } from 'src/opciones-sexo/entity/sexo.entity';
import { OpcionesNivelEducacional } from 'src/opciones-nivel-educacional/entity/nivel-educacional.entity';
import { OpcionEstadosCivilEntity } from 'src/opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { AxiosResponse } from 'axios';
import { AuthService } from 'src/auth/auth.service';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { User } from 'src/users/entity/user.entity';
import axios from 'axios';
import { ifError } from 'assert';
import { Resultado } from 'src/utils/resultado';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { formatearRun, devolverValorRUN, devolverValorDV } from 'src/shared/ValidarRun';
import { PostulantesDto } from 'src/ingreso-postulante/DTO/PostulanteDto.dto';
import { PermisosNombres } from 'src/constantes';
var ping = require('ping');

@Injectable()
export class IngresoPostulanteService {
  salidaUpdate;
  salidaInsert;
  flagInsertLic;
  flagUpadteLic;
  salidaCambiestado;
  flagSalidaProceso;
  SalidaInsertHis;
  BASE_URL_REGISTRO_CIVIL = 'https://4qnzjlsq1k.execute-api.sa-east-1.amazonaws.com';
  BASE_URL_INTERSGL = 'https://gechmrsjmh.execute-api.sa-east-1.amazonaws.com';

  constructor(
    private readonly authService: AuthService,

    @InjectRepository(Postulantes)
    private readonly ingresoAspiranteRespository: Repository<Postulantes>,

    @InjectRepository(Postulantes)
    private readonly postulanteRepository: Repository<Postulantes>,

    @InjectRepository(ConductoresEntity)
    private readonly conductoresRepository: Repository<ConductoresEntity>,

    @InjectRepository(Postulantes)
    private readonly postulanteEntityRepository: Repository<PostulanteEntity>,

    @InjectRepository(User)
    private readonly userRepo: Repository<User>
  ) {}

  arrayRC: any = [];
  async getPostulantes() {
    return await this.createQuery('postulantes').getMany();
  }
  async getPostulante(idPostulante: number) {
    const id = Number(idPostulante);
    const postulante = await this.createQuery('postulante').where('postulante.idPostulante = :id', { id }).getOne();

    if (!postulante) throw new NotFoundException('postulante dont exist');

    return postulante;
  }
  async create(dto: CreateAspiranteDto): Promise<Postulantes> {
    const created_at = new Date();
    const updated_at = created_at;
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const data = { ...dto, updated_at, created_at, idUsuario };

    return await this.postulanteRepository.save(data);
  }

  async getPostulanteByRUN(run: number): Promise<any> {
    return await this.createQuery('postulante').where('postulante.RUN = :run', { run }).getMany();
  }

  async getPostulanteByRUNV2(run: number): Promise<any> {
    let res: Resultado = new Resultado();

    try {
      const resultadoOperacion = await this.createQuery('postulante').where('postulante.RUN = :run', { run }).getOne();

      if (resultadoOperacion) {
        res.ResultadoOperacion = true;
        res.Respuesta = resultadoOperacion;
      } else {
        res.Error = 'El RUN no se encuentra registrado en el SGLC.';
        res.ResultadoOperacion = false;
      }
    } catch (err) {
      console.error(err);
      res.Error = 'Ha ocurrido un error, por favor intentelo mas tarde.';
      res.ResultadoOperacion = false;
    }

    return res;
  }

  async getPostulanteByRUNDV(run: number, dv: string, idUsuario: number): Promise<any> {
    const postulante = await this.postulanteRepository
      .createQueryBuilder('postulante')
      .where('postulante.RUN = :run', { run })
      .andWhere('postulante.DV = :dv', { dv: dv.toUpperCase() })
      .getOne();
    //console.log(' -------------------- ');

    try {
      const axios = require('axios');
      const data = { idUsuario: idUsuario, run: run.toString() + dv };
      const piseeProfesion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-profesion', data);
      const piseeDireccion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-pesronadireccion', data);
      console.log(piseeDireccion.data);
      console.log(piseeProfesion.data);
      // TODO: agregar validacion acorde a distintos tipos de respuesta. ver logica de negocio faltante sobre profesion.
      if (piseeProfesion && piseeDireccion && piseeProfesion.data && piseeDireccion.data) {
        const persona = piseeDireccion.data.respuesta.Persona || piseeProfesion.data.respuesta.Persona;
        const run = Number(piseeProfesion.data.run);
        const dv = piseeProfesion.data.dv;
        const profesion = piseeProfesion.data.respuesta.Profesion;
        const direccion = piseeDireccion.data.respuesta.Direccion;
        console.log(
          '================================================ direccion =================================================================================='
        );
        console.log(
          '================================================ direccion =================================================================================='
        );
        console.log(
          '================================================= direccion================================================================================='
        );
        console.log(direccion);
        if (!postulante) {
          await this.crearPostulante(run, dv, persona, profesion, direccion);
        } else {
          await this.updatePostulante(run, dv, persona, profesion, direccion, postulante);
        }
      }
    } catch (err) {
      console.error(err);
    }

    return await this.createQuery('postulante').where('postulante.RUN = :run', { run }).getOne();
  }

  async getPostulanteRegistroByRUN(RUN: string, idUsuario: number): Promise<any> {
    const RUT = devolverValorRUN(RUN);
    const dv = devolverValorDV(RUN);

    const postulante = await this.postulanteRepository
      .createQueryBuilder('postulante')
      .where('postulante.RUN = :run', { run: RUT })
      .andWhere('postulante.DV = :dv', { dv: dv })
      .getOne();

    if (!postulante) {
      try {
        const axios = require('axios');
        const data = { idUsuario: idUsuario, run: RUT + dv };
        const piseeProfesion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-profesion', data);
        const piseeDireccion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-pesronadireccion', data);

        if (piseeProfesion && piseeDireccion && piseeProfesion.data && piseeDireccion.data) {
          const persona = piseeProfesion.data.respuesta.Persona;
          const run = Number(piseeProfesion.data.run);
          const dv = piseeProfesion.data.dv;
          const profesion = piseeProfesion.data.respuesta.Profesion;
          const direccion = piseeDireccion.data.respuesta.Direccion;

          await this.crearPostulante(Number(RUT), dv, persona, profesion, direccion);
        }
      } catch (err) {
        console.error(err);
      }
    }
    return await this.createQuery('postulante').where('postulante.RUN = :value', { value: RUT }).getOne();
  }

  async getConductorByRUNDV(run: number, dv: string, idUsuario: string): Promise<any> {
    console.log('getConductorByRUNDV');
    const postulante: Postulantes = await this.ingresoAspiranteRespository
      .createQueryBuilder('postulante')
      .where('postulante.RUN = :run', { run })
      .andWhere('postulante.DV = :dv', { dv: dv.toUpperCase() })
      .getOne();

    const conductor: ConductoresEntity = await this.conductoresRepository
      .createQueryBuilder('conductores')
      .where('conductores.idPostulante = :idPostulante', { idPostulante: postulante.idPostulante })
      .getOne();

    if (!postulante) {
      try {
        const axios = require('axios');
        const data = { idUsuario: 8, run: run.toString() + dv };
        const piseeProfesion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-profesion', data);
        const piseeDireccion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-pesronadireccion', data);

        if (piseeProfesion && piseeDireccion && piseeProfesion.data && piseeDireccion.data) {
          const persona = piseeProfesion.data.respuesta.Persona;
          const run = Number(piseeProfesion.data.run);
          const dv = piseeProfesion.data.dv;
          const profesion = piseeProfesion.data.respuesta.Profesion;
          const direccion = piseeDireccion;

          const postulanteNew: Postulantes = await this.crearPostulante(run, dv, persona, profesion, direccion);
          if (!conductor) {
            const newConductor = new ConductoresEntity();
            newConductor.idPostulante = postulanteNew.idPostulante;
            newConductor.ingresadoPor = +idUsuario;
            newConductor.update_at = new Date();
            newConductor.created_at = new Date();
            await this.conductoresRepository.save(newConductor);
          }
        }
      } catch (err) {
        console.error(err);
      }
    } else {
      if (!conductor) {
        const newConductor = new ConductoresEntity();
        newConductor.idPostulante = postulante.idPostulante;
        newConductor.ingresadoPor = +idUsuario;
        newConductor.update_at = new Date();
        newConductor.created_at = new Date();
        await this.conductoresRepository.save(newConductor);
      }
    }

    const postulanteFinal: Postulantes = await this.ingresoAspiranteRespository
      .createQueryBuilder('postulante')
      .where('postulante.RUN = :run', { run })
      .andWhere('postulante.DV = :dv', { dv: dv.toUpperCase() })
      .getOne();
    return await this.conductoresRepository
      .createQueryBuilder('conductores')
      .where('conductores.idPostulante = :idPostulante', { idPostulante: postulanteFinal.idPostulante })
      .getOne();
  }

  /**
   * Metodo para crear un postulante con datos obtenidos de pise.
   * @param run
   * @param dv
   * @param persona
   * @param profesion
   * @param direccion
   * @private
   */
  private async crearPostulante(run: number, dv: string, persona: any, profesion: any, direccion: any) {
    if (run && dv && persona && direccion) {
      const postulante = new CreateAspiranteDto();

      postulante.RUN = run;
      postulante.DV = dv.toUpperCase();
      postulante.Nombres = persona.nombres;
      postulante.ApellidoPaterno = persona.apellidoPrimario;
      postulante.ApellidoMaterno = persona.apellidoSecundario;
      postulante.Nacionalidad = persona.nacionalidad;
      postulante.FechaNacimiento = this.getFecha(persona.fechaNacimiento);
      postulante.DetalleDiscapacidad = '';
      postulante.Calle = direccion.calle;
      postulante.CalleNro = Number(direccion.numero) || 0;
      postulante.Letra = direccion.letra;
      postulante.RestoDireccion = direccion.resto;

      if (persona.estadoCivil) {
        if (persona.estadoCivil.toLowerCase() == 'soltero/a') postulante.idOpcionEstadosCivil = 2;
        else if (persona.estadoCivil.toLowerCase() == 'viudo/a') postulante.idOpcionEstadosCivil = 4;
        else if (persona.estadoCivil.toLowerCase() == 'casado/a') postulante.idOpcionEstadosCivil = 1;
        else if (persona.estadoCivil.toLowerCase() == 'divorciado/a') postulante.idOpcionEstadosCivil = 3;
        else postulante.idOpcionEstadosCivil = 5;
      }

      if (persona.genero) {
        if (persona.genero.toLowerCase() == 'masculino') postulante.idOpcionSexo = 2;
        else postulante.idOpcionSexo = 1;
      }

      if (profesion) {
        if (typeof profesion == 'string') postulante.Profesion = profesion;
        else postulante.Profesion = profesion[0].profesion;
      }

      if (postulante.Profesion) {
        if (postulante.Profesion.toLowerCase() != 'sin profesion registrada') {
          postulante.idOpNivelEducacional = 3;
        } else {
          postulante.idOpNivelEducacional = 1;
        }
      }

      if (direccion.codigoComuna) {
        const comuna = await this.getComuna(direccion.codigoComuna);

        if (comuna) {
          postulante.idRegion = comuna.idRegion;
          postulante.idComuna = comuna.idComuna;
        }
      } else {
        postulante.idRegion = 22;
        postulante.idComuna = 304;
      }

      return await this.create(postulante);
    }
    return null;
  }

  /**
   * Metodo para actualizar un postulante con datos obtenidos de pise.
   * @param run
   * @param dv
   * @param persona
   * @param profesion
   * @param direccion
   * @param postulante
   * @private
   */
  private async updatePostulante(run: number, dv: string, persona: any, profesion: any, direccion: any, postulante: Postulantes) {
    if (run && dv && persona && direccion) {
      postulante.RUN = run;
      postulante.DV = dv.toUpperCase();
      postulante.Nombres = persona.nombres;
      postulante.ApellidoPaterno = persona.apellidoPrimario;
      postulante.ApellidoMaterno = persona.apellidoSecundario;
      postulante.Nacionalidad = persona.nacionalidad;
      postulante.FechaNacimiento = this.getFecha(persona.fechaNacimiento);
      postulante.DetalleDiscapacidad = '';
      postulante.Calle = direccion.calle;
      postulante.CalleNro = Number(direccion.numero) || 0;
      postulante.Letra = direccion.letra;
      postulante.RestoDireccion = direccion.resto;

      if (persona.estadoCivil) {
        if (persona.estadoCivil.toLowerCase() == 'soltero/a') postulante.idOpcionEstadosCivil = 2;
        else if (persona.estadoCivil.toLowerCase() == 'viudo/a') postulante.idOpcionEstadosCivil = 4;
        else if (persona.estadoCivil.toLowerCase() == 'casado/a') postulante.idOpcionEstadosCivil = 1;
        else if (persona.estadoCivil.toLowerCase() == 'divorciado/a') postulante.idOpcionEstadosCivil = 3;
        else postulante.idOpcionEstadosCivil = 5;
      }

      if (persona.genero) {
        if (persona.genero.toLowerCase() == 'masculino') postulante.idOpcionSexo = 2;
        else postulante.idOpcionSexo = 1;
      }

      if (profesion) {
        if (typeof profesion == 'string') postulante.Profesion = profesion;
        else postulante.Profesion = profesion[0].profesion;
      }

      if (postulante.Profesion) {
        if (postulante.Profesion.toLowerCase() != 'sin profesion registrada') {
          postulante.idOpNivelEducacional = 3;
        } else {
          postulante.idOpNivelEducacional = 1;
        }
      }

      if (direccion.codigoComuna) {
        const comuna = await this.getComuna(direccion.codigoComuna);

        if (comuna) {
          postulante.idRegion = comuna.idRegion;
          postulante.idComuna = comuna.idComuna;
        }
      } else {
        postulante.idRegion = 22;
        postulante.idComuna = 304;
      }

      const updated_at = new Date();
      const idUsuario = (await this.authService.usuario()).idUsuario;
      const data = { ...postulante, updated_at, idUsuario };

      return await this.postulanteRepository.save(data);
    }
    return null;
  }

  private getFecha(fecha: string) {
    const fechas = fecha.split('/');
    const nueva = new Date(Number(fechas[2]), Number(fechas[1]) - 1, Number(fechas[0]));

    return nueva;
  }

  async getPostulanteByRUNInterno(run: number): Promise<any> {
    return await this.createQuery('postulante').where('postulante.RUN = :run', { run }).getOne();
  }

  async getComuna(codigoComuna: number): Promise<any> {
    return await getConnection()
      .createQueryBuilder()
      .select('Comunas')
      .from(ComunasEntity, 'Comunas')
      .where('Comunas.codigoComuna = :codigoComuna', { codigoComuna: codigoComuna })
      .getOne();
  }

  createQuery(alias: string) {
    return this.postulanteRepository
      .createQueryBuilder(alias)
      .innerJoinAndMapMany(alias + '.idComuna', ComunasEntity, 'com', alias + '.idComuna             = com.idComuna')
      .innerJoinAndMapMany(alias + '.idRegion', RegionesEntity, 'reg', alias + '.idRegion             = reg.idRegion')
      .leftJoinAndMapOne(alias + '.OpcionSexo', OpcionSexoEntity, 'sex', alias + '.idOpcionSexo         = sex.idOpcionSexo')
      .leftJoinAndMapOne(
        alias + '.OpNivelEducacional',
        OpcionesNivelEducacional,
        'edu',
        alias + '.idOpNivelEducacional = edu.idOpNivelEducacional'
      )
      .leftJoinAndMapOne(
        alias + '.OpcionEstadoCivil',
        OpcionEstadosCivilEntity,
        'est',
        alias + '.idOpcionEstadosCivil = est.idOpcionEstadosCivil'
      );
  }
  async createByRUNNoExist(createAspiranteDto: CreateAspiranteDto): Promise<Postulantes> {
    const postul = await this.getPostulanteByRUNInterno(createAspiranteDto.RUN);

    if (!postul) return await this.postulanteRepository.save(createAspiranteDto);

    return null;
  }
  async update(req: any, idPostulante: number, data: Partial<CreateAspiranteDto>) {
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
      req,
      PermisosNombres.IngresoyEvaluacionDatosPersonalesModificarDatosPersonalesdelCiudadano
    );

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }

    await this.postulanteRepository.update({ idPostulante }, data);
    return await this.postulanteRepository.findOne({ idPostulante });
  }

  async consulPerDir(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-pesronadireccion', data);
      let direccion = datos.data.respuesta;
      return { direccion };
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.consulPerDir(data);
    }
  }

  async consulProf(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-profesion', data);
      let profesion = datos.data.respuesta;
      return { profesion };

      //return datos.data.respuesta.Profesion;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.consulProf(data);
    }
  }

  async consulHis(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/rnc-historicornc', data);
      //console.log(datos.data.respuesta.Licencias);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.consulHis(data);
    }
  }

  async certificadoAntecedentes(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/certificadoantecedentes', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.certificadoAntecedentes(data);
    }
  }

  async certificadoAntecedentesJuzgado(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/certificadoantecedentesjuzgado', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.certificadoAntecedentesJuzgado(data);
    }
  }
  async consultaUltimosDatosRNC(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/rnc-ultimosdatosrnc', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.consultaUltimosDatosRNC(data);
    }
  }

  async insertLic(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/insertlicencia', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.insertLic(data);
    }
  }

  async insertHistLic(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/insertlichist', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.insertHistLic(data);
    }
  }

  async insertHis(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/inserthistorico', data);
      //console.log('inyeccion his');
      //console.log(datos.data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.insertHis(data);
    }
  }

  async updateLic(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/updatelicencia', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.updateLic(data);
    }
  }

  async anotaciones(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/anotaciones', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.anotaciones(data);
    }
  }

  async sentencias(data) {
    try {
      //console.log(data[1].Sentencias);
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/sentencias', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.sentencias(data);
    }
  }

  async duplicados(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/duplicados', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.duplicados(data);
    }
  }

  async domicilio(data) {
    try {
      const datos = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/insertardomicilio', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.domicilio(data);
    }
  }

  async consulLic(data) {
    try {
      const datos = await axios.post(this.BASE_URL_INTERSGL + '/api/intersgl-licencias', data);
      //console.log(datos.data.Licencias);
      return datos.data.Licencias;
    } catch (e) {
      console.log(e);
      this.errorExcesoPeticiones(e);
      return this.consulLic(data);
    }
  }

  async consulLid(data) {
    try {
      const datos = await axios.post(this.BASE_URL_INTERSGL + '/api/intersgl-duplicados', data);
      // console.log(datos.data.Duplicados);
      return datos.data.Duplicados;
    } catch (e) {
      console.log(e);
      this.errorExcesoPeticiones(e);
      return this.consulLid(data);
    }
  }

  async consulSen(data) {
    try {
      const datos = await axios.post(this.BASE_URL_INTERSGL + '/api/intersgl-sentencias', data);
      return datos.data.respuesta;
    } catch (e) {
      console.log(e);
      this.errorExcesoPeticiones(e);
      return this.consulSen(data);
    }
  }

  async consulSenAux(data) {
    try {
      const datos = await axios.post(this.BASE_URL_INTERSGL + '/api/intersgl-sentenciasaux', data);
      return datos.data.respuesta;
    } catch (e) {
      console.log(e);
      this.errorExcesoPeticiones(e);
      return this.consulSenAux(data);
    }
  }

  async consultaRestricciones(data) {
    try {
      const datos = await axios.post(this.BASE_URL_INTERSGL + '/api/intersgl-restricciones', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.consultaRestricciones(data);
    }
  }

  async consultaDenegaciones(data) {
    try {
      const datos = await axios.post(this.BASE_URL_INTERSGL + '/api/intersgl-denegaciones', data);
      return datos.data;
    } catch (e) {
      this.errorExcesoPeticiones(e);
      return this.consultaDenegaciones(data);
    }
  }

  async errorExcesoPeticiones(e) {
    let err = e.response.status;
    if (err === 429) {
      await this.reintentoConsulta(10000);
      console.log('exceso de peticiones');
    }
  }
  async reintentoConsulta(milisec) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('');
      }, milisec);
    });
  }
  async validaStatus() {
    var host = ['privqa.srcei.cl'];

    let res = await ping.promise.probe(host);
    if (res.alive === true) {
      res.alive = 'OK';
    } else {
      res.alive = 'NOK';
    }
    let respTest = {
      host: res.host,
      ip: res.numeric_host,
      estatus: res.alive,
    };
    //console.log(respTest);
    return respTest.estatus;
  }

  async cambioEstadoIngPost(data) {
    try {
      let dataConsulta = data[0];
      let dataLic = data[1].Licencias;
      let tipoLic = data[0].tipoLic;

      //consulta lic  por run+dv
      let consulLic = await this.consulLic(dataConsulta);

      if (consulLic == undefined) {
        consulLic = [];
      }

      let lic = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));

      let longitud = data[0].tipoLic.length;

      //inerta los registros en el historico
      //await this.insertatHistoricoCambiEstado(dataLic, dataConsulta);

      for (let i = 0; i < longitud; i++) {
        let licTipo = data[0].tipoLic[i];
        //console.log(licTipo);

        let claseLics = lic.filter(claseLic => claseLic.licTipo == licTipo.toUpperCase());
        let respuesta = { tipo: licTipo, claseLic: claseLics };

        //console.log(claseLics);

        if (respuesta.claseLic != '') {
          console.log('posee lic ' + claseLics[0].licTipo + ' , se ejecuta updateLic');
          //let licPosee = lic.filter(claseLic => claseLic.licTipo == claseLics[0].licTipo.toUpperCase());
          this.salidaInsert = {};
          let idUsuario = { idUsuario: dataConsulta.idUsuario };
          let Licencias = { Licencias: dataLic };
          //console.log(dataLic);
          //console.log(dataLic[0].licRun);
          let dataLicUpdate = [];
          dataLicUpdate.push(idUsuario);
          dataLicUpdate.push(Licencias);

          let actualizaLic = await this.updateLic(dataLicUpdate);

          if (actualizaLic) {
            //console.log(actualizaLic.respuesta);
            if (actualizaLic.respuesta.codigo === '0') {
              let codigoRespuesta = actualizaLic.respuesta.codigo;
              let idPeticion = actualizaLic.datos_enviados.Licencias[0].idPeticion;
              let datos_enviados_update = actualizaLic.datos_enviados.Licencias;
              this.flagInsertLic = 0;
              this.flagUpadteLic = 1;

              this.salidaUpdate = { codigoUpdate: 200, statusProcesoUpdate: 'OK', datosEnviadosUpdate: datos_enviados_update };
            } else {
              this.salidaUpdate = { codigoUpdate: 403, statusProcesoUpdate: 'NOK', respuestaRCeI: actualizaLic.respuesta };
            }
          } else {
            this.flagInsertLic = 0;
            this.flagUpadteLic = 0;
          }
        }

        if (respuesta.claseLic == '') {
          console.log('NO Posee lic ' + respuesta.tipo + ' , se ejecuta insertLic');
          let dat = JSON.parse(JSON.stringify(dataLic).replace(/"\s+|\s+"/g, '"'));
          this.salidaUpdate = {};

          let licPoseeIns = dat.filter(claseLic => claseLic.licTipo == respuesta.tipo.toUpperCase());
          //let licPoseeInsert = JSON.parse(JSON.stringify(licPoseeIns).replace(/"\s+|\s+"/g, '"'));
          let licPoseeInsert = JSON.parse(JSON.stringify(licPoseeIns).trim());
          //console.log(licPoseeInsert);
          let idUsuario = { idUsuario: dataConsulta.idUsuario };
          let Licencias = { Licencias: licPoseeInsert };
          let dataLicInsert = [];
          dataLicInsert.push(idUsuario);
          dataLicInsert.push(Licencias);
          let insertaLic = await this.insertLic(dataLicInsert);
          console.log(insertaLic);
          if (insertaLic) {
            if (insertaLic.respuesta.codigo === '0') {
              let datos_enviados_insert = insertaLic.datos_enviados.Licencias;
              this.flagInsertLic = 1;
              this.flagUpadteLic = 0;
              this.salidaInsert = { codigoInsert: 200, statusProcesoInsert: 'OK', datosEnviadosInsert: datos_enviados_insert };
            } else {
              this.salidaInsert = { codigoInsert: 403, statusProcesoInsert: 'NOK', respuestaRCeI: insertaLic.respuesta };
            }
          } else {
            this.flagInsertLic = 0;
            this.flagUpadteLic = 0;
          }
        }
      }

      let statusSalida = this.flagInsertLic + this.flagUpadteLic;
      if (statusSalida == 1 || statusSalida == 2) {
        this.flagSalidaProceso = 'OK';
      } else {
        this.flagSalidaProceso = 'NOK';
      }

      let salidRespuestas = Object.assign(this.salidaInsert, this.salidaUpdate, this.SalidaInsertHis);
      let proceso = 1; //iniciar tramite
      await this.insertatHistoricoCambiEstado(dataLic, dataConsulta, proceso);
      return { respuestaProceso: this.flagSalidaProceso, salidRespuestas };
    } catch (err) {
      console.log(err);
      let e = err;
      //return e;
    }
  }

  async insertatHistoricoCambiEstado(dataLic, dataConsulta, proceso) {
    console.log('============================= data que llega =============================');
    console.log({ dataLic, dataConsulta, proceso });
    //proceso=1==>iniciar tramite ==>0
    //proceso=2==>otorgar lic ==>1
    //proceso=3==>recepcion conforme ==>0
    //proceso=4==>denegar ==>1
    let tipoTramite = dataConsulta.tipoTramite;
    let estadoTramite = dataConsulta.estadoTramite;
    let idUsuario = dataConsulta.idUsuario;
    let run = dataConsulta.run;
    let idUs = {
      idUsuario,
    };
    //let dataInsertHis = [];
    let flag_;

    let L;
    var f = new Date();
    let Mes;
    let Dia;

    if ((f.getMonth() + 1).toString().length == 1) {
      Mes = '0' + (f.getMonth() + 1).toString();
    } else {
      Mes = f.getMonth() + 1;
    }
    if (f.getDate().toString().length == 1) {
      Dia = '0' + f.getDate().toString();
    } else {
      Dia = f.getDate();
    }
    //valor 1 solo al momento de otorgar, denegar o informar un duplicado
    if (proceso == 1 || proceso == 3) {
      flag_ = 0;
      //iniciar trámites, desistir, abandonar, u otros
    } else {
      flag_ = 1;
    }

    let fecha = f.getFullYear() + '' + Mes + '' + Dia;

    for (L of dataLic) {
      let hisFechaOtorgada;
      if (proceso == 2 || proceso == 3) {
        if (L.licTipo == 'A') {
          hisFechaOtorgada = fecha;
        } else {
          hisFechaOtorgada = L.licFechaUltLic;
        }
      } else {
        hisFechaOtorgada = L.licFechaUltLic;
      }
      let Hist = [
        {
          hisRun: L.licRun,
          hisTipo: L.licTipo,
          hisFechaOtorgada: hisFechaOtorgada,
          hisComunaLic: L.licComunaUltLic,
          hisFechaIngLic: fecha,
          hisFolio: L.licFolio,
          hisFecRecepcion: fecha,
          hisCodOrigen: 3,
          hisRunEscuela: L.licRunEscuela,
          hisLetraFolio: L.licLetraFolioIni,
          hisEstadoClaCli: L.licEstadoClaLic,
          hisEstadoFisLic: L.licEstadoFisLic,
          hisEstadoDigLic: L.licEstadoDigLic,
          hisFechaPrxCtl: L.licFechaPrxCtl,
          hisObservaciones: '',
          flag: flag_,
        },
      ];

      let Historico = { Historico: Hist };

      let dataInsertHis = [];
      dataInsertHis.push(idUs);
      dataInsertHis.push(Historico);

      console.log('==================== data que se envia al his ====================');
      console.log(dataInsertHis[0]);
      console.log(dataInsertHis[1]);

      let insertaEnHist = await this.insertHis(dataInsertHis);
      console.log('==================== respuestra insert his ====================');
      console.log(insertaEnHist);
    }

    let dataHis = {
      idUsuario,
      run,
    };

    let H = await this.consulHis(dataHis);
  }

  async getPostulanteByRUNDV3(RUN: string, idUsuario: number): Promise<any> {
    const RUT = devolverValorRUN(RUN);
    const dv = devolverValorDV(RUN);

    const postulante = await this.postulanteRepository
      .createQueryBuilder('postulante')
      .where('postulante.RUN = :run', { run: RUT })
      .andWhere('postulante.DV = :dv', { dv: dv })
      .getOne();

    if (!postulante) {
      try {
        const axios = require('axios');
        const data = { idUsuario: idUsuario, run: RUT + dv };
        const piseeProfesion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-profesion', data);
        const piseeDireccion = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/pisee-pesronadireccion', data);

        if (piseeProfesion && piseeDireccion && piseeProfesion.data && piseeDireccion.data) {
          const persona = piseeProfesion.data.respuesta.Persona;
          const run = Number(piseeProfesion.data.run);
          const dv = piseeProfesion.data.dv;
          const profesion = piseeProfesion.data.respuesta.Profesion;
          const direccion = piseeDireccion.data.respuesta.Direccion;

          await this.crearPostulante(Number(RUT), dv, persona, profesion, direccion);
        }
      } catch (err) {
        console.error(err);
      }
    }
    return await this.createQuery('postulante').where('postulante.RUN = :value', { value: RUN }).getOne();
  }

  async getPostulanteByIdPostulante(idPostulante: number): Promise<any> {
    //return await this.createQuery('postulante').where('postulante.RUN = :run', { run }).getMany();
    const resultado: Resultado = new Resultado();

    try {
      const postulante: PostulanteEntity = await this.postulanteEntityRepository
        .createQueryBuilder('postulante')
        .innerJoinAndMapOne('postulante.opcionSexo', OpcionSexoEntity, 'opcionSexo', 'postulante.idOpcionSexo = opcionSexo.idOpcionSexo')
        .innerJoinAndMapOne(
          'postulante.opcionEstadoCivil',
          OpcionEstadosCivilEntity,
          'opcionEstadoCivil',
          'postulante.idOpcionEstadosCivil = opcionEstadoCivil.idOpcionEstadosCivil'
        )
        .innerJoinAndMapOne(
          'postulante.opcionNivelEducacional',
          OpcionesNivelEducacional,
          'opcionNivelEducacional',
          'postulante.idOpNivelEducacional = opcionNivelEducacional.idOpNivelEducacional'
        )
        .innerJoinAndMapOne('postulante.comuna', ComunasEntity, 'comuna', 'postulante.idComuna = comuna.idComuna')
        .innerJoinAndMapOne('comuna.regiones', RegionesEntity, 'regiones', 'comuna.idRegion = regiones.idRegion')
        // .innerJoinAndMapOne(
        //   'tramites.tiposTramite',
        //   TiposTramiteEntity,
        //   'tiposTramite',
        //   'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        // )
        .where('postulante.idPostulante = :idPostulante', { idPostulante })
        .getOne();

      // Si no lo encontramos
      if (!postulante) {
        resultado.Mensaje = 'No se ha encontrado ningún postulante con el id facilitado.';
        resultado.ResultadoOperacion = false;

        return resultado;
      }

      // En este caso lo habremos encontrado, mapeamos
      let postulanteDto: PostulantesDto = new PostulantesDto();

      postulanteDto.idPostulante = postulante.idPostulante;
      postulanteDto.RUNCompleto = formatearRun(postulante.RUN + '-' + postulante.DV);
      postulanteDto.Nombres = postulante.Nombres;
      postulanteDto.ApellidoPaterno = postulante.ApellidoPaterno;
      postulanteDto.ApellidoMaterno = postulante.ApellidoMaterno;
      postulanteDto.Sexo = postulante.opcionSexo.Nombre;
      postulanteDto.EstadoCivil = postulante.opcionEstadoCivil.Nombre;
      postulanteDto.Nacionalidad = postulante.Nacionalidad;
      postulanteDto.Email = postulante.Email;
      postulanteDto.Telefono = postulante.Telefono;
      postulanteDto.FechaNacimiento = postulante.FechaNacimiento;
      // Se calcula la edad en función de la fecha de nacimiento
      postulanteDto.Edad = this.Calculate_age(postulante.FechaNacimiento).toString();
      postulanteDto.FechaDefuncion = postulante.FechaDefuncion;
      postulanteDto.NivelEducacional = postulante.opcionNivelEducacional.Nombre;
      postulanteDto.Profesion = postulante.Profesion;
      postulanteDto.Diplomatico = postulante.Diplomatico;
      postulanteDto.Discapacidad = postulante.discapacidad;
      postulanteDto.DetalleDiscapacidad = postulante.DetalleDiscapacidad;
      postulanteDto.Region = postulante.comuna.regiones.Nombre;
      postulanteDto.Comuna = postulante.comuna.Nombre;
      postulanteDto.Calle = postulante.Calle;
      postulanteDto.RestoDireccion = postulante.RestoDireccion;
      postulanteDto.CalleNro = postulante.CalleNro;
      postulanteDto.Letra = postulante.Letra;

      resultado.Mensaje = 'Se devuelve el detalle del postulante';
      resultado.ResultadoOperacion = true;
      resultado.Respuesta = postulanteDto;

      return resultado;
    } catch (Error) {
      resultado.Mensaje = 'Se ha producido un error, contacte con el administrador.';
      resultado.ResultadoOperacion = false;

      return resultado;
    }
  }

  Calculate_age(dob) {
    var diff_ms = Date.now() - dob.getTime();
    var age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }

  async getComRNC(data): Promise<any> {
    let cod = data.data;
    try {
      const comuna = await getConnection().query(`select getcomunarnc(${cod}) as resp`);
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');

      console.log(comuna[0].resp.Nombre);

      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      console.log('=================================================================================================================');
      return { nombreComuna: comuna[0].resp.Nombre };
    } catch (e) {
      console.log(e);
    }
  }

  async datosSolicitud(data): Promise<any> {
    try {
      let idSolicitud = data.idSolicitud;
      let idusuario = data.idUsuario;
      let comunaUsuarioRNC = data.comunaUsuarioRNC;
      let tipoLic = [];
      let datosEviar = [];
      let proceso = 'ingresoPostulante';
      let lics = [];

      //const idUsuario = (await this.authService.usuario()).idUsuario;

      let Mes;
      let Dia;

      var f = new Date();

      let anio = f.getFullYear();
      let mes = f.getMonth() + 1;
      let dia = f.getDate();

      if ((f.getMonth() + 1).toString().length == 1) {
        Mes = '0' + (f.getMonth() + 1).toString();
      } else {
        Mes = f.getMonth() + 1;
      }
      if (f.getDate().toString().length == 1) {
        Dia = '0' + f.getDate().toString();
      } else {
        Dia = f.getDate();
      }

      let FECHARESTRICCION = Dia + '' + Mes + '' + f.getFullYear();
      let fecha = f.getFullYear() + '' + Mes + '' + Dia;

      const datosSolicitud = await getConnection().query(`select get_datos_inicio_solicitud(${idSolicitud}) as resp`);
      let salida = datosSolicitud[0].resp.map(item => {
        return {
          idTramite: item.f1,
          idSolicitud: item.f2,
          idTipoTramite: item.f3,
          idEstadoTramite: item.f4,
          Abreviacion: item.f6,
          codRNC: item.f7,
          run: item.f8,
          dv: item.f9,
          rutEC: item.f10,
          tipoTramite: item.f11,
          estadoTramite: item.f12,
        };
      });
      let dataSol = salida;

      console.log(
        ' ============================================== dataSol ========================================================================= '
      );
      console.log(
        ' ============================================================================================================================== '
      );
      console.log(
        ' ============================================================================================================================== '
      );
      ///actualiza_tramite_a_otorgamiento
      console.log(dataSol);

      let rundv = salida[0].run.toString() + salida[0].dv;
      let run = salida[0].run.toString();
      let tipoTramite = salida[0].tipoTramite;
      let estadoTramite = salida[0].estadoTramite;

      let D;
      let rutEC;
      for (D of dataSol) {
        if (D.idTipoTramite == 5) {
          const actualizaTramiteDomicilio = await getConnection().query(`select actualiza_tramite_a_otorgamiento(${D.idTramite}) as resp`);
          console.log(
            ' ============================================== actualizaTramiteDomicilio ========================================================================= '
          );
          console.log(actualizaTramiteDomicilio);
        }

        let tipo = D.Abreviacion;
        tipoLic = [...tipoLic, tipo];

        if (D.rutEC == '' || D.rutEC == undefined || D.rutEC == null) {
          rutEC = '0';
        } else {
          rutEC = D.rutEC;
        }
        let Licencias = {
          licRun: run,
          licTipo: D.Abreviacion,
          licComunaPriLic: '0',
          licComunaUltLic: '0',
          licComunaTraLic: comunaUsuarioRNC,
          licFechaPriLic: '0',
          licFechaUltLic: '0',
          licFechaTraLic: fecha,
          licFolio: '0',
          licFolioIni: '0',
          licFolioTra: '0',
          licLetraFolio: ' ',
          licLetraFolioIni: ' ',
          licLetraFolioTra: ' ',
          licEstadoClaLic: '102',
          licEstadoFisLic: '201',
          licEstadoDigLic: '301',
          licFechaPrxCtl: '0',
          licRunEscuela: rutEC,
          licObservaciones: ' ',
        };

        lics = [...lics, Licencias];
      }

      let LicA = {
        licRun: run,
        licTipo: 'A',
        licComunaPriLic: '0',
        licComunaUltLic: '0',
        licComunaTraLic: comunaUsuarioRNC,
        licFechaPriLic: '0',
        licFechaUltLic: '0',
        licFechaTraLic: fecha,
        licFolio: '0',
        licFolioIni: '0',
        licFolioTra: '0',
        licLetraFolio: ' ',
        licLetraFolioIni: ' ',
        licLetraFolioTra: ' ',
        licEstadoClaLic: '102',
        licEstadoFisLic: '201',
        licEstadoDigLic: '301',
        licFechaPrxCtl: '0',
        licRunEscuela: rutEC,
        licObservaciones: ' ',
      };

      lics = [...lics, LicA];
      tipoLic = [...tipoLic, 'A'];

      datosEviar = [...datosEviar, { run: rundv, tipoLic: tipoLic, proceso: proceso, idUsuario: idusuario, tipoTramite, estadoTramite }];
      datosEviar = [...datosEviar, { Licencias: lics }];
      //console.log(datosEviar[0]);
      let insertarLic = await this.cambioEstadoIngPost(datosEviar);
      console.log(insertarLic);
      if (insertarLic.respuestaProceso == 'OK') {
        return { respuestaProceso: 'Datos insertados correctamente en el RC', codigoRespuesta: 200 };
      } else {
        return { respuestaProceso: 'Error al insertar los datos en el RC', codigoRespuesta: 400 };
      }
    } catch (e) {
      console.log(e);
    }
  }
  async procesaDatosSen(data): Promise<any> {
    try {
      let dataSen = data.infracciones;
      let dataSentencias = [];
      let dataSentenciasAux = [];

      let Mes;
      let Dia;

      var f = new Date();

      let anio = f.getFullYear();
      let mes = f.getMonth() + 1;
      let dia = f.getDate();
      let _senCodRes001;
      let _senCodRes002;
      let _senCodRes003;
      let _senCodInf001;
      let _senCodInf002;
      let _senCodInf003;
      let _senauxCodInf1;
      let _senauxCodRes1;
      let _senauxCodInf2;
      let _senauxCodRes2;
      let respuesta;

      if ((f.getMonth() + 1).toString().length == 1) {
        Mes = '0' + (f.getMonth() + 1).toString();
      } else {
        Mes = f.getMonth() + 1;
      }
      if (f.getDate().toString().length == 1) {
        Dia = '0' + f.getDate().toString();
      } else {
        Dia = f.getDate();
      }
      let fecha = f.getFullYear() + '' + Mes + '' + Dia;

      //get_cod_infraccion_rc

      if (data.infracciones.length <= 5) {
        if (data.infracciones.length == 1) {
          let amonestacion1 = data.infracciones[0].amonestacion;
          if (amonestacion1 == true) {
            const codInf = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[0].idInfraccion})as resp`);
            _senCodInf001 = codInf[0].resp.codInfraccion;
            _senCodInf002 = 0;
            _senCodInf003 = 0;

            _senCodRes001 = 5;
            _senCodRes002 = 0;
            _senCodRes003 = 0;
          }
        } else if (data.infracciones.length == 2) {
          let amonestacion2 = data.infracciones[1].amonestacion;
          if (amonestacion2 == true) {
            const codInf1 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[0].idInfraccion})as resp`);
            const codInf2 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[1].idInfraccion})as resp`);
            _senCodInf001 = codInf1[0].resp.codInfraccion;
            _senCodInf002 = codInf2[0].resp.codInfraccion;
            _senCodInf003 = 0;

            _senCodRes001 = 5;
            _senCodRes002 = 5;
            _senCodRes003 = 0;
          }
        } else if (data.infracciones.length == 3) {
          let amonestacion3 = data.infracciones[2].amonestacion;
          if (amonestacion3 == true) {
            const codInf1 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[0].idInfraccion})as resp`);
            const codInf2 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[1].idInfraccion})as resp`);
            const codInf3 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[2].idInfraccion})as resp`);
            _senCodInf001 = codInf1[0].resp.codInfraccion;
            _senCodInf002 = codInf2[0].resp.codInfraccion;
            _senCodInf003 = codInf3[0].resp.codInfraccion;

            _senCodRes001 = 5;
            _senCodRes002 = 5;
            _senCodRes003 = 5;
          }
        } else if (data.infracciones.length == 4) {
          let amonestacion4 = data.infracciones[3].amonestacion;
          if (amonestacion4 == true) {
            const codInf1 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[0].idInfraccion})as resp`);
            const codInf2 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[1].idInfraccion})as resp`);
            const codInf3 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[2].idInfraccion})as resp`);
            const codInfA1 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[3].idInfraccion})as resp`);
            _senCodInf001 = codInf1[0].resp.codInfraccion;
            _senCodInf002 = codInf2[0].resp.codInfraccion;
            _senCodInf003 = codInf3[0].resp.codInfraccion;
            _senauxCodInf1 = codInfA1[0].resp.codInfraccion;

            _senCodRes001 = 5;
            _senCodRes002 = 5;
            _senCodRes003 = 5;
            _senauxCodRes1 = 5;
          }
        } else {
          let amonestacion5 = data.infracciones[4].amonestacion;
          if (amonestacion5 == true) {
            const codInf1 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[0].idInfraccion})as resp`);
            const codInf2 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[1].idInfraccion})as resp`);
            const codInf3 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[2].idInfraccion})as resp`);
            const codInfA1 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[3].idInfraccion})as resp`);
            const codInfA2 = await getConnection().query(`select get_cod_infraccion_rc(${data.infracciones[4].idInfraccion})as resp`);
            _senCodInf001 = codInf1[0].resp.codInfraccion;
            _senCodInf002 = codInf2[0].resp.codInfraccion;
            _senCodInf003 = codInf3[0].resp.codInfraccion;
            _senauxCodInf1 = codInfA1[0].resp.codInfraccion;
            _senauxCodInf2 = codInfA2[0].resp.codInfraccion;
            _senCodRes001 = 5;
            _senCodRes002 = 5;
            _senCodRes003 = 5;
            _senauxCodRes1 = 5;
            _senauxCodRes2 = 5;
          }
        }
      }

      // return data.infracciones[4].idInfraccion;

      let fechaResol = data.fechaResolucion;
      fechaResol = fechaResol.replace('-', '');
      fechaResol = fechaResol.replace('-', '');

      let fechaDen = data.fechaDenuncia;
      fechaDen = fechaDen.replace('-', '');
      fechaDen = fechaDen.replace('-', '');

      let run = data.infracciones[0].run;
      run = run.substring(0, run.length - 2);

      let idUsuario = { idusuario: data.ingresadoPor };

      if (data.infracciones.length <= 3) {
        let sen = [
          {
            senJuzgado: data.codJPL,
            senFechaResol: fechaResol,
            senNroProceso: data.nroProceso.toString(),
            senAnoProceso: data.anoProceso,
            senRun: run,
            senUnidadPolicial: '0',
            senNumeroParte: '0',
            senFechaDenuncia: fechaDen,
            senCodInf001: _senCodInf001.toString(),
            senCodInf002: _senCodInf002.toString(),
            senCodInf003: _senCodInf003.toString(),
            senCodRes001: _senCodRes001.toString(),
            senCodRes002: _senCodRes002.toString(),
            senCodRes003: _senCodRes003.toString(),
            senSuspencionTot: '0',
            senPatente: ' ',
            senOrdenElim: '0',
            senFechaOs: '0',
            senFlagEliminado: '0',
            senFechaIngreso: fecha,
            senFechaRecepDoc: fecha,
            senFechaIngSent: fecha,
            senTipoIngreso: '3',
          },
        ];
        dataSentencias = [...dataSentencias, idUsuario, { Sentencias: sen }];
        let insertSen = await this.sentencias(dataSentencias);
        if (insertSen.respuesta.codigo == '0') {
          respuesta = { datosInsertados: 'OK' };
        } else {
          respuesta = { datosInsertados: 'NOK' };
        }
        console.log(insertSen);
      }

      if (data.infracciones.length > 3 && data.infracciones.length <= 4) {
        console.log('Tiene 4');
        let sen = [
          {
            senJuzgado: data.codJPL,
            senFechaResol: fechaResol,
            senNroProceso: data.nroProceso.toString(),
            senAnoProceso: data.anoProceso,
            senRun: run,
            senUnidadPolicial: '0',
            senNumeroParte: '0',
            senFechaDenuncia: fechaDen,
            senCodInf001: _senCodInf001.toString(),
            senCodInf002: _senCodInf002.toString(),
            senCodInf003: _senCodInf003.toString(),
            senCodRes001: _senCodRes001.toString(),
            senCodRes002: _senCodRes002.toString(),
            senCodRes003: _senCodRes003.toString(),
            senSuspencionTot: '0',
            senPatente: ' ',
            senOrdenElim: '0',
            senFechaOs: '0',
            senFlagEliminado: '0',
            senFechaIngreso: fecha,
            senFechaRecepDoc: fecha,
            senFechaIngSent: fecha,
            senTipoIngreso: '3',
          },
        ];
        let senA = [
          {
            senauxJuzgado: data.codJPL,
            senauxFechaResol: fechaResol,
            senauxNroProceso: data.nroProceso.toString(),
            senauxAnoProceso: data.anoProceso,
            senauxRun: run,
            senauxCorrelativo: '1',
            senauxCodInf: _senauxCodInf1.toString(),
            senauxCodRes: _senauxCodRes1.toString(),
          },
        ];
        dataSentencias = [...dataSentencias, idUsuario, { Sentencias: sen, SentenciasAux: senA }];
        let insertSen = await this.sentencias(dataSentencias);
        if (insertSen.respuesta.codigo == '0') {
          respuesta = { datosInsertados: 'OK' };
        } else {
          respuesta = { datosInsertados: 'NOK' };
        }
        console.log(insertSen.respuesta.codigo);
      }

      if (data.infracciones.length > 4) {
        console.log('Tiene 5');

        let sen = [
          {
            senJuzgado: data.codJPL,
            senFechaResol: fechaResol,
            senNroProceso: data.nroProceso.toString(),
            senAnoProceso: data.anoProceso,
            senRun: run,
            senUnidadPolicial: '0',
            senNumeroParte: '0',
            senFechaDenuncia: fechaDen,
            senCodInf001: _senCodInf001.toString(),
            senCodInf002: _senCodInf002.toString(),
            senCodInf003: _senCodInf003.toString(),
            senCodRes001: _senCodRes001.toString(),
            senCodRes002: _senCodRes002.toString(),
            senCodRes003: _senCodRes003.toString(),
            senSuspencionTot: '0',
            senPatente: ' ',
            senOrdenElim: '0',
            senFechaOs: '0',
            senFlagEliminado: '0',
            senFechaIngreso: fecha,
            senFechaRecepDoc: fecha,
            senFechaIngSent: fecha,
            senTipoIngreso: '3',
          },
        ];
        let senA = [
          {
            senauxJuzgado: data.codJPL,
            senauxFechaResol: fechaResol,
            senauxNroProceso: data.nroProceso.toString(),
            senauxAnoProceso: data.anoProceso,
            senauxRun: run,
            senauxCorrelativo: '1',
            senauxCodInf: _senauxCodInf1.toString(),
            senauxCodRes: _senauxCodRes1.toString(),
          },
          {
            senauxJuzgado: data.codJPL,
            senauxFechaResol: fechaResol,
            senauxNroProceso: data.nroProceso.toString(),
            senauxAnoProceso: data.anoProceso,
            senauxRun: run,
            senauxCorrelativo: '2',
            senauxCodInf: _senauxCodInf2.toString(),
            senauxCodRes: _senauxCodRes2.toString(),
          },
        ];
        dataSentencias = [...dataSentencias, idUsuario, { Sentencias: sen, SentenciasAux: senA }];
        let insertSen = await this.sentencias(dataSentencias);
        if (insertSen.respuesta.codigo == '0') {
          respuesta = { datosInsertados: 'OK' };
        } else {
          respuesta = { datosInsertados: 'NOK' };
        }
        console.log(insertSen);
      }

      return respuesta;
    } catch (e) {
      console.log(e);
    }
  }

  async updateDomicilioRC(data): Promise<any> {
    try {
      const ConsultaComunaRC = await getConnection().query(`select bucarcomunarnc(${data.domComuna})as resp`);
      let comunaRC = ConsultaComunaRC[0].resp.codRNC;

      let dataDOM = [
        {
          idUsuario: data.idUsuario,
        },
        {
          Domicilio: [
            {
              domRun: data.domRun.toString(),
              domComuna: comunaRC.toString(),
              domCalle: data.domCalle,
              domNumero: data.domNumero.toString(),
              domLetra: data.domLetra,
              domResto: data.domResto,
              domFechaIngresoNac: '0',
              domFechaIngresoMat: '0',
              domFechaIngresoDef: '0',
              domFechaIngresoFil: '0',
              domCodigoPostal: '',
            },
          ],
        },
      ];

      console.log(dataDOM[1].Domicilio);

      let upDom = await this.domicilio(dataDOM);
      console.log(upDom);
      if (upDom.respuesta.codigo == '0') {
        return { respDom: 'OK' };
      } else {
        return { respDom: 'NOK' };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async valida_tramitres_en_proceso_otra_muni(data): Promise<any> {
    let run = data.data;
    let rundv = run.substring(0, run.length - 1);
    try {
      const consultaTramites = await getConnection().query(`select valida_tramitres_en_proceso_otra_muni(${rundv})as resp`);
      console.log(consultaTramites);
      if (consultaTramites[0].resp == null) {
        return { resp: 0 };
      } else {
        let salida = consultaTramites[0].resp.map(item => {
          return {
            munucipalidad: item.f1,
            oficina: item.f2,
            idSolicitud: item.f3,
            idTramite: item.f4,
            estadoSolicitud: item.f5,
            fechaSolicitud: item.f6.substring(0, 10),
            tipoTramite: item.f7,
            estadoTramite: item.f8,
            claseLicencia: item.f9,
            run: item.f10,
            dv: item.f11,
          };
        });
        let dataSol = salida;
        return { resp: 1, dataSol };
      }
    } catch (e) {
      console.log(e);
    }
  }

  async consulta_estados_rc(data): Promise<any> {
    try {
      let datos = '' + "'" + data.edd.toString() + "'," + "'" + data.edf.toString() + "'," + "'" + data.pcl.toString() + "'";
      const consulta = await getConnection().query(`select consulta_estados_rc(${datos})as resp`);
      let edd = consulta[0].resp.Nombre;
      let edf = consulta[1].resp.Nombre;
      let pcl = consulta[2].resp.Nombre;

      let estados = {
        edd,
        edf,
        pcl,
      };

      return estados;
    } catch (e) {
      console.log(e);
    }
  }

  async consulta_descrip_restricciones(data): Promise<any> {
    try {
      let restricciones = [];
      let D;
      for (D of data) {
        let restric = D.sbfDatos.substr(D.sbfDatos.length - 40);

        let res1 = restric.slice(0, 4);
        res1 = Number(res1);
        let res2 = restric.slice(5, 9);
        res2 = Number(res2);
        let res3 = restric.slice(10, 14);
        res3 = Number(res3);
        let res4 = restric.slice(15, 19);
        res4 = Number(res4);
        let res5 = restric.slice(20, 24);
        res5 = Number(res5);
        let res6 = restric.slice(25, 29);
        res6 = Number(res6);
        let res7 = restric.slice(30, 34);
        res7 = Number(res7);
        let res8 = restric.slice(35, 39);
        res8 = Number(res8);
        restricciones = [...restricciones, res1, res2, res3, res4, res5, res6, res7, res8];
      }
      console.log('=================================== restricciones =======================================');
      console.log(restricciones);
      let R;
      let descripcion = [];
      for (R of restricciones) {
        let desc;

        if (R == 0) {
          desc = 'Sin informacion';
        } else {
          //consulta_descrip_restricciones
          const consulta = await getConnection().query(`select consulta_descrip_restricciones(${R})as resp`);
          let descripciones = consulta[0].resp.nombre;
          descripcion = [...descripcion, descripciones];
        }
      }
      return descripcion;
    } catch (e) {
      console.log(e);
    }
  }

  async rpi(data): Promise<any> {
    let resp;

    try {
      const consulta = await axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/rpi', data);
      if (!consulta || consulta == undefined) {
        resp = { Codigorespuesta: 403, Respuesta: 'No se pudo obtener la informacion' };
      } else {
        resp = consulta.data;
      }

      return resp;
    } catch (e) {
      console.log(e);
      return e.message;
    }
  }
}
