import { Test, TestingModule } from '@nestjs/testing';
import { InstitucionesController } from './instituciones.controller';

describe('InstitucionesController', () => {
  let controller: InstitucionesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InstitucionesController],
    }).compile();

    controller = module.get<InstitucionesController>(InstitucionesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
