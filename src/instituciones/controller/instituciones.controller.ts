import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { InstitucionesService } from '../services/instituciones.service';

@ApiTags('Instituciones')
@Controller('instituciones')
export class InstitucionesController
{
    constructor( private readonly institucionesService: InstitucionesService ) 
    {}

    @Get( '/municipalidad' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve el listado de todas las municipalidades' })
    async getMunicipalidades()
    {
        return this.institucionesService.getMunicipalidades()
                .then( data => { return { code: 200, data }; });
    }

    @Get( '/municipalidad/:id' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los datos de una municipalidad' })
    async getMunicipalidad( @Param( 'id' ) id: number )
    {
        return this.institucionesService.getMunicipalidad( id )
                .then( data => { return { code: 200, data }; });
    }

    @Get( '/municipalidadesListado' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve el listado de todas las municipalidades' })
    async getMunicipalidadesListado()
    {
        return this.institucionesService.getMunicipalidadesListado()
                .then( data => { return { code: 200, data }; });
    }

    @Get( '/municipalidadDTO/:id' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los datos de una municipalidad' })
    async getMunicipalidadDTO( @Param( 'id' ) id: number )
    {
        return this.institucionesService.getMunicipalidadDTO( id )
                .then( data => { return { code: 200, data }; });
    }

    @Post( 'institucionByRunPostulante/' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los datos de una municipalidad por RUN del Postulante' })
    async getInstitucionByRunPostulante(@Body() run: any) {
        return this.institucionesService.getInstitucionByRunPostulante( run )
                .then( data => { return { code: 200, data }; });
    }

    @Get( '/oficina/:idMunicipalidad' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve el listadado de oficinas de la municipalidad' })
    async getOficinas( @Param( 'idMunicipalidad' ) id: number )
    {
        return this.institucionesService.getOficinas( id )
                .then( data => { return { code: 200, data }; });
    }

    @Get( '/oficina/datos/:idOficina' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los datos de una oficina' })
    async getOficina( @Param( 'idOficina' ) id: number )
    {
        return this.institucionesService.getOficina( Number( id ) )
                .then( data => { return { code: 200, data }; });
    }

    @Get( '/oficina/por-comuna/:idComuna' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve el listadado de oficinas de la municipalidad' })
    async getOficinaPorComuna( @Param( 'idComuna' ) id: number )
    {
        return this.institucionesService.getOficinaPorComuna( Number( id ) )
                .then( data => { return { code: 200, data }; });
    }

    @Get( '/getMunicipalidadesConCodigo' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un listado con id y nombre de todas las municipalidades' })
    async getMunicipalidadesConCodigo()
    {
        const data = await this.institucionesService.getMunicipalidadesConCodigo();
        return { data };
    }

    @Post( 'institucionBySolicitud/' )
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los datos de una municipalidad por id de la Solicitud' })
    async getInstitucionBySolicitud(@Body() solicitud: any) {
        return this.institucionesService.getInstitucionBySolicitud( solicitud.idSolicitud )
                .then( data => { return { code: 200, data }; });
    }
}
