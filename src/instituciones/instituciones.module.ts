import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { InstitucionesController } from './controller/instituciones.controller';
import { InstitucionesService } from './services/instituciones.service';

@Module({
  imports: [TypeOrmModule.forFeature([InstitucionesEntity, OficinasEntity, Solicitudes])],
  controllers: [InstitucionesController],
  providers: [InstitucionesService],
})
export class InstitucionesModule {}
