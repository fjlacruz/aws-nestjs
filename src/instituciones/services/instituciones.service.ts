import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { InstitucionesDTO } from 'src/tramites/DTO/instituciones.dto';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { InstitucionesDto } from '../DTO/instituciones.dto';

@Injectable()
export class InstitucionesService {
  constructor(
    @InjectRepository(OficinasEntity) private readonly oficinasRepository: Repository<OficinasEntity>,
    @InjectRepository(InstitucionesEntity) private readonly institucionesRepository: Repository<InstitucionesEntity>,
    @InjectRepository(Solicitudes) private readonly solicitudesRepository: Repository<Solicitudes>
  ) {}

  async getMunicipalidades() {
    return await this.institucionesRepository.createQueryBuilder('institucion').where('institucion.idTipoInstitucion = 1').getMany();
  }
  async getMunicipalidad(id: number) {
    return await this.institucionesRepository.findOne(id);
  }

  async getMunicipalidadesListado(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let resultadoMunicipalidades: InstitucionesEntity[] = [];
    let resultadosDTO: InstitucionesDTO[] = [];

    try {
      resultadoMunicipalidades = await this.institucionesRepository
        .createQueryBuilder('institucion')
        .where('institucion.idTipoInstitucion = 1')
        .getMany();

      if (Array.isArray(resultadoMunicipalidades) && resultadoMunicipalidades.length) {
        resultadoMunicipalidades.forEach(item => {
          let nuevoElemento: InstitucionesDTO = {
            idInstitucion: item.idInstitucion,
            idTipoInstitucion: item.idTipoInstitucion,
            nombre: item.Nombre,
            descripcion: item.Descripcion,
          };

          resultadosDTO.push(nuevoElemento);
        });
      }

      if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
        resultado.Respuesta = resultadosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Municipalidades obtenidas correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Municipalidades';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Municipalidades';
    }

    return resultado;
  }

  async getMunicipalidadDTO(id: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let resultadoMunicipalidad: InstitucionesEntity = null;
    let resultadoDTO: InstitucionesDTO = null;

    try {
      resultadoMunicipalidad = await this.institucionesRepository.findOne(id);

      if (resultadoMunicipalidad != null && resultadoMunicipalidad != undefined) {
        resultadoDTO = {
          idInstitucion: resultadoMunicipalidad.idInstitucion,
          idTipoInstitucion: resultadoMunicipalidad.idTipoInstitucion,
          nombre: resultadoMunicipalidad.Nombre,
          descripcion: resultadoMunicipalidad.Descripcion,
        };
      }

      if (resultadoDTO != null && resultadoDTO != undefined) {
        resultado.Respuesta = resultadoDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Municipalidad obtenida correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontró la Municipalidad';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Municipalidad';
    }

    return resultado;
  }

  async getInstitucionByRunPostulante(objetoRun): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let institucion: InstitucionesEntity;
    const run = objetoRun.run;

    try {
      institucion = await this.institucionesRepository.findOne({
        relations: ['oficinas', 'oficinas.comunas', 'oficinas.comunas.postulante'],
        join: {
          alias: 'instituciones',
          innerJoinAndSelect: {
            oficinas: 'instituciones.oficinas',
            comunas: 'oficinas.comunas',
            postulante: 'comunas.postulante',
          },
        },
        where: qb => {
          qb.where("postulante.RUN || '-' || postulante.DV = :RUN", { RUN: run });
        },
      });

      if (institucion) {
        resultado.Respuesta = institucion;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Institución obtenida correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontró la institución';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Instituciones';
    }
    return resultado;
  }
  async getOficinas(id: number) {
    return await this.oficinasRepository.createQueryBuilder('oficina').where('oficina.idInstitucion = :id', { id }).getMany();
  }

  async getOficina(idOficina: number) {
    return await this.oficinasRepository.findOne(idOficina);
  }

  async getOficinaPorComuna(idComuna: number) {
    return await this.oficinasRepository.createQueryBuilder('oficina').where('oficina.idComuna = :idComuna', { idComuna }).getMany();
  }

  async getMunicipalidadesConCodigo() {
    const resultado: Resultado = new Resultado();

    try {
      const institucionesEntity = await this.institucionesRepository.find({
        relations: ['comunas'],
        where: { idTipoInstitucion: 1 },
      });

      console.log(institucionesEntity);

      let institucionesDTO: InstitucionesDto[] = [];

      institucionesEntity.forEach(institucion => {
        console.log(institucion);
        let institucionDTO = new InstitucionesDto();
        institucionDTO.codigo = institucion.idInstitucion;
        institucionDTO.nombre = institucion.Nombre;
        institucionDTO.comunaNombre = institucion.comunas.Nombre;

        institucionesDTO.push(institucionDTO);
      });

      resultado.Respuesta = institucionesDTO;
      resultado.ResultadoOperacion = true;
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error obteniendo las municipaliades';
      resultado.ResultadoOperacion = false;
    }

    return resultado;
  }

  async getInstitucionBySolicitud(idSolicitud): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let sol: Solicitudes;

    try {
      sol = await this.solicitudesRepository.findOne({
        relations: ['oficinas', 'oficinas.instituciones'],
        where: qb => {
          qb.where("Solicitudes.idSolicitud = :_idSolicitud", { _idSolicitud: idSolicitud });
        },
      });

      if (sol) {
        let instiDto : InstitucionesDto = new InstitucionesDto();
        
        instiDto.nombre = sol.oficinas.instituciones.Nombre;

        resultado.Respuesta = instiDto;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Institución obtenida correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontró la institución';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Instituciones';
    }
    return resultado;
  }
}
