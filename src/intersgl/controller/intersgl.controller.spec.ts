import { Test, TestingModule } from '@nestjs/testing';
import { InterSglController } from './intersgl.controller';

describe('InterSGLController', () => {
  let controller: InterSglController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InterSglController],
    }).compile();

    controller = module.get<InterSglController>(InterSglController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
