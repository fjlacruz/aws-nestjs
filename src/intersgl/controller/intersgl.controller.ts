import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/auth/guards/auth.guard';
import { getConnection, Repository } from 'typeorm';
import {
  ExmedAlcoholDTO,
  ExmedDocslDTO,
  ExmedLoginDTO,
  ExmedSicolDTO,
  ExmedAudicionDTO,
  ExmedInfoDTO,
  ExmedVisionDTO,
  ExmedConductorDTO,
  ExmedDiabetesDTO,
  ExmedMedicoDTO,
} from '../dto/exmed.dto';
import { ExmedService } from '../services/exmed.service';
import { ExteoService } from '../services/exteo.service';
import { InterSglService } from '../services/intersgl.service';

@ApiTags('Inter-SGL')
@Controller('intersgl')
@ApiBearerAuth()
export class InterSglController {
  constructor(private readonly exteoService: ExteoService, private exmedService: ExmedService, private interSGLService: InterSglService) {}

  @Public()
  @Post('exmed/login')
  @ApiOperation({ summary: 'Servicio para loguear y generar token' })
  async login(@Body() dto: ExmedLoginDTO) {
    return this.armarRespuesta(this.exmedService.login(dto));
  }

  @Post('exmed/info')
  @ApiOperation({ summary: 'Servicio para traer las preguntas info del exmed' })
  async postExMedInfo(@Body() dto: ExmedInfoDTO) {
    return this.armarRespuesta(this.exmedService.info(dto));
  }

  @Post('exmed/conductor')
  @ApiOperation({ summary: 'Servicio para traer las preguntas relacionadas con los habitos del conductor exmed' })
  async postExMedConductor(@Body() dto: ExmedConductorDTO) {
    return this.armarRespuesta(this.exmedService.conductor(dto));
  }

  @Post('exmed/diabetes')
  @ApiOperation({ summary: 'Servicio para traer las preguntas de vision del exmed' })
  async postExMedDiabetes(@Body() dto: ExmedDiabetesDTO) {
    return this.armarRespuesta(this.exmedService.diabetes(dto));
  }

  @Post('exmed/alcohol')
  @ApiOperation({ summary: 'Servicio para traer las preguntas relacionadas con el alcohol del exmed' })
  async postExMedAlcohol(@Body() dto: ExmedAlcoholDTO) {
    return this.armarRespuesta(this.exmedService.alcohol(dto));
  }

  @Post('exmed/inspeccion')
  @ApiOperation({ summary: 'Servicio para traer las preguntas relacionadas con el alcohol del exmed' })
  async postExMedInspeccion(@Body() dto: ExmedAlcoholDTO) {
    return this.armarRespuesta(this.exmedService.inspeccion(dto));
  }

  @Post('exmed/vision')
  @ApiOperation({ summary: 'Servicio para traer las preguntas de vision del exmed' })
  async postExMedVision(@Body() dto: ExmedVisionDTO) {
    return this.armarRespuesta(this.exmedService.vision(dto));
  }

  @Post('exmed/audicion')
  @ApiOperation({ summary: 'Servicio para traer las preguntas relacionadas con el alcohol del exmed' })
  async postExMedAudicion(@Body() dto: ExmedAudicionDTO) {
    return this.armarRespuesta(this.exmedService.audicion(dto));
  }

  @Post('exmed/medico')
  @ApiOperation({ summary: 'Servicio para traer las preguntas relacionadas con el fisico del exmed' })
  async postExMedMedico(@Body() dto: ExmedMedicoDTO) {
    return this.armarRespuesta(this.exmedService.medico(dto));
  }

  @Post('exmed/docs')
  @ApiOperation({ summary: 'Servicio para traer las preguntas relacionadas con los docs del exmed' })
  async postExMedDocs(@Body() dto: ExmedDocslDTO) {
    return this.armarRespuesta(this.exmedService.docs(dto));
  }

  @Post('exmed/sico')
  @ApiOperation({ summary: 'Servicio para traer las preguntas relacionadas con el sicosometrico del exmed' })
  async postExMedSico(@Body() dto: ExmedSicolDTO) {
    return this.armarRespuesta(this.exmedService.sico(dto));
  }

  async armarRespuesta(promesa: Promise<any>) {
    return promesa
      .then(data => {
        return { statusCode: 200, data };
      })
      .catch(error => {
        return { statusCode: 406, error };
      });
  }

  // @Get( 'exteo/run/:run/:dv')
  // @ApiOperation({ summary: 'Servicio para probar exteo por run-dv' })
  // async getExTeoPorRunDv( @Param('run') run: number, @Param('dv') dv: string )
  // {
  //     return this.exteoService.procesar( Number( run ), dv ).then( data => { return { statusCode: 200, data }; });
  // }

  @Get('exmed/resp_info/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuesta de informacion del examinado' })
  async getResp_info(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_info(${run}) as datos`);
    return this.respuestaSalida(info);
  }

  @Get('exmed/resp_conductor/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de habitos y antecedentes del examinado' })
  async getRespConductor(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_habitos(${run}) as datos`);
    return this.respuestaSalida(info);
  }

  @Get('exmed/resp_diabetes/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de diabetes del examinado' })
  async getRespDiabetes(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_diabetes(${run}) as datos`);
    return this.respuestaSalida(info);
  }

  @Get('exmed/resp_alcohol/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de alcohol del examinado' })
  async getResAlcohol(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_alcohol(${run}) as datos`);
    return this.respuestaSalida(info);
  }
  @Get('exmed/resp_inspeccion/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de inspeccion del examinado' })
  async getResInspeccion(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_inspeccion(${run}) as datos`);
    return this.respuestaSalida(info);
  }
  @Get('exmed/resp_vision/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de vision del examinado' })
  async getResVision(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_vision(${run}) as datos`);
    return this.respuestaSalida(info);
  }

  @Get('exmed/resp_audicion/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de vision del examinado' })
  async getResAuditivo(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_auditivo(${run}) as datos`);
    return this.respuestaSalida(info);
  }
  @Get('exmed/resp_medico/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de evaluacion fisica-medica del examinado' })
  async getResMedico(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_medico(${run}) as datos`);
    return this.respuestaSalida(info);
  }
  @Get('exmed/resp_docs/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de ducumentos subidos examinado' })
  async getResDocs(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_docs(${run}) as datos`);
    return this.respuestaSalida(info);
  }
  @Get('exmed/resp_sico/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las respuestas de evaluacion psicometrica' })
  async getResSico(@Param('run') run: number) {
    const info = await getConnection().query(`select examed_resp_sico(${run}) as datos`);
    return this.respuestaSalida(info);
  }

  async respuestaSalida(info) {
    if (info == '') {
      return { respuesta: 'sin informacion', statusCode: 400 };
    }
    return { respuesta: info[0], statusCode: 200 };
  }
}
