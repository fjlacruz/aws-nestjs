import { ApiProperty } from "@nestjs/swagger";

export class ExmedLoginDTO
{
    @ApiProperty()
    rut: number;

    @ApiProperty()
    dv: string;

    @ApiProperty()
    password: string;

    @ApiProperty()
    postulante_run: number;

    @ApiProperty()
    postulante_dv: string;
};

export class ExmedDTO
{
    @ApiProperty()
    run: number;

    @ApiProperty()
    dv: string;

    @ApiProperty()
    datos: any;
};

export class ExmedInfoDTO extends ExmedDTO
{
};

export class ExmedConductorDTO extends ExmedDTO
{
};

export class ExmedDiabetesDTO extends ExmedDTO
{
};

export class ExmedAlcoholDTO extends ExmedDTO
{
};

export class ExmedInspeccionDTO extends ExmedDTO
{
};

export class ExmedVisionDTO extends ExmedDTO
{
};

export class ExmedAudicionDTO extends ExmedDTO
{
};

export class ExmedMedicoDTO extends ExmedDTO
{
};

export class ExmedDocslDTO extends ExmedDTO
{
};

export class ExmedSicolDTO extends ExmedDTO
{
};
