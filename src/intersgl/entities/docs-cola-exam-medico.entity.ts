import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'DocsColaExamMedico')
export class DocsColaExamMedicoEntity
{
    @PrimaryGeneratedColumn()
    idDocsColaExamMedico:  number;

    @Column()
    idExamMedico: number;

    @Column()
    nombreDoc: string;

    @Column({ type: "bytea"})
    archivo: Buffer;

    @Column()
    created_at: Date;
}