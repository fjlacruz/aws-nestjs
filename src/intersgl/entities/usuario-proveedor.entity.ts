import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'UsuarioProveedor' )
export class UsuarioProveedorEntity
{
    @PrimaryGeneratedColumn()
    idUsuarioProveedor:  number
    
    @Column()
    idProveedor: number;

    @Column()
    idUsuario: number;
}