import { HttpModule, HttpService, Module } from '@nestjs/common';
import { InterSglService } from './services/intersgl.service';
import { InterSglController } from './controller/intersgl.controller';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PreguntaFormularioExaminacionesEntity } from 'src/formularios-examinaciones/entity/pregunta-formulario-examinaciones.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { TiposRespuestaPreguntaEntity } from 'src/formularios-examinaciones/entity/TiposRespuestaPregunta.entity';
import { RespuestasFormularioExaminacionEntity } from 'src/respuestas-formulario-examinacion/entity/respuestas-formulario-examinacion.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { ExteoService } from './services/exteo.service';
import { ExmedService } from './services/exmed.service';
import { ExaminacionesService } from './services/examinaciones.service';
import { RespuestasSeleccionEntity } from 'src/respuestas-seleccion/entity/respuestas-seleccion.entity';

@Module({
  imports: 
  [
    HttpModule, 
    TypeOrmModule.forFeature
    ([
      User, 
      RolesUsuarios, 
      Postulante, 
      Solicitudes, 
      PreguntaFormularioExaminacionesEntity, 
      TiposRespuestaPreguntaEntity, 
      RespuestasFormularioExaminacionEntity,
      ResultadoExaminacionEntity,
      RespuestasSeleccionEntity,
    ])
  ],
  providers: [InterSglService, AuthService, ExteoService, ExmedService, ExaminacionesService],
  controllers: [InterSglController],
})
export class InterSglModule {}
