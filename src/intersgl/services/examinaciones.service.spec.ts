import { Test, TestingModule } from '@nestjs/testing';
import { ExaminacionesService } from './examinaciones.service';

describe('ExaminacionesService', () => {
  let service: ExaminacionesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExaminacionesService],
    }).compile();

    service = module.get<ExaminacionesService>(ExaminacionesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
