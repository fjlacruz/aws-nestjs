import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { PreguntaFormularioExaminacionesEntity } from 'src/formularios-examinaciones/entity/pregunta-formulario-examinaciones.entity';
import { TiposRespuestaPreguntaEntity } from 'src/formularios-examinaciones/entity/TiposRespuestaPregunta.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { RespuestasFormularioExaminacionEntity } from 'src/respuestas-formulario-examinacion/entity/respuestas-formulario-examinacion.entity';
import { RespuestasSeleccionEntity, RespuestasSeleccionTipoExamEntity } from 'src/respuestas-seleccion/entity/respuestas-seleccion.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { ColaExaminacionTramiteEntity } from 'src/solicitudes/entity/cola-examinacion-tramite.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { EntityManager, getManager, Repository } from 'typeorm';

@Injectable()
export class ExaminacionesService {
  constructor(
    @InjectRepository(PreguntaFormularioExaminacionesEntity)
    private preguntasRepo: Repository<PreguntaFormularioExaminacionesEntity>,

    @InjectRepository(RespuestasFormularioExaminacionEntity)
    private respuestasRepo: Repository<RespuestasFormularioExaminacionEntity>,

    @InjectRepository(ResultadoExaminacionEntity)
    private resultadoRepo: Repository<ResultadoExaminacionEntity>,

    @InjectRepository(RespuestasSeleccionEntity)
    private valoresRepo: Repository<RespuestasSeleccionEntity>
  ) {}

  async procesar(datos: any, preguntas: any[], idUsuarioExaminador: number, idResultadoExam: number, idPostulante: number) {
    const respuestas = [];
    const errores = [];
    const archivos = [];
    const idUsuario = idUsuarioExaminador;
    const claves = Object.keys(datos);

    for (const clave of claves) {
      const valor = datos[clave];
      const codigo = clave.toLowerCase();
      const pregunta = preguntas.find((pregunta: any) => pregunta.codigo == codigo);

      let respuestaValor = null;

      if (pregunta) {
        const tipo = pregunta.nombreTipo.toLowerCase();
        console.log(tipo);
        //if (tipo != 'archivo') {
        switch (tipo) {
          case 'entero':
            respuestaValor = { respuestaInt: Number(valor) };
            break;

          case 'texto':
          case 'text area':
            respuestaValor = { respuestaAbierta: valor };
            break;

          case 'hora':
            respuestaValor = { respuestaHora: valor };
            break;

          case 'booleano':
            respuestaValor = { respuestaBooleana: Boolean(valor) };
            break;

          case 'fecha':
            respuestaValor = { respuestaFecha: new Date(valor) };
            break;

          case 'seleccion multiple':
            respuestaValor = { respuestaMultiple: valor };

            /*if (valores.find((seleccion: RespuestasSeleccionTipoExamEntity) => seleccion.idPreguntaExam == valor))
                respuestaValor = { idRespuesta: Number(valor) };
              console.log('============================================');
              console.log(respuestaValor);*/
            break;

          case 'array decimales':
            respuestaValor = { respuestaMultipleFloat: valor };
            break;

          case 'decimales':
            respuestaValor = { respuestaFloat: valor };
            break;

          case 'archivo':
            respuestaValor = { respuestaArchivo: valor };
            break;

          default:
            break;
        }

        if (respuestaValor) {
          const idFomularioPregunta = 1; // De donde se saca?
          const idPreguntaExam = pregunta.idPreguntaExam;
          const respuesta = {
            idResultadoExam,
            idFomularioPregunta,
            idPreguntaExam,
            idUsuarioExaminador,
            idUsuario,
            idPostulante,
            ...respuestaValor,
          };

          respuestas.push(respuesta);
        } else if (pregunta.requerida) errores.push('[' + tipo + '] - Tipo no encontrado');
        //} else archivos.push({ tipo, valor });
      } else errores.push('[' + codigo + '] - Pregunta no encontrada');
    }
    return { respuestas, archivos, errores };
  }

  async buscarValoresSeleccion(idPreguntaExam: number) {
    const valores = await this.valoresRepo.find({ idPreguntaExam });

    if (!valores.length)
      Logger.error('buscarValoresSeleccion() | Pregunta sin valores:::: ' + idPreguntaExam.toString(), 'ExaminacionesService');

    return valores;
  }

  async buscarPreguntas(formularios: any[], prefijo: string = null) {
    const sql = this.preguntasRepo
      .createQueryBuilder('pre')
      .select('pre.idPreguntaExam', 'idPreguntaExam')
      .addSelect('pre.pregunta', 'pregunta')
      .addSelect('pre.codigo', 'codigo')
      .addSelect('pre.requerida', 'requerida')
      .addSelect('pre.idTipoRespuesta', 'idTipoRespuesta')
      .addSelect('res.nombreTipo', 'nombreTipo')
      .addFrom(TiposRespuestaPreguntaEntity, 'res')
      .where('pre.idFromularioExaminaciones IN ( :...formularios)', { formularios })
      .andWhere('pre.idTipoRespuesta = res.idTipoRespuesta');

    if (prefijo) sql.andWhere('LEFT( pre.codigo, :largo ) = :prefijo', { largo: prefijo.length, prefijo });

    return await sql.getRawMany();
  }

  async buscarRespuestas(run: number, idTipoExaminacion: number) {
    const respuestas = await this.respuestasRepo
      .createQueryBuilder('res')
      .innerJoin(ResultadoExaminacionColaEntity, 'rec', 'rec.idResultadoExam     = res.idResultadoExam')
      .innerJoin(ColaExaminacionTramiteEntity, 'cet', 'cet.idColaExaminacion   = rec.idColaExaminacion')
      .innerJoin(ColaExaminacionEntity, 'col', 'col.idColaExaminacion   = cet.idColaExaminacion')
      .innerJoin(TramitesEntity, 'tra', 'cet.idTramite           = tra.idTramite')
      .innerJoin(Solicitudes, 'sol', 'tra.idSolicitud         = sol.idSolicitud')
      .innerJoin(PostulanteEntity, 'pos', 'sol.idPostulante        = pos.idPostulante')
      .where('pos.RUN               = :run', { run })
      .andWhere('col.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion })
      .getMany();

    return respuestas;
  }

  async grabarRespuestas(respuestas: RespuestasFormularioExaminacionEntity[], idResultadoExam: number) {
    const preguntas = respuestas.map((respuesta: RespuestasFormularioExaminacionEntity) => respuesta.idPreguntaExam);

    console.log('preguntas:', preguntas);

    return await getManager().transaction(async (manager: EntityManager) => {
      await manager
        .createQueryBuilder(RespuestasFormularioExaminacionEntity, 'resp')
        .delete()
        .where('idPreguntaExam IN (:...preguntas )', { preguntas })
        .execute();

      for (const respuesta of respuestas) manager.save(RespuestasFormularioExaminacionEntity, respuesta);
    });
  }

  async tieneExaminacionPendiente(run: number, idTipoExaminacion: number) {
    const examinacion = await this.buscarExaminacion(run, idTipoExaminacion);

    if (examinacion) return examinacion.aprobado == null;

    return false;
  }

  async buscarExaminacion(run: number, idTipoExaminacion: number) {
    console.log('========================================================================================================');
    console.log(
      '================================ buscarExaminacion ========================================================================'
    );
    console.log('========================================================================================================');
    try {
      // TODO: agregar ResultadoExaminacionEntity al inner joun
      // innerJoin(ResultadoExaminacionEntity, 're', 're.idResultadoExam     = rec.idResultadoExam')
      const examinacion = await this.resultadoRepo
        .createQueryBuilder('rex')
        .innerJoin(ResultadoExaminacionColaEntity, 'rec', 'rec.idResultadoExam     = rex.idResultadoExam')
        .innerJoin(ResultadoExaminacionEntity, 're', 're.idResultadoExam     = rec.idResultadoExam')
        .innerJoin(ColaExaminacionTramiteEntity, 'cet', 'cet.idColaExaminacion   = rec.idColaExaminacion')
        .innerJoin(ColaExaminacionEntity, 'col', 'col.idColaExaminacion   = cet.idColaExaminacion')
        .innerJoin(TramitesEntity, 'tra', 'cet.idTramite           = tra.idTramite')
        .innerJoin(Solicitudes, 'sol', 'tra.idSolicitud         = sol.idSolicitud')
        .innerJoin(PostulanteEntity, 'pos', 'sol.idPostulante        = pos.idPostulante')
        .where('pos.RUN               = :run', { run })
        .andWhere('col.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion })
        .andWhere('re.idFormularioExaminaciones = 2')
        .getOneOrFail();

      return examinacion;
    } catch (error) {
      Logger.error(
        'buscarExaminacion() | Mas de una examinacion para run:' + run.toString() + ' y idTipoExaminacion:' + idTipoExaminacion.toString(),
        null,
        'ExaminacionesService'
      );
      throw { mensaje: 'Mas de una examinación pendiente para el run indicado' };
    }
  }
}
