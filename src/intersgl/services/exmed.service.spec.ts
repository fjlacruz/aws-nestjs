import { Test, TestingModule } from '@nestjs/testing';
import { ExmedService } from './exmed.service';

describe('ExmedService', () => {
  let service: ExmedService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExmedService],
    }).compile();

    service = module.get<ExmedService>(ExmedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
