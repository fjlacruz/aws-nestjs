import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { EntityManager, getManager, Repository } from 'typeorm';
import {
  ExmedAlcoholDTO,
  ExmedDocslDTO,
  ExmedLoginDTO,
  ExmedSicolDTO,
  ExmedAudicionDTO,
  ExmedVisionDTO,
  ExmedInfoDTO,
  ExmedConductorDTO,
  ExmedDiabetesDTO,
  ExmedInspeccionDTO,
  ExmedDTO,
  ExmedMedicoDTO,
} from '../dto/exmed.dto';
import { DocsColaExamMedicoEntity } from '../entities/docs-cola-exam-medico.entity';
import { ExaminacionesService } from './examinaciones.service';
import { InterSglService } from './intersgl.service';

const TIPO_EXAMINACION = 4;
const FORMULARIOS = [2];

@Injectable()
export class ExmedService {
  constructor(
    private authService: AuthService,
    private interSGLService: InterSglService,
    private examinaciones: ExaminacionesService,

    @InjectRepository(Postulante)
    private postulantesRepo: Repository<Postulante>
  ) {}

  async login(dto: ExmedLoginDTO) {
    const usuario = await this.interSGLService.usuarioValidar(dto);

    if (usuario) {
      const tienePendiente = await this.tieneExaminacionPendiente(dto.postulante_run);
      console.log('tienePendiente: ' + tienePendiente);

      if (tienePendiente) return await this.authService.login(usuario.RUN);
      else throw { error: 'Postulante sin examinaciones pendientes' };
    } else throw { error: 'Usuario invalido' };
  }

  // async buscarDatosPorRun( run: number, dv: string, idUsuario: number )
  // {
  //     const url       = "exmedrun";
  //     const data      = { run, dv, idUsuario };
  //     const response  = await this.interSGLService.post( url, data );

  //     return response;
  // }

  async info(dto: ExmedInfoDTO) {
    return this.procesar(dto, 'a2_info_');
  }

  async conductor(dto: ExmedConductorDTO) {
    return this.procesar(dto, 'a1_habitos_');
  }

  async diabetes(dto: ExmedDiabetesDTO) {
    return this.procesar(dto, 'a2_diabetes_');
  }

  async alcohol(dto: ExmedAlcoholDTO) {
    return this.procesar(dto, 'a2_alcohol_');
  }

  async inspeccion(dto: ExmedInspeccionDTO) {
    return this.procesar(dto, 'a2_inspeccion_');
  }

  async vision(dto: ExmedVisionDTO) {
    return this.procesar(dto, 'a2_vision_');
  }

  async audicion(dto: ExmedAudicionDTO) {
    return this.procesar(dto, 'a2_audicion_');
  }

  async medico(dto: ExmedMedicoDTO) {
    return this.procesar(dto, 'a2_medico_');
  }

  async docs(dto: ExmedDocslDTO) {
    return this.procesar(dto, 'a2_docs_');
  }

  async sico(dto: ExmedSicolDTO) {
    return this.procesar(dto, 'a2_sico_');
  }

  private async procesar(dto: ExmedDTO, prefijo: string) {
    const { run, datos } = dto;
    const datosCantidad = this.datosCantidad(datos);

    if (datosCantidad) {
      const postulante = await this.postulantesRepo.findOne({ RUN: run });

      if (postulante) {
        const examinacion = await this.buscarExaminacion(run);
        const idPostulante = postulante.idPostulante;

        if (examinacion) {
          const idResultadoExam = examinacion.idResultadoExam;
          const preguntas = await this.buscarPreguntas(prefijo);
          console.log('preguntas:', preguntas);
          console.log('idResultadoExam:', idResultadoExam);

          if (preguntas.length) {
            const datosMapeados = this.mapearDatos(datos, prefijo);
            const mapeadosCantidad = this.datosCantidad(datosMapeados);

            if (mapeadosCantidad == preguntas.length) {
              const idUsuario = (await this.authService.usuario()).idUsuario;
              const { respuestas, archivos, errores } = await this.examinaciones.procesar(
                datosMapeados,
                preguntas,
                idUsuario,
                idResultadoExam,
                idPostulante
              );

              if (!errores.length) {
                try {
                  await getManager().transaction(async () => {
                    await this.examinaciones.grabarRespuestas(respuestas, idResultadoExam);
                    await this.grabarArchivos(archivos, idResultadoExam);
                  });
                  return { mensaje: 'datos actualizados' };
                } catch (error) {
                  throw { mensaje: JSON.stringify(error) };
                }
              } else throw { errores };
            } else throw { mensaje: 'Datos sobrantes o faltantes' };
          } else throw { mensaje: 'No hay preguntas asociadas' };
        } else throw { mensaje: 'Sin examinacion pendiente' };
      } else throw { mensaje: 'RUN-DV invalido' };
    } else throw { mensaje: 'Datos no proporcionados o vacios' };
  }

  private datosCantidad(datos: any) {
    if (datos) return Object.keys(datos).length;

    return 0;
  }

  private async grabarArchivos(archivos: any[], idResultadoExam: number) {
    if (archivos.length)
      getManager().transaction(async (manager: EntityManager) => {
        manager.delete(DocsColaExamMedicoEntity, { idResultadoExam });

        for (const archivo of archivos) manager.save(DocsColaExamMedicoEntity, { archivo, created_at: new Date(), idResultadoExam });
      });
  }

  private buscarPreguntas(prefijo: string = null) {
    return this.examinaciones.buscarPreguntas(FORMULARIOS, prefijo);
  }

  private buscarExaminacion(run: number) {
    return this.examinaciones.buscarExaminacion(run, TIPO_EXAMINACION);
  }

  private tieneExaminacionPendiente(run: number) {
    return this.examinaciones.tieneExaminacionPendiente(run, TIPO_EXAMINACION);
  }

  private mapearDatos(datos: any, prefijo: string) {
    const nuevo = {};

    Object.keys(datos).forEach((key: string) => {
      const descriptor = { value: datos[key], enumerable: true, configurable: false, writable: false };
      const clave = prefijo + key.replace(prefijo, '');

      Object.defineProperty(nuevo, clave, descriptor);
    });
    return nuevo;
  }
}
