import { Test, TestingModule } from '@nestjs/testing';
import { ExteoService } from './exteo.service';

describe('ExteoService', () => {
  let service: ExteoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExteoService],
    }).compile();

    service = module.get<ExteoService>(ExteoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
