import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { Repository } from 'typeorm';
import { ExaminacionesService } from './examinaciones.service';
import { InterSglService } from './intersgl.service';

const TIPO_EXAMINACION = 2;
const FORMULARIOS = [28];

@Injectable()
export class ExteoService {
  constructor(
    private authService: AuthService,
    private interSGLService: InterSglService,
    private examinaciones: ExaminacionesService,

    @InjectRepository(Postulante)
    private postulantesRepo: Repository<Postulante>
  ) {}

  async buscarDatosPorRun(run: number, dv: string, idUsuario: number) {
    const url = 'exteorun';
    const data = { run, dv, idUsuario };
    const response = await this.interSGLService.post(url, data);

    return response;
  }

  async buscarDatosPorFolio(folio: number, idUsuario: number) {
    const url = 'exteofolio';
    const data = { folio, idUsuario };
    const response = await this.interSGLService.post(url, data);

    return response;
  }

  async procesar(run: number, dv: string) {
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const postulante = await this.postulantesRepo.findOne({ RUN: run });
    const datos = await this.buscarDatosPorRun(run, dv, idUsuario);

    if (datos && postulante) {
      const idPostulante = postulante.idPostulante;
      const examinacion = await this.buscarExaminacion(run);
      const idResultadoExam = examinacion.idResultadoExam;
      const preguntas = await this.buscarPreguntas();
      const { respuestas, errores } = await this.examinaciones.procesar(datos, preguntas, idUsuario, idResultadoExam, idPostulante);

      if (!errores.length) this.examinaciones.grabarRespuestas(respuestas, idResultadoExam);
      else return { errores };
    }
  }

  buscarExaminacion(run: number) {
    return this.examinaciones.buscarExaminacion(run, TIPO_EXAMINACION);
  }

  buscarPreguntas() {
    return this.examinaciones.buscarPreguntas(FORMULARIOS);
  }

  buscarRespuestas(run: number) {
    return this.examinaciones.buscarRespuestas(run, TIPO_EXAMINACION);
  }

  tieneExaminacioinPendiente(run: number) {
    return this.examinaciones.tieneExaminacionPendiente(run, TIPO_EXAMINACION);
  }
}
