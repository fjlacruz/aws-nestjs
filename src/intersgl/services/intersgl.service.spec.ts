import { Test, TestingModule } from '@nestjs/testing';
import { InterSglService } from './intersgl.service';

describe('interSGLService', () => {
  let service: InterSglService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InterSglService],
    }).compile();

    service = module.get<InterSglService>(InterSglService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
