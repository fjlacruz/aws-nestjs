import { HttpService, Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/auth/auth.service';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { PreguntaFormularioExaminacionesEntity } from 'src/formularios-examinaciones/entity/pregunta-formulario-examinaciones.entity';
import { TiposRespuestaPreguntaEntity } from 'src/formularios-examinaciones/entity/TiposRespuestaPregunta.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { Proveedor } from 'src/proveedor/entities/proveedor.entity';
import { RespuestasFormularioExaminacionEntity } from 'src/respuestas-formulario-examinacion/entity/respuestas-formulario-examinacion.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { ColaExaminacionTramiteEntity } from 'src/solicitudes/entity/cola-examinacion-tramite.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { interSGLConfig } from 'src/interSGL.config';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { User } from 'src/users/entity/user.entity';
import { EntityManager, getManager, Repository } from 'typeorm';
import { ExmedLoginDTO } from '../dto/exmed.dto';
import { UsuarioProveedorEntity } from '../entities/usuario-proveedor.entity';
import { ExaminacionesService } from './examinaciones.service';

const API_URL = interSGLConfig.URL;

@Injectable()
export class InterSglService {
  constructor(
    private readonly authService: AuthService,
    // private readonly examinacionesService: ExaminacionesService,
    private readonly httpService: HttpService,

    @InjectRepository(User)
    private readonly userRepo: Repository<User>
  ) {}

  auth: string;

  async usuarioValidar(dto: ExmedLoginDTO) {
    const usuario = await this.usuarioBuscar(dto.rut, dto.dv);

    if (usuario) return this.authService.validateUserRun(usuario.RUN, dto.password);

    return null;
  }

  async logout() {
    // this.authService.logout();
  }

  async usuarioBuscar(rut: number, dv: string) {
    const usuario = await this.userRepo
      .createQueryBuilder('usr')
      .innerJoin(UsuarioProveedorEntity, 'usp', 'usr.idUsuario = usp.idUsuario')
      .innerJoin(Proveedor, 'pro', 'pro.idProveedor = usp.idProveedor')
      .where('pro.RUT = :rut', { rut })
      .andWhere('pro.DV  = :dv', { dv })
      .cache(true)
      .getOne();

    console.log(usuario);

    return usuario;
  }

  async post(urlMetodo: string, data: any) {
    const headers = { Accept: 'application/json' };
    const url = API_URL + urlMetodo;

    if (this.auth) headers['authorizaton'] = 'Bearer ' + this.auth;

    return this.httpService
      .post(url, data, { headers })
      .pipe(map(response => response.data))
      .toPromise()
      .then(response => response.data)
      .catch(error => error);
  }
}
