import { ApiProperty } from '@nestjs/swagger';

export class CreateJornadasLaboraleDto {
  @ApiProperty()
  nombre: string;

  @ApiProperty()
  descripcion: string;
}
