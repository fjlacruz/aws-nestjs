import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateJornadasLaboraleDto } from './create-jornadas-laborale.dto';

export class UpdateJornadasLaboraleDto extends PartialType(CreateJornadasLaboraleDto) {
  @ApiProperty()
  idJornadasLaborales: number;
}
