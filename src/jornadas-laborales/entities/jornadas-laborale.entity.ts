import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('JornadasLaborales')
export class JornadaLaboralEntity {
  @PrimaryGeneratedColumn()
  idJornadasLaborales: number;

  @Column()
  nombre: string;

  @Column()
  descripcion: string;
}
