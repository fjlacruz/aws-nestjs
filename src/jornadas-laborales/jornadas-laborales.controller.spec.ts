import { Test, TestingModule } from '@nestjs/testing';
import { JornadasLaboralesController } from './jornadas-laborales.controller';
import { JornadasLaboralesService } from './jornadas-laborales.service';

describe('JornadasLaboralesController', () => {
  let controller: JornadasLaboralesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JornadasLaboralesController],
      providers: [JornadasLaboralesService],
    }).compile();

    controller = module.get<JornadasLaboralesController>(JornadasLaboralesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
