import { Controller, Get, Post, Body, Param, Delete, Put } from '@nestjs/common';
import { JornadasLaboralesService } from './jornadas-laborales.service';
import { CreateJornadasLaboraleDto } from './dto/create-jornadas-laborale.dto';
import { UpdateJornadasLaboraleDto } from './dto/update-jornadas-laborale.dto';

@Controller('jornadas-laborales')
export class JornadasLaboralesController {
  constructor(private readonly jornadasLaboralesService: JornadasLaboralesService) {}

  @Post()
  create(@Body() createJornadasLaboraleDto: CreateJornadasLaboraleDto) {
    return this.jornadasLaboralesService.create(createJornadasLaboraleDto);
  }

  @Get()
  findAll() {
    return this.jornadasLaboralesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.jornadasLaboralesService.findOne(+id);
  }

  @Put()
  update(@Body() updateJornadasLaboraleDto: UpdateJornadasLaboraleDto) {
    return this.jornadasLaboralesService.update(updateJornadasLaboraleDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.jornadasLaboralesService.remove(+id);
  }
}
