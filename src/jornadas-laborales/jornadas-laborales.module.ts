import { Module } from '@nestjs/common';
import { JornadasLaboralesService } from './jornadas-laborales.service';
import { JornadasLaboralesController } from './jornadas-laborales.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JornadaLaboralEntity } from './entities/jornadas-laborale.entity';

@Module({
  imports: [TypeOrmModule.forFeature([JornadaLaboralEntity])],
  controllers: [JornadasLaboralesController],
  providers: [JornadasLaboralesService],
  exports: [JornadasLaboralesService],
})
export class JornadasLaboralesModule {}
