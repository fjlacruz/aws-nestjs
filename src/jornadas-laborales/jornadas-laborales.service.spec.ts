import { Test, TestingModule } from '@nestjs/testing';
import { JornadasLaboralesService } from './jornadas-laborales.service';

describe('JornadasLaboralesService', () => {
  let service: JornadasLaboralesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JornadasLaboralesService],
    }).compile();

    service = module.get<JornadasLaboralesService>(JornadasLaboralesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
