import { Injectable } from '@nestjs/common';
import { CreateJornadasLaboraleDto } from './dto/create-jornadas-laborale.dto';
import { UpdateJornadasLaboraleDto } from './dto/update-jornadas-laborale.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OficinasEntity } from '../tramites/entity/oficinas.entity';
import { Repository } from 'typeorm';
import { InstitucionesEntity } from '../tramites/entity/instituciones.entity';
import { JornadaLaboralEntity } from './entities/jornadas-laborale.entity';

@Injectable()
export class JornadasLaboralesService {
  constructor(@InjectRepository(JornadaLaboralEntity) private readonly jornadaLaboralEntityRepository: Repository<JornadaLaboralEntity>) {}

  async create(createJornadasLaboraleDto: CreateJornadasLaboraleDto): Promise<JornadaLaboralEntity | undefined> {
    return this.jornadaLaboralEntityRepository.save(createJornadasLaboraleDto);
  }

  async findAll(): Promise<JornadaLaboralEntity[] | undefined> {
    return this.jornadaLaboralEntityRepository.find();
  }

  async findOne(id: number): Promise<JornadaLaboralEntity | undefined> {
    return this.jornadaLaboralEntityRepository.findOne(id);
  }

  async update(updateJornadasLaboraleDto: UpdateJornadasLaboraleDto): Promise<JornadaLaboralEntity | undefined> {
    return this.jornadaLaboralEntityRepository.save(updateJornadasLaboraleDto);
  }

  async remove(id: number): Promise<JornadaLaboralEntity[] | undefined> {
    const fmi = await this.jornadaLaboralEntityRepository.find({
      where: { idJornadasLaborales: id },
    });
    return await this.jornadaLaboralEntityRepository.remove(fmi);
  }
}
