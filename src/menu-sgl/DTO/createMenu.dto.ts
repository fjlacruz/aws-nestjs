import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateMenuDto {

    @ApiPropertyOptional()
    idMenuSGL: number;
    @ApiPropertyOptional()
    titulo: string;
    @ApiPropertyOptional()
    url: string;
    @ApiPropertyOptional()
    habilitado: boolean;
    @ApiPropertyOptional()
    created_at: Date;
    @ApiPropertyOptional()
    updated_at: Date;
    @ApiPropertyOptional()
    roles: string;
    @ApiPropertyOptional()
    descripcion: string;
    @ApiPropertyOptional()
    orden: number;

}