import { Test, TestingModule } from '@nestjs/testing';
import { MenuSglController } from './menu-sgl.controller';

describe('MenuSglController', () => {
  let controller: MenuSglController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MenuSglController],
    }).compile();

    controller = module.get<MenuSglController>(MenuSglController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
