import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateMenuDto } from '../DTO/createMenu.dto';
import { MenuSglService } from '../services/menu-sgl.service';

@ApiTags('MenuSGL')
@Controller('menu-sgl')
export class MenuSglController {

    constructor(private readonly menuService: MenuSglService) { }

    // @Get('allMenus')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que devuelve todos los Menus SGL' })
    // async getMenues() {
    //     const data = await this.menuService.getMenues();
    //     return { data };
    // }

    // @Get('menuById/:id')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que devuelve un MenuSGL por Id' })
    // async getMenu(@Param('id') id: number) {
    //     const data = await this.menuService.getMenu(id);
    //     return { data };
    // }

    @Post('creaMenuSGL')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Menu SGL' })
    async addMenuSGL(
        @Body() createExamenDto: CreateMenuDto) {
    
        const data = await this.menuService.create(createExamenDto);
        return {data};

    }

    // @Get('menuByroles/:roles')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que devuelve un MenuSGL por Roles ordenados por el campo orden Ascendentemente' })
    // async getUsuarioByRUN(@Param('roles') roles: string) {          
    //     const data = await this.menuService.getMenuByroles(roles);        
    //     return { data };
    // }

    // @Get('usuario/:idUsuario')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que devuelve el menu del usuario' })
    // async getMenuPorIdUsuario( @Param('idUsuario') id: number ) {          
    //     return await this.menuService.menuPorIdUsuario( Number( id )).then( data => { return { data, code: 200 }; });        
    // }

    @Get('run/:run')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve el menu del usuario' })
    async getMenuPorRun( @Param('run') run: number, @Req() req: any)
    {
        return await this.menuService.menuPorRun( Number( run ), req).then( data => { return { data, code: 200 }; });        
    }

}
