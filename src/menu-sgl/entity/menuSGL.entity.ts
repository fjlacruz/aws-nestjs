import { ApiHideProperty, ApiProperty } from "@nestjs/swagger";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne} from "typeorm";
import { PermisosMenuEntity } from "./permisos-menu.entity";

@Entity('MenuSGL')
export class MenuSGL {

    @PrimaryGeneratedColumn()
    idMenuSGL: number;

    @ManyToOne( () => MenuSGL, menu => menu.items)
    @JoinColumn({name : 'ParentidMenuSGL'})
    padre: MenuSGL;
    
    @Column()
    TituloMenu: string;

    @Column()
    URL: string;

    @Column({ select: false })
    Habilitado: boolean;

    @Column({ select: false })
    created_at: Date;

    @Column({ select: false })
    updated_at: Date;

    @Column({ select: false })
    descripcion: string;

    @Column({ select: false })
    orden: number;

    @Column()
    iconPath: string;

    @Column({ select: false })
    ParentidMenuSGL: number;

    @OneToMany( () => MenuSGL, menu => menu.padre )
    @JoinColumn({ name: "idMenuSGL" })
    items: MenuSGL[];

    @ApiProperty()
    @OneToMany(() => PermisosMenuEntity, permisoMenu => permisoMenu.menu)
    readonly permisoMenu: PermisosMenuEntity[];
}