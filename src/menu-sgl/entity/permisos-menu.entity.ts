import { Permisos } from "src/permisos/entity/permisos.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { MenuSGL } from "./menuSGL.entity";

@Entity( 'permisosMenu' )
export class PermisosMenuEntity
{
    @PrimaryGeneratedColumn()
    idRolesMenu: number 

    @Column()
    idMenuSGL: number;
    @JoinColumn({name: 'idMenuSGL'})
    @ManyToOne(() => MenuSGL, menu => menu.permisoMenu)
    readonly menu: MenuSGL;

    @Column()
    idPermiso: number;
    @JoinColumn({name: 'idPermiso'})
    @ManyToOne(() => Permisos, permisos => permisos.permisosMenu)
    readonly permisos: Permisos;
}