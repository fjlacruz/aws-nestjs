import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { Permisos } from 'src/permisos/entity/permisos.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { jwtConstants } from 'src/users/constants';
import { User } from 'src/users/entity/user.entity';
import { MenuSglController } from './controller/menu-sgl.controller';
import { MenuSGL } from './entity/menuSGL.entity';
import { MenuSglService } from './services/menu-sgl.service';

@Module({
  imports: [JwtModule.register( { secret: jwtConstants.secret, signOptions: { expiresIn: '360d' }}) , TypeOrmModule.forFeature([MenuSGL, RolesUsuarios, Permisos, User])],
  controllers: [MenuSglController],
  exports: [MenuSglService],
  providers: [MenuSglService, AuthService, ]
})
export class MenuSglModule {}
