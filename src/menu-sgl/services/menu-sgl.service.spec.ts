import { Test, TestingModule } from '@nestjs/testing';
import { MenuSglService } from './menu-sgl.service';

describe('MenuSglService', () => {
  let service: MenuSglService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MenuSglService],
    }).compile();

    service = module.get<MenuSglService>(MenuSglService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
