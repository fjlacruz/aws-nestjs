import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Permisos } from 'src/permisos/entity/permisos.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { getConnection, Repository } from 'typeorm';
import { CreateMenuDto } from '../DTO/createMenu.dto';
import { MenuSGL } from '../entity/menuSGL.entity';
import { RolPermiso } from 'src/rolPermiso/entity/rolPermiso.entity';
import { Roles } from 'src/roles/entity/roles.entity';
import { User } from 'src/users/entity/user.entity';
import { PermisosMenuEntity } from '../entity/permisos-menu.entity';
import { AuthService } from 'src/auth/auth.service';
import { Resultado } from 'src/utils/resultado';
import { GrupoPermisoXPermisos } from 'src/grupo-permisos/entity/grupo-permiso-permisos.entity';
import { GrupoPermisos } from 'src/grupo-permisos/entity/grupo-permisos.entity';
import { GrupoPermisosRoles } from 'src/grupo-permisos/entity/grupo-permisos-roles.entity';

@Injectable()
export class MenuSglService {
  constructor(
    private authService: AuthService,
    @InjectRepository(MenuSGL)
    private readonly menuRespository: Repository<MenuSGL>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Permisos)
    private readonly permisosRepository: Repository<Permisos>
  ) {}

  // async getMenues() {
  //     return await this.menuRespository.find();
  // }

  // async getMenu(id:number){
  //     const menu= await this.menuRespository.findOne(id);
  //     if (!menu)throw new NotFoundException("MenuSGL dont exist");

  //     return menu;
  // }

  async create(createMenuDto: CreateMenuDto): Promise<MenuSGL> {
    return await this.menuRespository.save(createMenuDto);
  }

  // async getMenuByroles(roles: string): Promise<MenuSGL[]> {
  //     return await this.menuRespository.find({ where: { roles } , order: {orden:"ASC"}});
  // }

  async menuPorRun(run: number, req:any) {
    const usuario = await this.userRepository.findOne({ RUN: run });
    if (usuario) return this.menuPorIdUsuario(usuario.idUsuario, req);
  }

  async menuPorIdUsuario(idUsuario: number, req: any) {
    const permisos = await this.permisosUsuario(idUsuario);

    let esSuperAdmin : boolean = false;

    // Se comprueba si es superusuario
    const rolesUsuarioRespuesta : Resultado = await this.authService.getUserAndRolesWithToken(req);
    
    if(rolesUsuarioRespuesta && rolesUsuarioRespuesta.Respuesta){
      const rolesUsuario : RolesUsuarios[] = rolesUsuarioRespuesta.Respuesta as RolesUsuarios[];

      if(rolesUsuario && rolesUsuario.length > 0 && this.authService.checkUserSuperAdmin(rolesUsuario)){
        esSuperAdmin = true;
      }
    }
    
    // O tiene permisos o es superadministrador
    if (permisos || esSuperAdmin) {
      let ids : number[];

      if(!esSuperAdmin && permisos)
      {

        ids = permisos.map((permiso: Permisos) => {
          return Number(permiso.idPermiso);
        });
      }


      console.log(ids);


      const menu = await this.menu(ids, esSuperAdmin);


      return menu;
    }
  }

  async menu(ids: number[], esSuperAdmin: boolean) {

    const menuQuery = await this.menuRespository
      .createQueryBuilder('menu')
      .select(['menu.TituloMenu', 'menu.URL', 'menu.iconPath', 'menu.idMenuSGL','menu.Habilitado'])

      .innerJoin(PermisosMenuEntity, 'permisos', 'menu.idMenuSGL = permisos.idMenuSGL')
      
      //.where('permisos.idPermiso IN ( :...ids ) AND menu.ParentidMenuSGL is null', { ids })

      .orderBy('menu.orden')

      

      if(esSuperAdmin){
        menuQuery.where('menu.ParentidMenuSGL is null');
      }
      else{
        menuQuery.where('permisos.idPermiso IN ( :...ids ) AND menu.ParentidMenuSGL is null', { ids });
      }

      const menu = await menuQuery.getMany();

      const idsPadres = menu.map(x => x.idMenuSGL);

      if(idsPadres.length > 0){

      const menuHijosQuery = await this.menuRespository
      .createQueryBuilder('menu')
      .select(['menu.TituloMenu', 'menu.URL', 'menu.iconPath', 'menu.idMenuSGL', 'menu.ParentidMenuSGL','menu.Habilitado'])
      .innerJoin(PermisosMenuEntity, 'permisos', 'menu.idMenuSGL = permisos.idMenuSGL')
      //.where('permisos.idPermiso IN ( :...ids ) AND menu.ParentidMenuSGL IN ( :...idsPadres )', { ids, idsPadres })
      .orderBy('menu.orden');
      //.getMany();

      if(esSuperAdmin){
        menuHijosQuery.where('menu.ParentidMenuSGL IN ( :...idsPadres )', { idsPadres })
      }
      else{
        menuHijosQuery.where('permisos.idPermiso IN ( :...ids ) AND menu.ParentidMenuSGL IN ( :...idsPadres )', { ids, idsPadres })
      }


      const menuHijos = await menuHijosQuery.getMany();


      menu.forEach(x => {
        x.items = menuHijos.filter(y => y.ParentidMenuSGL == x.idMenuSGL)
      })

      const idsHijos = menuHijos.map(x => x.idMenuSGL);

      if(idsHijos.length > 0){

        const menuNietosQuery = await this.menuRespository
        .createQueryBuilder('menu')
        .select(['menu.TituloMenu', 'menu.URL', 'menu.iconPath', 'menu.idMenuSGL', 'menu.ParentidMenuSGL','menu.Habilitado'])
        .innerJoin(PermisosMenuEntity, 'permisos', 'menu.idMenuSGL = permisos.idMenuSGL')
        //.where('permisos.idPermiso IN ( :...ids ) AND menu.ParentidMenuSGL IN ( :...idsHijos )', { ids, idsHijos })
        .orderBy('menu.orden');
        
        if(esSuperAdmin){
          menuNietosQuery.where('menu.ParentidMenuSGL IN ( :...idsHijos )', { idsHijos })
        }
        else{
          menuNietosQuery.where('permisos.idPermiso IN ( :...ids ) AND menu.ParentidMenuSGL IN ( :...idsHijos )', { ids, idsHijos })
        }

        const menuNietos = await menuNietosQuery.getMany();

        menu.forEach(x => {
          x.items.forEach(y => {
            y.items = menuNietos.filter(z => z.ParentidMenuSGL == y.idMenuSGL)
          })
        })

      }
    }

    return menu;
  }

  async permisosUsuario(idUsuario: number) {
    // const rawData = await getConnection().query(
    //   `select "P"."idPermiso","P"."Nombre"  from "Permisos" as "P","RolesUsuario" as "RU", "Roles" as "R", "RolPermiso" as "RP" 
    //   where "RP"."idPermiso" = "P"."idPermiso"
    //   and "R"."idRol" = "RP"."idRol" 
    //   and "RU"."idRol" = "R"."idRol"
    //   and "RU"."idUsuario" = ` + idUsuario
    // );
    // console.log('----------------------');
    // console.log(rawData);
    // console.log('----------------------');
    try{
      let permisos : Permisos[] = await this.permisosRepository
        .createQueryBuilder('permisos')
        .leftJoin(RolPermiso, 'rolPermiso', 'permisos.idPermiso = rolPermiso.idPermiso')
        .leftJoin(Roles, 'roles', 'roles.idRol        = rolPermiso.idRol')
        .leftJoin(RolesUsuarios, 'rolUsuario', 'roles.idRol        = rolUsuario.idRol')
        .where('rolUsuario.idUsuario = :idUsuario', { idUsuario })
        .getMany();

      const permisosGrupos : Permisos[] = await this.permisosRepository
        .createQueryBuilder('permisos')
        .leftJoin(GrupoPermisoXPermisos, 'grupoPermisoXPermisos', 'permisos.idPermiso = grupoPermisoXPermisos.idPermiso')
        .leftJoin(GrupoPermisos, 'grupoPermisos', 'grupoPermisoXPermisos.idGrupoPermiso = grupoPermisos.idGrupoPermiso')
        .leftJoin(GrupoPermisosRoles, 'grupoPermisosRoles', 'grupoPermisos.idGrupoPermiso = grupoPermisosRoles.idGrupoPermiso')
        .leftJoin(Roles, 'roles', 'roles.idRol        = grupoPermisosRoles.idRol')
        .leftJoin(RolesUsuarios, 'rolUsuario', 'roles.idRol        = rolUsuario.idRol')
        .where('rolUsuario.idUsuario = :idUsuario', { idUsuario })
        .getMany();        

      if(permisosGrupos){
        permisos = permisos.concat(permisosGrupos);
      }
      
      console.log(permisos);
      return permisos;
    }
    catch(Error){
      console.log(Error);
      return [];
    }
  }
}
