import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {IsNotEmpty } from 'class-validator';

export class ListadoMotivosDenegacionDto {
    
    @ApiPropertyOptional()
    id: number;
    
    @IsNotEmpty()
    Nombre: string;
}