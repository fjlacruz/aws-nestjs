import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Resultado } from 'src/utils/resultado';
import { MotivosDenegacionService } from '../service/motivos-denegacion.service';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';


@Controller('motivosDenegacion')
@ApiTags('motivosDenegacion')
export class MotivosDenegacionController {

    constructor(

        private readonly motivosDenegacionService: MotivosDenegacionService,
        private readonly authService: AuthService
    ) { }

    @Get('/getMotivosDenegacion')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve los motivos de denegación' })
    async getMotivosDenegacion(@Req() req: any) {

        const data = await this.motivosDenegacionService.getMotivosDenegacion();
        return { data };

    }
}
