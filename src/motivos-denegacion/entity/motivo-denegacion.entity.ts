import { ApiProperty } from '@nestjs/swagger';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity('MotivosDenegacion')
export class MotivosDenegacionEntity {
  @PrimaryGeneratedColumn()
  idMotivoDenegacion: number;

  @Column()
  motivo: string;
  
  @Column()
  descripcion: string;
  
  @Column()
  codigoSRCeI: string;

  @Column()
  codigoMotivoDenegacion: string;

  @Column()
  idSiguienteEstadoTramite: number;
  @ManyToOne(() => EstadosTramiteEntity, estadosTramite => estadosTramite.motivoDenegacion)
  @JoinColumn({ name: 'idSiguienteEstadoTramite' })
  readonly estadoTramite: EstadosTramiteEntity;

  @ApiProperty()
  @OneToMany(() => TramitesEntity, tramite => tramite.motivoDenegacion)
  readonly tramite: TramitesEntity[];

}
