import { Module } from '@nestjs/common';
import { MotivosDenegacionController } from './controller/motivos-denegacion.controller';
import { MotivosDenegacionService } from './service/motivos-denegacion.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MotivosDenegacionEntity } from './entity/motivo-denegacion.entity';
import { User } from './../users/entity/user.entity';
import { UsersModule } from 'src/users/users.module';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';

@Module({
  controllers: [MotivosDenegacionController],
  providers: [MotivosDenegacionService, AuthService],
  exports: [MotivosDenegacionService],
  imports: 
  [
    TypeOrmModule.forFeature([MotivosDenegacionEntity,User,RolesUsuarios]),
    UsersModule,
  ],
})
export class MotivosDenegacionModule {}
