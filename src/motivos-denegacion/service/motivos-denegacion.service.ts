import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { ListadoMotivosDenegacionDto } from '../DTO/listado-motivos-denegacion.dto';
import { MotivosDenegacionEntity } from '../entity/motivo-denegacion.entity';

@Injectable()
export class MotivosDenegacionService {
  constructor(
    @InjectRepository(MotivosDenegacionEntity) private readonly motivosDenegacionRepository: Repository<MotivosDenegacionEntity>
  ) {}

  async getMotivosDenegacion(): Promise<Resultado> {
    //Inicializamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      const motivosDenegacion = await this.motivosDenegacionRepository.find();

      //Comprobamos que los objetos no sean vacios
      if (motivosDenegacion.length > 0) {
        const listadoMotivosDenegacionDto: ListadoMotivosDenegacionDto[] = [];

        motivosDenegacion.forEach(motivo => {
          let motivosDenegacionDto = new ListadoMotivosDenegacionDto();
          motivosDenegacionDto.id = motivo.idMotivoDenegacion;
          motivosDenegacionDto.Nombre = motivo.motivo;

          listadoMotivosDenegacionDto.push(motivosDenegacionDto);
        });

        //Manejamos la respuesta
        resultado.Respuesta = listadoMotivosDenegacionDto;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Motivos de denegación obtenidos correctamente';
      } else {
        resultado.Error = 'No se encontraron motivos de denegación';
      }
    } catch (error) {
      //capturamos error
      console.log(error);
      resultado.Error = 'Error obteniendo los motivos de denegación';
    }

    return resultado;
  }
}
