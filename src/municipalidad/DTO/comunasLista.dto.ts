import { Exclude, Expose } from "class-transformer";

@Exclude()
export class ComunasListaDto
{
    @Expose()
    idComuna:number;

    @Expose()
    nombre:string;

    @Expose()
    idRegion:number;
}