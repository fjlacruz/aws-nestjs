import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Exclude, Expose } from "class-transformer";
import {IsNotEmpty, IsNumber } from 'class-validator';
import { Column } from "typeorm";


@Exclude()
export class CrearDocumentoResolucionDTO {
    
    @Expose()
    @ApiPropertyOptional()
    idDocsResoluciones:number;

    idOficina:number;

    @Expose()
    @IsNotEmpty()
    nombreDoc:string;

    @Expose()
    @IsNotEmpty()
    @IsNumber({allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0})
    numeroResolucion: number;

    @Expose()
    @IsNotEmpty()
    archivo:string;

    @Column({
        type: 'bytea',
        nullable: false
    })
    @IsNotEmpty()
    binario: Buffer;

    @Expose()
    fechaLC:Date;

}