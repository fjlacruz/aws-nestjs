import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {IsNotEmpty } from 'class-validator';



export class CrearMunicipalidadesDto {
    
    @ApiPropertyOptional()
    idInstitucion?: number;

    @ApiPropertyOptional()
    idTipoInstitucion:number;
    
    @IsNotEmpty()
    Nombre: string;

    @ApiPropertyOptional()
    Descripcion:string;

    @ApiPropertyOptional()
    activo?:boolean;

    @ApiPropertyOptional()
    idComuna:number;

}