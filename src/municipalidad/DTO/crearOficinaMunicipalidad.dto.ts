import { ApiPropertyOptional } from "@nestjs/swagger";
import { Exclude, Expose, Type } from "class-transformer";
import {IsNotEmpty, IsOptional } from 'class-validator';
import { Column } from "typeorm";
import {DocsResolucionesEntity} from '../entity/docsResoluciones.entity';
import { CrearDocumentoResolucionDTO } from "./crearDocumentoResolucion.dto";

@Exclude()
export class CrearOficinaMunicipalidadDto {
    
    @ApiPropertyOptional()
    idOficina?: number;
    
    @Expose()
    @IsOptional()
    idComuna:number;

    @Expose()
    @IsNotEmpty()
    Nombre:string;


    @Expose()
    @IsNotEmpty()
    calleDireccion: string;
    @Expose()
    @IsNotEmpty()
    nroDireccion: number;
    @Expose()
    letraDireccion: string;
    @Expose()
    restoDireccion: string;

    @ApiPropertyOptional()
    Descripcion?:string;

    @IsNotEmpty()
    RUT:number;

    @IsNotEmpty()
    DV:string;

    @Expose()
    @IsNotEmpty()
    idInstitucion:number;
    
    @ApiPropertyOptional()
    creadoPor:number;
    
    @ApiPropertyOptional()
    activo?:boolean;

    @ApiPropertyOptional()
    motivoRechazo?:string;

    @ApiPropertyOptional()
    fechaCreacion?:Date;

    // @ApiPropertyOptional()
    // directorTransito?:number;

    // @ApiPropertyOptional()
    // directorTransitoSubrogante?:number;

    @Expose()
    @ApiPropertyOptional()
    observacion?:string;
    
    @Exclude()
    @ApiPropertyOptional()
    RUTCompleto?:string;

    @Expose()
    ListaResolucionesCrear?:CrearDocumentoResolucionDTO[];

  
}