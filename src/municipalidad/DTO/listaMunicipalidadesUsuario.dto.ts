import { Exclude, Expose } from "class-transformer";
import { IsNumber, IsString } from "class-validator";

@Exclude()
export class ListaMunicipalidadesUsuarioDto
{
    @Expose()
    @IsNumber()
    idInstitucion:number;
    
    @Expose()
    @IsNumber()
    idComuna:number;
    
    @Expose()
    @IsString()
    Nombre:string;

}