import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Exclude, Expose } from "class-transformer";
import {IsNotEmpty } from 'class-validator';
import { Column } from "typeorm";


@Exclude()
export class ListaResolucionesDTO {
    
    @Expose()
    @ApiPropertyOptional()
    idDocsResoluciones:number;

    @Expose()
    @IsNotEmpty()
    nombreDoc:string;

    @Expose()
    numeroResolucion: number;

    @Expose()
    @Column({
        type: 'bytea',
        nullable: false
    })
    @IsNotEmpty()
    binario: Buffer;

    @Expose()
    archivo:string;



    @Expose()
    fechaLC:Date;

}