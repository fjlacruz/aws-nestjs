
import {Exclude, Expose, Transform, Type} from 'class-transformer';


@Exclude()
export class MunicipalidadDto {
    
    @Expose({groups: ['listado','detalles']})
    idInstitucion: number;
    
    idTipoInstitucion:number;

    @Expose({groups: ['listado','detalles']})
    activo:boolean;
    @Expose({groups: ['listado','detalles']})
    MotivoRechazo?:string;
    
    @Expose({groups: ['listado','detalles']})
    Nombre: string;
    
    @Expose({groups: ['listado','detalles']})
    Descripcion?:string;

    @Expose({groups: ['listado','detalles']})
    listaNombresOficinas:string[];

    @Expose({groups: ['listado','detalles']})
    idComuna:number;

    @Expose({groups: ['listado','detalles']})
    NombreComuna:string;

    @Expose({groups: ['detalles']})
    @Transform(transform => {
        const {idRegion} = transform.obj.comuna.regiones;
        return idRegion;
    })
    idRegion: number;

    @Expose({groups: ['detalles']})
    @Transform(transform => {
        const {Nombre} = transform.obj.comuna.regiones;
        return Nombre;
    })
    NombreRegion: string;
}