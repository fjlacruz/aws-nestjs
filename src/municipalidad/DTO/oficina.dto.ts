import { ApiPropertyOptional } from "@nestjs/swagger";
import {IsNotEmpty } from 'class-validator';
import {Exclude, Expose,Type} from 'class-transformer';
import { ComunasEntity } from "src/comunas/entity/comunas.entity";
import { UsuariosOficinaDto } from "./usuariosOficina.dto";
import { ListaUsuariosDirectoresDto } from "./listaUsuariosDirectores.dto";
import { ListaResolucionesDTO } from "./listaResoluciones.dto";


@Exclude()
export class OficinaDto {

   
    @Expose()
    @ApiPropertyOptional()
    idOficina: number;
    
    @Expose()
    @ApiPropertyOptional()
    idInstitucion:number;
    
    @Expose()
    @IsNotEmpty()
    idComuna:number;
    
    @Expose()
    @ApiPropertyOptional()
    Nombre:string;
   
    @Expose()
    @ApiPropertyOptional()
    Direccion:string;

    @Expose()
    @ApiPropertyOptional()
    calleDireccion:string;
    @Expose()
    @ApiPropertyOptional()
    nroDireccion:number;
    @Expose()
    @ApiPropertyOptional()
    letraDireccion:string;
    @Expose()
    @ApiPropertyOptional()
    restoDireccion:string;
   
    @Expose()
    @ApiPropertyOptional()
    Descripcion:string;

   
    @Type(()=>ComunasEntity )
    readonly comunas: ComunasEntity;

    @Expose()
    NombreComuna: string;

    @Expose()
    NombreRegion: string;

    @Expose()
    ListaUsuariosOficina?:UsuariosOficinaDto[];
        
    creadoPor?:number;
    
    @Expose()
    activo?:boolean;

    @Expose()
    @ApiPropertyOptional()
    motivoRechazo:string;

    @Expose()
    fechaCreacion?:Date;


    @Expose()
    directorTransitoNombre:string;

    @Expose()
    directorSubroganteNombre:string;


    @Expose()
    ListaUsuariosDirectores?: ListaUsuariosDirectoresDto[];

    @Expose()
    RUT:string;

    @Expose()
    ListaResoluciones:ListaResolucionesDTO[]

    @Expose()
    idRegion:number;




}

