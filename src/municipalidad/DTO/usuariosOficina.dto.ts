import {Exclude, Expose, Type} from 'class-transformer';
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, OneToMany} from "typeorm";
import { OficinaDto } from './oficina.dto';

@Exclude()
export class UsuariosOficinaDto
{
    @Expose()
    RUN: number;

    @Expose()
    Nombres:string;
    
    @Expose()
    rol:string[];
   

}