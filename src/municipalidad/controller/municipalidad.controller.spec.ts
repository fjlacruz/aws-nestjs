import { Test, TestingModule } from '@nestjs/testing';
import { MunicipalidadController } from './municipalidad.controller';

describe('MunicipalidadController', () => {
  let controller: MunicipalidadController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MunicipalidadController],
    }).compile();

    controller = module.get<MunicipalidadController>(MunicipalidadController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
