import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Resultado } from 'src/utils/resultado';
import { CrearMunicipalidadesDto } from '../DTO/crearMunicipalidad.dto';
import { MunicipalidadService } from '../service/municipalidad.service';
import { CrearOficinaMunicipalidadDto } from '../DTO/crearOficinaMunicipalidad.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';

@Controller('municipalidades')
@ApiTags('municipalidades')
export class MunicipalidadController {
  constructor(private readonly municipalidadService: MunicipalidadService, private readonly authService: AuthService) {}

  //#region  Municipalidades

  @Get('/obtenerMunicipalidad/:idMunicipalidad')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve una municipalidad' })
  async getMunicipalidad(@Param('idMunicipalidad') id: number, @Req() req: any) {
    // let splittedBearerToken = req.headers.authorization.split(' ');
    // let token = splittedBearerToken[1];

    // let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesVerDetalleMunicipalidad]);

    // let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    // if (validarPermisos.ResultadoOperacion) {
    //   const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.municipalidadService.getMunicipalidad(req, id);
      return { data };
    // } else {
    //   return { data: validarPermisos };
    // }
  }

  @Post('/obtenerListaMunicipalidades')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con id y nombre de todas las municipalidades' })
  async getMunicipalidadesConCodigo(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {

      const data = await this.municipalidadService.getMunicipalidades(req, paginacionArgs);
      return { data };

  }

  @Post('/eliminarMunicipalidad/:idMunicipalidad')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina una Municipalidad' })
  async deleteMunicipalidad(@Param('idMunicipalidad') id: number, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesActivarRechazarInhabilitarMunicipalidad]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.municipalidadService.deleteMunicipalidad(id, permisos);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('/crearMunicipalidad/')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina una Municipalidad' })
  async createMunicipalidad(@Body() crearMunicipalidadesDto: CrearMunicipalidadesDto, @Req() req: any) {

      const data = await this.municipalidadService.addMunicipalidad(req, crearMunicipalidadesDto);
      return { data };

  }

  @Post('/actualizarMunicipalidad/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina una Municipalidad' })
  async updateMunicipalidad(@Param('id') id: number, @Body() crearMunicipalidadesDto: Partial<CrearMunicipalidadesDto>, @Req() req: any) {

      const data = await this.municipalidadService.updateMunicipalidad(req, id, crearMunicipalidadesDto);
      return { data };

  }

  //#endregion

  //#region Oficias

  @Get('/getOficinasListaNombres')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con id y nombre de todas las Oficina' })
  async getOficinasListaNombres(@Req() req: any) {

      const data = await this.municipalidadService.getOficinasListaNombres();
      return { data };
  }

  @Post('/obtenerListaOficinas')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con id y nombre de todas las Oficina' })
  async getOficinasMunicipalidad(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {

      const data = await this.municipalidadService.getOficinas(req, paginacionArgs);
      return { data };

  }

  @Get('/obtenerOficina/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con id y nombre de todas las Oficina' })
  async getDetalleOficinaMunicipalidad(@Param('id') id: number, @Req() req: any) {

      const data = await this.municipalidadService.getOficina(req, id);

      return { data };

  }

  @Post('crearOficina')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una Oficina' })
  async addOficina(@Body() crearOficinaDto: CrearOficinaMunicipalidadDto[], @Req() req: any) {

      const data = await this.municipalidadService.addOficinas(req, crearOficinaDto);
      return { data };

  }

  @Post('editarOficina/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una Oficina' })
  async updateOficina(@Param('id') id: number, @Body() updateOficinaDto: Partial<CrearOficinaMunicipalidadDto>, @Req() req: any) {

      const data = await this.municipalidadService.updateOficina(req, id, updateOficinaDto);
      return { data };

  }

  @Post('/eliminarOficina/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina una Oficina' })
  async deleteOficina(@Param('id') id: number, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaCreacionAdministracionMunicipalidades]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.municipalidadService.deleteOficina(id, permisos);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  //#endregion

  @Get('/institucion-usuario/:idUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con id y nombre de todas las Oficina' })
  async getInstitucionByUsuario(@Param('idUsuario') idUsuario: number, @Req() req: any) {
    const data = await this.municipalidadService.getInstitucionByIdUser(idUsuario);
    return { data };
  }

  @Get('/listaComunasUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con las comunas sobre las que puedes operar' })
  async getlistaComunasUsuario(@Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaCreacionAdministracionMunicipalidades]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];
      const data = await this.municipalidadService.obtenerComunasUsuario(permisos);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('/listaComunasUsuarioCrearMunicipalidad')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con las comunas sobre las que puedes operar' })
  async obtenerComunasUsuarioCrearMunicipalidad(@Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaCreacionAdministracionMunicipalidades]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const permisos: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];
      const data = await this.municipalidadService.obtenerComunasUsuarioCrearMunicipalidad(permisos);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('/listaMunicipalidadesUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un listado con las municipalidades disponibles según región y usuario' })
  async getlistaMunicipalidadesUsuario(@Req() req: any) {

      const data = await this.municipalidadService.obtenerMunicipalidadesUsuario(req);

      return { data };

  }
}
