import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn} from "typeorm";

@Entity('DocsResoluciones')
export class DocsResolucionesEntity {

    @PrimaryGeneratedColumn()
    idDocsResoluciones:number;

    @Column()
    @IsNotEmpty()
    nombreDoc:string;

    @Column()
    numeroResolucion: number;

    @Column({
        type: 'bytea',
        nullable: false
    })
    @IsNotEmpty()
    binario: Buffer;
    
    @Column()
    idOficina:number;
    @OneToOne(() => OficinasEntity, oficinas => oficinas.resoluciones)
    @JoinColumn({ name: 'idOficina' }) 
    readonly oficinas: OficinasEntity;

    @Column()
    fechaLC:Date;

}
