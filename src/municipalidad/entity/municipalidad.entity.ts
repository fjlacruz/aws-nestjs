import { ApiProperty } from "@nestjs/swagger";
import { ComunasEntity } from "src/comunas/entity/comunas.entity";
import { PapelSeguridadEntity } from "src/papel-seguridad/entity/papel-seguridad.entity";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, JoinColumn} from "typeorm";

@Entity('Instituciones')
export class MunicipalidadEntity {

    @PrimaryGeneratedColumn()
    idInstitucion:number;

    @Column()
    idTipoInstitucion:number;

    @Column()
    Nombre:string;

    @Column()
    activo:boolean;

    @Column()
    MotivoRechazo:string;

    @Column()
    Descripcion:string;
    @ApiProperty()
    @OneToMany(() => OficinasEntity, oficinas => oficinas.instituciones)
    readonly oficinas: OficinasEntity[];

    @Column()
    idComuna:number;
    @OneToOne(() => ComunasEntity, comuna => comuna.municipalidad)
    @JoinColumn({ name: 'idComuna' })
    readonly comuna: ComunasEntity;

    // @ApiProperty()
    // @OneToMany(() => PapelSeguridadEntity, papelSeguridad => papelSeguridad.instituciones)
    // readonly papelSeguridad: PapelSeguridadEntity[];
}
