import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("RegistroOficina")
export class RegistroOficinaEntity
{

    @PrimaryGeneratedColumn()
    idRegistroOficina:number;

    @Column()
    idOficina?:number;
    // @OneToOne(()=>OficinasEntity,oficinas=> oficinas.registros)
    // @JoinColumn({name:'idOficina'})
    // readonly oficinas:OficinasEntity;

    @Column()
    aprobador?:number;

    @Column()
    observacion:string;

    @Column()
    activo:boolean;

    @Column()
    fecha:Date;

    @Column()
    tipoCambio:string;



}