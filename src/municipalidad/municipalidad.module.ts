import { Module } from '@nestjs/common';
import { MunicipalidadController } from './controller/municipalidad.controller';
import { MunicipalidadService } from './service/municipalidad.service';
import { InstitucionesService } from '../instituciones/services/instituciones.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { MunicipalidadEntity } from './entity/municipalidad.entity';
import { User } from './../users/entity/user.entity';
import { InstitucionesEntity } from './../tramites/entity/instituciones.entity';
import { DocsResolucionesEntity } from './entity/docsResoluciones.entity';
import { RolesUsuarios } from './../roles-usuarios/entity/roles-usuarios.entity';
import { UsersModule } from 'src/users/users.module';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { AuthService } from 'src/auth/auth.service';
import { jwtConstants } from 'src/users/constants';
import { UsuarioOficinaEntity } from '../tramites/entity/usuarioOficina.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';

@Module({
  controllers: [MunicipalidadController],
  providers: [MunicipalidadService, InstitucionesService, AuthService],
  exports: [MunicipalidadService],
  imports: 
  [
    TypeOrmModule.forFeature(
    [
      InstitucionesEntity,
      OficinasEntity,
      MunicipalidadEntity,
      User,
      DocsResolucionesEntity,
      RolesUsuarios,
      ComunasEntity,
      UsuarioOficinaEntity,
      Solicitudes
    ]),
    UsersModule,
  ],
})
export class MunicipalidadModule {}
