import { Test, TestingModule } from '@nestjs/testing';
import { MunicipalidadService } from './municipalidad.service';

describe('MunicipalidadService', () => {
  let service: MunicipalidadService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MunicipalidadService],
    }).compile();

    service = module.get<MunicipalidadService>(MunicipalidadService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
