import { orderBy } from 'lodash';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import {
  AdministradorSGLC,
  ColumnasMunicipalidades,
  ColumnasOficinas,
  directorTransito,
  PermisosNombres,
  TipoInstitucionesIds,
  TipoInstitucion_Municipalidad,
  tipoRolOficinaID,
  tipoRolRegion,
  tipoRolRegionID,
  tipoRolSuperUsuarioID,
} from 'src/constantes';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { Resultado } from 'src/utils/resultado';
import { getConnection, In, IsNull, Not, QueryRunner, Repository } from 'typeorm';
import { CrearMunicipalidadesDto } from '../DTO/crearMunicipalidad.dto';
import { MunicipalidadDto } from '../DTO/municipalidad.dto';
import { MunicipalidadEntity } from '../entity/municipalidad.entity';
import { OficinaDto } from '../DTO/oficina.dto';
import { User } from '../../users/entity/user.entity';
import { DocsResolucionesEntity } from '../entity/docsResoluciones.entity';
import { CrearOficinaMunicipalidadDto } from '../DTO/crearOficinaMunicipalidad.dto';
import { CrearDocumentoResolucionDTO } from '../DTO/crearDocumentoResolucion.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { UsuariosOficinaDto } from '../DTO/usuariosOficina.dto';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { ListaResolucionesDTO } from '../DTO/listaResoluciones.dto';
import { RegistroOficinaEntity } from '../entity/registroOficina.entity';
import { UsersService } from 'src/users/services/users.service';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { permisosRol } from '../../shared/ValidarPermisosResultadoRol';
import { UsuarioOficinaEntity } from '../../tramites/entity/usuarioOficina.entity';
import { InstitucionesEntity } from '../../tramites/entity/instituciones.entity';
import { ObjetoListado } from 'src/utils/objetoListado';
import { validarRut } from 'src/shared/ValidarRun';
import { abort } from 'process';
import { ComunasListaDto } from '../DTO/comunasLista.dto';
import { ListaMunicipalidadesUsuarioDto } from '../DTO/listaMunicipalidadesUsuario.dto';
import { RegionesEntity } from '../../escuela-conductores/entity/regiones.entity';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class MunicipalidadService {
  constructor(
    @InjectRepository(MunicipalidadEntity) private readonly municipalidadRepository: Repository<MunicipalidadEntity>,
    @InjectRepository(OficinasEntity) private readonly oficinasRepository: Repository<OficinasEntity>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(DocsResolucionesEntity) private readonly docResolucionesRepository: Repository<DocsResolucionesEntity>,
    @InjectRepository(RolesUsuarios) private readonly rolesUsuarioRepository: Repository<RolesUsuarios>,
    @InjectRepository(ComunasEntity) private readonly comunasRepository: Repository<ComunasEntity>,
    @InjectRepository(UsuarioOficinaEntity) private readonly usuarioOficinaRepository: Repository<UsuarioOficinaEntity>,
    private readonly userService: UsersService,
    private readonly authService: AuthService
  ) {}

  //#region Muncipalidades

  async getInstitucionByIdUser(idUsuario: number) {
    const resultadoExaminacion = await this.rolesUsuarioRepository
      .createQueryBuilder('RolesUsuarios')
      .innerJoinAndMapMany('RolesUsuarios', OficinasEntity, 'ofi', 'RolesUsuarios.idOficina = ofi.idOficina')
      .innerJoinAndMapMany('ofi.idInstitucion', InstitucionesEntity, 'inst', 'ofi.idInstitucion = inst.idInstitucion');
    resultadoExaminacion.where('RolesUsuarios.idUsuario = :idUsuario', {
      idUsuario: idUsuario,
    });
    return resultadoExaminacion.getRawMany();
  }

  async getMunicipalidades(req:any, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    //Inicializamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    const orderBy = ColumnasMunicipalidades[paginacionArgs.orden.orden];

    //Creamos lo objetos necesarios para la paginacion
    let municipalidadesPag: Pagination<MunicipalidadEntity>;
    let municipalidadesParseadas = new PaginacionDtoParser<MunicipalidadDto>();

    let filtro = paginacionArgs.filtro;

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaCreacionAdministracionMunicipalidades);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      //Si eres usuario seremitt solo en las regiones correspondientes
      const superUsuario = usuarioValidado.Respuesta.rolesUsuarios.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      if (superUsuario) {
        //Buscamos en la BD usando el la paginacion para todos al ser superusuario
        municipalidadesPag = await paginate<MunicipalidadEntity>(this.municipalidadRepository, paginacionArgs.paginationOptions, {
          relations: ['oficinas'],
          where: qb => {
            qb.where('"MunicipalidadEntity"."idTipoInstitucion" = :tipoInstitucion', { tipoInstitucion: TipoInstitucion_Municipalidad });
            if (filtro.nombre != null && filtro.nombre != '') {
              qb.andWhere('lower(unaccent(MunicipalidadEntity.Nombre)) like lower(unaccent(:nombre))', { nombre: `%${filtro.nombre}%` });
            }
            if (filtro.descripcion != null && filtro.descripcion != '') {
              qb.andWhere('lower(unaccent(MunicipalidadEntity.Descripcion)) like lower(unaccent(:descripcion))', {
                descripcion: `%${filtro.descripcion}%`,
              });
            }
          },
          order: { [orderBy]: paginacionArgs.orden.ordenarPor },
        });
      } else {
        //Si eres usuario seremitt solo en las regiones correspondientes
        let regiones: number[] = [];

        const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];

        let listaRegionesRolRegion :number[] = [];
        listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion);

        let listaRegionesRolOficina :number[] = [];
        listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion);

        regiones = regiones.concat(listaRegionesRolRegion);
        regiones = regiones.concat(listaRegionesRolOficina);

        //Buscamos en la BD usando el la paginacion para todos al ser superusuario
        municipalidadesPag = await paginate<MunicipalidadEntity>(this.municipalidadRepository, paginacionArgs.paginationOptions, {
          relations: ['oficinas', 'oficinas.comunas'],
          where: qb => {
            qb.where('"MunicipalidadEntity"."idTipoInstitucion" = :tipoInstitucion', {
              tipoInstitucion: TipoInstitucion_Municipalidad,
            }).andWhere('MunicipalidadEntity__oficinas__comunas.idRegion IN(:...regiones)', { regiones: regiones });
            if (filtro.nombre != null && filtro.nombre != '') {
              qb.andWhere('lower(unaccent(MunicipalidadEntity.Nombre)) like lower(unaccent(:nombre))', { nombre: `%${filtro.nombre}%` });
            }
            if (filtro.descripcion != null && filtro.descripcion != '') {
              qb.andWhere('lower(unaccent(MunicipalidadEntity.Descripcion)) like lower(unaccent(:descripcion))', {
                descripcion: `%${filtro.descripcion}%`,
              });
            }
          },
          order: { [orderBy]: paginacionArgs.orden.ordenarPor },
        });
      }

      //Comprobamos que los objetos no sean vacios
      if (Array.isArray(municipalidadesPag.items) && municipalidadesPag.items.length) {
        // //Obtemos los datos BD
        // const municipalidadBD = await this.municipalidadRepository.find({idInstitucion:TipoInstitucion_Municipalidad});

        //Usamos el método plainToClass para mapear DTO con entidad de dominio
        const { items } = municipalidadesPag;
        municipalidadesParseadas.items = plainToClass(MunicipalidadDto, items, { groups: ['listado'] });

        //Asignamos los distintos valores de paginacion
        municipalidadesParseadas.links = municipalidadesPag.links;
        municipalidadesParseadas.meta = municipalidadesPag.meta;

        //Manejamos la respuesta
        resultado.Respuesta = municipalidadesParseadas;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Municipalidades obtenidas correctamente';
      } else {
        resultado.Error = 'No se encontraron municipalidades';
      }
    } catch (error) {
      //capturamos error
      console.log(error);
      resultado.Error = 'Error obteniendo las municipaliades';
    }

    return resultado;
  }

  async addMunicipalidad(req:any, crearMunicipalidadesDto: CrearMunicipalidadesDto): Promise<Resultado> {
    //Instanciamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    //Asignamos por defecto el tipo Institucion Municipalidad
    crearMunicipalidadesDto.idTipoInstitucion = TipoInstitucion_Municipalidad;
    //Campo a ver mas adelante
    crearMunicipalidadesDto.activo = true;

    try {

       // Comprobamos permisos	
       let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesCrearNuevaMunicipalidad);	
    	
       // En caso de que el usuario no sea correcto	
       if (!usuarioValidado.ResultadoOperacion) {	
         return usuarioValidado;	
       }	       
     
       const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];	

      //Si eres usuario seremitt solo en las regiones correspondientes
      const superUsuario = rolesUsuarioRecuperados.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //Si no es superusuario comprobamos la region corresponda a la comuna
      if (!superUsuario) {
        //Si eres usuario seremitt solo en las regiones correspondientes
        //Obtenmos las regiones
        let regiones : number[] = [];

        let listaRegionesRolRegion :number[] = [];
        listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion);

        let listaRegionesRolOficina :number[] = [];
        listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion);

        regiones = regiones.concat(listaRegionesRolRegion);
        regiones = regiones.concat(listaRegionesRolOficina);

        //validamos la comuna y región
        var comuna = await this.comunasRepository.find({ where: { idComuna: crearMunicipalidadesDto.idComuna, idRegion: In(regiones) } });

        if (comuna.length <= 0) {
          resultado.Error = 'No tienes permisos para la región correspondiente';
          return resultado;
        }
      }

      // Se comprueba si ya existe una municipalidad asociada a la comuna
      let municipalidadesConIDComunaExistente : MunicipalidadEntity[] = await this.municipalidadRepository.find({where: {idComuna : crearMunicipalidadesDto.idComuna, idTipoInstitucion:TipoInstitucionesIds.Municipalidad}});

      if(municipalidadesConIDComunaExistente.length > 0){
        resultado.Error = 'Ya existe una municipalidad asociada a la comuna seleccionada.';
        resultado.ResultadoOperacion = false;
        return resultado;        
      }

      //Guardamos el objeto en la BD
      var institucion = await this.municipalidadRepository.save(crearMunicipalidadesDto);

      //asignamos la respuesta
      resultado.ResultadoOperacion = true;
      resultado.Respuesta = institucion;
    } catch (error) {
      //Capturamos el error
      console.log(error);
      resultado.Mensaje = 'Error al crear institucion';
    }

    return resultado;
  }

  async deleteMunicipalidad(id: number, permisos: RolesUsuarios[]) {
    //Instanciamos la respuesta
    const resultado: Resultado = new Resultado();
    //asignamos por defecto el resultado false
    resultado.ResultadoOperacion = false;

    try {
      //validamos que antes de elminar la municipalidad no exista ninguna oficina asignada.
      var municipalidadBD = await this.municipalidadRepository.findOne({
        where: { idInstitucion: id, idTipoInstitucion: TipoInstitucion_Municipalidad },
        relations: ['oficinas'],
      });

      if (municipalidadBD.oficinas.length > 0) {
        resultado.Mensaje = 'No se puede eliminar una Municipalidad con Oficinas asociadas';
        return resultado;
      }

      //validamos que tengas permisos en la region

      const superUsuario = permisos.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //Si no es superusuario comprobamos la region corresponda a la comuna
      if (!superUsuario) {
        //Si eres usuario seremitt solo en las regiones correspondientes
        const regiones: number[] = permisos.filter(x => x.roles.tipoRoles.Nombre == tipoRolRegion).map(x => x.idRegion);

        //validamos la comuna y región
        var comuna = await this.comunasRepository.find({ where: { idComuna: municipalidadBD.idComuna, idRegion: In(regiones) } });
        if (comuna.length <= 0) {
          resultado.Error = 'No tienes permisos para la region correspondiente';
          return resultado;
        }
      }

      //eliminamos el objeto
      var municipalidadDelete = await this.municipalidadRepository.delete(id);

      //si todo es correcto devolvemos en true el resultado
      if (municipalidadDelete.affected > 0) {
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Municipalidad eliminada correctamente';
      } else {
        resultado.Mensaje = 'Error al eliminar la Municipalidad';
      }
    } catch (error) {
      //capturamos el error
      console.log(error);
      resultado.Error = 'Error al eliminar las municipalidades';
    }

    //devolvemos la respuesta
    return resultado;
  }

  async getMunicipalidad(req:any, id: number): Promise<Resultado> {
    //Instanciamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {

       // Comprobamos permisos	
       let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesCrearNuevaMunicipalidad);	
    	
       // En caso de que el usuario no sea correcto	
       if (!usuarioValidado.ResultadoOperacion) {	
         return usuarioValidado;	
       }	       
     
       const permisos = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];

      //Objetos en BD
      var municipalidadBD = await this.municipalidadRepository.findOne({
        where: { idInstitucion: id, idTipoInstitucion: TipoInstitucion_Municipalidad },
        relations: ['oficinas', 'oficinas.comunas', 'comuna', 'comuna.regiones'],
      });

      if (!municipalidadBD) {
        resultado.Error = 'La municipalidad no existe o no es del Tipo Municipalidad';
        return resultado;
      }

      //validamos que tengas permisos en la region

      const superUsuario = permisos.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //Si no es superusuario comprobamos la region corresponda a la comuna
      if (!superUsuario) {
        //Si eres usuario seremitt solo en las regiones correspondientes
        let regionesPermisos: number[] = permisos.filter(x => x.roles.tipoRol == tipoRolRegionID).map(x => x.idRegion);

        if(permisos.filter(x => x.roles.tipoRol == tipoRolOficinaID).length > 0)
          regionesPermisos = regionesPermisos.concat(permisos.filter(x => x.roles.tipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion));

        //validamos la comuna y región
        var comuna: boolean = regionesPermisos.includes(municipalidadBD.comuna.idRegion);

        if (!comuna) {
          resultado.Error = 'No tienes permisos para la region correspondiente';
          return resultado;
        }
      }

      //Mapeamos el objeto de Dominio al DTO
      const municipalidadDto = plainToClass(MunicipalidadDto, municipalidadBD, { groups: ['listado'] });

      //Compramos si existe municipalidades para asignar
      if (municipalidadBD.oficinas.length > 0) {
        // //mapeamos a la clase DTO y las asignamos
        // const oficinasLista = plainToClass(ListaOficinasMunicipalidadDTO, municipalidadBD.oficinas);
        //Si existen las añadimos a la lista de string
        municipalidadDto.listaNombresOficinas = [];
        municipalidadBD.oficinas.forEach(element => {
          municipalidadDto.listaNombresOficinas.push(element.Nombre);
        });

        //municipalidadDto.listaOficinas = oficinasLista;
      }

      municipalidadDto.NombreComuna = municipalidadBD.comuna.Nombre;

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = municipalidadDto;
    } catch (error) {
      //capturamos el error
      console.log(error);
      resultado.Error = 'Error al obtener la Municipalidad';
    }

    return resultado;
  }

  async updateMunicipalidad(req, id: number, data: Partial<CrearMunicipalidadesDto>): Promise<Resultado> {
    //Instanciamos la respuesta en false
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
       // Comprobamos permisos	
       let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesMunicipalidadesEditarMunicipalidad);	
    	
       // En caso de que el usuario no sea correcto	
       if (!usuarioValidado.ResultadoOperacion) {	
         return usuarioValidado;	
       }	       
     
       const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];

      //Comprobamos que el objeto existe en la BD
      var municipalidadUpdateBD = await this.municipalidadRepository.findOne(id, { relations: ['oficinas'] });

      if (municipalidadUpdateBD.idTipoInstitucion == null || municipalidadUpdateBD.idTipoInstitucion !== TipoInstitucion_Municipalidad) {
        resultado.Error = 'La institución que no es del Tipo Municipalidad';
        return resultado;
      }

      //validamos que tengas permisos en la region

      const superUsuario = rolesUsuarioRecuperados.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //Si no es superusuario comprobamos la region corresponda a la comuna
      if (!superUsuario) {
        //Si eres usuario seremitt solo en las regiones correspondientes
        //Obtenmos las regiones
        let regiones : number[] = [];

        let listaRegionesRolRegion :number[] = [];
        listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion);

        let listaRegionesRolOficina :number[] = [];
        listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion);

        regiones = regiones.concat(listaRegionesRolRegion);
        regiones = regiones.concat(listaRegionesRolOficina);

        //validamos la comuna y región
        var comuna = await this.comunasRepository.find({ where: { idComuna: municipalidadUpdateBD.idComuna, idRegion: In(regiones) } });

        if (comuna.length <= 0) {
          resultado.Error = 'No tienes permisos para la region correspondiente';
          return resultado;
        }
      }

      //Forzamos el tipo de institucion correcta
      data.idTipoInstitucion = municipalidadUpdateBD.idTipoInstitucion;

      // Se comprueba si ya existe una municipalidad asociada a la comuna
      let municipalidadesConIDComunaExistente : MunicipalidadEntity[] = await this.municipalidadRepository.find({where: {idComuna : data.idComuna, idInstitucion: Not(id), idTipoInstitucion:TipoInstitucionesIds.Municipalidad}});

      if(municipalidadesConIDComunaExistente.length > 0){
        resultado.Error = 'Ya existe otra municipalidad asociada a la comuna seleccionada.';
        resultado.ResultadoOperacion = false;
        return resultado;        
      }

      //Actualizamos la municipalidad
      var municipalidadActualizada = await this.municipalidadRepository.update(id, data);

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = municipalidadActualizada;
    } catch (error) {
      //capturamos el error
      console.log(error);
      resultado.Mensaje = 'Error actualizando Municipalidad';
    }
    return resultado;
  }

  //#endregion

  //#region Oficinas

  async updateOficina(req:any, id: number, updateOficinaDto: Partial<CrearOficinaMunicipalidadDto>): Promise<Resultado> {
    //Inicializamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {

       // Comprobamos permisos	
       let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesOficinasEditarOficina);	
    	
       // En caso de que el usuario no sea correcto	
       if (!usuarioValidado.ResultadoOperacion) {	
         return usuarioValidado;	
       }	 

      const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];       

      //Obtenemos la oficina de la BD comprobamos que exite y  que sea de tipo institucion
      const oficinaBD = await this.oficinasRepository.findOne(id, { relations: ['comunas', 'instituciones', 'resoluciones'] });

      if (
        oficinaBD == undefined ||
        oficinaBD.instituciones.idTipoInstitucion != TipoInstitucion_Municipalidad ||
        oficinaBD.idInstitucion != updateOficinaDto.idInstitucion
      ) {
        resultado.Error = 'No se puede actualizar la Oficina porque no es del tipo municipalidad';
        return resultado;
      }

      //le asignamos el id de comuna correcto
      updateOficinaDto.idComuna = oficinaBD.idComuna;

      //de momento siempre es true otros usuarios?
      var userSeremit: boolean = true;

      //obtenemos la lista de roles de los permisos del controllador
      //let roles = await permisosRol(permisos);

      //Comprobamos si es superAdministrador
      var superAdministrador = rolesUsuarioRecuperados.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //Si no lo es Comprobamos entonces las regiones de Seremitt que tiene permiso, para la oficina
      if (!superAdministrador) {
        //Obtenmos las regiones
        let seremittRegion : number[] = [];

        let listaRegionesRolRegion :number[] = [];
        listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion);

        let listaRegionesRolOficina :number[] = [];
        listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion);

        seremittRegion = seremittRegion.concat(listaRegionesRolRegion);
        seremittRegion = seremittRegion.concat(listaRegionesRolOficina);

        var regionOk = seremittRegion.includes(oficinaBD.comunas.idRegion);

        if (!regionOk) {
          resultado.Error = 'No tienes permisos para ver la Oficina';
          return resultado;
        }
      }

      //comprobamos el rut y lo preparamos para la inserción de BD
      if (!validarRut(updateOficinaDto.RUTCompleto)) {
        resultado.Error = 'El formato del rut es incorrecto';
        return resultado;
      }

      var dividirRut = updateOficinaDto.RUTCompleto.split('-');

      updateOficinaDto.DV = dividirRut[1];
      updateOficinaDto.RUT = +dividirRut[0];

      //creamos el objeto que vamos a actualizar
      let oficinaUpdate: Partial<OficinasEntity> = new OficinasEntity();
      for (let [keyOficcina, valueOficina] of Object.entries(oficinaBD)) {
        for (const [keyUpdate, valueUpdate] of Object.entries(updateOficinaDto)) {
          if (keyOficcina == keyUpdate) {
            oficinaUpdate[keyOficcina] = valueUpdate;
          }
        }
      }

      //Guardamos la oficins
      var modificacion = await queryRunner.manager.update(OficinasEntity, id, oficinaUpdate);

      //Comprobamos que se ha realizado la operacion
      if (modificacion.affected < 0) {
        resultado.Error = 'Error al editar Oficina';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      //Si lleva resoluciones nuevas o modificadas las guardamos
      if (updateOficinaDto.ListaResolucionesCrear.length > 0) {
        let resoluciones = await this.actualizarResoluciones(oficinaBD, updateOficinaDto, queryRunner);

        if (!resoluciones) {
          resultado.Error = 'Error al guardar o actualizar los documentos de la Oficina';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }
      }

      //si se produce un cambio en el estado
      if (oficinaBD.activo != updateOficinaDto.activo) {
        let oficinaUpdateRegistro = [];
        oficinaUpdateRegistro.push(updateOficinaDto);

        // //guardamos en el registro los cambios
        var registro = await this.registrarCambiosOficinas(queryRunner, oficinaUpdateRegistro, userSeremit, id);

        if (!registro) {
          resultado.Error = 'Error al actualizar el registro';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }
      }

      await queryRunner.commitTransaction();
      resultado.Mensaje = 'Oficina editada correctamente';
      resultado.ResultadoOperacion = true;
    } catch (error) {
      //capturamos el error
      await queryRunner.rollbackTransaction();
      console.log(error);
      resultado.Error = 'Error al editar la Oficina';
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async getOficina(req:any, id: number): Promise<Resultado> {
    //Inicializamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      //Consultamos a la BD

      const oficinaBD = await this.oficinasRepository.findOne(id, { relations: ['comunas', 'comunas.regiones', 'resoluciones'] });

      //Obtenemos los roles desde la lista de permisos del controller
      //let roles = await permisosRol(permisos);
       // Comprobamos permisos	
       let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesOficinasVerDetalleOficina);	
    	
       // En caso de que el usuario no sea correcto	
       if (!usuarioValidado.ResultadoOperacion) {	
         return usuarioValidado;	
       }	
       
       const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];	

      //Comprobamos que sea Superadmininstrado
      var superAdministrador = rolesUsuarioRecuperados.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //Si no lo es comprobamos si tiene permisos para las regiones correspondientes
      if (!superAdministrador) {
      //Obtenmos las regiones
      let seremittRegion : number[] = [];

      let listaRegionesRolRegion :number[] = [];
      listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion);

      let listaRegionesRolOficina :number[] = [];
      listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion);

      seremittRegion = seremittRegion.concat(listaRegionesRolRegion);
      seremittRegion = seremittRegion.concat(listaRegionesRolOficina);

        var regionOk = seremittRegion.includes(oficinaBD.comunas.idRegion);

        if (!regionOk) {
          resultado.Error = 'No tienes permisos para ver la Oficina';
          return resultado;
        }
      }

      //Obtenemos la lista de usuarios
      var usuarios =
        (await this.userRepository
          .createQueryBuilder('user')
          .leftJoinAndSelect('user.rolesUsuarios', 'rolesUsuarios')
          .leftJoinAndSelect('rolesUsuarios.roles', 'roles')
          .where('rolesUsuarios.idOficina = :id', { id: id })
          .getMany()) ?? null;

      const usuariosDirectores = await this.rolesUsuarioRepository.find({
        relations: ['user'],
        where: { idRol: directorTransito, idOficina: id },
      });

      var director = usuariosDirectores.find(d => (d.idRol = directorTransito))?.idUsuario ?? null;

      //Se tiene que crear el permiso para acceder a este rol
      var directorSubrogado = usuariosDirectores.find(d => (d.idRol = directorSubrogado))?.idUsuario ?? null;

      //Comprabamos que exitan datos
      if (oficinaBD == undefined) {
        resultado.Error = 'Oficina no encontrada';
        return resultado;
      }

      //mapeamos al objeto dto
      const oficinaDTO = plainToClass(OficinaDto, oficinaBD);

      // //Mapeamos los nombres region y comuna
      oficinaDTO.idRegion = oficinaBD.comunas.regiones.idRegion;

      //Mapemamos el RUT
      oficinaDTO.RUT = oficinaBD.RUT + '-' + oficinaBD.DV;

      //Generamos la lista de usuarios
      await this.getListaUsuarios(usuarios, director, directorSubrogado, oficinaDTO);

      //Añadimos la lista de Resoluciones
      oficinaDTO.ListaResoluciones = plainToClass(ListaResolucionesDTO, oficinaBD.resoluciones);
      oficinaDTO.ListaResoluciones.forEach(element => {
        element.archivo = element.binario.toString();
        delete element.binario;
      });

      resultado.Respuesta = oficinaDTO;
      resultado.ResultadoOperacion = true;
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error al obtener la Oficina';
    }

    return resultado;
  }

  /**
   * Método para obtener la lista de usuarios de la Oficina así como sus roles
   * @param usuarios Lista usuarios
   * @param director id del director
   * @param directorSubrogado id del director subrogado
   * @param oficinaDTO oficina que vamos obtener
   */
  async getListaUsuarios(usuarios: User[], director: number, directorSubrogado: number, oficinaDTO: OficinaDto) {
    //Vamos a recorrer la lista de usuarios para añadirla
    let usuarioOficinaDTO: UsuariosOficinaDto[] = [];
    try {
      if (usuarios.length > 0) {
        //recorremos la lista y vamos añadiendo los atributos
        usuarios.forEach(element => {
          var usuarioOficinaAdd = new UsuariosOficinaDto();
          usuarioOficinaAdd.rol = [];
          usuarioOficinaAdd.RUN = element.RUN;
          usuarioOficinaAdd.Nombres = element.Nombres + ' ' + element.ApellidoPaterno + ' ' + element.ApellidoMaterno;

          //creamos un array con los distintos roles del usuario
          element.rolesUsuarios.forEach(rolItem => {
            usuarioOficinaAdd.rol.push(rolItem.roles.Nombre);
          });

          //mapeamos a la clase Dto y añadimos a la lista
          usuarioOficinaDTO.push(plainToClass(UsuariosOficinaDto, usuarioOficinaAdd));

          //comprobar y añadir al objeto de oficina los usuarios director y director subrogante
          if (element.idUsuario == director && director) {
            oficinaDTO.directorTransitoNombre = element.Nombres + ' ' + element.ApellidoPaterno + ' ' + element.ApellidoMaterno;
          }
          if (element.idUsuario == directorSubrogado && directorSubrogado) {
            oficinaDTO.directorSubroganteNombre = element.Nombres + ' ' + element.ApellidoPaterno + ' ' + element.ApellidoMaterno;
          }
        });
        //Añadimos al dto la lista de usuarios
        oficinaDTO.ListaUsuariosOficina = usuarioOficinaDTO;
      }
    } catch (error) {
      console.log(error);
    }
  }

  async getOficinasListaNombres() {
    const resultado: Resultado = new Resultado();
    let resultadoOficinas: OficinasEntity[] = [];
    let resultadosDTO: ObjetoListado[] = [];
    let esSuperusuario = false;

    try {
      // Si es superUsuario obtendra todas las oficinas
      // permisos.forEach(rolUsuario => {
      //   if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID) esSuperusuario = true;
      // });

      // //Si es usuario administrador SGLC obtenemos las regiones en la que lo sea
      // const regiones: number[] = permisos.filter(x => x.idRol == AdministradorSGLC).map(x => x.idRegion);

      // //Si es usuario Director de transito obtenemos las oficinas en las que lo sea
      // const oficinas: number[] = permisos.filter(x => x.idRol == directorTransito).map(x => x.idOficina);

      //if (regiones.length > 0 || oficinas.length > 0 || esSuperusuario) {
        resultadoOficinas = await this.oficinasRepository.find({
           relations: ['comunas', 'comunas.regiones', 'instituciones'],
          //  where: qb => {
          //    qb.where(regiones.length > 0 && !esSuperusuario ? 'OficinasEntity__comunas__regiones.idRegion IN(:...idsRegiones)' : 'TRUE', {
          //      idsRegiones: regiones,
          //    }).orWhere(oficinas.length > 0 && !esSuperusuario ? 'OficinasEntity.idOficina IN(:...idsOficinas)' : 'TRUE', {
          //      idsOficinas: oficinas,
          //    });
          //  },
        });

        if (Array.isArray(resultadoOficinas) && resultadoOficinas.length) {
          resultadoOficinas.forEach(item => {
            let nuevoElemento: ObjetoListado = {
              id: item.idOficina,
              Nombre: `${item.Nombre} - ${item.instituciones.Nombre}`,
            };

            resultadosDTO.push(nuevoElemento);
          });
        }
      //}

      if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
        resultado.Respuesta = resultadosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Oficinas obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Oficinas';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Oficinas';
    }

    return resultado;
  }

  async getOficinas(req, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    //Inicializamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    const orden = ColumnasOficinas[paginacionArgs.orden.orden];
    let oficinasParseadas = new PaginacionDtoParser<OficinaDto>();
    let oficinasPaginados: Pagination<OficinasEntity>;

    let filtro = paginacionArgs.filtro;

    try {
      //Comprobamos lo roles que nos llegas del controlador
      //let roles = await permisosRol(permisos);
      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesOficinasBuscarOficina);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	

      //Comprbamos si es superUsuario
      var superAdministrador = usuarioValidado.Respuesta.rolesUsuarios.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      var listaRegiones: string[] = [];

      //Ejecutamos una u otra consulta según  tipo de usuario

      if (!superAdministrador) {
        const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];

        let listaRegionesRolRegion :string[] = [];
        listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion.toString());

        let listaRegionesRolOficina :string[] = [];
        listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion.toString());

        listaRegiones = listaRegiones.concat(listaRegionesRolRegion);
        listaRegiones = listaRegiones.concat(listaRegionesRolOficina);

        oficinasPaginados = await paginate<OficinasEntity>(this.oficinasRepository, paginacionArgs.paginationOptions, {
          relations: ['comunas', 'comunas.regiones', 'instituciones'],
          where: qb => {
            qb.where('"OficinasEntity__instituciones"."idTipoInstitucion" = :tipoInstitucion', {
              tipoInstitucion: TipoInstitucion_Municipalidad,
            }).andWhere('OficinasEntity__comunas.idRegion IN(:...regiones)', { regiones: listaRegiones });

            if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.Nombre', 'ASC');
            if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.Nombre', 'DESC');
            if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.Descripcion', 'ASC');
            if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.Descripcion', 'DESC');
            if (paginacionArgs.orden.orden === 'direccion' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.Direccion', 'ASC');
            if (paginacionArgs.orden.orden === 'direccion' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.Direccion', 'DESC');
            if (paginacionArgs.orden.orden === 'comuna' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.idComuna', 'ASC');
            if (paginacionArgs.orden.orden === 'comuna' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.idComuna', 'DESC');
            if (paginacionArgs.orden.orden === 'region' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity__comunas__regiones.idRegion', 'ASC');
            if (paginacionArgs.orden.orden === 'region' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity__comunas__regiones.idRegion', 'DESC');
            if (paginacionArgs.orden.orden === 'fecha' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.fechaCreacion', 'ASC');
            if (paginacionArgs.orden.orden === 'fecha' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.fechaCreacion', 'DESC');
          },
        });
      } else {
        oficinasPaginados = await paginate<OficinasEntity>(this.oficinasRepository, paginacionArgs.paginationOptions, {
          relations: ['comunas', 'comunas.regiones', 'instituciones'],
          where: qb => {
            qb.where('"OficinasEntity__instituciones"."idTipoInstitucion" = :tipoInstitucion', {
              tipoInstitucion: TipoInstitucion_Municipalidad,
            });
            if (filtro.nombre != null && filtro.nombre != '') {
              qb.andWhere('lower(unaccent(OficinasEntity.Nombre)) like lower(unaccent(:nombre))', { nombre: `%${filtro.nombre}%` });
            }
            if (filtro.descripcion != null && filtro.descripcion != '') {
              qb.andWhere('lower(unaccent(OficinasEntity.Descripcion)) like lower(unaccent(:descripcion))', {
                descripcion: `%${filtro.descripcion}%`,
              });
            }
            if (filtro.calle != null && filtro.calle != '') {
              qb.andWhere('lower(unaccent(OficinasEntity.calleDireccion)) like lower(unaccent(:calleDireccion))', {
                calleDireccion: `%${filtro.calle}%`,
              });
            }
            if (filtro.fecha != null) {
              qb.andWhere('OficinasEntity.fechaCreacion = :fechaCreacion', { fechaCreacion: `%${filtro.fecha}%` });
            }
            if (filtro.numero != null) {
              qb.andWhere('OficinasEntity.nroDireccion = :nroDireccion', { nroDireccion: filtro.numero });
            }
            if (filtro.region != null) {
              qb.leftJoinAndMapOne('OficinasEntity.idComuna', InstitucionesEntity, 'comuna', 'comuna.idComuna = OficinasEntity.idComuna');
              qb.andWhere('OficinasEntity__comunas__regiones.idRegion = :idRegion', { idRegion: filtro.region });
            }
            if (filtro.comuna != null) {
              qb.andWhere('OficinasEntity.idComuna = :idComuna', { idComuna: filtro.comuna });
            }
            if (filtro.estado != '') {
              if (filtro.estado == 'null') {
                qb.andWhere('OficinasEntity.activo = :activo', { activo: null });
              } else {
                qb.andWhere('OficinasEntity.activo = :activo', { activo: filtro.estado });
              }
            }
            if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.Nombre', 'ASC');
            if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.Nombre', 'DESC');
            if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.Descripcion', 'ASC');
            if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.Descripcion', 'DESC');
            if (paginacionArgs.orden.orden === 'direccion' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.calleDireccion', 'ASC');
            if (paginacionArgs.orden.orden === 'direccion' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.calleDireccion', 'DESC');
            if (paginacionArgs.orden.orden === 'comuna' && paginacionArgs.orden.ordenarPor === 'ASC') qb.orderBy('OficinasEntity__comunas.Nombre', 'ASC');
            if (paginacionArgs.orden.orden === 'comuna' && paginacionArgs.orden.ordenarPor === 'DESC') qb.orderBy('OficinasEntity__comunas.Nombre', 'DESC');
            if (paginacionArgs.orden.orden === 'region' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity__comunas__regiones.Nombre', 'ASC');
            if (paginacionArgs.orden.orden === 'region' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity__comunas__regiones.Nombre', 'DESC');
            if (paginacionArgs.orden.orden === 'fecha' && paginacionArgs.orden.ordenarPor === 'ASC')
              qb.orderBy('OficinasEntity.fechaCreacion', 'ASC');
            if (paginacionArgs.orden.orden === 'fecha' && paginacionArgs.orden.ordenarPor === 'DESC')
              qb.orderBy('OficinasEntity.fechaCreacion', 'DESC');
          },
        });
      }

      //Comprobamos que los objetos no sean vacios
      if (Array.isArray(oficinasPaginados.items) && oficinasPaginados.items.length) {
        this.formatdireccionesString(oficinasPaginados.items);
        // //Lista de Oficinas para la respuesta
        let oficinasListaDTOs: OficinaDto[] = [];

        //Recorremos la lista
        oficinasPaginados.items.forEach(element => {
          //Plaintoclass sirve para mapear el la clase de dominio a dto
          var item = plainToClass(OficinaDto, element);

          item.NombreComuna = element.comunas.Nombre;
          item.NombreRegion = element.comunas.regiones.Nombre;
          oficinasListaDTOs.push(item);
        });

        //añadimos la lista para devolverla
        oficinasParseadas.items = oficinasListaDTOs;

        //Asignamos los distintos valores de paginacion
        oficinasParseadas.links = oficinasPaginados.links;
        oficinasParseadas.meta = oficinasPaginados.meta;

        //Manejamos la respuesta
        resultado.Respuesta = oficinasParseadas;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Municipalidades obtenidas correctamente';
      } else {
        resultado.Error = 'No se encontraron Oficinas';
      }
    } catch (error) {
      //Capturamos error
      console.log(error);
      resultado.Error = 'Error al obtener las Oficinas';
    }

    //retornamos respuesta
    return resultado;
  }

  private formatdireccionesString(oficinas: OficinasEntity[]) {
    oficinas.forEach(oficina => {
      oficina.Direccion = `${oficina.calleDireccion} ${oficina.nroDireccion}`;
      if (oficina.letraDireccion != null) {
        oficina.Direccion += ' ' + oficina?.letraDireccion;
      }
      if (oficina.restoDireccion != null) {
        oficina.Direccion += ', ' + oficina?.restoDireccion;
      }
    });
  }
  async addOficinas(req, crearOficinasMunicipalidadDto: CrearOficinaMunicipalidadDto[]): Promise<Resultado> {
    //Inciamos respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeMunicipalidadesOficinasCrearNuevaOficina);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	

      const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];

      const municipalidadTipo = await await this.municipalidadRepository.findOne(crearOficinasMunicipalidadDto[0].idInstitucion);

      // Validamos el rut
      if (!validarRut(crearOficinasMunicipalidadDto[0].RUTCompleto)) {
        resultado.Error = 'El formato del rut es incorrecto';
        return resultado;
      }

      //Comprobamos el tipo de la institucion sea el correcto
      if (municipalidadTipo.idInstitucion == null || municipalidadTipo.idTipoInstitucion != TipoInstitucion_Municipalidad) {
        resultado.Error = 'La Institución a la que quiere vincular la Oficina no es una Municipalidad';
        return resultado;
      }

      //Obtenemos los roles del objteo del controllador
      let roles = await permisosRol(rolesUsuarioRecuperados);

      //si el usuario es SuperAdministrador no tenemos que validar nada mas
      var superAdministrador = rolesUsuarioRecuperados.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //ahora mismo siempre es verdadero otros usuarios?
      let userSeremit: boolean = true;

      crearOficinasMunicipalidadDto[0].idComuna = municipalidadTipo.idComuna;

      //Si no lo es comprobamos las regiones de la cuales es Seremitt
      if (!superAdministrador) {

        //Obtenmos las regiones
        let seremittRegion : string[] = [];

        let listaRegionesRolRegion :string[] = [];
        listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion.toString());

        let listaRegionesRolOficina :string[] = [];
        listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion.toString());

        seremittRegion = seremittRegion.concat(listaRegionesRolRegion);
        seremittRegion = seremittRegion.concat(listaRegionesRolOficina);

        //Obtenemos las regiones de las Oficinas para introducir
        listaRegionesOficinas = await (
          await this.comunasRepository.find({ idComuna: In(crearOficinasMunicipalidadDto.map(x => x.idComuna)) })
        ).map(x => x.idRegion?.toString());
        listaRegionesOficinas = [...new Set(listaRegionesOficinas)];

        //Comprobamos cuales son las que no corresponden
        var regionesnoIncluidas = listaRegionesOficinas.filter(x => !seremittRegion.includes(x));

        //Si no tiene permisos
        if (regionesnoIncluidas.length > 0) {
          resultado.Error = 'No tienes permiso para crear Oficinas en la región';
          return resultado;
        }
      }

      //Iniciamos la lista de regiones Oficinas
      var listaRegionesOficinas: string[];

      //asignamos los atributos que no se obtienen directamente del objeto crear
      await this.asignarAtributosFaltantes(crearOficinasMunicipalidadDto, roles[0].idUsuario, userSeremit);

      //procedemos a guardar
      await queryRunner.manager.save(OficinasEntity, crearOficinasMunicipalidadDto);

      if (crearOficinasMunicipalidadDto[0].ListaResolucionesCrear) {
        //creamos la lista de los Documentos de las oficinas y le asignamos sus ids
        let listaResolucionesOficinas = await this.crearListaDeResoluciones(crearOficinasMunicipalidadDto);
        //guardamos los Documentos relacionados
        const documentos = await this.addResolucion(listaResolucionesOficinas, queryRunner);

        //en funcion de la respuesta procedemos
        if (!documentos.ResultadoOperacion) {
          resultado.Error = 'Error al guardar Oficina en los documentos asociados';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }
      }

      //guardamos en el registro los cambios
      var registro = await this.registrarCambiosOficinas(queryRunner, crearOficinasMunicipalidadDto, userSeremit);

      //validamos respuesta
      if (!registro) {
        resultado.Error = 'Error al guardar los registros de oficina Oficina';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      //Procedemos avisar al seremit para que active la oficina
      //this.enviarAvisoSeremit(listaRegionesOficinas);

      await queryRunner.commitTransaction();
      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Oficina Guardada correctamente';
    } catch (error) {
      //capturamos el error
      console.log(error);
      resultado.Error = 'Error al guardar las Oficinas';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  /**
   * Método para asignar los atributos faltantes directamente
   * @param crearOficinasMunicipalidadDto  objet  para crear
   * @param user id de usuario
   * @param userSeremitt si es o no seremit
   */
  async asignarAtributosFaltantes(crearOficinasMunicipalidadDto: CrearOficinaMunicipalidadDto[], user: number, userSeremitt: boolean) {
    //Asignamos lo atributos faltantes
    crearOficinasMunicipalidadDto.forEach(element => {
      // if (!userSeremitt) {
      //     element.activo = null;
      // }
      element.activo = null;
      var separaRUT = element.RUTCompleto.split('-');
      element.RUT = +separaRUT[0];
      element.DV = separaRUT[1];
      element.fechaCreacion = new Date();
      element.creadoPor = user;
    });
  }

  async deleteOficina(id: number, permisos: RolesUsuarios[]): Promise<Resultado> {
    //Instanciamos la respuesta
    const resultado: Resultado = new Resultado();
    //asignamos por defecto el resultado false
    resultado.ResultadoOperacion = false;

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      //Obtenemos la oficina de la BD comprobamos que exite y  que sea de tipo institucion
      const oficinaBD = await this.oficinasRepository.findOne(id, { relations: ['comunas', 'instituciones'] });

      if (oficinaBD == undefined || oficinaBD.instituciones.idTipoInstitucion != TipoInstitucion_Municipalidad) {
        resultado.Error = 'No se puede actualizar la Oficina porque no es del tipo municipalidad';
        return resultado;
      }

      //obtenemos la lista de roles de los permisos del controllador
      let roles = await permisosRol(permisos);

      //Comprobamos si es superAdministrador
      var superAdministrador = permisos.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      console.log(roles[0].idUsuario);

      //Si no lo es Comprobamos entonces las regiones de Seremitt que tiene permiso, para la oficina
      if (!superAdministrador) {
        let seremittRegion = permisos.filter(x => x.roles.tipoRoles.Nombre == tipoRolRegion).map(x => x.idRegion);

        var regionOk = seremittRegion.includes(oficinaBD.comunas.idRegion);

        if (!regionOk) {
          resultado.Error = 'No tienes permisos para ver la Oficina';
          return resultado;
        }
      }

      //generamos las operaciones de la transaccion
      await queryRunner.manager.delete(DocsResolucionesEntity, { idOficina: id });
      await queryRunner.manager.delete(OficinasEntity, { idOficina: id });

      const registroDelete: RegistroOficinaEntity = new RegistroOficinaEntity();
      registroDelete.activo = false;
      registroDelete.aprobador = roles[0].idUsuario;
      registroDelete.fecha = new Date();
      registroDelete.idOficina = id;
      registroDelete.observacion = '';
      registroDelete.tipoCambio = 'Oficina Eliminada';

      await queryRunner.manager.save(RegistroOficinaEntity, registroDelete);

      //lanzamos la transaccion
      await queryRunner.commitTransaction();
      //eliminamos la oficina
      resultado.Mensaje = 'Oficina Eliminada Correctamente';
      resultado.ResultadoOperacion = true;
    } catch (error) {
      //capturamos el error
      console.log(error);
      resultado.Error = 'Error al eliminar las Oficina';
      //En caso de error anulamos la transaccion
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }
    //devolvemos la respuesta
    return resultado;
  }

  //#endregion

  //#region Resoluciones

  async addResolucion(resolucionNueva: CrearDocumentoResolucionDTO[], queryRunner: QueryRunner): Promise<Resultado> {
    //Instanciamos la respuesta
    const resultado: Resultado = new Resultado();
    //asignamos por defecto el resultado false
    resultado.ResultadoOperacion = false;

    try {
      await resolucionNueva.forEach(async element => {
        if (element.archivo != undefined && element.archivo != '' && element.archivo.includes('pdf')) {
          element.binario = Buffer.from(element.archivo);
        } else {
          resultado.Error = 'Formato de archivo incorrecto';
          return resultado;
        }
      });

      await queryRunner.manager.save(DocsResolucionesEntity, resolucionNueva);

      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Resolución añadida correctamente';
    } catch (error) {
      //Capturamos el error

      console.log(error);
      resultado.Error = 'Error al añadir resolución';
    }

    return resultado;
  }

  async crearListaDeResoluciones(crearOficinasMunicipalidadDto: CrearOficinaMunicipalidadDto[]): Promise<CrearDocumentoResolucionDTO[]> {
    //creamos el elemento de resolucion de documentos a para añadir
    let listaResoluciones: CrearDocumentoResolucionDTO[] = [];
    try {
      //Recorremos el objeto para añadirle el idOficina
      crearOficinasMunicipalidadDto.forEach(element => {
        element.ListaResolucionesCrear?.forEach(elementListaResoluciones => {
          elementListaResoluciones.idOficina = element.idOficina;
          listaResoluciones.push(elementListaResoluciones);
        });
      });
    } catch (error) {
      console.log(error);
    }

    return listaResoluciones;
  }

  async actualizarResoluciones(
    oficinaBD: OficinasEntity,
    updateOficinaDto: Partial<CrearOficinaMunicipalidadDto>,
    queryRunner: QueryRunner
  ): Promise<Boolean> {
    //Iniciamos la respuesta
    let ok: boolean = true;

    try {
      let idOficina = oficinaBD.idOficina;

      if (oficinaBD.resoluciones.length > 0) {
        await queryRunner.manager.delete(DocsResolucionesEntity, oficinaBD.resoluciones);
      }
      if (updateOficinaDto.ListaResolucionesCrear.length > 0) {
        for (const elementUpdate of updateOficinaDto.ListaResolucionesCrear) {
          if (elementUpdate != null) {
            delete elementUpdate.idDocsResoluciones;
            elementUpdate.idOficina = idOficina;
            var validar = await this.validarResolucion(elementUpdate);
            if (validar) {
              await queryRunner.manager.save(DocsResolucionesEntity, elementUpdate);
            } else {
              return (ok = false);
            }
          }
        }
      }
    } catch (error) {
      console.log(error);
    }

    return await ok;
  }

  async validarResolucion(elementUpdate: CrearDocumentoResolucionDTO): Promise<boolean> {
    let resultado = false;
    if (elementUpdate.archivo != undefined && elementUpdate.archivo != '' && elementUpdate.archivo.includes('pdf')) {
      elementUpdate.binario = Buffer.from(elementUpdate.archivo);
      resultado = true;
    }
    return resultado;
  }

  //#endregion

  //#region RegistroOficinas

  async registrarCambiosOficinas(
    queryRunner: QueryRunner,
    crearOficinasMunicipalidadDto: CrearOficinaMunicipalidadDto[],
    userSeremit: boolean,
    idOficina?: number
  ): Promise<Boolean> {
    //Inicializamos la repuesta
    let respuesta = false;

    try {
      //Creamos la lista de registro para insertar
      let listaDeRegistros: RegistroOficinaEntity[] = [];

      //recorremos los objetos a crear de las oficinas y los usarmos para crear el registro
      crearOficinasMunicipalidadDto.forEach(elementOficina => {
        const registroOficina: RegistroOficinaEntity = new RegistroOficinaEntity();
        registroOficina.idOficina = elementOficina.idOficina ?? idOficina;
        registroOficina.fecha = new Date();
        registroOficina.observacion = elementOficina.observacion ?? '';

        //Dependiendo del tipo de usuario procedemos según los casos de uso
        if (elementOficina.activo == null && !userSeremit) {
          registroOficina.tipoCambio = 'Oficina creada, espera activación ';
          registroOficina.activo = false;
        } else if ((elementOficina.activo = true && userSeremit)) {
          registroOficina.activo = true;
          registroOficina.tipoCambio = 'Oficina creada y activada por el Seremitt correspondiente';
        } else if (elementOficina.activo == null && userSeremit) {
          registroOficina.activo = false;
          registroOficina.tipoCambio = 'Oficina creada sin activar por Seremitt correspondiente';
        } else if ((elementOficina.activo = false && userSeremit)) {
          registroOficina.activo = false;
          registroOficina.tipoCambio = 'Oficina rechazada por Seremitt correspondiente';
        }

        listaDeRegistros.push(registroOficina);
      });

      //Guardamos la lista
      await queryRunner.manager.save(RegistroOficinaEntity, listaDeRegistros);
      respuesta = true;
    } catch (error) {
      console.log(error);
    }
    return respuesta;
  }

  //#endregion

  async obtenerComunasUsuario(permisos: RolesUsuarios[]): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      let comunasLista: ComunasEntity[] = [];
      //const superUsuario = permisos.find(x => x.roles.tipoRoles.Nombre == tipoRolSuperUsuario);

      // //Si no es superusuario comprobamos la region corresponda a la comuna
      // if (!superUsuario) {
      //   //Si eres usuario seremitt solo en las regiones correspondientes
      //   const regiones: number[] = permisos.filter(x => x.roles.tipoRoles.Nombre == tipoRolRegion).map(x => x.idRegion);

      //   //validamos la comuna y región
      //   const comuna: ComunasEntity[] = await this.comunasRepository.find({
      //     relations: ['municipalidad'],
      //     where: { idRegion: In(regiones) },
      //   });
      //   if (comuna.length <= 0) {
      //     resultado.Error = 'No tienes comunas disponibles para tu región';
      //     return resultado;
      //   }

      //   comunasLista = comuna;
      // } else {
        const comuna: ComunasEntity[] = await this.comunasRepository.find({ relations: ['municipalidad'] });
        comunasLista = comuna;
      //}

      //si la comuna no tiene una municipalidad asociada la seleccionamos
      let comunasListaDto: ComunasListaDto[] = [];
      for (const element of comunasLista) {
        if (element.municipalidad == null) {
          const itemComuna: ComunasListaDto = new ComunasListaDto();
          itemComuna.idComuna = element.idComuna;
          itemComuna.nombre = element.Nombre;
          itemComuna.idRegion = element.idRegion;

          comunasListaDto.push(itemComuna);
        }
      }

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = comunasListaDto;
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error al obtener las comunas';
    }

    return resultado;
  }

  async obtenerComunasUsuarioCrearMunicipalidad(permisos: RolesUsuarios[]): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      let comunasLista: ComunasEntity[] = [];
      //const superUsuario = permisos.find(x => x.roles.tipoRoles.Nombre == tipoRolSuperUsuario);

      // //Si no es superusuario comprobamos la region corresponda a la comuna
      // if (!superUsuario) {
      //   //Si eres usuario seremitt solo en las regiones correspondientes
      //   const regiones: number[] = permisos.filter(x => x.roles.tipoRoles.Nombre == tipoRolRegion).map(x => x.idRegion);

      //   //validamos la comuna y región
      //   const comuna: ComunasEntity[] = await this.comunasRepository.find({
      //     relations: ['municipalidad'],
      //     where: { idRegion: In(regiones) },
      //   });
      //   if (comuna.length <= 0) {
      //     resultado.Error = 'No tienes comunas disponibles para tu región';
      //     return resultado;
      //   }

      //   comunasLista = comuna;
      // } else {
        const comuna: ComunasEntity[] = await this.comunasRepository.find();
        comunasLista = comuna;
      //}

      //si la comuna no tiene una municipalidad asociada la seleccionamos
      let comunasListaDto: ComunasListaDto[] = [];
      for (const element of comunasLista) {
        if (element.municipalidad == null) {
          const itemComuna: ComunasListaDto = new ComunasListaDto();
          itemComuna.idComuna = element.idComuna;
          itemComuna.nombre = element.Nombre;
          itemComuna.idRegion = element.idRegion;

          comunasListaDto.push(itemComuna);
        }
      }

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = comunasListaDto;
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error al obtener las comunas';
    }

    return resultado;
  }

  async obtenerMunicipalidadesUsuario(req:any): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      let municipalidades: MunicipalidadEntity[];

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaCreacionAdministracionMunicipalidades);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      const superUsuario = usuarioValidado.Respuesta.rolesUsuarios.find(x => x.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID);

      //Si no es superusuario comprobamos la region corresponda a la comuna
      if (!superUsuario) {
        //Si eres usuario seremitt solo en las regiones correspondientes
        const rolesUsuarioRecuperados = usuarioValidado.Respuesta.rolesUsuarios as RolesUsuarios[];
        let regiones :number[] = [];

        let listaRegionesRolRegion :number[] = [];
        listaRegionesRolRegion = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolRegionID).map(x => x.idRegion);

        let listaRegionesRolOficina :number[] = [];
        listaRegionesRolOficina = rolesUsuarioRecuperados.filter(x => x.roles.tipoRoles.idTipoRol == tipoRolOficinaID).map(x => x.oficinas.instituciones.comunas.idRegion);

        regiones = regiones.concat(listaRegionesRolRegion);
        regiones = regiones.concat(listaRegionesRolOficina);

        //Nos traemos las municipalidades según la region y que estén disponibles
        municipalidades = await this.municipalidadRepository
          .createQueryBuilder('municipalidad')
          .leftJoinAndSelect('municipalidad.comuna', 'comuna')
          .where('comuna.idRegion IN(:...regiones)', { regiones: regiones })
          .andWhere('municipalidad.idTipoInstitucion=:id', { id: TipoInstitucion_Municipalidad })
          .andWhere('municipalidad.idComuna is not null')
          .andWhere('municipalidad.activo = true')
          .getMany();
        if (municipalidades.length <= 0) {
          resultado.Error = 'No tienes municipalidades disponibles';
          return resultado;
        }
      } else {
        municipalidades = await this.municipalidadRepository.find({
          where: { idTipoInstitucion: TipoInstitucion_Municipalidad, idComuna: Not(IsNull()), activo: true },
        });
      }

      const listaMunicipalidadesDto: ListaMunicipalidadesUsuarioDto[] = plainToClass(ListaMunicipalidadesUsuarioDto, municipalidades);

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = listaMunicipalidadesDto;
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error al obtener las municipalidades';
    }

    return resultado;
  }
}
