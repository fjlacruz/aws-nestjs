import { ApiPropertyOptional } from "@nestjs/swagger";

export class AudicenciaConductoresNotificacionesDto {
    @ApiPropertyOptional()
    IdConductor: number;
    @ApiPropertyOptional()
    IdNotificacion: number;
    @ApiPropertyOptional()
    RunConductor: string;
    @ApiPropertyOptional()
    DvConductor: string;
    @ApiPropertyOptional()
    RunCompletoConductor: string;
}
