import { ApiPropertyOptional } from "@nestjs/swagger";

export class AudicenciaComunaNotificacionesDto {
    @ApiPropertyOptional()
    IdInstitucion: number;
    @ApiPropertyOptional()
    IdNotificacion: number;
    @ApiPropertyOptional()
    CodigoComuna: string;

}
