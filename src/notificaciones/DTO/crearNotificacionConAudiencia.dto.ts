import { ApiPropertyOptional } from "@nestjs/swagger";
import { AudienciaNotificacionesComuna } from "../entity/audienciaComunasNotificaciones.entity";
import { AudienciaNotificacionesConductores } from "../entity/audienciaConductoresNotificaciones.entity";
import { AudicenciaComunaNotificacionesDto } from "./audienciaComunaNotificiacionDto.dto";
import { AudicenciaConductoresNotificacionesDto } from "./AudienciaConductorNotificacionesDto.dto";


export class CrearNotificacionAudienciaDTO {

    @ApiPropertyOptional()
    titulo: string;

    @ApiPropertyOptional()
    textoNotificacion: string;

    @ApiPropertyOptional()
    idCategoria: number;

    @ApiPropertyOptional()
    fecha: Date;

    @ApiPropertyOptional()
    foto: string;
    
    @ApiPropertyOptional()
    todasComunas:boolean;

    @ApiPropertyOptional()
    audienciaConductoresNotificacionesDto : AudicenciaConductoresNotificacionesDto[];

    @ApiPropertyOptional()
    audienciaComunasNotificacionesDto: AudicenciaComunaNotificacionesDto[];
    
    @ApiPropertyOptional()
    idsComunas : number[];

    @ApiPropertyOptional()
    fechaPublicacion:Date;

    @ApiPropertyOptional()
    IdNotificacionesTipoAudiencia?: number;      
    
}

