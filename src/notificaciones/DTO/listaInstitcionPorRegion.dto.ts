import { Exclude, Expose } from "class-transformer";

@Expose()
export class ListaInstitucionPorRegionDto
{

    @Expose()
    idInstitucion:number;

    @Exclude()
    idTipoInstitucion:number;

    @Expose()
    Nombre:string;

    @Exclude()
    Descripcion:string;

    @Exclude()
    idComuna:number;
}