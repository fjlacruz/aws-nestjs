import { ApiPropertyOptional } from "@nestjs/swagger";

export class NotificacionDto {
    @ApiPropertyOptional()
    titulo:string;
    @ApiPropertyOptional()
    textoNotificacion:string;
    @ApiPropertyOptional()
    fecha: Date;
    @ApiPropertyOptional()
    idCategoria:number;
    @ApiPropertyOptional()
    foto:string;
}
