import { IsBoolean, IsNumber, IsString } from "class-validator";
import { isBoolean } from "lodash";

export class VistaListaNotificacionesDto
{
    @IsNumber()
    idNotificacion:number;

    @IsString()
    titulo:string;

    @IsString()
    fechaCreacion: Date;

    @IsString()
    responsable:string;

    @IsString()
    categoria:string;

    @IsBoolean()
    estado:boolean;

    @IsString()
    tipo:string;

    @IsNumber()
    idEstadoEnviado:number;
}