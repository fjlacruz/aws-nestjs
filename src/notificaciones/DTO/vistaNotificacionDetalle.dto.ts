import { IsBoolean, IsNumber, IsString } from "class-validator";
import { isBoolean, isString } from "lodash";

export class VistaNotificacionDetalleDto
{

    @IsString()
    responsable:string;

    @IsString()
    titulo: string;

    @IsString()
    textoNotificacion: string;

    @IsString()
    fotoNotificacionBinary: string;
  
    fechaPublicacion: string;
    
    @IsString()
    categoria:number;

    @IsBoolean()
    todasLasregiones:boolean;

    @IsNumber()
    idEstadoEnviadoNotificacion:number;

    listaUsuariosDestino:string[];

    listaMunicipalidadesDestino:number[];

    listaRegionesDestino:number[];

     


}
