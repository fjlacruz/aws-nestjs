import { Controller, Param, Req } from '@nestjs/common';
import { Get, Post, Body } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { CrearNotificacionAudienciaDTO } from '../DTO/crearNotificacionConAudiencia.dto';
import { NotificacionDto } from '../DTO/notificacion.dto';
import { NotificacionesService } from '../services/notificaciones.service';

@ApiTags('Notificaciones')
@Controller('Notificaciones')
export class NotificacionesController {

    constructor(private readonly notificacionesService: NotificacionesService,
        private readonly authService: AuthService) { }

    @Get('getNotificaciones')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las notificaciones' })
    async getNotificaciones(@Req() req: any) {

        // let splittedBearerToken = req.headers.authorization.split(" ");
        // let token = splittedBearerToken[1];

        // let tokenPermisos = new TokenPermisoDto(token, [
        //     PermisosNombres.VisualizarConsultaAntecedentesObtencionRenovacion
        // ]);

        // let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        // if (validarPermisos.ResultadoOperacion) {
            const data = await this.notificacionesService.getNotificaciones();
            return { data };
        // } else {

        //     return { data: validarPermisos };
        // }

    }

    // @Post('crearNotificacion')
    // @ApiBearerAuth()
    // @ApiOperation({ summary: 'Servicio que crea una notificacion' })
    // async crearNotificacion(@Body() notificacion: NotificacionDto, @Req() req: any) {

    //     // let splittedBearerToken = req.headers.authorization.split(" ");
    //     // let token = splittedBearerToken[1];

    //     // let tokenPermisos = new TokenPermisoDto(token, [
    //     //     PermisosNombres.VisualizarConsultaAntecedentesObtencionRenovacion
    //     // ]);

    //     // let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    //     // if (validarPermisos.ResultadoOperacion) {
    //         const data = await this.notificacionesService.crearNotificacion(notificacion);
    //         return { data };
    //     // } else {

    //     //     return { data: validarPermisos };
    //     // }

    // }

    @Post('crearNotificacionGeneral')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una notificacion' })
    async crearNotificacionGeneral(@Body() notificacion: CrearNotificacionAudienciaDTO, @Req() req: any) {

         let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

         let tokenPermisos = new TokenPermisoDto(token, [
             PermisosNombres.AdministracionSGLCAdministracionyParametrizacionAppMobileCrearNotificacionGeneral
         ]);

         let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

         if (validarPermisos.ResultadoOperacion) {

            let user = validarPermisos.Respuesta as RolesUsuarios;

             
            const data = await this.notificacionesService.crearNotificacionGeneral(notificacion,user[0].idUsuario);
            return { data };
         } else {

            return { data: validarPermisos };
         }

    }

    
    @Post('crearNotificacionAudienciaComuna')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una notificacion de Audiencia Comuna' })
    async crearNotificacionAudienciaComuna(@Body() notificacion: CrearNotificacionAudienciaDTO, @Req() req: any) {

         let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

         let tokenPermisos = new TokenPermisoDto(token, [
             PermisosNombres.AdministracionSGLCAdministracionyParametrizacionAppMobileCrearNotificacionGeneral
         ]);

         let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

         if (validarPermisos.ResultadoOperacion) {

            let user = validarPermisos.Respuesta as RolesUsuarios;

             
            const data = await this.notificacionesService.crearNotificacionesAudienciaComuna(notificacion,user[0].idUsuario);
            return { data };
         } else {

            return { data: validarPermisos };
         }

    }

    @Post('crearNotificacionAudienciaConductores')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una notificacion de Audiencia Conductor' })
    async crearNotificacionAudienciaConductores(@Body() notificacion: CrearNotificacionAudienciaDTO, @Req() req: any) {

         let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

         let tokenPermisos = new TokenPermisoDto(token, [
             PermisosNombres.AdministracionSGLCAdministracionyParametrizacionAppMobileCrearNotificacionGeneral
         ]);

         let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

         if (validarPermisos.ResultadoOperacion) {

            let user = validarPermisos.Respuesta as RolesUsuarios;

             
            const data = await this.notificacionesService.crearNotificacionesAudienciaConductor(notificacion,user[0].idUsuario);
            return { data };
         } else {

            return { data: validarPermisos };
         }

    }

    @Get('obtenerNotificacion/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio para obtener el detalle de una notificacion' })
    async geNotificacion(@Param('id') id:number, @Req() req: any) {

         let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

         let tokenPermisos = new TokenPermisoDto(token, [
             PermisosNombres.AdministracionSGLCAdministracionyParametrizacionAppMobileVisualizarNotificacionesGenerales
         ]);

         let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

         if (validarPermisos.ResultadoOperacion) {

            let user = validarPermisos.Respuesta as RolesUsuarios;

             
            const data = await this.notificacionesService.getNotificacion(id);
            return { data };
         } else {

            return { data: validarPermisos };
         }

    }

    @Post('/obtenerListaNotificaciones')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un listado de notificaciones' })
    async getListaNotificaciones(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
        
            const data = await this.notificacionesService.getListaNotificaciones(req, paginacionArgs);
            return { data };
    
    }

    @Post('municipalidadesByRegion/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Lista de nmunicipalidades segun una region' })
    async getMunicipalidadesByRegion(@Param('id') id:number,@Req() req: any) {

         let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

         let tokenPermisos = new TokenPermisoDto(token, [
             PermisosNombres.VisualizarListaAppMobileAdminYParam
         ]);

         let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

         if (validarPermisos.ResultadoOperacion) {

            let user = validarPermisos.Respuesta as RolesUsuarios;

             
            const data = await this.notificacionesService.getMuniciapalidadesByRegion(id);
            return { data };
         } else {

            return { data: validarPermisos };
         }

    }

    @Post('reenviarNotificacion/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Lista de nmunicipalidades segun una region' })
    async postReenviarNotificacion(@Param('id') id:number,@Req() req: any) {

         let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

         let tokenPermisos = new TokenPermisoDto(token, [
             PermisosNombres.AdministracionSGLCAdministracionyParametrizacionAppMobileCrearNotificacionGeneral
         ]);

         let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

         if (validarPermisos.ResultadoOperacion) {

            const data = await this.notificacionesService.reenviarNotificacion(id);
            return { data };
         } else {

            return { data: validarPermisos };
         }

    }
    

}
