
import { ComunasEntity } from "src/comunas/entity/comunas.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { NotificacionesEntity } from "./notificaciones.entity";

@Entity('AudienciaNotificacionesComunas')
export class AudienciaNotificacionesComuna {
    @PrimaryGeneratedColumn('uuid')
    readonly idAudienciaNotificacionesComunas: number;

    @Column({type: 'integer'})
    idNotificacion: number;
    @ManyToOne(()=> NotificacionesEntity,notificacionComuna=> notificacionComuna.audienciaComuna)
    @JoinColumn({name:"idNotificacion"})
    readonly notificacionComuna: NotificacionesEntity;



    @Column()
    idComuna: number;

    @ManyToOne(() => ComunasEntity, comuna => comuna.AudienciaNotificacionesComuna)
    @JoinColumn({ name: 'idComuna' })
    readonly comuna: ComunasEntity;

}