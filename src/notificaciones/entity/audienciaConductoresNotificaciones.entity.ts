import { Postulante } from "src/registro-pago/entity/postulante.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { NotificacionesEntity } from "./notificaciones.entity";

@Entity('AudienciaNotificacionesConductores')
export class AudienciaNotificacionesConductores {
    @PrimaryGeneratedColumn('uuid')
    readonly idAudienciaNotificacionesConductores: number;

    // @Column({ type: 'integer'})
    // readonly IdConductor: number;

    @Column({type: 'integer'})
    idNotificacion: number;
    @ManyToOne(()=> NotificacionesEntity,notificacionConductor=> notificacionConductor.audienciaConductor)
    @JoinColumn({name:"idNotificacion"})
    readonly notificacionConductor: NotificacionesEntity;

    @Column()
    idConductor: number;
    @ManyToOne(() => Postulante, conductores => conductores.audicenciaConductoresNotificaciones)
    @JoinColumn({ name: 'idConductor' })
    readonly conductor: Postulante;

}