import { ApiProperty } from "@nestjs/swagger";
import { CategoriasNotificacionEntity } from "src/categorias-notificacion/entity/categorias-notificacion.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, ManyToOne, JoinColumn, OneToMany} from "typeorm";
import { AudienciaNotificacionesComuna } from "./audienciaComunasNotificaciones.entity";
import { AudienciaNotificacionesConductores } from "./audienciaConductoresNotificaciones.entity";

@Entity('Notificaciones')
export class NotificacionesEntity {

    @PrimaryGeneratedColumn()
    idNotificacion:number;

    @Column()
    Titulo:string;

    @Column()
    TextoNotificacion:string;

    @Column()
    Activado:boolean;

    @Column()
    idCategoria:number;
    @ManyToOne(() => CategoriasNotificacionEntity, categoriasNotificacion => categoriasNotificacion.notificacion)
    @JoinColumn({ name: 'idCategoria' })
    readonly categoriasNotificacion: CategoriasNotificacionEntity;

    @Column({
        type: 'bytea',
        nullable: false
    })
    FotoNotificacionBinary:Buffer;

    @Column()
    Fecha:Date;

    
    @Column({ type: 'integer'})
    idNotificacionesTipoAudiencia: number;    

    @Column()
    fechaPublicacion: Date;

    @Column()
    idEstadoEnviadoNotificacion:number;

    @Column()
    idNotificacionMovil:number;

    @Column()
    idUsuario:number;
    @ManyToOne(()=> User, usuarios => usuarios.notificacion)
    @JoinColumn( {name:'idUsuario'})
    readonly usuarios:User

    @OneToMany(() => AudienciaNotificacionesConductores, audienciaConductoresNotificaciones => audienciaConductoresNotificaciones.notificacionConductor)
    readonly audienciaConductor: AudienciaNotificacionesConductores[];

    
    @OneToMany(() => AudienciaNotificacionesComuna, audienciaComuna => audienciaComuna.notificacionComuna)
    readonly audienciaComuna: AudienciaNotificacionesComuna[];
    


}
