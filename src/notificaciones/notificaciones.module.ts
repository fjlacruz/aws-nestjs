import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotificacionesController } from './controller/notificaciones.controller';
import { NotificacionesEntity } from './entity/notificaciones.entity';
import { NotificacionesService } from './services/notificaciones.service';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { AuthService } from 'src/auth/auth.service';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { UtilService } from 'src/utils/utils.service';

@Module({
  imports: 
  [
    TypeOrmModule.forFeature([NotificacionesEntity, User, RolesUsuarios,PostulantesEntity,ComunasEntity,InstitucionesEntity]),
  ],
  providers: [NotificacionesService, AuthService, UtilService],
  exports: [NotificacionesService],
  controllers: [NotificacionesController]
})
export class NotificacionesModule {}
