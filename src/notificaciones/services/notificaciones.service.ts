import { Injectable, NotFoundException, Res } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { randomBytes } from 'crypto';
import { orderBy } from 'lodash';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { BASE_URL_BACK_MOBILE, URL_MOBILE_CREAR_NOTIFICACION, URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_COMUNAS, URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_CONDUCTOR } from 'src/approutes.config';
import { AuthService } from 'src/auth/auth.service';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { CATEGORIANOTIFCONTROLESVENCCAMRNC, CATEGORIANOTIFMINISTERIOTRANSPORTESTELEC, CATEGORIANOTIFSEGURIDADVIAL, CATEGORIANOTIFTRAMITESOLICITUDESLICCOND, ColumnasNotificaciones, NotificacionesTipoAudienciaComuna, NotificacionesTipoAudienciaConductor, PermisosNombres, SECRET_PASS_CRYPTO, TipoInstitucion_Municipalidad, TOKEN_NOTIFICACION } from 'src/constantes';
import { MunicipalidadEntity } from 'src/municipalidad/entity/municipalidad.entity';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { OficinasDTO } from 'src/tramites/DTO/oficinas.dto';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { User } from 'src/users/entity/user.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { Resultado } from 'src/utils/resultado';
import { UtilService } from 'src/utils/utils.service';
import { In, Repository, UpdateResult } from 'typeorm';
import { getConnection } from "typeorm";
import { AudicenciaComunaNotificacionesDto } from '../DTO/audienciaComunaNotificiacionDto.dto';
import { AudicenciaConductoresNotificacionesDto } from '../DTO/AudienciaConductorNotificacionesDto.dto';
import { CrearNotificacionAudienciaDTO } from '../DTO/crearNotificacionConAudiencia.dto';
import { FiltroNotificacionesDto } from '../DTO/filtroNotificaciones.dto';
import { ListaInstitucionPorRegionDto } from '../DTO/listaInstitcionPorRegion.dto';
import { NotificacionDto } from '../DTO/notificacion.dto';
import { VistaListaNotificacionesDto } from '../DTO/vistaListaNotificaciones.dto';
import { VistaNotificacionDetalleDto } from '../DTO/vistaNotificacionDetalle.dto';
import { AudienciaNotificacionesComuna } from '../entity/audienciaComunasNotificaciones.entity';
import { AudienciaNotificacionesConductores } from '../entity/audienciaConductoresNotificaciones.entity';
import { NotificacionesEntity } from '../entity/notificaciones.entity';

@Injectable()
export class NotificacionesService {

    constructor(
        @InjectRepository(NotificacionesEntity) private readonly notificacionesRepository: Repository<NotificacionesEntity>,
        @InjectRepository(PostulantesEntity) private readonly postulanteRepository: Repository<PostulantesEntity>,
        @InjectRepository(ComunasEntity) private readonly comunasRepository: Repository<ComunasEntity>,
        @InjectRepository(InstitucionesEntity) private readonly intitucionesRepository: Repository<InstitucionesEntity>,
        private readonly utilService: UtilService,
        private readonly authService: AuthService
    ) { }

    async getNotificaciones() {
        const resultado: Resultado = new Resultado();

        try {
            resultado.Respuesta = await this.notificacionesRepository.find();
            resultado.ResultadoOperacion = true;

        } catch (error) {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo notificaciones';
        }

        return resultado;
    }

    // async crearNotificacion(notificacionDTO: NotificacionDto) {
    //     const resultado: Resultado = new Resultado();

    //     const connection = getConnection();
    //     const queryRunner = connection.createQueryRunner();
    //     await queryRunner.connect();
    //     await queryRunner.startTransaction();

    //     try {
    //         const notificacion = new NotificacionesEntity();
    //         notificacion.idCategoria = notificacionDTO.idCategoria;
    //         notificacion.Titulo = notificacionDTO.titulo;
    //         notificacion.TextoNotificacion = notificacionDTO.textoNotificacion;
    //         notificacion.Activado = true;
    //         notificacion.FotoNotificacionBinary = Buffer.from(notificacionDTO.foto);
    //         notificacion.Fecha = new Date();

    //         // Insertado de la notificacion en la base de datos Web
    //         await this.notificacionesRepository.insert(notificacion);

    //         // Insertado de la notificacion en la parte Mobile
    //         const respuestaRegCivil = await this.postNotificacionMobile(notificacionDTO);

    //         if (respuestaRegCivil.ResultadoOperacion) {
    //             resultado.ResultadoOperacion = true;
    //             resultado.Mensaje = 'Notificacion creada corretamente';
    //             await queryRunner.commitTransaction();
    //         } else {
    //             resultado.ResultadoOperacion = true;
    //             resultado.Error = 'Error creando notificacion en la parte Mobile';
    //             await queryRunner.rollbackTransaction();
    //             await queryRunner.release();
    //         }

    //     } catch (error) {
    //         resultado.ResultadoOperacion = false;
    //         resultado.Error = 'Error insertando la notificacion';
    //         await queryRunner.rollbackTransaction();
    //         await queryRunner.release();
    //     }

    //     return resultado;
    // }

    // async postNotificacionMobile(notificacionDTO: NotificacionDto): Promise<Resultado> {
    //     const resultado: Resultado = new Resultado();

    //     const request = require('request');

    //     try {
    //         return new Promise(function (resolve, reject) {
    //             request.post(URL_MOBILE_CREAR_NOTIFICACION, { form: notificacionDTO }, function (error, res, body) {

    //                 if (!error && res.statusCode == 201) {
    //                     resultado.ResultadoOperacion = true;
    //                     resolve(resultado);
    //                 } else {
    //                     console.log(error);
    //                     resultado.Error = error;
    //                     resultado.ResultadoOperacion = false;
    //                     reject(resultado);
    //                 }
    //             });
    //         });
    //     } catch (error) {
    //         console.log(error)
    //     }

    // }

    async postNotificacionMobile(notificacionDTO: CrearNotificacionAudienciaDTO, urlMobile: string): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = false;

        const request = require('request');

        const tokenEncriptado = this.utilService.encriptar();

        try {
            return new Promise(function (resolve, reject) {
                request.post(urlMobile, { json: notificacionDTO, headers: { 'Authorization': tokenEncriptado } }, function (error, res, body) {

                    if (!error && res.statusCode == 201) {
                        const json = body;
                        resultado.ResultadoOperacion = json.ResultadoOperacion;
                        resultado.Respuesta = json.Respuesta;
                        resolve(resultado);
                    } else {
                        console.log(error);
                        resultado.Error = error;
                        resultado.ResultadoOperacion = false;
                        resolve(resultado);
                    }
                });
            });
        } catch (error) {
            console.log(error)
            return resultado;
        }

    }


    async crearNotificacionGeneral(notificacionGeneral: CrearNotificacionAudienciaDTO, userId: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = false;

        notificacionGeneral.idCategoria = Number(notificacionGeneral.idCategoria);

        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();


        try {



            //Comprobamos que la categoría es la correcta
            if (notificacionGeneral.idCategoria == CATEGORIANOTIFTRAMITESOLICITUDESLICCOND || notificacionGeneral.idCategoria == CATEGORIANOTIFCONTROLESVENCCAMRNC) {
                resultado.Mensaje = 'Error creando notificacion, la categoría facilitada no es correcta.';
                resultado.ResultadoOperacion = false;
                await queryRunner.release();
                return resultado;
            }

            //Comprobamos que la notificacion que se crea es general y no tiene
            if (!notificacionGeneral.todasComunas || notificacionGeneral.audienciaComunasNotificacionesDto != null || notificacionGeneral.audienciaConductoresNotificacionesDto != null) {

                await queryRunner.release();
                resultado.Error = "No es una notificacion de tipo general";
                return resultado;
            }


            //Creamos la notificaciones
            const notificacion = new NotificacionesEntity();
            notificacion.idCategoria = notificacionGeneral.idCategoria;
            notificacion.Titulo = notificacionGeneral.titulo;
            notificacion.TextoNotificacion = notificacionGeneral.textoNotificacion;
            notificacion.Activado = true;
            notificacion.FotoNotificacionBinary = Buffer.from(notificacionGeneral.foto);
            notificacion.Fecha = new Date();
            notificacion.idUsuario = userId;
            notificacion.fechaPublicacion = notificacionGeneral.fechaPublicacion;
            notificacion.idEstadoEnviadoNotificacion = 1;


            //Mandamos la notificiacion al servicio movil
            var servicioNotificacionMovil = await this.postNotificacionMobile(notificacionGeneral,
                BASE_URL_BACK_MOBILE + URL_MOBILE_CREAR_NOTIFICACION);

            if (!servicioNotificacionMovil.ResultadoOperacion) {

                notificacion.idEstadoEnviadoNotificacion = 2;
                notificacion.idNotificacionMovil= null;
            }
            if(servicioNotificacionMovil.ResultadoOperacion)
            {
                notificacion.idNotificacionMovil = +servicioNotificacionMovil.Respuesta
            }

            //Lo asignamos a la transaccion 
            await queryRunner.manager.save(notificacion);

            //Hacemos commit a la transaccion
            await queryRunner.commitTransaction();
            resultado.ResultadoOperacion = true;
            resultado.Mensaje = "Notificaciones creadas correctamente";



        }
        catch (error) {
            //Capturamos el error
            console.log(error);
            await queryRunner.rollbackTransaction();
            return resultado;

        }
        finally {
            await queryRunner.release();
        }


        return resultado;
    }

    async crearNotificacionesAudienciaConductor(notificacionAudienciaDTO: CrearNotificacionAudienciaDTO, userId: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = true;

        notificacionAudienciaDTO.idCategoria = Number(notificacionAudienciaDTO.idCategoria);


        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {


            //Comprobamos que la categoría es la correcta
            if (notificacionAudienciaDTO.idCategoria != CATEGORIANOTIFSEGURIDADVIAL || notificacionAudienciaDTO.idCategoria != CATEGORIANOTIFMINISTERIOTRANSPORTESTELEC) {
                resultado.Mensaje = 'Error creando notificacion, la categoría facilitada no es correcta.';
                resultado.ResultadoOperacion = false;

                return resultado;
            }

            //Comprobamos que manden objeto para determina la audiencia
            if (notificacionAudienciaDTO.audienciaConductoresNotificacionesDto == null || notificacionAudienciaDTO.audienciaConductoresNotificacionesDto.length < 1) {
                resultado.Mensaje = 'Error creando notificacion, no se ha definido audiencia para la notificación.';
                resultado.ResultadoOperacion = false;

                return resultado;
            }

            const obtenerConductores = await this.obtenerConductoresComunaDestinoNotificacion(notificacionAudienciaDTO);

            if (!obtenerConductores.ResultadoOperacion) {
                resultado.Error = obtenerConductores.Error;
                await queryRunner.release();
                return resultado;

            }

            let conductoresDestinoNotificacion = obtenerConductores.Respuesta as PostulantesEntity[];


            //Asignamos el tipo de notificacion
            notificacionAudienciaDTO.IdNotificacionesTipoAudiencia = NotificacionesTipoAudienciaConductor;
            //Creamos la notificaciones

            const notificacion = new NotificacionesEntity();
            notificacion.idCategoria = notificacionAudienciaDTO.idCategoria;
            notificacion.Titulo = notificacionAudienciaDTO.titulo;
            notificacion.TextoNotificacion = notificacionAudienciaDTO.textoNotificacion;
            notificacion.Activado = true;
            notificacion.FotoNotificacionBinary = Buffer.from(notificacionAudienciaDTO.foto);
            notificacion.Fecha = new Date();
            notificacion.fechaPublicacion = notificacionAudienciaDTO.fechaPublicacion;
            notificacion.idNotificacionesTipoAudiencia = notificacionAudienciaDTO.IdNotificacionesTipoAudiencia;
            notificacion.idUsuario = userId;
            notificacion.idEstadoEnviadoNotificacion = 1;

            //Mandamos la notificiacion al servicio movil
            var servicioNotificacionMovil = await this.postNotificacionMobile(notificacionAudienciaDTO,
                BASE_URL_BACK_MOBILE + URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_CONDUCTOR);

            if (!servicioNotificacionMovil.ResultadoOperacion) {
                notificacion.idEstadoEnviadoNotificacion = 2;
                notificacion.idNotificacionMovil = null;
                resultado.Mensaje = "Notificacion con Audiencia creada correctamente, pero no enviada ";
            }
            if(servicioNotificacionMovil.ResultadoOperacion)
            {
                notificacion.idNotificacionMovil = +servicioNotificacionMovil.Respuesta;
                resultado.Mensaje = "Notificacion con Audiencia creada correctamente y enviada";
            }


            //Insertamos al Notificacion
            await queryRunner.manager.save(notificacion);

            //recorremos el bucle de conductores para añadirlos al registro notificaciones
            for (const element of conductoresDestinoNotificacion) {
                let conductorAudiencia: AudienciaNotificacionesConductores = new AudienciaNotificacionesConductores();
                conductorAudiencia.idConductor = element.idUsuario;
                conductorAudiencia.idNotificacion = notificacion.idNotificacion;

                await queryRunner.manager.save(conductorAudiencia);
            }


            await queryRunner.commitTransaction();
            resultado.Mensaje = "Notificacion con Audiencia creada correctamente";
            resultado.ResultadoOperacion = true;

        }
        catch (error) {
            console.log(error);
            resultado.Error = "Error al almacenar la notificaciones";
            await queryRunner.rollbackTransaction();


        }
        finally {
            await queryRunner.release();
        }

        return resultado;

    }

    async crearNotificacionesAudienciaComuna(notificacionAudienciaDTO: CrearNotificacionAudienciaDTO, userId: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = false;

        notificacionAudienciaDTO.idCategoria = Number(notificacionAudienciaDTO.idCategoria);

        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {


            //Comprobamos que la categoría es la correcta
            if (notificacionAudienciaDTO.idCategoria == CATEGORIANOTIFTRAMITESOLICITUDESLICCOND || notificacionAudienciaDTO.idCategoria == CATEGORIANOTIFCONTROLESVENCCAMRNC) {
                resultado.Mensaje = 'Error creando notificacion, la categoría facilitada no es correcta.';
                resultado.ResultadoOperacion = false;

                return resultado;
            }

            //Comprobamos que manden objeto para determina la audiencia
            if (notificacionAudienciaDTO.audienciaComunasNotificacionesDto == null || notificacionAudienciaDTO.audienciaComunasNotificacionesDto.length < 1) {
                resultado.Mensaje = 'Error creando notificacion, no se ha definido audiencia para la notificación.';
                resultado.ResultadoOperacion = false;

                return resultado;
            }

            const obtenerConductores = await this.obtenerConductoresComunaDestinoNotificacion(notificacionAudienciaDTO);

            if (!obtenerConductores.ResultadoOperacion) {
                resultado.Error = obtenerConductores.Mensaje;
                await queryRunner.release();
                return resultado;

            }

            let conductoresDestinoNotificacion = obtenerConductores.Respuesta as number[];

            notificacionAudienciaDTO.idsComunas = [];
            // Cojemos el id de la municipalidad y obtenemos el de la comuna
            for (const institucion of notificacionAudienciaDTO.audienciaComunasNotificacionesDto) {
                const institucionBBDD =  await queryRunner.manager.findOne(InstitucionesEntity, {where: {idInstitucion: institucion.IdInstitucion}});

                notificacionAudienciaDTO.idsComunas.push(institucionBBDD.idComuna);
            }

            //Asignamos el tipo de audiencia
            notificacionAudienciaDTO.IdNotificacionesTipoAudiencia = NotificacionesTipoAudienciaComuna;

            //Creamos la notificaciones

            const notificacion = new NotificacionesEntity();
            notificacion.idCategoria = notificacionAudienciaDTO.idCategoria;
            notificacion.Titulo = notificacionAudienciaDTO.titulo;
            notificacion.TextoNotificacion = notificacionAudienciaDTO.textoNotificacion;
            notificacion.Activado = true;
            notificacion.FotoNotificacionBinary = Buffer.from(notificacionAudienciaDTO.foto);
            notificacion.Fecha = new Date();
            notificacion.fechaPublicacion = notificacionAudienciaDTO.fechaPublicacion;
            notificacion.idNotificacionesTipoAudiencia = notificacionAudienciaDTO.IdNotificacionesTipoAudiencia;
            notificacion.idUsuario = userId;
            notificacion.idEstadoEnviadoNotificacion = 1;


            //Mandamos la notificiacion al servicio movil
            var servicioNotificacionMovil = await this.postNotificacionMobile(notificacionAudienciaDTO,
                BASE_URL_BACK_MOBILE + URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_COMUNAS);

            if (!servicioNotificacionMovil.ResultadoOperacion) {
                notificacion.idEstadoEnviadoNotificacion = 2;
                notificacion.idNotificacionMovil = null;
            }
            if(servicioNotificacionMovil.ResultadoOperacion)
            {
                notificacion.idNotificacionMovil = +servicioNotificacionMovil.Respuesta;
            }


            //Insertamos al Notificacion
            await queryRunner.manager.save(notificacion);

            //recorremos el bucle de conductores para añadirlos al registro notificaciones
            for (const element of conductoresDestinoNotificacion) {
                let comunaAudiencia: AudienciaNotificacionesComuna = new AudienciaNotificacionesComuna();
                comunaAudiencia.idComuna = element
                comunaAudiencia.idNotificacion = notificacion.idNotificacion;

                await queryRunner.manager.save(comunaAudiencia);
            }


            await queryRunner.commitTransaction();
            resultado.Mensaje = "Notificacion con Audiencia creada correctamente";
            resultado.ResultadoOperacion = true;

        }
        catch (error) {
            console.log(error);
            resultado.Error = "Error al almacenar la notificaciones";
            await queryRunner.rollbackTransaction();


        }
        finally {
            await queryRunner.release();
        }

        return resultado;

    }


    /**
     * Obtenemos los distintos postulantes o comunas según la petición 
     * @param notificacionAudienciaDTO el objeto notificiacione a crear
     * @returns retornamos el objeto resultado.
     */
    async obtenerConductoresComunaDestinoNotificacion(notificacionAudienciaDTO: CrearNotificacionAudienciaDTO): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = true;
        let conductoresDestinoNotificacion: Object[] = [];

        //Usamos el atributo del Dto para determinar cual camino coger si son not usuario o notificaciones comunas
        if (notificacionAudienciaDTO.audienciaConductoresNotificacionesDto != null) {


            conductoresDestinoNotificacion = await this.postulanteRepository.find({
                relations: ['usuario'],
                where: qb => {
                    qb.where(notificacionAudienciaDTO.audienciaConductoresNotificacionesDto.length > 0 ?
                        'CONCAT("PostulantesEntity"."RUN",\'-\',\"PostulantesEntity"."DV") IN (:...runsPostulantes)' : 'TRUE', { runsPostulantes: notificacionAudienciaDTO.audienciaConductoresNotificacionesDto.map(x => x.RunCompletoConductor) })
                }

            });


        }
        if (notificacionAudienciaDTO.audienciaComunasNotificacionesDto != null) {


            if (!notificacionAudienciaDTO.todasComunas) {


                let comunas = await (await this.intitucionesRepository.find({ idInstitucion: In(notificacionAudienciaDTO.audienciaComunasNotificacionesDto.map(x => x.IdInstitucion)), idTipoInstitucion: TipoInstitucion_Municipalidad })).map(c => c.idComuna);

                //Cogemos los valores únicos del array
                comunas = [...new Set(comunas)];


                conductoresDestinoNotificacion = comunas;
            }
        }

        if (conductoresDestinoNotificacion.length < 1) {
            resultado.Mensaje = 'Error creando notificacion, no se ha encontrado ningún conductor con los Códigos RUN facilitados.';
            resultado.ResultadoOperacion = false;

            return resultado;

        }

        resultado.ResultadoOperacion = true;
        resultado.Respuesta = conductoresDestinoNotificacion;



        return resultado;

    }

    async getNotificacion(id: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = false;

        try {

            //Obtenemos la notificacion de la BD
            const notificacionBD = await this.notificacionesRepository.findOne({ relations: ['audienciaConductor', 'audienciaComuna', 'usuarios', 'categoriasNotificacion'], where: { idNotificacion: id } });

            //ASignamos los valores al objeto DTO que se muestra la vista
            const notificacionDTO: VistaNotificacionDetalleDto = new VistaNotificacionDetalleDto();
            notificacionDTO.categoria = notificacionBD.categoriasNotificacion.idCategoria;
            notificacionDTO.fechaPublicacion = notificacionBD.fechaPublicacion.toUTCString();
            notificacionDTO.fotoNotificacionBinary = notificacionBD.FotoNotificacionBinary.toString();
            notificacionDTO.textoNotificacion = notificacionBD.TextoNotificacion;
            notificacionDTO.titulo = notificacionBD.Titulo;
            notificacionDTO.todasLasregiones = notificacionBD.idNotificacionesTipoAudiencia == null ? true : false;
            notificacionDTO.responsable = notificacionBD.usuarios.Nombres + ' ' + notificacionBD.usuarios.ApellidoPaterno + ' ' + notificacionBD.usuarios.ApellidoMaterno;
            notificacionDTO.idEstadoEnviadoNotificacion = notificacionBD.idEstadoEnviadoNotificacion;

            //Determinamos si es del tipo audiencia Conductor para crear la lista
            if (notificacionBD.audienciaConductor.length > 0) {

                const usuarios = await this.postulanteRepository.find({ idUsuario: In(notificacionBD.audienciaConductor.map(x => x.idConductor)) });

                let listaUsuarios: string[] = [];

                for (const element of usuarios) {

                    listaUsuarios.push(element.Nombres + ' ' + element.ApellidoPaterno + ' ' + element.ApellidoMaterno);

                }

                notificacionDTO.listaUsuariosDestino = listaUsuarios;


            }

            //Determinamos si es del tipo audiencia Comuna para crear la lista
            if (notificacionBD.audienciaComuna.length > 0) {


                const comunas = await (await this.intitucionesRepository.find({ idComuna: In(notificacionBD.audienciaComuna.map(x => x.idComuna)) })).map(x => x.idInstitucion);
                notificacionDTO.listaMunicipalidadesDestino = comunas;

                const regiones = await (await this.comunasRepository.find({ idComuna: In(notificacionBD.audienciaComuna.map(x => x.idComuna)) })).map(r => r.idRegion);
                notificacionDTO.listaRegionesDestino = regiones;



            }



            //retornamos la respuesta
            resultado.Respuesta = notificacionDTO;


            resultado.ResultadoOperacion = true;


        }
        catch (error) {
            console.log(error);
            resultado.Error = "Error al obtener la notificacion";

        }

        return resultado;
    }

    async getListaNotificaciones(req:any, paginacionArgs: PaginacionArgs): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = false;

        let notificacionesParseadas = new PaginacionDtoParser<VistaListaNotificacionesDto>();
        let notificacionesPaginadas: Pagination<NotificacionesEntity>;
        let filtro: FiltroNotificacionesDto = paginacionArgs.filtro;
        let filtroBuscar = paginacionArgs.filtroBusqueda;
        const  orderBy = ColumnasNotificaciones[paginacionArgs.orden.orden]

        try {

            // Comprobamos permisos	
            let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaAppMobileAdminYParam);	
                
            // En caso de que el usuario no sea correcto	
            if (!usuarioValidado.ResultadoOperacion) {	
                return usuarioValidado;	
            }	


            //Determinamos el tipo de filtro si es el buscar por nombre general o si usa el dto
            if (filtroBuscar != null) {

                while (filtroBuscar.search('/') != -1) {
                    filtroBuscar = filtroBuscar.replace("/", "-");
                }

                notificacionesPaginadas = await paginate<NotificacionesEntity>(this.notificacionesRepository, paginacionArgs.paginationOptions, {
                    relations: ["usuarios", "categoriasNotificacion"],
                    where: qb => {
                        qb.where('lower(unaccent("NotificacionesEntity"."Titulo")) like lower(unaccent(:filtro))', { filtro: `%${filtroBuscar}%` })
                        .orWhere('lower(unaccent(NotificacionesEntity__categoriasNotificacion.NombreCategoria)) like lower(unaccent(:filtro))', { filtro: `%${filtroBuscar}%` })
                        .orderBy(orderBy, paginacionArgs.orden.ordenarPor);
                    }
                    //, order: { Titulo: paginacionArgs.orden.ordenarPor }

                });
            }
            else {
                notificacionesPaginadas = await paginate<NotificacionesEntity>(this.notificacionesRepository, paginacionArgs.paginationOptions, {
                    relations: ["usuarios", "categoriasNotificacion"],
                    where: qb => {
                        qb.where(filtro.categoria >= 0 ?
                            '"NotificacionesEntity"."idCategoria" =:idCategoria' : 'TRUE', { idCategoria: filtro.categoria })
                            .andWhere(filtro.estado != null ?
                                '"NotificacionesEntity"."Activado" =:estado' : 'TRUE', { estado: filtro.estado })
                            .andWhere(filtro.tipo > 0 ?
                                '"NotificacionesEntity"."idNotificacionesTipoAudiencia" =:tipoAudiencia' : 'TRUE', { tipoAudiencia: filtro.tipo })
                            .andWhere(filtro.tipo === null ?
                                '"NotificacionesEntity"."idNotificacionesTipoAudiencia" IS NULL' : 'TRUE')
                            .andWhere(filtro.titulo != null ?
                                'lower(unaccent("NotificacionesEntity"."Titulo")) like lower(unaccent(:nombre))' : 'TRUE', { nombre: `%${filtro.titulo}%` })
                            .orderBy(orderBy, paginacionArgs.orden.ordenarPor);
                    }

                });
            }
            //Comprobamos que los objetos no sean vacios
            if (Array.isArray(notificacionesPaginadas.items) && notificacionesPaginadas.items.length) {

                //Lista de Notificaciones para la respuesta
                let notificacionesDTOs: VistaListaNotificacionesDto[] = [];
                for (const element of notificacionesPaginadas.items) {
                    let item: VistaListaNotificacionesDto = new VistaListaNotificacionesDto();
                    item.idNotificacion = element.idNotificacion;
                    item.titulo = element.Titulo;
                    item.fechaCreacion = element.Fecha;
                    item.responsable = element.usuarios.Nombres + ' ' + element.usuarios.ApellidoPaterno + ' ' + element.usuarios.ApellidoMaterno;
                    item.categoria = element.categoriasNotificacion.NombreCategoria;
                    item.estado = element.Activado;
                    item.tipo = element.idNotificacionesTipoAudiencia == null ? "General" : "Personalizada";
                    item.idEstadoEnviado = element.idEstadoEnviadoNotificacion;

                    notificacionesDTOs.push(item);

                }

                notificacionesParseadas.items = notificacionesDTOs;

                notificacionesParseadas.links = notificacionesPaginadas.links;
                notificacionesParseadas.meta = notificacionesPaginadas.meta;

                resultado.ResultadoOperacion = true;
                resultado.Respuesta = notificacionesParseadas;
                resultado.Mensaje = "Notificaciones obtenidas correctamente";


            } else {
                resultado.Error = "No se ha podido obtener la respuesta";
            }

        }
        catch (error) {
            console.log(error);
            resultado.Error = "Error al listar las notificaciones";
        }

        return resultado;

    }

    async getMuniciapalidadesByRegion(id: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = false;

        try {




            var comunas = await (await this.comunasRepository.find({ where: { idRegion: +id } })).map(x => x.idComuna);

            var instituciones = await this.intitucionesRepository.find({ where: { idTipoInstitucion: TipoInstitucion_Municipalidad, idComuna: In(comunas) } });

            resultado.ResultadoOperacion = true;

            const resultadoLista: ListaInstitucionPorRegionDto[] = plainToClass(ListaInstitucionPorRegionDto, instituciones);

            resultado.Respuesta = resultadoLista;


        }
        catch (error) {
            console.log(error)
            resultado.Error = "Error al listar las municipalidades y regiones";

        }

        return resultado;
    }

    async reenviarNotificacion(id: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        resultado.ResultadoOperacion = false;

        let urlMovil: string = '';

        try {

            const notificacionBD: NotificacionesEntity = await this.notificacionesRepository.findOne({ relations: ['audienciaConductor', 'audienciaComuna', 'audienciaConductor.conductor'], where: { idNotificacion: id } });

            if (notificacionBD.idEstadoEnviadoNotificacion === 1) {
                resultado.Error = "No se puede enviar la notificación porque ya está enviada";
                return resultado;
            }

            const notificacionDTO: CrearNotificacionAudienciaDTO = new CrearNotificacionAudienciaDTO();

            notificacionDTO.IdNotificacionesTipoAudiencia = notificacionBD.idNotificacionesTipoAudiencia;
            notificacionDTO.fecha = notificacionBD.Fecha;
            notificacionDTO.fechaPublicacion = notificacionBD.fechaPublicacion;
            notificacionDTO.foto = notificacionBD.FotoNotificacionBinary.toString();
            notificacionDTO.idCategoria = notificacionBD.idCategoria;
            notificacionBD.idEstadoEnviadoNotificacion = notificacionBD.idEstadoEnviadoNotificacion;
            notificacionDTO.titulo = notificacionBD.Titulo;
            notificacionDTO.textoNotificacion = notificacionBD.TextoNotificacion;

            if (notificacionBD.idNotificacionesTipoAudiencia == null) {
                urlMovil = URL_MOBILE_CREAR_NOTIFICACION;
            } else {
                //Comprobamos que manden objeto para determina la audiencia
                if ( notificacionBD.audienciaComuna.length > 0) {

                    const idComunas: number[] = notificacionBD.audienciaComuna.map(x => x.idComuna);

                    let institucionesIds = await (await this.intitucionesRepository.find({ where: { idComuna: In(idComunas) } })).map(x => x.idInstitucion);

                    institucionesIds = [...new Set(institucionesIds)];


                    let audienciaComunasNotificacionesDto: AudicenciaComunaNotificacionesDto[] = [];

                    institucionesIds.forEach(element => {
                        const itemComuna: AudicenciaComunaNotificacionesDto = new AudicenciaComunaNotificacionesDto();
                        itemComuna.IdInstitucion = element;

                        audienciaComunasNotificacionesDto.push(itemComuna)
                    });

                    notificacionDTO.audienciaComunasNotificacionesDto = audienciaComunasNotificacionesDto;

                    urlMovil = URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_COMUNAS;


                }

                if ( notificacionBD.audienciaConductor.length > 0) {
                    const conductores = notificacionBD.audienciaConductor.map(x => x.conductor);



                    let audienciaConductoresNotificacionesDto: AudicenciaConductoresNotificacionesDto[] = [];

                    conductores.forEach(element => {

                        const itemConductor: AudicenciaConductoresNotificacionesDto = new AudicenciaConductoresNotificacionesDto();
                        itemConductor.RunCompletoConductor = element.RUN + '-' + element.DV;
                       
                        audienciaConductoresNotificacionesDto.push(itemConductor);

                    });

                    notificacionDTO.audienciaConductoresNotificacionesDto = audienciaConductoresNotificacionesDto;

                    urlMovil = URL_MOBILE_CREAR_NOTIFICACION_AUDIENCIA_CONDUCTOR;


                }



            }

            //Mandamos la notificiacion al servicio movil
            var servicioNotificacionMovil = await this.postNotificacionMobile(notificacionDTO,
                BASE_URL_BACK_MOBILE + urlMovil);

            if (!servicioNotificacionMovil.ResultadoOperacion) {

                resultado.Error = "Error al enviar la notificacion móvil";
                return resultado;

            }


            const notificacionActualizar:Partial<NotificacionesEntity> = await this.notificacionesRepository.findOne({idNotificacion:id});
            delete notificacionActualizar.idNotificacion;

            notificacionActualizar.idEstadoEnviadoNotificacion = 1;
            notificacionActualizar.idNotificacionMovil = +servicioNotificacionMovil.Respuesta;
            
        

         


            await this.notificacionesRepository.update(id, notificacionActualizar);

            resultado.ResultadoOperacion = true;
            resultado.Mensaje = "Notificacion Móvil enviada correctamente";



        }
        catch (error) {
            console.log(error);
            resultado.Error = "Error al enviar la notificacion Móvil";

        }



        return resultado;

    }



}
