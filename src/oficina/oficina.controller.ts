import { Body, Controller, Delete, Get, Logger, Param, Post, Put, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { OficinaService } from './oficina.service';
import { Request } from 'express';
import { Page, PageRequest } from '../tipo-institucion/pagination.entity';
import { HeaderUtil } from '../base/base/header-util';
import { OficinaEntity } from './oficina.entity';

@ApiTags('Oficinas')
@Controller('oficinas')
export class OficinaController {
  logger = new Logger('OficinaEntityController');
  constructor(private readonly oficinaService: OficinaService) {}

  @Get('/all')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Lista de Oficinas',
    type: OficinaEntity,
  })
  async getAllTipos(@Req() req: Request): Promise<OficinaEntity[]> {
    const result = await this.oficinaService.getAll();
    return result;
  }

  @Get('/all-jpl')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Lista de Oficinas',
    type: OficinaEntity,
  })
  async getAllTiposJpl(@Req() req: Request): Promise<OficinaEntity[]> {
    const result = await this.oficinaService.getAllJpl();
    return result;
  }

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Lista paginada de Oficinas',
    type: OficinaEntity,
  })
  async getAll(@Req() req: Request): Promise<OficinaEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.oficinaService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Obtiene una Oficina',
    type: OficinaEntity,
  })
  async getOne(@Param('id') id: number): Promise<OficinaEntity> {
    return await this.oficinaService.getOne(id);
  }

  @Post('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Crea un Tipo de Juzgado' })
  @ApiResponse({
    status: 201,
    description: 'La Oficina fue creada correctamente.',
    type: OficinaEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() OficinaEntity: OficinaEntity): Promise<OficinaEntity> {
    const created = await this.oficinaService.save(OficinaEntity);
    HeaderUtil.addEntityCreatedHeaders(req.res, 'OficinaEntity', created.idOficina);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Actualiza un Tipo de Juzgado' })
  @ApiResponse({
    status: 200,
    description: 'La Oficina fue actualizada correctamente.',
    type: OficinaEntity,
  })
  async put(@Req() req: Request, @Body() OficinaEntity: OficinaEntity): Promise<OficinaEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'OficinaEntity', OficinaEntity.idOficina);
    return await this.oficinaService.update(OficinaEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Actualiza un Tipo de Juzgado por ID' })
  @ApiResponse({
    status: 200,
    description: 'La Oficina fue actualizada correctamente.',
    type: OficinaEntity,
  })
  async putId(@Req() req: Request, @Body() postulante: OficinaEntity): Promise<OficinaEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'OficinaEntity', postulante.idOficina);
    return await this.oficinaService.update(postulante);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Eliminar un Tipo de Juzgado' })
  @ApiResponse({
    status: 204,
    description: 'La Oficina fue Eliminada correctamente.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: number): Promise<void> {
    HeaderUtil.addEntityDeletedHeaders(req.res, 'OficinaEntity', id);
    return await this.oficinaService.delete(id);
  }
}
