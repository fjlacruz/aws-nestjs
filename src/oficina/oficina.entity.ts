import { Entity, Column, OneToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';
import { InstitucionesEntity } from '../tramites/entity/instituciones.entity';
import { ComunasEntity } from '../comunas/entity/comunas.entity';

@Entity('Oficinas')
export class OficinaEntity {
  @PrimaryGeneratedColumn()
  idOficina: number;

  @Column()
  idInstitucion: number;

  @OneToOne(() => InstitucionesEntity, instituciones => instituciones.idInstitucion)
  @JoinColumn({ name: 'idInstitucion' })
  readonly institucion: InstitucionesEntity;

  @Column()
  idComuna: number;

  @OneToOne(() => ComunasEntity, comunas => comunas.idComuna)
  @JoinColumn({ name: 'idComuna' })
  readonly comuna: ComunasEntity;

  @Column()
  Nombre: string;

  @Column()
  Direccion: string;

  @Column()
  Descripcion: string;

  @Column()
  RUT?: number;

  @Column()
  DV?: string;

  @Column()
  fechaCreacion?: Date;

  @Column()
  activo?: boolean;

  @Column()
  creadoPor?: number;
}
