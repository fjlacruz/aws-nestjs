import { Module } from '@nestjs/common';
import { OficinaService } from './oficina.service';
import { OficinaController } from './oficina.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OficinaEntity } from './oficina.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OficinaEntity])],
  providers: [OficinaService],
  exports: [OficinaService],
  controllers: [OficinaController],
})
export class OficinaModule {}
