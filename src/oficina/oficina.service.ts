import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { OficinaEntity } from './oficina.entity';
import { OficinasEntity } from '../tramites/entity/oficinas.entity';
import { TipoJuzgadoEntity } from '../tipo-juzgado/tipo-juzgado.entity';
import { ComunasEntity } from '../comunas/entity/comunas.entity';
import { RegionesEntity } from '../regiones/entity/regiones.entity';
import { ConductoresEntity } from '../conductor/entity/conductor.entity';
import { InstitucionesEntity } from '../tramites/entity/instituciones.entity';
import { TipoInstitucionEntity } from '../tipo-institucion/entity/tipo-institucion.entity';

@Injectable()
export class OficinaService {
  constructor(@InjectRepository(OficinaEntity) private readonly oficinaEntityRepository: Repository<OficinaEntity>) {}

  async findAndCount(options: FindManyOptions<OficinaEntity>): Promise<[OficinaEntity[], number]> {
    const resultList = await this.oficinaEntityRepository.findAndCount(options);
    const banks: OficinaEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(bank => banks.push(bank));
      resultList[0] = banks;
    }
    return resultList;
  }

  async getAll() {
    return await this.oficinaEntityRepository.find();
  }

  async getAllJpl() {
    const resoluciones = this.oficinaEntityRepository
      .createQueryBuilder('Oficinas')
      .leftJoinAndMapMany(
        'Oficinas.idInstitucion',
        InstitucionesEntity,
        'institucion',
        'Oficinas.idInstitucion = institucion.idInstitucion'
      )
      .leftJoinAndMapMany(
        'institucion.idTipoInstitucion',
        TipoInstitucionEntity,
        'tipoinstitucion',
        'institucion.idTipoInstitucion = tipoinstitucion.idTipoInstitucion'
      );
    resoluciones.andWhere('tipoinstitucion.idTipoInstitucion = 2');
    return await resoluciones.getMany();
  }

  async getOne(idTipoInstitucion: number) {
    const data = await this.oficinaEntityRepository.findOne(idTipoInstitucion);
    if (!data) throw new NotFoundException('Tipo Resultado Cola Exam Practicos dont exist');

    return data;
  }

  async findOne(options: FindManyOptions<OficinaEntity>): Promise<OficinaEntity> {
    return await this.oficinaEntityRepository.findOne(options);
  }

  async update(bank: OficinaEntity): Promise<OficinaEntity | undefined> {
    return await this.oficinaEntityRepository.save(bank);
  }

  async save(data: OficinaEntity): Promise<OficinaEntity> {
    return await this.oficinaEntityRepository.save(data);
  }

  async delete(idOficina: number) {
    await this.oficinaEntityRepository.delete(idOficina);
    const entityFind = await this.oficinaEntityRepository.findOne({
      where: {
        idOficina: idOficina,
      },
    });
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
