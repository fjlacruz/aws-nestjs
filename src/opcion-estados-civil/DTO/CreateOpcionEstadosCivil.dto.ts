import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateOpcionEstadosCivilDTO {
    
    @ApiPropertyOptional()
    idOpcionEstadosCivil:number;

    @ApiPropertyOptional()
    Nombre:string;

    @ApiPropertyOptional()
    Descripcion:string;

    @ApiPropertyOptional()
    created_at:Date;

    @ApiPropertyOptional()
    update_at:Date
}
