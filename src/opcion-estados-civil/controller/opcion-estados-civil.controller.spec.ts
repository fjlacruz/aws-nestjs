import { Test, TestingModule } from '@nestjs/testing';
import { OpcionEstadosCivilController } from './opcion-estados-civil.controller';

describe('OpcionEstadosCivilController', () => {
  let controller: OpcionEstadosCivilController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OpcionEstadosCivilController],
    }).compile();

    controller = module.get<OpcionEstadosCivilController>(OpcionEstadosCivilController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
