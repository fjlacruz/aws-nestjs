import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CreateOpcionEstadosCivilDTO } from '../DTO/CreateOpcionEstadosCivil.dto';
import { OpcionEstadosCivilService } from '../services/opcion-estados-civil.service';

@ApiTags('Opcion-estados-civil')
@Controller('opcion-estados-civil')
export class OpcionEstadosCivilController {


    constructor(private readonly opcionEstadosCivilService: OpcionEstadosCivilService) { }

    @Get('opcionEstadosCivil')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las Opcion Estados Civiles' })
    async getComunas() {
        const data = await this.opcionEstadosCivilService.getOpcionEstadosCiviles();
        return { data };
    }

    @Get('opcionEstadosCivilById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Opcion Estados Civil por Id' })
    async comunaById(@Param('id') id: number) {
        const data = await this.opcionEstadosCivilService.getOpcionEstadosCivil(id);
        return { data };
    }

    @Get('opcionesEstadosCivilListado')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las Opcion Estados Civiles en Respuesta' })
    async getOpcionesECListado() {
        const data = await this.opcionEstadosCivilService.getOpcionesECListado();
        return { data };
    }

    @Get('opcionEstadosCivilDTO/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Opcion Estados Civil por Id en Respuesta' })
    async getOpcionECDTO(@Param('id') id: number) {
        const data = await this.opcionEstadosCivilService.getOpcionECDTO(id);
        return { data };
    }

    @Patch('opcionEstadosCivilUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de una Opcion Estados Civiles' })
    async comunaUpdate(@Param('id') id: number, @Body() data: CreateOpcionEstadosCivilDTO) {
        return await this.opcionEstadosCivilService.update(id, data);
    }

    @Post('crearOpcionEstadosCivil')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una nueva Opcion Estados Civiles' })
    async addComuna(
        @Body() createOpcionEstadosCivilDTO: CreateOpcionEstadosCivilDTO) {
        const data = await this.opcionEstadosCivilService.create(createOpcionEstadosCivilDTO);
        return {data};
    }

}
