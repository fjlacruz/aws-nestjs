import { ApiProperty } from "@nestjs/swagger";
import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne, OneToMany} from "typeorm";


@Entity('OpcionEstadosCivil')
export class OpcionEstadosCivilEntity {

    @PrimaryColumn()
    idOpcionEstadosCivil:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;

    @Column()
    created_at:Date;

    @Column()
    update_at:Date

    @ApiProperty()
    @OneToOne(() => User, user => user.opcionEstadoCivil)
    readonly user: User;
    
    @ApiProperty()
    @OneToMany(() => PostulantesEntity, postulantes => postulantes.opcionEstadosCivil)
    readonly postulante: PostulantesEntity[];
}
