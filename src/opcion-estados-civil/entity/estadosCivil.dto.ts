export class EstadosCivilDTO {
    idOpcionEstadosCivil: number;
    nombre: string;
    descripcion: string;
}