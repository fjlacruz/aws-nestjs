import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OpcionEstadosCivilController } from './controller/opcion-estados-civil.controller';
import { OpcionEstadosCivilEntity } from './entity/OpcionEstadosCivil.entity';
import { OpcionEstadosCivilService } from './services/opcion-estados-civil.service';

@Module({
  imports: [TypeOrmModule.forFeature([OpcionEstadosCivilEntity])],
  providers: [OpcionEstadosCivilService],
  exports: [OpcionEstadosCivilService],
  controllers: [OpcionEstadosCivilController]
})
export class OpcionEstadosCivilModule {}
