import { Test, TestingModule } from '@nestjs/testing';
import { OpcionEstadosCivilService } from './opcion-estados-civil.service';

describe('OpcionEstadosCivilService', () => {
  let service: OpcionEstadosCivilService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OpcionEstadosCivilService],
    }).compile();

    service = module.get<OpcionEstadosCivilService>(OpcionEstadosCivilService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
