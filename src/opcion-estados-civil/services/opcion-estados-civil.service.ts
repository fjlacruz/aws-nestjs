import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { Repository, UpdateResult } from 'typeorm';
import {getConnection} from "typeorm";
import { CreateOpcionEstadosCivilDTO } from '../DTO/CreateOpcionEstadosCivil.dto';
import { EstadosCivilDTO } from '../entity/estadosCivil.dto';
import { OpcionEstadosCivilEntity } from '../entity/OpcionEstadosCivil.entity';

@Injectable()
export class OpcionEstadosCivilService {

    constructor(@InjectRepository(OpcionEstadosCivilEntity) private readonly opcionEstadosCivilEntity: Repository<OpcionEstadosCivilEntity>) { }

    async getOpcionEstadosCiviles() {
        return await this.opcionEstadosCivilEntity.find();
    }

    async getOpcionEstadosCivil(id:number){
        const region= await this.opcionEstadosCivilEntity.findOne(id);
        if (!region)throw new NotFoundException("Opcion Estado Civil dont exist");
        
        return region;
    }

    async getOpcionesECListado(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoEstadosCiviles: OpcionEstadosCivilEntity[] = [];
        let resultadosDTO: EstadosCivilDTO[] = [];

        try {
            resultadoEstadosCiviles = await this.opcionEstadosCivilEntity.find();

            if (Array.isArray(resultadoEstadosCiviles) && resultadoEstadosCiviles.length) {

                resultadoEstadosCiviles.forEach(item => {
                    let nuevoElemento: EstadosCivilDTO = {
                        idOpcionEstadosCivil: item.idOpcionEstadosCivil,
                        nombre: item.Nombre,
                        descripcion: item.Descripcion
                    }

                    resultadosDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Estados Civiles obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Estados Civiles';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Estados Civiles';
        }

        return resultado;
    }

    async getOpcionECDTO(id: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoEstadosCiviles: OpcionEstadosCivilEntity = null;
        let resultadoDTO: EstadosCivilDTO = null;

        try {
            resultadoEstadosCiviles = await this.opcionEstadosCivilEntity.findOne(id);

            if (resultadoEstadosCiviles != null && resultadoEstadosCiviles != undefined) {
                resultadoDTO = {
                    idOpcionEstadosCivil: resultadoEstadosCiviles.idOpcionEstadosCivil,
                    nombre: resultadoEstadosCiviles.Nombre,
                    descripcion: resultadoEstadosCiviles.Descripcion
                }
            }

            if (resultadoDTO != null && resultadoDTO != undefined) {
                resultado.Respuesta = resultadoDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Estado Civil obtenido correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontró el Estado Civil';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Estado Civil';
        }

        return resultado;
    }

    async update(idOpcionEstadosCivil: number, data: Partial<CreateOpcionEstadosCivilDTO>) {
        await this.opcionEstadosCivilEntity.update({ idOpcionEstadosCivil }, data);
        return await this.opcionEstadosCivilEntity.findOne({ idOpcionEstadosCivil });
      }
      
    async create(data: CreateOpcionEstadosCivilDTO): Promise<OpcionEstadosCivilEntity> {
        return await this.opcionEstadosCivilEntity.save(data);
    }
}
