import { ApiPropertyOptional } from "@nestjs/swagger";

export class createOpcionNacionalidades {

    @ApiPropertyOptional()
    idnacionalidad: number;

    @ApiPropertyOptional()
    nombre: string;
    
    @ApiPropertyOptional()
    pais: string;
}