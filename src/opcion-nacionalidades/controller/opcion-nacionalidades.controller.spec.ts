import { Test, TestingModule } from '@nestjs/testing';
import { OpcionNacionalidadesController } from './opcion-nacionalidades.controller';

describe('OpcionesNacionalidadesController', () => {
  let controller: OpcionNacionalidadesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OpcionNacionalidadesController],
    }).compile();

    controller = module.get<OpcionNacionalidadesController>(OpcionNacionalidadesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
