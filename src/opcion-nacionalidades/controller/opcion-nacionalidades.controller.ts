import { Controller, Get } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { OpcionNacionalidadesService } from '../services/opcion-nacionalidades.service';

@ApiTags('Opcion-Nacionalidades')
@Controller('Opcion-Nacionalidades')
export class OpcionNacionalidadesController
{
    constructor( private nacionalidadService: OpcionNacionalidadesService )
    {}

    @Get('/opcion-nacionalidades')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos las opciones de nacionalidad' })
    
    async getAll()
    {
        return this.nacionalidadService.getAll().then(data =>
        {
            return { code: 200, data };
        });
    }

}
