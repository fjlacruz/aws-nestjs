import { ApiProperty } from "@nestjs/swagger";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne} from "typeorm";

@Entity('opcionesnacionalidad')
export class OpcionNacionalidadesEntity {

    @PrimaryGeneratedColumn()
    idnacionalidad: number;

    @Column()
    nombre: string;
    
    @Column()
    pais: string;

    @ApiProperty()
    @OneToOne(() => User, user => user.opcionNacionalidad)
    readonly user: User;

}