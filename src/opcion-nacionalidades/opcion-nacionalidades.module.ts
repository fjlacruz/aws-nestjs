import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OpcionNacionalidadesController } from './controller/opcion-nacionalidades.controller';
import { OpcionNacionalidadesEntity } from './entity/opcion-nacionalidades.entity';
import { OpcionNacionalidadesService } from './services/opcion-nacionalidades.service';

@Module({
  imports: [TypeOrmModule.forFeature([OpcionNacionalidadesEntity])],
  providers: [OpcionNacionalidadesService],
  exports: [OpcionNacionalidadesService],
  controllers: [OpcionNacionalidadesController]
})
export class OpcionNacionalidadesModule {}
