import { Test, TestingModule } from '@nestjs/testing';
import { OpcionNacionalidadesService } from './opcion-nacionalidades.service';

describe('OpcionNacionalidadesService', () => {
  let service: OpcionNacionalidadesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OpcionNacionalidadesService],
    }).compile();

    service = module.get<OpcionNacionalidadesService>(OpcionNacionalidadesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
