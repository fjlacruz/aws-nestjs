import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { OpcionNacionalidadesEntity } from '../entity/opcion-nacionalidades.entity';

@Injectable()
export class OpcionNacionalidadesService {
    constructor(@InjectRepository(OpcionNacionalidadesEntity) private readonly opcionNacionalidadesRespository: Repository<OpcionNacionalidadesEntity>) { }

    async getAll(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoOpcionNacionalidades: OpcionNacionalidadesEntity[] = [];

        try {
            resultadoOpcionNacionalidades = await this.opcionNacionalidadesRespository.find();

            if (Array.isArray(resultadoOpcionNacionalidades) && resultadoOpcionNacionalidades.length) {
                resultado.Respuesta = resultadoOpcionNacionalidades;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Opciones obtenidas correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron opciones';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo opciones de Sexo';
        }

        return resultado;
    }
}
