import { Test, TestingModule } from '@nestjs/testing';
import { OpcionesNivelEducacionalController } from './opciones-nivel-educacional.controller';

describe('OpcionesNivelEducacionalController', () => {
  let controller: OpcionesNivelEducacionalController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OpcionesNivelEducacionalController],
    }).compile();

    controller = module.get<OpcionesNivelEducacionalController>(OpcionesNivelEducacionalController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
