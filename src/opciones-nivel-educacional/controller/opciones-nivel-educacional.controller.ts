import { Controller, Get } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { OpcionesNivelEducacionalService } from '../services/opciones-nivel-educacional.service';

@ApiTags('Opciones-Nivel-Educacional')
@Controller('opciones-nivel-educacional')
export class OpcionesNivelEducacionalController {

    constructor(private readonly nivelService: OpcionesNivelEducacionalService) { }

    @Get('allNivelEducacional')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos las Opciones de Nivel Educacional' })
    async getNivelEducacional() {
        const data = await this.nivelService.getNiveles();
        return { data };
    }
}
