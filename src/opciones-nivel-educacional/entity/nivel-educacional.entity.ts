import { ApiProperty } from "@nestjs/swagger";
import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";

@Entity('OpcionesNivelEducacional')
export class OpcionesNivelEducacional {
  @PrimaryGeneratedColumn()
  idOpNivelEducacional: number;

  @Column()
  Nombre: string;

  @Column()
  Descripcion: string;

  @ApiProperty()
  @OneToMany(() => PostulantesEntity, postulantes => postulantes.opcionesNivelEducacional)
  readonly postulante: PostulantesEntity[];
}
