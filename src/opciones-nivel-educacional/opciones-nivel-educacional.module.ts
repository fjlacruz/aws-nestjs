import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OpcionesNivelEducacionalController } from './controller/opciones-nivel-educacional.controller';
import { OpcionesNivelEducacional } from './entity/nivel-educacional.entity';
import { OpcionesNivelEducacionalService } from './services/opciones-nivel-educacional.service';

@Module({
  imports: [TypeOrmModule.forFeature([OpcionesNivelEducacional])],
  controllers: [OpcionesNivelEducacionalController],
  exports: [OpcionesNivelEducacionalService],
  providers: [OpcionesNivelEducacionalService]
})
export class OpcionesNivelEducacionalModule {}
