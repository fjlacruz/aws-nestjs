import { Test, TestingModule } from '@nestjs/testing';
import { OpcionesNivelEducacionalService } from './opciones-nivel-educacional.service';

describe('OpcionesNivelEducacionalService', () => {
  let service: OpcionesNivelEducacionalService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OpcionesNivelEducacionalService],
    }).compile();

    service = module.get<OpcionesNivelEducacionalService>(OpcionesNivelEducacionalService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
