import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OpcionesNivelEducacional } from '../entity/nivel-educacional.entity';

@Injectable()
export class OpcionesNivelEducacionalService {

    constructor(@InjectRepository(OpcionesNivelEducacional) private readonly nivelRespository: Repository<OpcionesNivelEducacional>) { }

    async getNiveles() {
        return await this.nivelRespository.find();
    }
}
