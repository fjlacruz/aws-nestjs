import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateOpcionesSexo {
    @ApiPropertyOptional()
    idOpcionSexo: number;

    @ApiPropertyOptional()
    nombre: string;

    @ApiPropertyOptional()
    descripcion: string;
}
