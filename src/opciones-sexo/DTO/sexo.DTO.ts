export class OpcionSexoDTO
{
    idOpcionSexo: number;
    nombre: string;
    descripcion: string;
}