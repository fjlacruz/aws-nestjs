import { Test, TestingModule } from '@nestjs/testing';
import { OpcionesSexoController } from './opciones-sexo.controller';

describe('OpcionesSexoController', () => {
  let controller: OpcionesSexoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OpcionesSexoController],
    }).compile();

    controller = module.get<OpcionesSexoController>(OpcionesSexoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
