import { Controller, Get } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { OpcionesSexoService } from '../service/opciones-sexo.service';

@ApiTags('Opciones-sexo')
@Controller('opciones-sexo')
export class OpcionesSexoController
{
    constructor( private sexoService: OpcionesSexoService )
    {}

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos las opciones de sexo' })
    async getAll()
    {
        return this.sexoService.getAll().then(  data =>
        {
            return { code: 200, data };
        });
    }

    @Get('/opciones-sexo')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos las opciones de sexo en un listado como respuesta' })
    async opcionesSexoListado()
    {
        return this.sexoService.opcionesSexoListado().then(  data =>
        {
            return { code: 200, data };
        });
    }

}
