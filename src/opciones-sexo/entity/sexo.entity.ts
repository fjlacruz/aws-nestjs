import { ApiProperty } from "@nestjs/swagger";
import { PapelSeguridadEntity } from "src/papel-seguridad/entity/papel-seguridad.entity";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, PrimaryColumn} from "typeorm";

@Entity('OpcionesSexo')
export class OpcionSexoEntity
{
    @PrimaryColumn()
    idOpcionSexo: number;

    @Column()
    Nombre: string;

    @Column()
    Descripcion: string;

    @ApiProperty()
    @OneToOne(() => User, user => user.opcionSexo)
    readonly user: User;

    @ApiProperty()
    @OneToOne(() => OficinasEntity, oficinas => oficinas.instituciones)
    readonly oficinas: OficinasEntity;

    @ApiProperty()
    @OneToMany(() => PapelSeguridadEntity, papelSeguridad => papelSeguridad.instituciones)
    readonly papelSeguridad: PapelSeguridadEntity[];

    @ApiProperty()
    @OneToMany(() => PostulantesEntity, postulantes => postulantes.opcionSexo)
    readonly postulante: PostulantesEntity[];
}