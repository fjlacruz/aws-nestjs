import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OpcionesSexoController } from './controller/opciones-sexo.controller';
import { OpcionSexoEntity } from './entity/sexo.entity';
import { OpcionesSexoService } from './service/opciones-sexo.service';

@Module({
  imports: [TypeOrmModule.forFeature([OpcionSexoEntity])],
  controllers: [OpcionesSexoController],
  providers: [OpcionesSexoService],
  exports: [OpcionesSexoService],
})
export class OpcionesSexoModule {}
