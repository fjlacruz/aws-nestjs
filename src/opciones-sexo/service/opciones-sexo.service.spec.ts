import { Test, TestingModule } from '@nestjs/testing';
import { OpcionesSexoService } from './opciones-sexo.service';

describe('OpcionesSexoService', () => {
  let service: OpcionesSexoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OpcionesSexoService],
    }).compile();

    service = module.get<OpcionesSexoService>(OpcionesSexoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
