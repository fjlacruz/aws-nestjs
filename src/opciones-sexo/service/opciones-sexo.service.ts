import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { OpcionSexoEntity } from '../entity/sexo.entity';
import { OpcionSexoDTO } from '../DTO/sexo.DTO';

@Injectable()
export class OpcionesSexoService {
    constructor(@InjectRepository(OpcionSexoEntity) private readonly sexoRespository: Repository<OpcionSexoEntity>) { }

    async getAll()
    {
        return await this.sexoRespository.find();
    }

    async opcionesSexoListado(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoOpcionesSexo: OpcionSexoEntity[] = [];
        let opcionesSexoDTO: OpcionSexoDTO[] = [];

        try {
            resultadoOpcionesSexo = await this.sexoRespository.find();

            if (Array.isArray(resultadoOpcionesSexo) && resultadoOpcionesSexo.length) {

                resultadoOpcionesSexo.forEach(opcion => {
                    let nuevoElemento: OpcionSexoDTO = {
                        idOpcionSexo: opcion.idOpcionSexo,
                        nombre: opcion.Nombre,
                        descripcion: opcion.Descripcion,
                    }

                    opcionesSexoDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(opcionesSexoDTO) && opcionesSexoDTO.length) {
                resultado.Respuesta = opcionesSexoDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Opciones obtenidas correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron opciones';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo opciones de Sexo';
        }

        return resultado;
    }
}
