import { Body, Controller, Delete, Get, Param, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { OportunidadService } from './oportunidad.service';
import { Request } from 'express';
import { Page, PageRequest } from '../oportunidad/pagination-oportunidad.entity';
import { HeaderUtil } from '../base/base/header-util';
import { AuthGuard } from '@nestjs/passport';
import { Post as PostMethod } from '@nestjs/common/decorators/http/request-mapping.decorator';
import { OportunidadEntity } from './oportunidad.entity';

@ApiTags('Oportunidades')
@Controller('oportunidades')
export class OportunidadController {
  constructor(private readonly oportunidadService: OportunidadService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all records',
    type: OportunidadEntity,
  })
  async getAll(@Req() req: Request): Promise<OportunidadEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.oportunidadService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una Sentencia por Id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: OportunidadEntity,
  })
  async getOne(@Param('id') id: string): Promise<OportunidadEntity> {
    return await this.oportunidadService.findById(id);
  }

  @PostMethod('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea una nueva Sentencia' })
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    type: OportunidadEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() oportunidadEntity: OportunidadEntity): Promise<OportunidadEntity> {
    const created = await this.oportunidadService.save(oportunidadEntity);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: OportunidadEntity,
  })
  async put(@Req() req: Request, @Body() oportunidadEntity: OportunidadEntity): Promise<OportunidadEntity> {
    return await this.oportunidadService.update(oportunidadEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: OportunidadEntity,
  })
  async putId(@Req() req: Request, @Body() oportunidadEntity: OportunidadEntity): Promise<OportunidadEntity> {
    return await this.oportunidadService.update(oportunidadEntity);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Elimina una Sentencia' })
  @ApiResponse({
    status: 204,
    description: 'The record has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
    return await this.oportunidadService.deleteById(id);
  }
}
