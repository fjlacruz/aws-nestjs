import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { ColaExaminacion } from 'src/tramites/entity/cola-examinacion.entity';

@Entity('Oportunidades')
export class OportunidadEntity {
  @PrimaryGeneratedColumn()
  idOportunidad: number;

  @Column()
  idSolicitudesProrrogaOportunidad: number;

  @Column()
  @ApiModelProperty()
  idColaExaminacion: number;
  @ManyToOne(() => ColaExaminacion, colaExaminacion => colaExaminacion.oportunidadExaminacion)
  @JoinColumn({ name: 'idColaExaminacion' })
  readonly colaExaminacion: ColaExaminacion;

  @Column()
  @ApiModelProperty()
  ingresadoPor: number;

  @Column()
  @ApiModelProperty()
  fechaIngreso: Date;

  @Column()
  @ApiModelProperty()
  observacion: string;

  @Column()
  @ApiModelProperty()
  oportunidadAprobada: boolean;

  @Column()
  @ApiModelProperty()
  aprobadoPor: number;

  @Column()
  @ApiModelProperty()
  fechaAprobacion: Date;

  @Column()
  @ApiModelProperty()
  created_at: Date;

  @Column()
  @ApiModelProperty()
  updated_at: Date;
}
