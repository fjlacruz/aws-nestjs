import { Module } from '@nestjs/common';
import { OportunidadController } from './oportunidad.controller';
import { OportunidadService } from './oportunidad.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OportunidadEntity } from './oportunidad.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OportunidadEntity])],
  controllers: [OportunidadController],
  providers: [OportunidadService],
  exports: [OportunidadService],
})
export class OportunidadModule {}
