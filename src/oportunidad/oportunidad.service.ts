import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { OportunidadEntity } from './oportunidad.entity';

const relationshipNames = [];

@Injectable()
export class OportunidadService {
  constructor(@InjectRepository(OportunidadEntity) private readonly oportunidadEntityRepository: Repository<OportunidadEntity>) {}

  async findById(id: string): Promise<OportunidadEntity | undefined> {
    const options = { relations: relationshipNames };
    const result = await this.oportunidadEntityRepository.findOne(id, options);
    return result;
  }

  async findByfields(options: FindOneOptions<OportunidadEntity>): Promise<OportunidadEntity | undefined> {
    const result = await this.oportunidadEntityRepository.findOne(options);
    return result;
  }

  async findAndCount(options: FindManyOptions<OportunidadEntity>): Promise<[OportunidadEntity[], number]> {
    options.relations = relationshipNames;
    const resultList = await this.oportunidadEntityRepository.findAndCount(options);
    const oportunidadEntities: OportunidadEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(oportunidad => oportunidadEntities.push(oportunidad));
      resultList[0] = oportunidadEntities;
    }
    return resultList;
  }

  async save(oportunidadEntity: OportunidadEntity): Promise<OportunidadEntity | undefined> {
    const entity = oportunidadEntity;
    const result = await this.oportunidadEntityRepository.save(entity);
    return result;
  }

  async update(oportunidadEntity: OportunidadEntity): Promise<OportunidadEntity | undefined> {
    const entity = oportunidadEntity;
    const result = await this.oportunidadEntityRepository.save(entity);
    return result;
  }

  async deleteById(id: string): Promise<void | undefined> {
    await this.oportunidadEntityRepository.delete(id);
    const entityFind = await this.findById(id);
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
