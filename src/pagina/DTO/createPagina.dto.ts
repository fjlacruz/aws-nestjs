import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreatePaginaDto {
    
    @ApiPropertyOptional()
    idPagina: number;
    @ApiPropertyOptional()
    Titulo: string;
    @ApiPropertyOptional()
    Descripcion: string;
    // @ApiPropertyOptional()
    // updated_at: Date;
    // @ApiPropertyOptional()
    // idUsuario: number;
}
