import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreatePaginaDto } from '../DTO/createPagina.dto';
import { PaginasService } from '../services/paginas.services.service';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';

@ApiTags('Paginas')
@Controller('paginas')
export class PaginaController {

    constructor(private readonly paginasService: PaginasService) { }
    @Get('paginas')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos las Paginas' })
    async getSolicitudes() {
        const data = await this.paginasService.getPaginas();
        return { data };
    }

    @Get('paginaById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Pagina por Id' })
    async getPaginaById(@Param('id') id: number) {
        const data = await this.paginasService.getPagina(id);
        return { data };
    }

    @Patch('paginasUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos una Pagina' })
    async uppdateUser(@Param('id') id: number, @Body() data: Partial<CreatePaginaDto>) {
        return await this.paginasService.update(id, data);
  }

    @Post('creaPagina')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una nueva Pagina' })
    async addPaginas(
        @Body() dto: CreatePaginaDto) {
        const data = await this.paginasService.create(dto);
        return {data};
    }
}
