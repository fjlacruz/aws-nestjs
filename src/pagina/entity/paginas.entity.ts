import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('Paginas')
export class Paginas {

    @PrimaryColumn()
    idPagina: number;
    
    @Column()
    Titulo: string;
    
    @Column()
    Descripcion: string;
    
    @Column()
    updated_at: Date;
    
    @Column()
    idUsuario: number;

}