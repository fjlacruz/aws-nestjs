import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Paginas } from './entity/paginas.entity';
import { PaginasService } from './services/paginas.services.service';
import { PaginaController } from './controller/paginas.controller.controller';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { AuthService } from 'src/auth/auth.service';


@Module({
  imports: [TypeOrmModule.forFeature([Paginas, User, RolesUsuarios])],
  providers: [PaginasService, AuthService],
  exports: [PaginasService],
  controllers: [PaginaController]
})
export class PaginaModule {}
