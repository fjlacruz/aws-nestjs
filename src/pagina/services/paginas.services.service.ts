import { Injectable, NotFoundException } from '@nestjs/common';
import { Paginas } from '../entity/paginas.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { CreatePaginaDto } from '../DTO/createPagina.dto';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class PaginasService {
    
    constructor
    (
        private readonly authService: AuthService, 

        @InjectRepository(Paginas) 
        private readonly paginaRespository: Repository<Paginas>
    )
    {}

    async getPaginas() {
        return await this.paginaRespository.find();
    }


    async getPagina(id:number){
        const pagina= await this.paginaRespository.findOne(id);
        if (!pagina)throw new NotFoundException("pagina dont exist");
        
        return pagina;
    }

    async update(idPagina: number, data: Partial<CreatePaginaDto>) {
        await this.paginaRespository.update({ idPagina }, data);
        return await this.paginaRespository.findOne({ idPagina });
      }
      
    async create(dto: CreatePaginaDto): Promise<Paginas> 
    {
        const idUsuario  = (await this.authService.usuario()).idUsuario;
        const updated_at = new Date();
        const data       = { ...dto, idUsuario, updated_at }
        
        return await this.paginaRespository.save( data );
    }

}
