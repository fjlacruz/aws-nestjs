import { ApiProperty } from "@nestjs/swagger";
import { InstitucionesEntity } from "src/tramites/entity/instituciones.entity";
import { DocumentosLicencia } from "src/registro-pago/entity/documentos-licencia.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne} from "typeorm";
import { LotePapelSeguridadEntity } from "src/folio/entity/lotePapelSeguridad.entity";

@Entity('PapelSeguridad')
export class PapelSeguridadEntity {

    @PrimaryGeneratedColumn()
    idPapelSeguridad: number;

    @Column()
    idInstitucion: number;
    @ManyToOne(() => InstitucionesEntity, instituciones => instituciones.papelSeguridad)
    @JoinColumn({ name: 'idInstitucion' })
    readonly instituciones: InstitucionesEntity;

    @Column()
    idLotePapelSeguridad: number;
    @ManyToOne(() => LotePapelSeguridadEntity, lotepapelseguridad => lotepapelseguridad.papelSeguridad)
    @JoinColumn({ name: 'idLotePapelSeguridad' })
    readonly lotepapelseguridad: LotePapelSeguridadEntity;    
        
    @Column()
    LetrasFolio: string;

    @Column()
    Folio: string;

    @Column()
    Asignado: boolean;
    
    @ApiProperty()
    @OneToOne(() => DocumentosLicencia, documentosLicencia => documentosLicencia.papelSeguridad)
    readonly documentosLicencia: DocumentosLicencia;
}