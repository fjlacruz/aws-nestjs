import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateParametroDto {

    @ApiPropertyOptional()
    idParametro: number;
    @ApiPropertyOptional()
    Param: string;
    @ApiPropertyOptional()
    Value:string;
    @ApiPropertyOptional()
    Descripcion:string;
}