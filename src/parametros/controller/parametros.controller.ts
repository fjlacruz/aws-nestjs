import { Body, Controller, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateParametroDto } from '../DTO/createParametro.dto';
import { ParametrosService } from '../services/parametros.service';

@ApiTags('Parametros-General')
@Controller('parametros')
export class ParametrosController {

    constructor(private readonly parametroService: ParametrosService) { }

    @UseGuards(JwtAuthGuard)
    @Get('allParametros')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Parametros' })
    async getParametros() {
        const data = await this.parametroService.getParametros();
        return { data };
    }

    @Get('parametroByParam/:param')
    @ApiOperation({ summary: 'Servicio que devuelve un parametro por param' })
    async getParametroByParam(@Param('param') param) {
        const data = await this.parametroService.getParametrosByParam(param);
        return { data };
    }

    @Get('parametroById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Parametro por Id' })
    async getParametro(@Param('id') id: number) {
        const data = await this.parametroService.getParametro(id);
        return { data };
    }

    @Post('creaParametro')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Parametro' })
    async addPostulante(
        @Body() createParametroDto: CreateParametroDto) {

        const data = await this.parametroService.create(createParametroDto);
        return { data };

    }

    @Patch('parametrosUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de un Parametro' })
    async uppdateParametro(@Param('id') id: number, @Body() data: Partial<CreateParametroDto>) {
        return await this.parametroService.update(id, data);
    }
}
