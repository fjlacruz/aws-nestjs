import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('ParametrosGeneral')
export class ParametrosGeneral {

    @PrimaryGeneratedColumn()
    idParametro: number;
    
    @Column()
    Param: string;

    @Column()
    Value:string;

    @Column()
    Descripcion:string;

    @Column()
    tipoDato:number;

    @Column()
    ValueInt:number;

    @Column()
    ValueBoolean:boolean;
}