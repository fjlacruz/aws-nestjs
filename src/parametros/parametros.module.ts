import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParametrosController } from './controller/parametros.controller';
import { ParametrosGeneral } from './entity/parametros.entity';
import { ParametrosService } from './services/parametros.service';

@Module({
  imports: [TypeOrmModule.forFeature([ParametrosGeneral])],
  controllers: [ParametrosController],
  exports: [ParametrosService],
  providers: [ParametrosService]
})
export class ParametrosModule {}
