import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateParametroDto } from '../DTO/createParametro.dto';
import { ParametrosGeneral } from '../entity/parametros.entity';

@Injectable()
export class ParametrosService {

    constructor(@InjectRepository(ParametrosGeneral) private readonly parametroRespository: Repository<ParametrosGeneral>) { }

    async getParametros() {
        return await this.parametroRespository.find();
    }

    async getParametrosByParam(param) {
        const parametro = await this.parametroRespository
            .createQueryBuilder('ParametrosGeneral')
            .where("ParametrosGeneral.Param like :parametro",{parametro:param}).getOne();
            console.log(parametro)
            return parametro
    }

    async getParametro(id: number) {
        const parametro = await this.parametroRespository.findOne(id);
        if (!parametro) throw new NotFoundException("parametro dont exist");

        return parametro;
    }

    async create(createParametroDto: CreateParametroDto): Promise<ParametrosGeneral> {
        return await this.parametroRespository.save(createParametroDto);
    }

    async update(idParametro: number, data: Partial<CreateParametroDto>) {
        await this.parametroRespository.update({ idParametro }, data);
        return await this.parametroRespository.findOne({ idParametro });
    }

}
