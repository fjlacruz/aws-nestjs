import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocsRolesUsuarioEntity } from "src/documentos-usuarios/entity/DocsRolesUsuario.entity";
import { RolesUsuarios } from "src/roles-usuarios/entity/roles-usuarios.entity";

export class PermisosDto {

    @ApiPropertyOptional()
    idPermiso: number;

    @ApiPropertyOptional()
    Nombre: string;

    @ApiPropertyOptional()
    Descripcion: string;

    @ApiPropertyOptional()
    Codigo: string;

    @ApiPropertyOptional()
    rolesUsuarios: RolesUsuarios;
}