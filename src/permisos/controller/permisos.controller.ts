import {Body, Controller, Get, Param, Post, Req} from '@nestjs/common';
import {ApiBearerAuth, ApiOperation, ApiTags} from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosService } from '../service/permisos.service';

@ApiTags('Permisos')
@Controller('permisos')
@ApiBearerAuth()
export class PermisosController {

    constructor(private readonly permisosService: PermisosService, private readonly authService: AuthService ) { }
    @Get('/getPermisos')
    async getPermisos(@Req() req: any) {

        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaAdminitracionRolesYPermisos
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

        const data = await this.permisosService.getPermisos();
            return { data };

        } else {
            return { data: validarPermisos };
        }
    }

    @Post('/getPermisosPaginados')
    async getPermisosPaginados(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {


            const data = await this.permisosService.getPermisosPaginados(req, paginacionArgs);
            return { data };

    }

    @Get('getPermisosByIdUsuario/:idUsuario')
    @ApiOperation({summary: 'Servicio que devuelve la información de una pregunta'})
    async getPermisosByIdUsuario(@Param('idUsuario') idUsuario: number) {
        const data = await this.permisosService.getPermisosByIdUsuario(idUsuario);
        return data;
    }
}
