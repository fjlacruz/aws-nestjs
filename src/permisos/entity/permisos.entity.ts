import { ApiProperty } from "@nestjs/swagger";
import { DocsRolesUsuarioEntity } from "src/documentos-usuarios/entity/DocsRolesUsuario.entity";
import { GrupoPermisoXPermisos } from "src/grupo-permisos/entity/grupo-permiso-permisos.entity";
import { PermisosMenuEntity } from "src/menu-sgl/entity/permisos-menu.entity";
import { RolesUsuarios } from "src/roles-usuarios/entity/roles-usuarios.entity";
import { RolPermiso } from "src/rolPermiso/entity/rolPermiso.entity";
import { InstitucionesEntity } from "src/tramites/entity/instituciones.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn} from "typeorm";

@Entity('Permisos')
export class Permisos {

    @PrimaryGeneratedColumn()
    idPermiso: number;

    @Column()
    Nombre: string;

    @Column()
    Descripcion: string;

    @Column()
    Codigo: string;

    @ApiProperty()
    @OneToMany(() => RolPermiso, rolPermiso => rolPermiso.permiso)
    readonly rolPermiso: RolPermiso[];

    @ApiProperty()
    @OneToMany(() => GrupoPermisoXPermisos, grupoPermisoXPermisos => grupoPermisoXPermisos.permisos)
    readonly grupoPermisoXPermisos: GrupoPermisoXPermisos[];

    @ApiProperty()
    @OneToMany(() => PermisosMenuEntity, permisosMenu => permisosMenu.permisos)
    readonly permisosMenu: PermisosMenuEntity[];

    @ApiProperty()
    @OneToMany(() => GrupoPermisoXPermisos, grupoPermisoXPermisos => grupoPermisoXPermisos.permisosAux)
    readonly grupoPermisoXPermisosAux: GrupoPermisoXPermisos[];
}