import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PermisosController } from './controller/permisos.controller';
import { Permisos } from './entity/permisos.entity';
import { PermisosService } from './service/permisos.service';
import { AuthService } from 'src/auth/auth.service';
import { jwtConstants } from 'src/users/constants';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { SharedModule } from 'src/shared/shared.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Permisos, User, RolesUsuarios]),
    SharedModule,
  ],
  controllers: [PermisosController],
  providers: [PermisosService, AuthService],
  exports: [PermisosService],
})
export class PermisosModule {}
