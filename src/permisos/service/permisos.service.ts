import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { SetResultadoService } from 'src/shared/services/set-resultado/set-resultado.service';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { Resultado } from 'src/utils/resultado';
import { Repository } from 'typeorm';
import { PermisosDto } from '../DTO/permisos.dto';
import { Permisos } from '../entity/permisos.entity';
import {ComunicadoEntity} from '../../comunicados/entity/comunicados.entity';
import {RolesUsuarios} from '../../roles-usuarios/entity/roles-usuarios.entity';
import {Roles} from '../../roles/entity/roles.entity';
import {RolPermiso} from '../../rolPermiso/entity/rolPermiso.entity';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';

@Injectable()
export class PermisosService {
  constructor(
    @InjectRepository(Permisos) private readonly permisosRespository: Repository<Permisos>,
    private readonly setResultadoService: SetResultadoService,
    private readonly authService: AuthService
  ) {}


  async getPermisosByIdUsuario(idUsuario:number){

    let permisos: any = await this.permisosRespository.createQueryBuilder('P')
        .select('P.idPermiso', 'idPermiso')
        .addSelect('P.Nombre', 'Nombre')
        .addSelect('P.Codigo', 'Codigo')
        .innerJoin(RolPermiso, 'RP', 'RP.idPermiso = P.idPermiso')
        .innerJoin(Roles, 'R', 'R.idRol = RP.idRol')
        .innerJoin(RolesUsuarios, 'RU', 'RU.idRol = R.idRol')
        .where('RU.idUsuario = :idUsuario', {idUsuario: idUsuario})
        .getRawMany()
    return permisos;
  }

  async getPermisos() {
    const resultado = new Resultado();

    const permisos = await this.permisosRespository.find({ order: { Nombre: 'ASC' } });

    if (permisos == undefined) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo permisos';
    }

    resultado.Mensaje = 'Permisos obtenidos correctamente';
    resultado.Respuesta = permisos;
    resultado.ResultadoOperacion = true;

    return resultado;
  }

  public async getPermisoById(id: number): Promise<Resultado> {
    const resultado = new Resultado();

    await this.permisosRespository
      .createQueryBuilder()
      .where({ idPermiso: id })
      .getOneOrFail()
      .then((res: Permisos) => {
        // * Modificamos resultado para respuesta
        this.setResultadoService.setResponse<Permisos>(resultado, res);
      })
      .catch((err: Error) => {
        // * Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'No se ha podido obtener el permiso');
      });

    return resultado;
  }

  async getPermisosPaginados(req:any, paginacionArgs: PaginacionArgs) {
    const resultado: Resultado = new Resultado();
    let permisos: Pagination<Permisos>;
    let permisosParseados = new PaginacionDtoParser<PermisosDto>();

    // Comprobamos permisos	
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosConsultarListadePermisos);	
        
    // En caso de que el usuario no sea correcto	
    if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
    }

    let filtro = null;
    if (paginacionArgs.filtro != null && paginacionArgs.filtro != '') {
      filtro = paginacionArgs.filtro;
    }

    permisos = await paginate<Permisos>(this.permisosRespository, paginacionArgs.paginationOptions, {
      order: { Nombre: paginacionArgs.orden.ordenarPor },
      where: qb => {
        qb.where()
        if(filtro?.nombre != null && filtro.nombre != '' && filtro.nombre != 'null' ){
          qb.andWhere(
              'lower(unaccent(Permisos.Nombre)) like lower(unaccent(:nombre))', {nombre: `%${filtro.nombre}%`}
        )}
        if(filtro?.descripcion != null && filtro.descripcion != '' && filtro.descripcion != 'null' ){
          qb.andWhere(
              'lower(unaccent(Permisos.Descripcion)) like lower(unaccent(:descripcion))', {descripcion: `%${filtro.descripcion}%`}
        )}
      }
    });

    if (Array.isArray(permisos.items) && permisos.items.length) {
      let permisosDTO: PermisosDto[] = [];

      for (const p of permisos.items) {
        let permisoDTO: PermisosDto = new PermisosDto();
        permisoDTO.Nombre = p.Nombre;
        permisoDTO.Descripcion = p.Descripcion;
        permisoDTO.idPermiso = p.idPermiso;
        //permisoDTO.Codigo = p.Codigo;

        permisosDTO.push(permisoDTO);
      }

      permisosParseados.items = permisosDTO;
      permisosParseados.meta = permisos.meta;
      permisosParseados.links = permisos.links;

      resultado.Respuesta = permisosParseados;
      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Permisos obtenidos correctamente';
    } else {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'No se encontraron permisos';
    }


    return resultado;
  }
}
