import { Body, Controller, Delete, Get, Logger, Param, Post, Put, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PostulanteService } from './postulante.service';
import { Request } from 'express';
import { Page, PageRequest } from '../tipo-institucion/pagination.entity';
import { HeaderUtil } from '../base/base/header-util';
import { PostulanteEntity } from './postulante.entity';

@ApiTags('Postulante')
@Controller('postulante')
export class PostulanteController {
  logger = new Logger('PostulanteController');
  constructor(private readonly postulanteService: PostulanteService) {}

  @Get('/all')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all Postulantes',
    type: PostulanteEntity,
  })
  async getAllTipos(@Req() req: Request): Promise<PostulanteEntity[]> {
    const result = await this.postulanteService.getAll();
    return result;
  }

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all Paginate Postulante',
    type: PostulanteEntity,
  })
  async getAll(@Req() req: Request): Promise<PostulanteEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.postulanteService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'The found Postulante',
    type: PostulanteEntity,
  })
  async getOne(@Param('id') id: number): Promise<PostulanteEntity> {
    return await this.postulanteService.getOne(id);
  }

  @Get('/run/:run')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'The found Postulante by Run',
    type: PostulanteEntity,
  })
  async getOneByRun(@Param('run') run: number): Promise<PostulanteEntity> {
    return await this.postulanteService.findOne({ relations: ['region', 'comuna'], where: { RUN: run } });
  }

  @Post('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Create PostulanteEntity' })
  @ApiResponse({
    status: 201,
    description: 'The Postulante has been successfully created.',
    type: PostulanteEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() PostulanteEntity: PostulanteEntity): Promise<PostulanteEntity> {
    const created = await this.postulanteService.save(PostulanteEntity);
    HeaderUtil.addEntityCreatedHeaders(req.res, 'Postulante', created.idPostulante);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Update log' })
  @ApiResponse({
    status: 200,
    description: 'The Postulante has been successfully updated.',
    type: PostulanteEntity,
  })
  async put(@Req() req: Request, @Body() PostulanteEntity: PostulanteEntity): Promise<PostulanteEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'PostulanteEntity', PostulanteEntity.idPostulante);
    return await this.postulanteService.update(PostulanteEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Update log with id' })
  @ApiResponse({
    status: 200,
    description: 'The Postulante has been successfully updated.',
    type: PostulanteEntity,
  })
  async putId(@Req() req: Request, @Body() postulante: PostulanteEntity): Promise<PostulanteEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'Postulante', postulante.idPostulante);
    return await this.postulanteService.update(postulante);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Delete log' })
  @ApiResponse({
    status: 204,
    description: 'The Postulante has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: number): Promise<void> {
    HeaderUtil.addEntityDeletedHeaders(req.res, 'Postulante', id);
    return await this.postulanteService.delete(id);
  }
}
