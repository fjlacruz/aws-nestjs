import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm';
import { ComunasEntity } from '../comunas/entity/comunas.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Solicitudes } from '../solicitudes/entity/solicitudes.entity';
import { RegionesEntity } from '../regiones/entity/regiones.entity';
import { OpcionSexoEntity } from 'src/opciones-sexo/entity/sexo.entity';
import { OpcionEstadosCivilEntity } from 'src/opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { OpcionesNivelEducacional } from 'src/opciones-nivel-educacional/entity/nivel-educacional.entity';

@Entity('Postulantes')
export class PostulanteEntity {
  @PrimaryGeneratedColumn()
  idPostulante: number;

  @Column()
  RUN: number;

  @Column()
  DV: string;

  @Column()
  Nombres: string;

  @Column()
  ApellidoPaterno: string;

  @Column()
  ApellidoMaterno: string;

  @Column()
  Calle: string;

  @Column()
  CalleNro: number;

  @Column()
  Letra: string;

  @Column()
  RestoDireccion: string;

  @Column()
  Nacionalidad: string;

  @Column()
  Email: string;

  @Column()
  Profesion: string;

  @Column()
  Diplomatico: boolean;

  @Column()
  discapacidad: boolean;

  @Column()
  DetalleDiscapacidad: string;

  @Column()
  updated_at: Date;

  @Column()
  created_at: Date;

  @Column()
  idUsuario: number;

  @Column()
  Telefono: string;

  @Column()
  FechaNacimiento: Date;

  @Column()
  FechaDefuncion: Date;

  @Column()
  idRegion: number;

  @OneToOne(() => RegionesEntity, region => region.idRegion)
  @JoinColumn({ name: 'idRegion' })
  readonly region: RegionesEntity;

  @Column()
  idComuna: number;

  @OneToOne(() => ComunasEntity, comunas => comunas.idComuna)
  @JoinColumn({ name: 'idComuna' })
  readonly comuna: ComunasEntity;

  @Column()
  idOpcionSexo: number;
  @ManyToOne(() => OpcionSexoEntity, opcionSexo => opcionSexo.postulante)
  @JoinColumn({ name: 'idOpcionSexo' })
  readonly opcionSexo: OpcionSexoEntity;

  @Column()
  idOpcionEstadosCivil: number;
  @ManyToOne(() => OpcionEstadosCivilEntity, opcionEstadoCivil => opcionEstadoCivil.postulante)
  @JoinColumn({ name: 'idOpcionEstadosCivil' })
  readonly opcionEstadoCivil: OpcionEstadosCivilEntity;  
  
  @Column()
  idOpNivelEducacional: number;
  @ManyToOne(() => OpcionesNivelEducacional, opcionNivelEducacional => opcionNivelEducacional.postulante)
  @JoinColumn({ name: 'idOpNivelEducacional' })
  readonly opcionNivelEducacional: OpcionesNivelEducacional;
  
  @ApiProperty()
  @OneToMany(() => Solicitudes, solicitudes => solicitudes.postulante)
  readonly solicitudes: Solicitudes[];
}
