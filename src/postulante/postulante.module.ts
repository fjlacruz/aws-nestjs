import { Module } from '@nestjs/common';
import { PostulanteService } from './postulante.service';
import { PostulanteController } from './postulante.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostulanteEntity } from './postulante.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PostulanteEntity])],
  providers: [PostulanteService],
  exports: [PostulanteService],
  controllers: [PostulanteController],
})
export class PostulanteModule {}
