import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { PostulanteEntity } from './postulante.entity';

@Injectable()
export class PostulanteService {
  constructor(@InjectRepository(PostulanteEntity) private readonly postulanteEntityRepository: Repository<PostulanteEntity>) {}

  async findAndCount(options: FindManyOptions<PostulanteEntity>): Promise<[PostulanteEntity[], number]> {
    const resultList = await this.postulanteEntityRepository.findAndCount(options);
    const banks: PostulanteEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(bank => banks.push(bank));
      resultList[0] = banks;
    }
    return resultList;
  }

  async getAll() {
    return await this.postulanteEntityRepository.find();
  }

  async getOne(idPostulante: number) {
    const data = await this.postulanteEntityRepository.findOne({ relations: ['comuna'], where: { idPostulante: idPostulante } });
    if (!data) throw new NotFoundException('Tipo Resultado Cola Exam Practicos dont exist');

    return data;
  }

  async findOne(options: FindManyOptions<PostulanteEntity>): Promise<PostulanteEntity> {
    return await this.postulanteEntityRepository.findOne(options);
  }

  async update(bank: PostulanteEntity): Promise<PostulanteEntity | undefined> {
    return await this.postulanteEntityRepository.save(bank);
  }

  async save(data: PostulanteEntity): Promise<PostulanteEntity> {
    return await this.postulanteEntityRepository.save(data);
  }

  async delete(idTipoInstitucion: number) {
    await this.postulanteEntityRepository.delete(idTipoInstitucion);
    const entityFind = await this.postulanteEntityRepository.findOne({
      where: {
        idTipoInstitucion: idTipoInstitucion,
      },
    });
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
