import { ApiPropertyOptional } from "@nestjs/swagger";


export class PreguntasDto {

    @ApiPropertyOptional()
    idPregunta: string;

    @ApiPropertyOptional()
    titulo:string;

    @ApiPropertyOptional()
    cuerpo:string;

    @ApiPropertyOptional()
    status:boolean;

    @ApiPropertyOptional()
    created_at:Date;
}
