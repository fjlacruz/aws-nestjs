import {Controller, Delete, Request, UploadedFile, UploadedFiles, UseInterceptors} from '@nestjs/common';
import {Get, Param, Post, Body, Put, Patch} from '@nestjs/common';
import {ApiBearerAuth, ApiOperation, ApiTags} from '@nestjs/swagger';
import {PreguntasDto} from '../DTO/preguntas.dto';
import {PreguntasService} from '../service/preguntas.service';
import {v4 as uuidv4} from 'uuid';
import {FileInterceptor, FilesInterceptor} from '@nestjs/platform-express';
import {Public} from '../../auth/guards/auth.guard';
import {AuthDTO} from '../../auth/auth.dto';
import {JwtService} from '@nestjs/jwt';



@ApiTags('Preguntas-Manager')
@Controller('preguntas')
@ApiBearerAuth()
export class PreguntasController {
    constructor(
        private preguntasService: PreguntasService,
        private jwtService: JwtService,
    ) {
    }


    @Post('/')
    @ApiOperation({summary: 'Servicio para crear una pregunta frecuente asociada a un comunicado'})
    @UseInterceptors(FileInterceptor('imagenCabecera'))

    async createPregunta( @Body() preguntaDTO: PreguntasDto) {
        preguntaDTO.idPregunta = uuidv4();
        let data = await this.preguntasService.createPregunta(preguntaDTO);
        return data;
    }

    @Patch('/publish/:idPregunta')
    @ApiOperation({summary: 'Servicio que activa un pregunta'})
    async publish(@Param('idPregunta') idPregunta: string) {
        const data = await this.preguntasService.publishPregunta(idPregunta);
        return data;
    }



    @Get('getPregunta/:idPregunta')
    @ApiOperation({summary: 'Servicio que devuelve la información de una pregunta'})
    async getPreguntaById(@Param('idPregunta') idPregunta: string) {
        const data = await this.preguntasService.getPregunta(idPregunta);
        return data;
    }

    @Get('comunicados/:page?/:limit?')
    @ApiOperation({summary: 'Servicio que devuelve las preguntas con el titulo del comunicado asociado'})
    async getPreguntasWithComunicado(@Param('page') page: number, @Param('limit') limit: number, @Request() req) {
        let userData : AuthDTO = this.jwtService.decode(req.headers.authorization.split(" ")[1]) as AuthDTO;
        const data = await this.preguntasService.getPreguntasWithComunicado(req, userData.user, page, limit)
        return data;
    }

    @Post('readPregunta/:idPregunta')
    @ApiOperation({summary: 'Servicio que marca una pregunta como leido para un usuario'})
    async readPregunta(@Param('idPregunta') idPregunta: string, @Request() req) {
        let userData : AuthDTO = this.jwtService.decode(req.headers.authorization.split(" ")[1]) as AuthDTO;
        return this.preguntasService.readPregunta(idPregunta, userData.user);
    }

    @Delete('/:idPregunta')
    @ApiOperation({summary: 'Servicio que elimina una pregunta que no ha sido publicada'})
    async deletePregunta(@Param('idPregunta') idPregunta: string, @Request() req) {
        return this.preguntasService.delete(idPregunta);
    }

}
