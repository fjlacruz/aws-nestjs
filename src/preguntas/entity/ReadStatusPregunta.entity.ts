import {Entity, Column, PrimaryColumn, OneToMany, JoinColumn, Index, BaseEntity} from 'typeorm';

@Entity('ReadStatusPregunta')
export class ReadStatusPreguntaEntity extends BaseEntity {

    @PrimaryColumn()
    readStatusId: string;

    @Column()
    @Index()
    preguntaId: string;

    @Column()
    @Index()
    userId: string;

    @Column({
        type: 'timestamp', default: () => `CURRENT_TIMESTAMP`})
    created_at: Date;
}
