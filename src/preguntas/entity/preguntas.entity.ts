import {Entity, Column, PrimaryColumn, OneToMany, JoinColumn, Index, BaseEntity} from 'typeorm';


@Entity('Preguntas')
export class PreguntaEntity extends BaseEntity {

    @PrimaryColumn()
    idPregunta: string;

    @Column()
    titulo: string;

    @Column()
    cuerpo: string;

    @Column({nullable: true})
    idComunicado: string;

    @Column()
    status: boolean;

    @Column({
        type: 'timestamp', default: () => `CURRENT_TIMESTAMP`
    })
    created_at: Date;

}
