import { Module } from '@nestjs/common';
import { PreguntasService } from './service/preguntas.service';
import { PreguntasController } from './controller/preguntas.controller';
import {PreguntaEntity} from './entity/preguntas.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DocumentmanagerModule} from '../documentmanager/documentmanager.module';
import {JwtModule} from '@nestjs/jwt';
import {jwtConstants} from '../users/constants';
import {ReadStatusPreguntaEntity} from './entity/ReadStatusPregunta.entity';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';


@Module({

    imports: [DocumentmanagerModule, TypeOrmModule.forFeature([PreguntaEntity, ReadStatusPreguntaEntity, User, RolesUsuarios]),
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: '360d'}
        })
    ],
    providers: [PreguntasService, AuthService],
    controllers: [PreguntasController],
    exports: [PreguntasService],
})
export class PreguntasModule {}
