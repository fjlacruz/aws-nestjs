import {Injectable, NotFoundException} from '@nestjs/common';
import {PreguntaEntity} from '../entity/preguntas.entity';
import {PreguntasDto} from '../DTO/preguntas.dto';
import {createQueryBuilder, DeepPartial, getConnection, Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {DocumentsService} from '../../documentmanager/service/documents.service';
import {ComunicadoEntity} from '../../comunicados/entity/comunicados.entity';
import {EstadosSolicitudEntity} from '../../tramites/entity/estados-solicitud.entity';
import {ReadStatusPreguntaEntity} from '../entity/ReadStatusPregunta.entity';
import {v4 as uuidv4} from 'uuid';
import * as _ from 'lodash';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';


@Injectable()
export class PreguntasService {

    constructor(
        @InjectRepository(PreguntaEntity) private readonly preguntasEntity: Repository<PreguntaEntity>,
        @InjectRepository(ReadStatusPreguntaEntity) private readonly readStatusPreguntaEntity: Repository<ReadStatusPreguntaEntity>,
        private readonly fileService: DocumentsService,
        private readonly authService: AuthService
    ) {
    }

    async createPregunta(pregunta: PreguntasDto): Promise<PreguntaEntity> {
        return await this.preguntasEntity.save(pregunta);
    }

    async publishPregunta(idPregunta: string): Promise<PreguntaEntity> {
        const pregunta = await this.preguntasEntity.findOne(idPregunta);
        pregunta.status = true;
        return await pregunta.save();
    }

    async getPreguntas(idComunicado: string) {
        let preguntas = await this.preguntasEntity.find({where: {idComunicado: idComunicado, status: true}});
        let promisesPreguntas = [];
        _.forEach(preguntas, (pregunta)=>{
            promisesPreguntas.push(this.getFilesOfPregunta(pregunta))
        })
        return Promise.all(promisesPreguntas);
    }

    async getFilesOfPregunta(pregunta){
        let filesFilter = {
            limit: 100,
            where: {
                entityId: pregunta.idPregunta,
                moduleName: 'comunicaciones'
            }
        }
        console.log('filesFilter',filesFilter)
        pregunta.files = await this.fileService.getDocuments(filesFilter);
        return pregunta;
    }

    async getPregunta(id: string, filesIncluded = true) {
        const pregunta = await this.preguntasEntity.findOne(id);
        if (!pregunta) throw new NotFoundException('Pregunta dosnt exists');
        let filesFilter = {
            limit: 100,
            where: {
                entityId: id,
                moduleName: 'comunicaciones'
            }
        }
        let files;
        if (filesIncluded) {
            files = await this.fileService.getDocuments(filesFilter);
            let response = {
                ...pregunta,
                files: files
            }
            return response;
        } else {
            return pregunta
        }
    }


    async getPreguntasWithComunicado(req, userId = null, page = 1, limit = 1) {

        let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ComunicadosOficialesComunicadosOficialesVisualizarunComunicadoyunaPregunta);
    
        // En caso de que el usuario no sea correcto
        if (!usuarioValidado.ResultadoOperacion) {
          return usuarioValidado;
        }

        let loadMorePreguntas = false;
        limit = page * limit;

        let countPreguntas = await this.preguntasEntity.count({
                status: true
            }
        );


        let preguntas: any = await this.preguntasEntity.createQueryBuilder('Preguntas')
            .select('Preguntas.idPregunta', 'idPregunta')
            .addSelect('Preguntas.titulo', 'tituloPregunta')
            .addSelect('Preguntas.idComunicado', 'idComunicado')
            .addSelect('Preguntas.created_at', 'created_at')
            .addSelect('comunicado.titulo', 'tituloComunicado')
            .innerJoin(ComunicadoEntity, 'comunicado', 'comunicado.idComunicado = Preguntas.idComunicado')
            .where('Preguntas.status = :status', {status: true})
            .andWhere('comunicado.status = :status', {status: true})
            .orderBy('Preguntas.created_at', 'DESC')
            .limit(limit)
            .getRawMany()

        if (preguntas.length < countPreguntas) {
            loadMorePreguntas = true;
        }
        if (userId) {
            let preguntasReaded = await this.getReadStatus(userId);
            _.forEach(preguntas, (pregunta) => {
                pregunta.readed = preguntasReaded.includes(pregunta.idPregunta);
            })
        }
        return {preguntas: preguntas, loadMorePreguntas: loadMorePreguntas};

    }

    async getCountUnreaded(userId: string) {
        let preguntas = await this.preguntasEntity.find({
            select: ['idPregunta'],
            where: {status: true},
            order: {created_at: 'DESC'}
        });
        let preguntasReaded = await this.getReadStatus(userId);
        let unreaded = 0;
        _.forEach(preguntas, (pregunta: any) => {
            if (!preguntasReaded.includes(pregunta.idPregunta)) {
                ++unreaded;
            }
        });
        return {count: unreaded};
    }

    async delete(id: string) {
        const pregunta = await this.preguntasEntity.findOne(id);
        if (pregunta.status) {
            return {'code': 418, 'error': 'Pregunta ya ha sido publicada'};
        }
        try {
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(PreguntaEntity)
                .where('idPregunta = :id', {id: id})
                .execute();
            return {'code': 200, 'message': 'success'};
        } catch (error) {
            return {'code': 400, 'error': error};
        }
    }

    async readPregunta(idPregunta: string, userId: string) {
        const readEntry = {
            readStatusId: uuidv4(),
            preguntaId: idPregunta,
            userId: userId
        }
        let data: any = await this.readStatusPreguntaEntity.find({
            where: {
                preguntaId: idPregunta,
                userId: userId
            }
        });
        if (data.length == 0) {
            data = await this.readStatusPreguntaEntity.save(readEntry);
        }
        return data;
    }

    async getReadStatus(userId: string) {
        let data = await this.readStatusPreguntaEntity.find({select: ['preguntaId'], where: {userId: userId}});
        let response = [];
        if (data.length > 0) {
            _.forEach(data, id => {
                response.push(id.preguntaId);
            })
        }
        return response;
    }
}




