import { Test, TestingModule } from '@nestjs/testing';
import { ProrrogasController } from './prorrogas.controller';

describe('ProrrogasController', () => {
  let controller: ProrrogasController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProrrogasController],
    }).compile();

    controller = module.get<ProrrogasController>(ProrrogasController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
