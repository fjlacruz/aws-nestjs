import { Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Patch, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ProrrogasDocumentoDTO } from '../dto/prorrogas-documento.dto';
import { ProrrogasConfirmarDTO } from '../dto/prorrogas-confirmar.dto';
import { ProrrogasService } from '../service/prorrogas.service';
import { ProrrogasCrearDTO } from '../dto/prorrogas-crear.dto';
import { Resultado } from 'src/utils/resultado';
import { prorrogaExaminacionEndPoint } from '../dto/prorrogasExaminacionEndPoint.dto';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';

@ApiTags('Prorrogas')
@Controller('prorrogas')
@ApiBearerAuth()
export class ProrrogasController {
  constructor(private prorrogasService: ProrrogasService, private readonly authService: AuthService) {}

  // @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes por run' })
  // @Get('solicitudes/:run')
  // async getSolicitudesPorRun(
  //   @Param('run') run: number = null,
  //   @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
  //   @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
  //   @Query('orden') orden: string,
  //   @Query('ordenarPor') ordenarPor: string
  // ) {
  //   const join = [];
  //   const data = await this.prorrogasService.solicitudes(
  //     Number(run),
  //     {
  //       page,
  //       limit,
  //       route: '/registro-pago/allEstadoRegistroPago',
  //     },
  //     orden.toString(),
  //     ordenarPor.toString()
  //   );
  //   for (const solicitud of data.items) {
  //     const examinaciones = await this.prorrogasService.examinaciones(solicitud.idTramite);
  //     if (examinaciones.length!) join.push({ ...solicitud, examinaciones });
  //   }
  //   data.items = join;
  //   return data;
  // }

  // @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes' })
  // @Get('solicitudes')
  // async getSolicitudes(
  //   @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
  //   @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
  //   @Query('orden') orden: string,
  //   @Query('ordenarPor') ordenarPor: string
  // ) {
  //   const join = [];
  //   const data = await this.prorrogasService.solicitudes(
  //     null,
  //     {
  //       page,
  //       limit,
  //       route: '/registro-pago/allEstadoRegistroPago',
  //     },
  //     orden.toString(),
  //     ordenarPor.toString()
  //   );
  //   for (const solicitud of data.items) {
  //     const examinaciones = await this.prorrogasService.examinaciones(solicitud.idTramite);
  //     if (examinaciones.length!) join.push({ ...solicitud, examinaciones });
  //   }
  //   data.items = join;
  //   return data;
  // }

  @Get('getClasesLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las clases de licencia' })
  async getClasesLicencia() {
    const data = await this.prorrogasService.getClasesLicencias();
    return { data };
  }

  @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes de prórrogas disponibles' })
  @Get('solicitudesProrrogas')
  @ApiBearerAuth()
  async getSolicitudesProrrogas(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: any,
    @Req() req: any
  ) {
      const data = await this.prorrogasService.getSolicitudesProrrogas(
        req,
        query.idSolicitud,
        query.idTramite,
        query.estadoSolicitud,
        query.tipoTramite,
        query.RUN,
        query.claseLicencia,
        query.fechaIngresoResultado,
        query.update_at,
        query.fechaVencimiento,
        {
          page,
          limit,
          route: '/prorrogas/getSolicitudesProrrogas',
        },
        orden.toString(),
        ordenarPor
      );

      return data;
  }

  @ApiOperation({ summary: 'Servicio que devuelve todas las solicitudes de prórrogas disponibles' })
  @Get('solicitudesProrrogasPendientes')
  @ApiBearerAuth()
  async getSolicitudesProrrogasPendientes(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: any,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {

      const data = await this.prorrogasService.getSolicitudesProrrogasPendientes(
        req,
        query.idTramite,
        query.idEstadoTramite,
        query.nombreTramite,
        query.RUN,
        query.claseLicencia,
        query.fechaInicio,
        query.fechaProrroga,
        {
          page,
          limit,
          route: '/prorrogas/solicitudesProrrogasPendientes',
        },
        orden.toString(),
        ordenarPor.toString()
      );

      return data;

  }

  // @ApiOperation({ summary: 'Servicio que devuelve las examinaciones de un tramite' })
  // @Get('examinaciones/:idTramite')
  // async getExaminaciones(@Param('idTramite') idTramite: number) {
  //   return this.prorrogasService.examinaciones(Number(idTramite)).then((data: any) => {
  //     return { data, code: 200 };
  //   });
  // }

  // @ApiOperation({ summary: 'Servicio adjunta un documento para respaldar la prorroga' })
  // @Post('documento')
  // async postArchivo(@Body() dto: ProrrogasDocumentoDTO) {
  //   return this.prorrogasService.documentoCrear(dto).then((data: any) => {
  //     return { data, code: 200 };
  //   });
  // }

  @ApiOperation({ summary: 'Servicio que crea las prorroga de las examinaciones de un tramite' })
  @Post('prorrogas')
  async postProrrogas(@Body() dto: ProrrogasCrearDTO, @Req() req: any) {
    return this.prorrogasService.prorrogasCrear(req, dto).then((resultadoOperacion: boolean) => {
      return { resultadoOperacion };
    });
  }

  // @ApiOperation({ summary: 'Servicio que crea una prorroga para un tramite' })
  // @Get('pendientes')
  // async getPendientes() {
  //   return this.prorrogasService.pendientes().then((data: any) => {
  //     return { data, code: 200 };
  //   });
  // }

  // @ApiOperation({ summary: 'Servicio que crea una prorroga para un tramite' })
  // @Get('pendientes/:run')
  // async getPendientesPorRun(@Param('run') run: number) {
  //   return this.prorrogasService.pendientes(Number(run)).then((data: any) => {
  //     return { data, code: 200 };
  //   });
  // }

  // @ApiOperation({ summary: 'Servicio que crea una prorroga para un tramite' })
  // @Get('pendientes/examinaciones/:idTramite')
  // async getExaminacionesPendientes(@Param('idTramite') idTramite: number) {
  //   return this.prorrogasService.examinacionesPendientes(Number(idTramite)).then((data: any) => {
  //     return { data, code: 200 };
  //   });
  // }

  @ApiOperation({ summary: 'Servicio para aprobar una lista de solicitudes de prorrogas pendientes' })
  @Post('prorrogasPendientesAprobar')
  @ApiBearerAuth()
  async prorrogasPendientesAprobar(@Body() prorrogasAprobar: prorrogaExaminacionEndPoint, @Req() req: any) {
    // const splittedBearerToken = req.headers.authorization.split(' ');
    // const token = splittedBearerToken[1];

    // const tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.MenuVerModificacionesEspecialesSolicitudesPendientes]);

    // let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    // if (validarPermisos.ResultadoOperacion) {
      const data = await this.prorrogasService.prorrogasPendientesAprobar(req, prorrogasAprobar.prorrogas, prorrogasAprobar.oportunidades);

      return data;
    // } else {
    //   return { data: validarPermisos };
    // }
  }

  @ApiOperation({ summary: 'Servicio para rechazar una lista de solicitudes de prorrogas pendientes' })
  @Post('prorrogasPendientesDesaprobar')
  @ApiBearerAuth()
  async prorrogasProrrogaDesaprobar(@Body() prorrogasRechazar: prorrogaExaminacionEndPoint, @Req() req: any) {
    // const splittedBearerToken = req.headers.authorization.split(' ');
    // const token = splittedBearerToken[1];

    // const tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.MenuVerModificacionesEspecialesSolicitudesPendientes]);

    // let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    // if (validarPermisos.ResultadoOperacion) {
      const data = await this.prorrogasService.prorrogasPendientesDesaprobar(req, prorrogasRechazar.prorrogas, prorrogasRechazar.oportunidades);

      return data;
    // } else {
    //   return { data: validarPermisos };
    // }
  }
}
