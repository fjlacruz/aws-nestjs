import { ApiPropertyOptional } from "@nestjs/swagger";
import { ProrrogaExaminacionDTO } from "./prorroga-examinacion.dto";

export class DocumentoDTO
{

    @ApiPropertyOptional()
    nombreDoc: string;

    @ApiPropertyOptional()
    archivo: any;

    @ApiPropertyOptional()
    binary: any;

    @ApiPropertyOptional()
    created_at: string;


}

