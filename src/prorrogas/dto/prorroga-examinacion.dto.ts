import { ApiPropertyOptional } from "@nestjs/swagger";

export class ProrrogaExaminacionDTO
{
        IdTramite: number;
        IdExaminacion: number;
        fechaLimite: Date;
        fechaProrroga: Date;
        prorroga: boolean;
        oportunidad: boolean;
        observacion: string;

        // para evitar zona horaria
        fechaProrrogaString: string;

        constructor(){}
}