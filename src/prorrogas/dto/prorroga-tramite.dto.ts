import { ApiPropertyOptional } from "@nestjs/swagger";

export class ProrrogaTramiteDTO
{
    @ApiPropertyOptional()
    idTramite: number;

    @ApiPropertyOptional()
    observacion: string;
}