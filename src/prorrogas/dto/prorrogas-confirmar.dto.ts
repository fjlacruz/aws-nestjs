import { ApiPropertyOptional } from "@nestjs/swagger";

export class ProrrogasConfirmarDTO
{
    @ApiPropertyOptional()
    prorrogas: any[];

    // @ApiPropertyOptional()
    // aprobadoPor: number;
}