import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocumentoDTO } from "./documento.dto";
import { ProrrogaExaminacionDTO } from "./prorroga-examinacion.dto";

export class ProrrogasCrearDTO
{

    @ApiPropertyOptional()
    examinaciones: ProrrogaExaminacionDTO[];

    @ApiPropertyOptional()
    documentos: DocumentoDTO[];

    @ApiPropertyOptional()
    observacion: string;

    idTramites: number[]

}

