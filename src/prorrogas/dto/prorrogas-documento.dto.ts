import { ApiPropertyOptional } from "@nestjs/swagger";

export class ProrrogasDocumentoDTO
{
    @ApiPropertyOptional()
    nombreDocs: string;

    @ApiPropertyOptional()
    binary: Buffer;

    @ApiPropertyOptional()
    idProrrogaTramite: number;
}