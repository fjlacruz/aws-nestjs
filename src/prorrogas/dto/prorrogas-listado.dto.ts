import { ApiPropertyOptional } from "@nestjs/swagger";

export class ProrrogasListadoDTO {
    constructor(){}

    @ApiPropertyOptional()
    IdSolicitud: number

    @ApiPropertyOptional()
    Run: Number;

    @ApiPropertyOptional()
    RunFormateado: string;    

    @ApiPropertyOptional()
    NombrePostulante: string;

    @ApiPropertyOptional()
    Tramites: SolicitudTramitesDTO[];

    @ApiPropertyOptional()
    Examinaciones: SolicitudExaminacionesDTO[];

    @ApiPropertyOptional()
    ClasesLicencias: string[];

    @ApiPropertyOptional()
    FechaInicio: Date;

    @ApiPropertyOptional()
    FechaModificacion: Date;

    @ApiPropertyOptional()
    FechaVencimiento: Date;

    @ApiPropertyOptional()
    EstadoExaminacion: boolean;

    @ApiPropertyOptional()
    EstadoAlerta: string;

    @ApiPropertyOptional()
    UltimaModificacion: Date;

    @ApiPropertyOptional()
    IdEstadoSolicitud: number;

    @ApiPropertyOptional()
    EstadoSolicitudString: string;
}

export class SolicitudTramitesDTO{
    @ApiPropertyOptional()
    IdTramite: number;

    @ApiPropertyOptional()
    EstadoTramiteString: string;

    @ApiPropertyOptional()
    idEstadoTramite: number;

    @ApiPropertyOptional()
    IdTipoTramite: number;

    @ApiPropertyOptional()
    TipoTramiteString: string;
    
    @ApiPropertyOptional()
    FechaTramite: Date;

    @ApiPropertyOptional()
    FechaUltimaModificacion: Date;

    @ApiPropertyOptional()
    FechaExpiracionTramite: Date;

    @ApiPropertyOptional()
    UrgenciaTramite: string;    

    @ApiPropertyOptional()
    IdEstadoTramite: number;
}

export class SolicitudExaminacionesDTO{
    @ApiPropertyOptional()
    IdExaminacion: number;

    @ApiPropertyOptional()
    IdEstadoExaminacion: number;    

    @ApiPropertyOptional()
    IdTipoExaminacion: number;

    @ApiPropertyOptional()
    EstadoExaminacionString: string; 

    @ApiPropertyOptional()
    EstadoAprobadoString: string; 

    @ApiPropertyOptional()
    TipoExaminacionString: string;     

    @ApiPropertyOptional()
    AbreviacionClaseLicenciaAsocString: string;  
    
    @ApiPropertyOptional()
    IdTramitesAsociados: number[];

}
