import { ApiPropertyOptional } from "@nestjs/swagger";

export class ProrrogasOportunidadesPendientesListadoDTO {
    constructor(){}

    @ApiPropertyOptional()
    IdProrrogaOportunidadExaminacion: number;

    @ApiPropertyOptional()
    IdProrrogaOportunidadTramite: number;

    @ApiPropertyOptional()
    IdTramite: number;

    @ApiPropertyOptional()
    IdEstadoTramite: number;

    @ApiPropertyOptional()
    EstadoTramiteString: string;

    @ApiPropertyOptional()
    IdTipoTramite: number;

    @ApiPropertyOptional()
    TipoTramiteString: string;    

    @ApiPropertyOptional()
    Run: Number;

    @ApiPropertyOptional()
    RunFormateado: string; 

    @ApiPropertyOptional()
    ClaseLicencias: string;
    
    @ApiPropertyOptional()
    Examinaciones: ProrrogaOportunidadExaminacionesDTO[];

    @ApiPropertyOptional()
    FechaInicio: Date;

    @ApiPropertyOptional()
    FechaProrrogaOportunidad: Date;    

    @ApiPropertyOptional()
    EsProrroga: Boolean
}


export class ProrrogaOportunidadExaminacionesDTO{
    @ApiPropertyOptional()
    IdExaminacion: number;

    @ApiPropertyOptional()
    IdEstadoExaminacion: number;    

    @ApiPropertyOptional()
    IdTipoExaminacion: number;

    @ApiPropertyOptional()
    EstadoExaminacionString: string; 

    @ApiPropertyOptional()
    TipoExaminacionString: string;     

    @ApiPropertyOptional()
    AbreviacionClaseLicenciaAsocString: string;  
    
    @ApiPropertyOptional()
    IdTramitesAsociados: number[];

}