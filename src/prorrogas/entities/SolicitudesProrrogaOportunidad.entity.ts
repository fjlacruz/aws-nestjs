import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('SolicitudesProrrogaOportunidad')
export class SolicitudesProrrogaOportunidadEntity {
    @PrimaryGeneratedColumn()
    idSolicitudesProrrogaOportunidad: number;

    @ApiModelProperty()
    @Column()
    observacion: string;

    @ApiModelProperty()
    @Column()
    created_at: Date;
}