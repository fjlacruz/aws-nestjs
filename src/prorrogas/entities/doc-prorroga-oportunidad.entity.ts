import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('DocsProrrogasOportunidades')
export class DocsProrrogaOportunidadEntity {
    @PrimaryGeneratedColumn()
    idDocsProrrogasOportunidad: number;

    @ApiModelProperty()
    @Column()
    nombreDoc: string;

    @ApiModelProperty()
    @Column({ name: 'binary', type: 'bytea', nullable: false })
    binary: Buffer;

    @ApiModelProperty()
    @Column()
    created_at: Date;

    @ApiModelProperty()
    @Column()
    idSolicitudesProrrogaOportunidad: number;
}