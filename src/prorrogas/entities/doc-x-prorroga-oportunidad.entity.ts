import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'DocsXProrrogasOportunidades' )
export class DocXProrrogaOportunidadEntity
{
    @PrimaryGeneratedColumn()
    id

    @Column()
    idDoc: number;

    @Column()
    idOportunidad: number;

    @Column()
    idProrroga: number;
}