import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'DocsProrrogas' )
export class DocsProrrogasEntity
{
    @PrimaryGeneratedColumn()
    idDocsProrrogas: number;

    @Column()
    nombreDocs: string;

    @Column({name: 'binray', type: 'bytea', nullable: false })
    binary: Buffer;

    @Column()
    created_at: Date;

    @Column()
    idProrrogaTramite: number;
}