import { ColaExaminacion } from "src/tramites/entity/cola-examinacion.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { ProrrogaTramiteEntity } from "./prorroga-tramite.entity";

@Entity( 'ProrrogaExaminacion' )
export class ProrrogaExaminacionEntity
{
    @PrimaryGeneratedColumn()
    idProrrogaExaminacion: number;

    @Column()
    idSolicitudesProrrogaOportunidad: number;

    @Column()
    idColaExaminacion: number;
    @ManyToOne(() => ColaExaminacion, (colaExaminacion) => colaExaminacion.prorrogaExaminacion)
    @JoinColumn({ name: 'idColaExaminacion' })
    readonly colaExaminacion: ColaExaminacion;

    @Column()
    ingresadaPor: number;

    @Column()
    nuevaFechaSolicitada: Date;

    @Column()
    fechaIngreso: Date;

    @Column()
    aprobadoPor: number;

    @Column()
    prorrogaAprobada: boolean;

    @Column()
    fechaAprobacion: Date;

    @Column()
    idProrrogaTramite: number;

    @Column()
    observacion: string;
    // @ManyToOne(() => ProrrogaTramiteEntity, (prorrogaTramites) => prorrogaTramites.prorrogaExaminacion)
    // @JoinColumn({ name: 'idProrrogaTramite' })
    // readonly prorrogaTramites: ProrrogaTramiteEntity;
}