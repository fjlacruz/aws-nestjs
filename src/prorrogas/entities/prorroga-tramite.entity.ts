import { TramitesEntity } from "src/tramites/entity/tramites.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ProrrogaExaminacionEntity } from "./prorroga-examinacion.entity";

@Entity('ProrrogaTramite')
export class ProrrogaTramiteEntity
{
    @PrimaryGeneratedColumn()
    idProrrogaTramite: number;
    
    @Column()
    idTramite: number;
    @ManyToOne(() => TramitesEntity, (tramites) => tramites.prorrogaTramite)
    @JoinColumn({ name: 'idTramite' })
    readonly tramites: TramitesEntity;

    @Column()
    created_at: Date;

    @Column()
    observacion: string;

    // @OneToMany(() => ProrrogaExaminacionEntity, (prorrogaExaminacion) => prorrogaExaminacion.)
    // readonly prorrogaExaminacion: ProrrogaExaminacionEntity[];
}