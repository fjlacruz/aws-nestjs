import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'get_prorrogasxoportunidades()' )
export class ProrrogaXOportunidades
{
    @PrimaryColumn()
    colaexam: number;

    @Column()
    nuevafecha: Date; 

    @Column()
    idoportunidadprorroga: number;

    @Column()
    fechaingreso: Date;
}