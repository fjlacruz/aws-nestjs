import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { OportunidadEntity } from 'src/oportunidad/oportunidad.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { ServiciosComunesService } from 'src/shared/services/ServiciosComunes/ServiciosComunes.services';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { User } from 'src/users/entity/user.entity';
import { ProrrogasController } from './controller/prorrogas.controller';
import { DocsProrrogaOportunidadEntity } from './entities/doc-prorroga-oportunidad.entity';
import { DocXProrrogaOportunidadEntity } from './entities/doc-x-prorroga-oportunidad.entity';
import { DocsProrrogasEntity } from './entities/docs-prorrogas.entity';
import { ProrrogaExaminacionEntity } from './entities/prorroga-examinacion.entity';
import { ProrrogaTramiteEntity } from './entities/prorroga-tramite.entity';
import { ProrrogaXOportunidades } from './entities/prorrogas-oportunidades.entity';
import { ProrrogasService } from './service/prorrogas.service';

@Module({
  imports: [TypeOrmModule.forFeature(  [ProrrogaTramiteEntity, DocsProrrogasEntity, ProrrogaExaminacionEntity, 
                                        TramitesEntity, TramitesClaseLicencia,ResultadoExaminacionColaEntity,
                                        ResultadoExaminacionEntity,ColaExaminacionEntity, User, RolesUsuarios, Solicitudes,
                                        TiposTramiteEntity, EstadosTramiteEntity, estadosExaminacionEntity, 
      ClasesLicenciasEntity, TipoExaminacionesEntity, estadosExaminacionEntity, EstadosSolicitudEntity, ProrrogaXOportunidades, OportunidadEntity, DocXProrrogaOportunidadEntity, DocsProrrogaOportunidadEntity])],
  controllers: [ProrrogasController],
  providers: [ProrrogasService, AuthService, ServiciosComunesService],
  exports: [ProrrogasService]
})
export class ProrrogasModule {}
