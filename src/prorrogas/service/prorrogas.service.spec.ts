import { Test, TestingModule } from '@nestjs/testing';
import { ProrrogasService } from './prorrogas.service';

describe('ProrrogasService', () => {
  let service: ProrrogasService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProrrogasService],
    }).compile();

    service = module.get<ProrrogasService>(ProrrogasService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
