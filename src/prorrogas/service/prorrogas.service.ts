import { EstadosExaminacionEntity } from './../../tramites/entity/estados-examinacion.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { EntityManager, getConnection, getManager, In, Repository } from 'typeorm';
import { ProrrogasDocumentoDTO } from '../dto/prorrogas-documento.dto';
import { ProrrogasConfirmarDTO } from '../dto/prorrogas-confirmar.dto';
import { DocsProrrogasEntity } from '../entities/docs-prorrogas.entity';
import { ProrrogaExaminacionEntity } from '../entities/prorroga-examinacion.entity';
import { ProrrogaTramiteEntity } from '../entities/prorroga-tramite.entity';
import { ProrrogasCrearDTO } from '../dto/prorrogas-crear.dto';
import { AuthService } from 'src/auth/auth.service';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { Resultado } from 'src/utils/resultado';
import { validarStringEsEntero } from 'src/shared/Common/ValidadoresFormato';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { ColaExaminacionTramiteNMEntity } from 'src/tramites/entity/cola-examinacion-tramite-n-m.entity';
import { ColaExaminacion } from 'src/tramites/entity/cola-examinacion.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { devolverValorRUN, formatearRun, validarRut } from 'src/shared/ValidarRun';
import { ProrrogasListadoDTO, SolicitudExaminacionesDTO, SolicitudTramitesDTO } from '../dto/prorrogas-listado.dto';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { ServiciosComunesService } from 'src/shared/services/ServiciosComunes/ServiciosComunes.services';
import { ProrrogaOportunidadExaminacionesDTO, ProrrogasOportunidadesPendientesListadoDTO } from '../dto/prorrogas-pendientes-listado.dto';
import { ProrrogaXOportunidades } from '../entities/prorrogas-oportunidades.entity';
import { OportunidadEntity } from 'src/oportunidad/oportunidad.entity';
import { paginationMeta, paginationResult } from 'src/shared/Entities/pagination';
import * as moment from 'moment';
import { async } from 'rxjs';
import { DocsProrrogaOportunidadEntity } from '../entities/doc-prorroga-oportunidad.entity';
import { DocXProrrogaOportunidadEntity } from '../entities/doc-x-prorroga-oportunidad.entity';
import { SolicitudesEnProcesoDto } from 'src/solicitudes/DTO/solicitudesEnProceso.dto';
import { SolicitudesProrrogaOportunidadEntity } from '../entities/SolicitudesProrrogaOportunidad.entity';
import { ColumnasSolicitudesProrroga, PermisosNombres } from 'src/constantes';

@Injectable()
export class ProrrogasService {
  constructor(
    private readonly authService: AuthService,

    @InjectRepository(TramitesClaseLicencia)
    private readonly tramitesLicenciaRepository: Repository<TramitesClaseLicencia>,

    @InjectRepository(DocsProrrogasEntity)
    private readonly documentosRepository: Repository<DocsProrrogasEntity>,

    @InjectRepository(TramitesEntity)
    private readonly tramitesRepository: Repository<TramitesEntity>,

    @InjectRepository(Solicitudes) private readonly solicitudesEntityRespository: Repository<Solicitudes>,
    @InjectRepository(ProrrogaExaminacionEntity) private readonly prorrogaExaminacionEntityRespository: Repository<ProrrogaExaminacionEntity>,
    @InjectRepository(OportunidadEntity) private readonly oportunidadExaminacionEntityRespository: Repository<OportunidadEntity>,
    @InjectRepository(ProrrogaXOportunidades) private readonly prorrogaXOportunidadesRepository: Repository<ProrrogaXOportunidades>,

    @InjectRepository(DocsProrrogaOportunidadEntity)
    private readonly docsProrrogaOportunidadEntityRepository: Repository<DocsProrrogaOportunidadEntity>,

    @InjectRepository(DocXProrrogaOportunidadEntity)
    private readonly DocXProrrogaOportunidadEntityRepository: Repository<DocXProrrogaOportunidadEntity>,
    private serviciosComunesService: ServiciosComunesService
  ) { }

  async solicitudes(run?: number, options?: IPaginationOptions, orden?: string, ordenarPor?: string): Promise<any> {
    const join = [];
    const idOficina = this.authService.oficina().idOficina;
    const where = run ? 'postulante.RUN = ' + run.toString() : 'true';
    const solicitudes = await this.tramitesLicenciaRepository
      .createQueryBuilder('tralic')
      .innerJoinAndSelect('tralic.clasesLicencias', 'claLic')
      .innerJoinAndSelect('tralic.tramite', 'tramite')
      .innerJoinAndSelect('tramite.tiposTramite', 'tipoTramite')
      .innerJoinAndSelect('tramite.solicitudes', 'solicitud')
      .innerJoinAndSelect('solicitud.tramites', 'tramitesAsociados')
      .innerJoinAndSelect('tramitesAsociados.tiposTramite', 'tipoTramitesAsociados')
      .innerJoinAndSelect('tramitesAsociados.estadoTramite', 'estadoTramitesAsociados')
      .innerJoinAndSelect('tramitesAsociados.tramitesClaseLicencia', 'tramiteClaseTramitesAsociados')
      .innerJoinAndSelect('tramiteClaseTramitesAsociados.clasesLicencias', 'claseAsociados')
      .innerJoinAndSelect('tramite.estadoTramite', 'estTra')
      .innerJoinAndSelect('solicitud.postulante', 'postulante')
      .innerJoinAndSelect('solicitud.estadosSolicitud', 'estado')
      .where(where)
      .andWhere('solicitud.idOficina = :idOficina', { idOficina })
      .andWhere('tramite.idEstadoTramite = 1'); // tramite inciado
    return paginate<any>(solicitudes, options);

    // for (const solicitud of solicitudes) {
    //   const examinaciones = await this.examinaciones(solicitud.idTramite);
    //   if (examinaciones.length!) join.push({ ...solicitud, examinaciones });
    // }
    // return join;
  }

  async getSolicitudesProrrogas(
    req:any,
    idSolicitud?: string,
    idTramite?: string,
    idEstadoSolicitud?: string,
    nombreTramite?: string,
    run?: string,
    claseLicencia?: string,
    fechaCreacion?: Date,
    updated_at?: Date,
    fechaVencimiento?: Date,
    options?: IPaginationOptions,
    orden?: string,
    ordenarPor?: "ASC" | "DESC"
  ): Promise<any> {

    const orderBy = ColumnasSolicitudesProrroga[orden];


    let res: Resultado = new Resultado();

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaModificacionesEspecialesProrrogasOportunidades);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      // Comprobación de tipos numéricos
      if (((idTramite) && (!validarStringEsEntero(idTramite))) ||
        ((claseLicencia) && (!validarStringEsEntero(claseLicencia))) ||
        ((nombreTramite) && (!validarStringEsEntero(nombreTramite))) ||
        ((idEstadoSolicitud) && (!validarStringEsEntero(idEstadoSolicitud))) ||
        ((idSolicitud) && (!validarStringEsEntero(idSolicitud)))) {

        res.Error = "Existe un error en los datos enviados, por favor revise los tipos de datos enviados.";
        res.ResultadoOperacion = false;
        res.Respuesta = [];

        return res;
      }

      const qbSolicitudes = this.solicitudesEntityRespository.createQueryBuilder('Solicitudes')
        .innerJoinAndMapOne('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne('Solicitudes.estadosSolicitud', EstadosSolicitudEntity, 'estadosSolicitud', 'estadosSolicitud.idEstado = Solicitudes.idEstado')
        .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne('tramites.tiposTramite', TiposTramiteEntity, 'tiposTramite', 'tramites.idTipoTramite = tiposTramite.idTipoTramite')
        .innerJoinAndMapOne('tramites.estadoTramite', EstadosTramiteEntity, 'estadoTramite', 'tramites.idEstadoTramite = estadoTramite.idEstadoTramite')
        .innerJoinAndMapOne('tramites.tramitesClaseLicencia', TramitesClaseLicencia, 'tramitesClaseLicencia', 'tramites.idTramite = tramitesClaseLicencia.idTramite')
        .innerJoinAndMapOne('tramitesClaseLicencia.clasesLicencias', ClasesLicenciasEntity, 'clasesLicencias', 'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia')
        .innerJoinAndMapMany('tramites.colaExaminacionNM', ColaExaminacionTramiteNMEntity, 'colaExaminacionNM', 'tramites.idTramite = colaExaminacionNM.idTramite')
        .innerJoinAndMapOne('colaExaminacionNM.colaExaminacion', ColaExaminacion, 'colaExaminacion', 'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion')
        .innerJoinAndMapOne('colaExaminacion.tipoExaminaciones', TipoExaminacionesEntity, 'tipoExaminaciones', 'colaExaminacion.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion')
        .innerJoinAndMapOne('colaExaminacion.estadoExaminaciones', EstadosExaminacionEntity, 'estadoExaminaciones', 'colaExaminacion.idEstadosExaminacion = estadoExaminaciones.idEstadosExaminacion')

        //Para filtros
        .innerJoinAndMapMany('Solicitudes.tramitesFiltroTramite', TramitesEntity, 'tramitesFiltroTramite', 'Solicitudes.idSolicitud = tramitesFiltroTramite.idSolicitud')
        .innerJoinAndMapMany('Solicitudes.tramitesFiltroExaminaciones', TramitesEntity, 'tramitesFiltroExaminaciones', 'Solicitudes.idSolicitud = tramitesFiltroExaminaciones.idSolicitud')
        .innerJoinAndMapMany('tramitesFiltroExaminaciones.colaExaminacionNMFiltroExaminaciones', ColaExaminacionTramiteNMEntity, 'colaExaminacionNMFiltroExaminaciones', 'tramitesFiltroExaminaciones.idTramite = colaExaminacionNMFiltroExaminaciones.idTramite')
        .innerJoinAndMapOne('colaExaminacionNM.colaExaminacionFiltroExaminaciones', ColaExaminacion, 'colaExaminacionFiltroExaminaciones', 'colaExaminacionNMFiltroExaminaciones.idColaExaminacion = colaExaminacionFiltroExaminaciones.idColaExaminacion')

        // Recogemos las solicitudes que no estén en estado borrador
        .andWhere('(Solicitudes.idEstado != 1)')

        // // Eliminamos registros con una oportunidad o prorroga ya abierta
        .andWhere('(Solicitudes.idSolicitud NOT IN (SELECT DISTINCT "SolicitudesAux"."idSolicitud"' +
          'FROM "Solicitudes" "SolicitudesAux"' +
          'INNER JOIN "Postulantes" "postulanteAux" ON "SolicitudesAux"."idPostulante" = "postulanteAux"."idPostulante"' +
          'INNER JOIN "Tramites" "tramitesAux" ON "SolicitudesAux"."idSolicitud" = "tramitesAux"."idSolicitud"' +
          'INNER JOIN "ColaExaminacionTramiteNM" "colaExaminacionNMAux" ON "tramitesAux"."idTramite" = "colaExaminacionNMAux"."idTramite"' +
          'INNER JOIN "ColaExaminacion" "colaExaminacionAux" ON "colaExaminacionNMAux"."idColaExaminacion" = "colaExaminacionAux"."idColaExaminacion"' +
          'INNER JOIN "ProrrogaExaminacion" "ProrrogaExaminacionAux" ON "colaExaminacionAux"."idColaExaminacion" = "ProrrogaExaminacionAux"."idColaExaminacion"' +
          'WHERE "ProrrogaExaminacionAux"."prorrogaAprobada" IS NULL))')

        .andWhere('(Solicitudes.idSolicitud NOT IN (SELECT DISTINCT "SolicitudesAux"."idSolicitud"' +
          'FROM "Solicitudes" "SolicitudesAux"' +
          'INNER JOIN "Postulantes" "postulanteAux" ON "SolicitudesAux"."idPostulante" = "postulanteAux"."idPostulante"' +
          'INNER JOIN "Tramites" "tramitesAux" ON "SolicitudesAux"."idSolicitud" = "tramitesAux"."idSolicitud"' +
          'INNER JOIN "ColaExaminacionTramiteNM" "colaExaminacionNMAux" ON "tramitesAux"."idTramite" = "colaExaminacionNMAux"."idTramite"' +
          'INNER JOIN "ColaExaminacion" "colaExaminacionAux" ON "colaExaminacionNMAux"."idColaExaminacion" = "colaExaminacionAux"."idColaExaminacion"' +
          'INNER JOIN "Oportunidades" "OportunidadExaminacionAux" ON "colaExaminacionAux"."idColaExaminacion" = "OportunidadExaminacionAux"."idColaExaminacion"' +
          'WHERE "OportunidadExaminacionAux"."fechaAprobacion" IS NULL ))')

        // Recogemos los trámites que estén en los estados : Iniciado 
        .andWhere('(' +

          '(' +
          '(tramites.idEstadoTramite = 1) ' +
          //' AND (' + 

          //'( tramitesFiltroTramite.update_at < NOW() - INTERVAL \'180 days\' )' + 
          //')' + // Para los trámites de estado en curso sacamos los antiguos y pendientes (ex idon moral y medica)

          'AND ((' +

          '(colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 6 AND colaExaminacionFiltroExaminaciones.idTipoExaminacion = 5 ) OR ' +
          '(colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 8 AND colaExaminacionFiltroExaminaciones.idTipoExaminacion = 4) OR ' +
          '(colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 31 AND colaExaminacionFiltroExaminaciones.idTipoExaminacion = 5 ) OR' +
          '(colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 31 AND colaExaminacionFiltroExaminaciones.idTipoExaminacion = 4 ) )' +
          ')' + // Para los trámites de estado en curso sacamos los antiguos y pendientes (ex idon moral y medica)                                            

          ')' +

          'OR ' +

          '(' +
          // Solicitudes con trámite en estado Trámite reconsiderado por JPL o SML o iniciado 
          '((tramites.idEstadoTramite = 2) AND (' +

          'colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 16 OR ' +
          'colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 17 OR ' +
          'colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 18 OR ' +
          'colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 22 OR ' +
          'colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 23 OR ' +
          'colaExaminacionFiltroExaminaciones.idEstadosExaminacion = 24))' + // Para el caso de las que están denegadas y tengan el estado de examinación reprobado       

          ') )')


      //Filtro por id de tramite
      if (idTramite) {
        qbSolicitudes
          //.innerJoinAndMapOne('tramites.solicitudesFiltro', Solicitudes, 'solicitudesFiltro', 'tramites.idSolicitud = solicitudesFiltro.idSolicitud')
          .innerJoinAndMapMany('Solicitudes.tramitesFiltroIdTramite', TramitesEntity, 'tramitesFiltroIdTramite', 'Solicitudes.idSolicitud = tramitesFiltroIdTramite.idSolicitud')

        qbSolicitudes.andWhere('tramitesFiltroIdTramite.idTramite = :_idTramite', {
          //Realizamos consulta para recoger las solicitudes asociadas a 
          _idTramite: Number(idTramite)
        });
      }

      if (claseLicencia) {
        qbSolicitudes
          .innerJoinAndMapMany('Solicitudes.tramitesFiltroIdClase', TramitesEntity, 'tramitesFiltroIdClase', 'Solicitudes.idSolicitud = tramitesFiltroIdClase.idSolicitud')
          .leftJoinAndMapMany('tramitesFiltroIdClase.tramitesClaseLicenciaFiltroIdClase', TramitesClaseLicencia, 'tramitesClaseLicenciaFiltroIdClase', 'tramitesClaseLicenciaFiltroIdClase.idTramite = tramitesFiltroIdClase.idTramite')

        qbSolicitudes.andWhere('tramitesClaseLicenciaFiltroIdClase.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: claseLicencia
        });
      }


      // Filtros
      if (run) {
        const rut = run.split('-');
        let runsplited: string = rut[0];
        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');
        const div = rut[1];
        qbSolicitudes.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbSolicitudes.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }



      if (fechaCreacion) qbSolicitudes.andWhere("\"Solicitudes\".\"FechaCreacion\"::date = :created_at::date", { //"FechaCreacion" > '2022-07-20'
        created_at: fechaCreacion
      });

      if (updated_at) qbSolicitudes.andWhere("\"Solicitudes\".\"updated_at\"::date = :fechaUpdate::date", {
        fechaUpdate: updated_at
      });

      if (fechaVencimiento) qbSolicitudes.andWhere("\"Solicitudes\".\"FechaCreacion\"::date = :vencimiento::date", { //Se compara con la fecha de creacion por que actualmente la fecha de vencimiento viene nula
        vencimiento: fechaVencimiento
      });

      //Filtro por tipo de tramite
      if (nombreTramite) qbSolicitudes.andWhere('tramites.idTipoTramite = :_idTipoTramite', {
        _idTipoTramite: Number(nombreTramite)
      });



      // Filtrar por id solicitud
      if (idSolicitud) qbSolicitudes.andWhere('Solicitudes.idSolicitud = :_idSolicitud', {
        _idSolicitud: Number(idSolicitud)
      });

      // Filtrar por id Estado solicitud
      if (idEstadoSolicitud) qbSolicitudes.andWhere('tramites.idEstadoTramite = :_idEstado', {
        _idEstado: Number(idEstadoSolicitud)
      });


      qbSolicitudes.orderBy(orderBy, ordenarPor);

      try {
        const resultadoSolicitudes: any = await paginate<Solicitudes>(qbSolicitudes, options);

        // Inicializamos el listado
        let listadoSolicitudesDTO: ProrrogasListadoDTO[] = [];



        // Transformacion al DTO
        resultadoSolicitudes.items.forEach(sol => {

          let resRecorrido: Solicitudes = sol as Solicitudes;

          let plDTO: ProrrogasListadoDTO = new ProrrogasListadoDTO()

          plDTO.IdSolicitud = resRecorrido.idSolicitud;
          plDTO.EstadoSolicitudString = resRecorrido.estadosSolicitud.Nombre;
          plDTO.IdEstadoSolicitud = resRecorrido.idEstado;


          plDTO.Tramites = resRecorrido.tramites.map((t: TramitesEntity) => {

            //Fecha expiración
            let newDueDate: Date = new Date(t.created_at);
            newDueDate.setMonth(newDueDate.getMonth() + 6);

            //Fechas para cálculo de urgencia del trámite
            let urgenciaDeTramiteString: string = "";

            let sinUrgencia: Date = new Date(); // Fecha de hoy para comparar
            sinUrgencia.setMonth(sinUrgencia.getMonth() - 5); // de 5 meses hacia delante sin urgencia

            let urgenciaMedia: Date = new Date();
            urgenciaMedia.setMonth(urgenciaMedia.getMonth() - 5);
            urgenciaMedia.setDate(urgenciaMedia.getDate() - 15); // de 5 meses hasta 5 meses y medio hasta 5 urgencia media

            let urgenciaAlta: Date = new Date();
            urgenciaAlta.setMonth(urgenciaAlta.getMonth() - 6);
            //urgenciaMedia.setMonth(urgenciaMedia.getDay() - 15); // de 5 meses hasta 5 meses y medio hasta 5 urgencia media

            if (t.created_at >= sinUrgencia) {
              urgenciaDeTramiteString = "URGENCIABAJA";
            }
            else if (t.created_at >= urgenciaMedia) {
              urgenciaDeTramiteString = "URGENCIAMEDIA";
            }
            else {
              urgenciaDeTramiteString = "URGENCIAALTA";
            }

            const temp: SolicitudTramitesDTO = {
              IdTramite: t.idTramite,
              IdTipoTramite: t.idTipoTramite,
              idEstadoTramite: t.estadoTramite.idEstadoTramite,
              EstadoTramiteString: t.estadoTramite.Nombre,
              TipoTramiteString: t.tiposTramite.Nombre,
              FechaTramite: t.created_at,
              FechaUltimaModificacion: t.update_at,
              FechaExpiracionTramite: newDueDate,
              UrgenciaTramite: urgenciaDeTramiteString,
              IdEstadoTramite: t.idEstadoTramite
            }

            return temp;

          });
          //TODO revisar bien el ordenamiento de la columna fecha expiracion
          /* if (orderBy == ColumnasSolicitudesProrroga.FechaExpiracionTramite) {
            console.log("ENTRO SORT")
            plDTO.Tramites = plDTO.Tramites.sort((a: SolicitudTramitesDTO, b: SolicitudTramitesDTO) => {
              if (ordenarPor == 'ASC') {
                if (a.FechaExpiracionTramite > b.FechaExpiracionTramite) { return 1 } else {
                  if (a.FechaExpiracionTramite == b.FechaExpiracionTramite) {
                    return 0
                  } else {
                    return -1
                  }

                }
              } else {
                if (a.FechaExpiracionTramite < b.FechaExpiracionTramite) { return 1 } else {
                  if (a.FechaExpiracionTramite == b.FechaExpiracionTramite) {
                    return 0
                  } else {
                    return -1
                  }
                }
              }

            });
          } */
          plDTO.Run = +(resRecorrido.postulante.RUN + '' + resRecorrido.postulante.DV);
          plDTO.RunFormateado = formatearRun(resRecorrido.postulante.RUN + '-' + resRecorrido.postulante.DV);
          plDTO.ClasesLicencias = resRecorrido.tramites.map(t => t.tramitesClaseLicencia.clasesLicencias.Abreviacion);
          plDTO.NombrePostulante = resRecorrido.postulante.Nombres + ' ' + resRecorrido.postulante.ApellidoPaterno + ' ' + resRecorrido.postulante.ApellidoMaterno;

          //Guardamos todos los ids de las colas de examinación, dentro se repetirán los que estén más de una vez (se usará para asignar abreviación de exámen y comprobar
          // si ya se ha insertado)
          let colasExaminacionesIds: number[] = [];

          resRecorrido.tramites.forEach(z => {
            z.colaExaminacionNM.forEach(y => {
              if (y.colaExaminacion.idTipoExaminacion != 6) // Descartamos preidoneidad moral
              { colasExaminacionesIds.push(y.idColaExaminacion); }
            })
          })

          //Se inicializa el array de cola examinación dtos
          let listadoSolExamDTO: SolicitudExaminacionesDTO[] = [];

          // Booleano para indicar si se debe asociar las clases de licencia
          let asignarAbreviacionClaseExaminacion: Boolean = false;

          resRecorrido.tramites.forEach((x: any) => {



            // Primero sacamos la clase de licencia
            let claseAbreviacionTramite = x.tramitesClaseLicencia.clasesLicencias.Abreviacion;

            // Segundo las examinaciones asociadas al trámite
            x.colaExaminacionNM.forEach(cExam => {


              // Inicializamos el DTO
              let solExamDTO: SolicitudExaminacionesDTO = new SolicitudExaminacionesDTO();

              // Comprobamos primero si no existe en el listado para agregarlo (en caso contrario ya fue agregado)
              if ((cExam.colaExaminacion.idTipoExaminacion != 6) && ((listadoSolExamDTO.length == 0) || (!listadoSolExamDTO.some(x => x.IdExaminacion == cExam.idColaExaminacion)))) {
                solExamDTO.IdExaminacion = cExam.idColaExaminacion;
                solExamDTO.EstadoExaminacionString = cExam.colaExaminacion.estadoExaminaciones.nombreEstado;
                solExamDTO.EstadoAprobadoString = (cExam.colaExaminacion.aprobado == null) ? "PENDIENTE" : (cExam.colaExaminacion.aprobado == true) ? "APROBADO" : "REPROBADO";
                solExamDTO.TipoExaminacionString = cExam.colaExaminacion.tipoExaminaciones.nombreExaminacion;
                solExamDTO.AbreviacionClaseLicenciaAsocString = claseAbreviacionTramite;
                solExamDTO.IdTramitesAsociados = [];
                solExamDTO.IdTramitesAsociados.push(x.idTramite);

                //En el caso de existir más de una cola examinación asociada, procedemos a añadir la distinción
                if (colasExaminacionesIds.filter(x => x == cExam.idColaExaminacion).length > 1) {
                  solExamDTO.AbreviacionClaseLicenciaAsocString = null;
                  asignarAbreviacionClaseExaminacion = true;
                }

                // Agregamos a la lista
                listadoSolExamDTO.push(solExamDTO);
              }
              // En caso de que lo hayamos encontrado, procedemos sólo a agregar un trámite adicional asociado a la examinación
              else if (listadoSolExamDTO.some(x => x.IdExaminacion == cExam.idColaExaminacion)) {
                let solExamAgregarTramite: SolicitudExaminacionesDTO = listadoSolExamDTO.find(x => x.IdExaminacion == cExam.idColaExaminacion);
                solExamAgregarTramite.IdTramitesAsociados.push(x.idTramite);
              }
            })

          })

          //En esta linea se terminó la inserción de examinaciones de un trámite, comprobamos para resetear a null si no es necesario indicar la abreviación
          if (!asignarAbreviacionClaseExaminacion) {

            listadoSolExamDTO.forEach(lseDTO => {
              lseDTO.AbreviacionClaseLicenciaAsocString = null;
            });

          }

          plDTO.Examinaciones = listadoSolExamDTO;

          plDTO.FechaInicio = resRecorrido.FechaCreacion;
          plDTO.FechaVencimiento = resRecorrido.fechaVencimiento;
          plDTO.UltimaModificacion = resRecorrido.updated_at;

          // Se agrega el elemento a la lista
          listadoSolicitudesDTO.push(plDTO);

        });

        console.log("------------------LLEGO SIN ERRORES-----------------------------------------------------------")

        console.log("----------------------------------------------------------------------------------------------------")

        // Se devuelve resultado correcto
        resultadoSolicitudes.items = listadoSolicitudesDTO;

        res.ResultadoOperacion = true;
        res.Respuesta = resultadoSolicitudes;

        return res;

      } catch (error) {
        res.Error = "Ha ocurrido un error en la respuesta, por favor consulte con el administrador de la aplicación.";
        res.ResultadoOperacion = false;
        res.Respuesta = [];

        return res;
      }

    }
    catch (Error) {
      res.Error = "Ha ocurrido un error en la respuesta, por favor consulte con el administrador de la aplicación.";
      res.ResultadoOperacion = false;
      res.Respuesta = [];

      return res;
    }

  }

  async getSolicitudesProrrogasPendientes(
    req:any,
    idTramite?: string,
    idEstadoTramite?: string,
    nombreTramite?: string,
    run?: string,
    claseLicencia?: string,
    fechaCreacion?: Date,
    fechaProrroga?: Date,
    options?: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ): Promise<any> {



    let res: Resultado = new Resultado();

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaModificacionesEspecialesSolicitudesEspecialesPendientes);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      // Comprobación de tipos numéricos
      if (((idTramite) && (!validarStringEsEntero(idTramite))) ||
        ((claseLicencia) && (!validarStringEsEntero(claseLicencia))) ||
        ((nombreTramite) && (!validarStringEsEntero(nombreTramite))) ||
        ((run) && (!validarRut(run.split(".").join("")))) ||
        ((idEstadoTramite) && (!validarStringEsEntero(idEstadoTramite)))) {

        res.Error = "Existe un error en los datos enviados, por favor revise los tipos de datos enviados.";
        res.ResultadoOperacion = false;
        res.Respuesta = [];

        return res;
      }

      //Calcular a partir de qué registro nos traemos
      let offset: number = 0;
      if (options.page != 1)
        offset = ((options.page as number) - 1) * (options.limit as number);

      //Recogemos según los ids de oportunidades Examinación que existan en la tabla que recuperamos antes desde la función

      // if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qbSolicitudes.orderBy('colaExaminacion.idColaExaminacion', 'ASC');
      // if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qbSolicitudes.orderBy('colaExaminacion.idColaExaminacion', 'DESC');
      // if (orden === 'run' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.RUN', 'ASC');
      // if (orden === 'run' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.RUN', 'DESC');
      // if (orden === 'postulante' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.Nombres', 'ASC');
      // if (orden === 'postulante' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.Nombres', 'DESC');
      // if (orden === 'claseLicencia' && ordenarPor === 'ASC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'ASC');
      // if (orden === 'claseLicencia' && ordenarPor === 'DESC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'DESC');
      let columnaOrden: string = null;
      let modoOrden: string = null; // true ASC / false DESC

      if (orden === 'IdTramite')
        columnaOrden = 'idTramite';
      else if (orden === 'TipoTramiteString')
        columnaOrden = 'nombreTipoTramite';
      else if (orden === 'EstadoTramiteString')
        columnaOrden = 'nombreEstadoTramite';
      else if (orden === 'RunFormateado')
        columnaOrden = 'runPostulante';
      else if (orden === 'ClaseLicencias')
        columnaOrden = 'nombreClaseLicencia';
      else if (orden === 'FechaInicio')
        columnaOrden = 'FechaCreacionSolicitud';

      if (ordenarPor && ordenarPor != null) {
        if (ordenarPor === 'ASC')
          modoOrden = 'ASC';
        else
          modoOrden = 'DESC';
      }


      console.log('');//qbSolicitudes.orderBy('colaExaminacion.idTramite', 'ASC');
      if (orden === 'IdTramite' && ordenarPor === 'DESC') console.log('');//qbSolicitudes.orderBy('colaExaminacion.idTramite', 'DESC');

      const queryUnionOportunidadesProrrogas: string = this.getQueryUnionOportunidadesProrrogas((fechaProrroga != undefined), false, columnaOrden, modoOrden);
      const queryUnionOportunidadesProrrogasCount: string = this.getQueryUnionOportunidadesProrrogas((fechaProrroga != undefined), true, columnaOrden, modoOrden);


      // Llamamos a la query que hace la unión  
      const oportunidadesXprorrogasQuery: ProrrogaXOportunidades[] = await getConnection().manager.query(queryUnionOportunidadesProrrogas, [
        (idTramite) ? idTramite : null,
        (nombreTramite) ? nombreTramite : null,
        (idEstadoTramite) ? idEstadoTramite : null,
        (run) ? devolverValorRUN(run) : null,
        (claseLicencia) ? claseLicencia : null,
        (fechaCreacion) ? moment(fechaCreacion).format("YYYY-MM-DD") : null,
        (fechaProrroga) ? moment(fechaProrroga).format("YYYY-MM-DD") : null,
        options.limit,
        offset
      ]) as ProrrogaXOportunidades[];

      // Llamamos a la query que hace el conteo del total de elementos                                                                                                                                                  // Llamamos a la query que hace la unión  
      const oportunidadesXprorrogasQueryCount: any = await getConnection().manager.query(queryUnionOportunidadesProrrogasCount, [
        (idTramite) ? idTramite : null,
        (nombreTramite) ? nombreTramite : null,
        (idEstadoTramite) ? idEstadoTramite : null,
        (run) ? devolverValorRUN(run) : null,
        (claseLicencia) ? claseLicencia : null,
        (fechaCreacion) ? moment(fechaCreacion).format("YYYY-MM-DD") : null,
        (fechaProrroga) ? moment(fechaProrroga).format("YYYY-MM-DD") : null
      ]);

      let contadorTotalElementos = oportunidadesXprorrogasQueryCount[0].count;

      //Recogemos las examinaciones excepto idoneidad moral
      //.andWhere('(colaExaminacionAux.idTipoExaminacion != 6)');

      //Recogemos según los ids de prorroga Examinación que existan en la tabla que recuperamos antes desde la función
      const idsProrrogas = (oportunidadesXprorrogasQuery.filter(x => x.nuevafecha) as ProrrogaXOportunidades[]).map(x => x.idoportunidadprorroga);
      const idsOportunidades = (oportunidadesXprorrogasQuery.filter(x => !x.nuevafecha) as ProrrogaXOportunidades[]).map(x => x.idoportunidadprorroga);

      const qbOportunidades = this.oportunidadExaminacionEntityRespository.createQueryBuilder('Oportunidad')
        .innerJoinAndMapOne('Oportunidad.colaExaminacion', ColaExaminacion, 'colaExaminacion', 'Oportunidad.idColaExaminacion = colaExaminacion.idColaExaminacion')
        .innerJoinAndMapOne('colaExaminacion.colaExaminacionNM', ColaExaminacionTramiteNMEntity, 'colaExaminacionNM', 'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion')
        .innerJoinAndMapOne('colaExaminacionNM.tramites', TramitesEntity, 'tramites', 'colaExaminacionNM.idTramite = tramites.idTramite')
        .innerJoinAndMapOne('tramites.solicitudes', Solicitudes, 'solicitudes', 'tramites.idSolicitud = solicitudes.idSolicitud')
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne('tramites.tiposTramite', TiposTramiteEntity, 'tiposTramite', 'tramites.idTipoTramite = tiposTramite.idTipoTramite')
        .innerJoinAndMapOne('tramites.estadoTramite', EstadosTramiteEntity, 'estadoTramite', 'tramites.idEstadoTramite = estadoTramite.idEstadoTramite')
        .innerJoinAndMapOne('tramites.tramitesClaseLicencia', TramitesClaseLicencia, 'tramitesClaseLicencia', 'tramites.idTramite = tramitesClaseLicencia.idTramite')
        .innerJoinAndMapOne('tramitesClaseLicencia.clasesLicencias', ClasesLicenciasEntity, 'clasesLicencias', 'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia')
        .innerJoinAndMapMany('tramites.colaExaminacionNMAux', ColaExaminacionTramiteNMEntity, 'colaExaminacionNMAux', 'tramites.idTramite = colaExaminacionNMAux.idTramite')
        .innerJoinAndMapOne('colaExaminacionNMAux.colaExaminacionAux', ColaExaminacion, 'colaExaminacionAux', 'colaExaminacionNMAux.idColaExaminacion = colaExaminacionAux.idColaExaminacion')
        .innerJoinAndMapOne('colaExaminacionAux.tipoExaminaciones', TipoExaminacionesEntity, 'tipoExaminaciones', 'colaExaminacionAux.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion')

        .andWhere('(colaExaminacionAux.idTipoExaminacion != 6)') // tipo de examinación distinta a pre idoneidad moral
        .andWhere('(Oportunidad.idOportunidad IN (:..._idsOportunidades))', {
          //Realizamos consulta para recoger las solicitudes asociadas a oportunidades
          _idsOportunidades: idsOportunidades
        });





      const qbProrrogas = this.prorrogaExaminacionEntityRespository.createQueryBuilder('ProrrogaExaminacion')
        .innerJoinAndMapOne('ProrrogaExaminacion.colaExaminacion', ColaExaminacion, 'colaExaminacion', 'ProrrogaExaminacion.idColaExaminacion = colaExaminacion.idColaExaminacion')
        .innerJoinAndMapOne('colaExaminacion.colaExaminacionNM', ColaExaminacionTramiteNMEntity, 'colaExaminacionNM', 'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion')
        .innerJoinAndMapOne('colaExaminacionNM.tramites', TramitesEntity, 'tramites', 'colaExaminacionNM.idTramite = tramites.idTramite')
        .innerJoinAndMapOne('tramites.solicitudes', Solicitudes, 'solicitudes', 'tramites.idSolicitud = solicitudes.idSolicitud')
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne('tramites.tiposTramite', TiposTramiteEntity, 'tiposTramite', 'tramites.idTipoTramite = tiposTramite.idTipoTramite')
        .innerJoinAndMapOne('tramites.estadoTramite', EstadosTramiteEntity, 'estadoTramite', 'tramites.idEstadoTramite = estadoTramite.idEstadoTramite')
        .innerJoinAndMapOne('tramites.tramitesClaseLicencia', TramitesClaseLicencia, 'tramitesClaseLicencia', 'tramites.idTramite = tramitesClaseLicencia.idTramite')
        .innerJoinAndMapOne('tramitesClaseLicencia.clasesLicencias', ClasesLicenciasEntity, 'clasesLicencias', 'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia')
        .innerJoinAndMapMany('tramites.colaExaminacionNMAux', ColaExaminacionTramiteNMEntity, 'colaExaminacionNMAux', 'tramites.idTramite = colaExaminacionNMAux.idTramite')
        .innerJoinAndMapOne('colaExaminacionNMAux.colaExaminacionAux', ColaExaminacion, 'colaExaminacionAux', 'colaExaminacionNMAux.idColaExaminacion = colaExaminacionAux.idColaExaminacion')
        .innerJoinAndMapOne('colaExaminacionAux.tipoExaminaciones', TipoExaminacionesEntity, 'tipoExaminaciones', 'colaExaminacionAux.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion')

        .andWhere('(colaExaminacionAux.idTipoExaminacion != 6)') // tipo de examinación distinta a pre idoneidad moral
        .andWhere('(ProrrogaExaminacion.idProrrogaExaminacion IN (:..._idsProrrogas))', {
          //Realizamos consulta para recoger las solicitudes asociadas a prorrogas
          _idsProrrogas: idsProrrogas
        });


      let resultadoOportunidades: OportunidadEntity[] = [];
      let resultadoProrrogas: ProrrogaExaminacionEntity[] = [];

      if (idsOportunidades && idsOportunidades.length > 0) {
        resultadoOportunidades = await qbOportunidades.getMany();
      }

      if (idsProrrogas && idsProrrogas.length > 0) {
        resultadoProrrogas = await qbProrrogas.getMany();
      }

      // Inicializamos el listado
      let listadoSolicitudesDTO: ProrrogasOportunidadesPendientesListadoDTO[] = [];

      // // Transformacion al DTO
      oportunidadesXprorrogasQuery.forEach(oxp => {

        let plDTO: ProrrogasOportunidadesPendientesListadoDTO = new ProrrogasOportunidadesPendientesListadoDTO();

        // Variable para guardar el listado de examinaciones
        let examinacionesPorRecorrer: ColaExaminacionTramiteNMEntity[] = [];

        // Variable para asignar la clase de licencia (Abreviación)
        let claseAbreviacionTramite: string = "";

        //Si es nulo, entendemos que es una oportunidad, asignamos según el caso
        if (!oxp.nuevafecha) {
          plDTO.EsProrroga = false;

          let oportunidadRecuperada: OportunidadEntity = resultadoOportunidades.find(x => x.idOportunidad == oxp.idoportunidadprorroga);

          plDTO.IdTramite = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.idTramite;
          plDTO.IdEstadoTramite = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.idTramite;
          plDTO.EstadoTramiteString = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.estadoTramite.Nombre;
          plDTO.IdTipoTramite = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.idTramite;
          plDTO.TipoTramiteString = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.tiposTramite.Nombre;
          plDTO.FechaInicio = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.solicitudes.FechaCreacion;
          //plDTO.FechaProrroga = sol.nuevaFechaSolicitada;
          plDTO.IdProrrogaOportunidadExaminacion = oportunidadRecuperada.idOportunidad;
          //plDTO.Run = +(resRecorrido.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.RUN + '' + resRecorrido.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.DV);
          plDTO.RunFormateado = formatearRun(oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.RUN + '-' + oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.DV);
          plDTO.ClaseLicencias = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.tramitesClaseLicencia.clasesLicencias.Abreviacion;

          examinacionesPorRecorrer = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.colaExaminacionNMAux;

          claseAbreviacionTramite = oportunidadRecuperada.colaExaminacion.colaExaminacionNM.tramites.tramitesClaseLicencia.clasesLicencias.Abreviacion;
        }
        else {
          plDTO.EsProrroga = true;

          let prorrogaRecuperada: ProrrogaExaminacionEntity = resultadoProrrogas.find(x => x.idProrrogaExaminacion == oxp.idoportunidadprorroga);

          plDTO.IdTramite = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.idTramite;
          plDTO.IdEstadoTramite = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.idTramite;
          plDTO.EstadoTramiteString = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.estadoTramite.Nombre;
          plDTO.IdTipoTramite = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.idTramite;
          plDTO.TipoTramiteString = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.tiposTramite.Nombre;
          plDTO.FechaInicio = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.solicitudes.FechaCreacion;
          plDTO.FechaProrrogaOportunidad = prorrogaRecuperada.nuevaFechaSolicitada;
          plDTO.IdProrrogaOportunidadExaminacion = prorrogaRecuperada.idProrrogaExaminacion;
          //plDTO.Run = +(resRecorrido.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.RUN + '' + resRecorrido.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.DV);
          plDTO.RunFormateado = formatearRun(prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.RUN + '-' + prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.solicitudes.postulante.DV);
          plDTO.ClaseLicencias = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.tramitesClaseLicencia.clasesLicencias.Abreviacion;

          examinacionesPorRecorrer = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.colaExaminacionNMAux;

          claseAbreviacionTramite = prorrogaRecuperada.colaExaminacion.colaExaminacionNM.tramites.tramitesClaseLicencia.clasesLicencias.Abreviacion;
        }


        // Inicializamos el listado de examinaciones
        let listadoProrrogaExaminacionesDTO: ProrrogaOportunidadExaminacionesDTO[] = [];

        examinacionesPorRecorrer.forEach(colaExamNM => {

          // examinaciones asociadas al trámite

          // Inicializamos el DTO
          let prorrogasExamDTO: ProrrogaOportunidadExaminacionesDTO = new ProrrogaOportunidadExaminacionesDTO();

          // Comprobamos primero si no existe en el listado para agregarlo (en caso contrario ya fue agregado)
          prorrogasExamDTO.IdExaminacion = colaExamNM.idColaExaminacion;
          prorrogasExamDTO.EstadoExaminacionString = (colaExamNM.colaExaminacionAux.aprobado == null) ? "PENDIENTE" : (colaExamNM.colaExaminacionAux.aprobado == true) ? "APROBADO" : "REPROBADO";
          prorrogasExamDTO.TipoExaminacionString = colaExamNM.colaExaminacionAux.tipoExaminaciones.nombreExaminacion;
          prorrogasExamDTO.AbreviacionClaseLicenciaAsocString = claseAbreviacionTramite;

          // Agregamos a la lista
          listadoProrrogaExaminacionesDTO.push(prorrogasExamDTO);



        })

        plDTO.Examinaciones = listadoProrrogaExaminacionesDTO;
        listadoSolicitudesDTO.push(plDTO);




      });

      // Se devuelve resultado correcto
      //resultadoSolicitudes.items = listadoSolicitudesDTO;

      //Generamos la repuesta
      let pRes: paginationResult = new paginationResult();
      pRes.items = listadoSolicitudesDTO;

      let metaPagination: paginationMeta = new paginationMeta();
      metaPagination.itemCount = listadoSolicitudesDTO.length;
      metaPagination.totalItems = +contadorTotalElementos;
      metaPagination.itemsPerPage = options.limit as number;
      metaPagination.totalPages = Math.ceil(metaPagination.itemCount / metaPagination.itemsPerPage);
      metaPagination.currentPage = options.page as number;

      pRes.meta = metaPagination;

      res.Respuesta = pRes;
      res.ResultadoOperacion = true;

      return res;

    }
    catch (Error) {
      res.Error = "Ha ocurrido un error en la respuesta, por favor consulte con el administrador de la aplicación.";
      res.ResultadoOperacion = false;
      res.Respuesta = [];

      return res;
    }

  }


  async examinaciones(idTramite: number) {
    const examinaciones = await getManager()
      .createQueryBuilder()
      .select('"cola".*, "esta"."nombreEstado", tipo.nombreExaminacion')
      .from('ColaExaminacionTramiteNM', 'colaTram')
      .innerJoin(ColaExaminacionEntity, 'cola', 'cola.idColaExaminacion        = "colaTram"."idColaExaminacion"')
      .innerJoin('estadosExaminacion', 'esta', '"esta"."idEstadosExaminacion" = "cola"."idEstadosExaminacion"')
      .innerJoin(TipoExaminacionesEntity, 'tipo', 'tipo.idTipoExaminacion        = cola.idTipoExaminacion')
      .where('"colaTram"."idTramite" = :idTramite', { idTramite })
      .andWhere('cola.idTipoExaminacion <> 6')
      .getRawMany();

    return examinaciones;
  }
  async documentoCrear(dto: ProrrogasDocumentoDTO) {
    dto['created_at'] = new Date();
    return await this.documentosRepository.save(dto);
  }

  async prorrogasCrear(req: any, dto: ProrrogasCrearDTO) {
    const idUsuario = (await this.authService.usuario()).idUsuario;

    let resultadoOperacion = false;
    let tramitesGuardados: any[] = [];
    let solicitudGuardada: SolicitudesProrrogaOportunidadEntity;
    let oportunidadesGuardadas: any[] = [];
    let prorrogasGuardadas: any[] = [];
    let documentosGuardados: any[] = [];
    let relacionesGuardadas: any[] = [];
    let fecha = new Date();

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ModificacionesEspecialesProrrogasOportunidadesSolicitarAprobacion);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	 

    return await getManager().transaction(async (manager: EntityManager) => {

      // Guardamos los tramites
      await Promise.all((dto.idTramites.map(async (idTramite) => {

        //Se guarda la nueva fecha para el trámite correspondiente
        let tramiteActual = await this.tramitesRepository.findOne({ where: { idTramite: idTramite } });

        tramiteActual.update_at = new Date();

        const tramiteActualParaActualizar = await manager.save(TramitesEntity, tramiteActual);

        //Se guarda la prórroga asociada al trámite
        let tramiteProrrogaSave: ProrrogaTramiteEntity = new ProrrogaTramiteEntity();
        tramiteProrrogaSave.idTramite = idTramite
        tramiteProrrogaSave.created_at = new Date()

        const tramite = await manager.save(ProrrogaTramiteEntity, tramiteProrrogaSave)
        tramitesGuardados.push(tramite);


      }))).then(async res => {

        // Guardamos la solicitud de prorroga / oportunidad
        let solicitudProrrogaOportunidad: SolicitudesProrrogaOportunidadEntity = new SolicitudesProrrogaOportunidadEntity()
        solicitudProrrogaOportunidad.created_at = new Date();
        solicitudProrrogaOportunidad.observacion = dto.observacion;

        solicitudGuardada = await manager.save(SolicitudesProrrogaOportunidadEntity, solicitudProrrogaOportunidad)


      }).then(async res => {

        // Guardamos las prorrogas y las examinaciones
        await Promise.all((dto.examinaciones.map(async e => {

          // Obtenemos el Id de ProrrogaTramite correspondiente
          let tramiteTemp = tramitesGuardados.filter(t => {
            return e.IdTramite == t.idTramite
          })

          if (e.prorroga) {
            let prorrogaTramiteSave: ProrrogaExaminacionEntity = new ProrrogaExaminacionEntity();

            prorrogaTramiteSave.idColaExaminacion = e.IdExaminacion
            prorrogaTramiteSave.idSolicitudesProrrogaOportunidad = solicitudGuardada.idSolicitudesProrrogaOportunidad
            prorrogaTramiteSave.nuevaFechaSolicitada = e.fechaProrrogaString ? new Date(e.fechaProrrogaString) : new Date()
            prorrogaTramiteSave.idProrrogaTramite = tramiteTemp[0].idProrrogaTramite
            prorrogaTramiteSave.ingresadaPor = idUsuario
            prorrogaTramiteSave.fechaIngreso = fecha;
            //prorrogaTramiteSave.observacion = dto.observacion


            let prorroga = await manager.save(ProrrogaExaminacionEntity, prorrogaTramiteSave);
            prorrogasGuardadas.push(prorroga)
          }

          if (e.oportunidad) {
            let oportunidadTramiteSave: OportunidadEntity = new OportunidadEntity();

            oportunidadTramiteSave.idColaExaminacion = e.IdExaminacion
            oportunidadTramiteSave.idSolicitudesProrrogaOportunidad = solicitudGuardada.idSolicitudesProrrogaOportunidad
            oportunidadTramiteSave.ingresadoPor = idUsuario
            oportunidadTramiteSave.fechaIngreso = fecha;
            oportunidadTramiteSave.created_at = fecha;
            oportunidadTramiteSave.updated_at = fecha;
            //oportunidadTramiteSave.observacion = dto.observacion


            let oportunidad = await manager.save(OportunidadEntity, oportunidadTramiteSave);
            oportunidadesGuardadas.push(oportunidad)
          }

        }))).then(async res => {

          // Guardamos los documentos
          await Promise.all(dto.documentos.map(async doc => {
            let docSave: DocsProrrogaOportunidadEntity = new DocsProrrogaOportunidadEntity();
            docSave.nombreDoc = doc.nombreDoc
            docSave.binary = doc.binary;
            docSave.created_at = fecha
            docSave.idSolicitudesProrrogaOportunidad = solicitudGuardada.idSolicitudesProrrogaOportunidad

            let docSaved = await manager.save(DocsProrrogaOportunidadEntity, docSave)
            documentosGuardados.push(docSaved)

          }))
        })

      }).then(res => {
        resultadoOperacion = true;
        return resultadoOperacion;
      }).catch(err => {
        return resultadoOperacion;
      })

      return resultadoOperacion;
    })
  }

  async pendientes(run: number = null) {
    const join = [];
    const idOficina = this.authService.oficina().idOficina;
    const where = run ? 'postulante.RUN = ' + run.toString() : 'true';
    const solicitudes = await this.tramitesLicenciaRepository
      .createQueryBuilder('tralic')
      .innerJoin(ProrrogaTramiteEntity, 'prorroga', 'prorroga.idTramite = tralic.idTramite')
      .innerJoinAndSelect('tralic.clasesLicencias', 'claLic')
      .innerJoinAndSelect('tralic.tramite', 'tramite')
      .innerJoinAndSelect('tramite.tiposTramite', 'tipoTramite')
      .innerJoinAndSelect('tramite.solicitudes', 'solicitud')
      .innerJoinAndSelect('tramite.estadoTramite', 'estTra')
      .innerJoinAndSelect('solicitud.postulante', 'postulante')
      .innerJoinAndSelect('solicitud.estadosSolicitud', 'estado')
      .where(where)
      .andWhere('solicitud.idOficina = :idOficina', { idOficina })
      .getMany();

    for (const solicitud of solicitudes) {
      const examinaciones = await this.examinacionesPendientes(solicitud.idTramite);
      if (examinaciones.length!) join.push({ ...solicitud, examinaciones });
    }
    return join;
  }
  async examinacionesPendientes(idTramite: number) {
    const examinaciones = await getManager()
      .createQueryBuilder()
      .select('"proExa".*, "cola".*, "esta"."nombreEstado", tipo.nombreExaminacion')
      .from(ProrrogaExaminacionEntity, 'proExa')
      .innerJoin(ColaExaminacionEntity, 'cola', 'cola.idColaExaminacion        = "proExa"."idColaExaminacion"')
      .innerJoin('estadosExaminacion', 'esta', '"esta"."idEstadosExaminacion" = "cola"."idEstadosExaminacion"')
      .innerJoin(TipoExaminacionesEntity, 'tipo', 'tipo.idTipoExaminacion        = cola.idTipoExaminacion')
      .innerJoin(ProrrogaTramiteEntity, 'proTra', 'proTra.idProrrogaTramite      = proExa.idProrrogaTramite')
      .where('proTra.idTramite           = :idTramite', { idTramite })
      .andWhere('proExa.prorrogaAprobada    IS NULL')
      .getRawMany();

    return examinaciones;
  }
  async prorrogasAprobar(dto: ProrrogasConfirmarDTO) {
    return await this.prorrgasCambiarEstado(dto, true);
  }
  async prorrogasDesaprobar(dto: ProrrogasConfirmarDTO) {
    return await this.prorrgasCambiarEstado(dto, false);
  }
  private async prorrgasCambiarEstado(dto: ProrrogasConfirmarDTO, valor: boolean) {
    const aprobadoPor = (await this.authService.usuario()).idUsuario;
    const fechaAprobacion = new Date();
    const prorrogaAprobada = valor;
    const data = { aprobadoPor, fechaAprobacion, prorrogaAprobada };

    await getManager().transaction(async (manager: EntityManager) => {
      for (const idProrrogaExaminacion of dto.prorrogas) await manager.update(ProrrogaExaminacionEntity, idProrrogaExaminacion, data);
    });
    return true;
  }

  public async getClasesLicencias() {

    return this.serviciosComunesService.getClasesLicencia();

  }

  async prorrogasPendientesAprobar(req:any, prorrogasPendientesAprobar: number[], oportunidadesPendientesAprobar: number[]): Promise<Resultado> {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ModificacionesEspecialesSolicitudesEspPendientesModificarEstadoProrrogasOportunidades);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	  

    return await this.prorrogasPendientesCambiarEstado(prorrogasPendientesAprobar, oportunidadesPendientesAprobar, true);
  }

  async prorrogasPendientesDesaprobar(req:any, prorrogasPendientesRechazar: number[], oportunidadesPendientesRechazar: number[]): Promise<Resultado> {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.ModificacionesEspecialesSolicitudesEspPendientesModificarEstadoProrrogasOportunidades);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	  

    return await this.prorrogasPendientesCambiarEstado(prorrogasPendientesRechazar, oportunidadesPendientesRechazar, false);

  }

  private async prorrogasPendientesCambiarEstado(prorrogasPendientesAsignacion: number[], oportunidadesPendientesAsignacion: number[], valor: boolean): Promise<Resultado> {
    let res: Resultado = new Resultado();



    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {

      const aprobadoPor = (await this.authService.usuario()).idUsuario;
      const fechaAprobacion = new Date();
      const prorrogaAprobada = valor;

      if (prorrogasPendientesAsignacion && prorrogasPendientesAsignacion.length > 0) {
        for (const i of prorrogasPendientesAsignacion) {

          queryRunner.manager.update(ProrrogaExaminacionEntity, i, {
            aprobadoPor: aprobadoPor,
            fechaAprobacion: fechaAprobacion,
            prorrogaAprobada: prorrogaAprobada
          });

        }
      }

      // En el caso en que existan oportunidades que modificar
      if (oportunidadesPendientesAsignacion && oportunidadesPendientesAsignacion.length > 0) {
        // Se recuperan la información de las oportunidades
        const qbOportunidades = this.oportunidadExaminacionEntityRespository
          .createQueryBuilder('Oportunidades')

          .innerJoinAndMapOne(
            'Oportunidades.colaExaminacion',
            ColaExaminacion,
            'colaExaminacion',
            'Oportunidades.idColaExaminacion = colaExaminacion.idColaExaminacion'
          )

          .innerJoinAndMapOne(
            'colaExaminacion.colaExaminacionNM',
            ColaExaminacionTramiteNMEntity,
            'colaExaminacionNM',
            'colaExaminacion.idColaExaminacion = colaExaminacionNM.idColaExaminacion'
          )

        qbOportunidades.andWhere("Oportunidades.idOportunidad IN (:..._oportunidadesPendientesAsignacion)", { _oportunidadesPendientesAsignacion: oportunidadesPendientesAsignacion });

        const resultadoOportunidades: OportunidadEntity[] = await qbOportunidades.getMany();

        // Con la información de las oportunidades realizamos las modificaciones y la generación de nueva examinación correspondiente con el mismo tipo

        for (const oportunidad of resultadoOportunidades) {

          //let colaExaminacionRecuperada = resultadoOportunidades.filter(x => x.id)

          let colaExaminacionEditar: ColaExaminacion = new ColaExaminacion();

          colaExaminacionEditar.idColaExaminacion = oportunidad.idColaExaminacion;
          colaExaminacionEditar.aprobado = null;
          colaExaminacionEditar.historico = false;

          //Depende del tipo de examinacion asociada, asignamos el estado de pendiente
          switch (oportunidad.colaExaminacion.idTipoExaminacion) {
            case 2: colaExaminacionEditar.idEstadosExaminacion = 13; break; // Examinación Teórica
            case 3: colaExaminacionEditar.idEstadosExaminacion = 19; break; // Examinación Práctica
            case 4: colaExaminacionEditar.idEstadosExaminacion = 8; break; // Examinación Médica
          }



          // Se crea la cola de examinación
          queryRunner.manager.update(ColaExaminacion, colaExaminacionEditar.idColaExaminacion, {
            aprobado: colaExaminacionEditar.aprobado,
            historico: colaExaminacionEditar.historico,
            idEstadosExaminacion: colaExaminacionEditar.idEstadosExaminacion
          }
          );
          // Se actualiza el estado del Trámite
          queryRunner.manager.update(TramitesEntity, oportunidad.colaExaminacion.idTramite, {
            idEstadoTramite: 1,
          }
          );

          // Se actualiza la petición de oportunidad
          queryRunner.manager.update(OportunidadEntity, oportunidad.idOportunidad, {
            aprobadoPor: aprobadoPor,
            fechaAprobacion: fechaAprobacion,
            oportunidadAprobada: prorrogaAprobada
          });

        }
      }

      queryRunner.commitTransaction();

      res.Mensaje = "Se ha guardado la información correctamente";
      res.ResultadoOperacion = true;

      return res;
    }
    catch (Error) {
      res.Error = "Ha ocurrido un error guardando la información.";
      res.ResultadoOperacion = false;
      queryRunner.rollbackTransaction();
    }
    finally {
      queryRunner.release();
    }
  }

  private getQueryUnionOportunidadesProrrogas(obtenerSoloProrrogas: Boolean, obtenerContador: Boolean, columnaOrdenacion?: string, modoOrdenacion?: string): string {

    const obtenerConsultaContador: string = (obtenerContador) ? 'select count(*) from ( ' :
      'select colaexam, nuevafecha, idOportunidadProrroga, fechaIngreso from ( ';

    const consultaPaginacionContador: string = (obtenerContador) ? '' : ' limit $8 offset $9 ';

    const condicionSoloProrrogas: string = (obtenerSoloProrrogas) ? ' and (false)' : '';

    const consultaOrdenacion: string = (columnaOrdenacion && modoOrdenacion && columnaOrdenacion != null && modoOrdenacion != null) ?
      (' ORDER BY ' + columnaOrdenacion + ' ' + modoOrdenacion + ' ') : '';

    return ('' +
      obtenerConsultaContador +
      '  select ' +
      '     "Oportunidades"."idColaExaminacion" as colaexam, ' +
      '       null as nuevafecha,' +
      '     "Oportunidades"."idOportunidad" as idOportunidadProrroga,' +
      '     "Oportunidades"."fechaIngreso" as fechaIngreso, ' +
      '     "tramites"."idTramite" as idTramite, ' +
      '     "tiposTramite"."Nombre" as nombreTipoTramite, ' +
      '     "estadoTramite"."Nombre" as nombreEstadoTramite, ' +
      '     "postulante"."RUN" As runPostulante, ' +
      '     "clasesLicencias"."Abreviacion" as nombreClaseLicencia, ' +
      '     "solicitudes"."FechaCreacion" AS FechaCreacionSolicitud ' +
      '  from "Oportunidades" "Oportunidades" ' +
      '  inner join "ColaExaminacion" "colaExaminacion" ON "Oportunidades"."idColaExaminacion" = "colaExaminacion"."idColaExaminacion" ' +
      '  inner join "ColaExaminacionTramiteNM" "colaExaminacionNM" ON "colaExaminacionNM"."idColaExaminacion" = "colaExaminacion"."idColaExaminacion" ' +
      '  inner join "Tramites" "tramites" ON "colaExaminacionNM"."idTramite" = "tramites"."idTramite" ' +
      '  inner join "Solicitudes" "solicitudes" ON "tramites"."idSolicitud" = "solicitudes"."idSolicitud" ' +
      '  inner join "Postulantes" "postulante" ON "solicitudes"."idPostulante" = "postulante"."idPostulante" ' +
      '  inner join "TiposTramite" "tiposTramite" ON "tramites"."idTipoTramite" = "tiposTramite"."idTipoTramite" ' +
      '  inner join "EstadosTramite" "estadoTramite" ON "tramites"."idEstadoTramite" = "estadoTramite"."idEstadoTramite" ' +
      '  inner join "TramitesClaseLicencia" "tramitesClaseLicencia" ON "tramites"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '  inner join "ClasesLicencias" "clasesLicencias" ON "clasesLicencias"."idClaseLicencia" = "tramitesClaseLicencia"."idClaseLicencia" ' +
      '  where true ' +
      ' and (("Oportunidades"."oportunidadAprobada" IS NULL)) ' +
      ' and ("solicitudes"."idEstado" != 1) ' +     // diferente a estado borrador 
      ' and ("tramites"."idTramite" = $1 OR $1 IS NULL) ' +
      ' and ("tramites"."idTipoTramite" = $2 OR $2 IS NULL) ' +
      ' and ("tramites"."idEstadoTramite" = $3 OR $3 IS NULL) ' +
      ' and ("postulante"."RUN" = $4 OR $4 IS NULL) ' +
      ' and ("tramitesClaseLicencia"."idClaseLicencia" = $5 OR $5 IS NULL) ' +
      ' and (("tramites"."created_at"::date = $6) OR ($6 IS NULL)) ' +
      ' and ("colaExaminacion"."aprobado" = false)' +

      condicionSoloProrrogas +      // En el caso de ser null buscamos oportunidades, en otro caso, no buscamos oportunidades

      '  union all ' +

      'select ' +
      '     "ProrrogaExaminacion"."idColaExaminacion" as colaexam, ' +
      '     "ProrrogaExaminacion"."nuevaFechaSolicitada" as nuevafecha, ' +
      '     "ProrrogaExaminacion"."idProrrogaExaminacion" as idOportunidadProrroga, ' +
      '     "ProrrogaExaminacion"."fechaIngreso" as fechaIngreso, ' +
      '     "tramites"."idTramite" as idTramite, ' +
      '     "tiposTramite"."Nombre" as nombreTipoTramite, ' +
      '     "estadoTramite"."Nombre" as nombreEstadoTramite, ' +
      '     "postulante"."RUN" As runPostulante, ' +
      '     "clasesLicencias"."Abreviacion" as nombreClaseLicencia, ' +
      '     "solicitudes"."FechaCreacion" AS FechaCreacionSolicitud ' +
      'from "ProrrogaExaminacion" "ProrrogaExaminacion" ' +
      'inner join "ColaExaminacion" "colaExaminacion" ON "ProrrogaExaminacion"."idColaExaminacion" = "colaExaminacion"."idColaExaminacion" ' +
      'inner join "ColaExaminacionTramiteNM" "colaExaminacionNM" ON "colaExaminacionNM"."idColaExaminacion" = "colaExaminacion"."idColaExaminacion" ' +
      'inner join "Tramites" "tramites" ON "colaExaminacionNM"."idTramite" = "tramites"."idTramite" ' +
      'inner join "Solicitudes" "solicitudes" ON "tramites"."idSolicitud" = "solicitudes"."idSolicitud" ' +
      'inner join "Postulantes" "postulante" ON "solicitudes"."idPostulante" = "postulante"."idPostulante" ' +
      'inner join "TiposTramite" "tiposTramite" ON "tramites"."idTipoTramite" = "tiposTramite"."idTipoTramite" ' +
      'inner join "EstadosTramite" "estadoTramite" ON "tramites"."idEstadoTramite" = "estadoTramite"."idEstadoTramite" ' +
      'inner join "TramitesClaseLicencia" "tramitesClaseLicencia" ON "tramites"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      'inner join "ClasesLicencias" "clasesLicencias" ON "clasesLicencias"."idClaseLicencia" = "tramitesClaseLicencia"."idClaseLicencia" ' +
      'where true' +
      ' and ("solicitudes"."idEstado" != 1) ' +     // diferente a estado borrador  
      ' and (("ProrrogaExaminacion"."prorrogaAprobada" IS NULL)) ' +
      ' and ("tramites"."idTramite" = $1 OR $1 IS NULL) ' +
      ' and ("tramites"."idTipoTramite" = $2 OR $2 IS NULL) ' +
      ' and ("tramites"."idEstadoTramite" = $3 OR $3 IS NULL) ' +
      ' and ("postulante"."RUN" = $4 OR $4 IS NULL) ' +
      ' and ("tramitesClaseLicencia"."idClaseLicencia" = $5 OR $5 IS NULL) ' +
      ' and (("tramites"."created_at"::date = $6) OR ($6 IS NULL)) ' +
      ' and (("ProrrogaExaminacion"."nuevaFechaSolicitada"::date = $7) OR ($7 IS NULL)) ' +
      consultaOrdenacion +
      ') AS OportunidadesXProrrogas ' +
      consultaPaginacionContador);
  }

}
