  import { Controller, Get, Post, Body, Param, ParseIntPipe, UsePipes, ValidationPipe, Req } from '@nestjs/common';
import { ProveedorService } from '../services/proveedor.service';
import { CreateProveedorDto } from '../dto/create-proveedor.dto';
import { UpdateProveedorDto } from '../dto/update-proveedor.dto';
import { Resultado } from 'src/utils/resultado';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CambiarEstadoProveedorDto } from '../dto/cambiar-estado-proveedor.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { CreateProveedorRechazo } from '../dto/create-proveedor-rechazo.dto';
import { AuthService } from 'src/auth/auth.service';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';

@Controller('Proveedor')
@ApiTags('Proveedor')
@UsePipes(ValidationPipe) // Habilitamos las validaciones
export class ProveedorController {
  constructor(private readonly proveedorService: ProveedorService, private authService: AuthService) {}

  // #region CRUD BÁSICO Provedores

  @Get()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que devuelve todos los proveedores',
  })
  public async findAll(@Req() req): Promise<{ data: Resultado }> {
    let data = null;
    // const splitBearerToken = req.headers.authorization.split(' ');
    // const token = splitBearerToken[1];
    // const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.GestionarProveedor]);

    // const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    // if (validarPermisos.ResultadoOperacion) {
      data = await this.proveedorService.findAll();
    // } else {
    //   data = validarPermisos;
    // }

    return { data };
  }

  @Get('listado')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que devuelve todos los proveedores listados',
  })
  public async findAllListado(@Req() req): Promise<{ data: Resultado }> {
    let data = null;
    // const splitBearerToken = req.headers.authorization.split(' ');
    // const token = splitBearerToken[1];
    // const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.GestionarProveedor]);

    // const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    // if (validarPermisos.ResultadoOperacion) {
      data = await this.proveedorService.findAllListado();
    // } else {
    //   data = validarPermisos;
    // }

    return { data };
  }

  @Post('paginado')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que devuelve todos los proveedores paginados',
  })
  public async findAllPaginate(@Req() req, @Body() paginacionArgs: PaginacionArgs): Promise<{ data: Resultado }> {
    let data = null;

    data = await this.proveedorService.findAllPaginate(req, paginacionArgs);
    return { data };
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que devuelve un proveedore',
  })
  public async findOne(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCFabricantedePapeldeSeguridadVisualizarFabricante]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.proveedorService.findOne(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('crearProveedor')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que crea un proveedor',
  })
  public async create(@Body() createProveedorDto: CreateProveedorDto, @Req() req): Promise<{ data: Resultado }> {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCFabricantedePapeldeSeguridadCrearNuevoFabricante]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const idUsuario =  ( await this.authService.usuario() ).idUsuario;  //validarPermisos.Respuesta[0].idUsuario;
      data = await this.proveedorService.create(createProveedorDto, idUsuario);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('editarProveedor/:id')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que actualiza un proveedor',
  })
  public async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateProveedorDto: UpdateProveedorDto,
    @Req() req
  ): Promise<{ data: Resultado }> {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCFabricantedePapeldeSeguridadEditarFabricante]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const idUsuario = validarPermisos.Respuesta[0].idUsuario;
      data = await this.proveedorService.update(id, updateProveedorDto, idUsuario);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('cambiarEstadoProveedor/:id')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que cambia el estado de un proveedor',
  })
  public async carmbiarEstado(
    @Req() req,
    @Param('id', ParseIntPipe) id: number,
    @Body() cambiarEstadoProveedorDto: CambiarEstadoProveedorDto
  ) {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCFabricantedePapeldeSeguridadEditarFabricante]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.proveedorService.cambiarEstado(id, cambiarEstadoProveedorDto);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('rechazarProveedor/:id')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que crea un motivo de rechazo para un proveedor',
  })
  public async rechazarProveedor(
    @Req() req,
    @Param('id', ParseIntPipe) id: number,
    @Body() createProveedorRechazo: CreateProveedorRechazo
  ): Promise<{ data: Resultado }> {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCFabricantedePapeldeSeguridadEditarFabricante]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.proveedorService.rechazar(id, createProveedorRechazo);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('eliminarProveedor/:id')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que elimina un proveedor',
  })
  public async remove(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCFabricantedePapeldeSeguridadEditarFabricante]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.proveedorService.remove(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  // #endregion
}
