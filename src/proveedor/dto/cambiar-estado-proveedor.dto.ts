import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty } from 'class-validator';

export class CambiarEstadoProveedorDto {
  @ApiProperty({
    name: 'activo',
    type: 'boolean',
    required: true,
    example: false,
  })
  @IsNotEmpty({ message: 'activo es obligatorio' })
  @IsBoolean({ message: 'activo debe ser un boolean' })
  activo: boolean;
}
