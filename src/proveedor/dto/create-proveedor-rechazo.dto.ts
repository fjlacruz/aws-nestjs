import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsPositive, IsString, MaxLength } from 'class-validator';

export class CreateProveedorRechazo {
  @ApiProperty({
    name: 'motivo',
    type: 'string',
    example: 'Motivo de rechazo para el proveedor',
    required: true,
  })
  @IsNotEmpty({ message: 'motivo es obligatorio' })
  @IsString({ message: 'motivo debe ser un string' })
  @MaxLength(100, { message: 'motivo debe tener menos de 100 caracteres' })
  motivo: string;
}
