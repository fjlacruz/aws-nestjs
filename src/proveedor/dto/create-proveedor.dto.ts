import { ApiProperty } from '@nestjs/swagger';
import { IsEmpty, IsNotEmpty, IsNumber, IsPositive, IsString, Matches, MaxLength } from 'class-validator';
import { isValidRut } from 'src/shared/ValidarRun';


export class CreateProveedorDto {
  @ApiProperty({
    name: 'código',
    type: 'string',
    example: 'Fabricante Papel',
    required: true,
  })
  @IsNotEmpty({ message: 'código es obligatorio' })
  @IsString({ message: 'código debe ser un string' })
  @MaxLength(10, { message: 'código debe tener menos de 50 caracteres' })
  @Matches(/[A-Za-zÁÉÍÓÚáéíóúñÑ]/gm, { message: 'formato de codigo invalido' })
  codigo: string;

  @ApiProperty({
    name: 'nombre',
    type: 'string',
    example: 'Fabricante Papel',
    required: true,
  })
  @IsNotEmpty({ message: 'nombre es obligatorio' })
  @IsString({ message: 'nombre debe ser un string' })
  @MaxLength(50, { message: 'nombre debe tener menos de 50 caracteres' })
  nombre: string;

  @ApiProperty({
    name: 'rut',
    type: 'integer',
    example: 185455971,
    required: true,
  })
  @IsNotEmpty({ message: 'rut es obligatorio' })
  @IsString({ message: 'rut debe ser un string' })
  @isValidRut({ message: 'formato de rut invalido' })
  rutCompleto: string;

  @ApiProperty({
    name: 'calleDireccion',
    type: 'string',
    example: 'Calle',
    required: true,
  })
  @IsNotEmpty({ message: 'Calle es obligatorio' })
  @IsString({ message: 'Calle debe ser un string' })
  calleDireccion: number;

  @ApiProperty({
    name: 'nroDireccion',
    type: 'integer',
    example: 1,
    required: true,
  })
  @IsNotEmpty({ message: 'nroDireccion es obligatorio' })
  @IsNumber({ allowNaN: false, allowInfinity: false, maxDecimalPlaces: 0 }, { message: 'nroDireccion debe ser un número' })
  @IsPositive({ message: 'nroDireccion debe ser positivo' })
  nroDireccion: number;


  @ApiProperty({
    name: 'letraDireccion',
    type: 'string',
    example: 'A',
  })
  @IsString({ message: 'Letra debe ser un string' })
  letraDireccion: number;

  @ApiProperty({
    name: 'restoDireccion',
    type: 'string',
    example: 'Resto',
  })
  @IsString({ message: 'Resto debe ser un string' })
  restoDireccion: number;

  @ApiProperty({
    name: 'activo',
    type: 'boolean',
    example: false,
  })
  activo: boolean;

  @ApiProperty({
    name: 'idRegion',
    type: 'integer',
    example: 1,
    required: true,
  })
  @IsNotEmpty({ message: 'idRegion es obligatorio' })
  @IsNumber({ allowNaN: false, allowInfinity: false, maxDecimalPlaces: 0 }, { message: 'idRegion debe ser un número' })
  @IsPositive({ message: 'idRegion debe ser positivo' })
  idRegion: number;

  @ApiProperty({
    name: 'idComuna',
    type: 'integer',
    example: 1,
    required: true,
  })
  @IsNotEmpty({ message: 'idComuna es obligatorio' })
  @IsNumber({ allowNaN: false, allowInfinity: false, maxDecimalPlaces: 0 }, { message: 'idComuna debe ser un número' })
  @IsPositive({ message: 'idComuna debe ser positivo' })
  idComuna: number;

  @IsEmpty({ message: 'idProveedor debe estar vacio' })
  idTipoProveedor: number;
}
