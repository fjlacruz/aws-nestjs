export class ProveedorPaginateDTO {
  idProveedor: number;
  codigo: string;
  nombre: string;
  activo: boolean;
  direccion: string;
  nombreRegion: any;
  nombreComuna: any;
}
