import { JoinColumn } from 'typeorm';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Proveedor } from './proveedor.entity';

@Entity('ProveedoresRechazo')
export class ProveedoresRechazo {
  @PrimaryGeneratedColumn({
    name: 'idProveedorRechazo',
    type: 'integer',
    unsigned: true,
  })
  idProveedorRechazo: number;

  @Column({
    name: 'motivo',
    type: 'character varying',
    length: 100,
    nullable: false,
  })
  motivo: string;

  @Column({
    name: 'idProveedor',
    type: 'integer',
    unsigned: true,
    nullable: false,
  })
  idProveedor: number;

  @OneToOne(() => Proveedor, motivosRechazo => motivosRechazo.motivoRechazo)
  @JoinColumn({ name: 'idProveedor' })
  readonly proveedor: Proveedor;
}
