import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { LotePapelSeguridadEntity } from 'src/folio/entity/lotePapelSeguridad.entity';
import { RegionesEntity } from 'src/regiones/entity/regiones.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ProveedoresRechazo } from './proveedor-rechazo.entity';

@Entity('Proveedores')
export class Proveedor {
  @PrimaryGeneratedColumn({
    name: 'idProveedor',
    type: 'integer',
    unsigned: true,
  })
  idProveedor: number;

  @Column({ name: 'codigo', type: 'varchar', length: 10 })
  codigo: string;

  @Column({ name: 'nombre', type: 'varchar', length: 50 })
  nombre: string;

  @Column({ name: 'RUT', type: 'integer', unsigned: true })
  rut: number;

  @Column({ name: 'DV', type: 'varchar', length: 1 })
  dv: string;

  @Column({ name: 'direccion', type: 'varchar', length: 50 })
  direccion: string;

  @Column({ name: 'calleDireccion', type: 'varchar', length: 50  })
  calleDireccion: number;

  @Column({ name: 'nrodireccion', type: 'integer', unsigned: true })
  nroDireccion: number;

  @Column({ name: 'letraDireccion', type: 'varchar',  length: 3  })
  letraDireccion: number;

  @Column({ name: 'restoDireccion', type: 'varchar',  length: 50 })
  restoDireccion: number;

  @Column({ name: 'activo', type: 'boolean', default: false })
  activo: boolean;

  @Column({
    name: 'idUsuario',
    type: 'integer',
    unsigned: true,
    nullable: false,
    default: 1,
  })
  idUsuario: number;

  @Column({
    name: 'idRegion',
    type: 'integer',
    unsigned: true,
    nullable: false,
  })
  idRegion: number;

  @Column({
    name: 'idComuna',
    type: 'integer',
    unsigned: true,
    nullable: false,
  })
  idComuna: number;

  @Column({
    name: 'idTipoProveedor',
    type: 'integer',
    unsigned: true,
    nullable: false,
  })
  idTipoProveedor: number;

  @ManyToOne(() => RegionesEntity, region => region.proveedores)
  @JoinColumn({ name: 'idRegion' })
  readonly region: RegionesEntity;

  @ManyToOne(() => ComunasEntity, comuna => comuna.proveedores)
  @JoinColumn({ name: 'idComuna' })
  readonly comuna: ComunasEntity;

  @OneToOne(() => ProveedoresRechazo, rechazo => rechazo.proveedor)
  readonly motivoRechazo: ProveedoresRechazo;

  @OneToMany(() => LotePapelSeguridadEntity, lote => lote.proveedor)
  readonly lote: LotePapelSeguridadEntity[];
}
