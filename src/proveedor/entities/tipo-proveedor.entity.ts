import { Exclude, Expose } from 'class-transformer';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tipoProveedor')
@Exclude()
export class TipoProveedor {
  @PrimaryGeneratedColumn({
    name: 'idTipoProveedor',
    type: 'integer',
    unsigned: true,
  })
  @Expose()
  idTipoProveedor: number;

  @Column()
  @Expose()
  nombre: string;

  @Column()
  @Expose()
  descripcion: string;
}
