import { Exclude, Expose, Transform } from 'class-transformer';
import { ProveedoresRechazo } from '../entities/proveedor-rechazo.entity';
import { Proveedor } from '../entities/proveedor.entity';

export class ProveedorTransformer {
  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'listado', 'paginado'] })
  idProveedor: number;

  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'listado', 'paginado'] })
  codigo: string;

  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'listado', 'paginado'] })
  nombre: string;

  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'toShowOne'] })
  @Transform(value => {
    const proveedor = value.obj as Proveedor;
    return `${proveedor.rut}-${proveedor.dv}`;
  })
  rut: number;

  @Exclude()
  dv: string;

  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'paginado'] })
  @Transform(
    value => {
      const proveedor = value.obj as Proveedor;
      return `${proveedor.direccion}-${proveedor.nroDireccion}`;
    },
    { groups: ['paginado'] }
  )
  direccion: string;

  @Expose({ groups: ['all', 'toShow', 'toShowOne'] })
  calleDireccion: string;
  @Expose({ groups: ['all', 'toShow', 'toShowOne'] })
  nroDireccion: number;
  @Expose({ groups: ['all', 'toShow', 'toShowOne'] })
  letraDireccion: string;
  @Expose({ groups: ['all', 'toShow', 'toShowOne'] })
  restoDireccion: string;

  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'paginado'] })
  activo: boolean;

  @Expose({ groups: ['all', 'toShow', 'toShowOne'] })
  idUsuario: number;

  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'paginado'] })
  idRegion: number;

  @Expose({ groups: ['all', 'toShow', 'toShowOne', 'paginado'] })
  idComuna: number;

  @Expose({ groups: ['all'] })
  idTipoProveedor: number;

  @Expose({ groups: ['toShowOne'] })
  @Transform(
    value => {
      const proveedorRechazo = value.obj.motivoRechazo as ProveedoresRechazo;
      const motivo = proveedorRechazo ? proveedorRechazo.motivo : '';
      return motivo;
    },
    { groups: ['toShowOne'] }
  )
  motivoRechazo: string;
}
