import { forwardRef, Module } from '@nestjs/common';
import { ProveedorService } from './services/proveedor.service';
import { ProveedorController } from './controller/proveedor.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

// * Modules
import { RegionesModule } from 'src/regiones/regiones.module';
import { ComunasModule } from 'src/comunas/comunas.module';
import { UsersModule } from 'src/users/users.module';
import { SharedModule } from 'src/shared/shared.module';
import { AuthModule } from 'src/auth/auth.module';

// * Entities
import { Proveedor } from './entities/proveedor.entity';
import { TipoProveedor } from './entities/tipo-proveedor.entity';
import { ProveedoresRechazo } from './entities/proveedor-rechazo.entity';

@Module({
  imports: [
    SharedModule,
    TypeOrmModule.forFeature([TipoProveedor, ProveedoresRechazo, Proveedor]),
    forwardRef(() => RegionesModule),
    forwardRef(() => ComunasModule),
    forwardRef(() => UsersModule),
    AuthModule,
  ],
  controllers: [ProveedorController],
  providers: [ProveedorService],
})
export class ProveedorModule {}
