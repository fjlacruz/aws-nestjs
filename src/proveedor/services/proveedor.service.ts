import { RegionesEntity } from './../../escuela-conductores/entity/regiones.entity';
import { ColumnasProveedores, PermisosNombres, relacionProveedores, TipoProveedor_FabricaPapelSeguridad } from './../../constantes';
import { ProveedoresRechazo } from '../entities/proveedor-rechazo.entity';
import { Proveedor } from './../entities/proveedor.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { paginate, paginateRaw, Pagination } from 'nestjs-typeorm-paginate';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { ComunasService } from 'src/comunas/services/comunas.service';
import { RegionesService } from 'src/regiones/services/regiones.service';
import { UsersService } from 'src/users/services/users.service';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { Resultado } from 'src/utils/resultado';
import { DeleteResult, getConnection, InsertResult, QueryRunner, Repository, UpdateResult } from 'typeorm';
import { CambiarEstadoProveedorDto } from '../dto/cambiar-estado-proveedor.dto';
import { CreateProveedorRechazo } from '../dto/create-proveedor-rechazo.dto';
import { CreateProveedorDto } from '../dto/create-proveedor.dto';
import { UpdateProveedorDto } from '../dto/update-proveedor.dto';
import { TipoProveedor } from '../entities/tipo-proveedor.entity';
import { SetResultadoService } from 'src/shared/services/set-resultado/set-resultado.service';
import { ProveedorPaginateDTO } from '../dto/proveedor-paginate.dto';
import { ProveedorTransformer } from '../mapper/proveedor.transformer';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class ProveedorService {
  constructor(
    @InjectRepository(Proveedor)
    private readonly proveedorRepository: Repository<Proveedor>,
    @InjectRepository(TipoProveedor)
    private readonly tipoProveedorRepository: Repository<TipoProveedor>,
    @InjectRepository(ProveedoresRechazo)
    private readonly proveedoresRchazoRepository: Repository<ProveedoresRechazo>,
    private readonly regionesService: RegionesService,
    private readonly comunasService: ComunasService,
    private readonly usersService: UsersService,
    private readonly setResultadoService: SetResultadoService,
    private readonly authService: AuthService
  ) {}

  // #region CRUD BÁSICO

  public async findAll(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    await this.proveedorRepository
      .createQueryBuilder()
      .getMany()
      .then((res: Proveedor[]) => {
        // * Mapeamos los resultados
        const proveedores: ProveedorTransformer[] = plainToClass(ProveedorTransformer, [...res], {
          groups: ['toShow'],
        });

        if (Array.isArray(proveedores) && proveedores.length) {
          // * Modificamos resultados para respuesta con datos
          this.setResultadoService.setResponse<ProveedorTransformer[]>(resultado, proveedores);
        } else {
          // * Modificamos resultados para respuesta sin datos
          this.setResultadoService.setResponse<null>(resultado, null, false, 'No hay fabricantes para mostrar');
        }
      })
      .catch((err: Error) => {
        // * Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'No se ha podido cargar los fabricantes');
      });

    return resultado;
  }

  public async findAllListado(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    await this.proveedorRepository
      .createQueryBuilder()
      .getMany()
      .then((res: Proveedor[]) => {
        // * Mapeamos los resultados
        const proveedores: ProveedorTransformer[] = plainToClass(ProveedorTransformer, [...res], {
          groups: ['listado'],
        });

        if (Array.isArray(proveedores) && proveedores.length) {
          // * Modificamos resultados para respuesta con datos
          this.setResultadoService.setResponse<ProveedorTransformer[]>(resultado, proveedores);
        } else {
          // * Modificamos resultados para respuesta sin datos
          this.setResultadoService.setResponse<string>(resultado, 'No hay fabricantes para listar', false);
        }
      })
      .catch((err: Error) => {
        // * Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'No se ha podido cargar los fabricantes listados');
      });

    return resultado;
  }

  public async findAllPaginate(req, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    console.log('buscango proceedores..................');
    const resultado: Resultado = new Resultado();

    let orden = paginacionArgs.orden;
    let filtro = paginacionArgs.filtro;

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaAdministracionFabricantesPapelSeguridad);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      const qbSolicitudes = this.proveedorRepository
        .createQueryBuilder('Proveedores')
        .innerJoinAndMapOne('Proveedores.idComuna', ComunasEntity, 'comunas', 'comunas.idComuna = Proveedores.idComuna')
        .innerJoinAndMapOne('Proveedores.idRegion', RegionesEntity, 'regiones', 'regiones.idRegion = Proveedores.idRegion');

      // * Filtros
      if (filtro.nombre) {
        qbSolicitudes.andWhere('lower(unaccent(Proveedores.nombre)) like lower(unaccent(:nombre))', {
          nombre: `%${filtro.nombre}%`,
        });
      }
      if (filtro.codigo) {
        qbSolicitudes.andWhere('Proveedores.codigo = :codigo', {
          codigo: `%${filtro.codigo}%`,
        });
      }
      if (filtro.calle) {
        qbSolicitudes.andWhere('lower(unaccent(Proveedores.calleDireccion)) like lower(unaccent(:calle))', {
          calle: `%${filtro.calle}%`,
        });
      }
      if (filtro.numero) {
        qbSolicitudes.andWhere('Proveedores.nroDireccion = :numero', {
          numero: filtro.numero,
        });
      }
      if (filtro.region) {
        qbSolicitudes.andWhere('Proveedores.idRegion = :region', {
          region: filtro.region,
        });
      }
      if (filtro.comuna) {
        qbSolicitudes.andWhere('Proveedores.idComuna = :comuna', {
          comuna: filtro.comuna,
        });
      }
      if (filtro.estado) {
        //Filtramos tambien el estado Pendiente(valor null)
        if (filtro.estado == 'Pendiente') {
          qbSolicitudes.andWhere('Proveedores.activo is null', {
            estado: filtro.estado,
          });
        } else {
          qbSolicitudes.andWhere('Proveedores.activo = :estado', {
            estado: filtro.estado,
          });
        }
      }

      // * Ordenación
      if (orden.orden === 'nombre' && orden.ordenarPor === 'ASC') qbSolicitudes.orderBy('Proveedores.nombre', 'ASC');
      if (orden.orden === 'nombre' && orden.ordenarPor === 'DESC') qbSolicitudes.orderBy('Proveedores.nombre', 'DESC');
      if (orden.orden === 'codigo' && orden.ordenarPor === 'ASC') qbSolicitudes.orderBy('Proveedores.codigo', 'ASC');
      if (orden.orden === 'codigo' && orden.ordenarPor === 'DESC') qbSolicitudes.orderBy('Proveedores.codigo', 'DESC');
      if (orden.orden === 'direccion' && orden.ordenarPor === 'ASC') qbSolicitudes.orderBy('Proveedores.calleDireccion', 'ASC');
      if (orden.orden === 'direccion' && orden.ordenarPor === 'DESC') qbSolicitudes.orderBy('Proveedores.calleDireccion', 'DESC');
      if (orden.orden === 'region' && orden.ordenarPor === 'ASC') qbSolicitudes.orderBy('regiones.Nombre', 'ASC');
      if (orden.orden === 'region' && orden.ordenarPor === 'DESC') qbSolicitudes.orderBy('regiones.Nombre', 'DESC');
      if (orden.orden === 'comuna' && orden.ordenarPor === 'ASC') qbSolicitudes.orderBy('comunas.Nombre', 'ASC');
      if (orden.orden === 'comuna' && orden.ordenarPor === 'DESC') qbSolicitudes.orderBy('comunas.Nombre', 'DESC');
      if (orden.orden === 'estado' && orden.ordenarPor === 'ASC') qbSolicitudes.orderBy('Proveedores.activo', 'ASC');
      if (orden.orden === 'estado' && orden.ordenarPor === 'DESC') qbSolicitudes.orderBy('Proveedores.activo', 'DESC');

      const proveedoresPaginados: any = await paginate<Proveedor>(qbSolicitudes, paginacionArgs.paginationOptions);

      const items = proveedoresPaginados.items;
      if (Array.isArray(items) && items.length) {
        const response = {
          items: items,
          links: proveedoresPaginados.links,
          meta: proveedoresPaginados.meta,
        };

        // * Modificamos resultado para respuesta
        this.setResultadoService.setResponse<any>(resultado, response);
      } else {
        throw new Error('No hay fabricantes para paginar');
      }
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No hay fabricantes para paginar');
    } finally {
      return resultado;
    }
  }

  public async findOne(id: number, group: string = 'toShowOne'): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    await this.proveedorRepository
      .createQueryBuilder('p')
      .leftJoinAndSelect('p.motivoRechazo', 'mr')
      .where({ idProveedor: id })
      .getOneOrFail()
      .then((res: Proveedor) => {
        // * Mapeamos los resultados
        const proveedor: ProveedorTransformer = plainToClass(
          ProveedorTransformer,
          { ...res },
          {
            groups: [group],
          }
        );

        // * Modificamos resultados para respuesta
        this.setResultadoService.setResponse<ProveedorTransformer>(resultado, proveedor);
      })
      .catch((err: Error) => {
        // * Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'No se ha podido cargar el fabricante');
      });

    return resultado;
  }

  public async create(createProveedorDto: CreateProveedorDto, idUsuario: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    createProveedorDto.idTipoProveedor = TipoProveedor_FabricaPapelSeguridad;

    try {
      const {
        nombre,
        codigo,
        rutCompleto,
        calleDireccion,
        nroDireccion,
        letraDireccion,
        restoDireccion,
        activo,
        idRegion,
        idComuna,
        idTipoProveedor,
      } = createProveedorDto;

      // * Comprobamos que las FK existen en la DB
      if (
        (await this.userExists(idUsuario)) &&
        (await this.tipoProveedorExists(idTipoProveedor)) &&
        (await this.regionExists(idRegion)) &&
        (await this.comunaExists(idComuna)) &&
        (await this.isComunaByRegion(idRegion, idComuna))
      ) {
        const rutSplit = rutCompleto.split('-');
        const rut = parseInt(rutSplit[0]) as number;
        const dv = rutSplit[1];

        const newProveedor = this.proveedorRepository.create({
          nombre,
          codigo,
          rut,
          dv,
          calleDireccion,
          nroDireccion,
          letraDireccion,
          restoDireccion,
          activo,
          idUsuario,
          idTipoProveedor,
          idRegion,
          idComuna,
        });

        if (!newProveedor) throw new Error('Error al crear la entidad fabricante');

        await this.proveedorRepository
          .createQueryBuilder()
          .insert()
          .values(newProveedor)
          .execute()
          .then((res: InsertResult) => {
            if (res.identifiers.length > 0) {
              // * Modificamos resultado para respuesta
              this.setResultadoService.setResponse<null>(resultado, null, true, 'Fabricante creado con éxito');
            } else {
              // * Lanzamos error
              throw new Error('No se ha insertado ningun proveedor');
            }
          })
          .catch((err: Error) => {
            // * Modificamos resultado para errores
            throw new Error('No se ha podido añadir fabricante');
          });
      }
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido crear el fabricante');
    } finally {
      return resultado;
    }
  }

  public async update(id: number, updateProveedorDto: UpdateProveedorDto, idUsuario: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    updateProveedorDto.idTipoProveedor = TipoProveedor_FabricaPapelSeguridad;

    try {
      const {
        nombre,
        codigo,
        rut,
        calleDireccion,
        nroDireccion,
        letraDireccion,
        restoDireccion,
        activo,
        isPendiente,
        idRegion,
        idComuna,
        idTipoProveedor,
      } = updateProveedorDto;

      // * Comprobamos los ids que se van a modificar
      // * y si existen en su correspondiente tabla
      if (idUsuario > 0) await this.userExists(idUsuario);
      if (idRegion > 0) await this.regionExists(idRegion);
      if (idComuna > 0) await this.comunaExists(idComuna);
      // * Comprobamos que la comuna pertenece a la región
      if (idRegion > 0 && idComuna > 0) {
        await this.isComunaByRegion(idRegion, idComuna);
      } else {
        const proveedor: Proveedor = (await this.findOne(id, 'all')).Respuesta as Proveedor;
        if (idRegion > 0) await this.isComunaByRegion(idRegion, proveedor.idComuna);
        if (idComuna > 0) await this.isComunaByRegion(proveedor.idRegion, idComuna);
      }

      const rutSplit = rut.split('-');
      const rutToModify = parseInt(rutSplit[0]) as number;
      const dv = rutSplit[1];

      await this.proveedorRepository
        .createQueryBuilder()
        .update()
        .set({
          nombre,
          codigo,
          rut: rutToModify,
          dv,
          calleDireccion,
          nroDireccion,
          letraDireccion,
          restoDireccion,
          activo,
          idUsuario,
          idTipoProveedor,
          idRegion,
          idComuna,
        })
        .where({ idProveedor: id })
        .execute()
        .then((res: UpdateResult) => {
          if (res.affected > 0) {
            // * Controlamos que no se vaya a reasignar el estado a Pendiente
            if (isPendiente == false && activo == null) {
              // * Si se vuelve a reasignar Pendiente, devolvemos error
              throw new Error('No se puede volver a asignar Pendiente a un fabricante');
            } else {
              // * Modificamos resultado para respuesta
              this.setResultadoService.setResponse<null>(resultado, null, true, 'Fabricante actualizado con éxito');
            }
          } else {
            // * Si no ha modificado ninguna devolvemos error
            throw new Error('No se ha modificado ninguna fila');
          }
        })
        .catch((err: Error) => {
          // * Lanzamos error
          throw err;
        });
    } catch (error) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, error.message);
    } finally {
      return resultado;
    }
  }

  public async cambiarEstado(id: number, cambiarEstadoProveedorDto: CambiarEstadoProveedorDto) {
    const resultado: Resultado = new Resultado();

    await this.proveedorRepository
      .createQueryBuilder()
      .update()
      .set(cambiarEstadoProveedorDto)
      .where({ idProveedor: id })
      .execute()
      .then((res: UpdateResult) => {
        if (res.affected > 0) {
          // * Modificamos resultado para respuesta
          this.setResultadoService.setResponse<null>(resultado, null, true, 'Estado fabricante cambiado con éxito');
        } else {
          // * Si no ha modificado ninguna devolvemos error
          throw new Error('No se ha podido modificar el estado del fabricante');
        }
      })
      .catch((err: Error) => {
        // * Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'No se ha podido modificar activar/desactivar/rechazar el fabricante');
      });

    return resultado;
  }

  public async rechazar(id: number, createProveedorRechazo: CreateProveedorRechazo): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    // * Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const proveedor = await this.findOne(id, 'all');
      const { motivo } = createProveedorRechazo;

      // * Si no existe el proveedor lanzamos error
      if (!proveedor.ResultadoOperacion) throw new Error(proveedor.Error);

      // * Actualizamos el estado del Proveedor a false (Rechazado)
      await queryRunner.manager
        .createQueryBuilder()
        .update(Proveedor)
        .set({ activo: false })
        .where({ idProveedor: id })
        .execute()
        .then((res: UpdateResult) => {
          if (res.affected <= 0) throw new Error('No se ha actualizado ninguna fila');
        })
        .catch((err: Error) => {
          throw new Error(err.message);
        });

      // * Si ya tiene un motivo lo actualizamos y si no lo creamos
      if (await this.hasMotivoRechazo(id)) {
        await this.updateMotivoRechazo(queryRunner, id, motivo);
      } else {
        await this.createMotivoRechazo(queryRunner, id, motivo);
      }

      // * Commiteamos la transaccion
      queryRunner.commitTransaction();

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Fabricante rechazado con éxito');
    } catch (err) {
      // * Revertimos los cambios
      queryRunner.rollbackTransaction();

      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido añadir el motivo de rechazo del fabricante');
    } finally {
      await queryRunner.release();
      return resultado;
    }
  }

  public async remove(id: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    // * Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await this.findOne(id, 'all');

      // * Si tiene motivo de rechazo se elimina
      if (await this.hasMotivoRechazo(id)) {
        await this.deleteMotivoRechazo(queryRunner, id);
      }

      await queryRunner.manager
        .createQueryBuilder()
        .delete()
        .from(Proveedor)
        .where({ idProveedor: id })
        .execute()
        .then((res: DeleteResult) => {
          // * Comprobamos que el eliminar ha afectado alguna fila
          if (res.affected > 0) {
            this.setResultadoService.setResponse<null>(resultado, null, true, 'Fabricante eliminado con éxito');
          } else {
            throw new Error('No se ha eliminado ninguna fila');
          }
        })
        .catch((err: Error) => {
          throw new Error(err.message);
        });

      // * Commiteamos la transaccion
      queryRunner.commitTransaction();
    } catch (err) {
      // * Revertimos los cambios
      queryRunner.rollbackTransaction();

      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido elimininar el fabricante');
    } finally {
      await queryRunner.release();
      return resultado;
    }
  }

  // #endregion

  private async regionExists(id: number): Promise<boolean> {
    return await this.regionesService
      .getRegion(id)
      .then(() => true)
      .catch((err: Error) => {
        throw new Error('La región introducida es incorrecta');
      });
  }

  private async comunaExists(id: number): Promise<boolean> {
    return await this.comunasService
      .getComuna(id)
      .then(() => true)
      .catch((err: Error) => {
        throw new Error('La comuna introducida es incorrecta');
      });
  }

  private async isComunaByRegion(idRegion: number, idComuna: number): Promise<boolean> {
    return await this.comunasService
      .getComunaByRegion(idRegion)
      .then((comunas: ComunasEntity[]) => {
        const comuna = comunas.filter(filter => filter.idComuna === idComuna);

        if (comuna.length > 0) return true;

        throw new Error('La comuna introducida no pertenece a la región');
      })
      .catch((err: Error) => {
        throw new Error(err.message);
      });
  }

  private async userExists(id: number): Promise<boolean> {
    return await this.usersService
      .getUser(id)
      .then(() => true)
      .catch((err: Error) => {
        throw new Error('El usuario introducido es incorrecto');
      });
  }

  private async tipoProveedorExists(id: number): Promise<boolean> {
    return await this.tipoProveedorRepository
      .createQueryBuilder()
      .where({ idTipoProveedor: id })
      .getOneOrFail()
      .then(() => true)
      .catch((err: Error) => {
        throw new Error('El tipo de proveedor introducido es incorrecto');
      });
  }

  private async hasMotivoRechazo(id: number): Promise<boolean> {
    return await this.proveedoresRchazoRepository
      .createQueryBuilder()
      .where({ idProveedor: id })
      .getOneOrFail()
      .then((res: ProveedoresRechazo) => {
        let hasMotivo = false;

        if (res) {
          hasMotivo = true;
        }

        return hasMotivo;
      })
      .catch((err: Error) => {
        return false;
      });
  }

  private async createMotivoRechazo(queryRunner: QueryRunner, idProveedor: number, motivo: string): Promise<any> {
    return await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(ProveedoresRechazo)
      .values({ idProveedor, motivo })
      .execute()
      .then((res: InsertResult) => {
        if (res.identifiers.length <= 0) throw new Error('No se ha insertado ningun rechado de proveedor');
      })
      .catch((err: Error) => {
        throw new Error(err.message);
      });
  }

  private async updateMotivoRechazo(queryRunner: QueryRunner, id: number, motivo: string): Promise<any> {
    return await queryRunner.manager
      .createQueryBuilder()
      .update(ProveedoresRechazo)
      .set({ motivo })
      .where({ idProveedor: id })
      .execute()
      .then((res: UpdateResult) => {
        if (res.affected <= 0) throw new Error('No se ha actualizado el motivo de rechazo del proveedor');
      })
      .catch((err: Error) => {
        throw new Error(err.message);
      });
  }

  private async deleteMotivoRechazo(queryRunner: QueryRunner, idProveedor: number): Promise<any> {
    return await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(ProveedoresRechazo)
      .where({ idProveedor })
      .execute()
      .then((res: DeleteResult) => {
        // * Comprobamos que al eliminar ha afectado alguna fila
        if (res.affected <= 0) {
          throw new Error('Error al eliminar Proveedor Motivo Rechazo');
        }

        return true;
      })
      .catch((err: Error) => {
        throw new Error(err.message);
      });
  }

  // #endregion
}
