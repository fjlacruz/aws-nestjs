import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateRegionDto {

    @ApiPropertyOptional()
    idRegion:number;

    @ApiPropertyOptional()
    Nombre:string;

    @ApiPropertyOptional()
    Descripcion:string;
}
