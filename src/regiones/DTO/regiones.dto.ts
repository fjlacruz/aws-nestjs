export class RegionDTO {
    idRegion: number;
    nombre: string;
    descripcion: string;
}
