import { Controller, Req } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateRegionDto } from '../DTO/createRegion.dto';
import { RegionesEntity } from '../entity/regiones.entity';
import { RegionesService } from '../services/regiones.service';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { Resultado } from 'src/utils/resultado';
import { AuthService } from 'src/auth/auth.service';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';


@ApiTags('Regiones')
@Controller('Regiones')
export class RegionesController {

    constructor(
        private readonly regionesService: RegionesService,
        private readonly authService: AuthService,
    ) { }

    @Get('regiones')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las regiones' })
    async getRegiones() {
        const data = await this.regionesService.getRegiones();
        return { data };
    }

    @Get('regionById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Region por Id' })
    async regionById(@Param('id') id: number) {
        const data = await this.regionesService.getRegion(id);
        return { data };
    }

    @Get('regionesListado')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las regiones en la respuesta' })
    async regionesListado() {
        const data = await this.regionesService.getRegionesListado();
        return { data };
    }

    @Get('regionDTO/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Region por Id en la respuesta' })
    async regionDTO(@Param('id') id: number) {
        const data = await this.regionesService.getRegionDTO(id);
        return { data };
    }

    @Get('regionByComuna/:idComuna')
    @ApiBearerAuth()
    @ApiOperation({summary: 'Servicio que devuelve la region segun una comuna'})
    public async regionByComuna(@Req() req, @Param('idComuna') id: number) : Promise<{ data: Resultado }> {
        let data = null;

        data = await this.regionesService.getRegionByComuna(id);
    
        return { data };
    }

    @Patch('regionUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de una Region' })
    async regionUpdate(@Param('id') id: number, @Body() data: Partial<CreateRegionDto>) {
        return await this.regionesService.update(id, data);
    }

    @Post('crearRegion')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una nueva Region' })
    async addRegion(
        @Body() createRegionDto: CreateRegionDto) {
        const data = await this.regionesService.create(createRegionDto);
        return {data};
    }

}
