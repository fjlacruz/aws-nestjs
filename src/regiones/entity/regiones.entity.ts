import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { Proveedor } from 'src/proveedor/entities/proveedor.entity';

@Entity('Regiones')
export class RegionesEntity {
  @PrimaryColumn()
  idRegion: number;

  @Column()
  Nombre: string;

  @Column()
  Descripcion: string;

  @OneToMany(() => Proveedor, proveedores => proveedores.region)
  readonly proveedores: Proveedor[];

  @OneToMany(() => ComunasEntity, comuna => comuna.regiones)
  readonly comunas: ComunasEntity[];
}
