import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { SharedModule } from 'src/shared/shared.module';
import { RegionesController } from './controller/regiones.controller';
import { RegionesEntity } from './entity/regiones.entity';
import { RegionesService } from './services/regiones.service';


@Module({
  imports: [TypeOrmModule.forFeature([RegionesEntity]), SharedModule, AuthModule],
  providers: [RegionesService],
  exports: [RegionesService],
  controllers: [RegionesController]
})
export class RegionesModule {}
