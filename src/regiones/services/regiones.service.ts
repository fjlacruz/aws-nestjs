import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SetResultadoService } from 'src/shared/services/set-resultado/set-resultado.service';
import { Resultado } from 'src/utils/resultado';
import { Repository, UpdateResult } from 'typeorm';
import { CreateRegionDto } from '../DTO/createRegion.dto';
import { RegionDTO } from '../DTO/regiones.dto';
import { RegionesEntity } from '../entity/regiones.entity';


@Injectable()
export class RegionesService {

    constructor(
        @InjectRepository(RegionesEntity) private readonly regionesRespository: Repository<RegionesEntity>,
        private readonly setResultadoService: SetResultadoService
    ) { }

    async getRegiones() {
        return await this.regionesRespository.find();
    }


    async getRegion(id:number){
        const region= await this.regionesRespository.findOne(id);
        if (!region)throw new NotFoundException("region dont exist");
        
        return region;
    }

    async getRegionesListado(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoRegiones: RegionesEntity[] = [];
        let resultadosDTO: RegionDTO[] = [];

        try {
            resultadoRegiones = await this.regionesRespository.find();

            if (Array.isArray(resultadoRegiones) && resultadoRegiones.length) {

                resultadoRegiones.forEach(item => {
                    let nuevoElemento: RegionDTO = {
                        idRegion: item.idRegion,
                        nombre: item.Nombre,
                        descripcion: item.Descripcion
                    }

                    resultadosDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Regiones obtenidas correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Regiones';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Regiones';
        }

        return resultado;
    }

    async getRegionDTO(id: number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoRegion: RegionesEntity = null;
        let resultadoDTO: RegionDTO = null;

        try {
            resultadoRegion = await this.regionesRespository.findOne(id);

            if (resultadoRegion != null && resultadoRegion != undefined) {
                resultadoDTO = {
                    idRegion: resultadoRegion.idRegion,
                    nombre: resultadoRegion.Nombre,
                    descripcion: resultadoRegion.Descripcion
                }
            }

            if (resultadoDTO != null && resultadoDTO != undefined) {
                resultado.Respuesta = resultadoDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Región obtenida correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontró la Región';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Región';
        }

        return resultado;
    }

    public async getRegionByComuna(idComuna: number): Promise<Resultado> {
        const resultado = new Resultado();
        
        try {
            await this.regionesRespository.createQueryBuilder('r')
                .leftJoinAndSelect('r.comunas', 'c')
                .where('c.idComuna = :idComuna', {idComuna})
                .getOneOrFail()
                .then((res: RegionesEntity) => {
                    // * Modificamos resultado para respuesta
                    this.setResultadoService.setResponse<RegionesEntity>(resultado, res)
                })
                .catch((err: Error) => {
                    throw new Error('Error al buscar region by comuna');
                })
        } catch (err) {
            // * Modificamos resultado para error
            this.setResultadoService.setError(resultado, 'No se ha podido obtener la región')
        }finally{
            return resultado;
        }
        
    }

    async update(idRegion: number, data: Partial<CreateRegionDto>) {
        await this.regionesRespository.update({ idRegion }, data);
        return await this.regionesRespository.findOne({ idRegion });
      }
      
    async create(data: CreateRegionDto): Promise<RegionesEntity> {
        return await this.regionesRespository.save(data);
    }
}
