import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateRegistroDto {

    @ApiPropertyOptional()
    idRegistroPago: number;
    @ApiPropertyOptional()
    idSolicitud: number;
    @ApiPropertyOptional()
    idTramite: number;
    @ApiPropertyOptional()
    fechaCobro: Date;
    @ApiPropertyOptional()
    fechaExpiracion: Date;
    @ApiPropertyOptional()
    pagado: boolean;
    // @ApiPropertyOptional()
    // idOficina:number;
    // @ApiPropertyOptional()
    // idUsuario:number;
    // @ApiPropertyOptional()
    // created_at: Date;
    // @ApiPropertyOptional()
    // updated_at: Date;
}