export class DocumentosLicenciaDto {

    idDocumentoLicencia:number;
    created_at:Date;
    update_at:Date;
    idClaseLicencia:number;
    idPapelSeguridad:number;
    Folio:string;
    idUsuario:number;
    idConductor:number;
    idPostulante:number;
    IdEstadoEDF:number;
    IdEstadoEDD:number;
    idEstadoPCL:number;
    idImagenLicencia:number;
    idInstitucion: number;
    fechaVencimiento: Date;
    fechaPrimerOtorgamiento: Date;
    fechaOtorgamientoActual: Date;
}
