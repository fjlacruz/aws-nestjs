export class PostulanteDto {

    idPostulante:number;
    RUN:number;
    DV:string
    Nombres:string;
    ApellidoPaterno:string;
    ApellidoMaterno:string;
    Calle:string;
    CalleNro:number;
    Letra:string;
    RestoDireccion:string;
    Nacionalidad:string;
    Email:string;
    Profesion:string;
    idUsuario:string;
    Telefono:string;
    FechaNacimiento:Date;
    FechaDefuncion:Date;
    idRegion:number;
    idComuna:number;
    idOpcionSexo: number;
    idOpcionEstadosCivil: number;
    idOpNivelEducacional: number;
}
