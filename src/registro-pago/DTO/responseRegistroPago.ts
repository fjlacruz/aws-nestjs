import { DocumentosLicenciaDto } from "./documentos-licencia.dto";
import { PostulanteDto } from "./postulante.dto";

export class ResponseRegistroPago {

    idRegistroPago: number;
    idSolicitud: number;
    idTramite: number;
    idUsuario:number;
    idOficina:number;
    fechaCobro: Date;
    fechaExpiracion: Date;
    pagado: boolean;
    postulante: PostulanteDto;
    documentosLicenciaDto:DocumentosLicenciaDto;
}
