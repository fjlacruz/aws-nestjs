import { ApiPropertyOptional } from "@nestjs/swagger";

export class UpdateRegistroPagoDto {

    @ApiPropertyOptional()
    idRegistroPago: number;
    @ApiPropertyOptional()
    RUN:number;
    @ApiPropertyOptional()
    pagado: boolean;
}
