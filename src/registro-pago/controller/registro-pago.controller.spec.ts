import { Test, TestingModule } from '@nestjs/testing';
import { RegistroPagoController } from './registro-pago.controller';

describe('RegistroPagoController', () => {
  let controller: RegistroPagoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RegistroPagoController],
    }).compile();

    controller = module.get<RegistroPagoController>(RegistroPagoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
