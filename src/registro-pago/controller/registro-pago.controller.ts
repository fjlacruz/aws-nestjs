import { Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateRegistroDto } from '../DTO/createRegistro.dto';
import { RegistroPagoService } from '../services/registro-pago.service';
import { UsersService } from 'src/users/services/users.service';
import { UpdateRegistroPagoDto } from '../DTO/updateRegistroPago.dto';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Resultado } from 'src/utils/resultado';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';

@ApiTags('Registro-Pago')
@Controller('registro-pago')
export class RegistroPagoController {
  constructor(
    private readonly registroService: RegistroPagoService,
    private readonly usersService: UsersService,
    private readonly authService: AuthService
  ) {}

  @Get('allRegistro')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los regitros de pagos' })
  async getRegistrosPagos() {
    const data = await this.registroService.getRegistrosPagos();
    return { data };
  }

  @Get('allRegistroPagadoTrue')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los regitros de pagos con pagado igual a true' })
  async getRegistrosPagosTrue() {
    const data = await this.registroService.getRegistrosPagosTrue();
    return { data };
  }

  @Get('allRegistroPagadoFalse')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los regitros de pagos con pagado igual a false' })
  async getRegistrosPagosFalse() {
    const data = await this.registroService.getRegistrosPagosFalse();
    return { data };
  }

  @Get('exmanenById/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Registro por Id' })
  async getRegistroPago(@Param('id') id: number) {
    const data = await this.registroService.getRegistroPago(id);
    return { data };
  }

  @Post('creaRegistroPago')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Registro Pago' })
  async addRegistroPago(@Body() dto: CreateRegistroDto) {
    const data = await this.registroService.create(dto);
    return { data };
  }

  @Post('cambiarEstadoRegistroPago')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que permite cambiar el estado de un Registro Pago' })
  async cambiarEstadoRegistroPago(@Body() dto: CreateRegistroDto) {
    const data = await this.registroService.create(dto);
    return { data };
  }

  @Post('comprobarPago')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que permite cambiar el estado de un Registro Pago' })
  async comprobarPago(@Body() idTramitesOtorgamiento: number[]) {
    const data = await this.registroService.comprobarPago(idTramitesOtorgamiento);
    return { data };
  }

  @Post('updateEstadoRegistroPago')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que permite cambiar el estado de un Registro Pago' })
  async updateEstadoRegistroPago(@Body() dto: UpdateRegistroPagoDto, @Req() req: any) {
    const data = await this.registroService.updatePago(req, dto);
    return { data };
  }

  @Post('updateEstadoRegistroPagoHis')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que permite cambiar el estado de un Registro Pago' })
  async updateEstadoRegistroPagoHis(@Body() dto: UpdateRegistroPagoDto) {
    const data = await this.registroService.updateEstadoRegistroPagoHis(dto);
    return { data };
  }

  @Get('allEstadoRegistroPago')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los estados de Registro de Pago segun Solicitud y Postulante' })
  async getRolesByidUsuario(
    @Query('run') run: string,
    @Query('nombre') nombre: string,
    @Query('numeroSolicitud') numeroSolicitud: string,
    @Query('fechacreacion') fechacreacion: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {

    const data = await this.registroService.fetch(
      req,
      run,
      nombre,
      numeroSolicitud,
      fechacreacion,
      {
        page,
        limit,
        route: '/registro-pago/allEstadoRegistroPago',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    return { data };
  }

  @Get('RegistroPagoCancelados')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Registros de Pago cancelados segun Solicitud y Postulante' })
  async getRolesByidUsuarioC(
    @Query('run') run: string,
    @Query('nombre') nombre: string,
    @Query('numeroSolicitud') numeroSolicitud: string,
    @Query('fechacreacion') fechacreacion: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {
    const data = await this.registroService.fetchCancelados(
      req,
      run,
      nombre,
      numeroSolicitud,
      fechacreacion,
      {
        page,
        limit,
        route: '/registro-pago/RegistroPagoCancelados',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    return { data };
  }

  @Get('RegistroPagoPendientes')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Registros de Pago cancelados segun Solicitud y Postulante' })
  async getRolesByidUsuarioP(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.registroService.fetchPendientes(
      {
        page,
        limit,
        route: '/registro-pago/RegistroPagoPendientes',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    return { data };
  }
}
