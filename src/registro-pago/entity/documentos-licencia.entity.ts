import { ApiProperty } from "@nestjs/swagger";
import { DocumentosLicenciaRestriccionEntity } from "src/clases-licencia/entity/documentos-licencia-restriccion.entity";
import { ConductoresEntity } from "src/conductor/entity/conductor.entity";
import { DocumentoLicenciaTramiteEntity } from "src/documento-licencia-tramite/entity/documento-licencia-tramite.entity";
import { DocumentosLicenciaClaseLicenciaEntity } from "src/documento-licencia/entity/documento-licencia.entity";
import { EstadosEDDEntity } from "src/estados-EDD/entity/estados-EDD.entity";
import { EstadosEDFEntity } from "src/estados-EDF/entity/estados-EDF.entity";
import { EstadosPCLEntity } from "src/estados-PCL/entity/estados-PCL.entity";
import { ImagenLicenciaEntity } from "src/imagen-licencia/entity/imagen-licencia.entity";
import { PapelSeguridadEntity } from "src/papel-seguridad/entity/papel-seguridad.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany} from "typeorm";

@Entity('DocumentosLicencia')
export class DocumentosLicencia {

    @PrimaryGeneratedColumn()
    idDocumentoLicencia:number;

    @ApiProperty()
    @OneToMany(() => DocumentosLicenciaClaseLicenciaEntity, documentoLicenciaClaseLicencia => documentoLicenciaClaseLicencia.documentosLicencia)
    readonly documentoLicenciaClaseLicencia: DocumentosLicenciaClaseLicenciaEntity[];

    @Column()
    created_at:Date;

    @Column()
    update_at:Date;

    @Column()
    idClaseLicencia:number;

    @Column()
    idPapelSeguridad:number;
    @OneToOne(() => PapelSeguridadEntity, papelSeguridad => papelSeguridad.documentosLicencia)
    @JoinColumn({ name: 'idPapelSeguridad' })
    readonly papelSeguridad: PapelSeguridadEntity;

    @Column()
    Folio:string;

    @Column({
        nullable: true
    })
    Observaciones:string;

    @Column()
    idUsuario:number;
    @OneToOne(() => User, usuario => usuario.documentosLicencia)
    @JoinColumn({ name: 'idUsuario' })
    readonly usuario: User;

    @Column()
    idConductor:number;
    @OneToOne(() => ConductoresEntity, conductor => conductor.documentosLicencia)
    @JoinColumn({ name: 'idConductor' })
    readonly conductor: ConductoresEntity;

    @Column()
    idPostulante:number;

    @Column()
    IdEstadoEDF:number;
    @OneToOne(() => EstadosEDFEntity, estadosDF => estadosDF.documentosLicencia)
    @JoinColumn({ name: 'IdEstadoEDF' })
    readonly estadosDF: EstadosEDFEntity;

    @Column()
    IdEstadoEDD:number;
    @OneToOne(() => EstadosEDDEntity, estadosDD => estadosDD.documentosLicencia)
    @JoinColumn({ name: 'IdEstadoEDD' })
    readonly estadosDD: EstadosEDDEntity;

    @Column()
    idEstadoPCL:number;
    @OneToOne(() => EstadosPCLEntity, estadoPCL => estadoPCL.documentosLicencia)
    @JoinColumn({ name: 'idEstadoPCL' })
    readonly estadosPCL: EstadosPCLEntity;

    @Column()
    idImagenLicencia:number;

    @ApiProperty()
    @OneToMany(() => ImagenLicenciaEntity, imagenLicencia => imagenLicencia.documentosLicencia)
    readonly imagenLicencia: ImagenLicenciaEntity[];

    @Column()
    idInstitucion: number;
    
    @Column()
    fechacaducidad: Date;

    @ApiProperty()
    @OneToMany(() => DocumentoLicenciaTramiteEntity, documentoLicenciaTramite => documentoLicenciaTramite.documentosLicencia)
    readonly documentoLicenciaTramite: DocumentoLicenciaTramiteEntity[];

    @ApiProperty()
    @OneToMany(() => DocumentosLicenciaRestriccionEntity, documentoLicenciaRestriccion => documentoLicenciaRestriccion.documentoLicencia)
    readonly documentoLicenciaRestricciones: DocumentosLicenciaRestriccionEntity[];
}
