import { ApiProperty } from '@nestjs/swagger';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { AudienciaNotificacionesConductores } from 'src/notificaciones/entity/audienciaConductoresNotificaciones.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { OpcionSexoEntity } from '../../opciones-sexo/entity/sexo.entity';
import { OpcionEstadosCivilEntity } from '../../opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { OpcionesNivelEducacional } from '../../opciones-nivel-educacional/entity/nivel-educacional.entity';

@Entity('Postulantes')
export class Postulante {
  @PrimaryGeneratedColumn()
  idPostulante: number;

  @Column()
  RUN: number;

  @Column()
  DV: string;

  @Column()
  Nombres: string;

  @Column()
  ApellidoPaterno: string;

  @Column()
  ApellidoMaterno: string;

  @Column()
  Calle: string;

  @Column()
  CalleNro: number;

  @Column()
  Letra: string;

  @Column()
  RestoDireccion: string;

  @Column()
  Nacionalidad: string;

  @Column()
  Email: string;

  @Column()
  Profesion: string;

  @Column()
  Diplomatico: boolean;

  @Column()
  discapacidad: boolean;

  @Column()
  DetalleDiscapacidad: string;

  @Column()
  updated_at: Date;

  @Column()
  created_at: Date;

  @Column()
  idUsuario: number;

  @Column()
  Telefono: string;

  @Column()
  FechaNacimiento: Date;

  @Column()
  FechaDefuncion: Date;

  @Column()
  idRegion: number;

  @Column()
  idComuna: number;

  @OneToOne(() => ComunasEntity, comunas => comunas.postulante)
  @JoinColumn({ name: 'idComuna' })
  readonly comunas: ComunasEntity;

  @Column()
  idOpcionSexo: number;

  @Column()
  idOpcionEstadosCivil: number;

  @Column()
  idOpNivelEducacional: number;

  @Column()
  idSituacionLaboral: number;

  @ApiProperty()
  @OneToMany(() => Solicitudes, solicitudes => solicitudes.postulante)
  readonly solicitudes: Solicitudes[];

  @OneToMany(() => AudienciaNotificacionesConductores, audienciaConductoresNotificaciones => audienciaConductoresNotificaciones.conductor)
  readonly audicenciaConductoresNotificaciones: AudienciaNotificacionesConductores[];

  @ManyToOne(() => OpcionSexoEntity, opcionSexo => opcionSexo.idOpcionSexo)
  @JoinColumn({ name: 'idOpcionSexo' })
  readonly opcionSexo: OpcionSexoEntity;

  @ManyToOne(() => OpcionEstadosCivilEntity, opcionEstadosCivil => opcionEstadosCivil.idOpcionEstadosCivil)
  @JoinColumn({ name: 'idOpcionEstadosCivil' })
  readonly opcionEstadosCivil: OpcionEstadosCivilEntity;

  @ManyToOne(() => OpcionesNivelEducacional, opcionEstadosCivil => opcionEstadosCivil.idOpNivelEducacional)
  @JoinColumn({ name: 'idOpNivelEducacional' })
  readonly opcionesNivelEducacional: OpcionesNivelEducacional;
}
