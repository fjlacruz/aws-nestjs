import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('RegistroPago')
export class RegistroPago {
  @PrimaryGeneratedColumn()
  idRegistroPago: number;

  @Column()
  idSolicitud: number;

  @Column()
  idTramite: number;

  @Column()
  idUsuario: number;

  @Column()
  idOficina: number;

  @Column()
  fechaCobro: Date;

  @Column()
  fechaExpiracion: Date;

  @Column()
  pagado: boolean;

  @Column()
  created_at: Date;

  @Column()
  updated_at: Date;
}
