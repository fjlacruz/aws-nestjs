import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { UsersModule } from 'src/users/users.module';
import { RegistroPagoController } from './controller/registro-pago.controller';
import { RegistroPago } from './entity/registro.entity';
import { RegistroPagoService } from './services/registro-pago.service';


@Module({
  imports: [TypeOrmModule.forFeature([RegistroPago, User, RolesUsuarios]), UsersModule],
  controllers: [RegistroPagoController],
  exports: [RegistroPagoService],
  providers: [RegistroPagoService, AuthService]
})
export class RegistroPagoModule {}
