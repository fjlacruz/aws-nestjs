import { Test, TestingModule } from '@nestjs/testing';
import { RegistroPagoService } from './registro-pago.service';

describe('RegistroPagoService', () => {
  let service: RegistroPagoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RegistroPagoService],
    }).compile();

    service = module.get<RegistroPagoService>(RegistroPagoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
