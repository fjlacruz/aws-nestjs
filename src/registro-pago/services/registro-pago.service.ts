import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRegistroDto } from '../DTO/createRegistro.dto';
import { ResponseRegistroPago } from '../DTO/responseRegistroPago';
import { PostulanteDto } from '../DTO/postulante.dto';
import { RegistroPago } from '../entity/registro.entity';
import { DocumentosLicencia } from '../entity/documentos-licencia.entity';
import { getConnection } from 'typeorm';
import { UsersService } from 'src/users/services/users.service';
import { DocumentosLicenciaDto } from '../DTO/documentos-licencia.dto';
import { Postulante } from '../entity/postulante.entity';
import { UpdateRegistroPagoDto } from '../DTO/updateRegistroPago.dto';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { AuthService } from 'src/auth/auth.service';
import { IPaginationOptions, paginate, paginateRaw } from 'nestjs-typeorm-paginate';
import { Resultado } from 'src/utils/resultado';
import { ColumnasSolicitudesProrroga, PermisosNombres } from 'src/constantes';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';

@Injectable()
export class RegistroPagoService {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    @InjectRepository(RegistroPago)
    private readonly registroRespository: Repository<RegistroPago>
  ) {}

  async getRegistrosPagos() {
    const idOficina = this.authService.oficina().idOficina;
    const registros = await this.registroRespository.find({ idOficina });

    if (!registros) throw new NotFoundException('registro pago no existe');

    var aRegistros = [];
    for (var i in registros) {
      let registro = registros[i];

      const responseRegistroPago = new ResponseRegistroPago();
      responseRegistroPago.idRegistroPago = registro.idRegistroPago;
      responseRegistroPago.idSolicitud = registro.idSolicitud;
      responseRegistroPago.idTramite = registro.idTramite;
      responseRegistroPago.idUsuario = registro.idUsuario;

      responseRegistroPago.fechaCobro = registro.fechaCobro;
      responseRegistroPago.fechaExpiracion = registro.fechaExpiracion;
      responseRegistroPago.pagado = registro.pagado;

      const documentosLicenciaDto = new DocumentosLicenciaDto();
      const licencia = await this.getLicencia(registro.idUsuario);
      if (licencia != null && licencia != undefined) {
        documentosLicenciaDto.idDocumentoLicencia = licencia.idDocumentoLicencia;
        documentosLicenciaDto.created_at = licencia.created_at;
        documentosLicenciaDto.update_at = licencia.update_at;
        documentosLicenciaDto.idClaseLicencia = licencia.idClaseLicencia;
        documentosLicenciaDto.idPapelSeguridad = licencia.idPapelSeguridad;
        documentosLicenciaDto.Folio = licencia.Folio;
        documentosLicenciaDto.idUsuario = licencia.idUsuario;
        documentosLicenciaDto.idConductor = licencia.idConductor;
        documentosLicenciaDto.idPostulante = licencia.idPostulante;
        documentosLicenciaDto.IdEstadoEDF = licencia.IdEstadoEDF;
        documentosLicenciaDto.IdEstadoEDD = licencia.IdEstadoEDD;
        documentosLicenciaDto.idEstadoPCL = licencia.idEstadoPCL;
        documentosLicenciaDto.idImagenLicencia = licencia.idImagenLicencia;

        const postulanteDto = new PostulanteDto();
        const postulante = await getConnection()
          .createQueryBuilder()
          .select('Postulantes')
          .from(Postulante, 'Postulantes')
          .where('Postulantes.idPostulante = :id', { id: licencia.idPostulante })
          .getOne();

        if (postulante != null && postulante != undefined) {
          postulanteDto.RUN = postulante.RUN;
          postulanteDto.DV = postulante.DV;
          postulanteDto.Nombres = postulante.Nombres;
          postulanteDto.ApellidoPaterno = postulante.ApellidoPaterno;
          postulanteDto.ApellidoMaterno = postulante.ApellidoMaterno;
          postulanteDto.idOpcionSexo = postulante.idOpcionSexo;
        }
        responseRegistroPago.postulante = postulanteDto;
      }

      responseRegistroPago.documentosLicenciaDto = documentosLicenciaDto;
      aRegistros.push(responseRegistroPago);
    }

    return aRegistros;
  }

  async getRegistrosPagosTrue() {
    const idOficina = this.authService.oficina().idOficina;
    const registros = await this.registroRespository.find({ idOficina });

    if (!registros) throw new NotFoundException('registro pago no existe');

    var aRegistros = [];
    for (var i in registros) {
      if (registros[i].pagado == true) {
        let registro = registros[i];

        const responseRegistroPago = new ResponseRegistroPago();
        responseRegistroPago.idRegistroPago = registro.idRegistroPago;
        responseRegistroPago.idSolicitud = registro.idSolicitud;
        responseRegistroPago.idTramite = registro.idTramite;
        responseRegistroPago.idUsuario = registro.idUsuario;

        responseRegistroPago.fechaCobro = registro.fechaCobro;
        responseRegistroPago.fechaExpiracion = registro.fechaExpiracion;
        responseRegistroPago.pagado = registro.pagado;

        const documentosLicenciaDto = new DocumentosLicenciaDto();
        const licencia = await this.getLicencia(registro.idUsuario);
        if (licencia != null && licencia != undefined) {
          documentosLicenciaDto.idDocumentoLicencia = licencia.idDocumentoLicencia;
          documentosLicenciaDto.created_at = licencia.created_at;
          documentosLicenciaDto.update_at = licencia.update_at;
          documentosLicenciaDto.idClaseLicencia = licencia.idClaseLicencia;
          documentosLicenciaDto.idPapelSeguridad = licencia.idPapelSeguridad;
          documentosLicenciaDto.Folio = licencia.Folio;
          documentosLicenciaDto.idUsuario = licencia.idUsuario;
          documentosLicenciaDto.idConductor = licencia.idConductor;
          documentosLicenciaDto.idPostulante = licencia.idPostulante;
          documentosLicenciaDto.IdEstadoEDF = licencia.IdEstadoEDF;
          documentosLicenciaDto.IdEstadoEDD = licencia.IdEstadoEDD;
          documentosLicenciaDto.idEstadoPCL = licencia.idEstadoPCL;
          documentosLicenciaDto.idImagenLicencia = licencia.idImagenLicencia;

          const postulanteDto = new PostulanteDto();
          const postulante = await getConnection()
            .createQueryBuilder()
            .select('Postulantes')
            .from(Postulante, 'Postulantes')
            .where('Postulantes.idPostulante = :id', { id: licencia.idPostulante })
            .getOne();

          if (postulante != null && postulante != undefined) {
            postulanteDto.RUN = postulante.RUN;
            postulanteDto.DV = postulante.DV;
            postulanteDto.Nombres = postulante.Nombres;
            postulanteDto.ApellidoPaterno = postulante.ApellidoPaterno;
            postulanteDto.ApellidoMaterno = postulante.ApellidoMaterno;
            postulanteDto.idOpcionSexo = postulante.idOpcionSexo;
          }
          responseRegistroPago.postulante = postulanteDto;
        }

        responseRegistroPago.documentosLicenciaDto = documentosLicenciaDto;
        aRegistros.push(responseRegistroPago);
      }
    }

    return aRegistros;
  }

  async getRegistrosPagosFalse() {
    const idOficina = this.authService.oficina().idOficina;
    const registros = await this.registroRespository.find({ idOficina });

    if (!registros) throw new NotFoundException('registro pago no existe');

    var aRegistros = [];
    for (var i in registros) {
      if (registros[i].pagado == false) {
        let registro = registros[i];

        const responseRegistroPago = new ResponseRegistroPago();
        responseRegistroPago.idRegistroPago = registro.idRegistroPago;
        responseRegistroPago.idSolicitud = registro.idSolicitud;
        responseRegistroPago.idTramite = registro.idTramite;
        responseRegistroPago.idUsuario = registro.idUsuario;

        responseRegistroPago.fechaCobro = registro.fechaCobro;
        responseRegistroPago.fechaExpiracion = registro.fechaExpiracion;
        responseRegistroPago.pagado = registro.pagado;

        const documentosLicenciaDto = new DocumentosLicenciaDto();
        const licencia = await this.getLicencia(registro.idUsuario);
        if (licencia != null && licencia != undefined) {
          documentosLicenciaDto.idDocumentoLicencia = licencia.idDocumentoLicencia;
          documentosLicenciaDto.created_at = licencia.created_at;
          documentosLicenciaDto.update_at = licencia.update_at;
          documentosLicenciaDto.idClaseLicencia = licencia.idClaseLicencia;
          documentosLicenciaDto.idPapelSeguridad = licencia.idPapelSeguridad;
          documentosLicenciaDto.Folio = licencia.Folio;
          documentosLicenciaDto.idUsuario = licencia.idUsuario;
          documentosLicenciaDto.idConductor = licencia.idConductor;
          documentosLicenciaDto.idPostulante = licencia.idPostulante;
          documentosLicenciaDto.IdEstadoEDF = licencia.IdEstadoEDF;
          documentosLicenciaDto.IdEstadoEDD = licencia.IdEstadoEDD;
          documentosLicenciaDto.idEstadoPCL = licencia.idEstadoPCL;
          documentosLicenciaDto.idImagenLicencia = licencia.idImagenLicencia;

          const postulanteDto = new PostulanteDto();
          const postulante = await getConnection()
            .createQueryBuilder()
            .select('Postulantes')
            .from(Postulante, 'Postulantes')
            .where('Postulantes.idPostulante = :id', { id: licencia.idPostulante })
            .getOne();

          if (postulante != null && postulante != undefined) {
            postulanteDto.RUN = postulante.RUN;
            postulanteDto.DV = postulante.DV;
            postulanteDto.Nombres = postulante.Nombres;
            postulanteDto.ApellidoPaterno = postulante.ApellidoPaterno;
            postulanteDto.ApellidoMaterno = postulante.ApellidoMaterno;
            postulanteDto.idOpcionSexo = postulante.idOpcionSexo;
          }
          responseRegistroPago.postulante = postulanteDto;
        }

        responseRegistroPago.documentosLicenciaDto = documentosLicenciaDto;
        aRegistros.push(responseRegistroPago);
      }
    }

    return aRegistros;
  }

  async getLicencia(id: number) {
    const licencia = await getConnection()
      .createQueryBuilder()
      .select('documentosLicencia')
      .from(DocumentosLicencia, 'documentosLicencia')
      .where('documentosLicencia.idUsuario = :id', { id: id })
      .getOne();
    return licencia;
  }

  async getRegistroPago(id: number) {
    const registro = await this.registroRespository.findOne(id);
    if (!registro) throw new NotFoundException('registro pago dont exist');

    const responseRegistroPago = new ResponseRegistroPago();
    responseRegistroPago.idRegistroPago = registro.idRegistroPago;
    responseRegistroPago.idSolicitud = registro.idSolicitud;
    responseRegistroPago.idTramite = registro.idTramite;
    responseRegistroPago.idUsuario = registro.idUsuario;

    responseRegistroPago.fechaCobro = registro.fechaCobro;
    responseRegistroPago.fechaExpiracion = registro.fechaExpiracion;
    responseRegistroPago.pagado = registro.pagado;

    const documentosLicenciaDto = new DocumentosLicenciaDto();
    const licencia = await this.getLicencia(registro.idUsuario);
    if (licencia != null && licencia != undefined) {
      documentosLicenciaDto.idDocumentoLicencia = licencia.idDocumentoLicencia;
      documentosLicenciaDto.created_at = licencia.created_at;
      documentosLicenciaDto.update_at = licencia.update_at;
      documentosLicenciaDto.idClaseLicencia = licencia.idClaseLicencia;
      documentosLicenciaDto.idPapelSeguridad = licencia.idPapelSeguridad;
      documentosLicenciaDto.Folio = licencia.Folio;
      documentosLicenciaDto.idUsuario = licencia.idUsuario;
      documentosLicenciaDto.idConductor = licencia.idConductor;
      documentosLicenciaDto.idPostulante = licencia.idPostulante;
      documentosLicenciaDto.IdEstadoEDF = licencia.IdEstadoEDF;
      documentosLicenciaDto.IdEstadoEDD = licencia.IdEstadoEDD;
      documentosLicenciaDto.idEstadoPCL = licencia.idEstadoPCL;
      documentosLicenciaDto.idImagenLicencia = licencia.idImagenLicencia;

      const postulanteDto = new PostulanteDto();
      const postulante = await getConnection()
        .createQueryBuilder()
        .select('Postulantes')
        .from(Postulante, 'Postulantes')
        .where('Postulantes.idPostulante = :id', { id: licencia.idPostulante })
        .getOne();

      if (postulante != null && postulante != undefined) {
        postulanteDto.RUN = postulante.RUN;
        postulanteDto.DV = postulante.DV;
        postulanteDto.Nombres = postulante.Nombres;
        postulanteDto.ApellidoPaterno = postulante.ApellidoPaterno;
        postulanteDto.ApellidoMaterno = postulante.ApellidoMaterno;
        postulanteDto.idOpcionSexo = postulante.idOpcionSexo;
      }

      responseRegistroPago.postulante = postulanteDto;
    }

    responseRegistroPago.documentosLicenciaDto = documentosLicenciaDto;
    return responseRegistroPago;
  }

  async create(dto: CreateRegistroDto): Promise<RegistroPago> {
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const idOficina = this.authService.oficina().idOficina;
    const created_at = new Date();
    const updated_at = created_at;
    const data = { ...dto, created_at, updated_at, idUsuario, idOficina };

    return await this.registroRespository.save(data);
  }

  async updatePago(req:any, updateRegistroPagoDto: UpdateRegistroPagoDto) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.IngresoyEvaluacionModuloCajaModificarEstadodeSolicitudesPendientesdePago);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	     

    if (updateRegistroPagoDto.idRegistroPago != null && updateRegistroPagoDto.idRegistroPago != 0) {
      const registro = await this.registroRespository.findOne(updateRegistroPagoDto.idRegistroPago);
      if (!registro) throw new NotFoundException('registro pago dont exist');
      //registro.pagado = updateRegistroPagoDto.pagado;
      const id = registro.idRegistroPago;
      return await this.update(id, true);
    }

    //updateEstadoRegistroPagoHis
    if (updateRegistroPagoDto.RUN != null && updateRegistroPagoDto.RUN != 0) {
      const postulante = await getConnection()
        .createQueryBuilder()
        .select('Postulantes')
        .from(Postulante, 'Postulantes')
        .where('Postulantes.RUN = :run', { run: updateRegistroPagoDto.RUN })
        .getOne();

      if (postulante != null && postulante != undefined) {
        const pago = await getConnection()
          .createQueryBuilder()
          .select('RegistroPago')
          .from(RegistroPago, 'RegistroPago')
          .where('RegistroPago.idUsuario = :idUsuario', { idUsuario: postulante.idUsuario })
          .getOne();

        const pagado = true;
        const id = pago.idRegistroPago;
        return await this.update(id, pagado);
      }
    }
    return null;
  }

  async updateEstadoRegistroPagoHis(updateRegistroPagoDto: UpdateRegistroPagoDto) {
    if (updateRegistroPagoDto.idRegistroPago != null && updateRegistroPagoDto.idRegistroPago != 0) {
      const registro = await this.registroRespository.findOne(updateRegistroPagoDto.idRegistroPago);
      if (!registro) throw new NotFoundException('registro pago dont exist');
      //registro.pagado = updateRegistroPagoDto.pagado;
      const id = registro.idRegistroPago;
      return await this.update(id, false);
    }

    if (updateRegistroPagoDto.RUN != null && updateRegistroPagoDto.RUN != 0) {
      const postulante = await getConnection()
        .createQueryBuilder()
        .select('Postulantes')
        .from(Postulante, 'Postulantes')
        .where('Postulantes.RUN = :run', { run: updateRegistroPagoDto.RUN })
        .getOne();

      if (postulante != null && postulante != undefined) {
        const pago = await getConnection()
          .createQueryBuilder()
          .select('RegistroPago')
          .from(RegistroPago, 'RegistroPago')
          .where('RegistroPago.idUsuario = :idUsuario', { idUsuario: postulante.idUsuario })
          .getOne();

        const pagado = true;
        const id = pago.idRegistroPago;
        return await this.update(id, pagado);
      }
    }
    return null;
  }

  async comprobarPago(idTramitesOtorgamiento) {
    let resultado = new Resultado();

    try {
      const consultaPago = this.registroRespository
        .createQueryBuilder('RegistroPago')
        .where('RegistroPago.idTramite in (:...idTramite)', { idTramite: idTramitesOtorgamiento })
        .andWhere('RegistroPago.pagado = true');

      const pagoCount = await consultaPago.getCount();

      pagoCount == idTramitesOtorgamiento.length ? (resultado.Respuesta = true) : (resultado.Respuesta = false);
      resultado.ResultadoOperacion = true;
    } catch (err) {
      resultado.Error = err;
      resultado.Mensaje = 'Ha ocurrido un error';
    }

    return resultado;
  }

  async update(id: number, pagado: boolean) {
    await getConnection()
      .createQueryBuilder()
      .update(RegistroPago)
      .set({
        pagado: pagado,
      })
      .where('idRegistroPago = :id', { id: id })
      .execute();
    return await this.registroRespository.findOne(id);
  }

  public async fetch(
    req: any,
    run: string,
    nombre: string,
    numeroSolicitud: string,
    fechacreacion: string,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ): Promise<any> {
    // const idOficina = this.authService.oficina().idOficina;
    const qb = this.registroRespository
      .createQueryBuilder('RegistroPago')
      .innerJoinAndMapOne('RegistroPago.idSolicitud', Solicitudes, 'sol', 'RegistroPago.idSolicitud = sol.idSolicitud')
      .innerJoinAndMapOne('sol.idPostulante', Postulante, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoinAndMapOne('sol.Tramite', TramitesEntity, 'tr', 'tr.idSolicitud = RegistroPago.idSolicitud');
    // .where('sol.idOficina = :idOficina', { idOficina });


    // Filtramos por las oficinas en caso de que el usuario no sea administrador
    // Comprobamos permisos
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaPostulantesModuloCaja);
  
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      qb.andWhere('FALSE');
    }

    // 3.b Añadimos condición para filtrar por oficina si se da el caso
    // Filtramos por las oficinas en caso de que el usuario no sea administrador
    //Comprobamos si es superUsuario por si es necesario filtrar
    if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
      // Recogemos las oficinas asociadas al usuario
      let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);
      
      if (idOficinasAsociadas == null) {
        qb.andWhere('FALSE');
      } else {
        qb.andWhere('sol.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
      }
    }

    // Recogemos las solicitudes que no estén en estado borrador ni en estado cerrada
    qb.andWhere('(sol.idEstado != 1)').andWhere('(sol.idEstado != 5)');

    if (nombre) qb.andWhere('LOWER(pos.Nombres) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(pos.ApellidoPaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(pos.ApellidoMaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (numeroSolicitud) qb.andWhere('sol.idSolicitud = :numeroSolicitud', { numeroSolicitud: numeroSolicitud });
    if (run) {
      const rut = run.split('-');
      let runsplited: string = rut[0];
      runsplited = runsplited.replace('.', '');
      runsplited = runsplited.replace('.', '');
      const div = rut[1];
      qb.andWhere('pos.RUN = :run', {
        run: runsplited,
      });

      qb.andWhere('pos.DV = :dv', {
        dv: div,
      });
    }
    if (fechacreacion) qb.andWhere('DATE(sol.FechaCreacion) = :created_at', { created_at: fechacreacion });

    if (orden === 'numeroSolicitud' && ordenarPor === 'ASC') qb.orderBy('sol.idSolicitud', 'ASC');
    if (orden === 'numeroSolicitud' && ordenarPor === 'DESC') qb.orderBy('sol.idSolicitud', 'DESC');
    if (orden === 'run' && ordenarPor === 'ASC') qb.orderBy('pos.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qb.orderBy('pos.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qb.orderBy('pos.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qb.orderBy('pos.Nombres', 'DESC');
    if (orden === 'fechaInicioTramite' && ordenarPor === 'ASC') qb.orderBy('sol.FechaCreacion', 'ASC');
    if (orden === 'fechaInicioTramite' && ordenarPor === 'DESC') qb.orderBy('sol.FechaCreacion', 'DESC');

    return paginate<any>(qb, options);
  }

  public async fetchCancelados(
    req: any,
    run: string,
    nombre: string,
    numeroSolicitud: string,
    fechacreacion: string,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: any
  ): Promise<any> {
    let orderBy = ColumnasSolicitudesProrroga[orden];

    //const idOficina = this.authService.oficina().idOficina;
    let pagado = true;
    const qb = this.registroRespository
      .createQueryBuilder('RegistroPago')
      .innerJoinAndMapOne('RegistroPago.idSolicitud', Solicitudes, 'Solicitudes', 'RegistroPago.idSolicitud = Solicitudes.idSolicitud')
      .innerJoinAndMapOne('Solicitudes.idPostulante', Postulante, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
      .innerJoinAndMapOne('Solicitudes.Tramite', TramitesEntity, 'tramites', 'tramites.idSolicitud = RegistroPago.idSolicitud')
      //.where('Solicitudes.idOficina = :idOficina', { idOficina })
      .andWhere('RegistroPago.pagado = :pagado', { pagado });

    // Se comprueban permisos
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req,  PermisosNombres.VisualizarListaPostulantesModuloCaja);

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      qb.andWhere('FALSE');
    }

    // 3.b Añadimos condición para filtrar por oficina si se da el caso
    // Filtramos por las oficinas en caso de que el usuario no sea administrador
    //Comprobamos si es superUsuario por si es necesario filtrar
    if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
      // Recogemos las oficinas asociadas al usuario
      let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);
      
      if (idOficinasAsociadas == null) {
        qb.andWhere('FALSE');
      } else {
        qb.andWhere('Solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
      }
    }
    
      

    if (nombre) qb.andWhere('LOWER(postulante.Nombres) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(postulante.ApellidoPaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(postulante.ApellidoMaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (numeroSolicitud) qb.andWhere('Solicitudes.idSolicitud = :numeroSolicitud', { numeroSolicitud: numeroSolicitud });
    if (run) {
      const rut = run.split('-');
      let runsplited: string = rut[0];
      runsplited = runsplited.replace('.', '');
      runsplited = runsplited.replace('.', '');
      const div = rut[1];
      qb.andWhere('postulante.RUN = :run', {
        run: runsplited,
      });

      qb.andWhere('postulante.DV = :dv', {
        dv: div,
      });
    }
    if (fechacreacion) qb.andWhere('DATE(Solicitudes.FechaCreacion) = :created_at', { created_at: fechacreacion });
    qb.orderBy(orderBy, ordenarPor);
    return paginate<any>(qb, options);
  }

  public fetchPendientes(options: IPaginationOptions, orden?: string, ordenarPor?: string): Promise<any> {
    const idOficina = this.authService.oficina().idOficina;
    let pagado = false || null;
    const qb = this.registroRespository
      .createQueryBuilder('RegistroPago')
      .innerJoinAndMapOne('RegistroPago.idSolicitud', Solicitudes, 'sol', 'RegistroPago.idSolicitud = sol.idSolicitud')
      .innerJoinAndMapOne('sol.idPostulante', Postulante, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoinAndMapOne('sol.Tramite', TramitesEntity, 'tr', 'tr.idSolicitud = RegistroPago.idSolicitud')
      .where('sol.idOficina = :idOficina', { idOficina })
      //.andWhere('RegistroPago.pagado = :pagado', { pagado });
      .andWhere('RegistroPago.pagado IS null');

    return paginate<any>(qb, options);
  }
}
