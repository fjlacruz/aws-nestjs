import { Roles } from "src/roles/entity/roles.entity";
import { TipoResgistroUsuario } from "src/tipo-resgistro-usuario/entity/tipo-resgistro-usuario.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne} from "typeorm";

@Entity('RegistroUsuarios')
export class RegistroUsuarios {

    @PrimaryGeneratedColumn()
    idRegistroUsuarios: number;
    
    @Column()
    idUsuario: number;
    @ManyToOne(() => User, usuario => usuario.registroUsuarios)
    @JoinColumn({name: 'idUsuario'})
    readonly usuario: User;

    @Column()
    IdIngresadoPor: number;
    @ManyToOne(() => User, usuario => usuario.ingresadoPor)
    @JoinColumn({name: 'idUsuario'})
    readonly ingresadoPor: User;

    
    @Column()
    IdAprobadoPor: number;
    @ManyToOne(() => User, usuario => usuario.aprobadoPor)
    @JoinColumn({name: 'idUsuario'})
    readonly aprobadoPor: User;

    @Column()
    IdRol: number;
    @ManyToOne(() => Roles, roles => roles.registroUsuarios)
    @JoinColumn({name: 'IdRol'})
    readonly roles: Roles;

    @Column()
    Aprobado?: boolean;

    @Column()
    Observacion: string;

    @Column()
    Fecha: Date;

    @Column()
    IdTipoRegistroUsuario: number;
    @ManyToOne(() => TipoResgistroUsuario, tipoResgistroUsuario => tipoResgistroUsuario.registroUsuarios)
    @JoinColumn({name: 'IdTipoRegistroUsuario'})
    readonly tipoRegistroUsuario: TipoResgistroUsuario;
}