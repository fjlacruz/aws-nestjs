import { Test, TestingModule } from '@nestjs/testing';
import { ReservaHoraController } from './reserva-hora.controller';

describe('ReservaHoraController', () => {
  let controller: ReservaHoraController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReservaHoraController],
    }).compile();

    controller = module.get<ReservaHoraController>(ReservaHoraController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
