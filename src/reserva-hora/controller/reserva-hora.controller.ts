/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import {
  PostReservaDTO,
  PostBloqueReservaDTO,
  GetBloquesHorarioDTO,
  GetReservasTomadasDTO,
  PatchBloqueReservaDTO,
  GetHorasDisponibleDTO,
  PatchReservaAnularDTO,
  PatchReservaReprogramarDTO,
} from '../dto/reserva-hora.dto';
import { ReservaHoraService } from '../services/reserva-hora.service';
import { TramitesReservaService } from '../services/tramites-reserva.service';

@ApiTags('Reserva-hora')
@Controller('reserva-hora')
export class ReservaHoraController {
  constructor(private readonly reservaHoraService: ReservaHoraService, private readonly tramitesReservaService: TramitesReservaService) {}

  @Get('reserva/:fecha/:idMunicipio')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos las reservas para una fecha / municipalidad' })
  async getReservasTomadas(@Param() dto: GetReservasTomadasDTO, @Req() req: any) {
    return this.reservaHoraService.getReservasTomadas(req, dto).then(data => {
      return { code: 200, data };
    });
  }

  @Get('horario/:fecha/:idMunicipio/:idTipoTramite/:idClaseLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los horarios disponibles para una fecha / municipalidad / tramite / clase licencia' })
  async getHorasDisponibles(@Param() dto: GetHorasDisponibleDTO) {
    return this.reservaHoraService.getHorasDisponibles(dto).then(data => {
      return { code: 200, data };
    });
  }

  @Get('tramite/:idReserva')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites de una reserva' })
  async getTramites(@Param('idReserva') idReserva: number) {
    return this.tramitesReservaService.getTramites(Number(idReserva)).then(data => {
      return { code: 200, data };
    });
  }

  @Get('bloque/:fechaDesde/:fechaHasta/:idMunicipio/:idTipoTramite/:idClaseLicencia/:idEstadoBloqueReserva')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que trae todos los bloques de horario para una rango de fechas / municipio / tipo tramite / clase licencia / estado',
  })
  async getBloquesHorario(@Param() dto: GetBloquesHorarioDTO, @Req() req: any) {
    return this.reservaHoraService.getBloquesHorario(req, dto).then(data => {
      return { code: 200, data };
    });
  }

  @Post('reserva')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que agrega una reserva de un postulante' })
  async postReservaHora(@Body() dto: PostReservaDTO) {
    return this.reservaHoraService.postReserva(dto).then(data => {
      return { code: 200, data };
    });
  }

  @Patch('reserva/:id/:idEstadoReserva')
  @ApiBearerAuth('reserva')
  @ApiOperation({ summary: 'Servicio que modifica el estado de la reserva de un postulante' })
  async patchReservaHora(@Param('id') id: number, @Param('idEstadoReserva') idEstadoReserva: number) {
    return this.reservaHoraService.patchReserva(Number(id), Number(idEstadoReserva)).then(data => {
      return { code: 200 };
    });
  }

  @Patch('reserva-anular/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que anula la reserva de un postulante' })
  async patchReservaAnular(@Param('id') id: number, @Body() dto: PatchReservaAnularDTO) {
    return this.reservaHoraService.patchReservaAnular(Number(id), dto).then(data => {
      return { code: 200 };
    });
  }

  @Patch('reserva-reprogramar/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que reprograma la reserva de un postulante' })
  async patchReservaReprogramar(@Param('id') id: number, @Body() dto: PatchReservaReprogramarDTO) {
    console.log(id, dto);

    return this.reservaHoraService.patchReservaReprogramar(Number(id), dto).then(data => {
      return { code: 200 };
    });
  }

  @Delete('reserva/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina una reserva de un postulante' })
  async deleteReservaHora(@Param('id') id: number) {
    return this.reservaHoraService.deleteReserva(Number(id)).then(data => {
      return { code: 200 };
    });
  }

  @Post('bloque')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que agrega un bloque de horario para una municipalidad' })
  async postBloqueHorario(@Body() dto: PostBloqueReservaDTO, @Req() req) {
    if (dto.idClaseLicencia == 'null') dto.idClaseLicencia = null;

    return this.reservaHoraService.postBloqueHorario(req, dto).then(data => {
      return { code: 200, data };
    });
  }

  @Patch('bloque/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que permite actualizar un bloque de horario' })
  async patchBloqueHorario(@Param('id') id: number, @Body() dto: PatchBloqueReservaDTO) {
    return this.reservaHoraService.patchBloqueHorario(Number(id), dto).then(data => {
      return { code: 200 };
    });
  }

  @Delete('bloque/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina un bloque de horario' })
  async deleteBloqueHorario(@Param('id') id: number, @Req() req) {
    return this.reservaHoraService
      .deleteBloqueHorario(req, Number(id))
      .then(data => {
        return { code: 200 };
      })
      .catch(error => {
        return { code: 400, error: error.message };
      });
  }

  @Post('valida_reservas_postulante')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio que valida si el postulante tiene reserva de horas',
  })
  async valida_tramitres_en_proceso_otra_muni(@Body() data: string) {
    return this.reservaHoraService.valida_reserva_postulante(data);
  }
}
