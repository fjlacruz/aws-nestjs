import { ApiProperty } from "@nestjs/swagger";

export class GetReservasTomadasDTO 
{
    @ApiProperty()
    fecha: string;

    @ApiProperty()
    idMunicipio: number;

}
export class GetHorasDisponibleDTO extends GetReservasTomadasDTO
{
    @ApiProperty()
    idTipoTramite: number;

    @ApiProperty()
    idClaseLicencia: any;
}
export class GetBloquesHorarioDTO 
{
    @ApiProperty()
    fechaDesde: string;

    @ApiProperty()
    fechaHasta: string;

    @ApiProperty()
    idMunicipio: number;

    @ApiProperty()
    idTipoTramite: number;

    @ApiProperty()
    idClaseLicencia: any;

    @ApiProperty()
    idEstadoBloqueReserva: number;
}
export class PostBloqueReservaDTO 
{
    // @ApiProperty()
    // idMunicipio: number;

    @ApiProperty()
    FechaBloque: Date;

    @ApiProperty()
    HoraInicio: string;

    @ApiProperty()
    HoraFin: string;

    // @ApiProperty()
    // idUsuarioCreacion: number;

    @ApiProperty()
    Frecuencia: number;

    @ApiProperty()
    Capacidad: number;

    @ApiProperty()
    idTipoTramite: number;

    @ApiProperty()
    idClaseLicencia: any;

    @ApiProperty()
    idEstadoBloqueReserva: number;

    @ApiProperty()
    FechaBloqueString: string;
}
export class PatchBloqueReservaDTO 
{
    @ApiProperty()
    HoraInicio: string;

    @ApiProperty()
    HoraFin: string;

    @ApiProperty()
    Frecuencia: number;

    @ApiProperty()
    Capacidad: number;

    @ApiProperty()
    idEstadoBloqueReserva: number;
}
export class PostReservaDTO 
{
    @ApiProperty()
    idPostulante: number;

    @ApiProperty()
    idEstadoReserva: number;

    @ApiProperty()
    idHoraporBloque: number;
}
export class PatchReservaAnularDTO
{
    // @ApiProperty()
    // idUsuarioSGL: number;

    @ApiProperty()
    idPostulante: number;    

    @ApiProperty()
    motivoAnulacion: string;
}
export class PatchReservaReprogramarDTO
{
    // @ApiProperty()
    // idUsuarioSGL: number;

    @ApiProperty()
    idHoraporBloque: number;

    @ApiProperty()
    motivoReprogramacion: string;
}

