import {Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('AnulacionesReserva')
export class AnulacionesReservaEntity 
{
    @PrimaryGeneratedColumn()
    idAnulacionReserva: number

    @Column()
    idUsuarioSGL: number;

    @Column()
    idPostulante: number;

    @Column()
    idReserva: number;

    @Column()
    fechaAnulacion: Date;

    @Column()
    motivoAnulacion: string;
}
