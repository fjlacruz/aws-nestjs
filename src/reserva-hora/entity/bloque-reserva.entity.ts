import {Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('BloqueReserva')
export class BloqueReservaEntity 
{
    @PrimaryGeneratedColumn()
    idBloqueReserva: number;

    @Column()
    idMunicipio: number;

    @Column({ type: 'date' })
    FechaBloque: Date;

    @Column({ type: 'time' })
    HoraInicio: string;

    @Column({ type: 'time' })
    HoraFin: string;

    @Column({ type: 'date' })
    FechaCreacion: Date;

    @Column()
    idUsuarioCreacion: number;

    @Column()
    Frecuencia: number;

    @Column()
    Capacidad: number;

    @Column()
    idTipoTramite: number;

    @Column()
    idEstadoBloqueReserva: number;

    @Column()
    idClaseLicencia: number;
}
