import {Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('EstadoBloqueReserva')
export class EstadoBloqueReservaEntity 
{
    @PrimaryGeneratedColumn()
    idEstadoBloqueReserva: number;

    @Column()
    Nombre: string;

    @Column()
    Descripcion: string;
}
