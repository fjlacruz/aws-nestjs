import {Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity('EstadoReserva')
export class EstadoReservaEntity 
{
    @PrimaryGeneratedColumn()
    idEstadoReserva: number;

    @Column()
    Nombre: string;

    @Column()
    Descripcion: string;
}
