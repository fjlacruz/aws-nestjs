import {Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('HorasporBloque')
export class HorasPorBloqueEntity 
{
    @PrimaryGeneratedColumn()
    idHoraporBloque: number;

    @Column()
    idBloqueReserva: number;

    @Column()
    idReserva: number;

    @Column()
    tomada: boolean;

    @Column({ type: 'time' })
    horaInicio: string;
}
