import {Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('ReprogramacionesReserva')
export class ReprogramacionesReservaEntity 
{
    @PrimaryGeneratedColumn()
    idReprogramacionReserva: number

    @Column()
    idReserva: number

    @Column()
    fechaReprogramacion: Date;

    @Column()
    idUsuarioSGL: number;

    @Column()
    idHoraporBloque: number;

    @Column()
    motivoReprogramacion: string;
}
