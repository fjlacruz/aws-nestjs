import {Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('Reserva')
export class ReservaEntity 
{
    @PrimaryGeneratedColumn()
    idReserva: number;

    @Column()
    idPostulante: number;

    @Column()
    idEstadoReserva: number;

    @Column()
    idHoraporBloque: number;
}
