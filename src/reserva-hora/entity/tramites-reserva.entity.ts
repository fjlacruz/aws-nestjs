/* eslint-disable prettier/prettier */
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tramitesReserva')
export class TramitesReservaEntity {
  @PrimaryGeneratedColumn()
  idTramiteReserva: number;

  @Column()
  idReserva: number;

  @Column()
  idTipoTramite: number;

  @Column()
  idClaseLicencia: number;
}
