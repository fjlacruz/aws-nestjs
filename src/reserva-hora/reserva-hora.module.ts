import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReservaHoraController } from './controller/reserva-hora.controller';
import { ReservaHoraService } from './services/reserva-hora.service';
import { EstadoBloqueReservaEntity } from './entity/estado-bloque-reserva.entity';
import { BloqueReservaEntity } from './entity/bloque-reserva.entity';
import { EstadoReservaEntity } from './entity/estado-reserva.entity';
import { ReservaEntity } from './entity/reserva.entity';
import { HorasPorBloqueEntity } from './entity/horas-por-bloque.entity';
import { TramitesReservaEntity } from './entity/tramites-reserva.entity';
import { TramitesReservaService } from './services/tramites-reserva.service';
import { ReprogramacionesReservaEntity } from './entity/reprogramaciones-reserva.entity';
import { AnulacionesReservaEntity } from './entity/anulaciones-reserva.entity';
import { TiposTramitesModule } from 'src/tipos-tramites/tipos-tramites.module';
import { ClasesLicenciaModule } from 'src/clases-licencia/clases-licencia.module';
import { IngresoPostulanteModule } from 'src/ingreso-postulante/ingreso-postulante.module';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { AuthService } from 'src/auth/auth.service';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { ParametrosService } from 'src/parametros/services/parametros.service';
import { ParametrosModule } from 'src/parametros/parametros.module';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { OficinaEntity } from 'src/oficina/oficina.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';

@Module({
  imports: [
   
    TypeOrmModule.forFeature(
      [
        BloqueReservaEntity, EstadoBloqueReservaEntity, EstadoReservaEntity,
        ReservaEntity, HorasPorBloqueEntity, TramitesReservaEntity,
        ReprogramacionesReservaEntity, AnulacionesReservaEntity
      ], "reserva"), 
    TypeOrmModule.forFeature([User, RolesUsuarios,PostulanteEntity,ClasesLicenciasEntity,OficinaEntity,ComunasEntity]),
    TiposTramitesModule,
    ClasesLicenciaModule,
    IngresoPostulanteModule,
    ParametrosModule,
  ],
  providers: [ReservaHoraService, TramitesReservaService, AuthService],
  exports: [ReservaHoraService, TramitesReservaService],
  controllers: [ReservaHoraController]
})
@Module({})
export class ReservaHoraModule { }
