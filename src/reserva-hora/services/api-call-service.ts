import { HttpService, Injectable } from "@nestjs/common";
import { map } from "rxjs/operators";

@Injectable()
export class ApiCallService 
{
    constructor( private readonly httpService: HttpService )
    {}

    auth: string;

    async get( urlMetodo: string  )
    {
        const headers = { 'Accept': 'application/json' };
        const url     = process.env.API_URL + urlMetodo;

        if( this.auth )
            headers['authorizaton'] = "Bearer " + this.auth;

        return this.httpService.get( url, { headers } ).pipe( map( response => response.data ) ).toPromise()
            .then(  response  => response.data )
            .catch( error     => error );
    }
}

