import { Test, TestingModule } from '@nestjs/testing';
import { ReservaHoraService } from './reserva-hora.service';

describe('ReservaHoraService', () => {
  let service: ReservaHoraService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReservaHoraService],
    }).compile();

    service = module.get<ReservaHoraService>(ReservaHoraService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
