/* eslint-disable prettier/prettier */
import { HttpService, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, getConnection, getConnectionManager, getRepository, Repository } from 'typeorm';
import {
  PostReservaDTO,
  PostBloqueReservaDTO,
  PatchBloqueReservaDTO,
  GetReservasTomadasDTO as GetReservasTomadasDTO,
  GetBloquesHorarioDTO,
  GetHorasDisponibleDTO as GetHorasDisponiblesDTO,
  PatchReservaReprogramarDTO,
  PatchReservaAnularDTO,
} from '../dto/reserva-hora.dto';
import { BloqueReservaEntity } from '../entity/bloque-reserva.entity';
import { EstadoBloqueReservaEntity } from '../entity/estado-bloque-reserva.entity';
import { ReservaEntity } from '../entity/reserva.entity';
import { HorasPorBloqueEntity } from '../entity/horas-por-bloque.entity';
import { TramitesReservaService } from './tramites-reserva.service';
import { AnulacionesReservaEntity } from '../entity/anulaciones-reserva.entity';
import { ReprogramacionesReservaEntity } from '../entity/reprogramaciones-reserva.entity';
import { TiposTramitesService } from 'src/tipos-tramites/services/tipos-tramites.service';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';
import { AuthService } from 'src/auth/auth.service';
import { MailerService } from '@nestjs-modules/mailer';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { ParametrosGeneralEntity } from 'src/users/entity/parametros-general.entity';
import { ParametrosService } from 'src/parametros/services/parametros.service';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { OficinaEntity } from 'src/oficina/oficina.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { PermisosNombres } from 'src/constantes';
import { TramitesReservaEntity } from '../entity/tramites-reserva.entity';

@Injectable()
export class ReservaHoraService {
  constructor(
    @InjectRepository(BloqueReservaEntity, 'reserva')
    private readonly bloqueReservaRepository: Repository<BloqueReservaEntity>,

    @InjectRepository(HorasPorBloqueEntity, 'reserva')
    private readonly horasRepository: Repository<HorasPorBloqueEntity>,

    @InjectRepository(ReservaEntity, 'reserva')
    private readonly reservaRepository: Repository<ReservaEntity>,

    @InjectRepository(AnulacionesReservaEntity, 'reserva')
    private readonly anulacionesRepository: Repository<AnulacionesReservaEntity>,

    @InjectRepository(ReprogramacionesReservaEntity, 'reserva')
    private readonly reprogramacionesRepository: Repository<ReprogramacionesReservaEntity>,

    @InjectRepository(PostulanteEntity)
    private readonly postulanteRepository: Repository<PostulanteEntity>,

    @InjectRepository(ClasesLicenciasEntity)
    private readonly claseLicenciaRepository: Repository<ClasesLicenciasEntity>,
    @InjectRepository(OficinaEntity)
    private readonly oficinaRepository: Repository<OficinaEntity>,

    @InjectRepository(ComunasEntity)
    private readonly comunaRepository: Repository<ComunasEntity>,

    @InjectRepository(TramitesReservaEntity, 'reserva')
    private readonly tramitesReservaRepository: Repository<TramitesReservaEntity>,

    private readonly tramitesReservaService: TramitesReservaService,
    private readonly tiposTramitesService: TiposTramitesService,
    private readonly postulanteService: IngresoPostulanteService,
    private readonly authService: AuthService,
    private readonly mailerService: MailerService,
    private readonly parametroService: ParametrosService
  ) {}
  async getReservasTomadas(req, dto: GetReservasTomadasDTO) {
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
      req,
      PermisosNombres.VisualizarListaReservaHorasYBloques
    );

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }

    const { fecha, idMunicipio } = dto;
    const bloques = await this.reservaRepository
      .createQueryBuilder('reserva')
      .select('reserva.idReserva', 'idReserva')
      .addSelect('reserva.idPostulante', 'idPostulante')
      .addSelect('bloque.FechaBloque', 'Fecha')
      .addSelect('bloque.idTipoTramite', 'idTipoTramite')
      .addSelect('bloque.idClaseLicencia', 'idClaseLicencia')
      .addSelect('horas.horaInicio', 'Hora')
      .innerJoin(HorasPorBloqueEntity, 'horas', 'reserva.idHoraporBloque = horas.idHoraporBloque')
      .innerJoin(BloqueReservaEntity, 'bloque', 'bloque.idBloqueReserva  = horas.idBloqueReserva')
      .where('bloque.FechaBloque         = :fecha', { fecha })
      .andWhere('bloque.idMunicipio         = :idMunicipio', { idMunicipio })
      .andWhere('horas.tomada               = :tomada', { tomada: true })
      .andWhere('reserva.idEstadoReserva    < 2')
      .orderBy('horas.horaInicio')
      .getRawMany();
    return Promise.all(
      bloques.map(async (bloque: any, index: number) => {
        const Postulante = await this.getPostulante(bloque.idPostulante);
        const Tramites = await this.getReservaTramites(bloque.idReserva);

        return { ...bloque, Postulante, Tramites };
      })
    );
  }
  async getHorasDisponibles(dto: GetHorasDisponiblesDTO) {
    const { fecha, idTipoTramite, idClaseLicencia, idMunicipio } = dto;
    const whereLicencia = this.buildWhereLicencia(idClaseLicencia);
    const bloques = await this.bloqueReservaRepository
      .createQueryBuilder('bloque')
      .select('horas.idHoraporBloque', 'idHoraporBloque')
      .addSelect('horas.horaInicio', 'Hora')
      .innerJoin(HorasPorBloqueEntity, 'horas', 'bloque.idBloqueReserva = horas.idBloqueReserva')
      .andWhere('bloque.FechaBloque           =  :fecha', { fecha })
      .andWhere('bloque.idMunicipio           =  :idMunicipio', { idMunicipio })
      .andWhere('bloque.idTipoTramite         =  :idTipoTramite', { idTipoTramite })
      .andWhere('horas.tomada                 =  :tomada', { tomada: false })
      .andWhere(whereLicencia, { idClaseLicencia })
      .orderBy('horas.horaInicio')
      .getRawMany();
    return bloques;
  }
  async getBloquesHorario(req, dto: GetBloquesHorarioDTO) {
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
      req,
      PermisosNombres.VisualizarListaReservaHorasYBloques
    );

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }

    const { fechaDesde, fechaHasta, idTipoTramite, idClaseLicencia, idEstadoBloqueReserva, idMunicipio } = dto;
    const whereLicencia = this.buildWhereLicencia(idClaseLicencia);
    const bloques = await this.bloqueReservaRepository
      .createQueryBuilder('bloque')
      .select('bloque.idBloqueReserva', 'idBloqueReserva')
      .addSelect('bloque.FechaBloque', 'FechaBloque')
      .addSelect('bloque.HoraInicio', 'HoraInicio')
      .addSelect('bloque.HoraFin', 'HoraFin')
      .addSelect('bloque.idTipoTramite', 'idTipoTramite')
      .addSelect('bloque.idMunicipio', 'idMunicipio')
      .addSelect('bloque.idEstadoBloqueReserva', 'idEstadoBloqueReserva')
      .addSelect('bloque.Frecuencia', 'Frecuencia')
      .addSelect('bloque.Capacidad', 'Capacidad')
      .addSelect('estado.Nombre', 'Estado')
      .innerJoin(EstadoBloqueReservaEntity, 'estado', 'bloque.idEstadoBloqueReserva = estado.idEstadoBloqueReserva')
      .where('bloque.FechaBloque           >= :fechaDesde', { fechaDesde })
      .andWhere('bloque.FechaBloque           <= :fechaHasta', { fechaHasta })
      .andWhere('bloque.idMunicipio           =  :idMunicipio', { idMunicipio })
      .andWhere('bloque.idTipoTramite         =  :idTipoTramite', { idTipoTramite })
      .andWhere('bloque.idEstadoBloqueReserva =  :idEstadoBloqueReserva', { idEstadoBloqueReserva })
      .andWhere(whereLicencia, { idClaseLicencia })
      .getRawMany();

    return Promise.all(
      bloques.map(async (bloque: any, index: number) => {
        const tramite = await this.getTipoTramite(bloque.idTipoTramite);

        return { ...bloque, tramite };
      })
    );
  }
  async postBloqueHorario(req, dto: PostBloqueReservaDTO) {
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
      req,
      PermisosNombres.ReservadeHorasAdministracionBloquesHorariosCrearBloqueHorario
    );

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }

    const idUsuario = (await this.authService.usuario()).idUsuario;
    const idMunicipio = this.authService.oficina().idOficina;
    const FechaCreacion = new Date();
    console.log(FechaCreacion);
    const data = { ...dto, idMunicipio, idUsuario, FechaCreacion };

    // data.FechaBloque = new Date(data.FechaBloqueString); // Convertir para evitar zona horaria

    const bloque = await this.bloqueReservaRepository.save(data);

    return await this.postHorasPorBloque(bloque);
  }
  private async postHorasPorBloque(bloque: BloqueReservaEntity) {
    const idBloqueReserva = Number(bloque.idBloqueReserva);
    const frecuencia = Number(bloque.Frecuencia);
    const capacidad = Number(bloque.Capacidad);
    const diferencia = this.getTimeDiff(bloque.HoraInicio, bloque.HoraFin);
    const cantidad = diferencia / frecuencia;
    let horaInicio = bloque.HoraInicio;

    this.horasRepository.manager.transaction(async (manager: EntityManager) => {
      for (let turno = 0; turno < cantidad; turno++) {
        for (let puesto = 0; puesto < capacidad; puesto++)
          await manager.save(HorasPorBloqueEntity, { horaInicio, idBloqueReserva, tomada: false });

        horaInicio = this.timeAddMinutes(horaInicio, frecuencia);
      }
    });
    return bloque;
  }
  async patchBloqueHorario(idBloqueReserva: number, dto: PatchBloqueReservaDTO) {
    return await this.bloqueReservaRepository.update({ idBloqueReserva }, dto);
  }
  async deleteBloqueHorario(req, idBloqueReserva: number) {
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
      req,
      PermisosNombres.ReservadeHorasAdministracionBloquesHorariosEliminarBloqueHorario
    );

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }

    await this.deleteHorasBloque(idBloqueReserva);
    return this.bloqueReservaRepository.delete(idBloqueReserva);
  }
  private async deleteHorasBloque(idBloqueReserva: number) {
    return await this.horasRepository
      .createQueryBuilder('horas')
      .delete()
      .where('idBloqueReserva = :idBloqueReserva', { idBloqueReserva })
      .execute();
  }
  async postReserva(dto: PostReservaDTO) {
    const reserva = await this.reservaRepository.save(dto);
    const idHora = dto.idHoraporBloque;
    const idReserva = reserva.idReserva;

    await this.postHoraBloque(idHora, idReserva);
    return reserva;
  }
  async patchReserva(idReserva: number, idEstadoReserva: number) {
    return await this.reservaRepository.update({ idReserva }, { idEstadoReserva });
  }
  async patchReservaReprogramar(idReserva: number, dto: PatchReservaReprogramarDTO) {
    const idUsuarioSGL = (await this.authService.usuario()).idUsuario;
    const fechaReprogramacion = new Date();
    const reprogramacion = { ...dto, fechaReprogramacion, idReserva, idUsuarioSGL };
    const idEstadoReserva = 1; // Reprogramada
    const idHoraporBloque = dto.idHoraporBloque;

    await this.clearHoraBloque(idReserva);
    await this.postHoraBloque(idHoraporBloque, idReserva);
    await this.reprogramacionesRepository.save(reprogramacion);

    this.sendEmailReagendarCancelar(idReserva, idHoraporBloque, false);

    return await this.reservaRepository.update({ idReserva }, { idEstadoReserva, idHoraporBloque });
  }

  private async sendEmailReagendarCancelar(idReserva, idHoraporBloque, isCancelar) {
    const reserva = await this.reservaRepository.findOne(idReserva);
    console.log('========================= reserva ================================================');
    console.log(reserva.idReserva);
    const postulante = await this.postulanteRepository.findOne(reserva.idPostulante);
    console.log('========================= postulante ================================================');
    console.log(postulante);

    const EmailAccount = await getRepository(ParametrosGeneralEntity)
      .createQueryBuilder('ParametrosGeneral')
      .where("ParametrosGeneral.Param = 'correoComunicadosOficiales'")
      .getOne();

    const hora = await this.horasRepository.findOne(idHoraporBloque);

    const tramitesReserva = await this.tramitesReservaRepository.findOne({ idReserva: reserva.idReserva });
    console.log('=========================== tramitesReserva ==============================================');
    console.log(tramitesReserva.idClaseLicencia);

    const bloque = await this.bloqueReservaRepository.findOne(hora.idBloqueReserva);
    console.log('=========================== bloque ==============================================');
    console.log(bloque);

    const tipoTramite = await this.tiposTramitesService.getById(bloque.idTipoTramite);
    // bloque.idClaseLicencia
    const claseLicencia = await this.claseLicenciaRepository.findOne(tramitesReserva.idClaseLicencia);
    console.log('=========================== claseLicencia ==============================================');
    console.log(claseLicencia);
    const oficina = await this.oficinaRepository.findOne(bloque.idMunicipio);
    const comuna = await this.comunaRepository.findOne(oficina.idComuna);
    const header = await this.parametroService.getParametrosByParam('HeaderHTMLCorreos');

    let subject = '';
    let replace = '';
    if (isCancelar) {
      subject = '[No responder]- Cancelamiento Reserva de Hora Licencia de Conducir';
      replace = 'la cancelacion';
    } else {
      subject = '[No responder]- Re agendamiento Reserva de Hora Licencia de Conducir';
      replace = 'el reagendamiento';
    }

    const body = `<br> <p>Estimad@ ${postulante.Nombres}:
    <br>
    <br> Junto con saludar, comunicamos que hemos confirmado ${replace} de su reserva de hora

    <br> Nombre completo: ${postulante.Nombres} ${postulante.ApellidoPaterno} ${postulante.ApellidoMaterno}
    <br> RUN: ${postulante.RUN} - ${postulante.DV}
    <br> Tipo de tramite: ${tipoTramite.Nombre}
    <br> Tipo de licencia: ${claseLicencia.Abreviacion}
    <br> Fecha : ${bloque.FechaBloque}
    <br> Hora : ${hora.horaInicio}
    <br> Municipalidad y Oficina: ${comuna.Nombre}, ${oficina.Nombre}.
    <br> Dirección: ${oficina.Direccion}</p>`;

    const footer = await this.parametroService.getParametrosByParam('footerHTMLCorreoCliente');

    const html = header.Value + body + footer.Value;

    this.mailerService.sendMail({ to: postulante.Email, from: EmailAccount.Value, subject: subject, html: html });
  }

  async patchReservaAnular(idReserva: number, dto: PatchReservaAnularDTO) {
    const idUsuarioSGL = (await this.authService.usuario()).idUsuario;
    const fechaAnulacion = new Date();
    const anulacion = { ...dto, fechaAnulacion, idReserva, idUsuarioSGL };
    const idEstadoReserva = 2; // Anulada
    const idHoraporBloque = null;

    await this.clearHoraBloque(idReserva);
    await this.anulacionesRepository.save(anulacion);

    this.sendEmailReagendarCancelar(idReserva, idHoraporBloque, true);

    return await this.reservaRepository.update({ idReserva }, { idEstadoReserva, idHoraporBloque });
  }
  async deleteReserva(idReserva: number) {
    await this.clearHoraBloque(idReserva);
    return await this.reservaRepository.delete(idReserva);
  }
  private async postHoraBloque(idHoraporBloque: number, idReserva: number) {
    return await this.horasRepository
      .createQueryBuilder('horas')
      .update()
      .set({ idReserva, tomada: true })
      .where('idHoraporBloque = :idHoraporBloque', { idHoraporBloque })
      .execute();
  }
  private async clearHoraBloque(idReserva: number) {
    return await this.horasRepository
      .createQueryBuilder('horas')
      .update()
      .set({ idReserva: null, tomada: false })
      .where('idReserva = :idReserva', { idReserva })
      .execute();
  }
  private async getPostulante(idPostulante: number) {
    return await this.postulanteService.getPostulante(idPostulante);
  }
  private async getTipoTramite(idTipoTramite: number) {
    return await this.tiposTramitesService.getById(idTipoTramite);
  }
  private timeAddMinutes(hora: string, mins: number) {
    const actual = this.getMinutes(hora) + mins;
    const horas = Math.floor(actual / 60);
    const minutos = actual % 60;
    const convertido = horas.toString().padStart(2, '0') + ':' + minutos.toString().padStart(2, '0');

    return convertido;
  }
  private getTimeDiff(horaInicio: string, horaFin: string) {
    const inicio = this.getMinutes(horaInicio);
    const fin = this.getMinutes(horaFin);

    return fin - inicio;
  }
  private getMinutes(hora: string) {
    const horasArray = hora.split(':');
    const horas = Number(horasArray[0]);
    const minutos = Number(horasArray[1]);
    const calculado = horas * 60 + minutos;

    return calculado;
  }
  private async getReservaTramites(idReserva: number) {
    return await this.tramitesReservaService.getTramites(idReserva);
  }
  private buildWhereLicencia(idClaseLicencia: any) {
    let where = '( bloque.idClaseLicencia ';

    if (idClaseLicencia == 'null') where += 'IS NULL';
    else where += '= :idClaseLicencia OR bloque.idClaseLicencia IS NULL';

    return where + ')';
  }

  async valida_reserva_postulante(data): Promise<any> {
    const run = data.run.toString();

    const _run = run.substring(0, run.length - 1);

    const id = await this.postulanteService.getPostulanteByRUN(_run);

    let respuesta;
    if (id == '') {
      respuesta = {
        postulante: 0,
      };
    } else {
      const idP = id[0].idPostulante;

      const validaRes = await getConnection('reserva').query(`select valida_reserva_postulante(${idP})`);
      if (validaRes == '' || validaRes == undefined) {
        respuesta = {
          usuario: 0,
        };
      } else {
        respuesta = {
          postulante: 1,
          validaRes,
        };
      }
    }
    return respuesta;
  }
}
