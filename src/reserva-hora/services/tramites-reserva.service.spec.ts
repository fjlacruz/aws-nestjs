import { Test, TestingModule } from '@nestjs/testing';
import { TramitesReservaService } from './tramites-reserva.service';

describe('TramitesReservaService', () => {
  let service: TramitesReservaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TramitesReservaService],
    }).compile();

    service = module.get<TramitesReservaService>(TramitesReservaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
