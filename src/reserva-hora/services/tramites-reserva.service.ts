/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClasesLicenciaService } from 'src/clases-licencia/services/clases-licencia.service';
import { TiposTramitesService } from 'src/tipos-tramites/services/tipos-tramites.service';
import { Repository } from 'typeorm';
import { TramitesReservaEntity } from '../../reserva-hora/entity/tramites-reserva.entity';

@Injectable()
export class TramitesReservaService {
  constructor(
    @InjectRepository(TramitesReservaEntity, 'reserva')
    private readonly tramitesReservaRepository: Repository<TramitesReservaEntity>,

    private readonly tiposTramitesService: TiposTramitesService,
    private readonly clasesLicenciaService: ClasesLicenciaService
  ) {}
  async getTramites(idReserva: number) {
    const tramites = await this.tramitesReservaRepository
      .createQueryBuilder('tramites')
      .where('tramites.idReserva = :idReserva', { idReserva })
      .getMany();

    return Promise.all(
      tramites.map(async (tramite: TramitesReservaEntity) => {
        const TipoTramite = await this.getTipoTramite(tramite.idTipoTramite);
        const ClaseLicencia = await this.getClaseLicencia(tramite.idClaseLicencia);

        return { ...tramite, TipoTramite, ClaseLicencia };
      })
    );
  }
  private async getTipoTramite(idTipoTramite: number) {
    return await this.tiposTramitesService.getById(idTipoTramite);
  }
  private async getClaseLicencia(idClaseLicencia: number) {
    return await this.clasesLicenciaService.getById(idClaseLicencia);
  }
}
