import { ApiPropertyOptional } from '@nestjs/swagger';

export class InfraccionesDto {
  @ApiPropertyOptional()
  idConductor?: number;
  @ApiPropertyOptional()
  idDocumentoLicencia?: null;
  @ApiPropertyOptional()
  idInfraccion?: number;
  @ApiPropertyOptional()
  idOficina?: null;
  @ApiPropertyOptional()
  codJPL?: string;
  @ApiPropertyOptional()
  fechaDenuncia?: Date;
  @ApiPropertyOptional()
  fechaResolucion?: string;
  @ApiPropertyOptional()
  nroProceso?: string;
  @ApiPropertyOptional()
  anoProceso?: string;
  @ApiPropertyOptional()
  suspension?: boolean;
  @ApiPropertyOptional()
  diasSuspension?: number;
  @ApiPropertyOptional()
  cancelacion?: boolean;
  @ApiPropertyOptional()
  tiempoCancelacion?: number;
  @ApiPropertyOptional()
  cancelacionPerpetua?: boolean;
  @ApiPropertyOptional()
  multa?: boolean;
  @ApiPropertyOptional()
  montoMulta?: null;
  @ApiPropertyOptional()
  idtipoUnidadMonetaria?: null;
  @ApiPropertyOptional()
  amonestacion?: boolean;
  @ApiPropertyOptional()
  anotacion?: any;
  @ApiPropertyOptional()
  created_at?: null;
  @ApiPropertyOptional()
  updated_at?: null;
  @ApiPropertyOptional()
  ingresadoPor?: null;
  @ApiPropertyOptional()
  fechaResolucionDesde?: null;
  @ApiPropertyOptional()
  fechaResolucionHasta?: null;
  // postulante - conductor
  @ApiPropertyOptional()
  run?: any;
  @ApiPropertyOptional()
  infraccionDetalle?: string;
  @ApiPropertyOptional()
  amonestacionDetalle?: string;
  @ApiPropertyOptional()
  cancelacionDetalle?: string;
  @ApiPropertyOptional()
  anotacionDetalle?: string;
}
