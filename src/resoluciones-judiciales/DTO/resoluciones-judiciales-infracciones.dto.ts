import { ApiPropertyOptional } from '@nestjs/swagger';
import { InfraccionesDto } from './infracciones.dto';

export class ResolucionesJudicialesInfraccionesDto {
  @ApiPropertyOptional()
  idresolucionJudicial: number;

  @ApiPropertyOptional()
  idConductor: number;

  @ApiPropertyOptional()
  idDocumentoLicencia: number;

  @ApiPropertyOptional()
  idOficina: number;

  @ApiPropertyOptional()
  codJPL: string;

  @ApiPropertyOptional()
  fechaDenuncia: Date;

  @ApiPropertyOptional()
  fechaResolucion: Date;

  @ApiPropertyOptional()
  nroProceso: number;

  @ApiPropertyOptional()
  anoProceso: number;

  @ApiPropertyOptional()
  updated_at: Date;

  @ApiPropertyOptional()
  created_at: Date;

  @ApiPropertyOptional()
  ingresadoPor: number;

  @ApiPropertyOptional()
  fechaResolucionDesde: Date;

  @ApiPropertyOptional()
  fechaResolucionHasta: Date;

  @ApiPropertyOptional()
  idRegion: number;

  @ApiPropertyOptional()
  idComuna: number;

  @ApiPropertyOptional()
  anotacion: number;

  @ApiPropertyOptional()
  infracciones: InfraccionesDto[];

  @ApiPropertyOptional()
  run: string;
}
