import { Test, TestingModule } from '@nestjs/testing';
import { ResolucionesJudicialesController } from './resoluciones-judiciales.controller';

describe('ResolucionesJudicialesController', () => {
  let controller: ResolucionesJudicialesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ResolucionesJudicialesController],
    }).compile();

    controller = module.get<ResolucionesJudicialesController>(ResolucionesJudicialesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
