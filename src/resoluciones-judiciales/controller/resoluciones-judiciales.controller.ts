import { Controller, DefaultValuePipe, ParseIntPipe, Query, Req, UseGuards } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth } from '@nestjs/swagger';
import { ResolucionesJudicialesDTO } from '../DTO/resoluciones-judiciales.dto';
import { ResolucionesJudicialesService } from '../services/resoluciones-judiciales.service';
import { AuthGuard } from '@nestjs/passport';
import { ResolucionesJudicialesInfraccionesDto } from '../DTO/resoluciones-judiciales-infracciones.dto';

@ApiTags('Resoluciones-judiciales')
@Controller('resoluciones-judiciales')
export class ResolucionesJudicialesController {
  constructor(private readonly resolucionesJudicialesService: ResolucionesJudicialesService) {}

  @Patch('resolucionJudicialUpdate/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza datos de una resolucion judicial' })
  async comunaUpdate(@Param('id') id: number, @Body() data: Partial<ResolucionesJudicialesDTO>) {
    return await this.resolucionesJudicialesService.update(id, data);
  }

  @Post('crearResolucionJudicial')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea una nueva resolucion judicial' })
  async addComuna(@Body() resolucionJudicialesDTO: ResolucionesJudicialesInfraccionesDto, @Req() req: any) {
    console.log(resolucionJudicialesDTO);
    const data = await this.resolucionesJudicialesService.create(req, resolucionJudicialesDTO);
    return { data };
  }

  @Get('resolucionesJudiciales')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todas las resoluciones Judiciales' })
  async getResolucionesJudiciales() {
    const data = await this.resolucionesJudicialesService.getResolucionesJudiciales();
    return { data };
  }

  @Get('search')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Lista de Resoluciones Judiciales.',
    type: ResolucionesJudicialesDTO,
  })
  async searchResolucionesJudiciales(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit = 10,
    @Query() query: ResolucionesJudicialesDTO,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {
    const data = await this.resolucionesJudicialesService.findAllByQueryParams(
      req,
      query,
      {
        page,
        limit,
        route: '/resoluciones-judiciales/search',
      },
      orden.toString(),
      ordenarPor.toString()
    );
    return data;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una resolucion Judicial por Id' })
  async resolucionesJudicialesById(@Param('id') id: number) {
    return await this.resolucionesJudicialesService.findOne({ where: { idresolucionJudicial: id } });
  }
}
