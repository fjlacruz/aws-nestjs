import { JplSentenciasEntity } from './jpl-sentencias.entity';
import { JplSentenciasAuxEntity } from './jpl-sentencias-aux.entity';

export class JplIntegracionEntity {
  idUsuario: string;
  Sentencias: JplSentenciasEntity[];
  SentenciasAux: JplSentenciasAuxEntity[];
}
