export class JplSentenciasAuxEntity {
  senauxJuzgado: string;
  senauxFechaResol: string;
  senauxNroProceso: string;
  senauxAnoProceso: string;
  senauxRun: string;
  senauxCorrelativo: string;
  senauxCodInf: string;
  senauxCodRes: string;
}
