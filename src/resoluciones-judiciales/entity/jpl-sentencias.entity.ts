export class JplSentenciasEntity {
  senJuzgado: string;
  senFechaResol: string;
  senNroProceso: string;
  senAnoProceso: string;
  senRun: string;
  senUnidadPolicial: string;
  senNumeroParte: string;
  senFechaDenuncia: string;
  senCodInf001: string;
  senCodInf002: string;
  senCodInf003: string;
  senCodRes001: string;
  senCodRes002: string;
  senCodRes003: string;
  senSuspencionTot: string;
  senPatente: string;
  senOrdenElim: string;
  senFechaOs: string;
  senFlagEliminado: string;
  senFechaIngreso: string;
  senFechaRecepDoc: string;
  senFechaIngSent: string;
  senTipoIngreso: string;
}
