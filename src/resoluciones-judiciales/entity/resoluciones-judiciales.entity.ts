import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from 'typeorm';
import { ConductoresEntity } from '../../conductor/entity/conductor.entity';
import { PostulantesEntity } from '../../tramites/entity/postulantes.entity';
import { ApiProperty } from '@nestjs/swagger';
import { SentenciaEntity } from '../../sentencia/entity/sentencia.entity';

@Entity('ResolucionesJudiciales')
export class ResolucionesJudicialesEntity {
  @PrimaryGeneratedColumn()
  idresolucionJudicial: number;
  @Column()
  idConductor: number;

  @ManyToOne(() => ConductoresEntity, conductor => conductor.resolucion)
  @JoinColumn({ name: 'idConductor' })
  readonly conductor: ConductoresEntity;

  @Column()
  idDocumentoLicencia: number;
  @Column()
  idOficina: number;
  @Column()
  fechaDenuncia: Date;
  @Column()
  fechaResolucion: Date;
  @Column()
  nroProceso: number;
  @Column()
  anoProceso: number;
  @Column()
  updated_at: Date;
  @Column()
  created_at: Date;
  @Column()
  ingresadoPor: number;
  @Column()
  idPostulante?: number;

  @ApiProperty()
  @OneToOne(() => SentenciaEntity, sentencia => sentencia.resolucion)
  readonly sentencia: SentenciaEntity;
}
