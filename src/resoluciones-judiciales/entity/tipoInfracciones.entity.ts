import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('tipoInfracciones')
export class TipoInfraccionesEntity {
  @PrimaryGeneratedColumn()
  idInfraccion: number;

  @Column()
  codInfraccion: number;

  @Column()
  descripcion: string;
}
