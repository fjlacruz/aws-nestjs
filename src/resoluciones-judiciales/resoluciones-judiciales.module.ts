import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResolucionesJudicialesController } from './controller/resoluciones-judiciales.controller';
import { ResolucionesJudicialesEntity } from './entity/resoluciones-judiciales.entity';
import { ResolucionesJudicialesService } from './services/resoluciones-judiciales.service';
import { InfraccionEntity } from '../infraccion/infraccion.entity';
import { SentenciaEntity } from '../sentencia/entity/sentencia.entity';
import { AnotacionesResolucionEntity } from '../anotaciones-resolucion/anotaciones-resolucion.entity';
import { OficinasEntity } from '../tramites/entity/oficinas.entity';
import { ConductoresEntity } from '../conductor/entity/conductor.entity';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ResolucionesJudicialesEntity,
      InfraccionEntity,
      SentenciaEntity,
      AnotacionesResolucionEntity,
      OficinasEntity,
      ConductoresEntity,
      User,
      RolesUsuarios
    ]),
  ],
  providers: [ResolucionesJudicialesService, AuthService],
  exports: [ResolucionesJudicialesService],
  controllers: [ResolucionesJudicialesController],
})
export class ResolucionesJudicialesModule {}
