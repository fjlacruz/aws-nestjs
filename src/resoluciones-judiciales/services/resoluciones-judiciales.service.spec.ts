import { Test, TestingModule } from '@nestjs/testing';
import { ResolucionesJudicialesService } from './resoluciones-judiciales.service';

describe('ResolucionesJudicialesService', () => {
  let service: ResolucionesJudicialesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ResolucionesJudicialesService],
    }).compile();

    service = module.get<ResolucionesJudicialesService>(ResolucionesJudicialesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
