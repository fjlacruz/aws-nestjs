import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { FindManyOptions, Repository } from 'typeorm';
import { getConnection } from 'typeorm';
import { ResolucionesJudicialesDTO } from '../DTO/resoluciones-judiciales.dto';
import { ResolucionesJudicialesEntity } from '../entity/resoluciones-judiciales.entity';
import { OficinasEntity } from '../../tramites/entity/oficinas.entity';
import { ComunasEntity } from '../../comunas/entity/comunas.entity';
import { RegionesEntity } from '../../regiones/entity/regiones.entity';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import * as moment from 'moment';
import { User } from '../../users/entity/user.entity';
import { ResolucionesJudicialesInfraccionesDto } from '../DTO/resoluciones-judiciales-infracciones.dto';
import { SentenciaEntity } from '../../sentencia/entity/sentencia.entity';
import { InfraccionEntity } from '../../infraccion/infraccion.entity';
import { AnotacionesResolucionEntity } from '../../anotaciones-resolucion/anotaciones-resolucion.entity';
import { JplIntegracionEntity } from '../entity/jpl-integracion.entity';
import { JplSentenciasEntity } from '../entity/jpl-sentencias.entity';
import { JplSentenciasAuxEntity } from '../entity/jpl-sentencias-aux.entity';
import axios from 'axios';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';

@Injectable()
export class ResolucionesJudicialesService {
  constructor(
    @InjectRepository(ResolucionesJudicialesEntity) private readonly resolucionesJudicialesEntity: Repository<ResolucionesJudicialesEntity>,
    @InjectRepository(InfraccionEntity) private readonly infraccionEntityRepository: Repository<InfraccionEntity>,
    @InjectRepository(SentenciaEntity) private readonly sentenciaEntityRepository: Repository<SentenciaEntity>,
    @InjectRepository(AnotacionesResolucionEntity)
    private readonly anotacionesResolucionEntityRepository: Repository<AnotacionesResolucionEntity>,
    @InjectRepository(OficinasEntity)
    private readonly oficinasEntityRepository: Repository<OficinasEntity>,
    @InjectRepository(ConductoresEntity)
    private readonly conductoresEntityRepository: Repository<ConductoresEntity>,
    private readonly authService: AuthService
  ) { }

  async findAndCount(
    options: FindManyOptions<ResolucionesJudicialesEntity>,
    query: ResolucionesJudicialesDTO
  ): Promise<[ResolucionesJudicialesEntity[], number]> {
    const resultList = await this.resolucionesJudicialesEntity.findAndCount(options);
    const bindDTO: ResolucionesJudicialesEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(bind => bindDTO.push(bind));
      resultList[0] = bindDTO;
    }
    return resultList;
  }

  async update(idresolucionJudicial: number, data: Partial<ResolucionesJudicialesDTO>) {
    await this.resolucionesJudicialesEntity.update({ idresolucionJudicial }, data);
    return await this.resolucionesJudicialesEntity.findOne({ idresolucionJudicial });
  }

  async create(req: any, data: ResolucionesJudicialesInfraccionesDto): Promise<ResolucionesJudicialesEntity> {
    data.created_at = new Date();
    data.updated_at = new Date();
    data.fechaDenuncia = moment(data.fechaDenuncia.toString(), 'YYYY-MM-DD').toDate();
    data.fechaResolucion = moment(data.fechaResolucion.toString(), 'YYYY-MM-DD').toDate();
    const resolucion: ResolucionesJudicialesEntity = await this.resolucionesJudicialesEntity.save(data);
    try {

      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.JuzgadodePoliciaLocalResolucionesJudicialesCrearNuevaResolucion);
    
      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return null;
      }	  

      const sentencias: SentenciaEntity[] = [];
      for (const infraccion of data.infracciones) {
        const infrac: InfraccionEntity = new InfraccionEntity();
        infrac.idTipoInfraccion = infraccion.idInfraccion;
        infrac.fechaDenunca = infraccion.fechaDenuncia;
        const infaccionSaved = await this.infraccionEntityRepository.save(infrac);
        const sentencia: SentenciaEntity = new SentenciaEntity();
        sentencia.idresolucionJudicial = resolucion.idresolucionJudicial;
        sentencia.idInfracciones = infrac.idInfracciones;
        sentencia.suspension = infraccion.suspension;
        sentencia.diasTotalesSuspension = infraccion.diasSuspension;
        sentencia.amonestacion = infraccion.amonestacion;
        sentencia.multa = infraccion.multa;
        sentencia.montoMulta = infraccion.montoMulta;
        sentencia.idtipoUnidadMonetaria = infraccion.idtipoUnidadMonetaria;
        sentencia.cancelacion = infraccion.cancelacion;
        sentencia.aniosCancelacion = infraccion.tiempoCancelacion;
        sentencia.cancelacionPerpetua = infraccion.cancelacionPerpetua;
        const sentenciasaved = await this.sentenciaEntityRepository.save(sentencia);
        sentencias.push(sentenciasaved);
        if (infraccion.anotacion !== null && infraccion.anotacion !== undefined && infraccion.anotacion !== '') {
          const anotacion: AnotacionesResolucionEntity = new AnotacionesResolucionEntity();
          anotacion.idTipoAnotaciones = infraccion.anotacion;
          anotacion.idSentencias = sentenciasaved.idSentencias;
          anotacion.ingresadaPor = data.ingresadoPor;
          anotacion.created_at = new Date();
          anotacion.observacion = '';
          await this.anotacionesResolucionEntityRepository.save(anotacion);
        }
      }
      //await this.callIntegrationJplService(data, resolucion, sentencias);
      return resolucion;
    } catch (error) {
      console.log(error);
      await this.resolucionesJudicialesEntity.delete(resolucion.idresolucionJudicial);
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private async callIntegrationJplService(
    data: ResolucionesJudicialesInfraccionesDto,
    resolucion: ResolucionesJudicialesEntity,
    sentencias: SentenciaEntity[]
  ) {
    const jplIntegracion: JplIntegracionEntity = new JplIntegracionEntity();
    jplIntegracion.idUsuario = data.ingresadoPor.toString();
    jplIntegracion.Sentencias = [];
    const sentencia: JplSentenciasEntity = new JplSentenciasEntity();
    const sentenciaAux1: JplSentenciasAuxEntity = new JplSentenciasAuxEntity();
    const sentenciaAux2: JplSentenciasAuxEntity = new JplSentenciasAuxEntity();
    const oficina: OficinasEntity = await this.oficinasEntityRepository.findOne(resolucion.idOficina);
    const conductor: ConductoresEntity = await this.conductoresEntityRepository.findOne(resolucion.idConductor);

    sentencia.senJuzgado = oficina.codigoSRCeI;
    sentencia.senFechaResol = moment(resolucion.fechaResolucion).format('YYYYMMDD');
    sentencia.senNroProceso = resolucion.nroProceso.toString();
    sentencia.senAnoProceso = resolucion.anoProceso.toString();
    sentencia.senRun = data.run;
    sentencia.senUnidadPolicial = '';
    sentencia.senNumeroParte = '';
    sentencia.senFechaDenuncia = moment(resolucion.fechaDenuncia).format('YYYYMMDD');
    sentencia.senCodInf001 = sentencias[0] !== null && sentencias[0] !== undefined ? sentencias[0]?.idInfracciones.toString() : '';
    sentencia.senCodInf002 = sentencias[1] !== null && sentencias[1] !== undefined ? sentencias[1]?.idInfracciones.toString() : '';
    sentencia.senCodInf003 = sentencias[2] !== null && sentencias[2] !== undefined ? sentencias[2]?.idInfracciones.toString() : '';
    sentencia.senCodRes001 = sentencias[0] !== null && sentencias[0] !== undefined ? sentencias[0]?.idSentencias.toString() : '';
    sentencia.senCodRes002 = sentencias[1] !== null && sentencias[1] !== undefined ? sentencias[1]?.idSentencias.toString() : '';
    sentencia.senCodRes003 = sentencias[2] !== null && sentencias[2] !== undefined ? sentencias[2]?.idSentencias.toString() : '';
    sentencia.senPatente = '';
    sentencia.senOrdenElim = '';
    sentencia.senFechaOs = '';
    sentencia.senFlagEliminado = '';
    sentencia.senFechaIngreso = '';
    sentencia.senFechaRecepDoc = '';
    sentencia.senFechaIngSent = '';
    sentencia.senTipoIngreso = '3';
    jplIntegracion.Sentencias.push(sentencia);
    if (sentencias.length === 4) {
      jplIntegracion.SentenciasAux = [];
      sentenciaAux1.senauxJuzgado = oficina.codigoSRCeI;
      sentenciaAux1.senauxFechaResol = moment(resolucion.fechaResolucion).format('YYYYMMDD');
      sentenciaAux1.senauxNroProceso = resolucion.nroProceso.toString();
      sentenciaAux1.senauxAnoProceso = resolucion.anoProceso.toString();
      sentenciaAux1.senauxRun = conductor.postulante.RUN + '-' + conductor.postulante.DV;
      sentenciaAux1.senauxCorrelativo = '1';
      sentenciaAux1.senauxCodInf = sentencias[3]?.idInfracciones.toString();
      sentenciaAux1.senauxCodRes = sentencias[3]?.idSentencias.toString();
      jplIntegracion.SentenciasAux.push(sentenciaAux1);
    }
    if (sentencias.length === 5) {
      // jplIntegracion.SentenciasAux = [];
      sentenciaAux2.senauxJuzgado = oficina.codigoSRCeI;
      sentenciaAux2.senauxFechaResol = moment(resolucion.fechaResolucion).format('YYYYMMDD');
      sentenciaAux2.senauxNroProceso = resolucion.nroProceso.toString();
      sentenciaAux2.senauxAnoProceso = resolucion.anoProceso.toString();
      sentenciaAux2.senauxRun = conductor.postulante.RUN + '-' + conductor.postulante.DV;
      sentenciaAux2.senauxCorrelativo = '1';
      sentenciaAux2.senauxCodInf = sentencias[4]?.idInfracciones.toString();
      sentenciaAux2.senauxCodRes = sentencias[4]?.idSentencias.toString();
      jplIntegracion.SentenciasAux.push(sentenciaAux2);
    }

    console.log(JSON.stringify(this.getJsonJPl(jplIntegracion)));
    const sentenciaSended = await this.callSaveSentencia(JSON.stringify(this.getJsonJPl(jplIntegracion)));
    console.log('---------------------------------------------------');
    console.log('sentenciaSended');
    console.log(sentenciaSended);
  }

  private getJsonJPl(jplIntegracion: JplIntegracionEntity): any {
    const datosJson = [];
    const id_usu = {
      idUsuario: jplIntegracion.idUsuario,
    };
    const sentencias = [];

    jplIntegracion.Sentencias.forEach(sentencia => {
      sentencias.push({
        senJuzgado: sentencia.senJuzgado,
        senFechaResol: sentencia.senFechaResol,
        senNroProceso: sentencia.senNroProceso,
        senAnoProceso: sentencia.senAnoProceso,
        senRun: sentencia.senRun,
        senUnidadPolicial: sentencia.senUnidadPolicial,
        senNumeroParte: sentencia.senNumeroParte,
        senFechaDenuncia: sentencia.senFechaDenuncia,
        senCodInf001: sentencia.senCodInf001,
        senCodInf002: sentencia.senCodInf002,
        senCodInf003: sentencia.senCodInf003,
        senCodRes001: sentencia.senCodRes001,
        senCodRes002: sentencia.senCodRes002,
        senCodRes003: sentencia.senCodRes003,
        senPatente: sentencia.senPatente,
        senOrdenElim: sentencia.senOrdenElim,
        senFechaOs: sentencia.senFechaOs,
        senFlagEliminado: sentencia.senFlagEliminado,
        senFechaIngreso: sentencia.senFechaIngreso,
        senFechaRecepDoc: sentencia.senFechaRecepDoc,
        senFechaIngSent: sentencia.senFechaIngSent,
        senTipoIngreso: sentencia.senTipoIngreso,
      });
    });
    const sentencia = {
      Sentencias: sentencias,
    };
    const sentenciasAux = [];
    jplIntegracion.SentenciasAux?.forEach(aux => {
      sentenciasAux.push(aux);
    });
    const sentenciaAUX = {
      SentenciasAux: sentenciasAux,
    };
    datosJson.push(id_usu);
    datosJson.push(sentencia);
    datosJson.push(sentenciaAUX);
    console.log(datosJson);
    return datosJson;
  }

  async callSaveSentencia(data: any) {
    return await axios.post('https://4qnzjlsq1k.execute-api.sa-east-1.amazonaws.com/api/sentencias', data);
  }

  async getResolucionesJudiciales() {
    return await this.resolucionesJudicialesEntity.find();
  }

  async getResolucionesJudiciale(idresolucionJudicial: number) {
    return await getConnection()
      .createQueryBuilder(ResolucionesJudicialesEntity, 'res')
      .select('res.idresolucionJudicial', 'idresolucionJudicial')
      .addSelect('res.codJPL', 'codJPL')
      .addSelect('res.fechaDenuncia', 'fechaDenuncia')
      .addSelect('res.fechaResolucion', 'fechaResolucion')
      .addSelect('res.nroProceso', 'nroProceso')
      .addSelect('res.diasSuspensión', 'diasSuspensión')
      .addSelect('res.multado', 'multado')
      .addSelect('res.amonestacion', 'amonestacion')
      .addSelect('res.ingresadoPor', 'ingresadoPor')
      .addSelect('res.suspension', 'suspension')
      .addSelect('cond.idConductor', 'idConductor')
      .addSelect('pos.RUN', 'RUN')
      .addSelect('pos.DV', 'DV')
      .addSelect('pos.Nombres', 'Nombres')
      .addSelect('pos.ApellidoPaterno', 'ApellidoPaterno')
      .addSelect('lic.idDocumentoLicencia', 'idDocumentoLicencia')
      .addSelect('lic.idClaseLicencia', 'idClaseLicencia')
      .addSelect('lic.Folio', 'Folio')
      .addSelect('lic.Observaciones', 'Observaciones')
      .addSelect('tip.idInfraccion', 'idInfraccion')
      .addSelect('tip.codInfraccion', 'codInfraccion')
      .addSelect('tip.descripcion', 'descripcion')
      .addSelect('ins.idInstitucion', 'idInstitucion')
      .addSelect('ins.Nombre', 'Nombre')
      .addSelect('ins.Descripcion', 'Descripcion')

      .innerJoin(ConductoresEntity, 'cond', 'cond.idConductor = res.idConductor')
      .innerJoin(PostulantesEntity, 'pos', 'pos.idPostulante = cond.idPostulante')
      .innerJoin(DocumentosLicencia, 'lic', 'lic.idDocumentoLicencia = res.idDocumentoLicencia')
      .innerJoin(User, 'user', 'user.idUsuario = res.ingresadoPor')
      .innerJoin(OficinasEntity, 'oficina', 'oficina.idOficina = res.idOficina')

      .where('(res.idresolucionJudicial = :idresolucionJudicial )', { idresolucionJudicial })

      .getRawMany();
  }

  async findAllByQueryParams(req: any, query: ResolucionesJudicialesDTO, options: IPaginationOptions, orden?: string, ordenarPor?: string) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaJuzgadoPoliciaLocalIngresoDeResolucionesJudiciales);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	      

    const resoluciones = this.resolucionesJudicialesEntity
      .createQueryBuilder('resolucionesJudiciales')
      .leftJoinAndMapMany(
        'resolucionesJudiciales.idOficina',
        OficinasEntity,
        'oficina',
        'resolucionesJudiciales.idOficina = oficina.idOficina'
      )
      .leftJoinAndMapMany('oficina.idComuna', ComunasEntity, 'comuna', 'oficina.idComuna = comuna.idComuna')
      .leftJoinAndMapMany('comuna.idRegion', RegionesEntity, 'region', 'comuna.idRegion = region.idRegion')
      .leftJoinAndMapMany(
        'resolucionesJudiciales.idConductor',
        ConductoresEntity,
        'conductor',
        'resolucionesJudiciales.idConductor = conductor.idConductor'
      )
      .leftJoinAndMapMany('conductor.idPostulante', PostulantesEntity, 'postulante', 'conductor.idPostulante = postulante.idPostulante');
    if (query.nroProceso) {
      resoluciones.andWhere(' CAST(resolucionesJudiciales.nroProceso AS TEXT) LIKE :nroProceso ', { nroProceso: `%${query.nroProceso}%` });
    }
    if (query.anoProceso) {
      resoluciones.andWhere('resolucionesJudiciales.anoProceso = :anoProceso', { anoProceso: query.anoProceso });
    }
    if (query.codJPL) {
      resoluciones.andWhere('resolucionesJudiciales.idOficina = :codJPL', { codJPL: query.codJPL });
    }
    if (query.fechaResolucionDesde && query.fechaResolucionHasta) {
      resoluciones.andWhere('resolucionesJudiciales.fechaResolucion BETWEEN :fechaResolucionDesde AND :fechaResolucionHasta', {
        fechaResolucionDesde: query.fechaResolucionDesde,
        fechaResolucionHasta: query.fechaResolucionHasta,
      });
    }

    if (query.fechaResolucionDesde == undefined && query.fechaResolucionHasta) {
      resoluciones.andWhere('resolucionesJudiciales.fechaResolucion <= :fechaResolucionHasta', {
        fechaResolucionHasta: query.fechaResolucionHasta,
      });
    }
 /*    console.log(query.fechaResolucionDesde)
    console.log(query.fechaResolucionHasta) */


    if (query.idRegion) {
      resoluciones.andWhere('comuna.idRegion = :idRegion', { idRegion: query.idRegion });
    }
    if (query.idComuna) {
      resoluciones.andWhere('comuna.idComuna = :idComuna', { idComuna: query.idComuna });
    }
    if (orden === 'nroProceso' && ordenarPor === 'ASC') resoluciones.orderBy('resolucionesJudiciales.nroProceso', 'ASC');
    if (orden === 'nroProceso' && ordenarPor === 'DESC') resoluciones.orderBy('resolucionesJudiciales.nroProceso', 'DESC');
    if (orden === 'anoProceso' && ordenarPor === 'ASC') resoluciones.orderBy('resolucionesJudiciales.anoProceso', 'ASC');
    if (orden === 'anoProceso' && ordenarPor === 'DESC') resoluciones.orderBy('resolucionesJudiciales.anoProceso', 'DESC');
    if (orden === 'codigoJPL' && ordenarPor === 'ASC') resoluciones.orderBy('oficina.Nombre', 'ASC');
    if (orden === 'codigoJPL' && ordenarPor === 'DESC') resoluciones.orderBy('oficina.Nombre', 'DESC');
    if (orden === 'fechaDenuncia' && ordenarPor === 'ASC') resoluciones.orderBy('resolucionesJudiciales.fechaDenuncia', 'ASC');
    if (orden === 'fechaDenuncia' && ordenarPor === 'DESC') resoluciones.orderBy('resolucionesJudiciales.fechaDenuncia', 'DESC');
    if (orden === 'fechaResolucion' && ordenarPor === 'ASC') resoluciones.orderBy('resolucionesJudiciales.fechaResolucion', 'ASC');
    if (orden === 'fechaResolucion' && ordenarPor === 'DESC') resoluciones.orderBy('resolucionesJudiciales.fechaResolucion', 'DESC');
    if (orden === 'runConductor' && ordenarPor === 'ASC') resoluciones.orderBy('postulante.RUN', 'ASC');
    if (orden === 'runConductor' && ordenarPor === 'DESC') resoluciones.orderBy('postulante.RUN', 'DESC');
    return paginate<ResolucionesJudicialesEntity>(resoluciones, options);
  }

  async findOne(options: FindManyOptions<ResolucionesJudicialesEntity>): Promise<ResolucionesJudicialesEntity> {
    return await this.resolucionesJudicialesEntity.findOne(options);
  }
}
