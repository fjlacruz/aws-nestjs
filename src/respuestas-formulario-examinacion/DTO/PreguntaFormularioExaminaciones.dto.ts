import { ApiPropertyOptional } from '@nestjs/swagger';

export class PreguntaFormularioExaminacionesDTO {
  @ApiPropertyOptional()
  idRespuestaFormExam: number;

  @ApiPropertyOptional()
  idResultadoExam: number;

  @ApiPropertyOptional()
  idPostulante: number;

  @ApiPropertyOptional()
  idFomularioPregunta: number;

  @ApiPropertyOptional()
  idPreguntaExam: number;

  @ApiPropertyOptional()
  idRespuesta: number;

  @ApiPropertyOptional()
  respuestaAbierta: string;

  @ApiPropertyOptional()
  respuestaBooleana: boolean;

  @ApiPropertyOptional()
  respuestaHora: any;

  @ApiPropertyOptional()
  respuestaFecha: Date;

  @ApiPropertyOptional()
  respuestaInt: number;

  @ApiPropertyOptional()
  idUsuarioExaminador: number;

  @ApiPropertyOptional()
  idUsuario: number;

  @ApiPropertyOptional()
  idColaExaminacion: number;

  @ApiPropertyOptional()
  idArchivosRespuesta: number;

  @ApiPropertyOptional()
  nombreArchivo: string;

  @ApiPropertyOptional()
  archivo: Buffer;

  @ApiPropertyOptional()
  idTramite: number;

  @ApiPropertyOptional()
  idSolicitud: number;
}
