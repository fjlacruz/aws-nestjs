import { ApiPropertyOptional } from "@nestjs/swagger";
export class ResultadoExaminacionColaDTO {
    

    @ApiPropertyOptional()
    idResultadoExam:number;

    @ApiPropertyOptional()
    idColaExaminacion:number;
}
