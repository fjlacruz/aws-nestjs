import { Test, TestingModule } from '@nestjs/testing';
import { RespuestasFormularioExaminacionController } from './respuestas-formulario-examinacion.controller';

describe('RespuestasFormularioExaminacionController', () => {
  let controller: RespuestasFormularioExaminacionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RespuestasFormularioExaminacionController],
    }).compile();

    controller = module.get<RespuestasFormularioExaminacionController>(RespuestasFormularioExaminacionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
