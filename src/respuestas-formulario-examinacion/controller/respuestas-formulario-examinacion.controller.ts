import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { PreguntaFormularioExaminacionesDTO } from '../DTO/PreguntaFormularioExaminaciones.dto';
import { RespuestasFormularioExaminacionService } from '../services/respuestas-formulario-examinacion.service';

@ApiTags('Respuestas-formulario-examinacion')
@Controller('respuestas-formulario-examinacion')
export class RespuestasFormularioExaminacionController {
  constructor(private readonly respuestasFormularioExaminacionService: RespuestasFormularioExaminacionService) {}

  @Get('getOpcionesPreguntaByIdPregunta/:idPreguntaExam')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las posibles respuestas de una pregunta de selección por idPreguntaExam' })
  async getOpcionesPreguntaByIdPregunta(@Param('idPreguntaExam') idPreguntaExam: number) {
    const data = await this.respuestasFormularioExaminacionService.getOpcionesPreguntaByIdPregunta(idPreguntaExam);

    return { data };
  }

  @Get('getRespuestaPregunta/:idPostulante')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las posibles respuestas de un Postulante' })
  async getRespuestaPregunta(@Param('idPostulante') idPostulante: number) {
    const data = await this.respuestasFormularioExaminacionService.getRespuestasByIdPostulante(idPostulante);

    return { data };
  }

  @Post('saveRespuestaPregunta')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una nueva Respuesta' })
  async saveRespuestaPregunta(@Body() preguntaFormularioExaminacionesDTO: PreguntaFormularioExaminacionesDTO) {
    const data = await this.respuestasFormularioExaminacionService.create(preguntaFormularioExaminacionesDTO);
    return { data };
  }

  @Post('saveRespuestaPreguntaMedico')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una nueva Respuesta' })
  async saveRespuestaPreguntaMedico(@Body() preguntaFormularioExaminacionesDTO: PreguntaFormularioExaminacionesDTO) {
    const data = await this.respuestasFormularioExaminacionService.createMedico(preguntaFormularioExaminacionesDTO);
    return { data };
  }

  @Post('saveRespuestasPreguntas')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una nueva Respuesta' })
  async saveRespuestasPreguntas(@Body() preguntaFormularioExaminacionesDTO: PreguntaFormularioExaminacionesDTO[]) {
    const data = await this.respuestasFormularioExaminacionService.createList(preguntaFormularioExaminacionesDTO);
    return { data };
  }

  @Post('saveRespuestaPreguntaDocumento')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una nueva Respuesta Documento' })
  async saveRespuestaPreguntaDocumento(@Body() preguntaFormularioExaminacionesDTO: PreguntaFormularioExaminacionesDTO) {
    const data = await this.respuestasFormularioExaminacionService.createRespuestaDocumento(preguntaFormularioExaminacionesDTO);
    return { data };
  }

  @Delete('deleteRespuestaPreguntaDocumento/:idArchivosRespuesta')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina un Documento del formulario de examen.' })
  async deleteRespuestaPreguntaDocumento(@Param('idArchivosRespuesta') idArchivosRespuesta: number) {
    const data = await this.respuestasFormularioExaminacionService.deleteRespuestaDocumento(idArchivosRespuesta);
    return { data };
  }
}
