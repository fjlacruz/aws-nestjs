import {Entity, Column, PrimaryColumn, OneToOne} from "typeorm";

@Entity('ResultadoExaminacionCola')
export class ResultadoExaminacionColaEntity {

    @PrimaryColumn()
    idResultadoExamCola:number;

    @Column()
    idResultadoExam:number;

    @Column()
    idColaExaminacion:number;
    
}
