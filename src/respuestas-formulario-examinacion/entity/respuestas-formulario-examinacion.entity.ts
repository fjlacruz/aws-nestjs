import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { JoinColumn, ManyToOne } from 'typeorm';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('RespuestasFormularioExaminacion')
export class RespuestasFormularioExaminacionEntity {
  @PrimaryGeneratedColumn()
  idRespuestaFormExam: number;

  @Column()
  idResultadoExam: number;

  @Column()
  idPostulante: number;

  @Column()
  idFomularioPregunta: number;

  @Column()
  idPreguntaExam: number;

  @Column()
  idRespuesta: number;

  @Column()
  respuestaAbierta: string;

  @Column()
  respuestaBooleana: boolean;

  @Column()
  respuestaHora: string;

  @Column()
  respuestaFecha: Date;

  @Column()
  respuestaInt: number;

  @Column()
  idUsuarioExaminador: number;

  @Column()
  idUsuario: number;

  @Column()
  respuestaMultiple: number;

  @Column()
  respuestaMultipleFloat: number;

  @Column()
  respuestaFloat: number;

  @Column()
  respuestaArchivo: string;

  @ManyToOne(() => ResultadoExaminacionEntity, resultadoExaminacion => resultadoExaminacion.respuestasFormularioExaminacion)
  @JoinColumn({ name: 'idResultadoExam' })
  readonly resultadoExaminacion: ResultadoExaminacionEntity;
}
