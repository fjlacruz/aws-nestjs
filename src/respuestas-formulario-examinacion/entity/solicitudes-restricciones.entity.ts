import {Entity, Column, PrimaryColumn, OneToOne} from "typeorm";

@Entity('SolicitudesRestricciones')
export class SolicitudesRestriccionesEntity {

    @PrimaryColumn()
    idSolicitudesRestricciones:number;
    @Column()
    idSolicitud:number;
    @Column()
    idRestrccion:number;
    
}