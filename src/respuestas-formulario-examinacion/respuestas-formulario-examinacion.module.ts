import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResultadoExaminacionColaEntity } from 'src/cola-examinacion/entity/resultadoexaminacioncola.entity';
import { RespuestasFormularioExaminacionController } from './controller/respuestas-formulario-examinacion.controller';
import { RespuestasFormularioExaminacionEntity } from './entity/respuestas-formulario-examinacion.entity';
import { RespuestasFormularioExaminacionService } from './services/respuestas-formulario-examinacion.service';
import { DocumentosRespuestaEntity } from '../documentos-respuesta/entity/documentos-respuesta.entity';
import { ColaExaminacionEntity } from '../cola-examinacion/entity/cola-examinacion.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      RespuestasFormularioExaminacionEntity,
      ResultadoExaminacionColaEntity,
      DocumentosRespuestaEntity,
      ColaExaminacionEntity,
    ]),
  ],
  providers: [RespuestasFormularioExaminacionService],
  exports: [RespuestasFormularioExaminacionService],
  controllers: [RespuestasFormularioExaminacionController],
})
export class RespuestasFormularioExaminacionModule {}
