import { Test, TestingModule } from '@nestjs/testing';
import { RespuestasFormularioExaminacionService } from './respuestas-formulario-examinacion.service';

describe('RespuestasFormularioExaminacionService', () => {
  let service: RespuestasFormularioExaminacionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RespuestasFormularioExaminacionService],
    }).compile();

    service = module.get<RespuestasFormularioExaminacionService>(RespuestasFormularioExaminacionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
