import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResultadoExaminacionColaEntity } from 'src/cola-examinacion/entity/resultadoexaminacioncola.entity';
import { PreguntaFormularioExaminacionesEntity } from 'src/formularios-examinaciones/entity/pregunta-formulario-examinaciones.entity';
import { Repository } from 'typeorm';
import { PreguntaFormularioExaminacionesDTO } from '../DTO/PreguntaFormularioExaminaciones.dto';
import { RespuestasFormularioExaminacionEntity } from '../entity/respuestas-formulario-examinacion.entity';
import { DocumentosRespuestaEntity } from '../../documentos-respuesta/entity/documentos-respuesta.entity';
import { ColaExaminacionEntity } from '../../cola-examinacion/entity/cola-examinacion.entity';
import { TipoExaminacionesEntity } from '../../cola-examinacion/entity/tipoexaminacion.entity';
import { TramitesColaEntity } from '../../cola-examinacion/entity/tramitesCola.entity';
import { ResultadoExaminacionEntity } from '../../resultado-examinacion/entity/resultado-Examinacion.entity';
import { Solicitudes } from '../../solicitudes/entity/solicitudes.entity';

@Injectable()
export class RespuestasFormularioExaminacionService {
  constructor(
    @InjectRepository(RespuestasFormularioExaminacionEntity)
    private readonly respuestasFormularioExaminacionEntity: Repository<RespuestasFormularioExaminacionEntity>,
    @InjectRepository(ResultadoExaminacionColaEntity)
    private readonly resultadoExaminacionColaEntity: Repository<ResultadoExaminacionColaEntity>,
    @InjectRepository(DocumentosRespuestaEntity) private readonly documentosRespuestaEntity: Repository<DocumentosRespuestaEntity>,
    @InjectRepository(ColaExaminacionEntity) private readonly colaexamRespository: Repository<ColaExaminacionEntity>
  ) {}

  async getOpcionesPreguntaByIdPregunta(idPreguntaExam: number) {
    return this.respuestasFormularioExaminacionEntity
      .createQueryBuilder('RespuestasFormularioExaminacion')
      .innerJoinAndMapMany(
        'RespuestasFormularioExaminacion.idPreguntaExam',
        PreguntaFormularioExaminacionesEntity,
        'pre',
        'RespuestasFormularioExaminacion.idPreguntaExam = pre.idPreguntaExam'
      )
      .where('RespuestasFormularioExaminacion.idPreguntaExam = :id', { id: idPreguntaExam })
      .getMany();
  }

  async getRespuestasByIdPostulante(idPostulante: number) {
    return this.respuestasFormularioExaminacionEntity
      .createQueryBuilder('RespuestasFormularioExaminacion')
      .innerJoinAndMapMany(
        'RespuestasFormularioExaminacion.idPreguntaExam',
        PreguntaFormularioExaminacionesEntity,
        'pre',
        'RespuestasFormularioExaminacion.idPreguntaExam = pre.idPreguntaExam'
      )
      .where('RespuestasFormularioExaminacion.idPostulante = :id', { id: idPostulante })
      .getMany();
  }

  async getRespuestas(idPreguntaExam: number) {
    return this.respuestasFormularioExaminacionEntity
      .createQueryBuilder('RespuestasFormularioExaminacion')
      .innerJoinAndMapMany(
        'RespuestasFormularioExaminacion.idPreguntaExam',
        PreguntaFormularioExaminacionesEntity,
        'pre',
        'RespuestasFormularioExaminacion.idPreguntaExam = pre.idPreguntaExam'
      )
      .where('RespuestasFormularioExaminacion.idPreguntaExam = :id', { id: idPreguntaExam })
      .getMany();
  }

  /**
   * Metodo para guardar las respuestas en el list.
   * @param data
   * return list RespuestasFormularioExaminacionEntity[].
   */
  async createList(data: PreguntaFormularioExaminacionesDTO[]): Promise<RespuestasFormularioExaminacionEntity[]> {
    const list: RespuestasFormularioExaminacionEntity[] = [];
    for (const pregunta of data) {
      const respuesta: RespuestasFormularioExaminacionEntity = await this.create(pregunta);
      list.push(respuesta);
    }
    return list;
  }

  /**
   * Metodo para guardar una respuesta.
   * @param data
   */
  async create(data: PreguntaFormularioExaminacionesDTO): Promise<RespuestasFormularioExaminacionEntity> {
    const respuestaFind = await this.respuestasFormularioExaminacionEntity.findOne({
      where: {
        idFomularioPregunta: data.idFomularioPregunta,
        idPreguntaExam: data.idPreguntaExam,
        idResultadoExam: data.idResultadoExam,
      },
    });
    if (respuestaFind) {
      data.idRespuestaFormExam = respuestaFind.idRespuestaFormExam;
      const respuesta = await this.respuestasFormularioExaminacionEntity.save(data);
      return respuesta;
    } else {
      data.idRespuestaFormExam = null;
      const respuesta = await this.respuestasFormularioExaminacionEntity.save(data);
      return respuesta;
    }
  }

  /**
   * Metodo para guardar la respuesta de examen medico.
   * @param data PreguntaFormularioExaminacionesDTO
   */
  async createMedico(data: PreguntaFormularioExaminacionesDTO): Promise<RespuestasFormularioExaminacionEntity> {
    const respuesta = await this.create(data);
    const compartidos = await this.getColasCompartidas(data.idTramite, data.idSolicitud, data.idFomularioPregunta);
    if (compartidos.length > 0) {
      compartidos.forEach(async compartido => {
        const respuestaCompartida = data;
        respuestaCompartida.idColaExaminacion = compartido.ce_idColaExaminacion;
        respuestaCompartida.idResultadoExam = compartido.re_idResultadoExam;
        await this.create(respuestaCompartida);
      });
    }
    return respuesta;
  }

  /**
   * Metodo para obtener las colas compartidas por id tramite e id solicitud.
   * @param idTramite
   * @param idSolicitud
   */
  async getColasCompartidas(idTramite: number, idSolicitud: number, idFormularioExaminaciones) {
    const qb = this.colaexamRespository
      .createQueryBuilder('ce')
      .leftJoinAndMapMany('ce.idTipoExaminacion', TipoExaminacionesEntity, 'te', 'ce.idTipoExaminacion = te.idTipoExaminacion')
      .leftJoinAndMapMany('ce.idTramite', TramitesColaEntity, 'tram', 'ce.idTramite = tram.idTramite')
      .leftJoinAndMapMany('ce.idColaExaminacion', ResultadoExaminacionColaEntity, 'rec', 'ce.idColaExaminacion = rec.idColaExaminacion')
      .leftJoinAndMapMany('re.idResultadoExam', ResultadoExaminacionEntity, 're', 'rec.idResultadoExam = re.idResultadoExam')
      .leftJoinAndMapMany('tram.idSolicitud', Solicitudes, 'sol', 'tram.idSolicitud = sol.idSolicitud');
    qb.andWhere('te.idTipoExaminacion = 4');
    qb.andWhere('sol.idSolicitud = :idSolicitud', { idSolicitud: idSolicitud });
    qb.andWhere('tram.idTramite not in (:idTramite)', { idTramite: idTramite });
    qb.andWhere('re.idFormularioExaminaciones = :idFormularioExaminaciones', { idFormularioExaminaciones: idFormularioExaminaciones });

    const estados: any = await qb.getRawMany();
    return estados;
  }

  async createRespuestaDocumento(data: PreguntaFormularioExaminacionesDTO): Promise<any> {
    // se cambio a any por que se requiere una respuesta diferente A RespuestasFormularioExaminacionEntity
    const respuestaFind = await this.respuestasFormularioExaminacionEntity.findOne({
      where: {
        idFomularioPregunta: data.idFomularioPregunta,
        idPreguntaExam: data.idPreguntaExam,
        idResultadoExam: data.idResultadoExam,
      },
    });
    if (respuestaFind) {
      const respuesta = await this.respuestasFormularioExaminacionEntity.save(respuestaFind);
      const documentoRespuestaDto = new DocumentosRespuestaEntity();
      documentoRespuestaDto.nombreArchivo = data.nombreArchivo;
      documentoRespuestaDto.archivo = data.archivo;
      documentoRespuestaDto.idRespuestaFormExam = respuesta.idRespuestaFormExam;
      const respuestaDoc = await this.documentosRespuestaEntity.save(documentoRespuestaDto);

      const respuestaDto = {
        ...respuesta,
        nombreArchivo: respuestaDoc.nombreArchivo,
        archivo: undefined,
        idColaExaminacion: undefined,
        idArchivosRespuesta: respuestaDoc.idArchivosRespuesta,
      };
      console.log(respuestaDoc);
      return respuestaDto;
    } else {
      const respuesta = await this.respuestasFormularioExaminacionEntity.save(data);
      const respuestaFind = await this.respuestasFormularioExaminacionEntity.findOne({
        where: {
          idFomularioPregunta: respuesta.idFomularioPregunta,
          idPreguntaExam: respuesta.idPreguntaExam,
          idResultadoExam: respuesta.idResultadoExam,
        },
      });
      const documentoRespuestaDto = new DocumentosRespuestaEntity();
      documentoRespuestaDto.nombreArchivo = data.nombreArchivo;
      documentoRespuestaDto.archivo = data.archivo;
      documentoRespuestaDto.idRespuestaFormExam = respuestaFind.idRespuestaFormExam;

      await this.documentosRespuestaEntity.save(documentoRespuestaDto);
      return respuestaFind;
    }
  }

  async deleteRespuestaDocumento(idArchivosRespuesta: number) {
    await this.documentosRespuestaEntity.delete(idArchivosRespuesta);
    const entityFind = await this.documentosRespuestaEntity.findOne({
      where: {
        idArchivosRespuesta: idArchivosRespuesta,
      },
    });
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
