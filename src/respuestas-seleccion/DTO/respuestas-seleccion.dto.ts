import { ApiPropertyOptional } from "@nestjs/swagger";

export class RespuestasSeleccionDTO {


    @ApiPropertyOptional()
    nombreSeleccion:string;

    @ApiPropertyOptional()
    idPreguntaExam:number;
}
