import { ApiPropertyOptional } from "@nestjs/swagger";

export class UpdateRespuestaSeccionDTO {

    @ApiPropertyOptional()
    nombreSeleccion:string;

    @ApiPropertyOptional()
    idPreguntaExam:number;
}
