import { Test, TestingModule } from '@nestjs/testing';
import { RespuestasSeleccionController } from './respuestas-seleccion.controller';

describe('RespuestasSeleccionController', () => {
  let controller: RespuestasSeleccionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RespuestasSeleccionController],
    }).compile();

    controller = module.get<RespuestasSeleccionController>(RespuestasSeleccionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
