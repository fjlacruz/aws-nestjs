import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { RespuestasSeleccionDTO } from '../DTO/respuestas-seleccion.dto';
import { UpdateRespuestaSeccionDTO } from '../DTO/update-respuesta-seccion.dto';
import { RespuestasSeleccionService } from '../services/respuestas-seleccion.service';

@ApiTags('Respuestas-seleccion')
@Controller('respuestas-seleccion')
export class RespuestasSeleccionController {

    constructor(private readonly respuestasSeleccionService: RespuestasSeleccionService) { }

    @Get('getRespuestasSelecciones')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las Respuestas Seleccion' })
    async getRespuestasSelecciones() {
        const data = await this.respuestasSeleccionService.getRespuestasSelecciones();
        return { data };
    }

    @Get('getRespuestasSeleccionById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Respuesta Seleccion por Id' })
    async getRespuestasSeleccionById(@Param('id') id: number) {
        const data = await this.respuestasSeleccionService.getRespuestasSeleccion(id);
        return { data };
    }

    @Get('getRespuestasSeleccionByIdPreguntaExam/:idPreguntaExam')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Respuesta Sel.por idPreguntaExam' })
    async getRespuestasSeleccionByIdPreguntaExam(@Param('idPreguntaExam') idPreguntaExam: number) {
        const data = await this.respuestasSeleccionService.getRespuestasSeleccionPregExam(idPreguntaExam);
        return { data };
    }

    @Patch('updateRespuestasSeleccion/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de una Respuesta Seleccion' })
    async updateRespuestasSeleccion(@Param('id') id: number, @Body() data: UpdateRespuestaSeccionDTO) {
        return await this.respuestasSeleccionService.update(id, data);
    }

    @Post('crearRespuestasSeleccion')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una nueva Respuesta Seleccion' })
    async addComuna(
        @Body() createComunaDTO: RespuestasSeleccionDTO) {
        const data = await this.respuestasSeleccionService.create(createComunaDTO);
        return {data};
    }

}

