import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('respuestasSeleccion')
export class RespuestasSeleccionEntity {

    @PrimaryColumn()
    idRespuesta:number;

    @Column()
    nombreSeleccion:string;

    @Column()
    idPreguntaExam:number;

}

export class RespuestasSeleccionTipoExamEntity {

    @PrimaryColumn()
    idPreguntaExam:number;

    @Column()
    nombreSeleccion:string;

    @Column()
    idRespuesta:number;

}
