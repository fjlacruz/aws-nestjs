import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RespuestasSeleccionController } from './controller/respuestas-seleccion.controller';
import { RespuestasSeleccionEntity } from './entity/respuestas-seleccion.entity';
import { RespuestasSeleccionService } from './services/respuestas-seleccion.service';

@Module({
  imports: [TypeOrmModule.forFeature([RespuestasSeleccionEntity])],
  providers: [RespuestasSeleccionService],
  exports: [RespuestasSeleccionService],
  controllers: [RespuestasSeleccionController]
})
export class RespuestasSeleccionModule {}
