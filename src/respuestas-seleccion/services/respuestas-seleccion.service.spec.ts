import { Test, TestingModule } from '@nestjs/testing';
import { RespuestasSeleccionService } from './respuestas-seleccion.service';

describe('RespuestasSeleccionService', () => {
  let service: RespuestasSeleccionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RespuestasSeleccionService],
    }).compile();

    service = module.get<RespuestasSeleccionService>(RespuestasSeleccionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
