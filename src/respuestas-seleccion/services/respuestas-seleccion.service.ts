import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { RespuestasSeleccionDTO } from '../DTO/respuestas-seleccion.dto';
import { UpdateRespuestaSeccionDTO } from '../DTO/update-respuesta-seccion.dto';
import { RespuestasSeleccionEntity } from '../entity/respuestas-seleccion.entity';

@Injectable()
export class RespuestasSeleccionService {
    constructor(@InjectRepository(RespuestasSeleccionEntity) private readonly respuestasSeleccionEntity: Repository<RespuestasSeleccionEntity>) { }

    async getRespuestasSelecciones() {
        return await this.respuestasSeleccionEntity.find();
    }


    async getRespuestasSeleccion(id:number){
        const region= await this.respuestasSeleccionEntity.findOne(id);
        if (!region)throw new NotFoundException("Seleccion dont exist");
        
        return region;
    }

    async getRespuestasSeleccionPregExam(idPreguntaExam:number) {

        return this.respuestasSeleccionEntity.createQueryBuilder('respuestasSeleccion')          
        .where("respuestasSeleccion.idPreguntaExam = :idPreguntaExam", { idPreguntaExam: idPreguntaExam})
        .getMany();
        
    }

    async update(idRespuesta: number, data: Partial<UpdateRespuestaSeccionDTO>) {
        await this.respuestasSeleccionEntity.update({ idRespuesta }, data);
        return await this.respuestasSeleccionEntity.findOne({ idRespuesta });
      }
      
    async create(data: RespuestasSeleccionDTO): Promise<RespuestasSeleccionEntity> {
        return await this.respuestasSeleccionEntity.save(data);
    }
}
