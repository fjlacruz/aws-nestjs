import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { RestriccionesService } from '../services/restricciones.service';

@ApiTags('restricciones')
@Controller('restricciones')
export class RestriccionesController {

    constructor(private readonly restriccionesService: RestriccionesService) { }

    @Get('getRestricciones')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las Restricciones' })
    async getRestricciones() {
        const data = await this.restriccionesService.getRestriccones();
        return { data };
    }

    
}

