import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RestriccionesEntity } from 'src/clases-licencia/entity/restricciones.entity';
import { RestriccionesController } from './controller/restricciones.controller';
import { RestriccionesService } from './services/restricciones.service';
@Module({
  imports: [TypeOrmModule.forFeature([RestriccionesEntity])],
  providers: [RestriccionesService],
  exports: [RestriccionesService],
  controllers: [RestriccionesController]
})
export class RestriccionesModule {}
