import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { orderBy } from 'lodash';
import { RestriccionesEntity } from 'src/clases-licencia/entity/restricciones.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RestriccionesService {
    constructor(@InjectRepository(RestriccionesEntity) private readonly restriccionesRepository: Repository<RestriccionesEntity>) { }

    async getRestriccones() {
        return await this.restriccionesRepository.find({ order: { codrc: 'ASC' } });

    }
}
