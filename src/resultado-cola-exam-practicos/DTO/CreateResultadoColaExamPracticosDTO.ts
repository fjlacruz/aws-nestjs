import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateResultadoColaExamPracticosDTO {

    @ApiPropertyOptional()
    Codigo:String;

    @ApiPropertyOptional()
    Error:String;

    @ApiPropertyOptional()
	Leve:number;

    @ApiPropertyOptional()
	Grave:number;

    @ApiPropertyOptional()
	Reprobatorio:number;

    @ApiPropertyOptional()
    created_at:Date;

    @ApiPropertyOptional()
    update_at:Date;

    @ApiPropertyOptional()
    idExamPractico:number;

    @ApiPropertyOptional()
	idTipoResultadoColaExamPracticos:number;
}
