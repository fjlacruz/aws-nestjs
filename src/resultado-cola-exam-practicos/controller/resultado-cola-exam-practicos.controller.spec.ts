import { Test, TestingModule } from '@nestjs/testing';
import { ResultadoColaExamPracticosController } from './resultado-cola-exam-practicos.controller';

describe('ResultadoColaExamPracticosController', () => {
  let controller: ResultadoColaExamPracticosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ResultadoColaExamPracticosController],
    }).compile();

    controller = module.get<ResultadoColaExamPracticosController>(ResultadoColaExamPracticosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
