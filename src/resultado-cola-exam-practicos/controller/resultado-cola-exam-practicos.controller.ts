import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CreateResultadoColaExamPracticosDTO } from '../DTO/CreateResultadoColaExamPracticosDTO';
import { ResultadoColaExamPracticosService } from '../services/resultado-cola-exam-practicos.service';

@ApiTags('Resultado-cola-exam-practicos')
@Controller('resultado-cola-exam-practicos')
export class ResultadoColaExamPracticosController {

    constructor(private readonly resultadoColaExamPracticosService: ResultadoColaExamPracticosService) { }

    @Get('resultadoColaExamPracticos')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Resultado Cola Exam Practicos' })
    async getResultadoColaExamPracticos() {
        const data = await this.resultadoColaExamPracticosService.getTipoResultadoColaExamPracticos();
        return { data };
    }

    @Get('resultadoColaExamPracticoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Resultado Cola Exam Practicos por Id' })
    async getResultadoColaExamPractico(@Param('id') id: number) {
        const data = await this.resultadoColaExamPracticosService.getTipoResultadoColaExamPractico(id);
        return { data };
    }

    @Post('resultadoColaExamPracticosUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de un Resultado Cola Exam Practicos' })
    async resultadoColaExamPracticosUpdate(@Param('id') id: number, @Body() data: CreateResultadoColaExamPracticosDTO) {
        return await this.resultadoColaExamPracticosService.update(id, data);
    }

    @Post('crearResultadoColaExamPractico')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Resultado Cola Exam Practicos' })
    async addResultadoColaExamPractico(
        @Body() createResultadoColaExamPracticosDTO: CreateResultadoColaExamPracticosDTO) {
        const data = await this.resultadoColaExamPracticosService.create(createResultadoColaExamPracticosDTO);
        return {data};
    }

    @Get('deleteResultadoColaExamPracticoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Resultado Cola Exam Practicos por Id' })
    async deleteResultadoColaExamPracticoById(@Param('id') id: number) {
        const data = await this.resultadoColaExamPracticosService.delete(id);
        return data;
    }
}
