import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('ResultadoColaExamPracticos')
export class ResultadoColaExamPracticosEntity {

    @PrimaryColumn()
    idResultadoColaExamPracticos:number;

    @Column()
    Codigo:String;

    @Column()
    Error:String;

    @Column()
	Leve:number;

    @Column()
	Grave:number;

    @Column()
	Reprobatorio:number;

    @Column()
    created_at:Date;

    @Column()
    update_at:Date;

    @Column()
    idExamPractico:number;

    @Column()
	idTipoResultadoColaExamPracticos:number;

}
