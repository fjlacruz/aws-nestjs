import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResultadoColaExamPracticosController } from './controller/resultado-cola-exam-practicos.controller';
import { ResultadoColaExamPracticosEntity } from './entity/TipoResultadoColaExamPracticos.entity';
import { ResultadoColaExamPracticosService } from './services/resultado-cola-exam-practicos.service';


@Module({
  imports: [TypeOrmModule.forFeature([ResultadoColaExamPracticosEntity])],
  providers: [ResultadoColaExamPracticosService],
  exports: [ResultadoColaExamPracticosService],
  controllers: [ResultadoColaExamPracticosController]
})
export class ResultadoColaExamPracticosModule {}
