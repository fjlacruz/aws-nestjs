import { Test, TestingModule } from '@nestjs/testing';
import { ResultadoColaExamPracticosService } from './resultado-cola-exam-practicos.service';

describe('ResultadoColaExamPracticosService', () => {
  let service: ResultadoColaExamPracticosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ResultadoColaExamPracticosService],
    }).compile();

    service = module.get<ResultadoColaExamPracticosService>(ResultadoColaExamPracticosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
