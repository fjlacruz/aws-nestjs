import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TipoResultadoColaExamPracticosEntity } from 'src/tipo-resultado-cola-exam-practicos/entity/tipoResultadoColaExamPracticos.entity';
import { getConnection, Repository } from 'typeorm';
import { CreateResultadoColaExamPracticosDTO } from '../DTO/CreateResultadoColaExamPracticosDTO';
import { ResultadoColaExamPracticosEntity } from '../entity/TipoResultadoColaExamPracticos.entity';

@Injectable()
export class ResultadoColaExamPracticosService {
  constructor(
    @InjectRepository(ResultadoColaExamPracticosEntity)
    private readonly resultadoColaExamPracticosEntity: Repository<ResultadoColaExamPracticosEntity>,
  ) {}

  async getTipoResultadoColaExamPracticos() {
    return (
      this.resultadoColaExamPracticosEntity
        .createQueryBuilder('ResultadoColaExamPracticos')
        // .innerJoinAndMapMany('ResultadoColaExamPracticos.examPractico', ColaExamPracticosEntity, 'cola', 'ResultadoColaExamPracticos.idExamPractico = cola.idExamPractico',)
        .innerJoinAndMapMany(
          'ResultadoColaExamPracticos.tipoResultado',
          TipoResultadoColaExamPracticosEntity,
          'tipo',
          'ResultadoColaExamPracticos.idTipoResultadoColaExamPracticos = tipo.idTipoResultadoColaExamPracticos',
        )
        .getMany()
    );
  }

  async getTipoResultadoColaExamPractico(id: number) {
    const data = await this.resultadoColaExamPracticosEntity.findOne(id);
    if (!data)
      throw new NotFoundException(
        'Tipo Resultado Cola Exam Practicos dont exist',
      );

    return data;
  }

  async update(
    idTipoResultadoColaExamPracticos: number,
    data: Partial<CreateResultadoColaExamPracticosDTO>,
  ) {
    await this.resultadoColaExamPracticosEntity.update(
      { idTipoResultadoColaExamPracticos },
      data,
    );
    return await this.resultadoColaExamPracticosEntity.findOne({
      idTipoResultadoColaExamPracticos,
    });
  }

  async create(
    data: CreateResultadoColaExamPracticosDTO,
  ): Promise<ResultadoColaExamPracticosEntity> {
    return await this.resultadoColaExamPracticosEntity.save(data);
  }

  async delete(id: number) {
    try {
      await getConnection()
        .createQueryBuilder()
        .delete()
        .from(ResultadoColaExamPracticosEntity)
        .where('idResultadoColaExamPracticos = :id', { id: id })
        .execute();
      return { code: 200, message: 'success' };
    } catch (error) {
      return { code: 400, error: error };
    }
  }
}
