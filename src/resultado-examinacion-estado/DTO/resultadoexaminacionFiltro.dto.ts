import { ApiPropertyOptional } from "@nestjs/swagger";

export class ResultadoExaminacionFiltroDTO {

    @ApiPropertyOptional()
    RUN:number;

    @ApiPropertyOptional()
    idPostulante:number;

    @ApiPropertyOptional()
    idSolicitud:number;

}
