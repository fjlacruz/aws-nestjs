import { Test, TestingModule } from '@nestjs/testing';
import { ResultadoExaminacionEstadoController } from './resultado-examinacion-estado.controller';

describe('ResultadoExaminacionEstadoController', () => {
  let controller: ResultadoExaminacionEstadoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ResultadoExaminacionEstadoController],
    }).compile();

    controller = module.get<ResultadoExaminacionEstadoController>(ResultadoExaminacionEstadoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
