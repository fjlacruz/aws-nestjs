import { Controller, Get, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ResultadoExaminacionFiltroDTO } from '../DTO/resultadoexaminacionFiltro.dto';
import { ResultadoExaminEstadoService } from '../services/resultado-examinacion-estado.service';

@ApiTags('Resultado-examinacion-estado')
@Controller('resultado-examinacion-estado')
export class ResultadoExaminacionEstadoController {

    constructor(private readonly resultadoExaminEstadoService: ResultadoExaminEstadoService) { }

    @Get('getEstadoResultadoExaminacion')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve el Estado de las Examinaciones' })

    async getFiltrado(@Query() query: ResultadoExaminacionFiltroDTO) {

        const data = await this.resultadoExaminEstadoService
        .getFiltrado(query.RUN, query.idPostulante, query.idSolicitud);
        return { data };
    }

}
