import { ApiProperty } from "@nestjs/swagger";
import { PostulantesEntity } from "src/tramites/entity/postulantes.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToOne} from "typeorm";


@Entity('ColaExamPreEvalIdoneidadMoral')
export class ColaExamIdoneidadMoralEntity {

    @PrimaryColumn()
    idExamIMPreEval:number;

    @Column()
    idTramite:number;

    @Column()
    idUsuario:number;

    @Column()
    created_at:Date;

    @Column()
    updated_at:Date;

    @Column()
    aprobado:boolean;

    @Column()
    alertado:boolean;

    @Column()
    observacion:string;

}