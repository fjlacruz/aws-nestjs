import { ApiProperty } from '@nestjs/swagger';
// import { ColaExamMedicoEntity } from 'src/cola-exam-medico/entity/cola-exam-medico.entity';
// import { ColaExamPracticosEntity } from 'src/cola-exam-practicos/entity/cola-exam-practicos.entity';
// import { ColaExamTeoricos } from 'src/cola-exam-teoricos/entity/cola-exam-teoricos.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  OneToOne,
} from 'typeorm';

@Entity('Solicitudes')
export class ResultadoExaminacionEstadoEntity {
  @PrimaryColumn()
  idSolicitud: number;

  @Column()
  idPostulante: number;

  @Column()
  idTipoSolicitud: number;

  @ApiProperty()
  @OneToOne(() => PostulantesEntity, (postulantes) => postulantes.solicitudes)
  readonly postulantes: PostulantesEntity;

  @ApiProperty()
  @OneToOne(() => TramitesEntity, (tramites) => tramites.solicitudes)
  readonly tramites: TramitesEntity;

  // @ApiProperty()
  // @OneToOne(
  //   () => ColaExamPracticosEntity,
  //   (colaexamnepracticos) => colaexamnepracticos.tramite,
  // )
  // readonly colaexamnepracticos: ColaExamPracticosEntity;
  //
  // @ApiProperty()
  // @OneToOne(
  //   () => ColaExamMedicoEntity,
  //   (colaexammedico) => colaexammedico.tramite,
  // )
  // readonly colaexammedico: ColaExamMedicoEntity;
  //
  // @ApiProperty()
  // @OneToOne(
  //   () => ColaExamTeoricos,
  //   (colaexamteorico) => colaexamteorico.tramite,
  // )
  // readonly colaexamteorico: ColaExamTeoricos;
}
