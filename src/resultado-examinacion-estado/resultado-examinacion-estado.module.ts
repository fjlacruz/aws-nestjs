import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResultadoExaminacionEstadoController } from './controller/resultado-examinacion-estado.controller';
import { ResultadoExaminacionEstadoEntity } from './entity/resultadoexaminacionEstado.entity';
import { ResultadoExaminEstadoService } from './services/resultado-examinacion-estado.service';

@Module({
  providers: [ResultadoExaminEstadoService],
  controllers: [ResultadoExaminacionEstadoController],
  imports: [TypeOrmModule.forFeature([ResultadoExaminacionEstadoEntity])],
  exports: [ResultadoExaminEstadoService]
})
export class ResultadoExaminacionEstadoModule {}
