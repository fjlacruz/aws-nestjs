import { Test, TestingModule } from '@nestjs/testing';
import { ResultadoExaminacionEstadoService } from './resultado-examinacion-estado.service';

describe('ResultadoExaminacionEstadoService', () => {
  let service: ResultadoExaminacionEstadoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ResultadoExaminacionEstadoService],
    }).compile();

    service = module.get<ResultadoExaminacionEstadoService>(ResultadoExaminacionEstadoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
