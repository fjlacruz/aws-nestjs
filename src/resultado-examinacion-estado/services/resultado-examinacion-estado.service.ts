import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
// import { ColaExamIdoneidadMoral_deprecated } from 'src/cola-exam-idoneidad-moral/entity/cola-exam-idoneidad.entity';
// import { ColaExamMedicoEntity } from 'src/cola-exam-medico/entity/cola-exam-medico.entity';
// import { ColaExamPracticosEntity } from 'src/cola-exam-practicos/entity/cola-exam-practicos.entity';
// import { ColaExamPreEvalIdoneidadMoral } from 'src/cola-exam-pre-eval-idoneidad-moral/entity/cola-exam-pre-eval-ioneidad-moral.entity';
// import { ColaExamTeoricos } from 'src/cola-exam-teoricos/entity/cola-exam-teoricos.entity';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { getConnection, Repository } from 'typeorm';
import { ResultadoExaminacionEstadoEntity } from '../entity/resultadoexaminacionEstado.entity';

var vResestado: number;

@Injectable()
export class ResultadoExaminEstadoService {
  constructor(
    @InjectRepository(ResultadoExaminacionEstadoEntity)
    private readonly resulexamestadoRespository: Repository<ResultadoExaminacionEstadoEntity>,
  ) {}

  async getFiltrado(RUN?: number, idPostulante?: number, idSolicitud?: number) {
    vResestado = idSolicitud;

    return await getConnection()
      .createQueryBuilder(Solicitudes, 'sol')
      .select('sol.idSolicitud', 'idSolicitud')
      .addSelect('pos.RUN', 'RUN')
      .addSelect('pos.idPostulante', 'idPostulante')
      .addSelect('tra.idTramite', 'idTramite')
      .addSelect('cex.idColaExaminacion', 'idColaExaminacion')
      .addSelect('cex.idTipoExaminacion', 'idTipoExaminacion')
      .addSelect('tex.nombreExaminacion', 'nombreExaminacion')
      .addSelect('cex.Aprobado', 'Aprobado')

      .innerJoin(Postulante, 'pos', 'pos.idPostulante = sol.idPostulante')
      .innerJoin(TramitesEntity, 'tra', 'tra.idSolicitud = sol.idSolicitud')
      .innerJoin(ColaExaminacionEntity, 'cex', 'cex.idTramite = tra.idTramite')
      .innerJoin(
        TipoExaminacionesEntity,
        'tex',
        'tex.idTipoExaminacion = cex.idTipoExaminacion',
      )

      .orWhere('sol.idSolicitud = :idSolicitud', { idSolicitud })
      .orWhere('pos.idPostulante = :idPostulante', { idPostulante })
      .orWhere('pos.RUN = :RUN', { RUN })

      //        .where("((sol.idSolicitud = :idSolicitud AND :idSolicitud !=null) OR :idSolicitud =null  )", {idSolicitud} )
      //    .andWhere("pos.RUN = :RUN", {RUN})

      .getRawMany();
  }
}
