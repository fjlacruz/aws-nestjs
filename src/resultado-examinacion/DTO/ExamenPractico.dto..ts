import { ApiPropertyOptional } from '@nestjs/swagger';
import { CreateResultadoexaminacionDto } from './createResultadoexaminacion.dto.';
import { PreguntaFormularioExaminacionesDTO } from '../../respuestas-formulario-examinacion/DTO/PreguntaFormularioExaminaciones.dto';

export class ExamenPracticoDto {
  @ApiPropertyOptional()
  resultado: CreateResultadoexaminacionDto;
  @ApiPropertyOptional()
  respuestas: PreguntaFormularioExaminacionesDTO[];
}
