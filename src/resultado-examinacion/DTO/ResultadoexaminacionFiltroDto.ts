import { ApiPropertyOptional } from '@nestjs/swagger';

export class ResultadoexaminacionFiltroDto {
  @ApiPropertyOptional()
  idUsuario: number;

  @ApiPropertyOptional()
  idExaminacion: number;

  @ApiPropertyOptional()
  historico: boolean;

  @ApiPropertyOptional()
  idTipoExaminacion: number;
}
