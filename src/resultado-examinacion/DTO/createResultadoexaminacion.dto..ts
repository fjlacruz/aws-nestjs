import { ApiPropertyOptional } from '@nestjs/swagger';

export class CreateResultadoexaminacionDto {
  @ApiPropertyOptional()
  idResultadoExam?: number;

  @ApiPropertyOptional()
  aprobado?: boolean;

  @ApiPropertyOptional()
  alaEsperaAntecedentes?: boolean;

  @ApiPropertyOptional()
  UsuarioAprobador?: number;

  @ApiPropertyOptional()
  fechaAprobacion?: Date;

  @ApiPropertyOptional()
  observaciones?: string;

  @ApiPropertyOptional()
  idFormularioExaminaciones?: number;

  @ApiPropertyOptional()
  idSolicitud?: number;
}
