import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateResultadoExaminacionDTO {

    @ApiPropertyOptional()
    idResultadoExam:number;

    @ApiPropertyOptional()
    aprobado:boolean;

    @ApiPropertyOptional()
    fechaAprobacion:Date;

    @ApiPropertyOptional()
    UsuarioAprobador:number;

    @ApiPropertyOptional()
    observaciones:number;

    @ApiPropertyOptional()
    idFormularioExaminaciones:number;

}
