import { Test, TestingModule } from '@nestjs/testing';
import { ResultadoExaminacionController } from './resultado-examinacion.controller';

describe('ResultadoExaminacionController', () => {
  let controller: ResultadoExaminacionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ResultadoExaminacionController],
    }).compile();

    controller = module.get<ResultadoExaminacionController>(ResultadoExaminacionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
