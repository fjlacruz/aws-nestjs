import { Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Patch, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateResultadoexaminacionDto } from '../DTO/createResultadoexaminacion.dto.';
import { ResultadoExaminacionService } from '../services/resultado-examinacion.service';
import { CreateColaExaminacionDto } from '../../cola-examinacion/DTO/filtercolaexaminacion.dto';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { ExamenPracticoDto } from '../DTO/ExamenPractico.dto.';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Resultado } from 'src/utils/resultado';

@ApiTags('Resultado-examinacion')
@Controller('resultado-examinacion')
export class ResultadoExaminacionController {
  constructor(private readonly authService: AuthService, private readonly resultExaminService: ResultadoExaminacionService) {}

  @Get('resultadoexaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todas los resultados' })
  async getResultExamin() {
    const data = await this.resultExaminService.getResultExamin();
    return { data };
  }

  @Get('resultadoExaminacionById/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve un Resultado Examinacion' })
  async resultadoExaminacionById(@Param('id') id: number) {
    const data = await this.resultExaminService.getResultExaminById(id);
    return { data };
  }

  @Patch('resultadoExaminacionUpdate/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza datos de un resultado' })
  async resultadoexaminacionUpdate(@Param('id') id: number, @Body() data: Partial<CreateResultadoexaminacionDto>) {
    return await this.resultExaminService.update(id, data);
  }

  @Post('crearResultadoExaminacion/:idColaeximanacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Resultado' })
  async addResultExamin(@Param('idColaeximanacion') idColaeximanacion: number, @Body() createResulExamDto: CreateResultadoexaminacionDto) {
    const data = await this.resultExaminService.create(idColaeximanacion, createResulExamDto);
    return { data };
  }

  @Post('crearResultadoExaminacion/clasef/:idTramite')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Resultado' })
  async updateClaseF(@Param('idTramite') idTramite: number) {
    await this.resultExaminService.saveClaseF(idTramite);
    return 'ok';
  }

  @Post('saveExamenPractico/:idColaeximanacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Resultado' })
  async saveExamenPractivo(@Param('idColaeximanacion') idColaeximanacion: number, @Body() createResulExamDto: ExamenPracticoDto) {
    const data = await this.resultExaminService.saveExamenPractico(idColaeximanacion, createResulExamDto);
    return { data };
  }

  @Get('CalucularAprobacionColaExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve el resultado de todos las examinaciones asociadas' })
  async CalucularAprobacionColaExaminacion() {
    const data = await this.resultExaminService.CalucularAprobacionColaExaminacion();
    return { data };
  }

  @Get('getListaPendientesAtencion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getListaPendientesAtencion(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('idTipoExaminacion') idTipoExaminacion: number,
    @Query('run') run: string,
    @Query('nombre') nombre: string,
    @Query('numeroSolicitud') numeroSolicitud: string,
    @Query('numeroTramite') numeroTramite: string,
    @Query('claseLicencia') claseLicencia: string,
    @Query('fechacreacion') fechacreacion: string,
    @Query('resultadoExamen') resultadoExamen: string,
    @Query('oportunidad') oportunidad: string,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {

    let validacionPermisos : Resultado = await this.authService.checkPermission(req, PermisosNombres.VisualizarListaPostulantesExamenMedico);
    const permisos: RolesUsuarios[] = validacionPermisos.Respuesta as RolesUsuarios[];

    const data = await this.resultExaminService.getListaPendientesAtencion(
      req,
      idTipoExaminacion,
      run,
      nombre,
      numeroSolicitud,
      numeroTramite,
      claseLicencia,
      fechacreacion,
      resultadoExamen,
      oportunidad,
      {
        page,
        limit,
        route: '/resultado-examinacion/getListaPendientesAtencion',
      },
      orden.toString(),
      ordenarPor.toString()
    );
    console.log(data);
    for (const item of data.items) {
      const formu = await this.resultExaminService.getHistoricoExamenesDetalle(
        item.idColaExaminacion,
        item.idTramite[0].idTipoTramite[0].idTipoTramite,
        item.idTipoExaminacion[0].idTipoExaminacion,
        item.idTramite[0].idTramiteClaseLicencia[0].idClaseLicencia[0].idClaseLicencia
      );
      item['formulario'] = formu;
    }
    return { data };
  }

  @Get('getListaPendientesAtencionIdoneidadMoralEstado')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getListaPendientesAtencionIdoneidadMoralEstado(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('idTipoExaminacion') idTipoExaminacion: number,
    @Query('run') run: string,
    @Query('nombre') nombre: string,
    @Query('claseLicencia') claseLicencia: string,
    @Query('fechacreacion') fechacreacion: string,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const data = await this.resultExaminService.getListaPendientesAtencionIdoneidadMoral(
      idTipoExaminacion,
      run,
      nombre,
      claseLicencia,
      fechacreacion,
      {
        page,
        limit,
        route: '/resultado-examinacion/getListaPendientesAtencionIdoneidadMoralEstado',
      },
      orden.toString(),
      ordenarPor.toString()
    );
    return { data };
  }

  @Get('getHistorialExamanes')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getListaHistoricoExamenes(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: CreateColaExaminacionDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string,
    @Req() req: any
  ) {

    //const idMunicipalidad = this.authService.oficina().idOficina;
    const data = await this.resultExaminService.getHistoricoExamenes(
      req,
      query.idTipoExaminacion,
      query.RUN,
      query.Nombres,
      query.idColaExaminacion,
      query.numeroSolicitud,
      query.numeroTramite,
      query.Nomclaselic,
      query.fechaIngresoResultado,
      query.idMunicipalidad,
      query.resultadoExamen,
      query.oportunidad,
      query.idResultadoExam,
      query.nomExaminadorPractico,
      query.fechaRendicionExamen,
      {
        page,
        limit,
        route: '/resultado-examinacion/getHistorialExamanes',
      },
      orden.toString(),
      ordenarPor.toString()
    );

    for (const item of data.items) {
      const formu = await this.resultExaminService.getHistoricoExamenesDetalle(
        item.idColaExaminacion,
        item.idTramite[0].idTipoTramite[0].idTipoTramite,
        item.idTipoExaminacion[0].idTipoExaminacion,
        item.idTramite[0].idTramiteClaseLicencia[0].idClaseLicencia[0].idClaseLicencia
      );
      item['formulario'] = formu;
    }
    return { data };
  }

  @Get('getHistoricoExamenesIdoneidadMoralPreEvaluacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getHistoricoExamenesIdoneidadMoralPreEvaluacion(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() query: CreateColaExaminacionDto,
    @Query('orden') orden: string,
    @Query('ordenarPor') ordenarPor: string
  ) {
    const idMunicipalidad = this.authService.oficina().idOficina;
    console.log("--------------------------------------------------------------") 
    console.log(query)
    console.log("--------------------------------------------------------------")
    const data = await this.resultExaminService.getHistoricoExamenesIdoneidadMoralPreEvaluacion(
      {
        page,
        limit,
        route: '/resultado-examinacion/getHistoricoExamenesIdoneidadMoralPreEvaluacion',
      },
      query.idTipoExaminacion,
      query.RUN,
      query.Nombres,
      query.Nomclaselic,
      query.Nomet,
      query.fechaIngresoResultado,
      query.fechaRendicionExamen,
      query.nomExaminadorPractico,
      null,
      query.resultadoExamen,
      orden,
      ordenarPor
    );
    return { data };
  }

  @Get('getHistorialExamanesDetalle')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve datos filtrado de cola examinacion' })
  async getListaHistoricoExamenesDetalle(
    @Query('idColaExaminacion') idColaExaminacion: number,
    @Query('idTipoTramite') idTipoTramite: number,
    @Query('idTipoExaminacion') idTipoExaminacion: number,
    @Query('idClaseLicencia') idClaseLicencia: number
  ) {
    return await this.resultExaminService.getHistoricoExamenesDetalle(idColaExaminacion, idTipoTramite, idTipoExaminacion, idClaseLicencia);
  }

  @Post('crearResExaminacionPreevMoralBySolicitudId')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Resultado' })
  async crearResExaminacionPreevMoralBySolicitudId(@Body() createResulExamDto: CreateResultadoexaminacionDto, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.IngresoyEvaluacionIdoneidadMoralPreEvaluacionAlertarNoAlertar]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      // const data = await this.folioService.anularFolio(anularFolios, validarPermisos.Respuesta);
      // return { data };

      const data = await this.resultExaminService.crearResExaminacionPreevMoralBySolicitudId(createResulExamDto);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('crearResExaminacionIdonMoralBySolicitudId')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un nuevo Resultado' })
  async crearResExaminacionIdonMoralBySolicitudId(@Body() createResulExamDto: CreateResultadoexaminacionDto, @Req() req: any) {

      const data = await this.resultExaminService.crearResExaminacionIdonMoralBySolicitudId(req, createResulExamDto);

      return { data };

  }
}
