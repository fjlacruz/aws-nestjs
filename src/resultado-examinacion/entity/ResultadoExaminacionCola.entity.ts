import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { ResultadoExaminacionEntity } from './resultado-Examinacion.entity';

@Entity('ResultadoExaminacionCola')
export class ResultadoExaminacionColaEntity {
  @PrimaryGeneratedColumn()
  idResultadoExamCola: number;

  @Column()
  idResultadoExam: number;

  @Column()
  idColaExaminacion: number;

  @ManyToOne(() => ColaExaminacionEntity, colaExaminacion => colaExaminacion.resultadoExaminacionCola)
  @JoinColumn({ name: 'idColaExaminacion' })
  readonly colaExaminacion: ColaExaminacionEntity;

  @ManyToOne(() => ResultadoExaminacionEntity, resultadoExaminacion => resultadoExaminacion.resultadoExaminacionCola)
  @JoinColumn({ name: 'idResultadoExam' })
  readonly resultadoExaminacion: ResultadoExaminacionEntity;
}
