
import { ApiProperty } from "@nestjs/swagger";
import { OrdenTramiteExaminacionEntity } from "src/tipos-tramites/entity/ordenTramiteExaminacion.entity";
import { ColaExaminacion } from "src/tramites/entity/cola-examinacion.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, ManyToOne, OneToMany, OneToOne} from "typeorm";

@Entity('TipoExaminaciones')
export class TipoExaminacionesEntity {

    @PrimaryColumn()
    idTipoExaminacion:number;

    @Column()
    nombreExaminacion:string;

    @Column()
    descripcionExaminacion:string;

    @ApiProperty()
    @OneToMany(()=> OrdenTramiteExaminacionEntity, ordenTramiteExaminacion => ordenTramiteExaminacion.tipoExaminaciones )
    readonly ordenTramiteExaminacion: OrdenTramiteExaminacionEntity[];

    @ApiProperty()
    @OneToMany(()=> ColaExaminacion, colaExaminacion => colaExaminacion.tipoExaminaciones )
    readonly colaExaminacion: ColaExaminacion[];
}
