import { ApiProperty } from '@nestjs/swagger';
import { ResultadoExaminacionColaEntity } from 'src/cola-examinacion/entity/resultadoexaminacioncola.entity';
import { FormulariosExaminacionesEntity } from 'src/formularios-examinaciones/entity/formularios-examinaciones.entity';
import { RespuestasFormularioExaminacionEntity } from 'src/respuestas-formulario-examinacion/entity/respuestas-formulario-examinacion.entity';
import { User } from 'src/users/entity/user.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('ResultadoExaminacion')
export class ResultadoExaminacionEntity {
  @PrimaryGeneratedColumn()
  idResultadoExam: number;

  @Column()
  aprobado: boolean;

  @Column()
  UsuarioAprobador: number;

  @Column()
  fechaAprobacion: Date;

  @Column()
  observaciones: string;

  @Column()
  idFormularioExaminaciones: number;

  @Column()
  alaEsperaAntecedentes: number;

  @ApiProperty()
  @OneToMany(() => ResultadoExaminacionColaEntity, resultadoexaminacioncola => resultadoexaminacioncola.resultadoExaminacion)
  readonly resultadoExaminacionCola: ResultadoExaminacionColaEntity[];

  @ManyToOne(() => User, usuarioAprob => usuarioAprob.resultadoExaminacion)
  @JoinColumn({ name: 'UsuarioAprobador' })
  readonly usuarioAprob: User;

  @ManyToOne(() => FormulariosExaminacionesEntity, formularioExaminacion => formularioExaminacion.resultadoExaminacionCola)
  @JoinColumn({ name: 'idFormularioExaminaciones' })
  readonly formularioExaminacion: FormulariosExaminacionesEntity;

  @ApiProperty()
  @OneToMany(
    () => RespuestasFormularioExaminacionEntity,
    respuestasFormularioExaminacion => respuestasFormularioExaminacion.resultadoExaminacion
  )
  readonly respuestasFormularioExaminacion: RespuestasFormularioExaminacionEntity[];
}
