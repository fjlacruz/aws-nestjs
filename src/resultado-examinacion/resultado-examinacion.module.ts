import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/respuestas-formulario-examinacion/entity/ResultadoExaminacionCola.entity';
import { ResultadoExaminacionController } from './controller/resultado-examinacion.controller';
import { ResultadoExaminacionEntity } from './entity/resultado-Examinacion.entity';
import { ResultadoExaminacionService } from './services/resultado-examinacion.service';
import { FormularioExaminacionClaseLicenciaEntity } from '../formularios-examinaciones/entity/FormularioExaminacionClaseLicencia.entity';
import { FormulariosExaminacionesEntity } from '../formularios-examinaciones/entity/formularios-examinaciones.entity';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { AuthService } from 'src/auth/auth.service';
import { TramitesEntity } from '../tramites/entity/tramites.entity';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { OportunidadEntity } from '../oportunidad/oportunidad.entity';
import { RespuestasFormularioExaminacionEntity } from '../respuestas-formulario-examinacion/entity/respuestas-formulario-examinacion.entity';
import { TramitesClaseLicencia } from '../tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { Solicitudes } from '../solicitudes/entity/solicitudes.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ResultadoExaminacionEntity,
      ColaExaminacionEntity,
      FormularioExaminacionClaseLicenciaEntity,
      FormulariosExaminacionesEntity,
      ResultadoExaminacionColaEntity,
      User,
      RolesUsuarios,
      TramitesEntity,
      estadosExaminacionEntity,
      OportunidadEntity,
      RespuestasFormularioExaminacionEntity,
      TramitesClaseLicencia,
      Solicitudes,
    ]),
  ],
  providers: [ResultadoExaminacionService, AuthService],
  exports: [ResultadoExaminacionService],
  controllers: [ResultadoExaminacionController],
})
export class ResultadoExaminacionModule {}
