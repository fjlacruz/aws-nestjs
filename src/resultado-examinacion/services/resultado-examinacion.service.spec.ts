import { Test, TestingModule } from '@nestjs/testing';
import { ResultadoExaminacionService } from './resultado-examinacion.service';

describe('ResultadoExaminacionService', () => {
  let service: ResultadoExaminacionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ResultadoExaminacionService],
    }).compile();

    service = module.get<ResultadoExaminacionService>(ResultadoExaminacionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
