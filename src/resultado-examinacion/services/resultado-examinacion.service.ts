import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { OpcionEstadosCivilEntity } from 'src/opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { OpcionesNivelEducacional } from 'src/opciones-nivel-educacional/entity/nivel-educacional.entity';
import { OpcionSexoEntity } from 'src/opciones-sexo/entity/sexo.entity';
import { Postulante } from 'src/registro-pago/entity/postulante.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { getConnection, Repository } from 'typeorm';
import { CreateResultadoexaminacionDto } from '../DTO/createResultadoexaminacion.dto.';
import { ResultadoExaminacionEntity } from '../entity/resultado-Examinacion.entity';
import { TipoExaminacionesEntity } from '../entity/TipoExaminaciones.entity';
import { FormulariosExaminacionesEntity } from '../../formularios-examinaciones/entity/formularios-examinaciones.entity';
import { ResultadoExaminacionColaEntity } from '../entity/ResultadoExaminacionCola.entity';
import { AuthService } from 'src/auth/auth.service';
import { IPaginationOptions, paginate, paginateRaw } from 'nestjs-typeorm-paginate';
import { FormularioExaminacionClaseLicenciaEntity } from '../../formularios-examinaciones/entity/FormularioExaminacionClaseLicencia.entity';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { Resultado } from 'src/utils/resultado';
import {
  ESTADO_EXAM_ALERTA_PRE_IDON_MORAL_CODE,
  ESTADO_EXAM_APROBADO_IDON_MORAL_CODE,
  ESTADO_EXAM_NO_ALERTA_PRE_IDON_MORAL_CODE,
  ESTADO_EXAM_REPROBADO_IDON_MORAL_CODE,
  TIPOEXAMINACIONCOLA_IDONEIDADMORAL,
  TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL,
} from 'src/shared/Constantes/constantesEstadosExaminacion';
import { ColaExaminacionTramiteNMEntity } from 'src/tramites/entity/cola-examinacion-tramite-n-m.entity';
import { User } from '../../users/entity/user.entity';
import { OportunidadEntity } from '../../oportunidad/oportunidad.entity';
import { ExamenPracticoDto } from '../DTO/ExamenPractico.dto.';
import { PreguntaFormularioExaminacionesDTO } from '../../respuestas-formulario-examinacion/DTO/PreguntaFormularioExaminaciones.dto';
import { RespuestasFormularioExaminacionEntity } from '../../respuestas-formulario-examinacion/entity/respuestas-formulario-examinacion.entity';
import { ColumnasColaExaminacion, PermisosNombres, TipoExaminacionesIds } from 'src/constantes';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { ColaExaminacion } from '../../tramites/entity/cola-examinacion.entity';
import { PostulanteEntity } from '../../postulante/postulante.entity';
import { updateOutput } from 'ts-jest/dist/compiler/instance';

@Injectable()
export class ResultadoExaminacionService {
  constructor(
    private readonly authService: AuthService,
    @InjectRepository(ResultadoExaminacionEntity)
    private readonly resulexaminRespository: Repository<ResultadoExaminacionEntity>,
    @InjectRepository(ColaExaminacionEntity)
    private readonly colaExaminacionEntity: Repository<ColaExaminacionEntity>,
    @InjectRepository(ResultadoExaminacionColaEntity)
    private readonly resultadoExaminacionColaEntity: Repository<ResultadoExaminacionColaEntity>,
    @InjectRepository(FormulariosExaminacionesEntity)
    private readonly formulariosexamRespository: Repository<FormulariosExaminacionesEntity>,
    @InjectRepository(TramitesEntity)
    private readonly tramitesEntityRepository: Repository<TramitesEntity>,
    @InjectRepository(estadosExaminacionEntity)
    private readonly estadosExamRepository: Repository<estadosExaminacionEntity>,
    @InjectRepository(OportunidadEntity)
    private readonly oportunidadEntityRepository: Repository<OportunidadEntity>,
    @InjectRepository(RespuestasFormularioExaminacionEntity)
    private readonly respuestasFormularioExaminacionEntity: Repository<RespuestasFormularioExaminacionEntity>,
    @InjectRepository(TramitesClaseLicencia)
    private readonly tramitesClaseLicenciaRepository: Repository<TramitesClaseLicencia>,
    @InjectRepository(Solicitudes)
    private readonly solicitudesRepository: Repository<Solicitudes>
  ) {}

  async getResultExamin() {
    return await this.resulexaminRespository.find();
  }

  async getResultExaminById(id: number) {
    const region = await this.resulexaminRespository.findOne(id);
    if (!region) throw new NotFoundException('Resultado dont exist');

    return region;
  }

  async update(idResultadoExam: number, dto: Partial<CreateResultadoexaminacionDto>) {
    const UsuarioAprobador = (await this.authService.usuario()).idUsuario;
    const fechaAprobacion = new Date();
    const data = { ...dto, UsuarioAprobador, fechaAprobacion };

    // @ts-ignore
    await this.resulexaminRespository.update({ idResultadoExam }, data);
    return await this.resulexaminRespository.findOne({ idResultadoExam });
  }

  async saveRespuesta(data: PreguntaFormularioExaminacionesDTO): Promise<RespuestasFormularioExaminacionEntity> {
    const respuestaFind = await this.respuestasFormularioExaminacionEntity.findOne({
      where: {
        idFomularioPregunta: data.idFomularioPregunta,
        idPreguntaExam: data.idPreguntaExam,
        idResultadoExam: data.idResultadoExam,
      },
    });
    if (respuestaFind) {
      data.idRespuestaFormExam = respuestaFind.idRespuestaFormExam;
      const respuesta = await this.respuestasFormularioExaminacionEntity.save(data);
      return respuesta;
    } else {
      const respuesta = await this.respuestasFormularioExaminacionEntity.save(data);
      return respuesta;
    }
  }

  async saveExamenPractico(idColaExaminacion: number, data: ExamenPracticoDto): Promise<string> {
    for (const respuesta of data.respuestas) {
      await this.saveRespuesta(respuesta);
    }
    return await this.create(idColaExaminacion, data.resultado);
  }

  async create(idColaExaminacion: number, data: CreateResultadoexaminacionDto): Promise<string> {
    console.log(data);
    console.log('*************************************');
    const dato = new ResultadoExaminacionEntity();
    dato.UsuarioAprobador = data.UsuarioAprobador;
    dato.alaEsperaAntecedentes = data.alaEsperaAntecedentes === true ? 1 : 0;
    dato.aprobado = data.aprobado;
    dato.fechaAprobacion = data.fechaAprobacion;
    dato.idFormularioExaminaciones = data.idFormularioExaminaciones;
    dato.idResultadoExam = data.idResultadoExam;
    // dato.idSolicitud = data.idSolicitud;
    dato.observaciones = data.observaciones;
    await this.resulexaminRespository.save(dato);
    const resultadoExaminacionCola = await this.resultadoExaminacionColaEntity
      .createQueryBuilder('ResultadoExaminacionCola')
      .leftJoinAndMapMany(
        'ResultadoExaminacionCola.idResultadoExam',
        ResultadoExaminacionEntity,
        're',
        'ResultadoExaminacionCola.idResultadoExam = re.idResultadoExam'
      )
      .andWhere('ResultadoExaminacionCola.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
      .getRawMany();
    const respuestas = resultadoExaminacionCola.filter(item => item.re_aprobado === null);
    if (respuestas.length === 0) {
      const colaExaminacion = await this.colaExaminacionEntity
        .createQueryBuilder('ColaExaminacion')
        .andWhere('ColaExaminacion.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
        .getOne();
      colaExaminacion.historico = true;
      var f = new Date();
      colaExaminacion.updated_at = f;
      const aprobados = resultadoExaminacionCola.filter(item => item.re_aprobado === true);
      const rechazados = resultadoExaminacionCola.filter(item => item.re_aprobado === false);
      if (rechazados.length > 0) {
        // Distinguir tipos de examinación y asignar estado
        colaExaminacion.aprobado = false;

        // Asignar estado examinación según el tipo
        switch (colaExaminacion.idTipoExaminacion as number) {
          case 2: {
            colaExaminacion.idEstadosExaminacion = 16;
            break;
          }
          case 3: {
            colaExaminacion.idEstadosExaminacion = 22;
            break;
          }
          case 4: {
            colaExaminacion.idEstadosExaminacion = 11;
            break;
          }
          case 5: {
            colaExaminacion.idEstadosExaminacion = 36;
            break;
          }
          case 6: {
            colaExaminacion.idEstadosExaminacion = 3;
            break;
          }
        }
      }
      if (aprobados.length == resultadoExaminacionCola.length) {
        // Distinguir tipos de examinación y asignar estado
        colaExaminacion.aprobado = true;

        // Asignar estado examinación según el tipo
        switch (colaExaminacion.idTipoExaminacion as number) {
          case 2: {
            colaExaminacion.idEstadosExaminacion = 15;
            break;
          }
          case 3: {
            colaExaminacion.idEstadosExaminacion = 21;
            break;
          }
          case 4: {
            colaExaminacion.idEstadosExaminacion = 10;
            break;
          }
          case 5: {
            colaExaminacion.idEstadosExaminacion = 35;
            break;
          }
          case 6: {
            colaExaminacion.idEstadosExaminacion = 4;
            break;
          }
        }
      }
      await this.colaExaminacionEntity.save(colaExaminacion);
    }
    // else {
    //   const colaExaminacion = await this.colaExaminacionEntity
    //     .createQueryBuilder('ColaExaminacion')
    //     .andWhere('ColaExaminacion.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
    //     .getOne();
    //   colaExaminacion.historico = true;
    //   await this.colaExaminacionEntity.save(colaExaminacion);
    // }

    const cola: ColaExaminacionEntity = await this.colaExaminacionEntity.findOne({ idColaExaminacion: idColaExaminacion });
    const tramite: TramitesEntity = await this.tramitesEntityRepository.findOne({ idTramite: cola.idTramite });
    if (data.aprobado === true && cola.idTipoExaminacion === 3) {
      tramite.idEstadoTramite = 3;
    }
    if (data.aprobado === false && cola.idTipoExaminacion !== 3) {
      tramite.idEstadoTramite = 2;
    }
    await this.tramitesEntityRepository.save(tramite);
    // const oportunidad = new OportunidadEntity();
    // oportunidad.idColaExaminacion = idColaExaminacion;
    // oportunidad.ingresadoPor = data.UsuarioAprobador;
    // oportunidad.fechaIngreso = tramite.created_at;
    // oportunidad.fechaAprobacion = data.fechaAprobacion;
    // oportunidad.observacion = data.observaciones;
    // oportunidad.oportunidadAprobada = data.aprobado;
    // oportunidad.aprobadoPor = data.UsuarioAprobador;
    // oportunidad.created_at = new Date();
    // oportunidad.updated_at = new Date();
    // await this.oportunidadEntityRepository.save(oportunidad);
    return 'ok';
  }

  async saveClaseF(idTramite: number) {
    const tramite: TramitesEntity = await this.tramitesEntityRepository.findOne({ idTramite: idTramite });
    const solicitud: Solicitudes = await this.solicitudesRepository.findOne({ idSolicitud: tramite.idSolicitud });
    solicitud.idEstado = 7;
    tramite.idEstadoTramite = 3;
    await this.tramitesEntityRepository.save(tramite);
    await this.solicitudesRepository.save(solicitud);
    return 'ok';
  }

  async CalucularAprobacionColaExaminacion() {
    return await this.resulexaminRespository.find();
  }

  async getListaPendientesAtencion(
    req: any,
    idTipoExaminacion: number,
    run: string,
    nombre: string,
    numeroSolicitud: string,
    numeroTramite: string,
    claseLicencia: string,
    fechacreacion: string,
    resultadoExamen: string,
    oportunidad: string,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: any
  ) {
    let orderBy = ColumnasColaExaminacion[orden];

    const qb = this.colaExaminacionEntity
      .createQueryBuilder('ColaExaminacion')
      .leftJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesEntity, 'tr', 'ColaExaminacion.idTramite = tr.idTramite')
      .leftJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
      .leftJoinAndMapMany('tr.idTramiteClaseLicencia', TramitesClaseLicencia, 'tc', 'tr.idTramite = tc.idTramite')
      .leftJoinAndMapMany('tr.idEstadoTramite', EstadosTramiteEntity, 'EstadoTramite', 'tr.idEstadoTramite = EstadoTramite.idEstadoTramite')
      .leftJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
      .leftJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
      .leftJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')
      .leftJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante')
      .leftJoinAndMapMany('so.idUsuario', User, 'user', 'so.idUsuario = user.idUsuario')
      .leftJoinAndMapMany('so.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .leftJoinAndMapMany(
        'so.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .leftJoinAndMapMany(
        'so.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      )
      .loadRelationCountAndMap('ColaExaminacion.oportunidadesCount', 'ColaExaminacion.oportunidades', 'oportunidad')
      // .leftJoinAndSelect('tc.tramite', 'tramite')
      .select([
        'ColaExaminacion.idColaExaminacion',
        'ColaExaminacion.aprobado',
        'ColaExaminacion.historico',
        'ColaExaminacion.idTramite',
        'ColaExaminacion.created_at',
        'ColaExaminacion.tramites.idTramite',
        'ColaExaminacion.tipoExaminacion.idTipoExaminacion',
        'te.idTipoExaminacion',
        'te.nombreExaminacion',
        'tr.idTramite',
        'tr.created_at',
        'tr.solicitudes.idSolicitud',
        'tr.tiposTramite.idTipoTramite',
        'tr.estadoTramite.idEstadoTramite',
        'EstadoTramite.Nombre',
        'tc.idTramiteClaseLicencia',
        'tc.clasesLicencias.idClaseLicencia',
        'cl.idClaseLicencia',
        'cl.Nombre',
        'cl.Abreviacion',
        'tt.idTipoTramite',
        'tt.Nombre',
        'so.idSolicitud',
        'so.idTipoSolicitud',
        'so.FechaCreacion',
        'so.idPostulante',
        'so.postulante.idPostulante',
        'so.institucion.idInstitucion',
        'so.user.idUsuario',
        'user.Nombres',
        'user.RUN',
        'user.DV',
        'municipalidad.Nombre',
        'po.idPostulante',
        'po.opcionSexo.idOpcionSexo',
        'po.opcionEstadosCivil.idOpcionEstadosCivil',
        'po.opcionesNivelEducacional.idOpNivelEducacional',
        'po.RUN',
        'po.DV',
        'po.Nombres',
        'po.ApellidoPaterno',
        'po.ApellidoMaterno',
        'po.Nacionalidad',
        'po.Email',
        'po.Telefono',
        'po.FechaNacimiento',
        'po.Profesion',
        'po.Calle',
        'po.CalleNro',
        'po.RestoDireccion',
        'po.Letra',
        'po.idRegion',
        'po.idComuna',
        'sexo.Nombre',
        'EstadosCivil.idOpcionEstadosCivil',
        'EstadosCivil.Nombre',
        'NivelEducacional.Nombre',
      ]);

    qb.andWhere('ColaExaminacion.historico = false');
    qb.andWhere('ColaExaminacion.aprobado IS NULL');
    qb.andWhere('tr.estadoTramite in (1,2,3,19)');
    qb.andWhere('so.idEstado in (7,12)');
    qb.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
      idTipoExaminacion: idTipoExaminacion,
    });

    // Comprobamos permisos (Al ser usado en varios apartados, seleccionamos permisos según el caso)
    let permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenMedico;

    if (idTipoExaminacion == TipoExaminacionesIds.ExamenTeorico)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenTeorico;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenPractico)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenPractico;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenMedico) permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenMedico;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenIM)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesEvaluacionIdoneidadMoral;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenPreIM)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesPreEvaluacionIdoneidadMoral;

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, permisoAComprobar);

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      qb.andWhere('FALSE');
    }

    // 3.b Añadimos condición para filtrar por oficina si se da el caso
    // Filtramos por las oficinas en caso de que el usuario no sea administrador
    //Comprobamos si es superUsuario por si es necesario filtrar
    if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
      // Recogemos las oficinas asociadas al usuario
      let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

      if (idOficinasAsociadas == null) {
        qb.andWhere('FALSE');
      } else {
        qb.andWhere('so.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
      }
    }

    if (run) {
      const rut = run.split('-');
      let runsplited: string = rut[0];
      runsplited = runsplited.replace('.', '');
      runsplited = runsplited.replace('.', '');
      const div = rut[1];
      qb.andWhere('po.RUN = :run', {
        run: runsplited,
      });

      qb.andWhere('po.DV = :dv', {
        dv: div,
      });
    }
    if (nombre) qb.andWhere('LOWER(po.Nombres) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(po.ApellidoPaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(po.ApellidoMaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (numeroSolicitud) qb.andWhere('so.idSolicitud = :numeroSolicitud', { numeroSolicitud: numeroSolicitud });
    if (numeroTramite) qb.andWhere('tr.idTramite = :numeroTramite', { numeroTramite: numeroTramite });
    if (claseLicencia) qb.andWhere('cl.idClaseLicencia = :idClaseLicencia', { idClaseLicencia: claseLicencia });
    if (fechacreacion) qb.andWhere('DATE(so.FechaCreacion) = :created_at', { created_at: fechacreacion });
    // if (idMunicipalidad) qb.andWhere('municipalidad.idInstitucion = :idInstitucion', { idInstitucion: idMunicipalidad });
    if (resultadoExamen) qb.andWhere('ColaExaminacion.aprobado = :resultadoExamen', { resultadoExamen: resultadoExamen });
    if (idTipoExaminacion !== 3) {
      if (oportunidad) {
        qb.andWhere(
          '(SELECT COUNT(*) FROM "Oportunidades" WHERE "Oportunidades"."idColaExaminacion" = "ColaExaminacion"."idColaExaminacion") = ' +
            oportunidad
        );
      }
    }
    qb.orderBy(orderBy, ordenarPor);
    console.log('------------------------------------------------');
    return paginate<any>(qb, options);
  }

  async getListaPendientesAtencionIdoneidadMoral(
    idTipoExaminacion: number,
    run: string,
    nombre: string,
    claseLicencia: string,
    fechacreacion: string,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: string
  ) {
    const qb = this.colaExaminacionEntity
      .createQueryBuilder('ColaExaminacion')
      .leftJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesEntity, 'tr', 'ColaExaminacion.idTramite = tr.idTramite')
      .leftJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesClaseLicencia, 'tc', 'ColaExaminacion.idTramite = tc.idTramite')
      .leftJoinAndMapMany('tr.idEstadoTramite', EstadosTramiteEntity, 'EstadoTramite', 'tr.idEstadoTramite = EstadoTramite.idEstadoTramite')
      .leftJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
      .leftJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
      .leftJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')
      .leftJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante')
      .leftJoinAndMapMany('so.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .leftJoinAndMapMany(
        'so.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .leftJoinAndMapMany(
        'so.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      );

    qb.andWhere('ColaExaminacion.historico = false');
    // qb.andWhere('ColaExaminacion.idEstadosExaminacion IN (4,3,2)');
    qb.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
      idTipoExaminacion: idTipoExaminacion,
    });

    if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
    if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
    if (orden === 'run' && ordenarPor === 'ASC') qb.orderBy('po.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qb.orderBy('po.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qb.orderBy('po.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qb.orderBy('po.Nombres', 'DESC');
    if (orden === 'claseLicencia' && ordenarPor === 'ASC') qb.orderBy('cl.Abreviacion', 'ASC');
    if (orden === 'claseLicencia' && ordenarPor === 'DESC') qb.orderBy('cl.Abreviacion', 'DESC');

    return paginateRaw<any>(qb, options);
  }

  /**
   *
   * @param idTipoExaminacion
   * @param run
   * @param nombre
   * @param numeroSolicitud
   * @param numeroTramite
   * @param claseLicencia
   * @param fechacreacion
   * @param resultadoExamen
   * @param oportunidad
   * @param options
   * @param orden
   * @param ordenarPor
   */
  async getHistoricoExamenes(
    req: any,
    idTipoExaminacion: number,
    run: string,
    nombre: string,
    idColaExaminacion: string,
    numeroSolicitud: string,
    numeroTramite: string,
    claseLicencia: string,
    fechacreacion: string,
    idMunicipalidad: number,
    resultadoExamen: string,
    oportunidad: string,
    idResultadoExam: string,
    nomExaminadorPractico: number,
    fechaRendicionExamen: Date,
    options: IPaginationOptions,
    orden?: string,
    ordenarPor?: any
  ) {
    let orderBy = ColumnasColaExaminacion[orden];
    const qb = this.colaExaminacionEntity
      .createQueryBuilder('ColaExaminacion')
      .leftJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesEntity, 'tr', 'ColaExaminacion.idTramite = tr.idTramite')
      .leftJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
      .leftJoinAndMapMany('tr.idTramiteClaseLicencia', TramitesClaseLicencia, 'tc', 'tr.idTramite = tc.idTramite')
      .leftJoinAndMapMany('tr.idEstadoTramite', EstadosTramiteEntity, 'EstadoTramite', 'tr.idEstadoTramite = EstadoTramite.idEstadoTramite')
      .leftJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
      .leftJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
      .leftJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')
      .leftJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante')
      .leftJoinAndMapMany('so.idUsuario', User, 'user', 'so.idUsuario = user.idUsuario')
      .leftJoinAndMapMany('so.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .leftJoinAndMapMany(
        'so.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .leftJoinAndMapMany(
        'so.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      )
      .loadRelationCountAndMap('ColaExaminacion.oportunidadesCount', 'ColaExaminacion.oportunidades', 'oportunidad')
      .leftJoinAndSelect('tc.tramite', 'tramite')
      .leftJoinAndSelect('ColaExaminacion.resultadoExaminacionCola', 'resultado')
      .leftJoinAndMapMany('resultado.idResultadoExam', ResultadoExaminacionEntity, 'res', 'res.idResultadoExam = resultado.idResultadoExam')
      .select([
        'resultado',
        'resultado.resultadoExaminacion.idResultadoExam',
        'res.idResultadoExam',
        'res.aprobado',
        'res.fechaAprobacion',
        'ColaExaminacion.idColaExaminacion',
        'ColaExaminacion.aprobado',
        'ColaExaminacion.historico',
        'ColaExaminacion.idTramite',
        'ColaExaminacion.tramites.idTramite',
        'ColaExaminacion.tipoExaminacion.idTipoExaminacion',
        'te.idTipoExaminacion',
        'te.nombreExaminacion',
        'tr.idTramite',
        'tr.created_at',
        'tr.solicitudes.idSolicitud',
        'tr.tiposTramite.idTipoTramite',
        'tr.estadoTramite.idEstadoTramite',
        'EstadoTramite.Nombre',
        'tc.idTramiteClaseLicencia',
        'tc.clasesLicencias.idClaseLicencia',
        'cl.idClaseLicencia',
        'cl.Nombre',
        'cl.Abreviacion',
        'tt.idTipoTramite',
        'tt.Nombre',
        'so.idSolicitud',
        'so.idTipoSolicitud',
        'so.FechaCreacion',
        'so.idPostulante',
        'so.postulante.idPostulante',
        'so.institucion.idInstitucion',
        'so.user.idUsuario',
        'user.Nombres',
        'user.RUN',
        'user.DV',
        'municipalidad.idInstitucion',
        'municipalidad.Nombre',
        'po.idPostulante',
        'po.opcionSexo.idOpcionSexo',
        'po.opcionEstadosCivil.idOpcionEstadosCivil',
        'po.opcionesNivelEducacional.idOpNivelEducacional',
        'po.RUN',
        'po.DV',
        'po.Nombres',
        'po.ApellidoPaterno',
        'po.ApellidoMaterno',
        'po.Nacionalidad',
        'po.Email',
        'po.Telefono',
        'po.FechaNacimiento',
        'po.Profesion',
        'sexo.Nombre',
        'EstadosCivil.idOpcionEstadosCivil',
        'EstadosCivil.Nombre',
        'NivelEducacional.Nombre',
      ]);

    qb.andWhere('ColaExaminacion.historico = true');
    qb.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
      idTipoExaminacion: idTipoExaminacion,
    });

    // Comprobamos permisos (Al ser usado en varios apartados, seleccionamos permisos según el caso)
    let permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenMedico;

    if (idTipoExaminacion == TipoExaminacionesIds.ExamenTeorico)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenTeorico;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenPractico)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenPractico;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenMedico) permisoAComprobar = PermisosNombres.VisualizarListaPostulantesExamenMedico;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenIM)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesEvaluacionIdoneidadMoral;
    if (idTipoExaminacion == TipoExaminacionesIds.ExamenPreIM)
      permisoAComprobar = PermisosNombres.VisualizarListaPostulantesPreEvaluacionIdoneidadMoral;

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, permisoAComprobar);

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      qb.andWhere('FALSE');
    }

    // 3.b Añadimos condición para filtrar por oficina si se da el caso
    // Filtramos por las oficinas en caso de que el usuario no sea administrador
    //Comprobamos si es superUsuario por si es necesario filtrar
    if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
      // Recogemos las oficinas asociadas al usuario
      let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

      if (idOficinasAsociadas == null) {
        qb.andWhere('FALSE');
      } else {
        qb.andWhere('so.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
      }
    }

    if (run) {
      const rut = run.split('-');
      let runsplited: string = rut[0];
      runsplited = runsplited.replace('.', '');
      runsplited = runsplited.replace('.', '');
      const div = rut[1];
      qb.andWhere('po.RUN = :run', {
        run: runsplited,
      });

      qb.andWhere('po.DV = :dv', {
        dv: div,
      });
    }
    if (nombre) qb.andWhere('LOWER(po.Nombres) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(po.ApellidoPaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (nombre) qb.orWhere('LOWER(po.ApellidoMaterno) like LOWER(:nombre)', { nombre: `%${nombre}%` });
    if (numeroSolicitud) qb.andWhere('so.idSolicitud = :numeroSolicitud', { numeroSolicitud: numeroSolicitud });
    if (numeroTramite) qb.andWhere('tr.idTramite = :numeroTramite', { numeroTramite: numeroTramite });
    if (claseLicencia) qb.andWhere('cl.idClaseLicencia = :idClaseLicencia', { idClaseLicencia: claseLicencia });
    if (fechacreacion) qb.andWhere('DATE(so.FechaCreacion) = :created_at', { created_at: fechacreacion });
    if (idMunicipalidad) qb.andWhere('municipalidad.idInstitucion = :idInstitucion', { idInstitucion: idMunicipalidad });
    if (idColaExaminacion) qb.andWhere('ColaExaminacion.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion });
    if (resultadoExamen) qb.andWhere('ColaExaminacion.aprobado = :resultadoExamen', { resultadoExamen: resultadoExamen });
    if (idResultadoExam) qb.andWhere('res.idResultadoExam = :idResultadoExam', { idResultadoExam: idResultadoExam });
    if (oportunidad) {
      qb.andWhere(
        '(SELECT COUNT(*) FROM "Oportunidades" WHERE "Oportunidades"."idColaExaminacion" = "ColaExaminacion"."idColaExaminacion") = ' +
          oportunidad
      );
    }
    if (nomExaminadorPractico) qb.andWhere('so.idUsuario = :nomExaminadorPractico', { nomExaminadorPractico: nomExaminadorPractico });
    if (fechaRendicionExamen) qb.andWhere('DATE(res.fechaAprobacion) = :fechaAprobacion', { fechaAprobacion: fechaRendicionExamen });
    console.log(orderBy);
    console.log(ordenarPor);
    qb.orderBy(orderBy, ordenarPor);
    return paginate<any>(qb, options);
  }
  //SE UNSA SOLAMENTE PARA LA EXAMINANCION Idoneidad Moral Pre-Evaluacion
  async getHistoricoExamenesIdoneidadMoralPreEvaluacion(
    options: IPaginationOptions,
    idTipoExaminacion: number,
    run?: string,
    nombres?: string,
    Nomclaselic?: string,
    nomet?: string,
    fechaIngresoResultado?: Date,
    fechaRendicionExamen?: Date,
    nomExaminadorPractico?: number,
    idMunicipalidad?: number,
    resultadoExamen?: string,
    orden?: string,
    ordenarPor?: string
  ) {
    const qb = this.colaExaminacionEntity
      .createQueryBuilder('ColaExaminacion')
      .leftJoinAndMapMany(
        'ColaExaminacion.idTipoExaminacion',
        TipoExaminacionesEntity,
        'te',
        'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
      )
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesEntity, 'tr', 'ColaExaminacion.idTramite = tr.idTramite')
      .leftJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
      .leftJoinAndMapMany('ColaExaminacion.idTramite', TramitesClaseLicencia, 'tc', 'ColaExaminacion.idTramite = tc.idTramite')
      .leftJoinAndMapMany('tr.idEstadoTramite', EstadosTramiteEntity, 'EstadoTramite', 'tr.idEstadoTramite = EstadoTramite.idEstadoTramite')
      .leftJoinAndMapMany('tc.idClaseLicencia', ClasesLicenciasEntity, 'cl', 'tc.idClaseLicencia = cl.idClaseLicencia')
      .leftJoinAndMapMany('tr.idSolicitud', Solicitudes, 'so', 'tr.idSolicitud = so.idSolicitud')
      .leftJoinAndMapMany('so.idInstitucion', InstitucionesEntity, 'municipalidad', 'municipalidad.idInstitucion = so.idInstitucion')
      .leftJoinAndMapMany('so.idPostulante', Postulante, 'po', 'so.idPostulante = po.idPostulante')
      .leftJoinAndMapMany('so.sexo', OpcionSexoEntity, 'sexo', 'sexo.idOpcionSexo = po.idOpcionSexo')
      .leftJoinAndMapMany(
        'so.estadoCivil',
        OpcionEstadosCivilEntity,
        'EstadosCivil',
        'EstadosCivil.idOpcionEstadosCivil = po.idOpcionEstadosCivil'
      )
      .leftJoinAndMapMany(
        'so.nuivelEducacional',
        OpcionesNivelEducacional,
        'NivelEducacional',
        'NivelEducacional.idOpNivelEducacional = po.idOpNivelEducacional'
      );
    qb.andWhere('ColaExaminacion.historico = true');
    qb.andWhere('ColaExaminacion.idEstadosExaminacion IN (4,3,2)');
    qb.andWhere('te.idTipoExaminacion = :idTipoExaminacion', {
      idTipoExaminacion: idTipoExaminacion,
    });
    // qb.andWhere(
    //   'ColaExaminacion.idEstadosExaminacion = 4  OR  ColaExaminacion.idEstadosExaminacion = 2 OR  ColaExaminacion.idEstadosExaminacion = 3'
    // );

    if (run) {
      const rut = run.split('-');
      let runsplited: string = rut[0];
      runsplited = runsplited.replace('.', '');
      runsplited = runsplited.replace('.', '');
      const div = rut[1];
      qb.andWhere('po.RUN = :run', {
        run: runsplited,
      });

      qb.andWhere('po.DV = :dv', {
        dv: div,
      });
    }
    if (nombres) qb.andWhere('LOWER(po.Nombres) like LOWER(:nombre)', { nombre: `%${nombres}%` });
    if (nombres) qb.andWhere('LOWER(po.ApellidoPaterno) like LOWER(:nombre)', { nombre: `%${nombres}%` });
    if (nombres) qb.andWhere('LOWER(po.ApellidoMaterno) like LOWER(:nombre)', { nombre: `%${nombres}%` });
    if (Nomclaselic) qb.andWhere('cl.idClaseLicencia = :idClaseLicencia', { idClaseLicencia: Nomclaselic });
    if (fechaIngresoResultado) qb.andWhere('so.FechaCreacion = :created_at', { created_at: fechaIngresoResultado });
    if (idMunicipalidad) qb.andWhere('municipalidad.idInstitucion = :idInstitucion', { idInstitucion: idMunicipalidad });
    if (resultadoExamen) qb.andWhere('ColaExaminacion.aprobado = :resultadoExamen', { resultadoExamen: resultadoExamen });

    if (orden === 'idColaExaminacion' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'ASC');
    if (orden === 'idColaExaminacion' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.idColaExaminacion', 'DESC');
    if (orden === 'run' && ordenarPor === 'ASC') qb.orderBy('po.RUN', 'ASC');
    if (orden === 'run' && ordenarPor === 'DESC') qb.orderBy('po.RUN', 'DESC');
    if (orden === 'postulante' && ordenarPor === 'ASC') qb.orderBy('po.Nombres', 'ASC');
    if (orden === 'postulante' && ordenarPor === 'DESC') qb.orderBy('po.Nombres', 'DESC');
    if (orden === 'claseLicencia' && ordenarPor === 'ASC') qb.orderBy('cl.Abreviacion', 'ASC');
    if (orden === 'claseLicencia' && ordenarPor === 'DESC') qb.orderBy('cl.Abreviacion', 'DESC');
    if (orden === 'estadoExaminacion' && ordenarPor === 'ASC') qb.orderBy('ColaExaminacion.aprobado', 'ASC');
    if (orden === 'estadoExaminacion' && ordenarPor === 'DESC') qb.orderBy('ColaExaminacion.aprobado', 'DESC');

    return paginateRaw<any>(qb, options);
  }

  async getHistoricoExamenesDetalle(idColaExaminacion: number, idTipoTramite: number, idTipoExaminacion: number, idClaseLicencia: number) {
    const formularios = await this.getFormularioExaminacionClaseLicencia(idTipoTramite, idTipoExaminacion, idClaseLicencia);
    for (const form of formularios) {
      const resultadoExaminacion = await this.getResultadoExaminacionByFormulario(form.idFromularioExaminaciones, idColaExaminacion);
      form['resultadoExaminacion'] = resultadoExaminacion;
    }
    return formularios;
  }

  async getOportunidades(idColaExaminacion: number) {
    const oportunidades = await this.oportunidadEntityRepository.find({ where: { idColaExaminacion: idColaExaminacion } });
    return oportunidades;
  }

  async getFormularioExaminacionClaseLicencia(idTipoTramite: number, idTipoExaminacion: number, idClaseLicencia: number) {
    const formulario = await this.formulariosexamRespository
      .createQueryBuilder('FormulariosExaminaciones')
      .select([
        'FormulariosExaminaciones.idFromularioExaminaciones',
        'FormulariosExaminaciones.btntext',
        'FormulariosExaminaciones.nombreFormulario',
      ])
      .innerJoinAndMapMany(
        'FormularioExaminacionClaseLicencia',
        FormularioExaminacionClaseLicenciaEntity,
        'fexcla',
        'FormulariosExaminaciones.idFromularioExaminaciones = fexcla.idFormularioExaminaciones'
      )
      .innerJoinAndMapMany(
        'FormulariosExaminaciones.idTipoTramite',
        TiposTramiteEntity,
        'titra',
        'FormulariosExaminaciones.idTipoTramite = titra.idTipoTramite'
      )
      .innerJoinAndMapMany('ClasesLicencias', ClasesLicenciasEntity, 'clic', 'fexcla.idClaseLicencia = clic.idClaseLicencia');

    formulario.andWhere('FormulariosExaminaciones.idTipoTramite = :idTipoTramite', { idTipoTramite: idTipoTramite });
    formulario.andWhere('FormulariosExaminaciones.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: idTipoExaminacion });
    formulario.andWhere('fexcla.idClaseLicencia = :idClaseLicencia', { idClaseLicencia: idClaseLicencia });
    return formulario.getMany();
  }

  async getResultadoExaminacionByFormulario(idFormularioExaminaciones: number, idColaExaminacion: number) {
    const resultadoExaminacion = await this.resulexaminRespository
      .createQueryBuilder('ResultadoExaminacion')
      .select([
        'ResultadoExaminacion.idResultadoExam',
        'ResultadoExaminacion.aprobado',
        'ResultadoExaminacion.observaciones',
        'ResultadoExaminacion.alaEsperaAntecedentes',
      ])
      .innerJoinAndMapMany(
        'ResultadoExaminacionCola',
        ResultadoExaminacionColaEntity,
        'rec',
        'ResultadoExaminacion.idResultadoExam = rec.idResultadoExam'
      );
    resultadoExaminacion.where('ResultadoExaminacion.idFormularioExaminaciones = :idFormularioExaminaciones', {
      idFormularioExaminaciones: idFormularioExaminaciones,
    });
    resultadoExaminacion.andWhere('rec.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion });
    return resultadoExaminacion.getMany();
  }

  async crearResExaminacionPreevMoralBySolicitudId(data: CreateResultadoexaminacionDto): Promise<Resultado> {
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    // Recuperamos la/s cola/s de examinaciones para asignar el nuevo estado
    let resultado = new Resultado();

    try {
      let nuevoEstadoColaExamPreevIdonMoral: string = '';

      if (!data.aprobado) {
        nuevoEstadoColaExamPreevIdonMoral = ESTADO_EXAM_ALERTA_PRE_IDON_MORAL_CODE;
      } else {
        nuevoEstadoColaExamPreevIdonMoral = ESTADO_EXAM_NO_ALERTA_PRE_IDON_MORAL_CODE;
      }

      // 1 Recuperar el estado de examinación
      const estadoNuevoExaminacionAprobReaprob: estadosExaminacionEntity = await this.estadosExamRepository.findOne({
        codigo: nuevoEstadoColaExamPreevIdonMoral,
      });

      // 2. Nos traemos la información de los trámites asociados junto a sus examinaciones
      const qbTramites = this.tramitesEntityRepository
        .createQueryBuilder('tr')
        .innerJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
        .innerJoinAndMapMany(
          'tr.idEstadoTramite',
          EstadosTramiteEntity,
          'EstadoTramite',
          'tr.idEstadoTramite = EstadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapMany(
          'tr.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'ColaExaminacionNM',
          'ColaExaminacionNM.idTramite = tr.idTramite'
        )
        .innerJoinAndMapMany(
          'ColaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'ColaExaminacion',
          'ColaExaminacion.idColaExaminacion = ColaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne(
          'tr.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tr.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'tramitesClaseLicencia.idClaseLicencia = clasesLicencias.idClaseLicencia'
        );
      qbTramites.andWhere('tr.idSolicitud = ' + data.idSolicitud);

      const resultadoTramites: TramitesEntity[] = await qbTramites.getMany();

      // Variable para guardar los ids de las examinaciones
      let idsColaExaminacion: number[] = [];

      resultadoTramites.forEach((tramite, i) => {
        tramite.colaExaminacionNM.forEach((colaNM, j) => {
          idsColaExaminacion.push(colaNM.idColaExaminacion);
        });
      });

      // 3. Nos traemos los resultados de examinación asociados
      const qbResultadosExaminacion = this.colaExaminacionEntity
        .createQueryBuilder('ColaExaminacion')
        .innerJoinAndMapMany(
          'ColaExaminacion.TipoExaminacion',
          TipoExaminacionesEntity,
          'te',
          'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.ResultadoExaminacionCola',
          ResultadoExaminacionColaEntity,
          'ResultadoExaminacionCola',
          'ColaExaminacion.idColaExaminacion = ResultadoExaminacionCola.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'ResultadoExaminacion.idResultadoExam',
          ResultadoExaminacionEntity,
          'ResultadoExaminacion',
          'ResultadoExaminacionCola.idResultadoExam = ResultadoExaminacion.idResultadoExam'
        );

      qbResultadosExaminacion
        .andWhere('ColaExaminacion.idColaExaminacion IN (:...idsColaExaminacion)', { idsColaExaminacion: idsColaExaminacion })
        .andWhere('ColaExaminacion.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: TIPOEXAMINACIONCOLA_PREEVIDONEIDADMORAL });

      const resultadoColaExaminacion: ColaExaminacionEntity[] = await qbResultadosExaminacion.getMany();

      if (resultadoColaExaminacion) {
        for (const ce of resultadoColaExaminacion) {
          var f = new Date();
          ce.idEstadosExaminacion = estadoNuevoExaminacionAprobReaprob.idEstadosExaminacion;
          ce.aprobado = data.aprobado;
          ce.historico = true;
          ce.updated_at = f;

          // await queryRunner.manager.update( ColaExaminacionEntity,
          //                                   ce.idColaExaminacion,
          //                                   ce);
        }

        console.log(
          '======================================================================================================================================='
        );
        console.log(
          '======================================================================================================================================='
        );
        console.log(
          '======================================================================================================================================='
        );
        console.log(resultadoColaExaminacion);

        await queryRunner.manager.save(ColaExaminacionEntity, resultadoColaExaminacion);
      }

      // this.resulexaminRespository.save(resultadosExam);
      //this.colaExaminacionEntity.save(resultadoColaExaminacion);
      //await queryRunner.manager.update(ColaExaminacionEntity, tramiteLicencia.idTramite,);
      const idUsuario = (await this.authService.usuario()).idUsuario;

      //Guardamos los resultados de examinación asociados
      const resultadoResultadoExaminaciones: ResultadoExaminacionEntity[] = await this.resulexaminRespository
        .createQueryBuilder('ResultadoExaminacion')
        .innerJoinAndMapMany(
          'ResultadoExaminacion.resultadoExaminacionCola',
          ResultadoExaminacionColaEntity,
          'resultadoExaminacionCola',
          'ResultadoExaminacion.idResultadoExam = resultadoExaminacionCola.idResultadoExam'
        )
        .andWhere('resultadoExaminacionCola.idColaExaminacion IN (:...idsColaExaminacion)', {
          idsColaExaminacion: resultadoColaExaminacion.map(x => x.idColaExaminacion),
        })
        .getMany();

      if (resultadoResultadoExaminaciones) {
        resultadoResultadoExaminaciones.forEach(re => {
          re.aprobado = data.aprobado;
          re.fechaAprobacion = new Date();
          re.UsuarioAprobador = idUsuario;
          //re.UsuarioAprobador
        });
      }

      await queryRunner.manager.save(ResultadoExaminacionEntity, resultadoResultadoExaminaciones);

      await queryRunner.commitTransaction();

      resultado.Mensaje = 'Se han guardado los resultados de examinación correctamente';
      resultado.ResultadoOperacion = true;
    } catch (Error) {
      resultado.Error = 'Se ha producido un error, no se pudo asignar los resultados de examinación';
      resultado.ResultadoOperacion = true;

      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async crearResExaminacionIdonMoralBySolicitudId(req: any, data: CreateResultadoexaminacionDto): Promise<Resultado> {
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    // Recuperamos la/s cola/s de examinaciones para asignar el nuevo estado
    let resultado = new Resultado();

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.IngresoyEvaluacionIdoneidadMoralAprobarRechazar
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      let nuevoEstadoColaExamIdonMoral: string = '';

      if (data.aprobado) {
        nuevoEstadoColaExamIdonMoral = ESTADO_EXAM_APROBADO_IDON_MORAL_CODE;
      } else {
        nuevoEstadoColaExamIdonMoral = ESTADO_EXAM_REPROBADO_IDON_MORAL_CODE;
      }

      // 1 Recuperar el estado de examinación
      const estadoNuevoExaminacionAprobReaprob: estadosExaminacionEntity = await this.estadosExamRepository.findOne({
        codigo: nuevoEstadoColaExamIdonMoral,
      });

      // 2. Nos traemos la información de los trámites asociados junto a sus examinaciones
      const qbTramites = this.tramitesEntityRepository
        .createQueryBuilder('tr')
        .innerJoinAndMapMany('tr.idTipoTramite', TiposTramiteEntity, 'tt', 'tr.idTipoTramite = tt.idTipoTramite')
        .innerJoinAndMapMany(
          'tr.idEstadoTramite',
          EstadosTramiteEntity,
          'EstadoTramite',
          'tr.idEstadoTramite = EstadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapMany(
          'tr.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'ColaExaminacionNM',
          'ColaExaminacionNM.idTramite = tr.idTramite'
        )
        .innerJoinAndMapMany(
          'ColaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'ColaExaminacion',
          'ColaExaminacion.idColaExaminacion = ColaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne(
          'tr.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tr.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'tramitesClaseLicencia.idClaseLicencia = clasesLicencias.idClaseLicencia'
        );
      qbTramites.andWhere('tr.idSolicitud = ' + data.idSolicitud);

      const resultadoTramites: TramitesEntity[] = await qbTramites.getMany();

      // Variable para guardar los ids de las examinaciones
      let idsColaExaminacion: number[] = [];

      resultadoTramites.forEach((tramite, i) => {
        tramite.colaExaminacionNM.forEach((colaNM, j) => {
          idsColaExaminacion.push(colaNM.idColaExaminacion);
        });
      });

      // 3. Nos traemos los resultados de examinación asociados
      const qbResultadosExaminacion = this.colaExaminacionEntity
        .createQueryBuilder('ColaExaminacion')
        .innerJoinAndMapMany(
          'ColaExaminacion.TipoExaminacion',
          TipoExaminacionesEntity,
          'te',
          'ColaExaminacion.idTipoExaminacion = te.idTipoExaminacion'
        )
        .innerJoinAndMapMany(
          'ColaExaminacion.ResultadoExaminacionCola',
          ResultadoExaminacionColaEntity,
          'ResultadoExaminacionCola',
          'ColaExaminacion.idColaExaminacion = ResultadoExaminacionCola.idColaExaminacion'
        )
        .innerJoinAndMapMany(
          'ResultadoExaminacion.idResultadoExam',
          ResultadoExaminacionEntity,
          'ResultadoExaminacion',
          'ResultadoExaminacionCola.idResultadoExam = ResultadoExaminacion.idResultadoExam'
        );

      qbResultadosExaminacion
        .andWhere('ColaExaminacion.idColaExaminacion IN (:...idsColaExaminacion)', { idsColaExaminacion: idsColaExaminacion })
        .andWhere('ColaExaminacion.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: TIPOEXAMINACIONCOLA_IDONEIDADMORAL });

      const resultadoColaExaminacion: ColaExaminacionEntity[] = await qbResultadosExaminacion.getMany();

      if (resultadoColaExaminacion) {
        for (const ce of resultadoColaExaminacion) {
          var f = new Date();
          ce.idEstadosExaminacion = estadoNuevoExaminacionAprobReaprob.idEstadosExaminacion;
          ce.aprobado = data.aprobado;
          ce.historico = true;
          ce.updated_at = f;
        }

        await queryRunner.manager.save(ColaExaminacionEntity, resultadoColaExaminacion);
      }

      const idUsuario = (await this.authService.usuario()).idUsuario;

      //Guardamos los resultados de examinación asociados
      const resultadoResultadoExaminaciones: ResultadoExaminacionEntity[] = await this.resulexaminRespository
        .createQueryBuilder('ResultadoExaminacion')
        .innerJoinAndMapMany(
          'ResultadoExaminacion.resultadoExaminacionCola',
          ResultadoExaminacionColaEntity,
          'resultadoExaminacionCola',
          'ResultadoExaminacion.idResultadoExam = resultadoExaminacionCola.idResultadoExam'
        )
        .andWhere('resultadoExaminacionCola.idColaExaminacion IN (:...idsColaExaminacion)', {
          idsColaExaminacion: resultadoColaExaminacion.map(x => x.idColaExaminacion),
        })
        .getMany();

      if (resultadoResultadoExaminaciones) {
        resultadoResultadoExaminaciones.forEach(re => {
          re.aprobado = data.aprobado;
          re.fechaAprobacion = new Date();
          re.UsuarioAprobador = idUsuario;
          //re.UsuarioAprobador
        });
      }

      await queryRunner.manager.save(ResultadoExaminacionEntity, resultadoResultadoExaminaciones);

      if (data.aprobado == false)
        // En el caso de ser denegación, procedemos a colocar los trámites en espera de denegación
        for (let i = 0; i < resultadoTramites.length; i++) {
          queryRunner.manager.update(TramitesEntity, resultadoTramites[i].idTramite, {
            idEstadoTramite: 2, // Trámite en espera de denegación
          });
        }

      await queryRunner.commitTransaction();

      resultado.Mensaje = 'Se han guardado los resultados de examinación correctamente';
      resultado.ResultadoOperacion = true;
    } catch (Error) {
      resultado.Error = 'Se ha producido un error, no se pudo asignar los resultados de examinación 1';
      resultado.ResultadoOperacion = true;

      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    const preIdoneidad = await this.getHistoricoExamenesv2(data.idSolicitud);

    if (
      preIdoneidad.tramites[0].colaExaminacionNM[0].colaExaminacion[0].resultadoExaminacionCola[0].resultadoExaminacion[0].aprobado === null
    ) {
      const elemento: CreateResultadoexaminacionDto = {
        UsuarioAprobador: data.UsuarioAprobador,
        aprobado: data.aprobado,
        fechaAprobacion: new Date(),
        idSolicitud: data.idSolicitud,
        observaciones: '',
      };

      const preEvaluacion = await this.crearResExaminacionPreevMoralBySolicitudId(elemento);
    }

    return resultado;
  }

  async getHistoricoExamenesv2(idSolicitud?: number) {
    const qbSolicitudes = this.solicitudesRepository
      .createQueryBuilder('Solicitudes')
      .innerJoinAndMapMany('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
      .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
      .innerJoinAndMapOne(
        'tramites.tiposTramite',
        TiposTramiteEntity,
        'tiposTramite',
        'tramites.idTipoTramite = tiposTramite.idTipoTramite'
      )
      .innerJoinAndMapOne(
        'tramites.estadoTramite',
        EstadosTramiteEntity,
        'estadoTramite',
        'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
      )
      .innerJoinAndMapOne(
        'tramites.tramitesClaseLicencia',
        TramitesClaseLicencia,
        'tramitesClaseLicencia',
        'tramites.idTramite = tramitesClaseLicencia.idTramite'
      )
      .innerJoinAndMapOne(
        'tramitesClaseLicencia.clasesLicencias',
        ClasesLicenciasEntity,
        'clasesLicencias',
        'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
      )
      .innerJoinAndMapMany(
        'tramites.colaExaminacionNM',
        ColaExaminacionTramiteNMEntity,
        'colaExaminacionNM',
        'tramites.idTramite = colaExaminacionNM.idTramite'
      )
      .innerJoinAndMapMany(
        'colaExaminacionNM.colaExaminacion',
        ColaExaminacion,
        'colaExaminacion',
        'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
      )

      .innerJoinAndMapMany(
        'colaExaminacion.resultadoExaminacionCola',
        ResultadoExaminacionColaEntity,
        'resultadoExaminacionCola',
        'resultadoExaminacionCola.idColaExaminacion = colaExaminacion.idColaExaminacion'
      )

      .innerJoinAndMapMany(
        'resultadoExaminacionCola.resultadoExaminacion',
        ResultadoExaminacionEntity,
        'resultadoExaminacion',
        'resultadoExaminacion.idResultadoExam = resultadoExaminacionCola.idResultadoExam'
      )

      .innerJoinAndMapMany(
        'colaExaminacion.colaExaminacionNM',
        ColaExaminacionTramiteNMEntity,
        'colaExaminacionNMEstadoAlerta',
        'colaExaminacionNMEstadoAlerta.idTramite = colaExaminacion.idTramite'
      )
      .leftJoinAndMapMany(
        'colaExaminacionNMEstadoAlerta.colaExaminacionEstadoAlerta',
        ColaExaminacionEntity,
        'colaExaminacionEstadoAlerta',
        'colaExaminacionNMEstadoAlerta.idColaExaminacion = colaExaminacionEstadoAlerta.idColaExaminacion'
      )

      // Recogemos las solicitudes que no estén en estado borrador ni en estado cerrada
      .andWhere('(Solicitudes.idEstado != 1)')
      .andWhere('(Solicitudes.idEstado != 5)');

    qbSolicitudes.andWhere('colaExaminacion.idTipoExaminacion = :idTipoExaminacion', {
      idTipoExaminacion: 6,
    });

    // Filtrar por id solicitud
    if (idSolicitud)
      qbSolicitudes.andWhere('Solicitudes.idSolicitud = :_idSolicitud', {
        _idSolicitud: idSolicitud,
      });

    //const resultadoSolicitudes: Solicitudes[] = await qbSolicitudes.getMany();
    const resultadoSolicitudes: any = await qbSolicitudes.getOne();

    return resultadoSolicitudes;
  }
}
