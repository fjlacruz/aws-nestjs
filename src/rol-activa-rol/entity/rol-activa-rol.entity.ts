import { Roles } from "src/roles/entity/roles.entity";
import {Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne} from "typeorm";

@Entity('RolActivaRol')
export class RolActivaRol {

    @PrimaryGeneratedColumn()
    idRolActivaRol:number;

    @Column()
    idRolActivador:number;
    @JoinColumn({name: 'idRolActivador'})
    @ManyToOne(() => Roles, roles => roles.idRol)
    readonly rolActivador: Roles;

    @Column()
    idRolDestinoActivacion:number;
    @JoinColumn({name: 'idRolDestinoActivacion'})
    @ManyToOne(() => Roles, roles => roles.idRol)
    readonly rolDestinoActivacion: Roles;
}