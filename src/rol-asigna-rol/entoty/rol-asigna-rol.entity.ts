import { Roles } from "src/roles/entity/roles.entity";
import {Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne} from "typeorm";

@Entity('RolAsignaRol')
export class RolAsignaRol {

    @PrimaryGeneratedColumn()
    idRolAsignaRol:number;

    @Column()
    idRolAsignador:number;
    @JoinColumn({name: 'idRolAsignador'})
    @ManyToOne(() => Roles, roles => roles.idRol)
    readonly rolAsignador: Roles;

    @Column()
    idRolDestinoAsignacion:number;
    @JoinColumn({name: 'idRolDestinoAsignacion'})
    @ManyToOne(() => Roles, roles => roles.idRol)
    readonly rolDestinoAsignacion: Roles;
}