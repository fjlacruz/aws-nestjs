import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocsRolesUsuarioEntity } from "src/documentos-usuarios/entity/DocsRolesUsuario.entity";
import { RolesUsuarios } from "src/roles-usuarios/entity/roles-usuarios.entity";

export class RolPermisoDto {

    idRolPermiso: number;
    idRol: number;
    idPermiso: number;
}