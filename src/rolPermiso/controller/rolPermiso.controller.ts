import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RolPermisoService } from '../service/rolPermiso.service';

@ApiTags('Servicios-RolPermiso')
@Controller('rolPermiso')
export class RolPermisoController {

    constructor(private readonly rolPermisosService: RolPermisoService) { }
    @Get()
    async getRolPermisos() {
        const data = await this.rolPermisosService.getRolPermisos();
        return { data };
    }
}
