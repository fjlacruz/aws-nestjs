import { ApiProperty } from "@nestjs/swagger";
import { DocsRolesUsuarioEntity } from "src/documentos-usuarios/entity/DocsRolesUsuario.entity";
import { Permisos } from "src/permisos/entity/permisos.entity";
import { RolesUsuarios } from "src/roles-usuarios/entity/roles-usuarios.entity";
import { Roles } from "src/roles/entity/roles.entity";
import { InstitucionesEntity } from "src/tramites/entity/instituciones.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn, ManyToOne} from "typeorm";

@Entity('RolPermiso')
export class RolPermiso {

    @PrimaryGeneratedColumn()
    idRolPermiso: number;

    @Column()
    idRol: number;
    @JoinColumn({name: 'idRol'})
    @ManyToOne(() => Roles, roles => roles.rolPermiso)
    readonly roles: Roles;
    
    @Column()
    idPermiso: number;
    @JoinColumn({name: 'idPermiso'})
    @ManyToOne(() => Permisos, permiso => permiso.rolPermiso)
    readonly permiso: Permisos;
}