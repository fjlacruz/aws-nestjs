import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolPermisoController } from './controller/rolPermiso.controller';
import { RolPermiso } from './entity/rolPermiso.entity';
import { RolPermisoService } from './service/rolPermiso.service';

@Module({
  imports: [TypeOrmModule.forFeature([RolPermiso])],
  controllers: [RolPermisoController],
  providers: [RolPermisoService],
  exports: [RolPermisoService]
})
export class RolesModule {}
