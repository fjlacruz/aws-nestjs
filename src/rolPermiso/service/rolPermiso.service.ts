import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RolPermiso } from '../entity/rolPermiso.entity';

@Injectable()
export class RolPermisoService {

    constructor(@InjectRepository(RolPermiso) private readonly rolPermisosRespository: Repository<RolPermiso>) { }

    async getRolPermisos() {
        return await this.rolPermisosRespository.find();
    }
}
