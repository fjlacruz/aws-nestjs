import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber } from "class-validator";
export class RolTipoTramiteDTO {
  
    @ApiPropertyOptional()
    idRolTipoTramite: number;
    @ApiPropertyOptional()
    @IsNotEmpty()
    @IsNumber()
    idRol: number;
    @ApiPropertyOptional()
    idTipoTramite: number;
    @ApiPropertyOptional()
    idOficina: number;
}