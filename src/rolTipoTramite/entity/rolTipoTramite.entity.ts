import { Roles } from "src/roles/entity/roles.entity";
import { TiposTramiteEntity } from "src/tipos-tramites/entity/tipos-tramite.entity";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne} from "typeorm";

@Entity('RolTipoTramite')
export class RolTipoTramite {

    @PrimaryGeneratedColumn()
    idRolTipoTramite: number;

    @Column()
    idRol: number;
    @JoinColumn({ name: 'idRol' })
    @ManyToOne(() => Roles, roles => roles.rolTipoTramite)
    readonly roles: Roles[];

    @Column()
    idTipoTramite: number;
    @JoinColumn({ name: 'idTipoTramite' })
    @ManyToOne(() => TiposTramiteEntity, tiposTramite => tiposTramite.rolTipoTramite)
    readonly tiposTramite: TiposTramiteEntity[];

    @Column()
    idOficina: number;
    @JoinColumn({ name: 'idOficina' })
    @ManyToOne(() => OficinasEntity, oficinas => oficinas.rolTipoTramite)
    readonly oficinas: OficinasEntity[];

}
