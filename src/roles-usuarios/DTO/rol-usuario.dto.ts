export class RolUsuarioDTO {
    idRol: number;
    idUsuario: number;
    idOficina: number;
    idCalidadJuridica: number;
    idEstamento: number;
    idGrado: number;
    idRegion: number;
    activo: boolean;
}