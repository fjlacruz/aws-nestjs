export class RolesUsuariosDTO {
  idRol: number;
  nombre: string;
  descripcion: string;

  descripcionOficina?: string;
  descripcionRegion?: string;
  descripcionComuna?: string;
  descripcionInstitucion?: string;
  descripcionEstamento?: string;
  descripcionGrado?: string;
  descripcionCalidadJuridica?: string;
  descripcionActivo?: boolean;
  
  idInstitucion?:number;
}
