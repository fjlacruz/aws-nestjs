import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { RolesUsuarios } from '../entity/roles-usuarios.entity';
import { RolesUsuariosService } from '../services/roles-usuarios.service';

@ApiTags('Servicios-RolesUsuarios')
@Controller('roles-usuarios')
export class RolesUsuariosController {
  constructor(private readonly rolesUsuariosService: RolesUsuariosService, private readonly authService: AuthService) {}

  @Get()
  async getUsers() {
    const data = await this.rolesUsuariosService.getRolesUsuarios();
    return { data };
  }

  @Get('rolesByidUsuario/:idUsuario')
  @ApiOperation({ summary: 'Servicio que devuelve Roles por Usuario' })
  @ApiBearerAuth()
  async getRolesByidUsuario(@Param('idUsuario') idUsuario: number) {
    const data = await this.rolesUsuariosService.fetch(idUsuario);

    // Se pasa todo a dto para su tratamiento en front

    return { data };
  }

  @Get('infoRolesByidUsuario/:idUsuario')
  @ApiOperation({ summary: 'Servicio que devuelve Roles por Usuario' })
  @ApiBearerAuth()
  async getDatosRolesUsuario(@Param('idUsuario') idUsuario: number, @Req() req: any) {

      const data = await this.rolesUsuariosService.getDatosRolesUsuario(req, idUsuario);

      return { data };

  }

  @Get('getRolesByidUsuario/:idUsuario')
  @ApiOperation({ summary: 'Servicio que retorna los roles de un usuario por el idUsuario' })
  @ApiBearerAuth()
  async getRolesUsuarioByidUsuario(@Param('idUsuario') idUsuario: number) {
    const data = await this.rolesUsuariosService.getRolesUsuariosByIdUsuario(idUsuario);
    return { data };
  }

  @Get('getOficinasJpl/:idUsuario')
  @ApiOperation({ summary: 'Servicio que retorna los roles de un usuario por el idUsuario' })
  @ApiBearerAuth()
  async getOficinasJpl(@Param('idUsuario') idUsuario: number) {
    const data = await this.rolesUsuariosService.getOficinasJpl(idUsuario);
    return { data };
  }

  @Get('rolesByidUsuarioByRol/:idRol')
  @ApiOperation({ summary: 'Servicio que devuelve Roles por Usuario' })
  @ApiBearerAuth()
  async rolesByidUsuarioByRol(@Param('idRol') idRol: number) {
    const data = await this.rolesUsuariosService.rolesByidUsuarioByRol(idRol);
    return { data };
  }

  @Get('getRolesByIdUsuarioListado/:idUsuario')
  @ApiOperation({ summary: 'Servicio que devuelve Roles por Usuario' })
  @ApiBearerAuth()
  async getRolesByIdUsuarioListado(@Param('idUsuario') idUsuario: number) {
    const data = await this.rolesUsuariosService.getRolesByIdUsuarioListadoFetch(idUsuario);

    return { data };
  }

  @Post('activarRolUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que activa un RolUsuario' })
  async activarUsuario(@Body() rolesUsuarioDto: RolesUsuarios[], @Req() req: any) {
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const rolesUsuarios: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.rolesUsuariosService.activarRolUsuario(rolesUsuarioDto, rolesUsuarios);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }
}
