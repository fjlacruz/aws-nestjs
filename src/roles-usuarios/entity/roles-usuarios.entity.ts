import { CalidadJuridicaEntity } from 'src/calidad-juridica/entity/calidad-juridica.entity';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { EstamentoEntity } from 'src/estamento/entity/estamento.entity';
import { GradoEntity } from 'src/grados/entity/grado.entity';
import { Roles } from 'src/roles/entity/roles.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { User } from 'src/users/entity/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from 'typeorm';

@Entity('RolesUsuario')
export class RolesUsuarios {
  @PrimaryGeneratedColumn()
  idRolesUsuario: number;

  @Column()
  idRol: number;

  @JoinColumn({ name: 'idRol' })
  @ManyToOne(() => Roles, roles => roles.rolesUsuarios)
  roles: Roles;

  @Column()
  idUsuario: number;

  @JoinColumn({ name: 'idUsuario' })
  @ManyToOne(() => User, user => user.rolesUsuarios)
  readonly user: User;

  @Column()
  idOficina: number;

  @JoinColumn({ name: 'idOficina' })
  @OneToOne(() => OficinasEntity, oficinas => oficinas.rolesUsuarios)
  oficinas: OficinasEntity;

  @Column()
  idCalidadJuridica: number;

  @JoinColumn({ name: 'idCalidadJuridica' })
  @OneToOne(() => CalidadJuridicaEntity, calidad => calidad.idCalidadJuridica)
  calidadJuridica: CalidadJuridicaEntity;

  @Column()
  idEstamento: number;

  @JoinColumn({ name: 'idEstamento' })
  @OneToOne(() => EstamentoEntity, estamento => estamento.idEstamentos)
  estamento: EstamentoEntity;

  @Column()
  idGrado: number;

  @JoinColumn({ name: 'idGrado' })
  @OneToOne(() => GradoEntity, grado => grado.idGrado)
  grado: GradoEntity;

  @Column()
  idRegion: number;
  @JoinColumn({ name: 'idRegion' })
  @ManyToOne(() => RegionesEntity, regiones => regiones.rolesUsuarios)
  regiones: RegionesEntity;

  @Column()
  activo: boolean;
}
