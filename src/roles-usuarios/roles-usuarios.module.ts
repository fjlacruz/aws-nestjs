import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolActivaRol } from 'src/rol-activa-rol/entity/rol-activa-rol.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { User } from 'src/users/entity/user.entity';
import { UtilService } from 'src/utils/utils.service';
import { RolesUsuariosController } from './controller/roles-usuarios.controller';
import { RolesUsuarios } from './entity/roles-usuarios.entity';
import { RolesUsuariosService } from './services/roles-usuarios.service';
import { TipoInstitucionEntity } from '../tipo-institucion/entity/tipo-institucion.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, RolesUsuarios, OficinasEntity, RolActivaRol, TipoInstitucionEntity])],
  controllers: [RolesUsuariosController],
  providers: [RolesUsuariosService, AuthService, UtilService],
  exports: [RolesUsuariosService]
})
export class RolesUsuariosModule {}
