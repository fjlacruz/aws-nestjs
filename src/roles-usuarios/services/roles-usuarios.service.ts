import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Roles } from 'src/roles/entity/roles.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { User } from 'src/users/entity/user.entity';
import { Resultado } from 'src/utils/resultado';
import { Brackets, getConnection, In, Repository } from 'typeorm';
import { RolesUsuarios } from '../entity/roles-usuarios.entity';
import { RolesUsuariosDTO } from '../DTO/roles-usuarios.dto';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { RolActivaRol } from 'src/rol-activa-rol/entity/rol-activa-rol.entity';
import { correoUsuarioActivado, plantillaCorreo } from 'src/utils/cuerpoCorreos';
import { Email } from 'src/utils/Email';
import { asuntoUsuarioActivado, PermisosNombres, tipoRolSuperUsuarioID } from 'src/constantes';
import { UtilService } from 'src/utils/utils.service';
import { TipoInstitucionEntity } from '../../tipo-institucion/entity/tipo-institucion.entity';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class RolesUsuariosService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(RolesUsuarios) private readonly rolesUsuariosRespository: Repository<RolesUsuarios>,
    @InjectRepository(OficinasEntity) private readonly oficinasRepository: Repository<OficinasEntity>,
    @InjectRepository(RolActivaRol) private readonly roleActivaRolRepository: Repository<RolActivaRol>,
    @InjectRepository(TipoInstitucionEntity) private readonly tipoInstitucionEntityRepository: Repository<TipoInstitucionEntity>,
    private readonly utilService: UtilService,
    private readonly authService: AuthService
  ) {}

  async getRolesUsuariosByIdUsuario(idUsuario: number): Promise<RolesUsuarios[]> {
    return await this.rolesUsuariosRespository.find({
      relations: ['roles'],
      where: {
        idUsuario: idUsuario,
      },
    });
  }

  async getOficinasJpl(idUsuario: number): Promise<RolesUsuarios[]> {
    const data = await this.rolesUsuariosRespository.find({
      relations: ['roles', 'oficinas', 'oficinas.instituciones'],
      where: {
        idUsuario: idUsuario,
      },
    });
    const result: any[] = [];
    for (const oficina of data) {
      const idInstitucion = oficina.oficinas.instituciones.idTipoInstitucion;
      let tipoInst: TipoInstitucionEntity = await this.tipoInstitucionEntityRepository.findOne({ idTipoInstitucion: idInstitucion });
      oficina['tipoInstitucion'] = tipoInst;
      if (tipoInst.idTipoInstitucion === 2) {
        result.push(oficina);
      }
    }
    return result;
  }

  async getRolesUsuarios() {
    return await this.rolesUsuariosRespository.find();
  }

  async getRolesByIdUsuarioListadoFetch(idUsuario: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let resultadoRoles: RolesUsuarios[] = [];
    let resultadosDTO: RolesUsuariosDTO[] = [];

    try {
      resultadoRoles = await this.rolesUsuariosRespository
        .createQueryBuilder('RolesUsuarios')
        .innerJoinAndMapOne('RolesUsuarios.idRol', Roles, 'rol', 'RolesUsuarios.idRol = rol.idRol')
        .innerJoinAndMapOne('RolesUsuarios.idInstitucion', InstitucionesEntity, 'inst', 'RolesUsuarios.idInstitucion = inst.idInstitucion')
        .where({ idUsuario })
        .getMany();

      if (Array.isArray(resultadoRoles) && resultadoRoles.length) {
        resultadoRoles.forEach(item => {
          let nuevoElemento: RolesUsuariosDTO = {
            idRol: item.roles.idRol,
            nombre: item.roles.Nombre,
            descripcion: item.roles.Descripcion,
          };

          resultadosDTO.push(nuevoElemento);
        });
      }

      if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
        resultado.Respuesta = resultadosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Roles de usuario obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Roles de usuario';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Roles de usuario';
    }

    return resultado;
  }

  async getRolesByIdUsuario(idUsuario: number): Promise<RolesUsuarios> {
    return await this.rolesUsuariosRespository.findOne({ where: { idUsuario } });
  }

  async fetch(idUsuario: number): Promise<any> {
    const relations = ['roles'];
    const roles = await this.rolesUsuariosRespository
      .createQueryBuilder('rolUsu')
      .innerJoinAndSelect('rolUsu.estamento', 'estamento')
      .innerJoinAndSelect('rolUsu.grado', 'grado')
      .innerJoinAndSelect('rolUsu.calidadJuridica', 'calidad')
      .innerJoinAndSelect('rolUsu.roles', 'roles')
      .innerJoinAndSelect('rolUsu.user', 'usuario')
      .innerJoinAndSelect('rolUsu.oficinas', 'oficinas')
      .where({ idUsuario })
      .getMany();

    return roles;

    // return this.rolesUsuariosRespository.createQueryBuilder('RolesUsuarios')
    //   .innerJoinAndMapOne('RolesUsuarios.idUsuario', User, 'user', 'RolesUsuarios.idUsuario = user.idUsuario',)
    //     .innerJoinAndMapOne('RolesUsuarios.idRol', Roles, 'rol', 'RolesUsuarios.idRol = rol.idRol',)
    //     .innerJoinAndMapOne('RolesUsuarios.idInstitucion', InstitucionesEntity, 'inst', 'RolesUsuarios.idInstitucion = inst.idInstitucion')
    //     .where({ idUsuario })
    //     .getMany();
  }

  async getDatosRolesUsuario(req:any, idUsuario: number): Promise<Resultado> {
    let res: Resultado = new Resultado();

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AccesoDatosPersonalesDelUsuario);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      const relations = ['roles'];
      const roles = await this.rolesUsuariosRespository
        .createQueryBuilder('rolUsu')
        .innerJoinAndSelect('rolUsu.estamento', 'estamento')
        .innerJoinAndSelect('rolUsu.grado', 'grado')
        .innerJoinAndSelect('rolUsu.calidadJuridica', 'calidad')
        .innerJoinAndSelect('rolUsu.roles', 'roles')
        .innerJoinAndSelect('rolUsu.user', 'usuario')
        .leftJoinAndSelect('rolUsu.oficinas', 'oficinas')
        .leftJoinAndSelect('oficinas.instituciones', 'instituciones')
        .leftJoinAndSelect('instituciones.comunas', 'comunas')
        .leftJoinAndSelect('rolUsu.regiones', 'regiones')
        .where({ idUsuario })
        .getMany();

      // Se pasa todo a dto para su tratamiento en front
      let rolesUsuariosDto: RolesUsuariosDTO[] = [];

      roles.forEach(rol => {
        let rolUsuariosDto = new RolesUsuariosDTO();

        rolUsuariosDto.idRol = rol.idRol;
        rolUsuariosDto.nombre = rol.roles.Nombre;
        rolUsuariosDto.descripcion = rol.roles.Descripcion;
        rolUsuariosDto.descripcionEstamento = rol.calidadJuridica ? rol.calidadJuridica.nombre : '';
        rolUsuariosDto.descripcionGrado = rol.estamento ? rol.estamento.nombre : '';
        rolUsuariosDto.descripcionCalidadJuridica = rol.calidadJuridica ? rol.calidadJuridica.nombre : '';
        rolUsuariosDto.descripcionActivo = rol.activo;
        rolUsuariosDto.idInstitucion = (rol && rol.oficinas && rol.oficinas.instituciones)?rol.oficinas.instituciones.idInstitucion:null;

        if (rol.oficinas) {
          rolUsuariosDto.descripcionOficina = rol.oficinas.Nombre;
          if (rol.oficinas.instituciones.comunas != null) {
            rolUsuariosDto.descripcionComuna = rol.oficinas.instituciones.comunas.Nombre;
          } else {
            rolUsuariosDto.descripcionInstitucion = rol.oficinas.instituciones.Nombre;
          }
        } else if (rol.regiones) {
          rolUsuariosDto.descripcionRegion = rol.regiones.Nombre;
        }

        rolesUsuariosDto.push(rolUsuariosDto);
      });

      res.Respuesta = rolesUsuariosDto;
      res.ResultadoOperacion = true;

      return res;
    } catch (Error) {
      res.Respuesta = null;
      res.ResultadoOperacion = false;
    }

    // return this.rolesUsuariosRespository.createQueryBuilder('RolesUsuarios')
    //   .innerJoinAndMapOne('RolesUsuarios.idUsuario', User, 'user', 'RolesUsuarios.idUsuario = user.idUsuario',)
    //     .innerJoinAndMapOne('RolesUsuarios.idRol', Roles, 'rol', 'RolesUsuarios.idRol = rol.idRol',)
    //     .innerJoinAndMapOne('RolesUsuarios.idInstitucion', InstitucionesEntity, 'inst', 'RolesUsuarios.idInstitucion = inst.idInstitucion')
    //     .where({ idUsuario })
    //     .getMany();
  }

  public rolesByidUsuarioByRol(idRol: number): Promise<any> {
    return this.rolesUsuariosRespository
      .createQueryBuilder('RolesUsuarios')
      .innerJoinAndMapOne('RolesUsuarios.idUsuario', User, 'rol', 'RolesUsuarios.idUsuario = rol.idUsuario')
      .where({ idRol })
      .getMany();
  }

  public async activarRolUsuario(rolesUsuarioDto: RolesUsuarios[], rolesUsuarioConectado: RolesUsuarios[]) {
    const resultado: Resultado = new Resultado();

    let usuarioConectado = new User();

    // Se obtiene el usuario conectado
    usuarioConectado = await this.usersRepository.findOne({ idUsuario: rolesUsuarioConectado[0].idUsuario });

    if (usuarioConectado == undefined) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error: obteniendo usuario conectado';
      return resultado;
    }

    // Se obtiene conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      let idsRolesUsuarioConectado: number[] = [];
      let rolesActivables: number[] = [];
      let esSuperUsuario: boolean = false;

      // Obtenemos los ids de los Roles del UsuarioConectado
      for (const rolUsuario of rolesUsuarioConectado) {
        if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID) {
          esSuperUsuario = true;
        }
        idsRolesUsuarioConectado.push(rolUsuario.idRol);
      }

      // Traemos los Roles que puede editar el UsuarioConectado
      rolesActivables = await (
        await this.roleActivaRolRepository.find({ idRolActivador: In(idsRolesUsuarioConectado) })
      ).map(x => x.idRolDestinoActivacion);
      if (rolesActivables.length <= 0 && !esSuperUsuario) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error: No tienes permiso para realizar esta operacion';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      const user = await this.usersRepository.findOne({ where: { idUsuario: rolesUsuarioDto[0].idUsuario } });

      if (user == undefined) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error: no se encontro el usuario';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      let listaRolesDestinatarios: RolesUsuarios[] = [];

      for (const rolUser of rolesUsuarioDto) {
        if (!rolesActivables.includes(rolUser.idRol)) {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'Error: No tienes permiso para realizar esta operacion';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }

        let idRegion;
        //Cogemos id de la region el rol para comprobar si tenemos permiso para activar este Rol
        if (rolUser.idRegion == undefined) {
          rolUser.regiones = await (
            await this.oficinasRepository.findOne({
              relations: ['comunas', 'comunas.regiones'],
              where: { idOficina: rolUser.idOficina },
            })
          ).comunas.regiones;

          idRegion = rolUser.regiones.idRegion;
        } else {
          idRegion = rolUser.idRegion;
        }

        let resultadoOficinaRegion = rolesUsuarioConectado.find(x => x.idOficina == rolUser.idOficina || x.idRegion == idRegion);

        //Si no tenemos permiso para activar este Rol, terminamos el metodo
        if (resultadoOficinaRegion == undefined) {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'Error: no tiene permisos para activar los roles de este usuario';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }

        const resultadoUpdateRol = await queryRunner.manager.update(RolesUsuarios, rolUser.idRolesUsuario, { activo: true });

        if (resultadoUpdateRol.affected == 0) {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'Error: no se pudo activar el rol';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }

        if (user.Activo != true) {
          const resultadoUpdateUser = await queryRunner.manager.update(User, user.idUsuario, { Activo: true });

          if (resultadoUpdateUser.affected == 0) {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error: no se pudo activar el usuario';
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            return resultado;
          }

          user.Activo = true;
        }

        // Se envia correo a los Seremitt
        const usuariosDestinatarios = await this.recogerDestinatariosCorreo(rolUser);

        if (usuariosDestinatarios.ResultadoOperacion) {
          const rolesDestinatarios: RolesUsuarios[] = usuariosDestinatarios.Respuesta as RolesUsuarios[];
          rolesDestinatarios.forEach(rolDestinatario => {
            if (!listaRolesDestinatarios.find(x => x.idUsuario == rolDestinatario.idUsuario)) listaRolesDestinatarios.push(rolDestinatario);
          });
        }
      }

      // Se preparan datos para el correo y se envia
      const userName = user.RUN + '-' + user.DV + ', ' + user.Nombres + ' ' + user.ApellidoPaterno + ' ' + user.ApellidoMaterno;
      const seremittName = usuarioConectado.Nombres + ' ' + usuarioConectado.ApellidoPaterno + ' ' + usuarioConectado.ApellidoMaterno;
      const respuestaCorreo = await this.envioConfirmacionActivacion(listaRolesDestinatarios, userName, seremittName);

      resultado.Mensaje = 'Usuario y roles activados correctamente';
      resultado.ResultadoOperacion = true;
      await queryRunner.commitTransaction();
    } catch (error) {
      console.log(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error activando el rol';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  public async recogerDestinatariosCorreo(rolUsuario: RolesUsuarios): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let idRegion;
    let tipoOficina = true;

    // Si el rolUsuario no tiene idRegion, lo cogemos de la region
    if (rolUsuario.idRegion == undefined) {
      idRegion = rolUsuario.regiones.idRegion;
    } else {
      tipoOficina = false;
      idRegion = rolUsuario.idRegion;
    }

    const rolesDestinatarios = await this.rolesUsuariosRespository.find({
      relations: [
        'user',
        'roles',
        'roles.tipoRoles',
        'roles.rolPermiso',
        'roles.rolPermiso.permiso',
        'roles.grupoPermisosRoles',
        'roles.grupoPermisosRoles.grupoPermisos',
        'roles.grupoPermisosRoles.grupoPermisos.grupoPermisoXPermisos',
        'roles.grupoPermisosRoles.grupoPermisos.grupoPermisoXPermisos.permisos',
      ],
      where: qb => {
        qb.where(
          new Brackets(subQb => {
            subQb
              .where('RolesUsuarios__roles__rolPermiso__permiso.Codigo = :CodigoPermiso', {
                CodigoPermiso: PermisosNombres.AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario,
              })
              .orWhere('RoUs__role__grPeRo__grPe__grPeXP__permisos.Codigo = :CodigoPermiso', {
                CodigoPermiso: PermisosNombres.AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario,
              });
          })
        )
          .andWhere('RolesUsuarios.activo = true')
          .andWhere(
            tipoOficina
              ? 'RolesUsuarios__roles__tipoRoles.Nombre = :TipoOficina AND RolesUsuarios.idOficina = :IdOficina '
              : 'RolesUsuarios__roles__tipoRoles.Nombre = :TipoRegion AND RolesUsuarios.idRegion = :IdRegion ',
            { TipoOficina: 'Oficina', IdOficina: rolUsuario.idOficina, TipoRegion: 'Region', IdRegion: idRegion }
          );
      },
    });

    resultado.ResultadoOperacion = true;
    resultado.Respuesta = rolesDestinatarios;

    return resultado;
  }

  async envioConfirmacionActivacion(rolesDestinatarios: RolesUsuarios[], userName: string, seremittName: string) {
    let idDestinatario = 0;
    let Zona = '';

    for (let i = 0; i < rolesDestinatarios.length; i++) {
      //  Comprobacion de si el activador es el mismo o no en cada bucle, para juntar las peticiones por activadores
      if (rolesDestinatarios[i].idUsuario != idDestinatario) {
        
        let cuerpoCorreo = correoUsuarioActivado.replace('userName', userName);
        cuerpoCorreo = cuerpoCorreo.replace(
          'DestinatarioName',
          rolesDestinatarios[i].user.Nombres +
            ' ' +
            rolesDestinatarios[i].user.ApellidoPaterno +
            ' ' +
            rolesDestinatarios[i].user.ApellidoMaterno
        );

        //cuerpoCorreo = cuerpoCorreo.replace('SeremittName', seremittName);
        // cuerpoCorreo = cuerpoCorreo.replace('CargoName', rolesDestinatarios[i].roles.Nombre);
        // cuerpoCorreo = cuerpoCorreo.replace('ZonaName', Zona);

        //cuerpoCorreo = plantillaCorreo.replace('cuerpoCorreo', cuerpoCorreo);

        const destinatario = 'ximena.araya@babelgroup.com'; //rolesDestinatarios[i].user.email;

        // Se llama al servicio que envia el correo
        await this.utilService.prepararObjetoCorreo(destinatario, asuntoUsuarioActivado, cuerpoCorreo, '', '','','');
      }

      idDestinatario = rolesDestinatarios[i].idUsuario;
    }
  }
}
