import { ApiPropertyOptional } from "@nestjs/swagger";

export class CrearRolesDTO {

    @ApiPropertyOptional()
    idRol: number;
    @ApiPropertyOptional()
    nombreRol: string;
    @ApiPropertyOptional()
    descripcion: string;
    @ApiPropertyOptional()
    idGrupoPermisos: number[];
    @ApiPropertyOptional()
    idPermisos: number[];
    @ApiPropertyOptional()
    idDocumentos: number[];
    @ApiPropertyOptional()
    idTipoRol: number;
    @ApiPropertyOptional()
    activo: boolean;
}