import { ApiPropertyOptional } from "@nestjs/swagger";

export class ListaOficinasRegionesDTO{

    @ApiPropertyOptional()
    idOficina?:number;
    @ApiPropertyOptional()
    idRegion?:number;
    @ApiPropertyOptional()
    Nombre:string;
}