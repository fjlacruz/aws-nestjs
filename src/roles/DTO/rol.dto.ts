import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocsRolesUsuarioEntity } from "src/documentos-usuarios/entity/DocsRolesUsuario.entity";
import { RolesUsuarios } from "src/roles-usuarios/entity/roles-usuarios.entity";

export class RolDto {

    @ApiPropertyOptional()
    idRol: number;
    
    @ApiPropertyOptional()
    idInstitucion: number;
    
    @ApiPropertyOptional()
    Nombre: string;

    @ApiPropertyOptional()
    Descripcion: string;

    @ApiPropertyOptional()
    rolesUsuarios: RolesUsuarios;
    
    @ApiPropertyOptional()
    docsRolesUsuario: DocsRolesUsuarioEntity;
}