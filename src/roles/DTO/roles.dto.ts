export class RolesDto {
    idRol: number;
    Nombre: string;
    Descripcion: string;
    tipoRol: number;
    activo: boolean
}