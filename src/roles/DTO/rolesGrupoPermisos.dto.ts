export class RolesGrupoPermisosDto {
    idRol: number;
    nombreRol: string;
    descripcion: string;
    grupoPermisos: string[];
    documentos: number;
    creadoPor: string;
    idCreadoPor?: Number;
    asignado: boolean;
    activo: boolean;
}