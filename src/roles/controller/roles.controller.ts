import { Body, Controller, Get, Post, Param, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { CrearRolesDTO } from '../DTO/crear-roles.dto';
import { RolesService } from '../service/roles.service';

@ApiTags('Servicios-Roles')
@Controller('roles')
export class RolesController {

    constructor(private readonly rolesService: RolesService,
                private readonly authService: AuthService
    ) { }
    
    @Get('all')
    async getUsers() {
        const data = await this.rolesService.getRoles();
        return { data };
    }

    @Post('getRolesPaginados')
    @ApiBearerAuth()
    async getRolesPaginados(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {

        const data = await this.rolesService.getRolesPaginados(req, paginacionArgs);
        return { data };


    }

    @Get('getRolesListado')
    @ApiBearerAuth()
    async getRolesListado(@Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaIncripcionAdministracionUsuarios
        ]);
        
        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion) {

            const rolesUsuarios : RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];
            
            const data = await this.rolesService.getRolesListado(rolesUsuarios);
            return { data };

        } else {
            return { data: validarPermisos };
        }

    }

    
    @Get('getRolesListaNombres')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos las roles, lista de id y nombre' })
    async getRolesListaNombres(@Req() req: any) {
        // let splittedBearerToken = req.headers.authorization.split(" ");
        // let token = splittedBearerToken[1];

        // let tokenPermiso = new TokenPermisoDto(token, [
        //     PermisosNombres.VisualizarTiposTramitesLicencias
        // ]);
        
        // let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        // if (validarPermisos.ResultadoOperacion) {
            
            const data = await this.rolesService.getRolesListaNombres();
            return { data };

        // } else {
        //     return { data: validarPermisos };
        // }

    }

    @Get('getRolDetalle/:idRol')
    @ApiBearerAuth()
    async getRolDetalle(@Param('idRol') idRol: number, @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaAdminitracionRolesYPermisos
        ]);
        
        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion) {
            
            const data = await this.rolesService.getRolDetalle(idRol);
            return { data };

        } else {
            return { data: validarPermisos };
        }

    }

    
    @Get('eliminarRol/:idRol')
    @ApiBearerAuth()
    async eliminarRol(@Param('idRol') idRol: number, @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones
        ]);
        
        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion) {
            
            const data = await this.rolesService.eliminarRol(idRol);
            return { data };

        } else {
            return { data: validarPermisos };
        }

    }

    @Post('editarRol')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que edita un Rol' })
    async editarRol(@Body() rolDto: CrearRolesDTO,@Req() req: any) {
        
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones
        ]);
        
        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.rolesService.editarRol(rolDto);        
            return { data };

        } else {
            return { data: validarPermisos };
        }
    }

    @Get('getTipoRoles')
    @ApiBearerAuth()
    async getTipoRoles(@Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosCrearRolesGruposdePermisosTipodeDocumentos
        ]);
        
        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion) {

            const rolesUsuarios : RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

            const data = await this.rolesService.getTipoRoles(rolesUsuarios);
            return { data };

        } else {
            return { data: validarPermisos };
        }

    }

    @Get('getOficinasRegionesByRol/:idRol')
    @ApiBearerAuth()
    async getOficinasRegionesByRol(@Param('idRol') idRol: number, @Req() req: any) {
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.AdministracionSGLCAdministraciondeUsuariosCrearNuevoUsuario
        ]);
        
        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion) {

            const rolesUsuarios : RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];
            
            const data = await this.rolesService.getOficinasRegionesByRol(idRol, rolesUsuarios);
            return { data };

        } else {
            return { data: validarPermisos };
        }

    }

    @Post('crearRol')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un Rol' })
    async crearRol(@Body() rolDto: CrearRolesDTO,@Req() req: any) {
        
        // let splittedBearerToken = req.headers.authorization.split(" ");
        // let token = splittedBearerToken[1];

        // let tokenPermiso = new TokenPermisoDto(token, [
        //     PermisosNombres.CrearNuevosRoles
        // ]);
        
        // let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        // if (validarPermisos.ResultadoOperacion)
        // {
        //     const idUsuario = (await this.authService.usuario()).idUsuario;

            // let rolesUsuarioConectado : RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

            const data = await this.rolesService.crearRol(req, rolDto);        
            return { data };

        // } else {
        //     return { data: validarPermisos };
        // }
    }

    
}
