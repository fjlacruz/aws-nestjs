import { ApiProperty } from '@nestjs/swagger';
import { DocRequeridosRoles } from 'src/docsRequeridoRol/entity/docRequeridosRoles.entity';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';
import { GrupoPermisosRoles } from 'src/grupo-permisos/entity/grupo-permisos-roles.entity';
import { RegistroUsuarios } from 'src/registro-usuarios/entity/registro-usuario.entity';
import { RolActivaRol } from 'src/rol-activa-rol/entity/rol-activa-rol.entity';
import { RolAsignaRol } from 'src/rol-asigna-rol/entoty/rol-asigna-rol.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { RolPermiso } from 'src/rolPermiso/entity/rolPermiso.entity';
import { RolTipoTramite } from 'src/rolTipoTramite/entity/rolTipoTramite.entity';
import { TipoRol } from 'src/tipo-rol/entity/tipo-rol.entity';
import { User } from 'src/users/entity/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne } from 'typeorm';

@Entity('Roles')
export class Roles {
  @PrimaryGeneratedColumn()
  idRol: number;

  @Column()
  Nombre: string;

  @Column()
  Descripcion: string;

  @Column()
  debeAceptarCondiciones: boolean;

  @Column()
  idUsuario: number;
  @JoinColumn({ name: 'idUsuario' })
  @ManyToOne(() => User, user => user.roles)
  readonly usuario: User;

  @Column()
  FechaCreacion: Date;

  @Column()
  tipoRol: number;
  @JoinColumn({ name: 'tipoRol' })
  @ManyToOne(() => TipoRol, tipoRol => tipoRol.rol)
  readonly tipoRoles: TipoRol;

  @Column()
  activo: boolean;

  @ApiProperty()
  @OneToMany(() => GrupoPermisosRoles, grupoPermisosRoles => grupoPermisosRoles.roles)
  readonly grupoPermisosRoles: GrupoPermisosRoles[];

  @ApiProperty()
  @OneToMany(() => RolPermiso, rolPermiso => rolPermiso.roles)
  readonly rolPermiso: RolPermiso[];

  @ApiProperty()
  @OneToMany(() => RolesUsuarios, rolesUsuarios => rolesUsuarios.roles)
  readonly rolesUsuarios: RolesUsuarios[];

  @ApiProperty()
  @OneToMany(() => DocsRolesUsuarioEntity, docsRolesUsuario => docsRolesUsuario.roles)
  readonly docsRolesUsuario: DocsRolesUsuarioEntity[];

  @ApiProperty()
  @OneToMany(() => RegistroUsuarios, registroUsuarios => registroUsuarios.roles)
  readonly registroUsuarios: RegistroUsuarios[];

  @ApiProperty()
  @OneToMany(() => RolActivaRol, rolActivaRol => rolActivaRol.rolActivador)
  rolesActivador: RolActivaRol[];

  @ApiProperty()
  @OneToMany(() => RolActivaRol, rolActivaRol => rolActivaRol.rolDestinoActivacion)
  readonly rolesDestinoActivacion: RolActivaRol[];

  @ApiProperty()
  @OneToMany(() => RolAsignaRol, rolAsignaRol => rolAsignaRol.rolAsignador)
  rolesAsignador: RolAsignaRol[];

  @ApiProperty()
  @OneToMany(() => RolAsignaRol, rolAsignaRol => rolAsignaRol.rolDestinoAsignacion)
  readonly rolesDestinoAsignacion: RolAsignaRol[];

  @ApiProperty()
  @OneToMany(() => DocRequeridosRoles, docRequeridosRoles => docRequeridosRoles.roles)
  readonly docRequeridosRoles: DocRequeridosRoles[];

  @ApiProperty()
  @OneToMany(() => RolTipoTramite, rolTipoTramite => rolTipoTramite.roles)
  readonly rolTipoTramite: RolTipoTramite[];

  @Column()
  readonly recibeNotificacionActivacionUsuario: boolean;  
}
