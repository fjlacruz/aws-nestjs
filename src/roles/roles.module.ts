import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { TipoRol } from 'src/tipo-rol/entity/tipo-rol.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { jwtConstants } from 'src/users/constants';
import { User } from 'src/users/entity/user.entity';
import { RolesController } from './controller/roles.controller';
import { Roles } from './entity/roles.entity';
import { RolesService } from './service/roles.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, Roles, RolesUsuarios, TipoRol, RegionesEntity, OficinasEntity])],
  controllers: [RolesController],
  providers: [RolesService, AuthService],
  exports: [RolesService]
})
export class RolesModule {}
