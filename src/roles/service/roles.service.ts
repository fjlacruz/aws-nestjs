import { GrupoPermisos } from './../../grupo-permisos/entity/grupo-permisos.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { getConnection, In, Repository } from 'typeorm';
import { Roles } from '../entity/roles.entity';
import { RolesDto } from '../DTO/roles.dto';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { ColumnasRoles, PermisosNombres, tipoRolGeneralID, tipoRolOficinaID, tipoRolRegionID, tipoRolSuperUsuarioID } from 'src/constantes';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { RolesGrupoPermisosDto } from '../DTO/rolesGrupoPermisos.dto';
import { CrearRolesDTO } from '../DTO/crear-roles.dto';
import { RolPermiso } from 'src/rolPermiso/entity/rolPermiso.entity';
import { GrupoPermisosRoles } from 'src/grupo-permisos/entity/grupo-permisos-roles.entity';
import { DocsRolesUsuario } from 'src/docsRequeridoRol/entity/DocsRolesUsuario.entity';
import { DocRequeridosRoles } from 'src/docsRequeridoRol/entity/docRequeridosRoles.entity';
import { TipoRol } from 'src/tipo-rol/entity/tipo-rol.entity';
import { TipoRolDto } from 'src/tipo-rol/DTO/tipo-rol.dto';
import { tipoRolOficina, tipoRolRegion } from 'src/constantes';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { ListaOficinasRegionesDTO } from '../DTO/listaOficinasRegiones.dto';
import { RolActivaRol } from 'src/rol-activa-rol/entity/rol-activa-rol.entity';
import { RolAsignaRol } from 'src/rol-asigna-rol/entoty/rol-asigna-rol.entity';
import { RegistroUsuarios } from 'src/registro-usuarios/entity/registro-usuario.entity';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { DocsRequeridoRol } from 'src/docsRequeridoRol/entity/docsRequeridoRol.entity';
import { ObjetoListado } from 'src/utils/objetoListado';
import { AuthService } from 'src/auth/auth.service';
import { DocumentosColaExamPracticosModule } from 'src/documentos-cola-exam-practicos/documentos-cola-exam-practicos.module';
import { User } from 'src/users/entity/user.entity';

@Injectable()
export class RolesService {

    constructor(@InjectRepository(Roles) private readonly rolesRespository: Repository<Roles>,
                @InjectRepository(TipoRol) private readonly tipoRolRespository: Repository<TipoRol>,             
                @InjectRepository(RegionesEntity) private readonly regionesRespository: Repository<RegionesEntity>,
                @InjectRepository(OficinasEntity) private readonly oficinasRespository: Repository<OficinasEntity>,
                private readonly authService: AuthService) { }

    async getRoles() {
        return await this.rolesRespository.find();
    }

    async getRolesPaginados(req:any, paginacionArgs: PaginacionArgs): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        const orderBy : string = ColumnasRoles[(paginacionArgs.orden.orden)];
        let docsRequeridosPaginados: Pagination<Roles>;
        let docsRequeridosParseados = new PaginacionDtoParser<RolesGrupoPermisosDto>();

        let resultadosDTO: RolesGrupoPermisosDto[] = [];

        let filtro = null

        try {

            // Comprobamos permisos	
            let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaAdminitracionRolesYPermisos);	
                
            // En caso de que el usuario no sea correcto	
            if (!usuarioValidado.ResultadoOperacion) {	
                return usuarioValidado;	
            }	


            if (paginacionArgs.filtro != null && paginacionArgs.filtro != '') {
                filtro = paginacionArgs.filtro
            }
            
            docsRequeridosPaginados = await paginate<Roles>(this.rolesRespository, paginacionArgs.paginationOptions, {
                relations: ['grupoPermisosRoles', 
                            'grupoPermisosRoles.grupoPermisos', 
                            'docRequeridosRoles', 
                            'rolesUsuarios', 
                            'usuario'],
                where: qb => {
                    qb.where()
                    if(filtro?.grupoPermisos != null && filtro.grupoPermisos != '' && filtro.grupoPermisos != 'null' ){
                        qb.andWhere(
                            'Roles__grupoPermisosRoles__grupoPermisos.idGrupoPermiso = :grupo', {grupo: Number(filtro.grupoPermisos)}
                    )}
                    if(filtro?.nombre != null && filtro.nombre != '' && filtro.nombre != 'null' ){
                    qb.andWhere(
                        'lower(unaccent(Roles.Nombre)) like lower(unaccent(:Nombre))', {Nombre: `%${filtro.nombre}%`}
                    )}
                    if(filtro?.creadoPor != null && filtro.creadoPor != '' && filtro.creadoPor != 'null'){
                    qb.andWhere(
                        '(lower(unaccent("Roles__usuario"."Nombres")) like lower(unaccent(:creadoPor))' +
                        ' or lower(unaccent("Roles__usuario"."ApellidoPaterno")) like lower(unaccent(:creadoPor))' +
                        ' or lower(unaccent("Roles__usuario"."ApellidoMaterno")) like lower(unaccent(:creadoPor)))', {creadoPor: `%${filtro.creadoPor}%`}
                    )}
                    if(filtro?.descripcion != null && filtro.descripcion != '' && filtro.descripcion != 'null' ){
                    qb.andWhere(
                        'lower(unaccent(Roles.Descripcion)) like lower(unaccent(:Descripcion))', {Descripcion: `%${filtro.descripcion}%`}
                    )}
                    if(filtro?.estado != null && filtro.estado != ''){
                        if(filtro.estado == 'null') {
                            qb.andWhere('Roles.activo IS NULL')
                        } 
                        else if(filtro.estado == 'true'){
                            qb.andWhere('Roles.activo = true')
                        } 
                        else if(filtro.estado == 'false'){
                            qb.andWhere('Roles.activo = false')
                        }     
                    }
                    if(filtro?.documentos != null && filtro.documentos != '' && filtro.documentos != 'null' ){
                        if(filtro?.documentos == '1' || filtro?.documentos == '2' || filtro?.documentos == '3'){
                        
                            qb.andWhere(
                            'Roles.idRol IN (SELECT "idRol" FROM public."DocRequeridosRoles" GROUP BY "idRol" HAVING COUNT(*) = :numeroDocumentos)', { numeroDocumentos : Number(filtro.documentos) })
                        }
                        else if(filtro?.documentos == '4'){
                            qb.andWhere(
                                'Roles.idRol IN (SELECT "idRol" FROM public."DocRequeridosRoles" GROUP BY "idRol" HAVING COUNT(*) >= :numeroDocumentos)', { numeroDocumentos : Number(filtro.documentos) })                            
                        }
                    }

                    qb.orderBy(orderBy, paginacionArgs.orden.ordenarPor) 
                }
            });

            // Si hay resultado se mapean a DTO
            if (Array.isArray(docsRequeridosPaginados.items) && docsRequeridosPaginados.items.length) {

                docsRequeridosPaginados.items.forEach(rol => {
                    let nuevoElemento: RolesGrupoPermisosDto = {
                        idRol: rol.idRol,
                        nombreRol: rol.Nombre,
                        descripcion: rol.Descripcion,
                        grupoPermisos: [],
                        documentos: rol.docRequeridosRoles.length,
                        creadoPor: rol.usuario.Nombres + ' ' + rol.usuario.ApellidoPaterno + ' ' +rol.usuario.ApellidoMaterno,
                        idCreadoPor: rol.usuario.idUsuario,
                        asignado: rol.rolesUsuarios.length > 0? true : false,
                        activo: rol.activo
                    }

                    rol.grupoPermisosRoles.forEach(grupoPermisos => {
                        nuevoElemento.grupoPermisos.push(grupoPermisos.grupoPermisos.nombreGrupo)
                    });

                    if(filtro?.documentos != null && filtro.documentos != '' && filtro.documentos != 'null'){
                        if(Number(filtro.documentos) == 4 && rol.docRequeridosRoles.length >= 4){
                            resultadosDTO.push(nuevoElemento);
                        }else{
                            if(rol.docRequeridosRoles.length == Number(filtro.documentos)){
                                resultadosDTO.push(nuevoElemento);
                            }
                        }

                    }else{
                        resultadosDTO.push(nuevoElemento);
                    }

                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {

                // Se ordenan por numero de documentos
                if (paginacionArgs.orden.orden == 'Documentos') {
                    if (paginacionArgs.orden.ordenarPor == 'ASC') {
                        resultadosDTO.sort(function(a,b){
                            return a.documentos - b.documentos;
                        });

                    } else {
                        resultadosDTO.sort(function(a,b){
                            return b.documentos - a.documentos;
                        });
                    }

                // Se ordenan segun esten asignado o no
                } else if (paginacionArgs.orden.orden == 'Asignado') {
                    if (paginacionArgs.orden.ordenarPor == 'ASC') {
                        resultadosDTO.sort(function(a,b){
                            return (a.asignado? 1: 0) - (b.asignado? 1: 0);
                        });

                    } else {
                        resultadosDTO.sort(function(a,b){
                            return (b.asignado? 1: 0) - (a.asignado? 1: 0);
                        });
                    }
                }
                
                docsRequeridosParseados.items = resultadosDTO;
                docsRequeridosParseados.meta = docsRequeridosPaginados.meta;
                docsRequeridosParseados.links = docsRequeridosPaginados.links;

                resultado.Respuesta = docsRequeridosParseados;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Roles obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Roles';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Roles';
        }

        return resultado;
    }

    async getRolesListado(rolesUsuarioConectado : RolesUsuarios[]): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoRoles: Roles[] = [];
        let resultadosDTO: RolesDto[] = [];
        let esSuperusuario = false;

        try {

            let idsRolesBuscados: number[] = [];

            rolesUsuarioConectado.forEach(rolUsuario => {
                idsRolesBuscados.push(rolUsuario.idRol);
                if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID)
                    esSuperusuario = true;
            });
            
            resultadoRoles = await this.rolesRespository.find({
                // relations: ['rolesDestinoAsignacion', 'rolesDestinoAsignacion.rolAsignador'],
                // where:qb => {
                //     qb.where(!esSuperusuario?
                //         'Roles__rolesDestinoAsignacion__rolAsignador.idRol IN(:...IDsRoles)' : 'TRUE', {IDsRoles: idsRolesBuscados}
                //     )
                // }
            });

            if (Array.isArray(resultadoRoles) && resultadoRoles.length) {

                resultadoRoles.forEach(item => {
                    let nuevoElemento: RolesDto = {
                        idRol: item.idRol,
                        Nombre: item.Nombre,
                        Descripcion: item.Descripcion,
                        tipoRol: item.tipoRol,
                        activo: item.activo
                    }

                    resultadosDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Roles obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Roles';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Roles';
        }

        return resultado;
    }

    
    async getRolesListaNombres(): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoRoles: Roles[] = [];
        let resultadosDTO: ObjetoListado[] = [];

        try {

            resultadoRoles = await this.rolesRespository.find();

            if (Array.isArray(resultadoRoles) && resultadoRoles.length) {

                resultadoRoles.forEach(item => {
                    let nuevoElemento: ObjetoListado = {
                        id: item.idRol,
                        Nombre: item.Nombre
                    }

                    resultadosDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Roles obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Roles';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Roles';
        }

        return resultado;
    }

    async getRolDetalle(idRol : number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();

        try {

            let resultadoRoles = await this.rolesRespository.findOne({
                relations: ['rolPermiso', 'grupoPermisosRoles', 'docRequeridosRoles', 'docRequeridosRoles.docsRequeridoRol',
                'docRequeridosRoles.docsRequeridoRol.docsRolesUsuario'],
                where: {idRol}
            });

            if (resultadoRoles == undefined) {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'Error: no se encontro el Rol';
                return resultado;
            }

            let idsDocs : number[] = [];

            resultadoRoles.docRequeridosRoles.forEach(docRequeridos => {
                idsDocs.push(docRequeridos.idDocsRequeridoRol);
            });

            let nuevoElemento: CrearRolesDTO = {
                idRol: resultadoRoles.idRol,
                nombreRol: resultadoRoles.Nombre,
                descripcion: resultadoRoles.Descripcion,
                idTipoRol: resultadoRoles.tipoRol,
                idGrupoPermisos: resultadoRoles.grupoPermisosRoles.map(x => x.idGrupoPermiso),
                idPermisos: resultadoRoles.rolPermiso.map(x => x.idPermiso),
                idDocumentos: idsDocs,
                activo: resultadoRoles.activo
            }

            resultado.Respuesta = nuevoElemento;
            resultado.ResultadoOperacion = true;
            resultado.Mensaje = 'Rol obtenido correctamente';

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Rol';
        }

        return resultado;
    }
    
    async eliminarRol(idRol : number): Promise<Resultado> {
        const resultado: Resultado = new Resultado();

        // Obtenemos Conexion para empezar una Transaccion
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {

            let resultadoRoles = await this.rolesRespository.findOne({
                relations: ['rolPermiso', 'grupoPermisosRoles', 'docRequeridosRoles', 'rolesUsuarios', 'docsRolesUsuario',
                            'rolesActivador', 'rolesDestinoActivacion', 'rolesAsignador', 'rolesDestinoAsignacion', 'registroUsuarios' ],
                where: {idRol}
            });

            if (resultadoRoles == undefined) {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'Error: no se encontro el Rol';
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                return resultado;
            }

            // Si esta asignado a un RolUsuario o a un docRolesUsuario se cancela el borrado
            if (resultadoRoles.rolesUsuarios.length > 0 || resultadoRoles.docsRolesUsuario.length > 0){
                resultado.ResultadoOperacion = false;
                resultado.Error = 'Error: el Rol esta asignado, no puede borrarse';
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                return resultado;
            }

            // Borrado de RolPermiso
            for (let i = 0; i < resultadoRoles.rolPermiso.length; i++) {
                let respuestaRolPermiso = await queryRunner.manager.delete(RolPermiso, resultadoRoles.rolPermiso[i].idRolPermiso);

                if (respuestaRolPermiso.affected == 0){
                   resultado.ResultadoOperacion = false;
                   resultado.Error = 'Error: no se pudo borrar RolPermiso';
                   await queryRunner.rollbackTransaction();
                   await queryRunner.release();
                   return resultado;
                }
            }

            // Borrado de GrupoPermisosRoles
            for (let i = 0; i < resultadoRoles.grupoPermisosRoles.length; i++) {
                let respuestaGrupoPermisosRoles = await queryRunner.manager.delete(GrupoPermisosRoles, resultadoRoles.grupoPermisosRoles[i].idGrupoPermisosRoles);

                if (respuestaGrupoPermisosRoles.affected == 0){
                   resultado.ResultadoOperacion = false;
                   resultado.Error = 'Error: no se pudo borrar GrupoPermisosRoles';
                   await queryRunner.rollbackTransaction();
                   await queryRunner.release();
                   return resultado;
                }
            }

            // Borrado de DocRequeridosRoles
            for (let i = 0; i < resultadoRoles.docRequeridosRoles.length; i++) {
                 let respuestaDocRequerido = await queryRunner.manager.delete(DocRequeridosRoles, resultadoRoles.docRequeridosRoles[i].idDocRequeridosRoles);

                 if (respuestaDocRequerido.affected == 0){
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error: no se pudo borrar DocRequeridosRoles';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                 }
            }

            // Borrado de RolActivaRol donde es Activador
            for (let i = 0; i < resultadoRoles.rolesActivador.length; i++) {
                let respuestaRolesActivador = await queryRunner.manager.delete(RolActivaRol, resultadoRoles.rolesActivador[i].idRolActivaRol);

                if (respuestaRolesActivador.affected == 0){
                   resultado.ResultadoOperacion = false;
                   resultado.Error = 'Error: no se pudo borrar RolActivaRol donde es activador';
                   await queryRunner.rollbackTransaction();
                   await queryRunner.release();
                   return resultado;
                }
           }

            // Borrado de RolActivaRol donde es DestinoActivacion
            for (let i = 0; i < resultadoRoles.rolesDestinoActivacion.length; i++) {
                let respuestaRolesDestinoActivacion = await queryRunner.manager.delete(RolActivaRol, resultadoRoles.rolesDestinoActivacion[i].idRolActivaRol);

                if (respuestaRolesDestinoActivacion.affected == 0){
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error: no se pudo borrar RolActivaRol donde es destino de activacion';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Borrado de RolAsignaRol donde es Asignador
            for (let i = 0; i < resultadoRoles.rolesAsignador.length; i++) {
                let respuestaRolesAsignador = await queryRunner.manager.delete(RolAsignaRol, resultadoRoles.rolesAsignador[i].idRolAsignaRol);

                if (respuestaRolesAsignador.affected == 0){
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error: no se pudo borrar RolActivaRol donde es asignador';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Borrado de RolAsignaRol donde es DestinoAsignacion
            for (let i = 0; i < resultadoRoles.rolesDestinoAsignacion.length; i++) {
                let respuestaRolesDestinoAsignacion = await queryRunner.manager.delete(RolAsignaRol, resultadoRoles.rolesDestinoAsignacion[i].idRolAsignaRol);

                if (respuestaRolesDestinoAsignacion.affected == 0){
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error: no se pudo borrar RolActivaRol donde es destino de asignacion';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Borrado del idRol en el registro de Usuarios
            let idsRegistros : number[] = [];
            for (let i = 0; i < resultadoRoles.registroUsuarios.length; i++) {
                idsRegistros.push(resultadoRoles.registroUsuarios[i].idRegistroUsuarios);
            }

            if (idsRegistros.length != 0){
                let respuestaRolesDestinoAsignacion = await queryRunner.manager.update(RegistroUsuarios, {idRegistroUsuarios: In(idsRegistros)}, {IdRol: null});

                if (respuestaRolesDestinoAsignacion.affected == 0){
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error: no se pudo borrar RolActivaRol donde es destino de asignacion';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            let respuestaRol = await queryRunner.manager.delete(Roles, idRol);

            if (respuestaRol.affected == 0){
               resultado.ResultadoOperacion = false;
               resultado.Error = 'Error: no se pudo borrar el Rol';
               await queryRunner.rollbackTransaction();
               await queryRunner.release();
               return resultado;
            }


            resultado.ResultadoOperacion = true;
            resultado.Mensaje = 'Rol borrado correctamente';
            await queryRunner.commitTransaction();

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error borrando Roles';
            await queryRunner.rollbackTransaction();
        }
        finally {
            await queryRunner.release();
        }

        return resultado;
    }

    async getTipoRoles(rolesUsuarios : RolesUsuarios[]): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoTipoRoles: TipoRol[] = [];
        let resultadosDTO: TipoRolDto[] = [];

        try {

            let idsRolesBuscados: number[] = [];

            rolesUsuarios.forEach(rolUsuario => {
                idsRolesBuscados.push(rolUsuario.idRol);
            });
            
            resultadoTipoRoles = await this.tipoRolRespository.find();

            if (Array.isArray(resultadoTipoRoles) && resultadoTipoRoles.length) {

                resultadoTipoRoles.forEach(item => {
                    let nuevoElemento: TipoRolDto = {
                        idTipoRol: item.idTipoRol,
                        nombre: item.Nombre,
                        descripcion: item.Descripcion,
                    }

                    resultadosDTO.push(nuevoElemento);
                });
            }

            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Tipos de Roles obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Tipos de Roles';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Tipos de Roles';
        }

        return resultado;
    }

    public async editarRol(rol: CrearRolesDTO) {
        const resultado : Resultado = new Resultado();

        // Obtenemos Conexion para empezar una Transaccion
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {

            let rolBBDD = await queryRunner.manager.findOne(Roles, rol.idRol, {
                relations: ['rolPermiso', 'grupoPermisosRoles', 'docRequeridosRoles', 'docRequeridosRoles.docsRequeridoRol',
                'docRequeridosRoles.docsRequeridoRol.docsRolesUsuario'],
            });

            let rolEditado = new RolesDto();
            rolEditado.Nombre = rol.nombreRol;
            rolEditado.Descripcion = rol.descripcion;
            rolEditado.tipoRol = rol.idTipoRol;
            rolEditado.activo = rol.activo;

            let respuestaRol = await queryRunner.manager.update(Roles, rol.idRol, rolEditado);

            if (respuestaRol.affected == 0) {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'Error actualizando el rol';
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                return resultado;
            }

            let docsRequeridos : DocRequeridosRoles[] = [];

            rolBBDD.docRequeridosRoles.forEach(docRequeridos => {
                docsRequeridos.push(docRequeridos);
            });

            // Array de permisos a borrar
            let idsPermisosBorrar: number[] = [];
            // Array de permisos a agregar
            let idsPermisosAgregar: number[] = [];

            // Recorremos los permisos de la BBDD para este rol
            for (let i = 0; i < rolBBDD.rolPermiso.length; i++) {

                // Si el permiso esta entre los nuevos, se borra del array para no tocarlo
                if (rol.idPermisos.includes(rolBBDD.rolPermiso[i].idPermiso)) {
                    const indice = rol.idPermisos.findIndex( x => x == rolBBDD.rolPermiso[i].idPermiso)
                    rol.idPermisos.splice(indice, 1)
                
                // Si no esta entre los nuevos permisos, se agrega al array idsPermisosBorrar para borrarlo
                } else {
                    idsPermisosBorrar.push(rolBBDD.rolPermiso[i].idRolPermiso);
                }
            }

            // El array que quedo tras borrar los permisos que coincidian, se guardan en array para agregarlos
            idsPermisosAgregar = rol.idPermisos;

            // Se insertan RolPermiso, si tuviese permisos para agregar
            for (let i = 0; i < idsPermisosAgregar.length; i++) {

                let nuevoPermiso : RolPermiso = new RolPermiso();
                nuevoPermiso.idRol = rol.idRol;
                nuevoPermiso.idPermiso = idsPermisosAgregar[i];

                let respuestaGrupoPermisosRol = await queryRunner.manager.insert(RolPermiso, nuevoPermiso);

                if (respuestaGrupoPermisosRol.identifiers.length == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error insertando el RolPermiso';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Se borran RolPermiso, si tiene permisos para borrar
            for (let i = 0; i < idsPermisosBorrar.length; i++) {

                let respuestaRolPermiso = await queryRunner.manager.delete(RolPermiso, idsPermisosBorrar[i]);

                if (respuestaRolPermiso.affected == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error borrando el RolPermiso';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }


            // Array de GrupoPermisosRoles a borrar
            let idsGrupoPermisosBorrar: number[] = [];
            // Array de GrupoPermisosRoles a agregar
            let idsGrupoPermisosAgregar: number[] = [];

            // Recorremos los GrupoPermisosRoles de la BBDD para este rol
            for (let i = 0; i < rolBBDD.grupoPermisosRoles.length; i++) {

                // Si el GrupoPermisosRoles esta entre los nuevos, se borra del array para no tocarlo
                if (rol.idGrupoPermisos.includes(rolBBDD.grupoPermisosRoles[i].idGrupoPermiso)) {
                    const indice = rol.idGrupoPermisos.findIndex( x => x == rolBBDD.grupoPermisosRoles[i].idGrupoPermiso)
                    rol.idGrupoPermisos.splice(indice, 1)
                
                // Si no esta entre los nuevos GrupoPermisosRoles, se agrega al array idsGrupoPermisosBorrar para borrarlo
                } else {
                    idsGrupoPermisosBorrar.push(rolBBDD.grupoPermisosRoles[i].idGrupoPermisosRoles);
                }
            }
            
            // El array que quedo tras borrar los GrupoPermisosRoles que coincidian, se guardan en array para agregarlos
            idsGrupoPermisosAgregar = rol.idGrupoPermisos;

            // Se insertan GrupoPermisosRoles, si tuviese nuevos
            for (let i = 0; i < idsGrupoPermisosAgregar.length; i++) {

                let nuevoGrupoPermisosRol : GrupoPermisosRoles = new GrupoPermisosRoles();
                nuevoGrupoPermisosRol.idRol = rol.idRol;
                nuevoGrupoPermisosRol.idGrupoPermiso = idsGrupoPermisosAgregar[i];

                let respuestaGrupoPermisosRol = await queryRunner.manager.insert(GrupoPermisosRoles, nuevoGrupoPermisosRol);

                if (respuestaGrupoPermisosRol.identifiers.length == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error insertando el GrupoPermisosRoles';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Se borran GrupoPermisosRoles, si dejaron de estar
            for (let i = 0; i < idsGrupoPermisosBorrar.length; i++) {

                let respuestaRolPermiso = await queryRunner.manager.delete(GrupoPermisosRoles, idsGrupoPermisosBorrar[i]);

                if (respuestaRolPermiso.affected == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error borrando el GrupoPermisosRoles';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }


            // Array de DocRolUsuario a borrar
            let idsDocRolBorrar: number[] = [];
            // Array de DocRolUsuario a agregar
            let idsDocRolAgregar: number[] = [];

            // Recorremos los DocRolUsuario de la BBDD para este rol
            for (let i = 0; i < docsRequeridos.length; i++) {

                // Si el DocRolUsuario esta entre los nuevos, se borra del array para no tocarlo
                if (rol.idDocumentos.includes(docsRequeridos[i].idDocsRequeridoRol)) {
                    const indice = rol.idDocumentos.findIndex( x => x == docsRequeridos[i].idDocsRequeridoRol);
                    rol.idDocumentos.splice(indice, 1);
                
                // Si no esta entre los nuevos DocRolUsuario, se agrega al array idsDocRequeridosBorrar para borrarlo
                } else {
                    idsDocRolBorrar.push(docsRequeridos[i].idDocRequeridosRoles);
                }
            }

            // El array que quedo tras borrar los DocRolUsuario que coincidian, se guardan en array para agregarlos
            idsDocRolAgregar = rol.idDocumentos;

            // Se insertan DocRolUsuario, si los tuviese
            for (let i = 0; i < idsDocRolAgregar.length; i++) {

                const docsRequeridoRol = await queryRunner.manager.findOne(DocsRequeridoRol, idsDocRolAgregar[i]);

                let nuevoDocsRequeridoRol : DocRequeridosRoles = new DocRequeridosRoles();
                nuevoDocsRequeridoRol.idRol = rol.idRol;
                nuevoDocsRequeridoRol.idDocsRequeridoRol = docsRequeridoRol.idDocsRequeridoRol;

                let respuestaDocsRequeridoRol = await queryRunner.manager.insert(DocRequeridosRoles, nuevoDocsRequeridoRol);

                if (respuestaDocsRequeridoRol.identifiers.length == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error insertando el DocRolUsuario';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            for (let i = 0; i < idsDocRolBorrar.length; i++) {

                let respuestaDocsRequeridoRol = await queryRunner.manager.delete(DocRequeridosRoles, idsDocRolBorrar[i]);

                if (respuestaDocsRequeridoRol.affected == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error borrando el DocRolUsuario';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }                    
            }

            // Se termina la transaccion
            resultado.ResultadoOperacion = true;
            resultado.Mensaje = 'Rol actualizado correctamente';
            await queryRunner.commitTransaction();

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error actualizado Rol';
            await queryRunner.rollbackTransaction();
        }
        finally{
            await queryRunner.release();
        }

        return resultado;
    }

    public async crearRol(req, rol: CrearRolesDTO) {
        const resultado : Resultado = new Resultado();

        // Obtenemos Conexion para empezar una Transaccion
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {


            let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosCrearRolesGruposdePermisosTipodeDocumentos);
    
            // En caso de que el usuario no sea correcto
            if (!usuarioValidado.ResultadoOperacion) {
              return usuarioValidado;
            }

            const user : User = usuarioValidado.Respuesta as User;
            const idUsuario = user.idUsuario;

            // Se crea el nuevo Rol
            let nuevoRol : Roles = new Roles();
            nuevoRol.Nombre = rol.nombreRol;
            nuevoRol.Descripcion = rol.descripcion;
            nuevoRol.tipoRol = rol.idTipoRol;
            nuevoRol.idUsuario = idUsuario;
            nuevoRol.FechaCreacion = new Date();
            nuevoRol.activo = true;

            let respuestaRol = await queryRunner.manager.insert(Roles, nuevoRol);

            if (respuestaRol.identifiers.length == 0) {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'Error insertando el rol';
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                return resultado;
            }

            // Se insertan permisos sueltos, si los tuviese
            for (let i = 0; i < rol.idPermisos.length; i++) {

                let nuevoRolPermiso : RolPermiso = new RolPermiso();
                nuevoRolPermiso.idRol = nuevoRol.idRol;
                nuevoRolPermiso.idPermiso = rol.idPermisos[i];

                let respuestaRolPermiso = await queryRunner.manager.insert(RolPermiso, nuevoRolPermiso);

                if (respuestaRolPermiso.identifiers.length == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error insertando el rolPermiso';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Se insertan grupos de permisos, si los tuviese
            for (let i = 0; i < rol.idGrupoPermisos.length; i++) {

                let nuevoGrupoPermisosRol : GrupoPermisosRoles = new GrupoPermisosRoles();
                nuevoGrupoPermisosRol.idRol = nuevoRol.idRol;
                nuevoGrupoPermisosRol.idGrupoPermiso = rol.idGrupoPermisos[i];

                let respuestaGrupoPermisosRol = await queryRunner.manager.insert(GrupoPermisosRoles, nuevoGrupoPermisosRol);

                if (respuestaGrupoPermisosRol.identifiers.length == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error insertando el grupoPermisosRol';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Se insertan documentos, si los tuviese
            for (let i = 0; i < rol.idDocumentos.length; i++) {

                let nuevoDocsRequeridoRol : DocRequeridosRoles = new DocRequeridosRoles();
                nuevoDocsRequeridoRol.idRol = nuevoRol.idRol;
                nuevoDocsRequeridoRol.idDocsRequeridoRol = rol.idDocumentos[i];

                let respuestaDocsRequeridoRol = await queryRunner.manager.insert(DocRequeridosRoles, nuevoDocsRequeridoRol);

                if (respuestaDocsRequeridoRol.identifiers.length == 0) {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'Error insertando el docRequeridosRoles';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                }
            }

            // Se termina la transaccion
            resultado.ResultadoOperacion = true;
            resultado.Mensaje = 'Rol creado correctamente';
            await queryRunner.commitTransaction();

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error creando Rol';
            await queryRunner.rollbackTransaction();
        }
        finally{
            await queryRunner.release();
        }

        return resultado;
    }


    async getOficinasRegionesByRol(idRolCreado: number, rolesUsuarios : RolesUsuarios[]): Promise<Resultado> {
        const resultado: Resultado = new Resultado();
        let resultadoRegiones: RegionesEntity[] = [];
        let resultadoOficinas: OficinasEntity[] = [];
        let rolCreado: Roles;
        let resultadosDTO: ListaOficinasRegionesDTO[] = [];
        let idsOficinasUsuarioConectado: number[] = [];
        let idsRegionesUsuarioConectado: number[] = [];
        let esSuperUsuario: boolean = false;
        let esRegional: boolean = false;

        try {
            // Obtenermos el Rol cuya id recibimos del Front para saber el TipoRol
            rolCreado = await this.rolesRespository.findOne({ relations: ['tipoRoles'], where: {idRol : idRolCreado}});

            // Obtenemos de los Roles del UsuarioConectado: Regiones, Oficinas y si esSuperUsuario
            for (const rolUsuario of rolesUsuarios) {
                if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID) {
                    esSuperUsuario = true;
                } else if (rolUsuario.roles.tipoRoles.Nombre == tipoRolRegion) {
                    idsRegionesUsuarioConectado.push(rolUsuario.idRegion);
                    esRegional = true;
                } else if (rolUsuario.roles.tipoRoles.Nombre == tipoRolOficina) {
                    idsOficinasUsuarioConectado.push(rolUsuario.idOficina);
                }
            }


            if (rolCreado) {
                // Para asignar un Rol tipo SuperUsuario no hace falta devolver nada
                if ((rolCreado.tipoRoles.idTipoRol == tipoRolSuperUsuarioID || rolCreado.tipoRoles.idTipoRol == tipoRolGeneralID) /*&& esSuperUsuario*/){
                    resultado.ResultadoOperacion = true;
                    resultado.Mensaje = 'No se necesitan Regiones. Finalizado Correctamente.';

                    return resultado;

                // Para asignar un Rol tipo Region, devolveremos listado de regiones
                } else if (rolCreado.tipoRoles.idTipoRol == tipoRolRegionID && (esRegional || esSuperUsuario)){

                    // Obtenemos todas las regiones para SuperUsuario, o solo las mias si soy tipo Region
                    resultadoRegiones = await this.regionesRespository.find({
                        // where:qb => {
                        //     qb.where(!esSuperUsuario ?
                        //         'RegionesEntity.idRegion IN(:...IDsRegiones)': 'TRUE', {IDsRegiones: idsRegionesUsuarioConectado}
                        // )}
                    });

                    if (Array.isArray(resultadoRegiones) && resultadoRegiones.length) {

                        resultadoRegiones.forEach(region => {
                            let nuevoElemento: ListaOficinasRegionesDTO = {
                                idRegion: region.idRegion,
                                Nombre: region.Nombre
                            }
        
                            resultadosDTO.push(nuevoElemento);
                        });
                    }

                // Para asignar un Rol tipo Oficina, devolveremos listado de oficinas
                } else if (rolCreado.tipoRoles.idTipoRol == tipoRolOficinaID){

                    // Si soy Usuario de tipo Region obtengo las oficinas de mis Regiones
                    if (esRegional && !esSuperUsuario) {
                        let oficinasRegion = await this.oficinasRespository.find({ relations: ['comunas', 'comunas.regiones'],
                        // where:qb => {
                        //     qb.where(
                        //         'OficinasEntity__comunas__regiones.idRegion IN(:...IDsRegiones)', {IDsRegiones: idsRegionesUsuarioConectado}
                        //     )}
                        });

                        resultadoOficinas = oficinasRegion;

                    }

                    //if (idsOficinasUsuarioConectado.length > 0) {
                    // Finalmente, obtengo las oficinas (Todas en caso del SuperUsuario)
                    if(esSuperUsuario || idsOficinasUsuarioConectado.length > 0){
                        let oficinasPropias = await this.oficinasRespository.find({
                            // where:qb => {
                            //     qb.where(!esSuperUsuario ?
                            //         'OficinasEntity.idOficina IN(:...IDsOficinas)': 'TRUE', {IDsOficinas: idsOficinasUsuarioConectado}
                            //     )}
                            });
        
                        oficinasPropias.forEach(oficina => {
                            if (!resultadoOficinas.find(x => x.idOficina == oficina.idOficina))
                            resultadoOficinas.push(oficina);
                        });
                    }
                    //}

                    if (Array.isArray(resultadoOficinas) && resultadoOficinas.length) {
                        

                        resultadoOficinas.forEach(oficina => {
                            let nuevoElemento: ListaOficinasRegionesDTO = {
                                idOficina: oficina.idOficina,
                                Nombre: oficina.Nombre,
                            }
        
                            resultadosDTO.push(nuevoElemento);
                        });
                    }

                } else {
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'No se tienen permisos para este Rol';
                    return resultado;
                }
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontró el Rol';
                return resultado;
            }


            if (Array.isArray(resultadosDTO) && resultadosDTO.length) {

                resultado.Respuesta = resultadosDTO;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'Regiones y Oficinas obtenidos correctamente';
            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se encontraron Regiones ni Oficinas';
            }

        } catch (error) {
            console.error(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error obteniendo Regiones y Oficinas';
        }

        return resultado;
    }
}
