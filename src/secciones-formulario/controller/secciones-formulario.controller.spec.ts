import { Test, TestingModule } from '@nestjs/testing';
import { SeccionesFormularioController } from './secciones-formulario.controller';

describe('SeccionesFormularioController', () => {
  let controller: SeccionesFormularioController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SeccionesFormularioController],
    }).compile();

    controller = module.get<SeccionesFormularioController>(SeccionesFormularioController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
