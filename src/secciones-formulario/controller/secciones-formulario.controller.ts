import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SeccionesFormularioService } from '../service/secciones-formulario.service';

@ApiTags('Secciones-formulario')
@Controller('secciones-formulario')
export class SeccionesFormularioController {

    constructor(private readonly seccionService: SeccionesFormularioService) { }
    @Get()
    async getSeccionesFormulario() {
        const data = await this.seccionService.getSeccionesFormulario();
        return { data };
    }

}
