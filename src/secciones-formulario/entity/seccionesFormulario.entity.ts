import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('SeccionesFormulario')
export class SeccionesFormulario {

    @PrimaryGeneratedColumn()
    idSeccionFormulario: number;
    
    @Column()
    nombreSeccion: string;
    
}