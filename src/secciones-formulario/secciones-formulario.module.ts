import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SeccionesFormularioController } from './controller/secciones-formulario.controller';
import { SeccionesFormulario } from './entity/seccionesFormulario.entity';
import { SeccionesFormularioService } from './service/secciones-formulario.service';

@Module({
  imports: [TypeOrmModule.forFeature([SeccionesFormulario])],
  controllers: [SeccionesFormularioController],
  providers: [SeccionesFormularioService],
  exports: [SeccionesFormularioService]
})
export class SeccionesFormularioModule {}
