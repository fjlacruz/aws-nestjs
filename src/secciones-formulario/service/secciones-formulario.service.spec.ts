import { Test, TestingModule } from '@nestjs/testing';
import { SeccionesFormularioService } from './secciones-formulario.service';

describe('SeccionesFormularioService', () => {
  let service: SeccionesFormularioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SeccionesFormularioService],
    }).compile();

    service = module.get<SeccionesFormularioService>(SeccionesFormularioService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
