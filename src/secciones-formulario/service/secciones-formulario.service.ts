import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SeccionesFormulario } from '../entity/seccionesFormulario.entity';

@Injectable()
export class SeccionesFormularioService {
    constructor(@InjectRepository(SeccionesFormulario) private readonly seccionRespository: Repository<SeccionesFormulario>) { }

    async getSeccionesFormulario() {
        return await this.seccionRespository.find();
    }
}
