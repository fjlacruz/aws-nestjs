import { Test, TestingModule } from '@nestjs/testing';
import { SentenciaController } from './sentencia.controller';

describe('SentenciaController', () => {
  let controller: SentenciaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SentenciaController],
    }).compile();

    controller = module.get<SentenciaController>(SentenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
