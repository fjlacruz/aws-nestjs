import { Body, Controller, Delete, Get, Param, Post as PostMethod, Put, UseGuards, Req } from '@nestjs/common';
import { Request } from 'express';
import { ApiBearerAuth, ApiTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { SentenciaService } from '../service/sentencia.service';
import { SentenciaEntity } from '../entity/sentencia.entity';
import { AuthGuard } from '@nestjs/passport';
import { HeaderUtil } from '../../base/base/header-util';
import { Page, PageRequest } from '../pagination-sentencia.entity';
import { InfraccionEntity } from '../../infraccion/infraccion.entity';

@ApiTags('Sentencias')
@Controller('sentencias')
export class SentenciaController {
  constructor(private readonly sentenciaService: SentenciaService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all records',
    type: SentenciaEntity,
  })
  async getAll(@Req() req: Request): Promise<SentenciaEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.sentenciaService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/resolucion/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una Sentencia por Id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: SentenciaEntity,
  })
  async getAllByResolucionId(@Param('id') id: string): Promise<SentenciaEntity[]> {
    return await this.sentenciaService.findAllByResolucionId({
      where: { idresolucionJudicial: id },
      relations: ['tipoUnidadMonetariaEntity', 'infraccionEntity', 'infraccionEntity.tipoInfraccionEntity'],
    });
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una Sentencia por Id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: SentenciaEntity,
  })
  async getOne(@Param('id') id: string): Promise<SentenciaEntity> {
    return await this.sentenciaService.findById(id);
  }

  @PostMethod('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea una nueva Sentencia' })
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    type: SentenciaEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() sentenciaEntity: SentenciaEntity): Promise<SentenciaEntity> {
    const created = await this.sentenciaService.save(sentenciaEntity);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: SentenciaEntity,
  })
  async put(@Req() req: Request, @Body() sentenciaEntity: SentenciaEntity): Promise<SentenciaEntity> {
    return await this.sentenciaService.update(sentenciaEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: SentenciaEntity,
  })
  async putId(@Req() req: Request, @Body() sentenciaEntity: SentenciaEntity): Promise<SentenciaEntity> {
    return await this.sentenciaService.update(sentenciaEntity);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Elimina una Sentencia' })
  @ApiResponse({
    status: 204,
    description: 'The record has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
    return await this.sentenciaService.deleteById(id);
  }
}
