import { ApiPropertyOptional } from '@nestjs/swagger';
import { JoinColumn, ManyToOne } from 'typeorm';
import { ConductoresEntity } from '../../../conductor/entity/conductor.entity';

export class SentenciaDTO {
  @ApiPropertyOptional()
  idSentencias: number;
  @ApiPropertyOptional()
  idInfracciones: number;
  @ApiPropertyOptional()
  idresolucionJudicial: number;
  @ApiPropertyOptional()
  suspension: boolean;
  @ApiPropertyOptional()
  amonestacion: boolean;
  @ApiPropertyOptional()
  multa: boolean;
  @ApiPropertyOptional()
  idtipoUnidadMonetaria: number;
  @ApiPropertyOptional()
  diasTotalesSuspension: number;
  @ApiPropertyOptional()
  montoMulta: number;
  @ApiPropertyOptional()
  cancelacion: boolean;
  @ApiPropertyOptional()
  aniosCancelacion: number;
  @ApiPropertyOptional()
  cancelacionPerpetua: boolean;
}
