import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { Roles } from '../../roles/entity/roles.entity';
import { InfraccionEntity } from '../../infraccion/infraccion.entity';
import { TipoUnidadMonetariaEntity } from '../../tipo-unidad-monetaria/tipo-unidad-monetaria.entity';
import { ConductoresEntity } from '../../conductor/entity/conductor.entity';
import { ResolucionesJudicialesEntity } from '../../resoluciones-judiciales/entity/resoluciones-judiciales.entity';

@Entity('Sentencias')
export class SentenciaEntity {
  @PrimaryGeneratedColumn()
  idSentencias: number;
  @Column()
  @ApiModelProperty()
  idInfracciones: number;
  @JoinColumn({name: 'idInfracciones'})
  @ManyToOne(() => InfraccionEntity, roles => roles.idInfracciones, { eager: false })
  readonly infraccionEntity: InfraccionEntity;

  @Column()
  @ApiModelProperty()
  idresolucionJudicial: number;

  @ManyToOne(() => ResolucionesJudicialesEntity, resolucion => resolucion.sentencia)
  @JoinColumn({ name: 'idresolucionJudicial' })
  readonly resolucion: ResolucionesJudicialesEntity;

  @Column()
  @ApiModelProperty()
  suspension: boolean;
  @Column()
  @ApiModelProperty()
  amonestacion: boolean;
  @Column()
  @ApiModelProperty()
  multa: boolean;
  @Column()
  @ApiModelProperty()
  idtipoUnidadMonetaria: number;
  @JoinColumn({name: 'idtipoUnidadMonetaria'})
  @ManyToOne(() => TipoUnidadMonetariaEntity, roles => roles.idtipoUnidadMonetaria, { eager: false })
  readonly tipoUnidadMonetariaEntity: TipoUnidadMonetariaEntity;
  @Column()
  @ApiModelProperty()
  diasTotalesSuspension: number;
  @Column()
  @ApiModelProperty()
  montoMulta: number;
  @Column()
  @ApiModelProperty()
  cancelacion: boolean;
  @Column()
  @ApiModelProperty()
  aniosCancelacion: number;
  @Column()
  @ApiModelProperty()
  cancelacionPerpetua: boolean;
}
