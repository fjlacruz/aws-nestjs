import { Module } from '@nestjs/common';
import { SentenciaController } from './controller/sentencia.controller';
import { SentenciaService } from './service/sentencia.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SentenciaEntity } from './entity/sentencia.entity';
import { AnotacionesResolucionEntity } from '../anotaciones-resolucion/anotaciones-resolucion.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SentenciaEntity, AnotacionesResolucionEntity])],
  controllers: [SentenciaController],
  exports: [SentenciaService],
  providers: [SentenciaService],
})
export class SentenciaModule {}
