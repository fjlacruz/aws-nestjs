import { Test, TestingModule } from '@nestjs/testing';
import { SentenciaService } from './sentencia.service';

describe('SentenciaService', () => {
  let service: SentenciaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SentenciaService],
    }).compile();

    service = module.get<SentenciaService>(SentenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
