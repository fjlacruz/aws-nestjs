import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SentenciaEntity } from '../entity/sentencia.entity';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { ResolucionesJudicialesEntity } from '../../resoluciones-judiciales/entity/resoluciones-judiciales.entity';
import { AnotacionesResolucionEntity } from '../../anotaciones-resolucion/anotaciones-resolucion.entity';

const relationshipNames = [];

@Injectable()
export class SentenciaService {
  constructor(
    @InjectRepository(SentenciaEntity) private readonly sentenciaEntityRepository: Repository<SentenciaEntity>,
    @InjectRepository(AnotacionesResolucionEntity)
    private readonly anotacionesResolucionEntityRepository: Repository<AnotacionesResolucionEntity>
  ) {}

  async findById(id: string): Promise<SentenciaEntity | undefined> {
    const options = { relations: relationshipNames };
    const result = await this.sentenciaEntityRepository.findOne(id, options);
    return result;
  }

  async findByfields(options: FindOneOptions<SentenciaEntity>): Promise<SentenciaEntity | undefined> {
    const result = await this.sentenciaEntityRepository.findOne(options);
    return result;
  }

  async findAndCount(options: FindManyOptions<SentenciaEntity>): Promise<[SentenciaEntity[], number]> {
    options.relations = relationshipNames;
    const resultList = await this.sentenciaEntityRepository.findAndCount(options);
    const sentenciaEntity: SentenciaEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(sentencia => sentenciaEntity.push(sentencia));
      resultList[0] = sentenciaEntity;
    }
    return resultList;
  }

  async save(sentenciaEntity: SentenciaEntity): Promise<SentenciaEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.sentenciaEntityRepository.save(entity);
    return result;
  }

  async update(sentenciaEntity: SentenciaEntity): Promise<SentenciaEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.sentenciaEntityRepository.save(entity);
    return result;
  }

  async deleteById(id: string): Promise<void | undefined> {
    await this.sentenciaEntityRepository.delete(id);
    const entityFind = await this.findById(id);
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }

  async findAllByResolucionId(options: FindManyOptions<SentenciaEntity>) {
    const data = await this.sentenciaEntityRepository.find(options);
    for (const sentencia of data) {
      console.log(sentencia);
      const anotacion = await this.anotacionesResolucionEntityRepository.findOne({
        where: { idSentencias: sentencia.idSentencias },
        relations: ['tipoAnotacionEntity'],
      });
      console.log(anotacion);
      sentencia['anotacion'] = anotacion;
    }
    return data;
  }
}
