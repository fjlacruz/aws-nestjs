import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateSglcLogDTO {

    @ApiPropertyOptional()
    routeURL:string;

    @ApiPropertyOptional()
    message:string;

    @ApiPropertyOptional()
    timestamp:Date;

    @ApiPropertyOptional()
    idUsuario:number;
}
