import { Test, TestingModule } from '@nestjs/testing';
import { SGLCLogController } from './sglc-log.controller';

describe('SGLCLogController', () => {
  let controller: SGLCLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SGLCLogController],
    }).compile();

    controller = module.get<SGLCLogController>(SGLCLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
