import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { SGLCLogService } from '../services/sglc-log.service';
import { CreateSglcLogDTO } from '../DTO/createSglcLog.dto';
import { SglcLogEntity } from '../entity/sglc-log.entity';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';

@ApiTags('Log-errores')
@Controller('sglc-log')
export class SGLCLogController {

    constructor(private readonly sglcLogService: SGLCLogService) { }
    @Get('getLogErrores')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Logs de error' })
    async getLogErrores() {
        const data = await this.sglcLogService.getSglcLogs();
        return { data };
    }

    @Get('gerLogErrorById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Log de error por Id' })
    async gerLogErrorById(@Param('id') id: number) {
        const data = await this.sglcLogService.getSglcLogById(id);
        return { data };
    }


    @Post('creaLogError')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo log de errores' })
    async creaLogError(
        @Body() createSglcLogDTO: CreateSglcLogDTO) {
        const data = await this.sglcLogService.create(createSglcLogDTO);
        return {data};
    }
}
