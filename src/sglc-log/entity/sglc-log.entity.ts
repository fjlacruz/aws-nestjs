import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('SGLCLog')
export class SglcLogEntity {

    @PrimaryColumn()
    idSGLCLog:number;

    @Column()
    routeURL:string;

    @Column()
    message:string;

    @Column()
    timestamp:Date;

    @Column()
    idUsuario:number;
}
