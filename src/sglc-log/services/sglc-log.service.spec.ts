import { Test, TestingModule } from '@nestjs/testing';
import { SGLCLogService } from './sglc-log.service';

describe('SGLCLogService', () => {
  let service: SGLCLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SGLCLogService],
    }).compile();

    service = module.get<SGLCLogService>(SGLCLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
