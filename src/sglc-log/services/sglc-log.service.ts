import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { CreateSglcLogDTO } from '../DTO/createSglcLog.dto';
import { SglcLogEntity } from '../entity/sglc-log.entity';


@Injectable()
export class SGLCLogService {

    constructor(@InjectRepository(SglcLogEntity) private readonly sglcLogEntityRespository: Repository<SglcLogEntity>) { }

    async getSglcLogs() {
        return await this.sglcLogEntityRespository.find();
    }

    async getSglcLogById(id:number){
        const pagina= await this.sglcLogEntityRespository.findOne(id);
        if (!pagina)throw new NotFoundException("Log dont exist");
        
        return pagina;
    }

    async update(idSGLCLog: number, data: Partial<CreateSglcLogDTO>) {
        await this.sglcLogEntityRespository.update({ idSGLCLog }, data);
        return await this.sglcLogEntityRespository.findOne({ idSGLCLog });
      }
      
    async create(data: CreateSglcLogDTO): Promise<SglcLogEntity> {
        return await this.sglcLogEntityRespository.save(data);
    }

}
