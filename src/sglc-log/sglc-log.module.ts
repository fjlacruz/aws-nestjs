import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SGLCLogController } from './controller/sglc-log.controller';
import { SglcLogEntity } from './entity/sglc-log.entity';
import { SGLCLogService } from './services/sglc-log.service';


@Module({
  imports: [TypeOrmModule.forFeature([SglcLogEntity])],
  providers: [SGLCLogService],
  exports: [SGLCLogService],
  controllers: [SGLCLogController]
})
export class SglcLogModule {}
