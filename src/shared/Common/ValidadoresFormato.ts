import { isNumber } from "lodash";

// Función para validar si un string es un entero
export function validarStringEsEntero(str : string) {
    return !isNumber(str) && Number.isInteger(parseFloat(str));
  }