import { ApiPropertyOptional } from '@nestjs/swagger';

export class ClaseLicenciaDto {
  @ApiPropertyOptional()
  IdClaseLicencia: number;

  @ApiPropertyOptional()
  Nombre: string;

  @ApiPropertyOptional()
  Descripcion: string;

  @ApiPropertyOptional()
  Abreviacion: string;  
  
}
