import { ApiPropertyOptional } from '@nestjs/swagger';

export class EstadoExaminacionDto {
    
  @ApiPropertyOptional()
  IdEstadoExaminacion: number;

  @ApiPropertyOptional()
  Nombre: string;

  @ApiPropertyOptional()
  Descripcion: string;
  
}
