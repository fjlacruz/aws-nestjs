import { ApiPropertyOptional } from '@nestjs/swagger';

export class EstadoSolicitudDto {
  @ApiPropertyOptional()
  IdEstadoSolicitud: number;

  @ApiPropertyOptional()
  Nombre: string;

  @ApiPropertyOptional()
  Descripcion: string;
  
}