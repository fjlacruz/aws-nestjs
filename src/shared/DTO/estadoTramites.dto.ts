import { ApiPropertyOptional } from '@nestjs/swagger';

export class EstadoTramiteDto {
  @ApiPropertyOptional()
  IdEstadoTramite: number;

  @ApiPropertyOptional()
  Nombre: string;

  @ApiPropertyOptional()
  Descripcion: string;
  
}
