import { ApiPropertyOptional } from '@nestjs/swagger';

export class TipoExaminacionDto {
    
  @ApiPropertyOptional()
  IdTipoExaminacion: number;

  @ApiPropertyOptional()
  Nombre: string;

  @ApiPropertyOptional()
  Descripcion: string;
  
}
