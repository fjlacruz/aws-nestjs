import { ApiPropertyOptional } from '@nestjs/swagger';

export class TipoTramiteDto {
  @ApiPropertyOptional()
  IdTipoTramite: number;

  @ApiPropertyOptional()
  Nombre: string;

  @ApiPropertyOptional()
  Descripcion: string;
  
}
