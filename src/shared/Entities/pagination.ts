import { ApiPropertyOptional } from '@nestjs/swagger';

export class paginationResult{
    items: object;
    meta: paginationMeta;
}

export class paginationMeta {
  @ApiPropertyOptional()
  itemCount: number;
  @ApiPropertyOptional()
  totalItems: number;
  @ApiPropertyOptional()
  itemsPerPage: number;
  @ApiPropertyOptional()
  totalPages: number;  
  @ApiPropertyOptional()
  currentPage: number;  
}

export class paginationLinks {
    @ApiPropertyOptional()
    first: string;
    @ApiPropertyOptional()
    previous: string;
    @ApiPropertyOptional()
    next: string;
    @ApiPropertyOptional()
    last: string;        
  }


