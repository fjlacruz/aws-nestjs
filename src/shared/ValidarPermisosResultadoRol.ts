import { plainToClass } from "class-transformer";
import { RolesUsuarios } from "src/roles-usuarios/entity/roles-usuarios.entity";
import { Resultado } from "src/utils/resultado"
/**
 * Método para mapear la entidad Rolesusuarios desde la respuesta.resultado de checkUserAndRol 
 * @param permisos Objeto respuesta con la lista de permisos
 * @returns retorna una lista de RolesUsuario
 */
export async function permisosRol(permisos:Object)
{
    let roles:RolesUsuarios[]= [];
    for(const [key,value] of Object.entries(permisos))
    {        
         let item:RolesUsuarios = plainToClass(RolesUsuarios,value)
         roles.push(item);
    }
    return roles;
}




