import { registerDecorator, ValidationOptions } from 'class-validator';

// * Constante para el regex del rut
export const runRegex = new RegExp(/^[0-9]{7,8}[-|‐]{1}[0-9kK]{1}/s);

/**
 *
 * Función para validar que el rut enviado es correcto
 *
 * @param rutCompleto
 * @returns Devuelve true si el rut es correcto y false si no
 */
export function validarRut(rutCompleto: string): boolean {
  let rutValido = false;

  if (runRegex.test(rutCompleto)) {
    const rutCompletoSplit = rutCompleto.split('-');
    const rut = parseInt(rutCompletoSplit[0]);
    const dv = rutCompletoSplit[1].toLowerCase();

    if (obtenerRutDv(rut) === dv) rutValido = true;
  }

  return rutValido;
}

function obtenerRutDv(rut: number): string {
  var M = 0;
  let suma = 1;

  for (; rut; rut = Math.floor(rut / 10)) {
    suma = (suma + (rut % 10) * (9 - (M++ % 6))) % 11;
  }

  return suma ? (suma - 1).toString() : 'k';
}

/**
 * Decorador de Class validator para validar el rut.
 * Hace la misma función que la funcion de validar run, pero como decorador
 *
 * @param validationOptions
 * @returns
 */
export function isValidRut(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isValidRut',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(rutCompleto) {
          const validado = validarRut(rutCompleto);

          return validado;
        },
      },
    });
  };
}

// Formatea run xxxxxxxx-x -> xx.xxx.xxx-x
export function formatearRun(run : string) {
  // Separa por el '-'
  const arrRut = run.split('-');
  const rut1 = arrRut[0];
  const rutDV = arrRut[1];
  // Para la primera parte antes del guion, mete puntos
  const rutConMiles = rut1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  const rutFormateado = rutConMiles + '-' + rutDV;

  return rutFormateado;
}

export function devolverValorRUN(runCompleto : string){
  return runCompleto.split('.').join("").split("-")[0];
}

export function devolverValorDV(runCompleto : string){
  return runCompleto.split('.').join("").split("-")[1];
}