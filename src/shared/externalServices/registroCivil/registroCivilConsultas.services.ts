import {
    Injectable
} from '@nestjs/common';
import {
    InjectRepository
} from '@nestjs/typeorm';

import {
    TiposTramiteEntity
} from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { Resultado } from 'src/utils/resultado';


import {
    Repository
} from 'typeorm';

@Injectable()
export class RegistroCivilConsultasService {

    
    BASE_URL_REGISTRO_CIVIL = 'https://4qnzjlsq1k.execute-api.sa-east-1.amazonaws.com';
    axios = require('axios');


    constructor(
        //@InjectRepository(TiposTramiteEntity) private readonly tipoTramitesEntityRespository: Repository<TiposTramiteEntity>,
    ) { }

    async consulHis(data) {
        let resultado: Resultado = new Resultado();

        try {
            const datos = await this.axios.post(this.BASE_URL_REGISTRO_CIVIL + '/api/rnc-historicornc', data);

            resultado.ResultadoOperacion = true;
            resultado.Respuesta = datos.data
        } catch (e) {
            this.errorExcesoPeticiones(e);
            resultado = await this.consulHis(data);
        }

        return resultado;
    }

    async errorExcesoPeticiones(e) {
        let err = e.response.status;
        if (err === 429) {
            await this.reintentoConsulta(10000);
            console.log('exceso de peticiones');
        }
    }
    async reintentoConsulta(milisec) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve('');
            }, milisec);
        });
    }



}