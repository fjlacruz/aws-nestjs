import {
    Injectable
} from '@nestjs/common';
import {
    InjectRepository
} from '@nestjs/typeorm';

import {
    TiposTramiteEntity
} from 'src/tipos-tramites/entity/tipos-tramite.entity';

import {
    Repository
} from 'typeorm';


import {
    Resultado
} from 'src/utils/resultado';


import { TipoTramiteDto } from 'src/shared/DTO/tipoTramite.dto';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { ColaExaminacionTramiteNMEntity } from 'src/tramites/entity/cola-examinacion-tramite-n-m.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { ClasesLicenciasIds, imagenBarraRUNBase64, TipoExaminacionesIds } from 'src/constantes';
import * as moment from 'moment';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { DatosClasesLicenciasConFechas } from 'src/tramites/DTO/datosClasesLicenciasConFechas.dto';
import { RestriccionesEntity } from 'src/clases-licencia/entity/restricciones.entity';
import { ClasesLicenciaRegistroCivilDto } from 'src/clases-licencia/Dto/clases-licencia-reg-civil.dto';
import { ClasesLicenciasDTO } from 'src/clases-licencia/Dto/clases-licencias.entity';

@Injectable()
export class ServicioGenerarLicenciasService {

  constructor(
      @InjectRepository(TiposTramiteEntity) private readonly tipoTramitesEntityRespository: Repository<TiposTramiteEntity>

  ) { }


  // Método para la recuperación de los diferentes tipos de trámites
  public async getTipoTramites(filtroIds?:number[]) {


      let res: Resultado = new Resultado();

      try {

          let arrayTipoTramiteDto: TipoTramiteDto[] = [];

          const qbTramites = this.tipoTramitesEntityRespository.createQueryBuilder('TiposTramite')

          // Comprobamos si nos viene un filtro para restringuir
          if(filtroIds){
              qbTramites.andWhere('(TiposTramite.idTipoTramite IN ( :...ids ))', { ids: filtroIds });
          }

          let tipost: TiposTramiteEntity[] = await qbTramites.getMany();

          tipost.forEach(x => {
              let tipoTramiteDto: TipoTramiteDto = new TipoTramiteDto();

              tipoTramiteDto.IdTipoTramite = x.idTipoTramite;
              tipoTramiteDto.Nombre = x.Nombre;
              tipoTramiteDto.Descripcion = x.Descripcion;

              arrayTipoTramiteDto.push(tipoTramiteDto);
          });

          res.Respuesta = arrayTipoTramiteDto;
          res.ResultadoOperacion = true;

          return res;
      }
      catch (Error) {
          res.ResultadoOperacion = true;
          res.Error = "Ha ocurrido un error al obtener los estados de examinación";
      }
  }

  async obtenerFormularioF8(tcl:TramitesClaseLicencia, 
                            documentoLicencia: DocumentosLicencia, 
                            codigoQR: String, 
                            datosConLicenciasFechas : { idClaseLicencia: string, 
                                                        descClaseLicencia: string,
                                                        fechaPrimerOtorgamiento: string, 
                                                        fechaActualOtorgamiento: string, 
                                                        fechaProximoControl: string,
                                                        asignado: boolean,
                                                        folio: string}[], 
                            documentoLicenciaAnterior : ClasesLicenciasDTO [],
                            image,
                            Jimp,
                            RutaFuenteFGB,
                            RutaFuenteCourier,
                            RutaFuenteRobotoRegular,
                            RutaCourierNew34Route,
                            RutaCourierNew14Route,
                            RutaCourierNew38Route,
                            RutaCourierNew18Route){
                            
    try{
      // Procedemos a la edición de la imagen con los textos
    
      // Zona de cabecera
      await this.imprimirCabeceraDocumento(Jimp, image, documentoLicencia, RutaCourierNew38Route, RutaCourierNew18Route);

      // Zona Identificación del conductor
      await this.imprimirZonaIdentificacionConductor(Jimp, image, tcl, RutaFuenteRobotoRegular);

      // Zona Datos de la Licencia Otorgada
      await this.imprimirZonaDatosLicenciaOtorgada(Jimp, image, documentoLicencia, tcl, RutaFuenteRobotoRegular);

      // Zona Escuela de Conductor
      await this.imprimirZonaEscuelaConductor(Jimp, image, datosConLicenciasFechas, RutaFuenteRobotoRegular);

      // Zona CLASES DE LICENCIA
      await this.imprimirZonaClasesDeLicencia(Jimp, image, datosConLicenciasFechas, RutaFuenteFGB, documentoLicencia, RutaCourierNew34Route, RutaCourierNew14Route);

      // Zona DATOS DE LA LICENCIA ANTERIOR
      await this.imprimirZonaDatosLicenciaAnterior(Jimp, image, documentoLicenciaAnterior, RutaFuenteRobotoRegular);
      
      // Zona Licencia de Conductor Datos Personales
      await this.imprimirZonaLicenciaConductor(Jimp, image, tcl, codigoQR, RutaFuenteFGB, RutaFuenteCourier, documentoLicencia);

      // Zona LICENCIA DE CONDUCTOR Clases de Licencia
      await this.imprimirZonaLicenciaConductorClasesDeLicencia(Jimp, image, datosConLicenciasFechas, RutaFuenteFGB);

      // Zona Examenes Asociados
      await this.imprimirZonaExamenesAsociadosIdoneidadMoral(Jimp, image, documentoLicencia, RutaFuenteRobotoRegular);

      await this.imprimirZonaExamenesAsociadosExamenMedico(Jimp, image, documentoLicencia, RutaFuenteRobotoRegular);

      await this.imprimirZonaExamenesAsociadosExamenTeorico(Jimp, image, documentoLicencia, RutaFuenteRobotoRegular);

      await this.imprimirZonaExamenesAsociadosExamenPractico(Jimp, image, documentoLicencia, RutaFuenteRobotoRegular);

      // Zona Validación Dirección de Tránsito
      await this.imprimirZonaValidacionDireccionTransito(Jimp, image, documentoLicencia, RutaFuenteRobotoRegular);

      //await image.writeAsync(rutaImagenExportacion);

      let imageBase64 : string = await image.getBase64Async(Jimp.MIME_PNG);

      return imageBase64;
    }
    catch(Error){0
      console.log(Error);
    }
  }

  // Función para crear estructura completa con fechas de cada clase de licencia
  async obtenerDatosClasesLicenciasFechas(postulante : PostulantesEntity, folioCompleto : string, mapeoLicenciasRegistroCivil : ClasesLicenciaRegistroCivilDto[]){

    let clasesLicenciasFechas: DatosClasesLicenciasConFechas[] = [];                            


    for(let clid in ClasesLicenciasIds){

      let claseLicenciaAux = postulante.conductor.documentosLicencia.documentoLicenciaClaseLicencia.filter(x => x.idClaseLicencia.toString() == clid);

      if(claseLicenciaAux && claseLicenciaAux.length > 0){
        clasesLicenciasFechas.push({
          idClaseLicencia:clid,
          descClaseLicencia: claseLicenciaAux[0].clasesLicencias.Abreviacion,
          fechaPrimerOtorgamiento:(claseLicenciaAux[0].primerOtorgamiento)?moment(claseLicenciaAux[0].primerOtorgamiento).format('DD/MM/YYYY'):'',
          fechaActualOtorgamiento:(claseLicenciaAux[0].otorgamientoActual)?moment(claseLicenciaAux[0].otorgamientoActual).format('DD/MM/YYYY'):'',
          fechaProximoControl:(claseLicenciaAux[0].caducidadOtorgamiento)?moment(claseLicenciaAux[0].caducidadOtorgamiento).format('DD/MM/YYYY'):'',
          asignado:true,
          folio: folioCompleto,
          asignadoRC:false});
      }
      else{
        clasesLicenciasFechas.push({
          idClaseLicencia:clid,
          descClaseLicencia:'', 
          fechaPrimerOtorgamiento:'',
          fechaActualOtorgamiento:'',
          fechaProximoControl:'',
          asignado:false,
          folio: folioCompleto,
          asignadoRC:false});
      }

    }
    if(mapeoLicenciasRegistroCivil && mapeoLicenciasRegistroCivil.length > 0){
    // En caso de que no exista la licencia incluída por el sgl la incluímos desde registro civil
    mapeoLicenciasRegistroCivil.forEach(lrc => {
      // Recoger valores
      const keysClasesLicencias = Object.keys(ClasesLicenciasIds);

      if(keysClasesLicencias.includes(lrc.licTipo.toUpperCase())){

          let recuperacionLicenciaSGL = clasesLicenciasFechas.filter(clf => clf.idClaseLicencia == ClasesLicenciasIds[lrc.licTipo.toUpperCase()]);

          if(recuperacionLicenciaSGL && recuperacionLicenciaSGL.length == 1 && recuperacionLicenciaSGL[0].asignado == false){
            recuperacionLicenciaSGL[0].descClaseLicencia = lrc.licTipo.toUpperCase();
            recuperacionLicenciaSGL[0].fechaPrimerOtorgamiento = (lrc.licFechaPriLic)?moment(lrc.licFechaPriLic).format('DD/MM/YYYY'):'00/00/0000';
            recuperacionLicenciaSGL[0].fechaActualOtorgamiento = (lrc.licFechaUltLic)?moment(lrc.licFechaUltLic).format('DD/MM/YYYY'):'00/00/0000';
            recuperacionLicenciaSGL[0].fechaProximoControl = (lrc.licFechaPrxCtl)?moment(lrc.licFechaPrxCtl).format('DD/MM/YYYY'):'00/00/0000';
            recuperacionLicenciaSGL[0].asignado = true;
            recuperacionLicenciaSGL[0].asignadoRC = true;
            

          }
        }
      })
    }

    return clasesLicenciasFechas;
  }

  async imprimirCabeceraDocumento(Jimp, image, documentoLicencia, RutaFuenteCourerNew38, RutaFuenteCourerNew18){
    
    await Jimp.loadFont(RutaFuenteCourerNew38).then(fontCourerNew38 => {
      // Zona de cabecera
      image.print(fontCourerNew38,2020,116, 
      { 
        //text:'00/00/00',
        text: (documentoLicencia.papelSeguridad.LetrasFolio && documentoLicencia.papelSeguridad.Folio)?(documentoLicencia.papelSeguridad.LetrasFolio + ' ' + documentoLicencia.papelSeguridad.Folio):'',
        alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      396,
      35);
    });


    await Jimp.loadFont(RutaFuenteCourerNew18).then(fontCourerNew18 => {      
      image.print(fontCourerNew18,2020,175, 
      { 
        text: (documentoLicencia.papelSeguridad.lotepapelseguridad.proveedor.nombre)?documentoLicencia.papelSeguridad.lotepapelseguridad.proveedor.nombre:'',
        alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      396,
      23);
    });
    
  }

  async imprimirZonaIdentificacionConductor(Jimp, image, tcl, RutaFuente){


    let runConPuntos = '';

    if(tcl.tramite.solicitudes.postulante.RUN && tcl.tramite.solicitudes.postulante.RUN.toString().length > 6){
    
      if(tcl.tramite.solicitudes.postulante.RUN.toString().length == 8){
        runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 2), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(2)].join('');  
        runConPuntos = [runConPuntos.slice(0, 6), '.', runConPuntos.slice(6)].join('');
      }
      else{
        runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 1), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(1)].join('');  
        runConPuntos = [runConPuntos.slice(0, 5), '.', runConPuntos.slice(5)].join('');
      }
    }
    else{
      runConPuntos = tcl.tramite.solicitudes.postulante.RUN;
    }

    runConPuntos = runConPuntos + '-' + tcl.tramite.solicitudes.postulante.DV;

    await Jimp.loadFont(RutaFuente).then(font => {
     
      // Foto
      if(tcl.tramite.solicitudes.postulante.imagenesPostulante && tcl.tramite.solicitudes.postulante.imagenesPostulante.length > 0){
        Jimp.read(Buffer.from(tcl.tramite.solicitudes.postulante.imagenesPostulante[0].archivo.toString().split(',')[1],'base64'), (err, sec_img) => {
          if(err) {
              //console.log(err);
          } else {

            sec_img.resize(390, 490);
            image.blit(sec_img, 125, 515);
          }
        });
      }      


      image.print(font,738,465,
      // RUN
      runConPuntos,
      250,
      (err, image, { x, y }) => {});

      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (tcl.tramite.solicitudes.postulante.Nombres).toUpperCase(),
        28,
        27,
        2,
        738,
        510
      )

      // Apellidos  730, 590, 
      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (tcl.tramite.solicitudes.postulante.ApellidoPaterno + ' ' + tcl.tramite.solicitudes.postulante.ApellidoMaterno).toUpperCase(),
        28,
        27,
        2,
        738,
        590
      )      

      image.print(font, 738, 670, 
      // Fecha de nacimiento
      (tcl.tramite.solicitudes.postulante.FechaNacimiento)?moment(tcl.tramite.solicitudes.postulante.FechaNacimiento).format('DD/MM/YYYY'):'',
      400,
      (err, image, { x, y }) => {});
        
      image.print(font, 738, 811,
      // Comuna 
      (tcl.tramite.solicitudes.postulante.comunas.Nombre)?tcl.tramite.solicitudes.postulante.comunas.Nombre.toUpperCase():'',
      400,
      (err, image, { x, y }) => {});  
      
      let calle = (tcl.tramite.solicitudes.postulante.Calle && tcl.tramite.solicitudes.postulante.Calle != null) ?
      tcl.tramite.solicitudes.postulante.Calle : "";

      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (calle).toUpperCase(),
        28,
        27,
        2,
        738,
        891
      )

      image.print(font, 738, 972, 
      // Número
      (tcl.tramite.solicitudes.postulante.CalleNro)?tcl.tramite.solicitudes.postulante.CalleNro:'',
      400,
      (err, image, { x, y }) => {}); 
      
      image.print(font, 1146, 972, 
      // Letra
      (tcl.tramite.solicitudes.postulante.Letra)?tcl.tramite.solicitudes.postulante.Letra:'',
      400,
      (err, image, { x, y }) => {});  
      
      image.print(font, 425, 1016, 
      // Resto Dirección
      (tcl.tramite.solicitudes.postulante.RestoDireccion)?tcl.tramite.solicitudes.postulante.RestoDireccion:'',
      800,   
      (err, image, { x, y }) => {});      

    }); 
  }

  async imprimirZonaDatosLicenciaOtorgada(Jimp, image, documentoLicencia, tcl, RutaFuente){
  
    if(documentoLicencia.papelSeguridad && documentoLicencia.papelSeguridad.LetrasFolio && documentoLicencia.papelSeguridad.Folio){
      await Jimp.loadFont(RutaFuente).then(font => {
          
        image.print(font,1882,468,
          // Folio Licencia Otorgada
          documentoLicencia.papelSeguridad.LetrasFolio + ' ' + documentoLicencia.papelSeguridad.Folio,
          400,
          (err, image, { x, y }) => {
            // Comuna Licencia Otorgada
            image.print(font, 1882, 513, 
              tcl.tramite.solicitudes.postulante.comunas.Nombre,
            400,
            (err, image, { x, y }) => {
              image.print(font, 1882, 592, 
                // Restricciones de Licencia Otorgada (Cod. Reg. Civil)
                '',
              400,
              (err, image, { x, y }) => {
                image.print(font, 1430, 717, 
                  // Observaciones del Otorgamiento
                  documentoLicencia.Observaciones,
                800,
                (err, image, { x, y }) => {
    
                });
              });
            });    
        })
      });
    }  

  }  

  async imprimirZonaEscuelaConductor(Jimp, image, datosConLicenciasFechas : { idClaseLicencia: string, 
                                                                              descClaseLicencia: string,
                                                                              fechaPrimerOtorgamiento: string, 
                                                                              fechaActualOtorgamiento: string, 
                                                                              fechaProximoControl: string,
                                                                              asignado: boolean
                                                                            }[], 
                                                                            RutaFuente)
  {

    //const posX : number = 1700;
    //const posY : number = 980;

    let clasesLicenciasAsignadas : string[] = datosConLicenciasFechas.filter(x => x.asignado).map(y => y.descClaseLicencia);

    let stringClases = this.construirArrayToCadenaConEspacios(clasesLicenciasAsignadas);

    await Jimp.loadFont(RutaFuente).then(font => {
          
      image.print(font, 1696, 984, 
        // Clase(s) de Licencia
        stringClases,
        400,
        (err, image, { x, y }) => {
          image.print(font, 1696, 1064, 
            // Folio(s) de certificado(s)
            '',
            400,
            (err, image, { x, y }) => {
              image.print(font, 1696, 1073, 
                // Fecha(s) de aprobación
                '',
                400,
                (err, image, { x, y }) => {
                  image.print(font, 1696, 1224,
                    // RUT Escuela(s)
                    '',
                    100,
                    (err, image, { x, y }) => {
            
                  })
              })
          })   
      })
    });
  }

  async imprimirZonaLicenciaConductor(Jimp, image, tcl, codigoQR, RutaFuenteFranklinGothicBook, RutaFuenteCourierNew, documentoLicencia : DocumentosLicencia){
    let runConPuntos = '';

    if(tcl.tramite.solicitudes.postulante.RUN && tcl.tramite.solicitudes.postulante.RUN.toString().length > 6){
    
      if(tcl.tramite.solicitudes.postulante.RUN.toString().length == 8){
        runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 2), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(2)].join('');  
        runConPuntos = [runConPuntos.slice(0, 6), '.', runConPuntos.slice(6)].join('');
      }
      else{
        runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 1), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(1)].join('');  
        runConPuntos = [runConPuntos.slice(0, 5), '.', runConPuntos.slice(5)].join('');
      }
    }
    else{
      runConPuntos = tcl.tramite.solicitudes.postulante.RUN;
    }

    runConPuntos = runConPuntos + '-' + tcl.tramite.solicitudes.postulante.DV;

    await Jimp.loadFont(RutaFuenteFranklinGothicBook).then(font => {

      // Foto
      if(tcl.tramite.solicitudes.postulante.imagenesPostulante && tcl.tramite.solicitudes.postulante.imagenesPostulante.length > 0){

        Jimp.read(Buffer.from(tcl.tramite.solicitudes.postulante.imagenesPostulante[0].archivo.toString().split(',')[1],'base64'), (err, sec_img) => {
          if(err) {
              console.log(err);
          } else {

            sec_img.resize(331, 449);
            image.blit(sec_img, 572, 2697);
          }
        })
      }

      // QR
      Jimp.read(Buffer.from(codigoQR.split(',')[1],'base64'), (err, sec_img) => {
        if(err) {
            console.log(err);
        } else {

          sec_img.resize(194, 194);
          image.blit(sec_img, 1266, 2606);
        }
      });

      let imagenBarraRUNBase64String = imagenBarraRUNBase64;

      // Barra RUN
      Jimp.read(Buffer.from(imagenBarraRUNBase64String, 'base64'), (err, sec_img) => {
        if(err) {
            console.log(err);
        } else {

          sec_img.resize(359, 31);
          image.blit(sec_img, 558, 3072);
        }
      });  
      
      image.print(font, 942, 2744,
      // RUN
      runConPuntos,
      200,
      (err, image, { x, y }) => {});

      // Apellidos
      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (tcl.tramite.solicitudes.postulante.ApellidoPaterno).toUpperCase(),
        25,
        29,
        1,
        942,
        2800
      ) 

      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (tcl.tramite.solicitudes.postulante.ApellidoMaterno).toUpperCase(),
        25,
        29,
        1,
        942,
        2832
      )       
      
      // Nombres
      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (tcl.tramite.solicitudes.postulante.Nombres).toUpperCase(),
        25,
        29,
        1,
        942,
        2888
      )
      
      image.print(font, 943, 2943, 
      // Municipalidad
      (tcl.tramite.solicitudes.postulante.comunas)?tcl.tramite.solicitudes.postulante.comunas.Nombre.toUpperCase():'',
      400,
      (err, image, { x, y }) => {});      

      let calle = (tcl.tramite.solicitudes.postulante.Calle && tcl.tramite.solicitudes.postulante.Calle != null) ?
                  tcl.tramite.solicitudes.postulante.Calle : "";

      let calleNro = (tcl.tramite.solicitudes.postulante.CalleNro && tcl.tramite.solicitudes.postulante.CalleNro != null) ?
                  tcl.tramite.solicitudes.postulante.CalleNro : "";   
                  
      let letra = (tcl.tramite.solicitudes.postulante.Letra && tcl.tramite.solicitudes.postulante.Letra != null) ?
      tcl.tramite.solicitudes.postulante.Letra : ""; 

      // DIRECCION
      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (calle + ' ' + calleNro + ' ' + letra).toUpperCase(),
        25,
        29,
        2,
        942,
        2999
      )

      // Imprimir restricciones
      let restriccionesLicenciaString : string = "";
      
      if( documentoLicencia && 
          documentoLicencia.documentoLicenciaRestricciones && 
          documentoLicencia.documentoLicenciaRestricciones.length > 0 &&
          documentoLicencia.documentoLicenciaRestricciones.filter(x => x.idrestriccion == 12).length < 1){ // si tiene restricciones y las restricciones son diferente a "Sin restricciones"
        let restriccionesLicenciaArray : RestriccionesEntity[] = documentoLicencia.documentoLicenciaRestricciones.map(x => x.restricciones);
        let restriccionesLicenciaArrayString : string[] = restriccionesLicenciaArray.map( x => x.nombre);
        restriccionesLicenciaString = restriccionesLicenciaArrayString.join('/');
      }

      // RESTRICCIONES / OBSERVACIONES
      this.procesarCadenaConMaximoSaltosLinea(Jimp,
        image,
        font,
        (restriccionesLicenciaString).toUpperCase(),
        28,
        29,
        2,
        942,
        3096
      )


    }); 
    
    
      //RUN Encima de la barra
      await Jimp.loadFont(RutaFuenteCourierNew).then(font => {
      
        let runConPuntos = '';
        if(tcl.tramite.solicitudes.postulante.RUN.toString().length == 8){
          runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 2), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(2)].join('');  
          runConPuntos = [runConPuntos.slice(0, 6), '.', runConPuntos.slice(6)].join('');
        }
        else{
          runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 1), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(1)].join('');  
          runConPuntos = [runConPuntos.slice(0, 5), '.', runConPuntos.slice(5)].join('');
        }

        runConPuntos = runConPuntos + ' - ' + tcl.tramite.solicitudes.postulante.DV;


      image.print(font,565,3070,
        // RUN Encima de barra
        'RUN ' + runConPuntos,
        600,
        (err, image, { x, y }) => {

      })





    });



  }
  
  async imprimirZonaClasesDeLicencia(Jimp, image, datosConLicenciasFechas : { idClaseLicencia: string, 
                                                                              descClaseLicencia: string,
                                                                              fechaPrimerOtorgamiento: string, 
                                                                              fechaActualOtorgamiento: string, 
                                                                              fechaProximoControl: string,
                                                                              asignado: boolean,
                                                                              folio: string
                                                                            }[], 
                                                                            RutaFuenteFranklinGB,
                                                                            documentoLicencia,
                                                                            RutaFuenteCourier34,
                                                                            RutaFuenteCourier14){



    let folioCompleto : string = (datosConLicenciasFechas && datosConLicenciasFechas.length > 0 && datosConLicenciasFechas[0].folio)?datosConLicenciasFechas[0].folio:'';

    // Se carga la fuente
    const fontCourier32 = await Jimp.loadFont(RutaFuenteCourier34);  

    //Se carga una imagen que será la que se mueva a la plantilla
    const imagenTextoRotadoFolio = await Jimp.create(300,35);

    imagenTextoRotadoFolio
    .print(fontCourier32,0,0,
      { 
        text: folioCompleto,
        alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      300,
      35)
    .rotate(90);

    image.blit(imagenTextoRotadoFolio, 2345, 2622);

    let entidadProveedora : string = (documentoLicencia.papelSeguridad.lotepapelseguridad.proveedor.nombre)?documentoLicencia.papelSeguridad.lotepapelseguridad.proveedor.nombre:'';

    const fontCourier14 = await Jimp.loadFont(RutaFuenteCourier14);                                                                          

    //Se carga una imagen que será la que se mueva a la plantilla
    const imagenTextoRotadoEntidadProveedora = await Jimp.create(273,18);

    imagenTextoRotadoEntidadProveedora
    .print(fontCourier14,0,0,
      { 
        text: entidadProveedora,
        alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      273,
      18)
    .rotate(90);

    image.blit(imagenTextoRotadoEntidadProveedora, 2385, 2625);

    await Jimp.loadFont(RutaFuenteFranklinGB).then(font => {

      
      image.print(font,276,1152,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);      
      

      image.print(font,276,1188,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,276,1224,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      

      image.print(font,276,1260,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,276,1295,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,276,1331,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,276,1367,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      

      image.print(font,276,1403,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);

      image.print(font,276,1439,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      image.print(font,276,1475,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      
      image.print(font,276,1511,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      image.print(font,276,1546,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);

    });


    await Jimp.loadFont(RutaFuenteFranklinGB).then(font => {

      
      image.print(font,459,1152,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);      
      

      image.print(font,459,1188,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,459,1224,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      

      image.print(font,459,1260,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,459,1295,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,459,1331,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,459,1367,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      

      image.print(font,459,1403,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);

      image.print(font,459,1439,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      image.print(font,459,1475,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      
      image.print(font,459,1511,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      image.print(font,459,1546,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);

    });
    


    await Jimp.loadFont(RutaFuenteFranklinGB).then(font => {

      
      image.print(font,641,1152,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);      
      

      image.print(font,641,1188,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,641,1224,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      

      image.print(font,641,1260,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,641,1295,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,641,1331,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);


      image.print(font,641,1367,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      

      image.print(font,641,1403,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);

      image.print(font,641,1439,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      image.print(font,641,1475,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      
      image.print(font,641,1511,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);
      
      image.print(font,641,1546,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      182,
      35);

    });
    
  } 
  
  async imprimirZonaDatosLicenciaAnterior(Jimp, image, documentoLicenciaAnterior: ClasesLicenciasDTO[], RutaFuente){
    if(documentoLicenciaAnterior && documentoLicenciaAnterior.length > 0){

      //let tramitesAsociados : TramitesEntity[] = documentoLicenciaAnterior.documentoLicenciaTramite.map(dlt => dlt.tramites);
      const documentoParaFolio : ClasesLicenciasDTO[] = documentoLicenciaAnterior.filter(dla => dla.Folio != '' && dla.Folio != '0' && dla.LetrasFolio != '' && dla.LetrasFolio != '0');
      
      const LetrasFolio = (documentoParaFolio && documentoParaFolio.length > 0)?documentoParaFolio[0].LetrasFolio:'';
      const Folio = (documentoParaFolio && documentoParaFolio.length > 0)?documentoParaFolio[0].Folio:'';
      
      let clasesAnterioresConcatenedas : string = this.construirArrayToCadenaConEspacios(documentoLicenciaAnterior.map(ta => ta.Abreviacion));

      // Zona de cabecera
      await Jimp.loadFont(RutaFuente).then(font => {
        
        image.print(font,1404,1400,
          // FOLIO LICENCIA ANTERIOR
          LetrasFolio + ' ' + Folio,
          400,
          (err, image, { x, y }) => {
            image.print(font,2048,1400,
              // CLASE(S) ANTERIORE(S)
              clasesAnterioresConcatenedas,
              400,
              (err, image, { x, y }) => {
                image.print(font,1404,14444,
                  // COMUNA LICENCIA ANTERIOR
                  400,
                  (err, image, { x, y }) => {
          
                })     
            }) 
        })
      });
    }
  }  

  async imprimirZonaLicenciaConductorClasesDeLicencia(Jimp, image, datosConLicenciasFechas : { idClaseLicencia: string, 
                                                                                                descClaseLicencia: string,
                                                                                                fechaPrimerOtorgamiento: string, 
                                                                                                fechaActualOtorgamiento: string, 
                                                                                                fechaProximoControl: string,
                                                                                                asignado: boolean
                                                                                              }[],
                                                                                              RutaFuenteFranklinGB){

    let posX : number = 1727;
    const posY : number = 2676;
    const espacioEntreY = 38;
    const espacioEntreX = 189;                                                                                
   
    await Jimp.loadFont(RutaFuenteFranklinGB).then(font => {

      
      image.print(font,1706,2674,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);      
      

      image.print(font,1706,2711,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1706,2748,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      

      image.print(font,1706,2786,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1706,2823,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1706,2862,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1706,2898,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      

      image.print(font,1706,2936,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);

      image.print(font,1706,2974,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      image.print(font,1706,3012,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      
      image.print(font,1706,3049,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      image.print(font,1706,3087,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);

    });


    await Jimp.loadFont(RutaFuenteFranklinGB).then(font => {

      
      image.print(font,1897,2674,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);      
      

      image.print(font,1897,2711,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1897,2748,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      

      image.print(font,1897,2786,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1897,2823,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1897,2862,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,1897,2898,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      

      image.print(font,1897,2936,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);

      image.print(font,1897,2974,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      image.print(font,1897,3012,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      
      image.print(font,1897,3049,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      image.print(font,1897,3087,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);

    });
    


    await Jimp.loadFont(RutaFuenteFranklinGB).then(font => {

      
      image.print(font,2088,2674,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);      
      

      image.print(font,2088,2711,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,2088,2748,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      

      image.print(font,2088,2786,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,2088,2823,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,2088,2862,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);


      image.print(font,2088,2898,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      

      image.print(font,2088,2936,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);

      image.print(font,2088,2974,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      image.print(font,2088,3012,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      
      image.print(font,2088,3049,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);
      
      image.print(font,2088,3087,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      187,
      35);

    });
                                                                                
  }  

  async imprimirZonaExamenesAsociadosIdoneidadMoral(Jimp, image, documentoLicencia : DocumentosLicencia, RutaFuente){

    if(documentoLicencia.documentoLicenciaTramite && documentoLicencia.documentoLicenciaTramite.length > 0)
    {
        // Filtramos por pre-idoneidad moral
        let colaExamNMPreIdon : ColaExaminacionTramiteNMEntity[] = (documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM)?documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM
                                                                                                                  .filter(x => x.colaExaminacion.idTipoExaminacion == TipoExaminacionesIds.ExamenPreIM):[];

        // Filtramos por pre-idoneidad moral
        let colaExamNMIdon : ColaExaminacionTramiteNMEntity[] = (documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM)? documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM
                                                                                                                  .filter(x => x.colaExaminacion.idTipoExaminacion == TipoExaminacionesIds.ExamenIM):[];                                                                                                          
        
        // Zona de cabecera
        await Jimp.loadFont(RutaFuente).then(font => {
          
          image.print(font,868,1670,
            // Folio Pre-Evaluacion de Idoneidad Moral
            (colaExamNMPreIdon && colaExamNMPreIdon.length > 0)?colaExamNMPreIdon[0].colaExaminacion.resultadoExaminacionCola[0].idResultadoExam:'',
            400,
            (err, image, { x, y }) => {
              image.print(font,868,1715,
                // Folio Evaluación de Idoneidad Moral
                (colaExamNMIdon && colaExamNMIdon.length > 0)?colaExamNMIdon[0].colaExaminacion.resultadoExaminacionCola[0].idResultadoExam:'',
                400,
                (err, image, { x, y }) => {
                  image.print(font,132,1804,
                    // Observaciones Idoneidad Moral
                    (colaExamNMIdon && colaExamNMIdon.length > 0 && colaExamNMIdon[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones != null)?(colaExamNMIdon[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones):'',
                    400,
                    (err, image, { x, y }) => {
            
                  })     
              }) 
          })
        }); 
    }
  }
  
  async imprimirZonaExamenesAsociadosExamenMedico(Jimp, image, documentoLicencia : DocumentosLicencia, RutaFuente){
    if(documentoLicencia.documentoLicenciaTramite && documentoLicencia.documentoLicenciaTramite.length > 0)
    {
        // Filtramos por examen medico
        let colaExam : ColaExaminacionTramiteNMEntity[] = (documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM)?documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM
                                                                                                                  .filter(x => x.colaExaminacion.idTipoExaminacion == TipoExaminacionesIds.ExamenMedico):[];

                                                                                                            
        
        // Zona de cabecera
        await Jimp.loadFont(RutaFuente).then(font => {
          
          image.print(font,2054,1670,
            // Folio Exámen Médico
            (colaExam && colaExam.length > 0) ? colaExam[0].colaExaminacion.resultadoExaminacionCola[0].idResultadoExam : '',
            400,
            (err, image, { x, y }) => {
              image.print(font,2054,1715,
                // Funcionario/a responsable exámen médico
                (colaExam && colaExam.length > 0) ? this.procesarRUNConPuntos(colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.usuarioAprob.RUN.toString()) + '-' + 
                  colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.usuarioAprob.DV + ' ' : '',
                400,
                (err, image, { x, y }) => {
                  image.print(font,1319,1804,
                    // Observaciones Exámen Médico
                    (colaExam && colaExam.length > 0) ? ((colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones != null)?(colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones):''):'',
                    400,
                    (err, image, { x, y }) => {
            
                  })     
              }) 
          })
        }); 
    }
  }
  
  async imprimirZonaExamenesAsociadosExamenTeorico(Jimp, image, documentoLicencia : DocumentosLicencia, RutaFuente){
    
    if(documentoLicencia.documentoLicenciaTramite && documentoLicencia.documentoLicenciaTramite.length > 0)
    {

        // Filtramos por teórico
        let colaExam : ColaExaminacionTramiteNMEntity[] = documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM
                                                                          .filter(x => x.colaExaminacion.idTipoExaminacion == TipoExaminacionesIds.ExamenTeorico);

        let idsFolios : string [] = (colaExam && colaExam.length > 0) ?  colaExam[0].colaExaminacion.resultadoExaminacionCola.map( x => x.idResultadoExam.toString()) : [];

        let nombresResponsables : string [] = (colaExam && colaExam.length > 0) ?  colaExam[0].colaExaminacion.resultadoExaminacionCola.map( x => (  this.procesarRUNConPuntos(x.resultadoExaminacion.usuarioAprob.RUN.toString()) + '-' +
                                                                                                                x.resultadoExaminacion.usuarioAprob.DV)) : [];

        let idsConcatenados = this.construirArrayToCadenaConEspacios(idsFolios);

        let responsablesConcatenados = this.construirArrayToCadenaConEspacios(nombresResponsables);
        
        // Zona de cabecera
        await Jimp.loadFont(RutaFuente).then(font => {
          
          image.print(font,658,1966,
            // Folio Teórico
            idsConcatenados,
            400,
            (err, image, { x, y }) => {
              image.print(font,658,2082,
                // Folio Teórico
                responsablesConcatenados,
                400,
                (err, image, { x, y }) => {
                  image.print(font,132,2203,
                    // Observaciones Teórico
                    (colaExam && colaExam.length > 0) ? ((colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones != null)?(colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones):''):'',
                    400,
                    (err, image, { x, y }) => {
            
                  })     
              }) 
          })
        }); 

    }
      
  } 

  async imprimirZonaExamenesAsociadosExamenPractico(Jimp, image, documentoLicencia : DocumentosLicencia, RutaFuente){
    
    if(documentoLicencia.documentoLicenciaTramite && documentoLicencia.documentoLicenciaTramite.length > 0)
    {

        // Filtramos por teórico
        let colaExam : ColaExaminacionTramiteNMEntity[] = documentoLicencia.documentoLicenciaTramite[0].tramites.colaExaminacionNM
                                                                          .filter(x => x.colaExaminacion.idTipoExaminacion == TipoExaminacionesIds.ExamenPractico);

        let idsFolios : string [] = (colaExam && colaExam.length > 0) ?   colaExam[0].colaExaminacion.resultadoExaminacionCola.map( x => x.idResultadoExam.toString()) : [];

        let nombresResponsables : string [] = (colaExam && colaExam.length > 0) ?  colaExam[0].colaExaminacion.resultadoExaminacionCola.map( x => (  this.procesarRUNConPuntos(x.resultadoExaminacion.usuarioAprob.RUN.toString()) + '-' +
                                                                                                                x.resultadoExaminacion.usuarioAprob.DV)) : [];


        let idsConcatenados = this.construirArrayToCadenaConEspacios(idsFolios);

        let responsablesConcatenados = this.construirArrayToCadenaConEspacios(nombresResponsables);
        
        // Zona de cabecera
        await Jimp.loadFont(RutaFuente).then(font => {
          
          image.print(font,1843,1966,
            // Folio Pre-Evaluacion de Idoneidad Moral
            idsConcatenados,
            400,
            (err, image, { x, y }) => {
              image.print(font,1843,2082,
                // Folio Evaluación de Idoneidad Moral
                responsablesConcatenados,
                400,
                (err, image, { x, y }) => {
                  image.print(font,1319,2203,
                    // Observaciones Idoneidad Moral
                    (colaExam && colaExam.length > 0) ?  ((colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones != null)?(colaExam[0].colaExaminacion.resultadoExaminacionCola[0].resultadoExaminacion.observaciones):''):'',
                    400,
                    (err, image, { x, y }) => {
            
                  })     
              }) 
          })
        }); 

    }
      
  } 
  
  async imprimirZonaValidacionDireccionTransito(Jimp, image, documentoLicencia : DocumentosLicencia, RutaFuente){
    
    // Zona de cabecera
    await Jimp.loadFont(RutaFuente).then(font => {
      
      image.print(font,1012,2405,
        // RUN DIRECTOR(A) DE TRÁNSITO O JEFE/A DEPTO. DE LICENCIA
        this.procesarRUNConPuntos(documentoLicencia.usuario.RUN.toString()) + '-' + documentoLicencia.usuario.DV,
        400,
        (err, image, { x, y }) => {

          
      })
    }); 
      
  }
  
  async obtenerAnversoLicencia(Jimp, image, tcl, codigoQR, imageFontFGB28Route, RutaFuenteCourier, documentoLicencia: DocumentosLicencia){
    try{

      let runConPuntos = '';

      if(tcl.tramite.solicitudes.postulante.RUN && tcl.tramite.solicitudes.postulante.RUN.toString().length > 6){
      
        if(tcl.tramite.solicitudes.postulante.RUN.toString().length == 8){
          runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 2), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(2)].join('');  
          runConPuntos = [runConPuntos.slice(0, 6), '.', runConPuntos.slice(6)].join('');
        }
        else{
          runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 1), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(1)].join('');  
          runConPuntos = [runConPuntos.slice(0, 5), '.', runConPuntos.slice(5)].join('');
        }
      }
      else{
        runConPuntos = tcl.tramite.solicitudes.postulante.RUN;
      }
  
      runConPuntos = runConPuntos + ' - ' + tcl.tramite.solicitudes.postulante.DV;

        // Zona de cabecera
        await Jimp.loadFont(imageFontFGB28Route).then(font => {

          if(tcl.tramite.solicitudes.postulante.imagenesPostulante && tcl.tramite.solicitudes.postulante.imagenesPostulante.length > 0){        
            // Foto
            Jimp.read(Buffer.from(tcl.tramite.solicitudes.postulante.imagenesPostulante[0].archivo.toString().split(',')[1],'base64'), (err, sec_img) => {
              if(err) {
                  console.log(err);
              } else {

                sec_img.resize(332, 449);
                image.blit(sec_img, 32, 102);
              }
            })

          }

          // QR
          Jimp.read(Buffer.from(codigoQR.split(',')[1],'base64'), (err, sec_img) => {
            if(err) {
                console.log(err);
            } else {

              sec_img.resize(195, 195);
              image.blit(sec_img, 726, 12);
            }
          });

          let calle = (tcl.tramite.solicitudes.postulante.Calle && tcl.tramite.solicitudes.postulante.Calle != null) ?
                      tcl.tramite.solicitudes.postulante.Calle : "";

          let calleNro = (tcl.tramite.solicitudes.postulante.CalleNro && tcl.tramite.solicitudes.postulante.CalleNro != null) ?
                      tcl.tramite.solicitudes.postulante.CalleNro : "";   
                    
          let letra = (tcl.tramite.solicitudes.postulante.Letra && tcl.tramite.solicitudes.postulante.Letra != null) ?
                      tcl.tramite.solicitudes.postulante.Letra : "";                    



          image.print(font, 403, 148, 
          // RUN
          runConPuntos,
          400,
          (err, image, { x, y }) => {

          });   

          // Apellidos
          this.procesarCadenaConMaximoSaltosLinea(Jimp,
            image,
            font,
            (tcl.tramite.solicitudes.postulante.ApellidoPaterno).toUpperCase().toUpperCase(),
            25,
            29,
            1,
            403,
            203
          )
          
          this.procesarCadenaConMaximoSaltosLinea(Jimp,
            image,
            font,
            (tcl.tramite.solicitudes.postulante.ApellidoMaterno).toUpperCase().toUpperCase(),
            25,
            29,
            1,
            403,
            236
          )           
          
          // Nombres
          this.procesarCadenaConMaximoSaltosLinea(Jimp,
            image,
            font,
            this.procesarCadenaConMaximoLineaSimple(tcl.tramite.solicitudes.postulante.Nombres,35).toUpperCase(),
            25,
            29,
            1,
            403,
            292
          )

          image.print(font, 403, 347, 
          // Municipalidad
          this.procesarCadenaConMaximoLineaSimple(tcl.tramite.solicitudes.postulante.comunas.Nombre,35).toUpperCase(),
          347,
          (err, image, { x, y }) => {

          });

          // DIRECCION
          this.procesarCadenaConMaximoSaltosLinea(Jimp,
            image,
            font,
            (calle + ' ' + calleNro + ' ' + letra).toUpperCase(),
            25,
            29,
            2,
            403,
            403
          )

          // Imprimir restricciones
          let restriccionesLicenciaString : string = "";
          
          if( documentoLicencia && 
              documentoLicencia.documentoLicenciaRestricciones && 
              documentoLicencia.documentoLicenciaRestricciones.length > 0 &&
              documentoLicencia.documentoLicenciaRestricciones.filter(x => x.idrestriccion == 12).length < 1){ // si tiene restricciones y las restricciones son diferente a "Sin restricciones"
            let restriccionesLicenciaArray : RestriccionesEntity[] = documentoLicencia.documentoLicenciaRestricciones.map(x => x.restricciones);
            let restriccionesLicenciaArrayString : string[] = restriccionesLicenciaArray.map( x => x.nombre);
            restriccionesLicenciaString = restriccionesLicenciaArrayString.join('/');
          }

          // RESTRICCIONES / OBSERVACIONES
          this.procesarCadenaConMaximoSaltosLinea(Jimp,
            image,
            font,
            (restriccionesLicenciaString).toUpperCase(),
            28,
            29,
            2,
            403,
            500
          )




        });

        let imagenBarraRUNBase64String = imagenBarraRUNBase64;

        // Barra RUN
        Jimp.read(Buffer.from(imagenBarraRUNBase64String, 'base64'), (err, sec_img) => {
          if(err) {
              console.log(err);
          } else {

            sec_img.resize(359, 31);
            image.blit(sec_img, 19, 478);
          }
        });

      //RUN Encima de la barra
      await Jimp.loadFont(RutaFuenteCourier).then(font => {
        let runConPuntos = '';
        if(tcl.tramite.solicitudes.postulante.RUN.toString().length == 8){
          runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 2), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(2)].join('');  
          runConPuntos = [runConPuntos.slice(0, 6), '.', runConPuntos.slice(6)].join('');
        }
        else{
          runConPuntos = [tcl.tramite.solicitudes.postulante.RUN.toString().slice(0, 1), '.', tcl.tramite.solicitudes.postulante.RUN.toString().slice(1)].join('');  
          runConPuntos = [runConPuntos.slice(0, 5), '.', runConPuntos.slice(5)].join('');
        }

        runConPuntos = runConPuntos + ' - ' + tcl.tramite.solicitudes.postulante.DV;
  
        image.print(font,26,475,
          // RUN Encima de barra
          'RUN ' + runConPuntos,
          600,
          (err, image, { x, y }) => {
  
        })
  
      });        

        let imageBase64 : string = await image.getBase64Async(Jimp.MIME_PNG);

        return imageBase64;
      }
      catch(Error){
        console.log(Error);
      }    
      
  }

  async obtenerReversoLicencia(Jimp, 
                               image, 
                               datosConLicenciasFechas : DatosClasesLicenciasConFechas[],
                               RutaFuenteFranklingGothicBook,
                               documentoLicencia,
                               RutaFuenteCourier34,
                               RutaFuenteCourier14){

    let folioCompleto : string = (datosConLicenciasFechas && datosConLicenciasFechas.length > 0 && datosConLicenciasFechas[0].folio)?datosConLicenciasFechas[0].folio:'';                            
    
    // Impresión de fechas
 
    await Jimp.loadFont(RutaFuenteFranklingGothicBook).then(font => {

      
      image.print(font,223,78,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);      
      

      image.print(font,223,116,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,223,154,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      

      image.print(font,223,191,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,223,229,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,223,266,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,223,304,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      

      image.print(font,223,341,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);

      image.print(font,223,379,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      image.print(font,223,416,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      
      image.print(font,223,454,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      image.print(font,223,491,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaPrimerOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);

    });


    await Jimp.loadFont(RutaFuenteFranklingGothicBook).then(font => {

      
      image.print(font,413,78,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);      
      

      image.print(font,413,116,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,413,153,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      

      image.print(font,413,191,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,413,229,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,413,266,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,413,304,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      

      image.print(font,413,341,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);

      image.print(font,413,379,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      image.print(font,413,416,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      
      image.print(font,413,454,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      image.print(font,413,491,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaActualOtorgamiento,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);

    });
    


    await Jimp.loadFont(RutaFuenteFranklingGothicBook).then(font => {

      
      image.print(font,603,78,
      // CLASE C
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '2')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);      
      

      image.print(font,603,116,
      // CLASE B
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '1')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,603,153,
      // CLASE A4
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '8')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      

      image.print(font,603,191,
      // CLASE A5
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '9')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,603,229,
      // CLASE A2*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '15')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,603,266,
      // CLASE A1
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '4')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);


      image.print(font,603,304,
      // Clase A2
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '6')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      

      image.print(font,603,341,
      // Clase A3
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '7')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);

      image.print(font,603,379,
      // Clase A1*
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '14')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      image.print(font,603,416,
      // Clase D
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '11')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      
      image.print(font,603,454,
      // Clase E
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '10')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);
      
      image.print(font,603,491,
      // Clase F
      
      { 
        //text:'00/00/00',
        text: datosConLicenciasFechas.filter(x => x.idClaseLicencia == '12')[0].fechaProximoControl,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      190,
      38);

    });    
    
    // // Se carga la fuente
    // const font = await Jimp.loadFont(RutaFuenteFranklingGothicBook);  

    // // Se carga una imagen que será la que se mueva a la plantilla
    // const imagenTextoRotado = await Jimp.create(150,50);

    // imagenTextoRotado //print(font,0,0, folioCompleto)
    // .print(font,0,0,
    //   { 
    //     text: folioCompleto,
    //     alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
    //     alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
    //   },
    //   150,
    //   50)
    // .rotate(90);

    // // Se imprime en la posición indicada
    // image.blit(imagenTextoRotado, 868, 180);




    //let folioCompleto : string = (datosConLicenciasFechas && datosConLicenciasFechas.length > 0 && datosConLicenciasFechas[0].folio)?datosConLicenciasFechas[0].folio:'';

    // Se carga la fuente
    const fontCourier32 = await Jimp.loadFont(RutaFuenteCourier34);  

    //Se carga una imagen que será la que se mueva a la plantilla
    const imagenTextoRotadoFolio = await Jimp.create(300,35);

    imagenTextoRotadoFolio
    .print(fontCourier32,0,0,
      { 
        text: folioCompleto,
        alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      300,
      35)
    .rotate(90);

    image.blit(imagenTextoRotadoFolio, 854, 23);

    let entidadProveedora : string = (documentoLicencia.papelSeguridad.lotepapelseguridad.proveedor.nombre)?documentoLicencia.papelSeguridad.lotepapelseguridad.proveedor.nombre:'';

    const fontCourier14 = await Jimp.loadFont(RutaFuenteCourier14);                                                                          

    //Se carga una imagen que será la que se mueva a la plantilla
    const imagenTextoRotadoEntidadProveedora = await Jimp.create(273,18);

    imagenTextoRotadoEntidadProveedora
    .print(fontCourier14,0,0,
      { 
        text: entidadProveedora,
        alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      },
      273,
      18)
    .rotate(90);

    image.blit(imagenTextoRotadoEntidadProveedora, 894, 25);


    let imageBase64 : string = await image.getBase64Async(Jimp.MIME_PNG);

    return imageBase64;
  }  
  
  construirArrayToCadenaConEspacios(arrayString : string[]){
    let stringRetorno = "";

    if(arrayString && arrayString.length > 0){
      let contador = 0;
      
      arrayString.forEach(s => {
        if(contador == 0)
          stringRetorno = arrayString[contador];
        else
          stringRetorno = stringRetorno + " " + arrayString[contador];

        contador++;
      })
    }

    return stringRetorno;
  }
  
  async crearCodigoQR(datosConHash: string) {
    const resultado: Resultado = new Resultado();

    var QRCode = require('qrcode');
    await QRCode.toDataURL(datosConHash, {
      errorCorrectionLevel: 'M',
    })
      .then(imgSRC => {
        resultado.Respuesta = imgSRC;
        resultado.Mensaje = 'Código QR generado correctamente';
        resultado.ResultadoOperacion = true;
      })
      .catch(err => {
        //console.error(err);

        resultado.Error = 'Error creando código QR';
        resultado.ResultadoOperacion = false;
      });

    return resultado;
  }  
    
  procesarCadenaConMaximoLineaSimple( cadenaCaracteres: string, 
                                      maximoPorLinea: number){

    if(cadenaCaracteres.length > maximoPorLinea && cadenaCaracteres.length > 3){     
      return (cadenaCaracteres.substring(0,maximoPorLinea) + '');

    }
    else{
      return cadenaCaracteres;
    }

  }

  private procesarCadenaConMaximoSaltosLinea( 
                                      Jimp,
                                      image,
                                      font,
                                      cadenaCaracteres: string,
                                      maximoCaracteresPorLinea: number,
                                      tamanyoLinea: number,
                                      numeroDeLineas: number,
                                      posX:number,
                                      posY:number
                                      ){


                                        // Calcular máximo de caracteres para imprimir
                                        let numeroMaximoCaracteres = maximoCaracteresPorLinea * numeroDeLineas;
                                        let stringImpresion = this.procesarCadenaConMaximoLineaSimple(cadenaCaracteres, numeroMaximoCaracteres);

                                        for(let i=0;i<numeroDeLineas;i++){
                                          // Si contiene aún caracteres el string en la posicion especifica, seguimos
                                          //if(stringImpresion.length < ((i+1)*maximoCaracteresPorLinea)){
                                            // Recoger cadena, si el length es menor que el final, sólo imprimos hasta el final
                                            let stringImpresionRecorrido = '';

                                            if(stringImpresion.length <= (((i+1)*maximoCaracteresPorLinea) )){
                                              stringImpresionRecorrido = stringImpresion.substring((((i)*maximoCaracteresPorLinea)), stringImpresion.length);
                                            }
                                            else{
                                              stringImpresionRecorrido = stringImpresion.substring((((i)*maximoCaracteresPorLinea)), (((i+1)*maximoCaracteresPorLinea) ));
                                            }
                                            // Posicion Y será multiplicada por cada iteracion para situarnos
                                            image.print(font, posX, posY+(tamanyoLinea*i), 
                                              // Substring para recoger la cadena correspondiente
                                              stringImpresionRecorrido,
                                              800,
                                              (err, image, { x, y }) => {
                            
                                            })  
                                          //}
                                        }
                                        
                                      

  }

  private procesarRUNConPuntos(RUN: string) : string{
    let runConPuntos : string = "";

    if(RUN.length == 8){
      runConPuntos = [RUN.toString().slice(0, 2), '.', RUN.toString().slice(2)].join('');  
      runConPuntos = [runConPuntos.slice(0, 6), '.', runConPuntos.slice(6)].join('');
    }
    else{
      runConPuntos = [RUN.toString().slice(0, 1), '.', RUN.toString().slice(1)].join('');  
      runConPuntos = [runConPuntos.slice(0, 5), '.', runConPuntos.slice(5)].join('');
    }

    return runConPuntos;
  }
}