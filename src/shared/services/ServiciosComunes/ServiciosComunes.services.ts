import {
    Injectable
} from '@nestjs/common';
import {
    InjectRepository
} from '@nestjs/typeorm';

import {
    TiposTramiteEntity
} from 'src/tipos-tramites/entity/tipos-tramite.entity';

import {
    EstadosTramiteEntity
} from 'src/tramites/entity/estados-tramite.entity';

import {
    Repository
} from 'typeorm';


import {
    Resultado
} from 'src/utils/resultado';

import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { TipoTramiteDto } from 'src/shared/DTO/tipoTramite.dto';
import { ClaseLicenciaDto } from 'src/shared/DTO/claseLicencia.dto';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { EstadoExaminacionDto } from 'src/shared/DTO/estadoExaminacion.dto';
import { TipoExaminacionDto } from 'src/shared/DTO/tipoExaminacion.dto';
import { EstadoTramiteDto } from 'src/shared/DTO/estadoTramites.dto';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { EstadoSolicitudDto } from 'src/shared/DTO/estadoSolicitud.dto';

@Injectable()
export class ServiciosComunesService {
    [x: string]: any;

    constructor(
        @InjectRepository(TiposTramiteEntity) private readonly tipoTramitesEntityRespository: Repository<TiposTramiteEntity>,
        @InjectRepository(EstadosTramiteEntity) private readonly estadosTramitesEntityRespository: Repository<EstadosTramiteEntity>,
        @InjectRepository(estadosExaminacionEntity) private readonly estadosExamRepository: Repository<estadosExaminacionEntity>,
        @InjectRepository(ClasesLicenciasEntity) private readonly clasesLicenciasRespository: Repository<ClasesLicenciasEntity>,
        @InjectRepository(TipoExaminacionesEntity) private readonly tiposExamRepository: Repository<TipoExaminacionesEntity>,
        @InjectRepository(EstadosSolicitudEntity) private readonly estadosSolicitudRepository: Repository<EstadosSolicitudEntity>,
    ) { }


    // Método para la recuperación de los diferentes tipos de trámites
    public async getTipoTramites(filtroIds?:number[]) {


        let res: Resultado = new Resultado();

        try {

            let arrayTipoTramiteDto: TipoTramiteDto[] = [];

            const qbTramites = this.tipoTramitesEntityRespository.createQueryBuilder('TiposTramite')

            // Comprobamos si nos viene un filtro para restringuir
            if(filtroIds){
                qbTramites.andWhere('(TiposTramite.idTipoTramite IN ( :...ids ))', { ids: filtroIds });
            }

            let tipost: TiposTramiteEntity[] = await qbTramites.getMany();

            tipost.forEach(x => {
                let tipoTramiteDto: TipoTramiteDto = new TipoTramiteDto();

                tipoTramiteDto.IdTipoTramite = x.idTipoTramite;
                tipoTramiteDto.Nombre = x.Nombre;
                tipoTramiteDto.Descripcion = x.Descripcion;

                arrayTipoTramiteDto.push(tipoTramiteDto);
            });

            res.Respuesta = arrayTipoTramiteDto;
            res.ResultadoOperacion = true;

            return res;
        }
        catch (Error) {
            res.ResultadoOperacion = true;
            res.Error = "Ha ocurrido un error al obtener los estados de examinación";
        }
    }

    // Método para la recuperación de los diferentes estados de trámites
    public async getEstadosTramites(filtroIds?:number[]) {
        let res: Resultado = new Resultado();

        try {

            let arrayEstadoTramiteDto: EstadoTramiteDto[] = [];

            const qbTramites = this.estadosTramitesEntityRespository.createQueryBuilder('EstadosTramite')

            // Comprobamos si nos viene un filtro para restringuir
            if(filtroIds){
                qbTramites.andWhere('(EstadosTramite.idEstadoTramite IN ( :...ids ))', { ids: filtroIds });
            }

            let tipost: EstadosTramiteEntity[] = await qbTramites.getMany();

            tipost.forEach(x => {
                let estadosTramiteDto: EstadoTramiteDto = new EstadoTramiteDto();

                estadosTramiteDto.IdEstadoTramite = x.idEstadoTramite;
                estadosTramiteDto.Nombre = x.Nombre;
                estadosTramiteDto.Descripcion = x.Descripcion;

                arrayEstadoTramiteDto.push(estadosTramiteDto);
            });

            res.Respuesta = arrayEstadoTramiteDto;
            res.ResultadoOperacion = true;

            return res;
        }
        catch (Error) {
            res.ResultadoOperacion = true;
            res.Error = "Ha ocurrido un error al obtener los estados de examinación";
        }
    }

    // Método para la recuperación de todas las clases de licencia
    public async getClasesLicencia() {
        let res: Resultado = new Resultado();

        try {

            let arrayClasesDto: ClaseLicenciaDto[] = [];

            const qbcl = this.clasesLicenciasRespository.createQueryBuilder('ClasesLicencia')

            let cls: ClasesLicenciasEntity[] = await qbcl.getMany();

            cls.forEach(x => {
                let claseLicenciaDto: ClaseLicenciaDto = new ClaseLicenciaDto();

                claseLicenciaDto.IdClaseLicencia = x.idClaseLicencia;
                claseLicenciaDto.Nombre = x.Nombre;
                claseLicenciaDto.Descripcion = x.Descripcion;
                claseLicenciaDto.Abreviacion = x.Abreviacion;

                arrayClasesDto.push(claseLicenciaDto);
            });

            res.Respuesta = arrayClasesDto;
            res.ResultadoOperacion = true;

            return res;
        }
        catch (Error) {
            res.ResultadoOperacion = true;
            res.Error = "Ha ocurrido un error al obtener las clases de licencia";
        }
    }

    // Método para la recuperación de los diferentes tipos de examinacion
    public async getTiposExaminacion(filtroIds?:number[]) {


        let res: Resultado = new Resultado();

        try {

            let arrayTipoTramiteDto: TipoExaminacionDto[] = [];

            const qbTramites = this.tiposExamRepository.createQueryBuilder('TiposExaminacion')

            // Comprobamos si nos viene un filtro para restringuir
            if(filtroIds){
                qbTramites.andWhere('(TiposExaminacion.idTipoExaminacion IN ( :...ids ))', { ids: filtroIds });
            }

            let tipost: TipoExaminacionesEntity[] = await qbTramites.getMany();

            tipost.forEach(x => {
                let tipoExamDto: TipoExaminacionDto = new TipoExaminacionDto();

                tipoExamDto.IdTipoExaminacion = x.idTipoExaminacion;
                tipoExamDto.Nombre = x.nombreExaminacion;
                tipoExamDto.Descripcion = x.descripcionExaminacion;

                arrayTipoTramiteDto.push(tipoExamDto);
            });

            res.Respuesta = arrayTipoTramiteDto;
            res.ResultadoOperacion = true;

            return res;
        }
        catch (Error) {
            res.ResultadoOperacion = true;
            res.Error = "Ha ocurrido un error al obtener los estados de examinación";
        }
    }

    // Método para la recuperación de los diferentes estados de examinacion
    public async getEstadosExaminacion(filtroIds?:number[]) {


        let res: Resultado = new Resultado();

        try {

            let arrayEstExamsDto: EstadoExaminacionDto[] = [];

            const qbTramites = this.estadosExamRepository.createQueryBuilder('EstadoExaminacion')

            // Comprobamos si nos viene un filtro para restringuir
            if(filtroIds){
                qbTramites.andWhere('(EstadoExaminacion.idEstadosExaminacion IN ( :...ids ))', { ids: filtroIds });
            }

            let estExamsDto: estadosExaminacionEntity[] = await qbTramites.getMany();

            estExamsDto.forEach(x => {
                let estExamDto: EstadoExaminacionDto = new EstadoExaminacionDto();

                estExamDto.IdEstadoExaminacion = x.idEstadosExaminacion;
                estExamDto.Nombre = x.nombreEstado;
                estExamDto.Descripcion = x.descripcion;

                arrayEstExamsDto.push(estExamDto);
            });

            res.Respuesta = arrayEstExamsDto;
            res.ResultadoOperacion = true;

            return res;
        }
        catch (Error) {
            res.ResultadoOperacion = true;
            res.Error = "Ha ocurrido un error al obtener los estados de examinación";
        }
    }   

    // Método para la recuperación de los diferentes estados de examinacion
    public async getEstadosSolicitud(filtroIds?:number[]) {


        let res: Resultado = new Resultado();

        try {

            let arrayEstSolicitudDto: EstadoSolicitudDto[] = [];

            const qbTramites = this.estadosSolicitudRepository.createQueryBuilder('EstadosSolicitud')

            // Comprobamos si nos viene un filtro para restringuir
            if(filtroIds){
                qbTramites.andWhere('(EstadosSolicitud.idEstado IN ( :...ids ))', { ids: filtroIds });
            }

            let estSolicitudes: EstadosSolicitudEntity[] = await qbTramites.getMany();

            estSolicitudes.forEach(x => {
                let estExamDto: EstadoSolicitudDto = new EstadoSolicitudDto();

                estExamDto.IdEstadoSolicitud = x.idEstado;
                estExamDto.Nombre = x.Nombre;

                arrayEstSolicitudDto.push(estExamDto);
            });

            res.Respuesta = arrayEstSolicitudDto;
            res.ResultadoOperacion = true;

            return res;
        }
        catch (Error) {
            res.ResultadoOperacion = true;
            res.Error = "Ha ocurrido un error al obtener los estados de examinación";
        }
    }     
}