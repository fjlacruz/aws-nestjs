import { Test, TestingModule } from '@nestjs/testing';
import { SetResultadoService } from './set-resultado.service';

describe('SetResultadoService', () => {
  let service: SetResultadoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SetResultadoService],
    }).compile();

    service = module.get<SetResultadoService>(SetResultadoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
