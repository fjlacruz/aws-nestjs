import { Injectable } from '@nestjs/common';
import { Resultado } from 'src/utils/resultado';

@Injectable()
export class SetResultadoService {
  /**
   * Función que asigna la respuesta al resultado
   *
   * @param resultado Clase Resultado pasado por referencia para poder modifcarla
   * @param respuesta Respuesta a mandar dentro del Resultado
   * @param respuestaOperacion Resultado de la operación, por defecto es 'true'
   * @param mensaje Mensaje opcional para mostrar
   * @returns Resultado modificado con ResultadoOperacion = true y la respuesta
   */
  public setResponse<T>(resultado: Resultado, respuesta: T, resultadoOperacion: boolean = true, mensaje?: string): Resultado {
    resultado.ResultadoOperacion = resultadoOperacion;
    resultado.Respuesta = respuesta as Object;

    if (mensaje) resultado.Mensaje = mensaje;

    return resultado;
  }

  /**
   * Función que asigna los  mensajes de Errores al resultado
   *
   * @param resultado Clase Resultado pasado por referencia para poder modifcarla
   * @param error Mensaje de error amigable para el usuario
   * @param mensajeError Mensaje de error lanzado por consulsta
   * @param respuestaOperacion Resultado de la operación, por defecto es 'false'
   * @returns Resultado modificado con ResultadoOperacion = false y los mensajes de error
   */
  public setError(resultado: Resultado, error: string, mensajeError?: string, resultadoOperacion: boolean = false): Resultado {
    resultado.Error = error;
    if (mensajeError) resultado.Mensaje = mensajeError;
    resultado.ResultadoOperacion = resultadoOperacion;

    return resultado;
  }
}
