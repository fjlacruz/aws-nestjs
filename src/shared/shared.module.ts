import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { RegistroCivilConsultasService } from './externalServices/registroCivil/registroCivilConsultas.services';
import { ServicioGenerarLicenciasService } from './services/ServiciosComunes/ServicioGenerarLicencias.services';
import { ServiciosComunesService } from './services/ServiciosComunes/ServiciosComunes.services';
import { SetResultadoService } from './services/set-resultado/set-resultado.service';

@Module({
  imports: [TypeOrmModule.forFeature([
    TiposTramiteEntity,
    EstadosTramiteEntity,
    ClasesLicenciasEntity,
    TipoExaminacionesEntity,
    estadosExaminacionEntity,
    EstadosSolicitudEntity
  ]
  )],
  providers: [SetResultadoService, ServiciosComunesService, ServicioGenerarLicenciasService, RegistroCivilConsultasService],
  exports: [SetResultadoService, ServiciosComunesService, ServicioGenerarLicenciasService, RegistroCivilConsultasService],
})
export class SharedModule {}
