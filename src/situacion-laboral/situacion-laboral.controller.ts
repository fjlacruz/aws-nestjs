import { Controller, Get, Req } from '@nestjs/common';
import { SituacionLaboralService } from './situacion-laboral.service';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { Request } from 'express';
import { SituacionLaboralEntity } from './situacion-laboral.entity';

@Controller('situacion-laboral')
export class SituacionLaboralController {

  constructor(private readonly situacionLaboralService: SituacionLaboralService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all records',
    type: SituacionLaboralEntity,
  })
  async getAll(@Req() req: Request): Promise<SituacionLaboralEntity[]> {
    return await this.situacionLaboralService.findAll();
  }
}
