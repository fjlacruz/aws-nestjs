import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('OpcionesSituacionLaboral')
export class SituacionLaboralEntity {
  @PrimaryGeneratedColumn()
  idSituacionLaboral: number;

  @Column()
  descripcion: string;
}
