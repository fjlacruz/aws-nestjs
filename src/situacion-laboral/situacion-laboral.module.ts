import { Module } from '@nestjs/common';
import { SituacionLaboralController } from './situacion-laboral.controller';
import { SituacionLaboralService } from './situacion-laboral.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SituacionLaboralEntity } from './situacion-laboral.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SituacionLaboralEntity])],
  controllers: [SituacionLaboralController],
  providers: [SituacionLaboralService],
  exports: [SituacionLaboralService],
})
export class SituacionLaboralModule {}
