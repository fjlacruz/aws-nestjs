import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SituacionLaboralEntity } from './situacion-laboral.entity';

@Injectable()
export class SituacionLaboralService {
  constructor(
    @InjectRepository(SituacionLaboralEntity) private readonly situacionLaboralEntityRepository: Repository<SituacionLaboralEntity>
  ) {}

  async findAll() {
    return this.situacionLaboralEntityRepository.find();
  }
}
