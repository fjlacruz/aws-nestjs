import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

@Entity('SolicitudesAltaUsuarios')
export class SolicitudAltaUsuarioEntity {
  @PrimaryGeneratedColumn()
  idSolicitudesAltaUsuarios: number;

  @Column()
  @ApiModelProperty()
  idAprobador: number;

  @Column()
  @ApiModelProperty()
  idUsuario: number;

  @Column()
  @ApiModelProperty()
  idSolicitante: number;

  @Column()
  @ApiModelProperty()
  motivoRechazo: string;

  @Column()
  @ApiModelProperty()
  aprobado: boolean;

  @Column()
  @ApiModelProperty()
  created_at: Date;

  @Column()
  @ApiModelProperty()
  updated_at: Date;
}
