import { Module } from '@nestjs/common';
import { SolicitudesAltaUsuariosController } from './solicitudes-alta-usuarios.controller';
import { SolicitudesAltaUsuariosService } from './solicitudes-alta-usuarios.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SolicitudAltaUsuarioEntity } from './solicitudAltaUsuario.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SolicitudAltaUsuarioEntity])],
  controllers: [SolicitudesAltaUsuariosController],
  providers: [SolicitudesAltaUsuariosService],
  exports: [SolicitudesAltaUsuariosService],
})
export class SolicitudesAltaUsuariosModule {}
