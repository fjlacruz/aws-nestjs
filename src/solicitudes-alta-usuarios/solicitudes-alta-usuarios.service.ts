import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { SolicitudAltaUsuarioEntity } from './solicitudAltaUsuario.entity';

const relationshipNames = [];

@Injectable()
export class SolicitudesAltaUsuariosService {
  constructor(
    @InjectRepository(SolicitudAltaUsuarioEntity)
    private readonly solicitudAltaUsuarioEntityRepository: Repository<SolicitudAltaUsuarioEntity>
  ) {}

  async findById(id: string): Promise<SolicitudAltaUsuarioEntity | undefined> {
    const options = { relations: relationshipNames };
    const result = await this.solicitudAltaUsuarioEntityRepository.findOne(id, options);
    return result;
  }

  async findByfields(options: FindOneOptions<SolicitudAltaUsuarioEntity>): Promise<SolicitudAltaUsuarioEntity | undefined> {
    const result = await this.solicitudAltaUsuarioEntityRepository.findOne(options);
    return result;
  }

  async save(sentenciaEntity: SolicitudAltaUsuarioEntity): Promise<SolicitudAltaUsuarioEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.solicitudAltaUsuarioEntityRepository.save(entity);
    return result;
  }

  async update(sentenciaEntity: SolicitudAltaUsuarioEntity): Promise<SolicitudAltaUsuarioEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.solicitudAltaUsuarioEntityRepository.save(entity);
    return result;
  }
}
