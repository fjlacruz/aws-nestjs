import { Body, Controller, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { RUNDto } from '../DTO/RUN.dto';
import { SolicitudesProximasCierreService } from '../services/solicitudes-proximas-cierre.service';

@ApiTags('Servicios-solicitud')
@Controller('solicitudes-proximas-cierre')
export class SolicitudesProximasCierreController {

    constructor(private readonly solicitudesProximasCierreService: SolicitudesProximasCierreService,
                private readonly authService: AuthService
    ) { }
    
    @Post('')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que comprueba si tiene solicitudes próximas a su cierre' })
    async compruebaSolicitudes(@Body() RUN: RUNDto,@Req() req: any) {
      
        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermiso = new TokenPermisoDto(token, [
            PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosCrearRolesGruposdePermisosTipodeDocumentos
        ]);
        
        let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

        if (validarPermisos.ResultadoOperacion)
        {
            let result = await this.solicitudesProximasCierreService.compruebaSolicitudes(req.body.RUN)
            return result ;

        } else {
            return { data: validarPermisos };
        }
    } 
}
