import { Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';

@Injectable()
export class SolicitudesProximasCierreService {

    constructor() { }

    async compruebaSolicitudes(RUN: number): Promise<boolean> {
        try {
            const apelacion = await getConnection()
            .query(`
            SELECT count(S."idSolicitud") FROM "Usuarios" U
            left join "Solicitudes" S on U."idUsuario" = S."idUsuario"
            where 
            U."RUN" = ${RUN}
            and 
            ((select now()::DATE)- S."updated_at"::DATE) > (SELECT "Value" FROM "ParametrosGeneral" where "Param" = 'CierreSolicitudCincoMeses')::integer;`)
          
            return apelacion[0].count > 0;
            
        } catch (error) {
            return error;
        }
    }
}

