import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { User } from 'src/users/entity/user.entity';
import { SolicitudesProximasCierreController } from './controller/solicitudes-proximas-cierre.controller';
import { SolicitudesProximasCierreService } from './services/solicitudes-proximas-cierre.service';

@Module({
    imports: [TypeOrmModule.forFeature([User, RolesUsuarios])],
    controllers: [SolicitudesProximasCierreController],
    providers: [SolicitudesProximasCierreService, AuthService],
    exports: [SolicitudesProximasCierreService]
})
export class SolicitudesProximasCierreModule { }