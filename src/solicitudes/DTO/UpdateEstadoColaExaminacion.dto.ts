import { ApiPropertyOptional } from "@nestjs/swagger";

export class UpdateEstadoColaExaminacionDto {
    @ApiPropertyOptional()
    idColaExaminacion: number;
    @ApiPropertyOptional()
    idTipoExaminacion: number;
    @ApiPropertyOptional()
    idTramite: number;
    @ApiPropertyOptional()
    aprobado: boolean;
    @ApiPropertyOptional()
    historico: boolean;
    @ApiPropertyOptional()
    created_at: Date;
}