import { ApiPropertyOptional } from "@nestjs/swagger";

export class UpdateEstadoExaminacionByIdExaminacionDto {
    @ApiPropertyOptional()
    idColaExaminacion: number;
    @ApiPropertyOptional()
    idTipoExaminacion: number;
    @ApiPropertyOptional()
    idTramite: number;
    @ApiPropertyOptional()
    aprobado: boolean;
    @ApiPropertyOptional()
    historico: boolean;
    // @ApiPropertyOptional()
    // idUsuario: number;
    @ApiPropertyOptional()
    nombrearchivo: string[];
    @ApiPropertyOptional()
    binary: Buffer;
    @ApiPropertyOptional()
    observacion: string;
}
