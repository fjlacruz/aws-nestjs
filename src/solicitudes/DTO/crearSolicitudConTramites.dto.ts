import { ApiPropertyOptional } from "@nestjs/swagger";

export class CrearSolicitudConTramitesDTO
{
    @ApiPropertyOptional()
    idTipoSolicitud: number;

    @ApiPropertyOptional()
    idPostulante:number;

    // @ApiPropertyOptional()
    // idUsuario:number;

    // @ApiPropertyOptional()
    // idOficina: number;

    @ApiPropertyOptional()
    idEstado: number;

    @ApiPropertyOptional()
    idInstitucion: number;

    @ApiPropertyOptional()
    tramites: CrearTramiteDTO[];
}

export class CrearTramiteDTO 
{
    @ApiPropertyOptional()    
    idTipoTramite : number;

    @ApiPropertyOptional()
    idClaseLicencia:number;
}

export class CrearTramiteSolicitudDTO extends CrearTramiteDTO
{
    idSolicitud: number;
}
