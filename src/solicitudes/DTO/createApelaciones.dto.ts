import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateApelacionesDto {

    @ApiPropertyOptional()
    idapelaciones: number;
    @ApiPropertyOptional()
    idcolaexaminacion: number;
    @ApiPropertyOptional()
    fechaingreso:Date;
    @ApiPropertyOptional()
    observacion:string;
    @ApiPropertyOptional()    
    ingresadapor: number;
    @ApiPropertyOptional()
    apelacionaprobada:boolean;
    @ApiPropertyOptional()
    fechaaprobacion:Date;
    @ApiPropertyOptional()    
    aprobadapor: number;

}