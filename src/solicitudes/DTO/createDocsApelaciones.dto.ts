import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateDocsApelacionesDto {

    @ApiPropertyOptional()
    iddocsapelacion: number;
    @ApiPropertyOptional()
    nombrearchivo: string;
    @ApiPropertyOptional()
    binary:Buffer;
    @ApiPropertyOptional()    
    created_at: Date;
    @ApiPropertyOptional()
    idapelaciones:number;
}