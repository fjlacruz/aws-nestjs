import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateSolicitudDto {

    @ApiPropertyOptional()
    idSolicitud: number;
    @ApiPropertyOptional()
    idTipoSolicitud: number;
    @ApiPropertyOptional()
    idPostulante:number;
    @ApiPropertyOptional()
    idUsuario:number;
    @ApiPropertyOptional()
    idOficina: number;
    @ApiPropertyOptional()
    idEstado: number;
    @ApiPropertyOptional()
    idInstitucion: number;
    @ApiPropertyOptional()    
    FechaCreacion: Date;
}