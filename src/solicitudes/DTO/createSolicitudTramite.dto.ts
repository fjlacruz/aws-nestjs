import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateSolicitudTramiteDto {

    @ApiPropertyOptional()
    idTipoSolicitud: number;
    @ApiPropertyOptional()
    idPostulante:number;
    @ApiPropertyOptional()
    idUsuario:number;
    @ApiPropertyOptional()
    idOficina: number;
    @ApiPropertyOptional()
    idEstado: number;
    @ApiPropertyOptional()
    idInstitucion: number;
    @ApiPropertyOptional()    
    FechaCreacion: Date;

    @ApiPropertyOptional()    
    Observacion: string;
    @ApiPropertyOptional()    
    idTipoTramite : number;
    @ApiPropertyOptional()    
    idEstadotramite : number;
    @ApiPropertyOptional()    
    Alertado : boolean ;
    @ApiPropertyOptional()    
    Aprobado : boolean;
    @ApiPropertyOptional()    
    ExminadorIdUsuario : number;
    @ApiPropertyOptional()    
    historico : boolean;
    @ApiPropertyOptional()    
    ExaminadorID : number;
    @ApiPropertyOptional()    
    FolioRendicion : string;
    @ApiPropertyOptional()    
    Puntaje : number;
    @ApiPropertyOptional()    
    TiempoRendicionMin : number;
    @ApiPropertyOptional()    
    fechaCobro : Date;
    @ApiPropertyOptional()    
    fechaExpiracion : Date;
    @ApiPropertyOptional()    
    pagado : boolean;
    @ApiPropertyOptional()
    idClaseLicencia:number;

}