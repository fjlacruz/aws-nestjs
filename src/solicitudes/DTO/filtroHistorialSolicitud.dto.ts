export class FiltroHistorialSolicitudDto {
    id: number;
    run: string;
    estadoTramite: string;
    estadoSolicitud: string;
    nombrePostulante: string;
    tipoTramite: string;
    claseLicencia: any;
    fechaSolicitud: Date;
    fechaFinSolicitud: Date;
}