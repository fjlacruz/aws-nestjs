import { ApiPropertyOptional } from "@nestjs/swagger";

export class FiltroPagSolicitudDto {

    @ApiPropertyOptional()
    filtro: string;
    @ApiPropertyOptional()
    orden: object;
    @ApiPropertyOptional()
    pagination: object;
}