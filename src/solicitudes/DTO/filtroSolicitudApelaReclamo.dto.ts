import { ApiPropertyOptional } from "@nestjs/swagger";
import { isEmpty } from "rxjs/operators";

export class filtroSolicitudApelaReclamo {

    @ApiPropertyOptional()
    RUN: number;

    @ApiPropertyOptional()
    idSolicitud: number;

    @ApiPropertyOptional()
    idTramite: number;

    @ApiPropertyOptional()
    Nomclaselic:string;

    @ApiPropertyOptional()
    idTipoExam:number;

    @ApiPropertyOptional()
    idMunicipalidad:number;

    @ApiPropertyOptional()
    idEstadoSol:number;

    @ApiPropertyOptional()
    idEstadoTramite:number;
    
}