import { ApiPropertyOptional } from "@nestjs/swagger";
import { isEmpty } from "rxjs/operators";

export class filtroSolicitudProximoAvencer {

    @ApiPropertyOptional()
    idEstadoSol: number;

    @ApiPropertyOptional()
    fechadesde: Date;

    @ApiPropertyOptional()
    idEstadoTramite: string;

}