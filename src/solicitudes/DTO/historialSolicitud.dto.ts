import { DocumentoNombreIdDTO } from "src/documento-solicitud-postulante/DTO/documento-nombre-id.dto";
import { OportunidadEntity } from "src/oportunidad/oportunidad.entity";

export class HistorialSolicitudDto {
    idSolicitud: number;
    idTramite: number;
    run: string;
    estadoSolicitud: string;
    nombrePostulante: string;
    tipoTramite: string;
    estadoTramite: string
    claseLicencia: string;
    fechaSolicitud: Date;
    fechaFinSolicitud: Date;
    updated_at: Date;
    idExamenTeorico: number
    fechaExamenTeorico: Date;
    idExamenPractico: number
    fechaExamenPractico: Date;
    observaciones: string;
    idoneidadMoral: number;
    examenMedico: number;
    examenTeorico: number;
    oportunidadesTeorico: OportunidadEntity[];
    examenPractico: number;
    oportunidadesPractico: OportunidadEntity[];
    documentos: DocumentoNombreIdDTO[];
}