import { TramiteDTO } from "src/tramites/DTO/tramite.dto";

export class SolicitudDTO {
    idSolicitud: number;
    run: string;
    nombrePostulante: string;
    tramites: TramiteDTO[] = [];
    fechaSolicitud: Date;
}