import { TramiteConTipoDto } from "src/tramites/DTO/tramiteConTipo.dto";

export class SolicitudesEnProcesoDto {
    idSolicitud: number;
    tramites: TramiteConTipoDto[];
    run: string;
    estadoSolicitud: string;
    estadoTramite: string;
    nombrePostulante: string; // nombre + apellidos
    tipoTramite: string;
    fechaSolicitud: Date;
    idPostulante: number;

    tramitesAsociados:SolicitudTramitesEnProcesoDto[];
}

export class SolicitudTramitesEnProcesoDto{
    idTramite: number;
    idEstadoTramite:number;
    estadoTramite:string;
    tipoTramite:string;
    claseLicenciaTramite:string;
}