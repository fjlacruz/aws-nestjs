import { ApiPropertyOptional } from "@nestjs/swagger";

export class UpdateEstadoSolicitudDto {
    @ApiPropertyOptional()
    idSolicitud: number;
    @ApiPropertyOptional()
    idEstado: number;
    @ApiPropertyOptional()
    idTramite:number[];
    @ApiPropertyOptional()
    idNuevoEstadoTramite: number[];
    @ApiPropertyOptional()
    idTipoExaminacion: number;
    @ApiPropertyOptional()
    aprobado:boolean;  
    @ApiPropertyOptional()
    idUsuario: number ; 
    @ApiPropertyOptional()
    nombrearchivo: string[] ; 
    @ApiPropertyOptional()
    observacion: string ;

}
