import { Controller, Query, Req, UseGuards } from '@nestjs/common';
import { Get, Param, Post, Body } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { SolicitudesService } from '../services/solicitudes.service';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateSolicitudTramiteDto } from '../DTO/createSolicitudTramite.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';

import { UpdateEstadoSolicitudDto } from '../DTO/updateEstadoSolicitud.dto';
import { filtroSolicitudApelaReclamo } from '../DTO/filtroSolicitudApelaReclamo.dto';
import { filtroSolicitudProximoAvencer } from '../DTO/filtroSolicitudProximoAvencer.dto';
import { UpdateEstadoExaminacionByIdExaminacionDto } from '../DTO/UpdateEstadoExaminacionByIdExaminacion.dto';
import { AuthGuard } from '@nestjs/passport';
import { CrearSolicitudConTramitesDTO, CrearTramiteSolicitudDTO } from '../DTO/crearSolicitudConTramites.dto';

@ApiTags('Solicitudes')
@Controller('solicitudes')
export class SolicitudesController {
  constructor(private readonly solicitudService: SolicitudesService, private readonly authService: AuthService) {}

  @UseGuards(JwtAuthGuard)
  @Get('allSolicitudes')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos las Solicitudes' })
  async getSolicitudes() {
    const data = await this.solicitudService.getSolicitudes();
    return { data };
  }

  @Get('solicitudById/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve una Solicitud por Id' })
  async getSolicitud(@Param('id') id: number) {
    const data = await this.solicitudService.getSolicitud(id);
    return { data };
  }
  /*
          @Post('creaSolicitud')
          @ApiBearerAuth()
          @ApiOperation({ summary: 'Servicio que crea una nueva Solicitud' })
          async addPostulante(
              @Body() createSolicitudDto: CreateSolicitudDto) {

              const data = await this.solicitudService.create(createSolicitudDto);
              return {data};

          }
      **/
  @Post('creaSolicitudWithTramite')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una Solicitud con Tramite asociado' })
  async addSolicitudWithTramite(@Body() createSolicitudTramiteDto: CreateSolicitudTramiteDto) {
    const data = await this.solicitudService.create(createSolicitudTramiteDto);

    return { data };
  }

  @Post('crearSolicitudConTramites')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una solicitud uno o mas tramites asociados' })
  async postCrearSolicitudConTramites(@Body() dto: CrearSolicitudConTramitesDTO) {
    return await this.solicitudService.crearSolicitudConTramites(dto).then(data => {
      return { data, code: 200 };
    });
  }

  @Post('crearTramite')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un tramites asociados a una solicitud' })
  async postCrearTramite(@Body() dto: CrearTramiteSolicitudDTO) {
    return await this.solicitudService.crearTramiteDeSolicitud(dto).then(data => {
      return { data, code: 200 };
    });
  }

  @Post('cancelarSolicitud/:idSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que cancela una solicitud' })
  async postCancelarSolicitud(@Param('idSolicitud') id: number) {
    return await this.solicitudService.cancelarSolicitud(id).then(data => {
      return { data, code: 200 };
    });
  }

  @Get('getEstadosSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los estados de solicitud' })
  async getEstadosSolicitud(@Req() req: any) {
    const data = await this.solicitudService.getEstadosSolicitud();

    return { data };
  }

  @Post('updateEstadoSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza el estado de una solicicitud' })
  async updateEstadoSolicitud(@Body() updateEstadoSolicitudDto: UpdateEstadoSolicitudDto) {
    const data = await this.solicitudService.updateEstadoSolicitud(updateEstadoSolicitudDto.idSolicitud, updateEstadoSolicitudDto.idEstado);
    return data;
  }

  @Get('tieneClaseLicencia/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida si tiene clase de licencia' })
  async getTieneClaseLicencia(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getTieneClaseLicencia(RUN, idCl);
    return { data };
  }

  @Get('tieneClaseLicenciaRNC/:RUN/:idClaseLicencia/:idTipoTramite')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida si tiene clase de licencia para cambio de domicilio' })
  async tieneClaseLicenciaRNC(
    @Param('RUN') RUN: string,
    @Param('idClaseLicencia') idClaseLicencia: number,
    @Param('idTipoTramite') idTipoTramite: number
  ) {
    const data = await this.solicitudService.tieneClaseLicenciaRNC(RUN, idClaseLicencia, idTipoTramite);
    return { data };
  }

  @Get('getLicById/:idClaseLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida si tiene clase de licencia para cambio de domicilio' })
  async getLicById(@Param('idClaseLicencia') idClaseLicencia: number) {
    const data = await this.solicitudService.getLicById(idClaseLicencia);
    return { data };
  }

  @Get('edadByRUN/:RUN')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio devuelve edad' })
  async getEdad(@Param('RUN') RUN: number) {
    const data = await this.solicitudService.getEdad(RUN);
    return { data };
  }

  @Get('getEdad/:RUN')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio devuelve edad' })
  async getEdad2(@Param('RUN') RUN: number) {
    const data = await this.solicitudService.getEdad2(RUN);
    return data;
  }

  @Get('tieneTramitePrimerOtorgamientoIniciadoInferiorA6MesesByRUN/:RUN')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio retorna true si se inicio un tramite de primero otorgamiento dentro de 6 meses, caso contrario retorna false',
  })
  async getTieneTramitePrimerOtorgamientoIniciadoInferiorA6Meses(@Param('RUN') RUN: number) {
    const data = await this.solicitudService.getTieneTramitePrimerOtorgamientoIniciadoInferiorA6Meses(RUN);
    return { data };
  }

  @Get('validarReglaNegocioPrimeroBByRUNidCl/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida regla de negocio primero B ' })
  async getvalidarReglaNegocioPrimeroB(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getValidarReglaNegocioPrimeroB(RUN, idCl);
    return { data };
  }

  @Get('validarReglaNegocioPrimeroBByRUNidCl17/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida regla de negocio primero B-17 ' })
  async getvalidarReglaNegocioPrimeroB17(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getValidarReglaNegocioPrimeroB17(RUN, idCl);
    return { data };
  }
  @Get('validarClaseCyCrestringido/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida regla de negocio primero B ' })
  async getvalidarClaseCyCrestringido(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getTramiteprimotorginiinf6mesesclasec(RUN, idCl);
    return { data };
  }

  @Get('validarReglaNegocioPrimeroCByRUNidCl/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida regla de negocio primero C ' })
  async getvalidarReglaNegocioPrimeroC(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getValidarReglaNegocioPrimeroC(RUN, idCl);
    return { data };
  }

  @Get('validarReglaNegocioPrimeroProfesinales/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida regla de negocio clases profesionales' })
  async getValidarReglaNegocioPrimeroProfesionales(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getValidarReglaNegocioPrimeroProfesionales(RUN, idCl);
    return { data };
  }

  @Get('validarReglaNegocioPrimeroDByRUNidCl/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida regla de negocio primero D ' })
  async getvalidarReglaNegocioPrimeroD(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getValidarReglaNegocioPrimeroD(RUN, idCl);
    return { data };
  }

  @Get('validarReglaNegocioPrimeroEByRUNidCl/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida regla de negocio primero E ' })
  async getvalidarReglaNegocioPrimeroE(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getValidarReglaNegocioPrimeroE(RUN, idCl);
    return { data };
  }

  @Get('renovacionBcDeTieneClaseLicenciaByRUNidCl/:RUN/:idCl')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que valida renovacion B C D E ' })
  async getRenovacionBcDeTieneClaseLicencia(@Param('RUN') RUN: number, @Param('idCl') idCl: number) {
    const data = await this.solicitudService.getRenovacionBcDeTieneClaseLicencia(RUN, idCl);
    return { data };
  }

  @Post('getSolicitudesEnProceso')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las solicitudes en Proceso ' })
  async getSolicitudesEnProceso(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaSolicitudesEnProceso]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.solicitudService.getSolicitudesEnProceso(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('getSolicitudesEnProcesoV2')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las solicitudes en Proceso ' })
  async getSolicitudesEnProcesoV2(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    const data = await this.solicitudService.getSolicitudesEnProcesoV2(req, paginacionArgs);

    return { data };
  }

  @Post('getHistorialSolicitudes')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve las solicitudes que no se encuentran activas, con filtro' })
  async getHistorialSolicitudes(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    const data = await this.solicitudService.getHistorialSolicitudes(req, paginacionArgs);

    return { data };
  }

  @Get('getHistorialSolicitud/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve una solicitud por Id' })
  async getHistorialSolicitud(@Param('id') Id: number, @Req() req: any) {
    const data = await this.solicitudService.getHistorialSolicitud(req, Id);

    return { data };
  }

  @Get('getListaReprobadosByIdTipoexaminacion')
  //    @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve apelaciones y reclamos por denegacion p/JPL y/o SML' })
  async getListaReprobadosByIdTipoexaminacion(@Query() query: filtroSolicitudApelaReclamo, @Req() req: any) {
    const idMunicipalidad = this.authService.oficina().idOficina;
    const data = await this.solicitudService.getListaReprobadosByIdTipoexaminacion(
      query.RUN,
      query.idSolicitud,
      query.idTramite,
      query.Nomclaselic,
      query.idTipoExam,
      idMunicipalidad,
      query.idEstadoSol,
      query.idEstadoTramite
    );
    return { data };
  }

  @Get('getListaTramitesEnDenegacionInformadaCrJPLSML')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve apelaciones y reclamos por denegacion p/JPL y/o SML' })
  async getListaTramitesEnDenegacionInformadaCrJPLSML() {
    const data = await this.solicitudService.getListaTramitesEnDenegacionInformadaCrJPLSML();
    return { data };
  }

  @Get('getListaDocsReprobadosByIdTipoexaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve apelaciones y reclamos por denegacion p/JPL y/o SML' })
  async getListaDocsReprobadosByIdTipoexaminacion(@Query() query: filtroSolicitudApelaReclamo, @Req() req: any) {
    const idMunicipalidad = this.authService.oficina().idOficina;
    const data = await this.solicitudService.getListaDocsReprobadosByIdTipoexaminacion(
      query.RUN,
      query.idSolicitud,
      query.idTramite,
      query.Nomclaselic,
      query.idTipoExam,
      idMunicipalidad,
      query.idEstadoSol,
      query.idEstadoTramite
    );
    return { data };
  }

  @Get('getSolicitudesByEstadoFechavencimiento')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve Solicitudes próximas a vencer' })
  async getSolicitudesByEstadoFechavencimiento(@Query() query: filtroSolicitudProximoAvencer, @Req() req: any) {
    const data = await this.solicitudService.getSolicitudesByEstadoFechavencimiento(
      query.idEstadoSol,
      query.fechadesde,
      query.idEstadoTramite
    );
    return { data };
  }

  @Post('UpdateEstadoExaminacionByIdExaminacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza el estado de la cola de examinación,tramite,solicitud y apelaciones ' })
  async UpdateEstadoExaminacionByIdExaminacion(@Body() dto: UpdateEstadoExaminacionByIdExaminacionDto) {
    const data = await this.solicitudService.UpdateEstadoExaminacionByIdExaminacion(dto);
    // dto.idColaExaminacion,
    // dto.aprobado,
    // dto.idTipoExaminacion,
    // dto.idUsuario,
    // dto.nombrearchivo,
    // dto.binary,
    // dto.observacion);
    return data;
  }

  @Post('UpdateFechavencimientoByIdsolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza el estado de la cola de examinación' })
  async updateFechaVtoByidSolicitud(@Body() updateEstadoSolicitudDto: UpdateEstadoSolicitudDto) {
    const data = await this.solicitudService.updateFechaVtoByidSolicitud(
      updateEstadoSolicitudDto.idSolicitud,
      updateEstadoSolicitudDto.idEstado
    );
    return data;
  }

  @Post('UpdateEstadoSolicitudByIdSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza el estado de tramite,solicitud, apelaciones y documentos de apelaciones ' })
  async UpdateEstadoSolicitudByIdSolicitud(@Body() UpdateEstadoSolicitudByIdSolicitudDto: UpdateEstadoSolicitudDto) {
    const data = await this.solicitudService.UpdateEstadoSolicitudByIdSolicitud(
      UpdateEstadoSolicitudByIdSolicitudDto.idSolicitud,
      UpdateEstadoSolicitudByIdSolicitudDto.idEstado,
      UpdateEstadoSolicitudByIdSolicitudDto.idTramite,
      UpdateEstadoSolicitudByIdSolicitudDto.idNuevoEstadoTramite,
      UpdateEstadoSolicitudByIdSolicitudDto.idTipoExaminacion,
      UpdateEstadoSolicitudByIdSolicitudDto.aprobado,
      UpdateEstadoSolicitudByIdSolicitudDto.idUsuario,
      UpdateEstadoSolicitudByIdSolicitudDto.nombrearchivo,
      UpdateEstadoSolicitudByIdSolicitudDto.observacion
    );
    return data;
  }

  @Get('getEstadosTramites')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Estados de trámites' })
  async getEstadosTramites() {
    const data = await this.solicitudService.getEstadosTramites();
    return { data };
  }
}
