import { ApiProperty } from "@nestjs/swagger";
import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne} from "typeorm";

@Entity('apelaciones')
export class apelacionesEntity {

    @PrimaryGeneratedColumn()
    idapelaciones: number;
    
    @Column()
    idcolaexaminacion: number;
    
    @Column()
    fechaingreso: Date;
    
    @Column()
    observacion: string;
    
    @Column()
    ingresadapor: number;
    
    @Column()
    apelacionaprobada: boolean;
    
    @Column()
    fechaaprobacion: Date;
    
    @Column()
    aprobadapor: number;

}