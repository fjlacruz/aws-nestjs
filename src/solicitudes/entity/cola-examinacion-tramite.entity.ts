import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'ColaExaminacionTramiteNM')
export class ColaExaminacionTramiteEntity
{
    @PrimaryGeneratedColumn()
    idColaExaminacionTramite: number;

    @Column()
    idColaExaminacion: number;

    @Column()
    idTramite: number;
}