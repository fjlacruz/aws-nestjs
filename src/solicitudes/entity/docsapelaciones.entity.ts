import { ApiProperty } from "@nestjs/swagger";
import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne} from "typeorm";

@Entity('docsapelaciones')
export class docsapelacionesEntity {

    @PrimaryGeneratedColumn()
    iddocsapelacion: number;
    
    @Column()
    nombrearchivo: string;
    
    @Column({name: 'binary', type: 'bytea', nullable: true })
    binary: Buffer;
    
    @Column()
    created_at: Date;
    
    @Column()
    idapelaciones: number;
    
}