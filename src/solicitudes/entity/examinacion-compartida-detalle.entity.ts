import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'DetalleTipoTramiteClaseLicenciaExaminacionCompartida')
export class ExaminacionCompartidaDetalleEntity
{
    @PrimaryGeneratedColumn()
    idDetalleTipoTramiteClaseLicenciaExaminacionCompartida: number;

    @Column()
    idClaseLicencia: number;

    @Column()
    idExaminacionCompartida: number;
}