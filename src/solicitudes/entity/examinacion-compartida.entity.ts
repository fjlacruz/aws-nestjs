import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity( 'TipoTramiteClaseLicenciaExaminacionCompartida' )
export class ExaminacionCompartidaEntity
{
    @PrimaryGeneratedColumn()
    idExaminacionCompartida: number;

    @Column()
    idTipoExaminacion: number;

    @Column()
    idTipoTramite: number;

    @Column()
    idOficina: number;
}