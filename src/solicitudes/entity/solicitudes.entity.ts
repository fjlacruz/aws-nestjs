import { ApiProperty } from '@nestjs/swagger';
import { DocumentoSolicitudPostulanteEntity } from 'src/documento-solicitud-postulante/entity/documento-solicitud-postulante.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { OpcionesNivelEducacional } from '../../opciones-nivel-educacional/entity/nivel-educacional.entity';
import { InstitucionesEntity } from '../../tramites/entity/instituciones.entity';
import { User } from '../../users/entity/user.entity';

@Entity('Solicitudes')
export class Solicitudes {
  @PrimaryGeneratedColumn()
  idSolicitud: number;

  @Column()
  idTipoSolicitud: number;

  @Column()
  idPostulante: number;

  @ManyToOne(() => PostulantesEntity, postulante => postulante.solicitudes)
  @JoinColumn({ name: 'idPostulante' })
  readonly postulante: PostulantesEntity;

  @Column({
    nullable: true,
  })
  idUsuario: number;

  @Column({
    nullable: true,
  })
  idOficina: number;

  @ManyToOne(() => OficinasEntity, oficinas => oficinas.solicitudes)
  @JoinColumn({ name: 'idOficina' })
  readonly oficinas: OficinasEntity;

  @Column({
    nullable: true,
  })
  idEstado: number;

  @ManyToOne(() => EstadosSolicitudEntity, estadosSolicitudEntity => estadosSolicitudEntity.solicitudes)
  @JoinColumn({ name: 'idEstado' })
  readonly estadosSolicitud: EstadosSolicitudEntity;

  @ManyToOne(() => User, user => user.idUsuario)
  @JoinColumn({ name: 'idUsuario' })
  readonly user: User;

  @Column({
    nullable: true,
  })
  idInstitucion: number;

  @Column({
    nullable: true,
  })
  FechaCreacion: Date;

  @Column({
    nullable: true,
  })
  updated_at: Date;

  @Column({
    nullable: true,
  })
  fechaVencimiento: Date;

  @Column({
    nullable: true,
  })
  FechaCierre: Date;

  @ApiProperty()
  @OneToMany(() => TramitesEntity, tramites => tramites.solicitudes)
  readonly tramites: TramitesEntity[];

  @ApiProperty()
  @OneToMany(() => DocumentoSolicitudPostulanteEntity, documentos => documentos.solicitudes)
  readonly documentosSolicitudPostulante: DocumentoSolicitudPostulanteEntity[];

  @ManyToOne(() => InstitucionesEntity, institucion => institucion.idInstitucion)
  @JoinColumn({ name: 'idInstitucion' })
  readonly institucion: InstitucionesEntity;
}
