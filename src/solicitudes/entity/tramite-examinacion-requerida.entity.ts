import { Column, Entity, PrimaryGeneratedColumn } from "typeorm"

@Entity('TramiteExaminacionRequerida')
export class TramiteExaminacionRequeridaEntity
{
    @PrimaryGeneratedColumn()
    idtramiteexamreq :  number;

    @Column()
    idtipotramite: number;

    @Column()
    idclaselicencia: number;

    @Column()
    idtipoexaminacion: number;

    @Column()
    idOficina: number;
}

