import { Injectable, NotFoundException } from '@nestjs/common';
import { Solicitudes } from '../entity/solicitudes.entity';
import { getEntityManagerToken, InjectRepository } from '@nestjs/typeorm';
import { Binary, Brackets, EntityManager, getConnection, getManager, In, Repository } from 'typeorm';
import { CreateSolicitudDto } from '../DTO/createSolicitud.dto';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { CreateTramiteDto } from 'src/tramites/DTO/createTramite.dto';
import { CreateRegistroDto } from 'src/registro-pago/DTO/createRegistro.dto';
import { RegistroPago } from 'src/registro-pago/entity/registro.entity';
import { CreateSolicitudTramiteDto } from '../DTO/createSolicitudTramite.dto';
import { CreateTramiteLicenciaDto } from 'src/tramites-clase-licencia/DTO/createTramiteLicencia.dto';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { Resultado } from '../../utils/resultado';
import { SolicitudesEnProcesoDto, SolicitudTramitesEnProcesoDto } from '../DTO/solicitudesEnProceso.dto';
import {
  ColumnasSolicitudes,
  ColumnasSolicitudes2,
  ColumnasSolicitudesHistorial,
  EstadosSolicitudesActivas,
  EstadosSolicitudesNoActivas,
  PermisosNombres,
  relacionSolicitudes,
  relacionTramiteConExamenes,
  TipoExaminaciones,
} from 'src/constantes';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { HistorialSolicitudDto } from '../DTO/historialSolicitud.dto';
import { FiltroHistorialSolicitudDto } from '../DTO/filtroHistorialSolicitud.dto';
import { TramiteConTipoDto } from 'src/tramites/DTO/tramiteConTipo.dto';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { TipoExaminacionesEntity } from 'src/cola-examinacion/entity/tipoexaminacion.entity';
import { apelacionesEntity } from '../entity/apelaciones.entity';
import { docsapelacionesEntity } from '../entity/docsapelaciones.entity';
import { CreateDocsApelacionesDto } from '../DTO/createDocsApelaciones.dto';
import { CreateApelacionesDto } from '../DTO/createApelaciones.dto';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { CrearSolicitudConTramitesDTO, CrearTramiteDTO, CrearTramiteSolicitudDTO } from '../DTO/crearSolicitudConTramites.dto';
import { TramiteExaminacionRequeridaEntity } from '../entity/tramite-examinacion-requerida.entity';
import { FormulariosExaminacionesEntity } from 'src/formularios-examinaciones/entity/formularios-examinaciones.entity';
import { FormularioExaminacionClaseLicenciaEntity } from 'src/formularios-examinaciones/entity/FormularioExaminacionClaseLicencia.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { ExaminacionCompartidaDetalleEntity } from '../entity/examinacion-compartida-detalle.entity';
import { ExaminacionCompartidaEntity } from '../entity/examinacion-compartida.entity';
import { ColaExaminacionTramiteEntity } from '../entity/cola-examinacion-tramite.entity';
import { AuthService } from 'src/auth/auth.service';
import { UpdateEstadoExaminacionByIdExaminacionDto } from '../DTO/UpdateEstadoExaminacionByIdExaminacion.dto';
import { CambiosEstadoService } from 'src/cambios-estado/services/cambios-estado.service';
import { SolicitudCambioEstadoDTO } from 'src/cambios-estado/dto/solicitud-cambio-estado.dto';
import { TramiteCambioEstadoDTO } from 'src/cambios-estado/dto/tramite-cambio-estado.dto';
import { OportunidadEntity } from 'src/oportunidad/oportunidad.entity';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { ServiciosComunesService } from 'src/shared/services/ServiciosComunes/ServiciosComunes.services';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';
import { OrdenTramiteExaminacionEntity } from 'src/tipos-tramites/entity/ordenTramiteExaminacion.entity';

@Injectable()
export class SolicitudesService {
  constructor(
    private authService: AuthService,
    private cambioEstadoService: CambiosEstadoService,

    @InjectRepository(Solicitudes)
    private readonly solicitudRespository: Repository<Solicitudes>,
    @InjectRepository(EstadosSolicitudEntity)
    private readonly estadosSolicitudRespository: Repository<EstadosSolicitudEntity>,
    @InjectRepository(TramitesEntity)
    private readonly tramiteRespository: Repository<TramitesEntity>,
    @InjectRepository(TramitesClaseLicencia)
    private readonly tramiteLicenciaRespository: Repository<TramitesClaseLicencia>,
    @InjectRepository(RegistroPago)
    private readonly regPagoRespository: Repository<RegistroPago>,
    @InjectRepository(ColaExaminacionEntity)
    private readonly colaExaminacionRepository: Repository<ColaExaminacionEntity>,
    @InjectRepository(TipoExaminacionesEntity)
    private readonly tipoexaminacionRepository: Repository<TipoExaminacionesEntity>,
    @InjectRepository(apelacionesEntity)
    private readonly apelacionesRepository: Repository<apelacionesEntity>,
    @InjectRepository(docsapelacionesEntity)
    private readonly docapelRepository: Repository<docsapelacionesEntity>,

    private readonly ingresoPostulanteService: IngresoPostulanteService,

    private serviciosComunesService: ServiciosComunesService // @InjectRepository(OportunidadEntity) // private readonly oportunidadRepository: Repository<OportunidadEntity>,
  ) {}

  async getSolicitudes() {
    return await this.solicitudRespository.find();
  }

  async getSolicitud(id: number) {
    const postulante = await this.solicitudRespository.findOne(id);
    if (!postulante) throw new NotFoundException('solicitud dont exist');

    return postulante;
  }

  async getHistorialSolicitud(req: any, idTramite: number) {
    const resultado: Resultado = new Resultado();
    let tramiteClaseLicencia: TramitesClaseLicencia;
    let colaExaminacionOportunidades: OportunidadEntity;

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaTramitesEnProceso
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      tramiteClaseLicencia = await this.tramiteLicenciaRespository.findOne({
        relations: [
          'tramite',
          'clasesLicencias',
          'tramite.solicitudes',
          'tramite.solicitudes.postulante',
          'tramite.tiposTramite',
          'tramite.estadoTramite',
          //'tramite.colaExaminacion',
          'tramite.colaExaminacionNM',
          'tramite.colaExaminacionNM.colaExaminacion',
          'tramite.colaExaminacionNM.colaExaminacion.tipoExaminaciones',
          'tramite.colaExaminacionNM.colaExaminacion.oportunidadExaminacion',
          //'tramite.solicitudes.documentosSolicitudPostulante',
        ],
        where: qb => {
          qb.where('TramitesClaseLicencia__tramite.idTramite = :id', {
            id: idTramite,
          });
        },
      });

      const solicitudDTO: HistorialSolicitudDto = new HistorialSolicitudDto();

      // Se mapea la respuesta
      if (tramiteClaseLicencia != null) {
        solicitudDTO.idSolicitud = tramiteClaseLicencia.tramite.idSolicitud;
        solicitudDTO.run =
          tramiteClaseLicencia.tramite.solicitudes.postulante.RUN.toString() + '-' + tramiteClaseLicencia.tramite.solicitudes.postulante.DV;
        solicitudDTO.nombrePostulante =
          tramiteClaseLicencia.tramite.solicitudes.postulante.Nombres +
          ' ' +
          tramiteClaseLicencia.tramite.solicitudes.postulante.ApellidoPaterno +
          ' ' +
          tramiteClaseLicencia.tramite.solicitudes.postulante.ApellidoMaterno;
        solicitudDTO.tipoTramite = tramiteClaseLicencia.tramite.tiposTramite.Nombre;
        solicitudDTO.idTramite = tramiteClaseLicencia.tramite.idTramite;
        solicitudDTO.estadoTramite = tramiteClaseLicencia.tramite.estadoTramite.Nombre;
        solicitudDTO.estadoSolicitud = tramiteClaseLicencia.tramite.estadoTramite.Nombre;
        solicitudDTO.fechaSolicitud = tramiteClaseLicencia.tramite.solicitudes.FechaCreacion;
        solicitudDTO.fechaFinSolicitud = tramiteClaseLicencia.tramite.solicitudes.FechaCierre;
        solicitudDTO.updated_at = tramiteClaseLicencia.tramite.solicitudes.updated_at;
        solicitudDTO.observaciones = tramiteClaseLicencia.tramite.observacion;
        solicitudDTO.examenTeorico = 0;
        solicitudDTO.examenPractico = 0;
        solicitudDTO.examenMedico = 0;
        solicitudDTO.idoneidadMoral = 0;
        if (tramiteClaseLicencia.tramite.colaExaminacionNM.length > 0) {
          tramiteClaseLicencia.tramite.colaExaminacionNM.forEach(colaExaminacionNM => {
            let examinacion = colaExaminacionNM.colaExaminacion;

            if (examinacion.aprobado != null) {
              if (examinacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenTeorico) {
                solicitudDTO.examenTeorico = examinacion.aprobado == true ? 1 : -1;
                solicitudDTO.oportunidadesTeorico = examinacion.oportunidadExaminacion;
                solicitudDTO.idExamenTeorico = examinacion.idColaExaminacion;
              } else if (examinacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenPractico) {
                solicitudDTO.examenPractico = examinacion.aprobado == true ? 1 : -1;
                solicitudDTO.oportunidadesPractico = examinacion.oportunidadExaminacion;
                solicitudDTO.idExamenPractico = examinacion.idColaExaminacion;
              } else if (examinacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenMedico) {
                solicitudDTO.examenMedico = examinacion.aprobado == true ? 1 : -1;
              } else if (examinacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenIM) {
                solicitudDTO.idoneidadMoral = examinacion.aprobado == true ? 1 : -1;
              }
            }
          });
        }

        solicitudDTO.documentos = [];

        if (tramiteClaseLicencia.clasesLicencias) solicitudDTO.claseLicencia = tramiteClaseLicencia.clasesLicencias.Abreviacion;
        else solicitudDTO.claseLicencia = '';

        resultado.Respuesta = solicitudDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontró la solicitud';
      }
    } catch (error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo la solicitud';
    }

    return resultado;
  }

  /**
   * Metodo para crear Solicitud, Tramite y TramiteLicencia
   * llamando a la funcion createColaExaminacion
   * @param createSolicitudTramiteDto
   */
  async create(createSolicitudTramiteDto: CreateSolicitudTramiteDto): Promise<any> {}

  async cancelarSolicitud(idSolicitud: number) {
    const solicitud = await this.solicitudRespository.findOne(idSolicitud);
    const tramites = await this.tramiteRespository.find({ idSolicitud });
    console.log(idSolicitud);
    console.log(solicitud);
    console.log(tramites);
    return await getManager().transaction(async (manager: EntityManager) => {
      const dto = new SolicitudCambioEstadoDTO();

      dto.idSolicitud = idSolicitud;
      dto.idEstadoSolicitud = 5; // Cerrada
      dto.idEstadoSolicitudPrevio = solicitud.idEstado;
      dto.observacion = 'Cancelacion de solicitud';

      const sali = await this.cambioEstadoService.postSolicitudCambiarEstado(dto);
      console.log('---');
      console.log(sali);
      if (tramites) {
        for (const tramite of tramites) {
          const dto = new TramiteCambioEstadoDTO();

          dto.idEstadoTramite = 22;
          dto.idEstadoTramitePrevio = tramite.idEstadoTramite;
          dto.idTramite = tramite.idTramite;
          dto.observacion = 'Cancelacion de solicitud';

          await this.cambioEstadoService.postTramiteCambiarEstado(dto);
        }
      }
      return true;
    });
  }

  async crearTramiteDeSolicitud(dto: CrearTramiteSolicitudDTO) {
    return await getManager().transaction(async (manager: EntityManager) => {
      const idUsuario = (await this.authService.usuario()).idUsuario;
      const idOficina = this.authService.oficina().idOficina;

      return await this.crearTramite(manager, dto, idOficina, idUsuario);
    });
  }

  async crearSolicitudConTramites(dto: CrearSolicitudConTramitesDTO) {
    //
    //  valores fijos
    // ------------------
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const idOficina = this.authService.oficina().idOficina;
    const idTipoSolicitud = 1;
    const idEstado = 1;

    return await getManager().transaction(async (manager: EntityManager) => {
      //
      //  solicitud
      // ------------------
      const FechaCreacion = new Date();
      const idInstitucion = dto.idInstitucion;
      const idPostulante = dto.idPostulante;
      const solicitud = await manager.save(Solicitudes, {
        idEstado,
        idInstitucion,
        idOficina,
        idPostulante,
        idTipoSolicitud,
        idUsuario,
        FechaCreacion,
      });
      const idSolicitud = solicitud.idSolicitud;

      for (const tramite of dto.tramites) {
        const data = { ...tramite, idSolicitud };
        await this.crearTramite(manager, data, idOficina, idUsuario);
      }

      return solicitud;
    });
  }

  async crearTramite(manager: EntityManager, tra: CrearTramiteSolicitudDTO, idOficina: number, idUsuario: number) {
    //
    //  tramite
    // ------------------
    const idSolicitud = tra.idSolicitud;
    const observacion = 'Creacion de solicitud ' + idSolicitud.toString();
    const idEstadoTramite = 17;
    const created_at = new Date();
    const update_at = created_at;
    const idTipoTramite = tra.idTipoTramite;
    const tramite = await manager.save(TramitesEntity, { idSolicitud, created_at, update_at, observacion, idTipoTramite, idEstadoTramite });
    //
    //  clase licencia
    // ------------------
    const idClaseLicencia = tra.idClaseLicencia;
    const idTramite = tramite.idTramite;
    const tramiteLicencia = await manager.save(TramitesClaseLicencia, { idClaseLicencia, idTramite });
    //
    //  pago
    // ------------------
    const updated_at = update_at;
    const pago = await manager.save(RegistroPago, { created_at, updated_at, idOficina, idSolicitud, idTramite, idUsuario });
    //
    //  cola examinacion
    // ------------------
    // await manager.query( 'select * from createcolaexaminacion( $1, $2, $3 )', [idTramite, idClaseLicencia, idTipoTramite]);
    return this.crearColasExaminaciones(manager, idTramite, idClaseLicencia, idTipoTramite, idSolicitud, idUsuario);
  }

  async crearColasExaminaciones(
    manager: EntityManager,
    idTramite: number,
    idClaseLicencia: number,
    idTipoTramite: number,
    idSolicitud: number,
    idUsuario: number
  ) {
    const examinaciones = await this.examinacionesRequeridas(manager, idTipoTramite, idClaseLicencia);
    //const examinaciones : OrdenTramiteExaminacionEntity[] = [];

    for (const examinacion of examinaciones) {
      const idTipoExaminacion = examinacion.idTipoExaminacion;
      const historico = false;
      const created_at = new Date();

      let idEstadosExaminacion = 0;

      // Asignar estado examinación según el tipo
      switch (examinacion.idTipoExaminacion as number) {
        case 2: {
          idEstadosExaminacion = 13;
          break;
        }
        case 3: {
          idEstadosExaminacion = 19;
          break;
        }
        case 4: {
          idEstadosExaminacion = 8;
          break;
        }
        case 5: {
          idEstadosExaminacion = 6;
          break;
        }
        case 6: {
          idEstadosExaminacion = 1;
          break;
        }
      }

      //const idEstadosExaminacion = 31;
      const compartidas = await this.examinacionesCompartidas(manager, idTipoTramite, idTipoExaminacion);
      const compartida = compartidas.some((item: any) => item.idClaseLicencia == idClaseLicencia);

      let idColaExaminacion = -1;
      let creada = false;

      //
      // Si es compartida busco a ver si ya existe
      // ----------------------------------------------
      if (compartida) {
        const cola = await this.colaBuscar(manager, idSolicitud, idTipoExaminacion);

        console.log('cola buscada', cola);

        if (cola) idColaExaminacion = cola.idColaExaminacion;
      }

      //
      // Existe la examinacion ?
      // ------------------------
      if (idColaExaminacion == -1) {
        const cola = await manager.save(ColaExaminacionEntity, {
          idTipoExaminacion,
          idTramite,
          historico,
          created_at,
          idEstadosExaminacion,
        });

        if (cola) {
          creada = true;
          idColaExaminacion = cola.idColaExaminacion;
        }
      }

      //
      // Existe la examinacion ?
      // ------------------------
      if (idColaExaminacion != -1) {
        const colaExaminacion = await manager.save(ColaExaminacionTramiteEntity, {
          idColaExaminacion,
          idTramite,
          idSolicitud,
          idTipoExaminacion,
        });

        //
        // Si la examinacion se creo, creo los formularios, sino no!
        // ----------------------------------------------------------
        if (creada) {
          const formularios = await this.formularios(manager, idClaseLicencia, idTipoExaminacion);

          for (const formulario of formularios) {
            const idFormularioExaminaciones = formulario.idFromularioExaminaciones;
            const UsuarioAprobador = idUsuario;
            const resultado = await manager.save(ResultadoExaminacionEntity, { idFormularioExaminaciones, UsuarioAprobador });
            const idResultadoExam = resultado.idResultadoExam;
            const resultadoExaminacion = await manager.save(ResultadoExaminacionColaEntity, { idResultadoExam, idColaExaminacion });
          }
        }
      } else {
        throw new NotFoundException();
      }
    }
  }

  async colaBuscar(manager: EntityManager, idSolicitud: number, idTipoExaminacion: number) {
    const cola = await manager
      .createQueryBuilder()
      .select('cola.*')
      .from(ColaExaminacionTramiteEntity, 'cola')
      .innerJoin(TramitesEntity, 'tra', 'cola.idTramite        = tra.idTramite')
      .innerJoin(ColaExaminacionEntity, 'exa', 'exa.idColaExaminacion = cola.idColaExaminacion')
      .where('exa.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion })
      .andWhere('tra.idSolicitud       = :idSolicitud', { idSolicitud })
      .getRawOne();
    return cola;
  }

  async examinacionesRequeridas(
    manager: EntityManager,
    idTipoTramite: number,
    idClaseLicencia: number
  ): Promise<OrdenTramiteExaminacionEntity[]> {
    // const examinaciones = await manager
    //   .createQueryBuilder()
    //   .from(TramiteExaminacionRequeridaEntity, 'exa')
    //   .where('exa.idtipotramite   = :idTipoTramite', { idTipoTramite })
    //   .andWhere('exa.idclaselicencia = :idClaseLicencia', { idClaseLicencia })
    //   .getRawMany();
    // return examinaciones;
    let examinaciones: OrdenTramiteExaminacionEntity[] = await manager
      .createQueryBuilder()
      .from(OrdenTramiteExaminacionEntity, 'exa')
      .where('exa.idTipoTramite   = :idTipoTramite', { idTipoTramite })
      //.andWhere('exa.idclaselicencia = :idClaseLicencia', { idClaseLicencia })
      .getRawMany();

    // Recogemos la primera oficina asociada para no devolver duplicados
    if (examinaciones && examinaciones.length > 0 && examinaciones.map(x => x.idOficina).length > 1) {
      const idOficinaPrimeraCoincidencia: number = examinaciones[0].idOficina;

      examinaciones = examinaciones.filter(x => x.idOficina == idOficinaPrimeraCoincidencia);
    }

    return examinaciones;
  }

  async formularios(manager: EntityManager, idClaseLicencia: number, idTipoExaminacion: number) {
    console.log('=====================================================================================================================');
    console.log('==================================== formularios ====================================================================');
    console.log('=====================================================================================================================');
    const formularios = await manager
      .createQueryBuilder()
      .select('fe.*')
      .from(FormulariosExaminacionesEntity, 'fe')
      .innerJoin(FormularioExaminacionClaseLicenciaEntity, 'fecl', 'fe.idFromularioExaminaciones = fecl.idFormularioExaminaciones')
      .where('fecl.idClaseLicencia = :idClaseLicencia', { idClaseLicencia })
      .andWhere('fe.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion })
      .getRawMany();
    return formularios;
  }

  async examinacionesCompartidas(manager: EntityManager, idTipoTramite: number, idTipoExaminacion: number) {
    const compartidas = await manager
      .createQueryBuilder()
      .select('det.*')
      .from(ExaminacionCompartidaDetalleEntity, 'det')
      .innerJoin(ExaminacionCompartidaEntity, 'mae', 'det.idExaminacionCompartida = mae.idExaminacionCompartida')
      .where('mae.idTipoTramite     = :idTipoTramite', { idTipoTramite })
      .andWhere('mae.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion })

      .getRawMany();
    return compartidas;
  }

  async update(idSolicitud: number, data: Partial<CreateSolicitudDto>) {
    await this.solicitudRespository.update({ idSolicitud }, data);
    return await this.solicitudRespository.findOne({ idSolicitud });
  }

  async getEstadosSolicitud() {
    return await this.estadosSolicitudRespository.find({});
  }

  async updateEstadoSolicitud(idSolicitud: number, idEstado: number) {
    const solicitud = await this.solicitudRespository.findOne(idSolicitud);
    const dto = new SolicitudCambioEstadoDTO();

    dto.idSolicitud = idSolicitud;
    dto.idEstadoSolicitud = idEstado;
    dto.idEstadoSolicitudPrevio = solicitud.idEstado;
    dto.observacion = '';

    await this.cambioEstadoService.postSolicitudCambiarEstado(dto);
    await this.updateTramites(idSolicitud);
    return { code: 200, message: 'success' };
  }

  async getTieneClaseLicencia(RUN: number, idCl: number) {
    console.log(RUN);
    console.log(idCl);
    return await this.solicitudRespository.query('select tieneClaseLicencia(' + RUN + ',' + idCl + ')');
  }

  async getLicById(idClaseLicencia: number): Promise<any> {
    const claseLic = await getConnection().query(`select get_lic_por_id(${idClaseLicencia}) as resp`);

    let clasLicencia = claseLic[0].resp.Abreviacion;
    console.log(clasLicencia);
    return clasLicencia;
  }

  async updateTramites(idSolicitud) {
    const tramites: TramitesEntity[] = await this.tramiteRespository.find({ where: { idSolicitud: idSolicitud } });
    for (const tramite of tramites) {
      //======= aqui se deben ir agregando los diferentes casos segun el tipo de tramite ========//
      //1-Primer Otorgamiento
      //2-Control/Renovación (Ley 19.495)
      //3-Control/Renovación (Ley 18.290).
      //4-Duplicado
      //5-cambio de domicilio
      //7-Reconsideraciones Servicio Médico Legal (SML)
      //8-Reconsideraciones Juzgado Policía Local (JPL)
      //9-Documento Licencia Diplomático
      //11-Recuperación Licencia Chilena desde Canje
      //12-Canje de Licencia Extranjeros Perú - España - Corea del Sur

      if (
        tramite.idTipoTramite == 1 ||
        tramite.idTipoTramite == 2 ||
        tramite.idTipoTramite == 3 ||
        tramite.idTipoTramite == 12 ||
        tramite.idTipoTramite == 11
      ) {
        tramite.idEstadoTramite = 1;
      } else if (tramite.idTipoTramite == 5 || tramite.idTipoTramite == 9) {
        tramite.idEstadoTramite = 3;
      } else {
        console.log(tramite.idTipoTramite);
      }

      await this.tramiteRespository.save(tramite);
    }
  }
  //============================================================================================//

  async tieneClaseLicenciaRNC(RUN: string, idClaseLicencia: number, idTipoTramite: number) {
    console.log(' ======================================================================================================= ');
    console.log(' ======================================== idTipoTramite ================================================= ');
    console.log(' ======================================================================================================= ');
    console.log({ RUN, idClaseLicencia, idTipoTramite });
    let salida;
    const claseLic = await getConnection().query(`select get_lic_por_id(${idClaseLicencia}) as resp`);
    let _tipoLic = claseLic[0].resp.Abreviacion;

    const idUsuario = (await this.authService.usuario()).idUsuario;
    let dataConsulta = {
      idUsuario: idUsuario,
      run: RUN,
    };

    let consulLic = await this.ingresoPostulanteService.consulLic(dataConsulta);

    if (consulLic == undefined) {
      consulLic = [];
    }

    if (consulLic == undefined || consulLic == '') {
      salida = false;
    } else {
      salida = true;
    }

    let lics = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));

    let filtroLic = lics.filter(Lic => Lic.licTipo == _tipoLic);

    //===== clases de licencias para control de renovacion 19495 (idTranite=2) ========//
    let filtroLicA1 = lics.filter(Lic => Lic.licTipo == 'A1');
    let filtroLicA2 = lics.filter(Lic => Lic.licTipo == 'A2');
    let filtroLicA3 = lics.filter(Lic => Lic.licTipo == 'A3');
    let filtroLicA4 = lics.filter(Lic => Lic.licTipo == 'A4');
    let filtroLicA5 = lics.filter(Lic => Lic.licTipo == 'A5');

    //===== clases de licencias para control de renovacion 18290 (idTranite=3) ========//
    let filtroLicA1P = lics.filter(Lic => Lic.licTipo == 'A1P');
    let filtroLicA2P = lics.filter(Lic => Lic.licTipo == 'A2P');

    //===== clases de licencias para diplomatico (idTranite=9) ========//
    let filtroLic_diplomatico = lics.filter(Lic => Lic.licTipo == 'B');

    //==================== Primer otorgamiento =============================//
    let filtroLicPO_B = lics.filter(Lic => Lic.licTipo == 'B');
    let filtroLicPO_C = lics.filter(Lic => Lic.licTipo == 'C');
    let filtroLicPO_A1 = lics.filter(Lic => Lic.licTipo == 'A1');
    let filtroLicPO_A2 = lics.filter(Lic => Lic.licTipo == 'A2');
    let filtroLicPO_A3 = lics.filter(Lic => Lic.licTipo == 'A3');
    let filtroLicPO_A4 = lics.filter(Lic => Lic.licTipo == 'A4');
    let filtroLicPO_A5 = lics.filter(Lic => Lic.licTipo == 'A5');
    let filtroLicPO_D = lics.filter(Lic => Lic.licTipo == 'D');
    let filtroLicPO_E = lics.filter(Lic => Lic.licTipo == 'D');
    let filtroLicPO_F = lics.filter(Lic => Lic.licTipo == 'F');
    let filtroLicPO_CR = lics.filter(Lic => Lic.licTipo == 'CR');

    if (idTipoTramite == 1) {
      if (filtroLicPO_B == '' || filtroLicPO_B == undefined) {
        salida = false;
      } else if (filtroLicPO_C == '' || filtroLicPO_C == undefined) {
        salida = false;
      } else if (filtroLicPO_A1 == '' || filtroLicPO_A1 == undefined) {
        salida = false;
      } else if (filtroLicPO_A2 == '' || filtroLicPO_A2 == undefined) {
        salida = false;
      } else if (filtroLicPO_A3 == '' || filtroLicPO_A3 == undefined) {
        salida = false;
      } else if (filtroLicPO_A4 == '' || filtroLicPO_A4 == undefined) {
        salida = false;
      } else if (filtroLicPO_A5 == '' || filtroLicPO_A5 == undefined) {
        salida = false;
      } else if (filtroLicPO_D == '' || filtroLicPO_D == undefined) {
        salida = false;
      } else if (filtroLicPO_E == '' || filtroLicPO_E == undefined) {
        salida = false;
      } else if (filtroLicPO_F == '' || filtroLicPO_F == undefined) {
        salida = false;
      } else if (filtroLicPO_CR == '' || filtroLicPO_CR == undefined) {
        salida = false;
      } else {
        salida = true;
      }
    }

    //============ tramites de cambio de domicilio (idTranite=5) ===============//
    if (filtroLic == undefined || filtroLic == '') {
      salida = false;
    } else {
      if (idTipoTramite == 5) {
        let licEstadoClaLic = filtroLic[0].licEstadoClaLic;
        if (licEstadoClaLic != 103) {
          salida = false;
        } else {
          salida = true;
        }
      }
      //============ tramites control de renovacion 19495 (idTranite=2) ===============//
      if (idTipoTramite == 2) {
        if (filtroLicA1 == '' || filtroLicA1 == undefined) {
          salida = false;
        } else if (filtroLicA2 == '' || filtroLicA2 == undefined) {
          salida = false;
        } else if (filtroLicA3 == '' || filtroLicA3 == undefined) {
          salida = false;
        } else if (filtroLicA4 == '' || filtroLicA4 == undefined) {
          salida = false;
        } else if (filtroLicA5 == '' || filtroLicA5 == undefined) {
          salida = false;
        } else {
          salida = true;
        }
      }

      //============ tramites control de renovacion 18290 (idTranite=3) ===============//
      if (idTipoTramite == 3) {
        if (filtroLicA1P == '' || filtroLicA1P == undefined) {
          salida = false;
        } else if (filtroLicA2P == '' || filtroLicA2P == undefined) {
          salida = false;
        } else {
          salida = true;
        }
      }

      //============ tramites Licencias diplomaticas (idTranite=9) ===============//
      if (idTipoTramite == 9) {
        if (filtroLic_diplomatico == '' || filtroLic_diplomatico == undefined) {
          salida = false;
        } else {
          salida = true;
        }
      }

      salida = true;
    }

    console.log(salida);
    return [{ tieneclaselicencia: salida }];
  }

  async getEdad(RUN: number) {
    const data = await this.solicitudRespository.query('select getEdad(' + RUN + ')');

    return data;
  }

  async getEdad2(RUN: number) {
    const data = await this.solicitudRespository.query('select getEdad(' + RUN + ')');

    let edad = data[0].getedad.years;
    console.log(edad);
    return edad;
  }

  async getTieneTramitePrimerOtorgamientoIniciadoInferiorA6Meses(RUN: number) {
    const data = await this.solicitudRespository.query('select tieneTramitePrimerOtorgamientoIniciadoInferiorA6Meses(' + RUN + ')');

    return data;
  }

  async getValidarReglaNegocioPrimeroB(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select validarReglaNegocioPrimeroB(' + RUN + ',' + idCl + ')');

    return data;
  }

  async getValidarReglaNegocioPrimeroB17(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select validarReglaNegocioPrimeroB17(' + RUN + ',' + idCl + ')');

    return data;
  }

  async getValidarReglaNegocioPrimeroC(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select validarReglaNegocioPrimeroC(' + RUN + ',' + idCl + ')');

    return data;
  }

  async getValidarReglaNegocioPrimeroProfesionales(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select validarreglanegocioprimeroa(' + RUN + ',' + idCl + ')');

    return data;
  }

  async getValidarReglaNegocioPrimeroD(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select validarReglaNegocioPrimeroD(' + RUN + ',' + idCl + ')');

    return data;
  }

  async getValidarReglaNegocioPrimeroE(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select validarReglaNegocioPrimeroE(' + RUN + ',' + idCl + ')');

    return data;
  }

  async getRenovacionBcDeTieneClaseLicencia(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select renovacionBcDeTieneClaseLicencia(' + RUN + ',' + idCl + ')');

    return data;
  }
  async getTramiteprimotorginiinf6mesesclasec(RUN: number, idCl: number) {
    const data = await this.solicitudRespository.query('select tramiteprimotorginiinf6mesesclasec(' + RUN + ',' + idCl + ')');

    return data;
  }

  async getSolicitudesEnProceso(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    const orderBy = ColumnasSolicitudes[paginacionArgs.orden.orden];
    const idOficina = this.authService.oficina().idOficina;

    let solicitudesCLPaginados: Pagination<Solicitudes>;
    let solicitudesParseadas = new PaginacionDtoParser<SolicitudesEnProcesoDto>();

    let filtro = '';

    try {
      if (paginacionArgs.filtro != null && paginacionArgs.filtro != '') {
        filtro = paginacionArgs.filtro;

        while (filtro.search('/') != -1) {
          filtro = filtro.replace('/', '-');
        }
      }
      console.log(filtro);

      solicitudesCLPaginados = await paginate<Solicitudes>(this.solicitudRespository, paginacionArgs.paginationOptions, {
        relations: relacionSolicitudes,
        where: qb => {
          //qb.where('Solicitudes__estadosSolicitud.Nombre IN(:...estadosSolicitudes)', { estadosSolicitudes: EstadosSolicitudesActivas })
          qb.andWhere(
            new Brackets(subQb => {
              subQb
                .where(
                  "lower(unaccent(Solicitudes__postulante.Nombres)) || ' ' || lower(unaccent(Solicitudes__postulante.ApellidoPaterno)) || ' ' || lower(unaccent(Solicitudes__postulante.ApellidoMaterno)) like lower(unaccent(:nombre))",
                  { nombre: `%${filtro}%` }
                )
                .andWhere('Solicitudes.idOficina = :idOficina', { idOficina })

                .orWhere(
                  "lower(unaccent(Solicitudes__tramites__tiposTramite.Nombre)) || ' ' || lower(unaccent(Solicitudes__tramites__tramitesClaseLicencia__clasesLicencias.Abreviacion)) like lower(unaccent(:tipoTramite))",
                  { tipoTramite: `%${filtro}%` }
                )
                .orWhere('lower(unaccent(Solicitudes__estadosSolicitud.Nombre)) like lower(unaccent(:estadoSolicitud))', {
                  estadoSolicitud: `%${filtro}%`,
                })
                .orWhere("Solicitudes__postulante.RUN || '-' || Solicitudes__postulante.DV like :RUN", { RUN: `%${filtro}%` })
                .orWhere(`to_char(Solicitudes.FechaCreacion, 'DD-MM-YYYY') like :fechaSolicitud`, { fechaSolicitud: `%${filtro}%` })
                .orWhere('CAST(Solicitudes.idSolicitud AS TEXT) like :idSolicitud', { idSolicitud: `%${filtro}%` });
            })
          ).orderBy(orderBy, paginacionArgs.orden.ordenarPor);
        },
      });

      let solicitudesDTO: SolicitudesEnProcesoDto[] = [];

      if (Array.isArray(solicitudesCLPaginados.items) && solicitudesCLPaginados.items.length) {
        solicitudesCLPaginados.items.forEach(solicitud => {
          const solicitudDTO: SolicitudesEnProcesoDto = new SolicitudesEnProcesoDto();
          solicitudDTO.idSolicitud = solicitud.idSolicitud;

          let tramitesConTipo: TramiteConTipoDto[] = [];

          solicitud.tramites.forEach(tramite => {
            let tramiteConTipo = new TramiteConTipoDto();
            tramiteConTipo.idTramite = tramite.idTramite;
            tramiteConTipo.estadoTramite = tramite.tiposTramite.Descripcion;

            if (tramite.tramitesClaseLicencia)
              tramiteConTipo.tipoTramite = tramite.tiposTramite.Nombre + ' ' + tramite.tramitesClaseLicencia.clasesLicencias.Abreviacion;
            else tramiteConTipo.tipoTramite = tramite.tiposTramite.Nombre;

            tramitesConTipo.push(tramiteConTipo);
          });

          solicitudDTO.tramites = tramitesConTipo;
          solicitudDTO.run = solicitud.postulante.RUN.toString() + '-' + solicitud.postulante.DV;
          solicitudDTO.nombrePostulante =
            solicitud.postulante.Nombres + ' ' + solicitud.postulante.ApellidoPaterno + ' ' + solicitud.postulante.ApellidoMaterno;
          solicitudDTO.estadoSolicitud = solicitud.estadosSolicitud.Nombre;
          solicitudDTO.fechaSolicitud = solicitud.FechaCreacion;
          //solicitudDTO.estadoTramite = solicitud.tramites[0].estadoTramite

          solicitudesDTO.push(solicitudDTO);
        });

        solicitudesParseadas.items = solicitudesDTO;
        solicitudesParseadas.meta = solicitudesCLPaginados.meta;
        solicitudesParseadas.links = solicitudesCLPaginados.links;

        resultado.Respuesta = solicitudesParseadas;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Solicitudes obtenidas correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron solicitudes';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo solicitudes';
    }

    return resultado;
  }

  async getSolicitudesEnProcesoV2(req: any, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    const orderBy = ColumnasSolicitudes2[paginacionArgs.orden.orden];
    const filtro = paginacionArgs.filtro;
    /* 
    console.log('--------------------------------------------------------------');
    console.log(paginacionArgs);
    console.log('--------------------------------------------------------------'); */

    // Estados de solicitudes admitidos : 3 - "Hora reservada, FI y ACT enviados"
    //                                    4 - ""Hora Reservada""
    //                                    7  - "Abierta"
    //                                    9  - "Mesón"
    //                                    12 - "Re abierta por reconsideración apelación JPL o SML"

    const estadoTramitesPermitidos: number[] = [3, 4, 7, 9, 12];

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaSolicitudesEnProceso
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      // 1. Hacemos la consulta
      const qbSolicitudes = this.solicitudRespository
        .createQueryBuilder('Solicitudes')
        .innerJoinAndMapOne('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne(
          'Solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'Solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .innerJoinAndMapMany(
          'tramites.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'tramites.idTramite = colaExaminacion.idTramite'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        )
        //Inner Joins para filtros
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdTramite',
          TramitesEntity,
          'tramitesFiltroIdTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdTramite.idSolicitud'
        )
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdTipoTramite',
          TramitesEntity,
          'tramitesFiltroIdTipoTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdTipoTramite.idSolicitud'
        )
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdEstadoTramite',
          TramitesEntity,
          'tramitesFiltroIdEstadoTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdEstadoTramite.idSolicitud'
        )

        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdClaseLicencia',
          TramitesEntity,
          'tramitesFiltroIdClaseLicencia',
          'Solicitudes.idSolicitud = tramitesFiltroIdClaseLicencia.idSolicitud'
        )
        .innerJoinAndMapOne(
          'tramitesFiltroIdClaseLicencia.tramitesClaseLicenciaFiltroIdClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicenciaFiltroIdClaseLicencia',
          'tramitesFiltroIdClaseLicencia.idTramite = tramitesClaseLicenciaFiltroIdClaseLicencia.idTramite'
        );
      //   'tramite.colaExaminacionNM',
      //   'tramite.colaExaminacionNM.colaExaminacion',
      //   'tramite.colaExaminacionNM.colaExaminacion.tipoExaminaciones'

      // 2. Recogemos sólo solicitudes en proceso
      qbSolicitudes.andWhere('Solicitudes.idEstado in (:...estados)', { estados: estadoTramitesPermitidos }); // 3.b Añadimos condición para filtrar por oficina si se da el caso // Filtramos por las oficinas en caso de que el usuario no sea administrador //Comprobamos si es superUsuario por si es necesario filtrar

      // filtramos por oficina en caso de que no sea superuser
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null) {
          resultado.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          resultado.ResultadoOperacion = false;
          resultado.Respuesta = [];

          return resultado;
        } else {
          qbSolicitudes.andWhere('Solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
        }
      }

      // 3. Añadimos los filtros que vienen del front

      // filtro RUN
      if (filtro.run) {
        const rut = filtro.run.split('-');

        const div = rut[1];
        let runsplited: string = rut[0];

        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');

        qbSolicitudes.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbSolicitudes.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }

      // Filtro id Solicitud
      if (filtro.idSolicitud) {
        qbSolicitudes.andWhere('Solicitudes.idSolicitud = :idSolicitud', {
          idSolicitud: filtro.idSolicitud,
        });
      }

      // Filtro Estado Solicitud
      if (filtro.idEstadoSolicitud) {
        qbSolicitudes.andWhere('Solicitudes.idEstado = :_idEstadoSolicitud', {
          _idEstadoSolicitud: filtro.idEstadoSolicitud,
        });
      }

      // Filtro id Tramite, que la solicitud contenga el trámite con el id facilitado
      if (filtro.idTramite) {
        qbSolicitudes.andWhere('tramitesFiltroIdTramite.idTramite = :_idTramite', {
          _idTramite: filtro.idTramite,
        });
      }

      // Filtro nombre y apellidos
      if (filtro.nombreApellido) {
        qbSolicitudes.andWhere(
          '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${filtro.nombreApellido}%`,
          }
        );
      }

      // Filtro Fecha Inicio Solicitud
      if (filtro.fechaSolicitud) {
        qbSolicitudes.andWhere('"Solicitudes"."FechaCreacion"::date = :_fechaCreacion::date', {
          _fechaCreacion: filtro.fechaSolicitud,
        });
      }

      // if (fechaCreacion)
      // qbSolicitudes.andWhere('"Solicitudes"."FechaCreacion"::date = :created_at::date', {
      //   //"FechaCreacion" > '2022-07-20'
      //   created_at: fechaCreacion,
      // });

      if (filtro.idTipoTramite) {
        qbSolicitudes.andWhere('tramitesFiltroIdTipoTramite.idTipoTramite = :_idTipoTramite', {
          _idTipoTramite: filtro.idTipoTramite,
        });
      }

      if (filtro.idEstadoTramite) {
        qbSolicitudes.andWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = :_idEstadoTramite', {
          _idEstadoTramite: filtro.idEstadoTramite,
        });
      }

      if (filtro.idClaseLicencia) {
        qbSolicitudes.andWhere('tramitesClaseLicenciaFiltroIdClaseLicencia.idClaseLicencia = :_idClaseLicencia', {
          _idClaseLicencia: filtro.idClaseLicencia,
        });
      }

      //Se agrega el orden
      qbSolicitudes.orderBy(orderBy, paginacionArgs.orden.ordenarPor);
      /*    if (paginacionArgs.orden.orden === 'RUN') qbSolicitudes.orderBy('postulante.RUN', paginacionArgs.orden.ordenarPor);
         if (paginacionArgs.orden.orden === 'Id' ) qbSolicitudes.orderBy('Solicitudes.idSolicitud', paginacionArgs.orden.ordenarPor);
         if (paginacionArgs.orden.orden === 'NombrePostulante' ) qbSolicitudes.orderBy('postulante.Nombres', paginacionArgs.orden.ordenarPor); */

      let solicitudes: any = await paginate<Solicitudes>(qbSolicitudes, paginacionArgs.paginationOptions);
      //solicitudes = await qbSolicitudes.getMany();

      // Mapeo hacia DTO
      let solicitudesEnProceso: SolicitudesEnProcesoDto[] = [];

      solicitudes.items.forEach(element => {
        let sep: SolicitudesEnProcesoDto = new SolicitudesEnProcesoDto();

        sep.run = element.postulante.RUN + '-' + element.postulante.DV;
        sep.idSolicitud = element.idSolicitud;
        sep.estadoSolicitud = element.estadosSolicitud.Nombre;
        sep.nombrePostulante =
          element.postulante.Nombres + ' ' + element.postulante.ApellidoPaterno + ' ' + element.postulante.ApellidoMaterno;
        sep.fechaSolicitud = element.FechaCreacion;
        sep.idPostulante = element.postulante.idPostulante;

        // Datos de trámite
        sep.tramitesAsociados = [];

        element.tramites.forEach(t => {
          let sept: SolicitudTramitesEnProcesoDto = new SolicitudTramitesEnProcesoDto();

          sept.idTramite = t.idTramite;
          sept.estadoTramite = t.estadoTramite.Nombre;
          sept.tipoTramite = t.tiposTramite.Nombre;
          sept.claseLicenciaTramite = t.tramitesClaseLicencia.clasesLicencias.Abreviacion;

          sep.tramitesAsociados.push(sept);
        });

        solicitudesEnProceso.push(sep);
      });

      solicitudes.items = solicitudesEnProceso;

      resultado.ResultadoOperacion = true;
      resultado.Respuesta = solicitudes;
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo solicitudes';
    }

    return resultado;
  }

  async getHistorialSolicitudes(req, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    let esUsuarioAdmin: boolean = false;

    const resultado: Resultado = new Resultado();

    let solicitudesParseadas = new PaginacionDtoParser<HistorialSolicitudDto>();
    const orderBy = ColumnasSolicitudesHistorial[paginacionArgs.orden.orden];
    const filtro: FiltroHistorialSolicitudDto = paginacionArgs.filtro;
    let filtroBusqueda = paginacionArgs.filtroBusqueda;

    let tramitesClaseLicenciaPag: Pagination<TramitesClaseLicencia>;

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaHistorialDeSolicitudes
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      let idOficinasAsociadas: number[] = [];

      // filtramos por oficina en caso de que no sea superuser
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        idOficinasAsociadas = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null) {
          resultado.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          resultado.ResultadoOperacion = false;
          resultado.Respuesta = [];

          return resultado;
        } else {
          //qbSolicitudes.andWhere('Solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
          esUsuarioAdmin = false;
        }
      } else {
        esUsuarioAdmin = true;
      }

      console.log(paginacionArgs);

      if (!filtro) {
        while (filtroBusqueda.search('/') != -1) {
          filtroBusqueda = filtroBusqueda.replace('/', '-');
        }

        tramitesClaseLicenciaPag = await paginate<TramitesClaseLicencia>(
          this.tramiteLicenciaRespository,
          paginacionArgs.paginationOptions,
          {
            relations: relacionTramiteConExamenes,
            where: qb => {
              qb /*.where('TramitesClaseLicencia__tramite__solicitudes__estadosSolicitud.Nombre IN(:...estadosSolicitudes)', {
                estadosSolicitudes: EstadosSolicitudesNoActivas,
              })*/.andWhere(
                new Brackets(subQb => {
                  subQb
                    .where(
                      'lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__estadosSolicitud.Nombre)) like lower(unaccent(:estado))',
                      { estado: `%${filtroBusqueda}%` }
                    )
                    .andWhere(
                      esUsuarioAdmin == false ? 'TramitesClaseLicencia__tramite__solicitudes.idOficina IN (:..._idsOficina)' : 'TRUE',
                      { _idsOficina: idOficinasAsociadas }
                    )
                    .orWhere(
                      "lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.Nombres)) || ' ' || lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoPaterno))  || ' ' || lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoMaterno)) like lower(unaccent(:nombre))",
                      { nombre: `%${filtroBusqueda}%` }
                    )
                    .orWhere('CAST(TramitesClaseLicencia__tramite.idSolicitud AS TEXT) like :id', { id: `%${filtroBusqueda}%` })
                    .orWhere(
                      "TramitesClaseLicencia__tramite__solicitudes__postulante.RUN || '-' || TramitesClaseLicencia__tramite__solicitudes__postulante.DV like :RUN",
                      { RUN: `%${filtroBusqueda}%` }
                    )
                    .orWhere('lower(unaccent(TramitesClaseLicencia__tramite__tiposTramite.Nombre)) like lower(unaccent(:tipoTramite))', {
                      tipoTramite: `%${filtroBusqueda}%`,
                    })
                    .orWhere('lower(unaccent(TramitesClaseLicencia__tramite__estadoTramite.Nombre)) like lower(unaccent(:estadoTramite))', {
                      estadoTramite: `%${filtroBusqueda}%`,
                    })
                    .orWhere('lower(unaccent(TramitesClaseLicencia__clasesLicencias.Abreviacion)) like lower(unaccent(:claseLicencia))', {
                      claseLicencia: `%${filtroBusqueda}%`,
                    })
                    .orWhere(`to_char(TramitesClaseLicencia__tramite__solicitudes.FechaCreacion, 'DD-MM-YYYY') like :fechaSolicitud`, {
                      fechaSolicitud: `%${filtroBusqueda}%`,
                    })
                    .orWhere(`to_char(TramitesClaseLicencia__tramite__solicitudes.updated_at, 'DD-MM-YYYY') like :fechaFinSolicitud`, {
                      fechaFinSolicitud: `%${filtroBusqueda}%`,
                    });
                })
              )

                .orderBy(orderBy, paginacionArgs.orden.ordenarPor);
            },
          }
        );
      } else {
        tramitesClaseLicenciaPag = await paginate<TramitesClaseLicencia>(
          this.tramiteLicenciaRespository,
          paginacionArgs.paginationOptions,
          {
            relations: relacionTramiteConExamenes,
            where: qb => {
              qb /*.where('TramitesClaseLicencia__tramite__solicitudes__estadosSolicitud.Nombre IN(:...estadosSolicitudes)', {
                estadosSolicitudes: EstadosSolicitudesNoActivas,
              })*/.andWhere(
                //
                esUsuarioAdmin == false ? 'TramitesClaseLicencia__tramite__solicitudes.idOficina IN (:..._idsOficina)' : 'TRUE',
                { _idsOficina: idOficinasAsociadas }
              )
                .andWhere(
                  filtro.estadoSolicitud != null && filtro.estadoSolicitud != ''
                    ? 'lower(TramitesClaseLicencia__tramite__solicitudes__estadosSolicitud.Nombre) = lower(:estado)'
                    : 'TRUE',
                  { estado: filtro.estadoSolicitud }
                )
                .andWhere(
                  filtro.nombrePostulante != null && filtro.nombrePostulante != ''
                    ? "lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.Nombres)) || ' ' || lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoPaterno))  || ' ' || lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoPaterno)) like unaccent(:nombre)"
                    : 'TRUE',
                  { nombre: `%${filtro.nombrePostulante}%` }
                )
                .andWhere(filtro.id > 0 ? 'TramitesClaseLicencia__tramite.idSolicitud = :id' : 'TRUE', { id: filtro.id })
                .andWhere(
                  filtro.run != null && filtro.run != ''
                    ? "TramitesClaseLicencia__tramite__solicitudes__postulante.RUN || '-' || TramitesClaseLicencia__tramite__solicitudes__postulante.DV = :RUN"
                    : 'TRUE',
                  { RUN: filtro.run }
                )
                .andWhere(
                  filtro.tipoTramite != null && filtro.tipoTramite != ''
                    ? 'TramitesClaseLicencia__tramite__tiposTramite.Nombre = :tipoTramite'
                    : 'TRUE',
                  { tipoTramite: filtro.tipoTramite }
                )
                .andWhere(
                  filtro.estadoTramite != null && filtro.estadoTramite != ''
                    ? 'TramitesClaseLicencia__tramite__estadoTramite.idEstadoTramite = :estadoTramite'
                    : 'TRUE',
                  { estadoTramite: filtro.estadoTramite }
                )
                .andWhere(
                  filtro.claseLicencia.length > 0 ? 'TramitesClaseLicencia__clasesLicencias.Abreviacion like :claseLicencia' : 'TRUE',
                  { claseLicencia: filtro.claseLicencia }
                )
                .andWhere(
                  filtro.fechaSolicitud != null
                    ? `DATE_TRUNC('day', TramitesClaseLicencia__tramite__solicitudes.FechaCreacion) = :fechaSolicitud`
                    : 'TRUE',
                  { fechaSolicitud: filtro.fechaSolicitud }
                )
                .andWhere(
                  filtro.fechaFinSolicitud != null
                    ? `DATE_TRUNC('day', TramitesClaseLicencia__tramite__solicitudes.updated_at) = :fechaFinSolicitud`
                    : 'TRUE',
                  { fechaFinSolicitud: filtro.fechaFinSolicitud }
                )
                .orderBy(orderBy, paginacionArgs.orden.ordenarPor);
            },
          }
        );
      }

      let solicitudesDTO: HistorialSolicitudDto[] = [];

      if (Array.isArray(tramitesClaseLicenciaPag.items) && tramitesClaseLicenciaPag.items.length) {
        tramitesClaseLicenciaPag.items.forEach(item => {
          const solicitudDTO: HistorialSolicitudDto = new HistorialSolicitudDto();
          solicitudDTO.idSolicitud = item.tramite.idSolicitud;
          solicitudDTO.idTramite = item.tramite.idTramite;
          solicitudDTO.run = item.tramite.solicitudes.postulante.RUN.toString() + '-' + item.tramite.solicitudes.postulante.DV;
          solicitudDTO.nombrePostulante =
            item.tramite.solicitudes.postulante.Nombres +
            ' ' +
            item.tramite.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            item.tramite.solicitudes.postulante.ApellidoMaterno;
          solicitudDTO.tipoTramite = item.tramite.tiposTramite.Nombre;
          solicitudDTO.estadoTramite = item.tramite.estadoTramite.Nombre;
          solicitudDTO.estadoSolicitud = item.tramite.solicitudes.estadosSolicitud.Nombre;
          solicitudDTO.fechaSolicitud = item.tramite.solicitudes.FechaCreacion;
          solicitudDTO.fechaFinSolicitud = item.tramite.solicitudes.FechaCierre;
          solicitudDTO.updated_at = item.tramite.solicitudes.updated_at;

          if (item.clasesLicencias) solicitudDTO.claseLicencia = item.clasesLicencias.Abreviacion;
          else solicitudDTO.claseLicencia = '';
          solicitudesDTO.push(solicitudDTO);
        });

        solicitudesParseadas.items = solicitudesDTO;
        solicitudesParseadas.meta = tramitesClaseLicenciaPag.meta;
        solicitudesParseadas.links = tramitesClaseLicenciaPag.links;

        resultado.Respuesta = solicitudesParseadas;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron solicitudes';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo solicitudes';
    }

    return resultado;
  }

  async getListaReprobadosByIdTipoexaminacion(
    RUN?: number,
    idSolicitud?: number,
    idTramite?: number,
    Nomclaselic?: string,
    idTipoExam?: number,
    idMunicipalidad?: number,
    idEstadoSol?: number,
    idEstadoTramite?: number
  ) {
    let vquery: string = '';
    let vRun: string = '';
    let vidSolicitud: string = '';
    let vidTramite: string = '';
    let vNomclaselic: string = '';
    let vidTipoExam: string = '';
    let vidMunicipalidad: string = '';
    let vidEstadoSol: string = '';
    let vidEstadoTramite: string = '';

    if (RUN) vRun = '("pos"."RUN" = ' + RUN + ')';
    if (idSolicitud) vidSolicitud = '("sol"."idSolicitud" = ' + idSolicitud + ')';
    if (idTramite) vidTramite = '("tra"."idTramite" = ' + idTramite + ')';
    if (Nomclaselic) vNomclaselic = '("clic"."Nombre" ILIKE \'%' + Nomclaselic + "%')";
    if (idTipoExam) vidTipoExam = '("cex"."idTipoExaminacion" = ' + idTipoExam + ')';
    if (idMunicipalidad) vidMunicipalidad = '("insti"."idInstitucion" = ' + idMunicipalidad + ')';
    if (idEstadoSol) vidEstadoSol = '("sol"."idEstado" = ' + idEstadoSol + ')';
    if (idEstadoTramite) vidEstadoTramite = '("tra"."idEstadoTramite" = ' + idEstadoTramite + ')';

    if (vidEstadoSol != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidEstadoSol;
      } else {
        vquery = vidEstadoSol;
      }
    }
    if (vidEstadoTramite != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidEstadoTramite;
      } else {
        vquery = vidEstadoTramite;
      }
    }
    if (vRun != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vRun;
      } else {
        vquery = vRun;
      }
    }
    if (vidSolicitud != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidSolicitud;
      } else {
        vquery = vidSolicitud;
      }
    }
    if (vidTramite != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidTramite;
      } else {
        vquery = vidTramite;
      }
    }
    if (vNomclaselic != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vNomclaselic;
      } else {
        vquery = vNomclaselic;
      }
    }
    if (vidTipoExam != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidTipoExam;
      } else {
        vquery = vidTipoExam;
      }
    }
    if (vidMunicipalidad != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidMunicipalidad;
      } else {
        vquery = vidMunicipalidad;
      }
    }

    return await getConnection()
      .createQueryBuilder(Solicitudes, 'sol')
      .select('sol.idSolicitud', 'SolidSolicitud')
      .addSelect('sol.idEstado', 'SolidEstado')
      .addSelect('esol.Nombre', 'EstadosSolNombre')
      .addSelect('cex.idColaExaminacion', 'idColaExaminacion')
      .addSelect('tex.nombreExaminacion', 'NombreTipoExaminacion') // 16/06/2021
      .addSelect('cex.idTipoExaminacion', 'idTipoExaminacion')
      .addSelect('cex.aprobado', 'ColaexamAprobado')
      .addSelect('cex.historico', 'ColaexamHistorico')
      .addSelect('esexa.nombreEstado', 'NombreEstadoExaminacion')
      .addSelect('tra.idTramite', 'idTramite')
      .addSelect('tra.idTipoTramite', 'idTipoTramite')
      .addSelect('tra.created_at', 'tramiteFechaCreated')
      .addSelect('titr.Nombre', 'NomTipoTramite')
      .addSelect('pos.RUN', 'RUN')
      .addSelect('pos.DV', 'DV')
      .addSelect('pos.idPostulante', 'idPostulante')
      .addSelect('pos.Nombres', 'PosNombres')
      .addSelect('pos.ApellidoPaterno', 'PosApellidoPaterno')
      .addSelect('pos.ApellidoMaterno', 'PosApellidoMaterno')
      .addSelect('pos.idOpcionSexo', 'PosidSexo')
      .addSelect('pos.idOpcionEstadosCivil', 'PosidEstadoCivil')
      .addSelect('pos.idOpNivelEducacional', 'PosidNivelEducacional')
      .addSelect('pos.Profesion', 'PosProfesion')
      .addSelect('pos.Email', 'PosEmail')
      .addSelect('pos.Telefono', 'PosTelefono')
      .addSelect('pos.Nacionalidad', 'PosNacionalidad')
      .addSelect('pos.FechaNacimiento', 'PosFechaNacimiento')
      .addSelect('clic.idClaseLicencia', 'idClaseLicencia')
      .addSelect('clic.Nombre', 'LicNombre')
      .addSelect('clic.Abreviacion', 'LicAbreviacion')
      .addSelect('tra.idEstadoTramite', 'idEstadoTramite')
      .addSelect('et.Nombre', 'EstadoTramNombre')
      .addSelect('sol.idInstitucion', 'idInstitucion')
      .addSelect('insti.Nombre', 'InstNombre')

      .innerJoin(EstadosSolicitudEntity, 'esol', 'esol.idEstado = sol.idEstado')
      .innerJoin(TramitesEntity, 'tra', 'tra.idSolicitud = sol.idSolicitud')
      .innerJoin(ColaExaminacionEntity, 'cex', 'cex.idTramite = tra.idTramite')
      .innerJoin(EstadosTramiteEntity, 'et', 'tra.idEstadoTramite = et.idEstadoTramite')
      .innerJoin(Postulantes, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoin(InstitucionesEntity, 'insti', 'sol.idInstitucion = insti.idInstitucion')
      .innerJoin(TramitesClaseLicencia, 'tcla', 'tra.idTramite = tcla.idTramite')
      .innerJoin(ClasesLicenciasEntity, 'clic', 'tcla.idClaseLicencia = clic.idClaseLicencia')
      .innerJoin(TiposTramiteEntity, 'titr', 'tra.idTipoTramite = titr.idTipoTramite')
      .innerJoin(TipoExaminacionesEntity, 'tex', 'cex.idTipoExaminacion = tex.idTipoExaminacion')
      .leftJoin(apelacionesEntity, 'apel', 'cex.idcolaexaminacion = apel.idcolaexaminacion')
      .innerJoin(
        estadosExaminacionEntity,
        'esexa',
        'cex.idTipoExaminacion = esexa.idTipoExaminacion and cex.idEstadosExaminacion = esexa.idEstadosExaminacion'
      )

      .andWhere(vquery)

      .getRawMany();
  }

  async getListaTramitesEnDenegacionInformadaCrJPLSML() {
    const idOficina = this.authService.oficina().idOficina;
    const vidEstadoTramite = '("tra"."idEstadoTramite" = ' + 12 + ' or ' + '"tra"."idEstadoTramite" = ' + 13 + ')';

    return await getConnection()
      .createQueryBuilder(Solicitudes, 'sol')
      .select('sol.idSolicitud', 'SolidSolicitud')
      .addSelect('sol.idEstado', 'SolidEstado')
      .addSelect('esol.Nombre', 'EstadosSolNombre')
      .addSelect('cex.idColaExaminacion', 'idColaExaminacion')
      .addSelect('tex.nombreExaminacion', 'NombreTipoExaminacion') // 16/06/2021
      .addSelect('cex.idTipoExaminacion', 'idTipoExaminacion')
      .addSelect('cex.aprobado', 'ColaexamAprobado')
      .addSelect('cex.historico', 'ColaexamHistorico')
      .addSelect('esexa.nombreEstado', 'NombreEstadoExaminacion')
      .addSelect('tra.idTramite', 'idTramite')
      .addSelect('tra.idTipoTramite', 'idTipoTramite')
      .addSelect('tra.created_at', 'tramiteFechaCreated')
      .addSelect('titr.Nombre', 'NomTipoTramite')
      .addSelect('pos.RUN', 'RUN')
      .addSelect('pos.DV', 'DV')
      .addSelect('pos.idPostulante', 'idPostulante')
      .addSelect('pos.Nombres', 'PosNombres')
      .addSelect('pos.ApellidoPaterno', 'PosApellidoPaterno')
      .addSelect('pos.ApellidoMaterno', 'PosApellidoMaterno')
      .addSelect('pos.idOpcionSexo', 'PosidSexo')
      .addSelect('pos.idOpcionEstadosCivil', 'PosidEstadoCivil')
      .addSelect('pos.idOpNivelEducacional', 'PosidNivelEducacional')
      .addSelect('pos.Profesion', 'PosProfesion')
      .addSelect('pos.Email', 'PosEmail')
      .addSelect('pos.Telefono', 'PosTelefono')
      .addSelect('pos.Nacionalidad', 'PosNacionalidad')
      .addSelect('pos.FechaNacimiento', 'PosFechaNacimiento')
      .addSelect('clic.idClaseLicencia', 'idClaseLicencia')
      .addSelect('clic.Nombre', 'LicNombre')
      .addSelect('clic.Abreviacion', 'LicAbreviacion')
      .addSelect('tra.idEstadoTramite', 'idEstadoTramite')
      .addSelect('et.Nombre', 'EstadoTramNombre')
      .addSelect('sol.idInstitucion', 'idInstitucion')
      .addSelect('insti.Nombre', 'InstNombre')

      .innerJoin(EstadosSolicitudEntity, 'esol', 'esol.idEstado = sol.idEstado')
      .innerJoin(TramitesEntity, 'tra', 'tra.idSolicitud = sol.idSolicitud')
      .innerJoin(ColaExaminacionEntity, 'cex', 'cex.idTramite = tra.idTramite')
      .innerJoin(EstadosTramiteEntity, 'et', 'tra.idEstadoTramite = et.idEstadoTramite')
      .innerJoin(Postulantes, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoin(InstitucionesEntity, 'insti', 'sol.idInstitucion = insti.idInstitucion')
      .innerJoin(TramitesClaseLicencia, 'tcla', 'tra.idTramite = tcla.idTramite')
      .innerJoin(ClasesLicenciasEntity, 'clic', 'tcla.idClaseLicencia = clic.idClaseLicencia')
      .innerJoin(TiposTramiteEntity, 'titr', 'tra.idTipoTramite = titr.idTipoTramite')
      .innerJoin(TipoExaminacionesEntity, 'tex', 'cex.idTipoExaminacion = tex.idTipoExaminacion')
      .leftJoin(apelacionesEntity, 'apel', 'cex.idColaExaminacion = apel.idcolaexaminacion')
      .innerJoin(
        estadosExaminacionEntity,
        'esexa',
        'cex.idTipoExaminacion = esexa.idTipoExaminacion and cex.idEstadosExaminacion = esexa.idEstadosExaminacion'
      )

      .where('sol.idOficina = :idOficina', { idOficina })
      .andWhere(vidEstadoTramite)

      .getRawMany();
  }

  async getListaDocsReprobadosByIdTipoexaminacion(
    RUN?: number,
    idSolicitud?: number,
    idTramite?: number,
    Nomclaselic?: string,
    idTipoExam?: number,
    idMunicipalidad?: number,
    idEstadoSol?: number,
    idEstadoTramite?: number
  ) {
    let vquery: string = '';
    let vRun: string = '';
    let vidSolicitud: string = '';
    let vidTramite: string = '';
    let vNomclaselic: string = '';
    let vidTipoExam: string = '';
    let vidMunicipalidad: string = '';
    let vidEstadoSol: string = '';
    let vidEstadoTramite: string = '';

    if (RUN) vRun = '("pos"."RUN" = ' + RUN + ')';
    if (idSolicitud) vidSolicitud = '("sol"."idSolicitud" = ' + idSolicitud + ')';
    if (idTramite) vidTramite = '("tra"."idTramite" = ' + idTramite + ')';
    if (Nomclaselic) vNomclaselic = '("clic"."Nombre" ILIKE \'%' + Nomclaselic + "%')";
    if (idTipoExam) vidTipoExam = '("cex"."idTipoExaminacion" = ' + idTipoExam + ')';
    if (idMunicipalidad) vidMunicipalidad = '("insti"."idInstitucion" = ' + idMunicipalidad + ')';
    if (idEstadoSol) vidEstadoSol = '("sol"."idEstado" = ' + idEstadoSol + ')';
    if (idEstadoTramite) vidEstadoTramite = '("tra"."idEstadoTramite" = ' + idEstadoTramite + ')';

    if (vidEstadoSol != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidEstadoSol;
      } else {
        vquery = vidEstadoSol;
      }
    }
    if (vidEstadoTramite != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidEstadoTramite;
      } else {
        vquery = vidEstadoTramite;
      }
    }
    if (vRun != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vRun;
      } else {
        vquery = vRun;
      }
    }
    if (vidSolicitud != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidSolicitud;
      } else {
        vquery = vidSolicitud;
      }
    }
    if (vidTramite != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidTramite;
      } else {
        vquery = vidTramite;
      }
    }
    if (vNomclaselic != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vNomclaselic;
      } else {
        vquery = vNomclaselic;
      }
    }
    if (vidTipoExam != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidTipoExam;
      } else {
        vquery = vidTipoExam;
      }
    }
    if (vidMunicipalidad != '') {
      if (vquery != '') {
        vquery = vquery + ' AND ' + vidMunicipalidad;
      } else {
        vquery = vidMunicipalidad;
      }
    }

    return await getConnection()
      .createQueryBuilder(Solicitudes, 'sol')
      .select('sol.idSolicitud', 'SolidSolicitud')
      .addSelect('sol.idEstado', 'SolidEstado')
      .addSelect('esol.Nombre', 'EstadosSolNombre')
      .addSelect('cex.idColaExaminacion', 'idColaExaminacion')
      .addSelect('apel.observacion', 'ApelObservacion')
      .addSelect('docsapel.nombrearchivo', 'DocNombre')
      .addSelect('docsapel.binary', 'DocArchivo')

      .innerJoin(EstadosSolicitudEntity, 'esol', 'esol.idEstado = sol.idEstado')
      .innerJoin(TramitesEntity, 'tra', 'tra.idSolicitud = sol.idSolicitud')
      .innerJoin(ColaExaminacionEntity, 'cex', 'cex.idTramite = tra.idTramite')
      .innerJoin(EstadosTramiteEntity, 'et', 'tra.idEstadoTramite = et.idEstadoTramite')
      .innerJoin(Postulantes, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoin(InstitucionesEntity, 'insti', 'sol.idInstitucion = insti.idInstitucion')
      .innerJoin(TramitesClaseLicencia, 'tcla', 'tra.idTramite = tcla.idTramite')
      .innerJoin(ClasesLicenciasEntity, 'clic', 'tcla.idClaseLicencia = clic.idClaseLicencia')
      .innerJoin(TiposTramiteEntity, 'titr', 'tra.idTipoTramite = titr.idTipoTramite')
      .innerJoin(TipoExaminacionesEntity, 'tex', 'cex.idTipoExaminacion = tex.idTipoExaminacion')
      .innerJoin(apelacionesEntity, 'apel', 'cex.idcolaexaminacion = apel.idcolaexaminacion')
      .innerJoin(docsapelacionesEntity, 'docsapel', 'apel.idapelaciones = docsapel.idapelaciones')

      .andWhere(vquery)

      .getRawMany();
  }

  async getSolicitudesByEstadoFechavencimiento(idEstadoSol?: number, fechadesde?: Date, idEstadoTramite?: string) {
    const idOficina = this.authService.oficina().idOficina;

    let vfechahasta = new Date(fechadesde);
    let i = 1;
    let diasPactados = 25;

    const feriados = [[1], [15], [5], [24], [1], [10, 25], [9], [17, 23], [18], [15], [2], [25]]; //matriz de feriados por meses
    let diaPropuesto = new Date(vfechahasta.getFullYear(), vfechahasta.getMonth(), vfechahasta.getDate());
    while (diasPactados > 0) {
      let feriado = false;
      diaPropuesto = new Date(vfechahasta.getFullYear(), vfechahasta.getMonth(), vfechahasta.getDate() + i);
      if (diaPropuesto.getDay() > 0 && diaPropuesto.getDay() < 6) {
        let m = diaPropuesto.getMonth();
        let dia = diaPropuesto.getDate();
        for (let d in feriados[m]) {
          if (dia === feriados[m][d]) {
            feriado = true;
            break;
          }
        } //fin for

        if (!feriado) {
          diasPactados--;
        }
      }
      i++;
    } //fin while
    console.log(`Dia de vencimento: ${diaPropuesto}`);

    const qb = this.solicitudRespository
      .createQueryBuilder('Solicitudes')
      .innerJoinAndMapMany('Solicitudes.idSolicitud', TramitesEntity, 'tra', 'Solicitudes.idSolicitud = tra.idSolicitud')
      .innerJoinAndMapMany('tra.idEstadoTramite', EstadosTramiteEntity, 'et', 'tra.idEstadoTramite = et.idEstadoTramite')
      .innerJoinAndMapMany('tra.idTramite', TramitesClaseLicencia, 'tcla', 'tra.idTramite = tcla.idTramite')
      .innerJoinAndMapMany('tcla.idClaseLicencia', ClasesLicenciasEntity, 'clic', 'tcla.idClaseLicencia = clic.idClaseLicencia')
      .innerJoinAndMapMany('Solicitudes.idPostulante', PostulantesEntity, 'pos', 'Solicitudes.idPostulante = pos.idPostulante')
      .innerJoinAndMapMany('tra.idTramite', ColaExaminacionEntity, 'cexa', 'tra.idTramite = cexa.idTramite');

    qb.where('Solicitudes.idOficina = :idOficina', { idOficina });

    if (idEstadoSol) qb.andWhere('Solicitudes.idEstado = :idEstadoSol', { idEstadoSol: idEstadoSol });

    if (fechadesde) qb.andWhere('Solicitudes.fechaVencimiento <= :diaPropuesto', { diaPropuesto: diaPropuesto });

    if (idEstadoTramite) qb.andWhere('tra.idEstadoTramite in :idEstadoTramite', { idEstadoTramite: idEstadoTramite });

    return await qb.getMany();
  }

  async UpdateEstadoExaminacionByIdExaminacion(dto: UpdateEstadoExaminacionByIdExaminacionDto) {
    const idColaExaminacion = dto.idColaExaminacion;
    const aprobado = dto.aprobado;
    const idTipoExaminacion = dto.idTipoExaminacion;
    const idUsuario = (await this.authService.usuario()).idUsuario;
    const nombrearchivo = dto.nombrearchivo;
    const binary = dto.binary;
    const observacion = dto.observacion;

    try {
      const colaexaminacion = await getConnection()
        .createQueryBuilder()
        .select('ColaExaminacion')
        .from(ColaExaminacionEntity, 'ColaExaminacion')
        .where('ColaExaminacion.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
        .andWhere('ColaExaminacion.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: idTipoExaminacion })
        .getOne();
      if (!colaexaminacion) throw new NotFoundException("ColaExaminacion dont' exist");

      const idTramite = colaexaminacion.idTramite;

      const tramite = await getConnection()
        .createQueryBuilder()
        .select('Tramites')
        .from(TramitesEntity, 'Tramites')
        .where('Tramites.idTramite = :idTramite', { idTramite: idTramite })
        .getOne();
      if (!tramite) throw new NotFoundException("Tramites dont' exist");

      const idSolicitud = tramite.idSolicitud;

      await this.colaExaminacionRepository
        .createQueryBuilder()
        .update(ColaExaminacionEntity)
        .set({ aprobado: aprobado })
        .where('"ColaExaminacion"."idColaExaminacion" = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
        .andWhere('"ColaExaminacion"."idTipoExaminacion" = :idTipoExaminacion', { idTipoExaminacion: idTipoExaminacion })
        .execute();

      await this.tramiteRespository
        .createQueryBuilder()
        .update(TramitesEntity)
        .set({ idEstadoTramite: 14 })
        .where('"Tramites"."idTramite" = :idTramite', { idTramite: idTramite })
        .execute();

      await this.solicitudRespository
        .createQueryBuilder()
        .update(Solicitudes)
        .set({ idEstado: 7 })
        .where('"Solicitudes"."idSolicitud" = :idSolicitud', { idSolicitud: idSolicitud })
        .execute();

      let fechahoy: Date = new Date();

      const apelacion = await getConnection()
        .createQueryBuilder()
        .select('apelacion')
        .from(apelacionesEntity, 'apelacion')
        .where('apelacion.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
        .getOne();

      if (!apelacion) {
        const Apel = new CreateApelacionesDto();
        Apel.idcolaexaminacion = idColaExaminacion;
        Apel.fechaingreso = fechahoy;
        Apel.observacion = observacion;
        Apel.ingresadapor = idUsuario;
        Apel.apelacionaprobada = true;
        Apel.fechaaprobacion = fechahoy;
        Apel.aprobadapor = idUsuario;
        const ap = await this.apelacionesRepository.save(Apel);
        console.log(ap);

        const apelacion2 = await getConnection()
          .createQueryBuilder()
          .select('apelaciones')
          .from(apelacionesEntity, 'apelaciones')
          .where('apelaciones.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
          .getOne();
        const idapelacion2 = apelacion2.idapelaciones;

        // if (!nombrearchivo){
        //   for (let k in nombrearchivo ){
        //       const DocApel = new docsapelacionesEntity();
        //       DocApel.created_at = fechahoy;
        //       DocApel.idapelaciones = idapelacion2;
        //       DocApel.binary = binary;
        //       DocApel.nombrearchivo = nombrearchivo[k] ;
        //       const f = await this.docapelRepository.save(DocApel);
        //       console.log(f);
        //   }
        // }
      } else {
        const idApelaciones = apelacion.idapelaciones;

        await this.apelacionesRepository
          .createQueryBuilder()
          .update(apelacionesEntity)
          .set({ apelacionaprobada: true, observacion: observacion, fechaaprobacion: fechahoy, aprobadapor: idUsuario })
          .where('"apelaciones"."idcolaexaminacion" = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
          .execute();

        // for (let k in nombrearchivo ){
        //       const DocApel = new docsapelacionesEntity();
        //       DocApel.created_at = fechahoy;
        //       DocApel.idapelaciones = idApelaciones;
        //       DocApel.binary = binary;
        //       DocApel.nombrearchivo = nombrearchivo[k] ;
        //       const d = await this.docapelRepository.save(DocApel);
        //       console.log(d);
        // }
      }

      return { code: 200, message: 'success' };
    } catch (error) {
      return { code: 400, error: error };
    }
  }

  // Hay que consultar si la FechaProrroga pega en fechaVencimiento de la solicitud
  async updateFechaVtoByidSolicitud(idSolicitud: number, idEstado: number) {
    const solicitud = await getConnection()
      .createQueryBuilder()
      .select('Solicitudes')
      .from(Solicitudes, 'Solicitudes')
      .where('Solicitudes.idSolicitud = :idSolicitud', { idSolicitud: idSolicitud })
      .getOne();
    if (!solicitud) throw new NotFoundException("Solicitud dont' exist");

    await this.solicitudRespository
      .createQueryBuilder()
      .update(Solicitudes)
      .set({ idEstado: idEstado })
      .where('"Solicitudes"."idSolicitud" = :idSolicitud', { idSolicitud: idSolicitud })
      .execute();

    return { code: 200, message: 'success' };
  }

  async UpdateEstadoSolicitudByIdSolicitud(
    idSolicitud: number,
    idEstado: number,
    idTramite: number[],
    idNuevoEstadoTramite: number[],
    idTipoExaminacion: number,
    aprobado: boolean,
    idUsuario: number,
    nombrearchivo: string[],
    observacion: string
  ) {
    if (aprobado) {
      try {
        let fechahoy: Date = new Date();

        await this.solicitudRespository
          .createQueryBuilder()
          .update(Solicitudes)
          .set({ idEstado: idEstado, updated_at: fechahoy })
          .where('"Solicitudes"."idSolicitud" = :idSolicitud', { idSolicitud: idSolicitud })
          .execute();

        for (let i in idTramite) {
          await this.tramiteRespository
            .createQueryBuilder()
            .update(TramitesEntity)
            .set({ idEstadoTramite: idNuevoEstadoTramite[i], update_at: fechahoy })
            .where('"Tramites"."idTramite" = :idTramite', { idTramite: idTramite[i] })
            .andWhere('"Tramites"."idSolicitud" = :idSolicitud', { idSolicitud: idSolicitud })
            .execute();
        }

        const colaexaminacion = await getConnection()
          .createQueryBuilder()
          .select('ColaExaminacion')
          .from(ColaExaminacionEntity, 'ColaExaminacion')
          .where('ColaExaminacion.idTramite = :idTramite', { idTramite: idTramite })
          .andWhere('ColaExaminacion.idTipoExaminacion = :idTipoExaminacion', { idTipoExaminacion: idTipoExaminacion })
          .getOne();
        if (!colaexaminacion) throw new NotFoundException("ColaExaminacion dont' exist");

        const idColaExaminacion = colaexaminacion.idColaExaminacion;

        const apelacion = await getConnection()
          .createQueryBuilder()
          .select('apelaciones')
          .from(apelacionesEntity, 'apelaciones')
          .where('apelaciones.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
          .getOne();
        const idapelacion = apelacion.idapelaciones;

        if (!apelacion) {
          const Apel = new CreateApelacionesDto();
          Apel.idcolaexaminacion = idColaExaminacion;
          Apel.fechaingreso = fechahoy;
          Apel.observacion = observacion;
          Apel.ingresadapor = idUsuario;
          Apel.apelacionaprobada = true;
          Apel.fechaaprobacion = fechahoy;
          Apel.aprobadapor = idUsuario;
          const ap = await this.apelacionesRepository.save(Apel);
          console.log(ap);

          const apelacion2 = await getConnection()
            .createQueryBuilder()
            .select('apelaciones')
            .from(apelacionesEntity, 'apelaciones')
            .where('apelaciones.idColaExaminacion = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
            .getOne();
          const idapelacion2 = apelacion2.idapelaciones;

          for (let k in nombrearchivo) {
            const DocApel = new CreateDocsApelacionesDto();
            DocApel.created_at = fechahoy;
            DocApel.idapelaciones = idapelacion2;
            DocApel.binary.toString();
            DocApel.nombrearchivo = nombrearchivo[k];
            const f = await this.docapelRepository.save(DocApel);
            console.log(f);
          }
        } else {
          await this.apelacionesRepository
            .createQueryBuilder()
            .update(apelacionesEntity)
            .set({ apelacionaprobada: true, observacion: observacion, fechaaprobacion: fechahoy, aprobadapor: idUsuario })
            .where('"apelaciones"."idcolaexaminacion" = :idColaExaminacion', { idColaExaminacion: idColaExaminacion })
            .execute();

          for (let k in nombrearchivo) {
            const DocApel = new CreateDocsApelacionesDto();
            DocApel.created_at = fechahoy;
            DocApel.idapelaciones = idapelacion;
            DocApel.binary.toString();
            DocApel.nombrearchivo = nombrearchivo[k];
            const d = await this.docapelRepository.save(DocApel);
            console.log(d);
          }
        } // if !apelacion

        return { code: 200, message: 'success' };
      } catch (error) {
        return { code: 400, error: error };
      }
    } else {
      console.log('WS de Registro civil a devuelto NO-OK');
    } // if aprobado
  } // async

  public async getEstadosTramites() {
    return this.serviciosComunesService.getEstadosTramites();
  }
}
