import { SharedModule } from 'src/shared/shared.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Solicitudes } from './entity/solicitudes.entity';
import { SolicitudesService } from './services/solicitudes.service';
import { SolicitudesController } from './controller/solicitudes.controller';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { RegistroPago } from 'src/registro-pago/entity/registro.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosSolicitudEntity } from 'src/tramites/entity/estados-solicitud.entity';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';
import { ColaExaminacionEntity } from '../cola-examinacion/entity/cola-examinacion.entity';
import { TipoExaminacionesEntity } from 'src/cola-examinacion/entity/tipoexaminacion.entity';
import { apelacionesEntity } from './entity/apelaciones.entity';
import { docsapelacionesEntity } from './entity/docsapelaciones.entity';
import { CambiosEstadoService } from 'src/cambios-estado/services/cambios-estado.service';
import { RegistroCambiosEstadoSolicitudEntity } from 'src/cambios-estado/entities/registro-cambios-estado-solicitud.entity';
import { RegistroCambiosEstadoTramiteEntity } from 'src/cambios-estado/entities/registro-cambios-estado-tramite.entity';
import { DocRespaldoCambioEstadoEntity } from 'src/cambios-estado/entities/doc-respaldo-cambio-estado.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { MatrizEstadosSolicitudEntity } from 'src/cambios-estado/entities/matriz-estados-solicitud.entity';
import { MatrizEstadosTramiteEntity } from 'src/cambios-estado/entities/matriz-estados-tramite.entity';
import { MatrizEstadosExaminacionEntity } from 'src/cambios-estado/entities/matriz-estados-examinacion.entity';
import { EstadosExaminacionEntity } from 'src/tramites/entity/estados-examinacion.entity';

import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';

@Module({
  imports: [
    SharedModule,
    TypeOrmModule.forFeature([
      Solicitudes,
      TramitesEntity,
      TramitesClaseLicencia,
      EstadosSolicitudEntity,
      RegistroPago,
      User,
      DocsRolesUsuarioEntity,
      RolesUsuarios,
      ColaExaminacionEntity,
      TipoExaminacionesEntity,
      apelacionesEntity,
      docsapelacionesEntity,
      RegistroCambiosEstadoSolicitudEntity,
      RegistroCambiosEstadoTramiteEntity,
      DocRespaldoCambioEstadoEntity,
      EstadosTramiteEntity,
      MatrizEstadosSolicitudEntity,
      MatrizEstadosTramiteEntity,
      MatrizEstadosExaminacionEntity,
      EstadosExaminacionEntity,
      ConductoresEntity,
      ComunasEntity,
      Postulantes,
    ]),
  ],
  providers: [SolicitudesService, AuthService, CambiosEstadoService, IngresoPostulanteService],
  exports: [SolicitudesService],
  controllers: [SolicitudesController],
})
export class SolicitudesModule {}
