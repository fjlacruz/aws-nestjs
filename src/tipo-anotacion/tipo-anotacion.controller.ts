import { Body, Controller, Delete, Get, Param, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { Page, PageRequest } from '../tipo-anotacion/pagination-tipo-anotacion.entity';
import { HeaderUtil } from '../base/base/header-util';
import { AuthGuard } from '@nestjs/passport';
import { Post as PostMethod } from '@nestjs/common/decorators/http/request-mapping.decorator';
import { TipoAnotacionService } from './tipo-anotacion.service';
import { TipoAnotacionEntity } from './tipo-anotacion.entity';

@ApiTags('Tipos de Anotaciones')
@Controller('tipos-anotaciones')
export class TipoAnotacionController {
  constructor(private readonly tipoAnotacionService: TipoAnotacionService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all records',
    type: TipoAnotacionEntity,
  })
  async getAll(@Req() req: Request): Promise<TipoAnotacionEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.tipoAnotacionService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una Sentencia por Id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: TipoAnotacionEntity,
  })
  async getOne(@Param('id') id: string): Promise<TipoAnotacionEntity> {
    return await this.tipoAnotacionService.findById(id);
  }

  @PostMethod('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea una nueva Sentencia' })
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    type: TipoAnotacionEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() tipoAnotacionEntity: TipoAnotacionEntity): Promise<TipoAnotacionEntity> {
    const created = await this.tipoAnotacionService.save(tipoAnotacionEntity);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: TipoAnotacionEntity,
  })
  async put(@Req() req: Request, @Body() tipoAnotacionEntity: TipoAnotacionEntity): Promise<TipoAnotacionEntity> {
    return await this.tipoAnotacionService.update(tipoAnotacionEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: TipoAnotacionEntity,
  })
  async putId(@Req() req: Request, @Body() tipoAnotacionEntity: TipoAnotacionEntity): Promise<TipoAnotacionEntity> {
    return await this.tipoAnotacionService.update(tipoAnotacionEntity);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Elimina una Sentencia' })
  @ApiResponse({
    status: 204,
    description: 'The record has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
    return await this.tipoAnotacionService.deleteById(id);
  }
}
