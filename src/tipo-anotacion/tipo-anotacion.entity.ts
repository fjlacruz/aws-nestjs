import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

@Entity('TipoAnotaciones')
export class TipoAnotacionEntity {
  @PrimaryGeneratedColumn()
  idTipoAnotaciones: number;
  @Column()
  @ApiModelProperty()
  nombre: string;
  @Column()
  @ApiModelProperty()
  descripcion: string;
}
