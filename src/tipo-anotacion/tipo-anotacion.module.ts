import { Module } from '@nestjs/common';
import { TipoAnotacionController } from './tipo-anotacion.controller';
import { TipoAnotacionService } from './tipo-anotacion.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoAnotacionEntity } from './tipo-anotacion.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TipoAnotacionEntity])],
  controllers: [TipoAnotacionController],
  providers: [TipoAnotacionService],
  exports: [TipoAnotacionService],
})
export class TipoAnotacionModule {}
