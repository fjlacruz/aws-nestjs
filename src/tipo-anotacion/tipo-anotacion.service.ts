import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { TipoAnotacionEntity } from './tipo-anotacion.entity';

const relationshipNames = [];

@Injectable()
export class TipoAnotacionService {
  constructor(@InjectRepository(TipoAnotacionEntity) private readonly tipoAnotacionEntityRepository: Repository<TipoAnotacionEntity>) {}

  async findById(id: string): Promise<TipoAnotacionEntity | undefined> {
    const options = { relations: relationshipNames };
    const result = await this.tipoAnotacionEntityRepository.findOne(id, options);
    return result;
  }

  async findByfields(options: FindOneOptions<TipoAnotacionEntity>): Promise<TipoAnotacionEntity | undefined> {
    const result = await this.tipoAnotacionEntityRepository.findOne(options);
    return result;
  }

  async findAndCount(options: FindManyOptions<TipoAnotacionEntity>): Promise<[TipoAnotacionEntity[], number]> {
    options.relations = relationshipNames;
    const resultList = await this.tipoAnotacionEntityRepository.findAndCount(options);
    const tipoAnotacionEntity: TipoAnotacionEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(tipoAnotacion => tipoAnotacionEntity.push(tipoAnotacion));
      resultList[0] = tipoAnotacionEntity;
    }
    return resultList;
  }

  async save(sentenciaEntity: TipoAnotacionEntity): Promise<TipoAnotacionEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.tipoAnotacionEntityRepository.save(entity);
    return result;
  }

  async update(sentenciaEntity: TipoAnotacionEntity): Promise<TipoAnotacionEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.tipoAnotacionEntityRepository.save(entity);
    return result;
  }

  async deleteById(id: string): Promise<void | undefined> {
    await this.tipoAnotacionEntityRepository.delete(id);
    const entityFind = await this.findById(id);
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
