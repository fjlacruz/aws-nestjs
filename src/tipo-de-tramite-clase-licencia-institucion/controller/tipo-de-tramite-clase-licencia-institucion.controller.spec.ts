import { Test, TestingModule } from '@nestjs/testing';
import { TipoDeTramiteClaseLicenciaInstitucionController } from './tipo-de-tramite-clase-licencia-institucion.controller';

describe('TipoDeTramiteClaseLicenciaInstitucionController', () => {
  let controller: TipoDeTramiteClaseLicenciaInstitucionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TipoDeTramiteClaseLicenciaInstitucionController],
    }).compile();

    controller = module.get<TipoDeTramiteClaseLicenciaInstitucionController>(TipoDeTramiteClaseLicenciaInstitucionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
