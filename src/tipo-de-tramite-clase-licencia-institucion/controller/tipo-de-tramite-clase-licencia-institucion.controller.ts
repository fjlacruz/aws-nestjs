import { Controller, Get, Param, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { TipoDeTramiteClaseLicenciaInstitucionService } from '../service/tipo-de-tramite-clase-licencia-institucion.service';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Tipo-de-Tramite-Clase-Licencia-Institucion')
@Controller('tipo-de-tramite-clase-licencia-institucion')
export class TipoDeTramiteClaseLicenciaInstitucionController {
  constructor(private readonly tipTraClaLicInsService: TipoDeTramiteClaseLicenciaInstitucionService) {}

  @Get('tipo-tramite/:idOficina')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos los tipos de tramite para una municipalidad' })
  async getTipoDeTramites(@Param('idOficina') idOficina: number) {
    return this.tipTraClaLicInsService.getTipoDeTramites(Number(idOficina)).then(data => {
      return { code: 200, data };
    });
  }
  @Get('clase-licencia/:idOficina/:idTipoTramite')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos las clases de licencia para una municipalidad / tipo de tramite' })
  async getClaseDeLicencias(@Param('idOficina') idOficina: number, @Param('idTipoTramite') idTipoTramite: number) {
    return this.tipTraClaLicInsService.getClaseDeLicencias(Number(idOficina), Number(idTipoTramite)).then(data => {
      return { code: 200, data };
    });
  }
}
