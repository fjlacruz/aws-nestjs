import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber } from "class-validator";

export class TipoDeTramiteClaseLicenciaInstitucionDto {

    @ApiPropertyOptional()
    idTTramiteClaseLicenciaInstitucion: number;

    @ApiPropertyOptional()
    idInstitucion: number;
    
    @ApiPropertyOptional()
    idTipoTramite: number;
    
    @ApiPropertyOptional()
    @IsNotEmpty()
    @IsNumber()
    idClaseLicencia: number;
    
    @ApiPropertyOptional()
    idOficina: number;
}
