import { ClasesLicenciasEntity } from "src/clases-licencia/entity/clases-licencias.entity";
import { TiposTramiteEntity } from "src/tipos-tramites/entity/tipos-tramite.entity";
import { InstitucionesEntity } from "src/tramites/entity/instituciones.entity";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import {Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToMany, ManyToOne } from "typeorm";

@Entity('TipodeTramiteClaseLicenciaInstitucion')
export class TipoDeTramiteClaseLicenciaInstitucionEntity 
{
    @PrimaryGeneratedColumn()
    idTTramiteClaseLicenciaInstitucion: number;

    @Column()
    idInstitucion: number;
    @JoinColumn({ name: 'idInstitucion' })
    @ManyToOne(() => InstitucionesEntity, instituciones => instituciones.tipoTramiteLicenciaInstitucion)
    readonly instituciones: InstitucionesEntity;

    @Column()
    idTipoTramite: number;
    @JoinColumn({ name: 'idTipoTramite' })
    @ManyToOne(() => TiposTramiteEntity, tiposTramite => tiposTramite.tipoTramiteLicenciaInstitucion)
    readonly tiposTramite: TiposTramiteEntity;

    @Column()
    idClaseLicencia: number;
    @JoinColumn({ name: 'idClaseLicencia' })
    @ManyToOne(() => ClasesLicenciasEntity, clasesLicencias => clasesLicencias.tipoTramiteLicenciaInstitucion)
    readonly clasesLicencias: ClasesLicenciasEntity;

    @Column()
    idOficina: number;
    @JoinColumn({ name: 'idOficina' })
    @ManyToOne(() => OficinasEntity, oficinas => oficinas.tipoTramiteLicenciaInstitucion)
    readonly oficinas: OficinasEntity;
}
