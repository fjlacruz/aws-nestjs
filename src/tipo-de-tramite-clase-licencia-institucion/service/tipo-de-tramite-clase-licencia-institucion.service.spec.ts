import { Test, TestingModule } from '@nestjs/testing';
import { TipoDeTramiteClaseLicenciaInstitucionService } from './tipo-de-tramite-clase-licencia-institucion.service';

describe('TipoDeTramiteClaseLicenciaInstitucionService', () => {
  let service: TipoDeTramiteClaseLicenciaInstitucionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TipoDeTramiteClaseLicenciaInstitucionService],
    }).compile();

    service = module.get<TipoDeTramiteClaseLicenciaInstitucionService>(TipoDeTramiteClaseLicenciaInstitucionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
