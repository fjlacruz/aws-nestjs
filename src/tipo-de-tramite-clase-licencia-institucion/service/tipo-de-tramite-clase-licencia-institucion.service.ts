import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { getConnection, Repository } from 'typeorm';
import { TipoDeTramiteClaseLicenciaInstitucionEntity } from '../entity/tipo-de-tramte-clase-licencia-institucion.entity';

@Injectable()
export class TipoDeTramiteClaseLicenciaInstitucionService
{
    constructor
    (
        @InjectRepository( TiposTramiteEntity )   
        private readonly tipoTramiteRepository:  Repository<TiposTramiteEntity>,        

        @InjectRepository( ClasesLicenciasEntity )   
        private readonly claseLicenciaRepository:  Repository<ClasesLicenciasEntity>,        

    )
    {}
    async getTipoDeTramites( idOficina: number )
    {
        return await this.tipoTramiteRepository.createQueryBuilder( "tipo" )
            .innerJoin( TipoDeTramiteClaseLicenciaInstitucionEntity, "tipTraClaLicIns", "tipo.idTipoTramite = tipTraClaLicIns.idTipoTramite" )
            .where(     "tipTraClaLicIns.idOficina = :idOficina", { idOficina })
            .groupBy(   "tipo.idTipoTramite" )
            .getMany();
    }
    async getClaseDeLicencias( idOficina: number, idTipoTramite: number )
    {
        return await this.claseLicenciaRepository.createQueryBuilder( "clase" )
            .innerJoin( TipoDeTramiteClaseLicenciaInstitucionEntity, "tipTraClaLicIns", "clase.idClaseLicencia = tipTraClaLicIns.idClaseLicencia" )
            .where(     "tipTraClaLicIns.idOficina = :idOficina", { idOficina })
            .andWhere(  "tipTraClaLicIns.idTipoTramite = :idTipoTramite", { idTipoTramite })
            .getMany();
    }
}
