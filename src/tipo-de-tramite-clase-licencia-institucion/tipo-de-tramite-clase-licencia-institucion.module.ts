import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoDeTramiteClaseLicenciaInstitucionService } from './service/tipo-de-tramite-clase-licencia-institucion.service';
import { TipoDeTramiteClaseLicenciaInstitucionController } from './controller/tipo-de-tramite-clase-licencia-institucion.controller';
import { TipoDeTramiteClaseLicenciaInstitucionEntity } from './entity/tipo-de-tramte-clase-licencia-institucion.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TiposTramiteEntity, ClasesLicenciasEntity ])],
  providers: [TipoDeTramiteClaseLicenciaInstitucionService],
  controllers: [TipoDeTramiteClaseLicenciaInstitucionController],
  exports: [TipoDeTramiteClaseLicenciaInstitucionService]
})
export class TipoDeTramiteClaseLicenciaInstitucionModule {}
