import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class TipoInstitucionEntityDTO {
  @ApiModelProperty()
  idTipoInstitucion: number;

  @ApiModelProperty()
  codInfraccion: number;

  @ApiModelProperty()
  descripcion: string;

  @ApiModelProperty()
  articuloLey: string;

  @ApiModelProperty()
  idGravedadInfraccion: number;
}
