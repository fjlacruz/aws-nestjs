import { Controller, Delete, Req } from '@nestjs/common';
import { Get, Param, Post, Body, Put } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TipoInfraccionService } from '../service/tipo-infraccion.service';
import { TipoInfraccionEntity } from '../entity/tipo-infraccion.entity';
import { Page, PageRequest } from '../pagination.entity';
import { HeaderUtil } from '../../base/base/header-util';
import { Request } from 'express';

@ApiTags('Tipo de Infraccion')
@Controller('tipos-infracciones')
export class TipoInfraccionController {
  constructor(private readonly tipoInstitucionService: TipoInfraccionService) {}

  @Get('/all')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all Tipo Infraccion',
    type: TipoInfraccionEntity,
  })
  async getAllTipos(@Req() req: Request): Promise<TipoInfraccionEntity[]> {
    const result = await this.tipoInstitucionService.getAll();
    return result;
  }

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all Paginate Tipo Infraccion',
    type: TipoInfraccionEntity,
  })
  async getAll(@Req() req: Request): Promise<TipoInfraccionEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.tipoInstitucionService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'The found Tipo Infraccion',
    type: TipoInfraccionEntity,
  })
  async getOne(@Param('id') id: number): Promise<TipoInfraccionEntity> {
    return await this.tipoInstitucionService.getOne(id);
  }

  @Post('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Create TipoInfraccionEntity' })
  @ApiResponse({
    status: 201,
    description: 'The Tipo Infraccion has been successfully created.',
    type: TipoInfraccionEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() tipoInstitucionEntity: TipoInfraccionEntity): Promise<TipoInfraccionEntity> {
    const created = await this.tipoInstitucionService.save(tipoInstitucionEntity);
    HeaderUtil.addEntityCreatedHeaders(req.res, 'Log', created.idTipoInfraccion);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Update log' })
  @ApiResponse({
    status: 200,
    description: 'The Tipo Infraccion has been successfully updated.',
    type: TipoInfraccionEntity,
  })
  async put(@Req() req: Request, @Body() tipoInstitucionEntity: TipoInfraccionEntity): Promise<TipoInfraccionEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'TipoInstitucionEntity', tipoInstitucionEntity.idTipoInfraccion);
    return await this.tipoInstitucionService.update(tipoInstitucionEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Update log with id' })
  @ApiResponse({
    status: 200,
    description: 'The Tipo Infraccion has been successfully updated.',
    type: TipoInfraccionEntity,
  })
  async putId(@Req() req: Request, @Body() logDTO: TipoInfraccionEntity): Promise<TipoInfraccionEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'Log', logDTO.idTipoInfraccion);
    return await this.tipoInstitucionService.update(logDTO);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Delete log' })
  @ApiResponse({
    status: 204,
    description: 'The Tipo Infraccion has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: number): Promise<void> {
    HeaderUtil.addEntityDeletedHeaders(req.res, 'Log', id);
    return await this.tipoInstitucionService.delete(id);
  }
}
