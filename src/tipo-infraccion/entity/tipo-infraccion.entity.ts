import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity('TipoInfracciones')
export class TipoInfraccionEntity {
  @PrimaryColumn()
  idTipoInfraccion: number;

  @Column()
  codInfraccion: number;

  @Column()
  descripcion: string;

  @Column()
  articuloLey: string;

  @Column()
  idGravedadInfraccion: number;
}
