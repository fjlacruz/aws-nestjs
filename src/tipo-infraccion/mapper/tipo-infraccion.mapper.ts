import { TipoInstitucionEntityDTO } from '../DTO/tipo-infraccion.dto';
import { TipoInfraccionEntity } from '../entity/tipo-infraccion.entity';

/**
 * A TipoInfraccionEntity mapper object.
 */
export class TipoInfraccionMapper {
  static fromDTOtoEntity(entityDTO: TipoInstitucionEntityDTO): TipoInfraccionEntity {
    if (!entityDTO) {
      return;
    }
    const entity = new TipoInfraccionEntity();
    const fields = Object.getOwnPropertyNames(entityDTO);
    fields.forEach(field => {
      entity[field] = entityDTO[field];
    });
    return entity;
  }

  static fromEntityToDTO(entity: TipoInfraccionEntity): TipoInstitucionEntityDTO {
    if (!entity) {
      return;
    }
    const entityDTO = new TipoInstitucionEntityDTO();

    const fields = Object.getOwnPropertyNames(entity);

    fields.forEach(field => {
      entityDTO[field] = entity[field];
    });

    return entityDTO;
  }
}
