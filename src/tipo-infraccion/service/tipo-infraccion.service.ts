import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { TipoInfraccionEntity } from '../entity/tipo-infraccion.entity';

@Injectable()
export class TipoInfraccionService {
  constructor(@InjectRepository(TipoInfraccionEntity) private readonly tipoInfraccionEntityRepository: Repository<TipoInfraccionEntity>) {}

  async findAndCount(options: FindManyOptions<TipoInfraccionEntity>): Promise<[TipoInfraccionEntity[], number]> {
    const resultList = await this.tipoInfraccionEntityRepository.findAndCount(options);
    const banks: TipoInfraccionEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(bank => banks.push(bank));
      resultList[0] = banks;
    }
    return resultList;
  }

  async getAll() {
    return await this.tipoInfraccionEntityRepository.find();
  }

  async getOne(idTipoInstitucion: number) {
    const data = await this.tipoInfraccionEntityRepository.findOne(idTipoInstitucion);
    if (!data) throw new NotFoundException('Tipo Resultado Cola Exam Practicos dont exist');

    return data;
  }

  async update(bank: TipoInfraccionEntity): Promise<TipoInfraccionEntity | undefined> {
    return await this.tipoInfraccionEntityRepository.save(bank);
  }

  async save(data: TipoInfraccionEntity): Promise<TipoInfraccionEntity> {
    return await this.tipoInfraccionEntityRepository.save(data);
  }

  async delete(idTipoInstitucion: number) {
    await this.tipoInfraccionEntityRepository.delete(idTipoInstitucion);
    const entityFind = await this.tipoInfraccionEntityRepository.findOne({
      where: {
        idTipoInstitucion: idTipoInstitucion,
      },
    });
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
