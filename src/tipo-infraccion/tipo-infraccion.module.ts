import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoInfraccionService } from './service/tipo-infraccion.service';
import { TipoInfraccionEntity } from './entity/tipo-infraccion.entity';
import { TipoInfraccionController } from './controller/tipo-infraccion.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TipoInfraccionEntity])],
  providers: [TipoInfraccionService],
  exports: [TipoInfraccionService],
  controllers: [TipoInfraccionController],
})
export class TipoInfraccionModule {}
