import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class TipoInstitucionEntityDTO {
  @IsNotEmpty()
  @ApiModelProperty({ description: 'bind_transaction_id field' })
  idTipoInstitucion: number;

  @IsNotEmpty()
  @ApiModelProperty({ description: 'bind_transaction_id field' })
  Nombre?: string;

  @IsNotEmpty()
  @ApiModelProperty({ description: 'bind_transaction_id field' })
  Descripcion: Date;
}
