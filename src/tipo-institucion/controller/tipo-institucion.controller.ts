import { Controller, Delete, Req } from '@nestjs/common';
import { Get, Param, Post, Body, Put } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TipoInstitucionService } from '../service/tipo-institucion.service';
import { TipoInstitucionEntity } from '../entity/tipo-institucion.entity';
import { Page, PageRequest } from '../pagination.entity';
import { HeaderUtil } from '../../base/base/header-util';
import { Request } from 'express';

@ApiTags('Tipo de Institucion')
@Controller('tipo-institucion')
export class TipoInstitucionController {
  constructor(private readonly tipoInstitucionService: TipoInstitucionService) {}

  @Get('/all')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all Tipo Institucion',
    type: TipoInstitucionEntity,
  })
  async getAllTipos(@Req() req: Request): Promise<TipoInstitucionEntity[]> {
    const result = await this.tipoInstitucionService.getAll();
    return result;
  }

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all Paginate Tipo Institucion',
    type: TipoInstitucionEntity,
  })
  async getAll(@Req() req: Request): Promise<TipoInstitucionEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.tipoInstitucionService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'The found Tipo Institucion',
    type: TipoInstitucionEntity,
  })
  async getOne(@Param('id') id: number): Promise<TipoInstitucionEntity> {
    return await this.tipoInstitucionService.getOne(id);
  }

  @Post('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Create TipoInstitucionEntity' })
  @ApiResponse({
    status: 201,
    description: 'The Tipo Institucion has been successfully created.',
    type: TipoInstitucionEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() tipoInstitucionEntity: TipoInstitucionEntity): Promise<TipoInstitucionEntity> {
    const created = await this.tipoInstitucionService.save(tipoInstitucionEntity);
    HeaderUtil.addEntityCreatedHeaders(req.res, 'Log', created.idTipoInstitucion);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Update log' })
  @ApiResponse({
    status: 200,
    description: 'The Tipo Institucion has been successfully updated.',
    type: TipoInstitucionEntity,
  })
  async put(@Req() req: Request, @Body() tipoInstitucionEntity: TipoInstitucionEntity): Promise<TipoInstitucionEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'TipoInstitucionEntity', tipoInstitucionEntity.idTipoInstitucion);
    return await this.tipoInstitucionService.update(tipoInstitucionEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Update log with id' })
  @ApiResponse({
    status: 200,
    description: 'The Tipo Institucion has been successfully updated.',
    type: TipoInstitucionEntity,
  })
  async putId(@Req() req: Request, @Body() logDTO: TipoInstitucionEntity): Promise<TipoInstitucionEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'Log', logDTO.idTipoInstitucion);
    return await this.tipoInstitucionService.update(logDTO);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Delete log' })
  @ApiResponse({
    status: 204,
    description: 'The Tipo Institucion has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: number): Promise<void> {
    HeaderUtil.addEntityDeletedHeaders(req.res, 'Log', id);
    return await this.tipoInstitucionService.delete(id);
  }
}
