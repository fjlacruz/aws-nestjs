import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity('TiposInstitucion')
export class TipoInstitucionEntity {
  @PrimaryColumn()
  idTipoInstitucion: number;

  @Column()
  Nombre: string;

  @Column()
  Descripcion: Date;
}
