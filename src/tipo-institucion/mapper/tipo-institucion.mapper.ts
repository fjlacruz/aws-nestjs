import { TipoInstitucionEntityDTO } from '../DTO/tipo-institucion.dto';
import { TipoInstitucionEntity } from '../entity/tipo-institucion.entity';

/**
 * A TipoInstitucionEntity mapper object.
 */
export class TipoInstitucionMapper {
  static fromDTOtoEntity(entityDTO: TipoInstitucionEntityDTO): TipoInstitucionEntity {
    if (!entityDTO) {
      return;
    }
    const entity = new TipoInstitucionEntity();
    const fields = Object.getOwnPropertyNames(entityDTO);
    fields.forEach(field => {
      entity[field] = entityDTO[field];
    });
    return entity;
  }

  static fromEntityToDTO(entity: TipoInstitucionEntity): TipoInstitucionEntityDTO {
    if (!entity) {
      return;
    }
    const entityDTO = new TipoInstitucionEntityDTO();

    const fields = Object.getOwnPropertyNames(entity);

    fields.forEach(field => {
      entityDTO[field] = entity[field];
    });

    return entityDTO;
  }
}
