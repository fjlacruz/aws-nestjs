import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { TipoInstitucionEntity } from '../entity/tipo-institucion.entity';

@Injectable()
export class TipoInstitucionService {
  constructor(
    @InjectRepository(TipoInstitucionEntity) private readonly tipoInstitucionEntityRepository: Repository<TipoInstitucionEntity>
  ) {}

  async findAndCount(options: FindManyOptions<TipoInstitucionEntity>): Promise<[TipoInstitucionEntity[], number]> {
    const resultList = await this.tipoInstitucionEntityRepository.findAndCount(options);
    const banks: TipoInstitucionEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(bank => banks.push(bank));
      resultList[0] = banks;
    }
    return resultList;
  }

  async getAll() {
    return await this.tipoInstitucionEntityRepository.find();
  }

  async getOne(idTipoInstitucion: number) {
    const data = await this.tipoInstitucionEntityRepository.findOne(idTipoInstitucion);
    if (!data) throw new NotFoundException('Tipo Resultado Cola Exam Practicos dont exist');

    return data;
  }

  async update(bank: TipoInstitucionEntity): Promise<TipoInstitucionEntity | undefined> {
    return await this.tipoInstitucionEntityRepository.save(bank);
  }

  async save(data: TipoInstitucionEntity): Promise<TipoInstitucionEntity> {
    return await this.tipoInstitucionEntityRepository.save(data);
  }

  async delete(idTipoInstitucion: number) {
    await this.tipoInstitucionEntityRepository.delete(idTipoInstitucion);
    const entityFind = await this.tipoInstitucionEntityRepository.findOne({
      where: {
        idTipoInstitucion: idTipoInstitucion,
      },
    });
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
