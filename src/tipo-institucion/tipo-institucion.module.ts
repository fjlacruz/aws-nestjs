import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoInstitucionService } from './service/tipo-institucion.service';
import { TipoInstitucionEntity } from './entity/tipo-institucion.entity';
import { TipoInstitucionController } from './controller/tipo-institucion.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TipoInstitucionEntity])],
  providers: [TipoInstitucionService],
  exports: [TipoInstitucionService],
  controllers: [TipoInstitucionController],
})
export class TipoInstitucionModule {}
