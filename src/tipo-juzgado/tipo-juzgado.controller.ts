import { Body, Controller, Delete, Get, Logger, Param, Post, Put, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TipoJuzgadoService } from './tipo-juzgado.service';
import { Request } from 'express';
import { Page, PageRequest } from '../tipo-institucion/pagination.entity';
import { HeaderUtil } from '../base/base/header-util';
import { TipoJuzgadoEntity } from './tipo-juzgado.entity';

@ApiTags('Tipos de Juzgados')
@Controller('tipos-juzgados')
export class TipoJuzgadoController {
  logger = new Logger('TipoJuzgadoController');
  constructor(private readonly tipoJuzgadoService: TipoJuzgadoService) {}

  @Get('/all')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Lista Tipos de Juzgados',
    type: TipoJuzgadoEntity,
  })
  async getAllTipos(@Req() req: Request): Promise<TipoJuzgadoEntity[]> {
    const result = await this.tipoJuzgadoService.getAll();
    return result;
  }

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Lista paginada de Tipos de Juzgados',
    type: TipoJuzgadoEntity,
  })
  async getAll(@Req() req: Request): Promise<TipoJuzgadoEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.tipoJuzgadoService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Obtiene un Tipo de Juzgado',
    type: TipoJuzgadoEntity,
  })
  async getOne(@Param('id') id: number): Promise<TipoJuzgadoEntity> {
    return await this.tipoJuzgadoService.getOne(id);
  }

  @Post('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Crea un Tipo de Juzgado' })
  @ApiResponse({
    status: 201,
    description: 'El Tipo de Juzgado fue creado correctamente.',
    type: TipoJuzgadoEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() TipoJuzgadoEntity: TipoJuzgadoEntity): Promise<TipoJuzgadoEntity> {
    const created = await this.tipoJuzgadoService.save(TipoJuzgadoEntity);
    HeaderUtil.addEntityCreatedHeaders(req.res, 'TipoJuzgado', created.id);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Actualiza un Tipo de Juzgado' })
  @ApiResponse({
    status: 200,
    description: 'El Tipo de Juzgado fue actualizado correctamente.',
    type: TipoJuzgadoEntity,
  })
  async put(@Req() req: Request, @Body() TipoJuzgadoEntity: TipoJuzgadoEntity): Promise<TipoJuzgadoEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'TipoJuzgadoEntity', TipoJuzgadoEntity.id);
    return await this.tipoJuzgadoService.update(TipoJuzgadoEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Actualiza un Tipo de Juzgado por ID' })
  @ApiResponse({
    status: 200,
    description: 'El Tipo de Juzgado fue actualizado correctamente.',
    type: TipoJuzgadoEntity,
  })
  async putId(@Req() req: Request, @Body() postulante: TipoJuzgadoEntity): Promise<TipoJuzgadoEntity> {
    HeaderUtil.addEntityCreatedHeaders(req.res, 'TipoJuzgado', postulante.id);
    return await this.tipoJuzgadoService.update(postulante);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @ApiOperation({ description: 'Eliminar un Tipo de Juzgado' })
  @ApiResponse({
    status: 204,
    description: 'El Tipo de Juzgado fue Eliminado correctamente.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: number): Promise<void> {
    HeaderUtil.addEntityDeletedHeaders(req.res, 'TipoJuzgado', id);
    return await this.tipoJuzgadoService.delete(id);
  }
}
