import { Entity, Column, OneToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from '../base/base/base.entity';
import { OficinasEntity } from '../tramites/entity/oficinas.entity';
import { TipoInstitucionEntity } from '../tipo-institucion/entity/tipo-institucion.entity';

@Entity('TiposJuzgados')
export class TipoJuzgadoEntity extends BaseEntity {
  @Column()
  codigo: string;

  @Column()
  nombreJuzgado: string;

  @Column()
  idTipoInstitucion: number;

  @Column()
  idOficina: number;

  @OneToOne(() => OficinasEntity, oficina => oficina.idOficina)
  @JoinColumn({ name: 'idOficina' })
  readonly oficina: OficinasEntity;

  @OneToOne(() => TipoInstitucionEntity, tipoInstitucion => tipoInstitucion.idTipoInstitucion)
  @JoinColumn({ name: 'idTipoInstitucion' })
  readonly tipoInstitucion: TipoInstitucionEntity;
}
