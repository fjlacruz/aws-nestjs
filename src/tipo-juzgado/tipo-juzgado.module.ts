import { Module } from '@nestjs/common';
import { TipoJuzgadoService } from './tipo-juzgado.service';
import { TipoJuzgadoController } from './tipo-juzgado.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoJuzgadoEntity } from './tipo-juzgado.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TipoJuzgadoEntity])],
  providers: [TipoJuzgadoService],
  exports: [TipoJuzgadoService],
  controllers: [TipoJuzgadoController],
})
export class TipoJuzgadoModule {}
