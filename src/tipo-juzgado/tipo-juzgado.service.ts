import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { TipoJuzgadoEntity } from './tipo-juzgado.entity';

@Injectable()
export class TipoJuzgadoService {
  constructor(@InjectRepository(TipoJuzgadoEntity) private readonly postulanteEntityRepository: Repository<TipoJuzgadoEntity>) {}

  async findAndCount(options: FindManyOptions<TipoJuzgadoEntity>): Promise<[TipoJuzgadoEntity[], number]> {
    const resultList = await this.postulanteEntityRepository.findAndCount(options);
    const banks: TipoJuzgadoEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(bank => banks.push(bank));
      resultList[0] = banks;
    }
    return resultList;
  }

  async getAll() {
    return await this.postulanteEntityRepository.find();
  }

  async getOne(idTipoInstitucion: number) {
    const data = await this.postulanteEntityRepository.findOne(idTipoInstitucion);
    if (!data) throw new NotFoundException('Tipo Resultado Cola Exam Practicos dont exist');

    return data;
  }

  async findOne(options: FindManyOptions<TipoJuzgadoEntity>): Promise<TipoJuzgadoEntity> {
    return await this.postulanteEntityRepository.findOne(options);
  }

  async update(bank: TipoJuzgadoEntity): Promise<TipoJuzgadoEntity | undefined> {
    return await this.postulanteEntityRepository.save(bank);
  }

  async save(data: TipoJuzgadoEntity): Promise<TipoJuzgadoEntity> {
    return await this.postulanteEntityRepository.save(data);
  }

  async delete(idTipoInstitucion: number) {
    await this.postulanteEntityRepository.delete(idTipoInstitucion);
    const entityFind = await this.postulanteEntityRepository.findOne({
      where: {
        idTipoInstitucion: idTipoInstitucion,
      },
    });
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
