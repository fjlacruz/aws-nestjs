import { ApiProperty } from "@nestjs/swagger";
import { RegistroUsuarios } from "src/registro-usuarios/entity/registro-usuario.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";

@Entity('TipoResgistroUsuario')
export class TipoResgistroUsuario {

    @PrimaryGeneratedColumn()
    idTipoResgistroUsuario: number;
    
    @Column()
    Nombre: string;

    @Column()
    Descripcion: string;

    @ApiProperty()
    @OneToMany(() => RegistroUsuarios, registroUsuarios => registroUsuarios.tipoRegistroUsuario)
    readonly registroUsuarios: RegistroUsuarios[];
}