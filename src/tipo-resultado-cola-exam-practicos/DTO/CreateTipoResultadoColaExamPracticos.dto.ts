import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateTipoResultadoColaExamPracticosDTO {

    @ApiPropertyOptional()
    nombre:String;

    @ApiPropertyOptional()
    created_at:Date;
}
