import { Test, TestingModule } from '@nestjs/testing';
import { TipoResultadoColaExamPracticosController } from './tipo-resultado-cola-exam-practicos.controller';

describe('TipoResultadoColaExamPracticosController', () => {
  let controller: TipoResultadoColaExamPracticosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TipoResultadoColaExamPracticosController],
    }).compile();

    controller = module.get<TipoResultadoColaExamPracticosController>(TipoResultadoColaExamPracticosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
