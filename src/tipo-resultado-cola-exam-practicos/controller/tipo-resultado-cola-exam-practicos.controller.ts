import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { CreateTipoResultadoColaExamPracticosDTO } from '../DTO/CreateTipoResultadoColaExamPracticos.dto';
import { TipoResultadoColaExamPracticosService } from '../services/tipo-resultado-cola-exam-practicos.service';

@ApiTags('Tipo-resultado-cola-exam-practicos')
@Controller('tipo-resultado-cola-exam-practicos')
export class TipoResultadoColaExamPracticosController {

    constructor(private readonly tipoResultadoColaExamPracticosService: TipoResultadoColaExamPracticosService) { }

    @Get('tipoResultadoColaExamPracticos')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todos los Tipo Resultado Cola Exam Practicos' })
    async getTipoResultadoColaExamPracticos() {
        const data = await this.tipoResultadoColaExamPracticosService.getTipoResultadoColaExamPracticos();
        return { data };
    }

    @Get('tipoResultadoColaExamPracticoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una Tipo Resultado Cola Exam Practicos por Id' })
    async getTipoResultadoColaExamPractico(@Param('id') id: number) {
        const data = await this.tipoResultadoColaExamPracticosService.getTipoResultadoColaExamPractico(id);
        return { data };
    }

    @Post('tipoResultadoColaExamPracticosUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de un Tipo Resultado Cola Exam Practicos' })
    async tipoResultadoColaExamPracticosUpdate(@Param('id') id: number, @Body() data: CreateTipoResultadoColaExamPracticosDTO) {
        return await this.tipoResultadoColaExamPracticosService.update(id, data);
    }

    @Post('crearTipoResultadoColaExamPractico')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Tipo Resultado Cola Exam Practicos' })
    async addTipoResultadoColaExamPractico(
        @Body() createTipoResultadoColaExamPracticosDTO: CreateTipoResultadoColaExamPracticosDTO) {
        const data = await this.tipoResultadoColaExamPracticosService.create(createTipoResultadoColaExamPracticosDTO);
        return {data};
    }

    @Get('deleteTipoResultadoColaExamPracticoById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que elimina un Tipo Resultado Cola Exam Practicos por Id' })
    async deleteTipoResultadoColaExamPracticoById(@Param('id') id: number) {
        const data = await this.tipoResultadoColaExamPracticosService.delete(id);
        return data;
    }
}
