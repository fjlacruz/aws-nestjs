import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";

@Entity('TipoResultadoColaExamPracticos')
export class TipoResultadoColaExamPracticosEntity {

    @PrimaryColumn()
    idTipoResultadoColaExamPracticos:number;

    @Column()
    nombre:String;

    @Column()
    created_at:Date;
}
