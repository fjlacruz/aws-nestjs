import { Test, TestingModule } from '@nestjs/testing';
import { TipoResultadoColaExamPracticosService } from './tipo-resultado-cola-exam-practicos.service';

describe('TipoResultadoColaExamPracticosService', () => {
  let service: TipoResultadoColaExamPracticosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TipoResultadoColaExamPracticosService],
    }).compile();

    service = module.get<TipoResultadoColaExamPracticosService>(TipoResultadoColaExamPracticosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
