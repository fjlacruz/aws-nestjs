import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository, UpdateResult } from 'typeorm';
import { CreateTipoResultadoColaExamPracticosDTO } from '../DTO/CreateTipoResultadoColaExamPracticos.dto';
import { TipoResultadoColaExamPracticosEntity } from '../entity/tipoResultadoColaExamPracticos.entity';

@Injectable()
export class TipoResultadoColaExamPracticosService {

    constructor(@InjectRepository(TipoResultadoColaExamPracticosEntity) private readonly tipoResultadoColaExamPracticosEntity: Repository<TipoResultadoColaExamPracticosEntity>) { }

    async getTipoResultadoColaExamPracticos() {
        return await this.tipoResultadoColaExamPracticosEntity.find();
    }

    async getTipoResultadoColaExamPractico(id:number){
        const data= await this.tipoResultadoColaExamPracticosEntity.findOne(id);
        if (!data)throw new NotFoundException("Tipo Resultado Cola Exam Practicos dont exist");
        
        return data;
    }

    async update(idTipoResultadoColaExamPracticos: number, data: Partial<CreateTipoResultadoColaExamPracticosDTO>) {
        await this.tipoResultadoColaExamPracticosEntity.update({ idTipoResultadoColaExamPracticos }, data);
        return await this.tipoResultadoColaExamPracticosEntity.findOne({ idTipoResultadoColaExamPracticos });
      }
      
    async create(data: CreateTipoResultadoColaExamPracticosDTO): Promise<TipoResultadoColaExamPracticosEntity> {
        return await this.tipoResultadoColaExamPracticosEntity.save(data);
    }

    async delete(id: number) {
        try {
            await getConnection()
            .createQueryBuilder()
            .delete()
            .from(TipoResultadoColaExamPracticosEntity)
            .where("idTipoResultadoColaExamPracticos = :id", { id: id })
            .execute();
            return {"code":200, "message":"success"};
        } catch (error) {
            return {"code":400, "error":error};
        }    
    }

}
