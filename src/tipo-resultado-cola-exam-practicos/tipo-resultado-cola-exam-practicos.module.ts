import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoResultadoColaExamPracticosController } from './controller/tipo-resultado-cola-exam-practicos.controller';
import { TipoResultadoColaExamPracticosEntity } from './entity/tipoResultadoColaExamPracticos.entity';
import { TipoResultadoColaExamPracticosService } from './services/tipo-resultado-cola-exam-practicos.service';

@Module({
  imports: [TypeOrmModule.forFeature([TipoResultadoColaExamPracticosEntity])],
  providers: [TipoResultadoColaExamPracticosService],
  exports: [TipoResultadoColaExamPracticosService],
  controllers: [TipoResultadoColaExamPracticosController]
})
export class TipoResultadoColaExamPracticosModule {}
