import { ApiPropertyOptional } from "@nestjs/swagger";

export class TipoRolDto {

    @ApiPropertyOptional()
    idTipoRol:number;
    @ApiPropertyOptional()
    nombre: string;
    @ApiPropertyOptional()
    descripcion: string;
}