import { ApiProperty } from "@nestjs/swagger";
import { Roles } from "src/roles/entity/roles.entity";
import {Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, OneToMany} from "typeorm";


@Entity('TipoRol')
export class TipoRol {

    @PrimaryGeneratedColumn()
    idTipoRol:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;

    @ApiProperty()
    @OneToMany(() => Roles, roles => roles.tipoRoles)
    readonly rol: Roles[];
}