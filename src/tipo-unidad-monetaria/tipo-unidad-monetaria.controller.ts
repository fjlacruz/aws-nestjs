import { Body, Controller, Delete, Get, Param, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { HeaderUtil } from '../base/base/header-util';
import { AuthGuard } from '@nestjs/passport';
import { Post as PostMethod } from '@nestjs/common/decorators/http/request-mapping.decorator';
import { Page, PageRequest } from './pagination-tipo-unidad-monetaria.entity';
import { TipoUnidadMonetariaEntity } from './tipo-unidad-monetaria.entity';
import { TipoUnidadMonetariaService } from './tipo-unidad-monetaria.service';

@ApiTags('Tipo Unidad Monetaria')
@Controller('tipo-unidades-monetarias')
export class TipoUnidadMonetariaController {
  constructor(private readonly tipoUnidadMonetariaService: TipoUnidadMonetariaService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'List all records',
    type: TipoUnidadMonetariaEntity,
  })
  async getAll(@Req() req: Request): Promise<TipoUnidadMonetariaEntity[]> {
    const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
    const [results, count] = await this.tipoUnidadMonetariaService.findAndCount({
      skip: +pageRequest.page * pageRequest.size,
      take: +pageRequest.size,
      order: pageRequest.sort.asOrder(),
    });
    HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
    return results;
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve una Sentencia por Id' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: TipoUnidadMonetariaEntity,
  })
  async getOne(@Param('id') id: string): Promise<TipoUnidadMonetariaEntity> {
    return await this.tipoUnidadMonetariaService.findById(id);
  }

  @PostMethod('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea una nueva Sentencia' })
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    type: TipoUnidadMonetariaEntity,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async post(@Req() req: Request, @Body() tipoUnidadMonetariaEntity: TipoUnidadMonetariaEntity): Promise<TipoUnidadMonetariaEntity> {
    const created = await this.tipoUnidadMonetariaService.save(tipoUnidadMonetariaEntity);
    return created;
  }

  @Put('/')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: TipoUnidadMonetariaEntity,
  })
  async put(@Req() req: Request, @Body() tipoUnidadMonetariaEntity: TipoUnidadMonetariaEntity): Promise<TipoUnidadMonetariaEntity> {
    return await this.tipoUnidadMonetariaService.update(tipoUnidadMonetariaEntity);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que actualiza una Sentencia' })
  @ApiResponse({
    status: 200,
    description: 'The record has been successfully updated.',
    type: TipoUnidadMonetariaEntity,
  })
  async putId(@Req() req: Request, @Body() tipoUnidadMonetariaEntity: TipoUnidadMonetariaEntity): Promise<TipoUnidadMonetariaEntity> {
    return await this.tipoUnidadMonetariaService.update(tipoUnidadMonetariaEntity);
  }

  @Delete('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Elimina una Sentencia' })
  @ApiResponse({
    status: 204,
    description: 'The record has been successfully deleted.',
  })
  async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
    return await this.tipoUnidadMonetariaService.deleteById(id);
  }
}
