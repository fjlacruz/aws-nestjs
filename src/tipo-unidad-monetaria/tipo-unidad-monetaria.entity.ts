import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

@Entity('TipoUnidadMonetaria')
export class TipoUnidadMonetariaEntity {
  @PrimaryGeneratedColumn()
  idtipoUnidadMonetaria: number;
  @Column()
  @ApiModelProperty()
  moneda: string;
  @Column()
  @ApiModelProperty()
  descripcion: string;
}
