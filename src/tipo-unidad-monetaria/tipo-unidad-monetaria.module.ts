import { Module } from '@nestjs/common';
import { TipoUnidadMonetariaController } from './tipo-unidad-monetaria.controller';
import { TipoUnidadMonetariaService } from './tipo-unidad-monetaria.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoUnidadMonetariaEntity } from './tipo-unidad-monetaria.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TipoUnidadMonetariaEntity])],
  controllers: [TipoUnidadMonetariaController],
  providers: [TipoUnidadMonetariaService],
  exports: [TipoUnidadMonetariaService],
})
export class TipoUnidadMonetariaModule {}
