import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { TipoUnidadMonetariaEntity } from './tipo-unidad-monetaria.entity';

const relationshipNames = [];

@Injectable()
export class TipoUnidadMonetariaService {
  constructor(
    @InjectRepository(TipoUnidadMonetariaEntity) private readonly tipoUnidadMonetariaEntityRepository: Repository<TipoUnidadMonetariaEntity>
  ) {}

  async findById(id: string): Promise<TipoUnidadMonetariaEntity | undefined> {
    const options = { relations: relationshipNames };
    const result = await this.tipoUnidadMonetariaEntityRepository.findOne(id, options);
    return result;
  }

  async findByfields(options: FindOneOptions<TipoUnidadMonetariaEntity>): Promise<TipoUnidadMonetariaEntity | undefined> {
    const result = await this.tipoUnidadMonetariaEntityRepository.findOne(options);
    return result;
  }

  async findAndCount(options: FindManyOptions<TipoUnidadMonetariaEntity>): Promise<[TipoUnidadMonetariaEntity[], number]> {
    options.relations = relationshipNames;
    const resultList = await this.tipoUnidadMonetariaEntityRepository.findAndCount(options);
    const sentenciaEntity: TipoUnidadMonetariaEntity[] = [];
    if (resultList && resultList[0]) {
      resultList[0].forEach(tipoUnidadMonetaria => sentenciaEntity.push(tipoUnidadMonetaria));
      resultList[0] = sentenciaEntity;
    }
    return resultList;
  }

  async save(sentenciaEntity: TipoUnidadMonetariaEntity): Promise<TipoUnidadMonetariaEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.tipoUnidadMonetariaEntityRepository.save(entity);
    return result;
  }

  async update(sentenciaEntity: TipoUnidadMonetariaEntity): Promise<TipoUnidadMonetariaEntity | undefined> {
    const entity = sentenciaEntity;
    const result = await this.tipoUnidadMonetariaEntityRepository.save(entity);
    return result;
  }

  async deleteById(id: string): Promise<void | undefined> {
    await this.tipoUnidadMonetariaEntityRepository.delete(id);
    const entityFind = await this.findById(id);
    if (entityFind) {
      throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
    }
    return;
  }
}
