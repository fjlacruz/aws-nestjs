import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class ModifyTipoDocumentoDto {
  @ApiProperty({ name: 'nombre', type: 'string' })
  @IsNotEmpty({ message: 'nombre es obligatorio' })
  @IsString({ message: 'nombre debe ser un strring' })
  @MaxLength(100, { message: 'nombre debe tener menos de 100 carcateres' })
  nombre: string;

  @ApiProperty({ name: 'descripcion', type: 'string' })
  @IsNotEmpty({ message: 'descripcion' })
  @IsString({ message: 'descripción debe ser un strring' })
  @MaxLength(300, { message: 'descripción debe tener menos de 300 caracteres' })
  descripcion: string;
}
