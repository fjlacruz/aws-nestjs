export class TiposDocumentoDto {
  idTipoDocumento: number;
  nombre: string;
  descripcion: string;
  asignado: boolean;
}
