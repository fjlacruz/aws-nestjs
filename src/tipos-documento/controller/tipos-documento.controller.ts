import { Get, Param, Post, Body, Put, Patch, Controller, Req, UsePipes, ValidationPipe, ParseIntPipe, UseGuards } from '@nestjs/common';
import { TiposDocumentoService } from '../service/tipos-documento.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { Resultado } from 'src/utils/resultado';
import { ModifyTipoDocumentoDto } from '../DTO/modify-tipo-documento.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Tipos-documento')
@Controller('tipos-documento')
export class TiposDocumentoController {
  constructor(private readonly tiposDocumentoService: TiposDocumentoService, private readonly authService: AuthService) {}

  @Get()
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos los Tipos de documento' })
  async getSolicitudes(@Req() req) {
    let data = null;
    // const splittedBearerToken = req.headers.authorization.split(' ');
    // const token = splittedBearerToken[1];
    // const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListadoTipoDocumentos]);

    // const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    // if (validarPermisos.ResultadoOperacion) {
      data = await this.tiposDocumentoService.findAll();
    // } else {
    //   data = validarPermisos;
    // }

    return { data };
  }

    
  @Get('getTiposDocumentosListaNombres')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos las Tipos de Documentos, lista de id y nombre' })
  async getTiposDocumentosListaNombres(@Req() req: any) {

          
          const data = await this.tiposDocumentoService.getTiposDocumentosListaNombres();
          return { data };


  }

  @Get('tiposDocumentoListado')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los Tipos de documento con respuesta' })
  async getTiposDocumentoListado(@Req() req) {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.tiposDocumentoService.getTiposDocumentoListado();
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('tiposDocumentoPaginado')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Tipos de documento según filtros y paginación' })
  async getTiposDocumentoPaginado(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.tiposDocumentoService.getTiposDocumentoPaginado(paginacionArgs);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Get(':id')
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los Tipos de documento según filtros y paginación' })
  public async getTiposDocumentoById(@Req() req: any, @Param('id', ParseIntPipe) id: number) {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosVisualizarRolesGruposdePermisosTipodeDocumentos]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.tiposDocumentoService.findOne(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('crearTipoDocumento')
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un tipo de documento' })
  public async create(@Req() req, @Body() createTipoDocumento: ModifyTipoDocumentoDto): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const idUsuario = validarPermisos.Respuesta[0].idUsuario;
      data = await this.tiposDocumentoService.create(createTipoDocumento, idUsuario);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('editarTipoDocumento/:id')
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza un tipo de documento' })
  public async update(
    @Req() req,
    @Param('id', ParseIntPipe) id: number,
    @Body() createTipoDocumento: ModifyTipoDocumentoDto
  ): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.tiposDocumentoService.update(id, createTipoDocumento);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('eliminarTipoDocumento/:id')
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que elimina un tipo de documento' })
  public async remove(@Req() req, @Param('id', ParseIntPipe) id: number): Promise<{ data: Resultado }> {
    let data = null;
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCRolesPermisosTipodeDocumentosSeleccionarAcciones]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      data = await this.tiposDocumentoService.remove(id);
    } else {
      data = validarPermisos;
    }

    return { data };
  }
}
