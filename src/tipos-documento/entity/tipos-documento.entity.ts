import { ApiProperty } from '@nestjs/swagger';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
  OneToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('TiposDocumento')
export class TiposDocumentoEntity {
  @PrimaryGeneratedColumn()
  idTipoDocumento: number;

  @Column({ name: 'Nombre', type: 'varchar', length: 100 })
  nombre: string;

  @Column({ name: 'Descripcion', type: 'varchar', length: 300 })
  descripcion: string;

  @Column({
    name: 'created_at',
    type: 'time without time zone',
  })
  createdAt: Date;

  @Column({
    name: 'update_at',
    type: 'time without time zone',
    nullable: false,
  })
  updateAt: Date;

  @Column()
  idUsuario: number;

  @OneToMany(() => DocumentosTramitesEntity, documento => documento.tipoDocumento)
  readonly documentos: DocumentosTramitesEntity[];
}
