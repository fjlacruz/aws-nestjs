import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class TipoDocumentoTransformer {
  @Expose({ groups: ['toShow', 'listado'] })
  idTipoDocumento: number;

  @Expose({ groups: ['toShow', 'listado'] })
  nombre: string;

  @Expose({ groups: ['toShow', 'listado'] })
  descripcion: string;

  @Expose({ name: 'createdAt', groups: ['toShow'] })
  created_at: Date;

  @Expose({ name: 'updateAt', groups: ['toShow'] })
  update_at: Date;
}
