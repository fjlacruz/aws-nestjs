import { TipoDocumentoTransformer } from './../mapper/tipo-documento.transformer';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resultado } from 'src/utils/resultado';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { TiposDocumentoEntity } from '../entity/tipos-documento.entity';
import { TiposDocumentoDto } from '../DTO/tiposDocumento.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { ColumnasTiposDocumento } from '../../constantes';
import { SetResultadoService } from 'src/shared/services/set-resultado/set-resultado.service';
import { plainToClass } from 'class-transformer';
import { ModifyTipoDocumentoDto } from '../DTO/modify-tipo-documento.dto';
import { DocumentosTramitesService } from 'src/documentos-tramites/services/documentos-tramites.service';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { OrdenPaginacion } from 'src/utils/OrdenPaginacion';
import { ObjetoListado } from 'src/utils/objetoListado';

@Injectable()
export class TiposDocumentoService {
  constructor(
    @InjectRepository(TiposDocumentoEntity) private readonly tiposDocumentoRespository: Repository<TiposDocumentoEntity>,
    @InjectRepository(DocumentosTramitesEntity) private readonly documentosTramitesRepository: Repository<DocumentosTramitesEntity>,
    private readonly setResultadoService: SetResultadoService
  ) {}

  // #region CRUD

  public async findAll(group: string = 'toShow'): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    await this.tiposDocumentoRespository
      .createQueryBuilder()
      .getMany()
      .then((res: TiposDocumentoEntity[]) => {
        // * Mapeamos los resultados
        const tipoDocumentos: TipoDocumentoTransformer[] = plainToClass(TipoDocumentoTransformer, res, {
          groups: [group],
        });

        if (Array.isArray(tipoDocumentos) && tipoDocumentos.length) {
          // * Modificamos resultados para respuesta con datos
          this.setResultadoService.setResponse<TipoDocumentoTransformer[]>(resultado, tipoDocumentos);
        } else {
          // * Modificamos resultados para respuesta sin datos
          this.setResultadoService.setResponse<null>(resultado, null, false, 'No hay Tipo de Documentos para mostrar');
        }
      })
      .catch((err: Error) => {
        // * Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'No se ha podido cargar los Tipo de Documentos');
      });

    return resultado;
  }

  public async findOne(id: number, group: string = 'toShow'): Promise<Resultado> {
    const resultado = new Resultado();

    await this.tiposDocumentoRespository
      .createQueryBuilder()
      .where({ idTipoDocumento: id })
      .getOneOrFail()
      .then((res: TiposDocumentoEntity) => {
        // * Mapeamos los resultados
        const tipoDocumentos: TipoDocumentoTransformer = plainToClass(TipoDocumentoTransformer, res, {
          groups: [group],
        });

        // * Modificamos resultado para respuesta
        this.setResultadoService.setResponse<TipoDocumentoTransformer>(resultado, tipoDocumentos);
      })
      .catch((err: Error) => {
        // * Mapeamos respuesta para errores
        this.setResultadoService.setError(resultado, 'No se ha podido obtener el tipo de documento');
      });

    return resultado;
  }

  async getTiposDocumentosListaNombres(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let resultadoTiposDocumentos: TiposDocumentoEntity[] = [];
    let resultadosDTO: ObjetoListado[] = [];

    try {

        resultadoTiposDocumentos = await this.tiposDocumentoRespository.find();

        if (Array.isArray(resultadoTiposDocumentos) && resultadoTiposDocumentos.length) {

            resultadoTiposDocumentos.forEach(item => {
                let nuevoElemento: ObjetoListado = {
                    id: item.idTipoDocumento,
                    Nombre: item.nombre
                }

                resultadosDTO.push(nuevoElemento);
            });
        }

        if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
            resultado.Respuesta = resultadosDTO;
            resultado.ResultadoOperacion = true;
            resultado.Mensaje = 'Tipos de Documentos obtenidos correctamente';
        } else {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'No se encontraron Tipos de Documentos';
        }

    } catch (error) {
        console.error(error);
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error obteniendo Tipos de Documentos';
    }

    return resultado;
}

  async getTiposDocumentoListado(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let resultadoTiposDocumento: TiposDocumentoEntity[] = [];
    let resultadosDTO: TiposDocumentoDto[] = [];

    try {
      resultadoTiposDocumento = await this.tiposDocumentoRespository
        .createQueryBuilder('td')
        .leftJoinAndSelect('td.documentos', 'd')
        .getMany()
        .then((res: TiposDocumentoEntity[]) => res);

      if (Array.isArray(resultadoTiposDocumento) && resultadoTiposDocumento.length) {
        resultadoTiposDocumento.forEach(item => {
          let nuevoElemento: TiposDocumentoDto = {
            idTipoDocumento: item.idTipoDocumento,
            nombre: item.nombre,
            descripcion: item.descripcion,
            asignado: item.documentos.length > 0 ? true : false,
          };

          resultadosDTO.push(nuevoElemento);
        });
      }

      if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
        resultado.Respuesta = resultadosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tipos de Documento obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Tipos de Documento';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Tipos de Documento';
    }

    return resultado;
  }

  async getTiposDocumentoPaginado(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    const orderBy = ColumnasTiposDocumento[paginacionArgs.orden.orden];
    let tiposCLPaginados: Pagination<TiposDocumentoEntity>;
    let tiposParseados = new PaginacionDtoParser<TiposDocumentoDto>();

    let filtro = '';

    try {
      if (paginacionArgs.filtro) {
        filtro = paginacionArgs.filtro;
        while (filtro.search('/') !== -1) {
          filtro.replace('/', '-');
        }
      }

      const orderBy = this.getOrderBy(paginacionArgs.orden);

      tiposCLPaginados = await paginate<TiposDocumentoEntity>(this.tiposDocumentoRespository, paginacionArgs.paginationOptions, {
        relations: ['documentos'],
        where: qb => {
          qb.where('lower(unaccent(TiposDocumentoEntity.Nombre)) like lower(unaccent(:nombre))', { nombre: `%${filtro}%` }).orWhere(
            'lower(unaccent(TiposDocumentoEntity.Descripcion)) like lower(unaccent(:descripcion))',
            { descripcion: `%${filtro}%` }
          );
        },
        order: orderBy,
      });

      let tiposDTO: TiposDocumentoDto[] = [];

      if (Array.isArray(tiposCLPaginados.items) && tiposCLPaginados.items.length) {
        tiposCLPaginados.items.forEach(tipo => {
          const tipoDTO: TiposDocumentoDto = new TiposDocumentoDto();
          tipoDTO.idTipoDocumento = tipo.idTipoDocumento;
          tipoDTO.nombre = tipo.nombre;
          tipoDTO.descripcion = tipo.descripcion;
          tipoDTO.asignado = tipo.documentos.length > 0 ? true : false;

          tiposDTO.push(tipoDTO);
        });

        tiposParseados.items = tiposDTO;
        tiposParseados.meta = tiposCLPaginados.meta;
        tiposParseados.links = tiposCLPaginados.links;

        resultado.Respuesta = tiposParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tipos de Documento obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Tipos de Documento';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Tipos de Documento';
    }

    return resultado;
  }

  public async create(createTipoDocumentoDto: ModifyTipoDocumentoDto, idUsuario: number): Promise<Resultado> {
    const resultado = new Resultado();

    try {
      const { nombre, descripcion } = createTipoDocumentoDto;
      const date = new Date();
      const newTipoDocumento = this.tiposDocumentoRespository.create({ nombre, descripcion, createdAt: date, updateAt: date, idUsuario });

      if (!newTipoDocumento) throw new Error('Error al crear tipo de documento');

      await this.tiposDocumentoRespository
        .createQueryBuilder()
        .insert()
        .into(TiposDocumentoEntity)
        .values(newTipoDocumento)
        .execute()
        .then((res: InsertResult) => {
          if (res.identifiers.length <= 0) throw new Error('No se ha añadido ningun tipo de documento');
        })
        .catch((err: Error) => {
          throw new Error('Error al insertar tipo de documento');
        });

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Tipo Documento creado con éxito');
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido crear Tipo de Documento');
    } finally {
      return resultado;
    }
  }

  public async update(id: number, updateTipoDocumentoDto: ModifyTipoDocumentoDto): Promise<Resultado> {
    const resultado = new Resultado();

    try {
      const date = new Date();
      const { nombre, descripcion } = updateTipoDocumentoDto;

      await this.tiposDocumentoRespository
        .createQueryBuilder()
        .update(TiposDocumentoEntity)
        .set({ nombre, descripcion, updateAt: date })
        .where({ idTipoDocumento: id })
        .execute()
        .then((res: UpdateResult) => {
          if (res.affected <= 0) throw new Error('No se ha añadido ningun tipo de documento');
        })
        .catch((err: Error) => {
          throw new Error('Error al actualizar tipo de documento');
        });

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Tipo Documento actualiado con éxito');
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido actualizar Tipo de Documento');
    } finally {
      return resultado;
    }
  }

  public async remove(id: number): Promise<Resultado> {
    const resultado = new Resultado();

    try {
      if (await this.hasDocumentos(id)) throw new Error('El tipo de documento esta asignado');

      await this.tiposDocumentoRespository
        .createQueryBuilder()
        .delete()
        .where({ idTipoDocumento: id })
        .execute()
        .then((res: DeleteResult) => {
          if (res.affected <= 0) throw new Error('No se ha eliminado ninguna fila');
        })
        .catch((err: Error) => {
          throw new Error('Error no se ha podido eliminar tipo de documento');
        });

      // * Modificamos resultado para respuesta
      this.setResultadoService.setResponse<null>(resultado, null, true, 'Tipo de Documento eliminado con éxito');
    } catch (err) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido eliminar Tipo de Documento');
    } finally {
      return resultado;
    }
  }

  // #endregion CRUD

  // #region private functions

  private getOrderBy(ordenPaginacion: OrdenPaginacion) {
    let orderBy = {};

    switch (ordenPaginacion.orden.toLowerCase()) {
      case 'nombre':
        orderBy = { nombre: ordenPaginacion.ordenarPor };
        break;
      case 'descripcion':
        orderBy = { descripcion: ordenPaginacion.ordenarPor };
        break;
    }

    return orderBy;
  }

  private async hasDocumentos(id: number): Promise<boolean> {
    let hasDocs = false;

    await this.documentosTramitesRepository
      .createQueryBuilder('doc')
      .leftJoinAndSelect('doc.tipoDocumento', 'td')
      .where('doc.idTipoDocumento = :id', { id })
      .getCount()
      .then((res: number) => {
        if (res > 0) hasDocs = true;
      });

    return hasDocs;
  }

  // #endregion private functions
}
