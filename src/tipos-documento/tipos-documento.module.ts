import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { SharedModule } from 'src/shared/shared.module';
import { User } from 'src/users/entity/user.entity';
import { UtilService } from 'src/utils/utils.service';
import { TiposDocumentoController } from './controller/tipos-documento.controller';
import { TiposDocumentoEntity } from './entity/tipos-documento.entity';
import { TiposDocumentoService } from './service/tipos-documento.service';

@Module({
  imports: 
  [
    TypeOrmModule.forFeature([TiposDocumentoEntity, User, RolesUsuarios, DocumentosTramitesEntity]),
    SharedModule,
  ],
  providers: [TiposDocumentoService, UtilService, AuthService],
  exports: [TiposDocumentoService],
  controllers: [TiposDocumentoController],
})
export class TiposDocumentoModule {}
