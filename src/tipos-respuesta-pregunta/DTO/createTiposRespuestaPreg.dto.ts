import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateTiposRespuestaPregDto {
    
    @ApiPropertyOptional()
    idTipoRespuesta: number;
    
    @ApiPropertyOptional()
    nombreTipo:string;
    
  }
   