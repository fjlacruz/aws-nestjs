import { ApiPropertyOptional } from "@nestjs/swagger";

export class TiposRespuestaPreguntaDTO {
    @ApiPropertyOptional()
    idTipoRespuesta:number;

    @ApiPropertyOptional()
    nombreTipo:string;

}
