import { Test, TestingModule } from '@nestjs/testing';
import { TiposRespuestaPreguntaController } from './tipos-respuesta-pregunta.controller';

describe('TiposRespuestaPreguntaController', () => {
  let controller: TiposRespuestaPreguntaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TiposRespuestaPreguntaController],
    }).compile();

    controller = module.get<TiposRespuestaPreguntaController>(TiposRespuestaPreguntaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
