import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { TiposRespuestaPreguntaDTO } from '../DTO/tipos-respuesta-pregunta.dto';
import { TiposRespuestaPreguntaService } from '../services/tipos-respuesta-pregunta.service';

@ApiTags('Tipos-respuesta-pregunta')
@Controller('tipos-respuesta-pregunta')
export class TiposRespuestaPreguntaController {

    constructor(private readonly tiposRespuestaPreguntaService: TiposRespuestaPreguntaService) { }

    @Get('getTiposRespuestasPreguntas')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas los Tipos Respuestas Preguntas' })
    async getTiposRespuestasPreguntas() {
        const data = await this.tiposRespuestaPreguntaService.getTiposRespuestasPreguntas();
        return { data };
    }

    @Get('getTiposRespuestaPreguntaById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Tipo Respuesta Pregunta por Id' })
    async getTiposRespuestaPreguntaById(@Param('id') id: number) {
        const data = await this.tiposRespuestaPreguntaService.getTiposRespuestaPregunta(id);
        return { data };
    }

    @Patch('updateTiposRespuestaPregunta/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de un Tipo Respuesta Pregunta' })
    async updateRespuestasSeleccion(@Param('id') id: number, @Body() data: TiposRespuestaPreguntaDTO) {
        return await this.tiposRespuestaPreguntaService.update(id, data);
    }

    @Post('crearTiposRespuestaPregunta')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Tipo Respuesta Pregunta' })
    async addComuna(
        @Body() createComunaDTO: TiposRespuestaPreguntaDTO) {
        const data = await this.tiposRespuestaPreguntaService.create(createComunaDTO);
        return {data};
    }

}
