import { Test, TestingModule } from '@nestjs/testing';
import { TiposRespuestaPreguntaService } from './tipos-respuesta-pregunta.service';

describe('TiposRespuestaPreguntaService', () => {
  let service: TiposRespuestaPreguntaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TiposRespuestaPreguntaService],
    }).compile();

    service = module.get<TiposRespuestaPreguntaService>(TiposRespuestaPreguntaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
