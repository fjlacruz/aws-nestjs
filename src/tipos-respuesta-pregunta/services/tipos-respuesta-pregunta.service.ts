import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { TiposRespuestaPreguntaDTO } from '../DTO/tipos-respuesta-pregunta.dto';
import { TiposRespuestaPregEntity } from '../entity/tiposRespuestaPregunta.entity';

@Injectable()
export class TiposRespuestaPreguntaService {
    constructor(@InjectRepository(TiposRespuestaPregEntity) private readonly tiposRespuestaPregRepository: Repository<TiposRespuestaPregEntity>) { }

    async getTiposRespuestasPreguntas() {
        return await this.tiposRespuestaPregRepository.find();
    }


    async getTiposRespuestaPregunta(id:number){
        const tiporesp= await this.tiposRespuestaPregRepository.findOne(id);
        if (!tiporesp)throw new NotFoundException("Tipo Respusta dont exist");
        
        return tiporesp;
    }


    async update(idTipoRespuesta: number, data: Partial<TiposRespuestaPreguntaDTO>) {
        await this.tiposRespuestaPregRepository.update({ idTipoRespuesta }, data);
        return await this.tiposRespuestaPregRepository.findOne({ idTipoRespuesta });
      }
      
    async create(data: TiposRespuestaPreguntaDTO): Promise<TiposRespuestaPreguntaDTO> {
        return await this.tiposRespuestaPregRepository.save(data);
    }
}