import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TiposRespuestaPreguntaController } from './controller/tipos-respuesta-pregunta.controller';
import { TiposRespuestaPregEntity } from './entity/tiposRespuestaPregunta.entity';
import { TiposRespuestaPreguntaService } from './services/tipos-respuesta-pregunta.service';

@Module({
  imports: [TypeOrmModule.forFeature([TiposRespuestaPregEntity])],
  providers: [TiposRespuestaPreguntaService],
  exports: [TiposRespuestaPreguntaService],
  controllers: [TiposRespuestaPreguntaController]
})

export class TiposRespuestaPreguntaModule {}