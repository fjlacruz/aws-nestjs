import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber } from "class-validator";

export class OrdenTramiteExaminacionDTO {

    @ApiPropertyOptional()
    idOrdenTramiteExaminacion: number;

    @ApiPropertyOptional()
    idInstitucion: number;

    @ApiPropertyOptional()
    idTipoTramite: number;

    @ApiPropertyOptional()
    @IsNotEmpty()
    @IsNumber()
    idTipoExaminacion: number;

    @ApiPropertyOptional()
    idOficina: number;
    
    @ApiPropertyOptional()
    @IsNotEmpty()
    @IsNumber()
    orden: number;
}
