import { ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, ValidateNested } from "class-validator";
import { ClaseLicenciaRequeridaTipoTramiteDTO } from "src/ClaseLicenciaRequeridaTipoTramite/DTO/claseLicenciaRequeridaTipoTramite.dto";
import { RolTipoTramiteDTO } from "src/rolTipoTramite/DTO/rolTipoTramite.dto";
import { TipoDeTramiteClaseLicenciaInstitucionDto } from "src/tipo-de-tramite-clase-licencia-institucion/dto/tipo-de-tramite-clase-licencia-institucion.dto";
import { OrdenTramiteExaminacionDTO } from "./ordenTramiteExaminacion.dto";
import { TramiteDocumentoRequeridoDTO } from "./tramiteDocumentoRequerido.dto";

export class ParametrosGeneralDto {
  
    @ApiPropertyOptional()
    idParametrosGeneral: number;
    
    @ApiPropertyOptional()
    nombre: string;

    @ApiPropertyOptional()
    descripcion: string;

    @ApiPropertyOptional()
    tipoDato: number;

    @ApiPropertyOptional()
    valueString: string;

    @ApiPropertyOptional()
    valueNumber: number;

    @ApiPropertyOptional()
    valueBoolean: boolean;
}