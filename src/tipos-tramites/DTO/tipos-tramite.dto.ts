import { ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, ValidateNested } from "class-validator";
import { ClaseLicenciaRequeridaTipoTramiteDTO } from "src/ClaseLicenciaRequeridaTipoTramite/DTO/claseLicenciaRequeridaTipoTramite.dto";
import { RolTipoTramiteDTO } from "src/rolTipoTramite/DTO/rolTipoTramite.dto";
import { TipoDeTramiteClaseLicenciaInstitucionDto } from "src/tipo-de-tramite-clase-licencia-institucion/dto/tipo-de-tramite-clase-licencia-institucion.dto";
import { OrdenTramiteExaminacionDTO } from "./ordenTramiteExaminacion.dto";
import { TramiteDocumentoRequeridoDTO } from "./tramiteDocumentoRequerido.dto";

export class TiposTramiteDto {
  
    @ApiPropertyOptional()
    idTipoTramite: number;
    
    @ApiPropertyOptional()
    nombre: string;

    @ApiPropertyOptional()
    descripcion: string;

    @ApiPropertyOptional()
    activo: boolean;

    @ApiPropertyOptional()
    idsOficinas?: number[];

    @ApiPropertyOptional()
    @ValidateNested({ each: true })
    @Type(() => TipoDeTramiteClaseLicenciaInstitucionDto)
    licencias?: TipoDeTramiteClaseLicenciaInstitucionDto[];

    @ApiPropertyOptional()
    @ValidateNested({ each: true })
    @Type(() => RolTipoTramiteDTO)
    rolesAsociados?: RolTipoTramiteDTO[];

    @ApiPropertyOptional()
    controlEdad?: number;

    @ApiPropertyOptional()
    requiereLicenciaPrevia: boolean;

    @ApiPropertyOptional()
    @ValidateNested({ each: true })
    @Type(() => ClaseLicenciaRequeridaTipoTramiteDTO)
    licenciasPrevias?: ClaseLicenciaRequeridaTipoTramiteDTO[];

    @ApiPropertyOptional()
    @ValidateNested({ each: true })
    @Type(() => OrdenTramiteExaminacionDTO)
    actividadesAsociadas?: OrdenTramiteExaminacionDTO[];

    @ApiPropertyOptional()
    @ValidateNested({ each: true })
    @Type(() => TramiteDocumentoRequeridoDTO)
    documentosRequeridos?: TramiteDocumentoRequeridoDTO[];
}