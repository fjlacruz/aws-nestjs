import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsNumber } from "class-validator";

export class TramiteDocumentoRequeridoDTO {

    @ApiPropertyOptional()
    idTramiteDcoumentoReq: number;

    @ApiPropertyOptional()
    idTipoTramite: number;
    
    @ApiPropertyOptional()
    @IsNotEmpty()
    @IsNumber()
    idTipoDocumento: number;

    @ApiPropertyOptional()
    idClaseLicencia: number;

    @ApiPropertyOptional()
    activo:Boolean;

}
