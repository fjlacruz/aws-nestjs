import { Test, TestingModule } from '@nestjs/testing';
import { TiposTramitesController } from './tipos-tramites.controller';

describe('TiposTramitesController', () => {
  let controller: TiposTramitesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TiposTramitesController],
    }).compile();

    controller = module.get<TiposTramitesController>(TiposTramitesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
