import { Controller, Get, Req, Body, Param, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { TiposTramiteDto } from '../DTO/tipos-tramite.dto';
import { TiposTramitesService } from '../services/tipos-tramites.service';
import { AuthGuard } from '@nestjs/passport';
import { ParametrosGeneralDto } from '../DTO/parametros-General.dto';

@ApiTags('Tipos-tramites')
@Controller('tipos-tramites')
@UsePipes(new ValidationPipe())
export class TiposTramitesController {
  constructor(private readonly tiposTramitesService: TiposTramitesService, private readonly authService: AuthService) {}

  @Get('allTiposTramites')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos los Tipos de Tramites' })
  async getConsolidados(@Req() req: any) {
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaAdministracionParametrizacionDeTramites]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.getTiposTramites();
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('getTipoExaminacionesListaNombres')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos los Tipos de Examinaciones, lista de id y nombre' })
  async getTipoExaminacionesListaNombres(@Req() req: any) {
    // let splittedBearerToken = req.headers.authorization.split(' ');
    // let token = splittedBearerToken[1];

    // const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarTiposTramitesLicencias]);

    // const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    // if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.getTipoExaminacionesListaNombres();
      return { data };
    // } else {
    //   return { data: validarPermisos };
    // }
  }

  @Get('tiposTramitesListado')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos los Tipos de Tramites' })
  async tiposTramitesListado(@Req() req: any) {
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaAdministracionParametrizacionDeTramites]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.getTiposTramitesListado();
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('tiposTramitesPaginados')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos los Tipos de Tramites paginados' })
  async tiposTramitesPaginados(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {

      const data = await this.tiposTramitesService.getTiposTramitesPaginados(req, paginacionArgs);
      return { data };

  }

  @Get('getTipoTramite/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve un Tipo de Tramite por Id' })
  async getTipoTramite(@Param('id') id: number, @Req() req: any) {
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaAdministracionParametrizacionDeTramites]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.getTipoTramite(id);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('crearTipoTramite')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que crea un Tipos de Tramites' })
  async crearTipoTramite(@Body() tipoTramite: TiposTramiteDto, @Req() req: any) {
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministracionyParametrizaciondeTramitesCrearNuevoTramite]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.crearTipoTramite(tipoTramite);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('editarTipoTramite')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que edita un Tipo de Tramite' })
  async editarTipoTramite(@Body() tipoTramite: TiposTramiteDto, @Req() req: any) {
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministracionyParametrizaciondeTramitesEditarTramite]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.editarTipoTramite(tipoTramite);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('eliminarTipoTramite/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que elimina un Tipo de Tramite' })
  async eliminarTipoTramite(@Param('id') id: number, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaAdministracionParametrizacionDeTramites]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.eliminarTipoTramite(id);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('cambiarEstadoTipoTramite')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que cambiar el estado de un Tipo de Tramite' })
  async cambiarEstadoTipoTramite(@Body() tipoTramite: TiposTramiteDto, @Req() req: any) {
    const splittedBearerToken = req.headers.authorization.split(' ');
    const token = splittedBearerToken[1];

    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministracionyParametrizaciondeTramitesActivarDesactivarTramite]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tiposTramitesService.cambiarEstadoTipoTramite(tipoTramite);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve un Tipo de Tramites por Id' })
  async getById(@Param('id') id: number) {
    const data = await this.tiposTramitesService.getById(id);
    return { data };
  }

  @Post('parametrosPaginados')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Servicio que devuelve todos los Tipos de Tramites paginados' })
  async parametrosPaginados(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {

      const data = await this.tiposTramitesService.getParametrosPaginados(req, paginacionArgs);
      return { data };

  }  

  @Post('editarParametro')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea una Oficina' })
  async updateOficina(@Body() updateParametroDto: ParametrosGeneralDto, @Req() req: any) {

      const data = await this.tiposTramitesService.updateParametro(req, updateParametroDto);
      return { data };

  }  
}
