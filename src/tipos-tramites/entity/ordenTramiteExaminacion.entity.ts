import { TipoExaminacionesEntity } from "src/resultado-examinacion/entity/TipoExaminaciones.entity";
import { InstitucionesEntity } from "src/tramites/entity/instituciones.entity";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn} from "typeorm";
import { TiposTramiteEntity } from "./tipos-tramite.entity";

@Entity('OrdenTramiteExaminacion')
export class OrdenTramiteExaminacionEntity {

    @PrimaryGeneratedColumn()
    idOrdenTramiteExaminacion: number;

    @Column()
    idInstitucion: number;
    @JoinColumn({ name: 'idInstitucion' })
    @ManyToOne(() => InstitucionesEntity, instituciones => instituciones.ordenTramiteExaminacion)
    readonly instituciones: InstitucionesEntity;

    @Column()
    idTipoTramite: number;
    @JoinColumn({ name: 'idTipoTramite' })
    @ManyToOne(() => TiposTramiteEntity, tiposTramite => tiposTramite.ordenTramiteExaminacion)
    readonly tiposTramite: TiposTramiteEntity;

    @Column()
    idTipoExaminacion: number;
    @JoinColumn({ name: 'idTipoExaminacion' })
    @ManyToOne(() => TipoExaminacionesEntity, tipoExaminaciones => tipoExaminaciones.ordenTramiteExaminacion)
    readonly tipoExaminaciones: TipoExaminacionesEntity;

    @Column()
    idOficina: number;
    @JoinColumn({ name: 'idOficina' })
    @ManyToOne(() => OficinasEntity, oficinas => oficinas.ordenTramiteExaminacion)
    readonly oficinas: OficinasEntity;
    
    @Column()
    orden: number;
}
