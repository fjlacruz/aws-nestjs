import { ApiProperty } from '@nestjs/swagger';
import { ClaseLicenciaRequeridaTipoTramite } from 'src/ClaseLicenciaRequeridaTipoTramite/entity/claseLicenciaRequeridaTipoTramite.entity';
import { FormulariosExaminacionesEntity } from 'src/formularios-examinaciones/entity/formularios-examinaciones.entity';
import { RolTipoTramite } from 'src/rolTipoTramite/entity/rolTipoTramite.entity';
import { TipoDeTramiteClaseLicenciaInstitucionEntity } from 'src/tipo-de-tramite-clase-licencia-institucion/entity/tipo-de-tramte-clase-licencia-institucion.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { OrdenTramiteExaminacionEntity } from './ordenTramiteExaminacion.entity';
import { TramiteDocumentoRequerido } from './tramiteDocumentoRequerido.entity';

@Entity('TiposTramite')
export class TiposTramiteEntity {
  @PrimaryGeneratedColumn()
  idTipoTramite: number;

  @Column()
  Nombre: string;

  @Column()
  Descripcion: string;

  @Column({
    nullable: true,
  })
  activo: boolean;

  @Column({
    nullable: true,
  })
  controlEdad: number;

  @Column({
    nullable: true,
  })
  requiereLicenciaPrevia: boolean;

  @ApiProperty()
  @OneToMany(() => TramitesEntity, tramites => tramites.tiposTramite)
  readonly tramites: TramitesEntity[];

  @ApiProperty()
  @OneToMany(() => ClaseLicenciaRequeridaTipoTramite, licenciaRequerida => licenciaRequerida.tiposTramite)
  readonly licenciaRequerida: ClaseLicenciaRequeridaTipoTramite[];

  @ApiProperty()
  @OneToMany(() => OrdenTramiteExaminacionEntity, ordenTramiteExaminacion => ordenTramiteExaminacion.tiposTramite)
  readonly ordenTramiteExaminacion: OrdenTramiteExaminacionEntity[];

  @ApiProperty()
  @OneToMany(
    () => TipoDeTramiteClaseLicenciaInstitucionEntity,
    tipoTramiteLicenciaInstitucion => tipoTramiteLicenciaInstitucion.tiposTramite
  )
  readonly tipoTramiteLicenciaInstitucion: TipoDeTramiteClaseLicenciaInstitucionEntity[];

  @ApiProperty()
  @OneToMany(() => RolTipoTramite, rolTipoTramite => rolTipoTramite.tiposTramite)
  readonly rolTipoTramite: RolTipoTramite[];

  @ApiProperty()
  @OneToMany(() => TramiteDocumentoRequerido, tramiteDocumentoRequerido => tramiteDocumentoRequerido.tiposTramite)
  readonly tramiteDocumentoRequerido: TramiteDocumentoRequerido[];

  @ApiProperty()
  @OneToMany(() => FormulariosExaminacionesEntity, formulariosExaminaciones => formulariosExaminaciones.tiposTramite)
  readonly formulariosExaminaciones: FormulariosExaminacionesEntity[];
}
