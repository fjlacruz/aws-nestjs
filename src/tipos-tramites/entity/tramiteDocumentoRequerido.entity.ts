import {Entity, Column, PrimaryColumn, ManyToOne, JoinColumn} from "typeorm";
import { TiposTramiteEntity } from "./tipos-tramite.entity";


@Entity('TramiteDocumentoRequerido')
export class TramiteDocumentoRequerido {

    @PrimaryColumn()
    idTramiteDcoumentoReq: number;

    @Column()
    idTipoTramite: number;
    @JoinColumn({ name: 'idTipoTramite' })
    @ManyToOne(() => TiposTramiteEntity, tiposTramite => tiposTramite.tramiteDocumentoRequerido)
    readonly tiposTramite: TiposTramiteEntity[];

    @Column()
    idTipoDocumento: number;

    @Column()
    idClaseLicencia: number;

    @Column()
    activo:Boolean;

}
