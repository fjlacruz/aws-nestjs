import { Test, TestingModule } from '@nestjs/testing';
import { TiposTramitesService } from './tipos-tramites.service';

describe('TiposTramitesService', () => {
  let service: TiposTramitesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TiposTramitesService],
    }).compile();

    service = module.get<TiposTramitesService>(TiposTramitesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
