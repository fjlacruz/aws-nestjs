import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { AuthService } from 'src/auth/auth.service';
import { ClaseLicenciaRequeridaTipoTramiteDTO } from 'src/ClaseLicenciaRequeridaTipoTramite/DTO/claseLicenciaRequeridaTipoTramite.dto';
import { ClaseLicenciaRequeridaTipoTramite } from 'src/ClaseLicenciaRequeridaTipoTramite/entity/claseLicenciaRequeridaTipoTramite.entity';
import { PermisosNombres, tipoRolSuperUsuarioID } from 'src/constantes';
import { ParametrosGeneral } from 'src/parametros/entity/parametros.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { RolTipoTramiteDTO } from 'src/rolTipoTramite/DTO/rolTipoTramite.dto';
import { RolTipoTramite } from 'src/rolTipoTramite/entity/rolTipoTramite.entity';
import { validarRut } from 'src/shared/ValidarRun';
import { TipoDeTramiteClaseLicenciaInstitucionDto } from 'src/tipo-de-tramite-clase-licencia-institucion/dto/tipo-de-tramite-clase-licencia-institucion.dto';
import { TipoDeTramiteClaseLicenciaInstitucionEntity } from 'src/tipo-de-tramite-clase-licencia-institucion/entity/tipo-de-tramte-clase-licencia-institucion.entity';
import { EstadosTramiteEntity } from 'src/tramites/entity/estados-tramite.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { ObjetoListado } from 'src/utils/objetoListado';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { Resultado } from 'src/utils/resultado';
import { getConnection, In, Repository } from 'typeorm';
import { OrdenTramiteExaminacionDTO } from '../DTO/ordenTramiteExaminacion.dto';
import { ParametrosGeneralDto } from '../DTO/parametros-General.dto';
import { TiposTramiteDto } from '../DTO/tipos-tramite.dto';
import { TramiteDocumentoRequeridoDTO } from '../DTO/tramiteDocumentoRequerido.dto';
import { OrdenTramiteExaminacionEntity } from '../entity/ordenTramiteExaminacion.entity';
import { TiposTramiteEntity } from '../entity/tipos-tramite.entity';
import { TramiteDocumentoRequerido } from '../entity/tramiteDocumentoRequerido.entity';

@Injectable()
export class TiposTramitesService {
  constructor(
    @InjectRepository(TiposTramiteEntity) private readonly tiposServiciosRespository: Repository<TiposTramiteEntity>,
    @InjectRepository(TipoExaminacionesEntity) private readonly tipoExaminacionesRespository: Repository<TipoExaminacionesEntity>,
    @InjectRepository(TramitesEntity) private readonly tramitesRepository: Repository<TramitesEntity>,
    @InjectRepository(ParametrosGeneral) private readonly parametrosGeneralRepository: Repository<ParametrosGeneral>,
    private readonly authService : AuthService
  ) {}

  async getTiposTramites() {
    return await this.tiposServiciosRespository.find();
  }

  async getTipoExaminacionesListaNombres() {
    const resultado: Resultado = new Resultado();
    let resultadoTipoExaminaciones: TipoExaminacionesEntity[] = [];
    let resultadosDTO: ObjetoListado[] = [];

    try {
      resultadoTipoExaminaciones = await this.tipoExaminacionesRespository.find();

      if (Array.isArray(resultadoTipoExaminaciones) && resultadoTipoExaminaciones.length) {
        resultadoTipoExaminaciones.forEach(item => {
          let nuevoElemento: ObjetoListado = {
            id: item.idTipoExaminacion,
            Nombre: item.nombreExaminacion,
          };

          resultadosDTO.push(nuevoElemento);
        });
      }

      if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
        resultado.Respuesta = resultadosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tipos de Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Tipos de Tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Tipos de Tramites';
    }

    return resultado;
  }

  async getTiposTramitesListado(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let resultadoTiposTramite: TiposTramiteEntity[] = [];
    let resultadosDTO: TiposTramiteDto[] = [];

    try {
      resultadoTiposTramite = await this.tiposServiciosRespository.find();

      if (Array.isArray(resultadoTiposTramite) && resultadoTiposTramite.length) {
        resultadoTiposTramite.forEach(item => {
          let nuevoElemento: TiposTramiteDto = {
            idTipoTramite: item.idTipoTramite,
            nombre: item.Nombre,
            descripcion: item.Descripcion,
            activo: item.activo,
            controlEdad: item.controlEdad,
            requiereLicenciaPrevia: item.requiereLicenciaPrevia,
          };

          resultadosDTO.push(nuevoElemento);
        });
      }

      if (Array.isArray(resultadosDTO) && resultadosDTO.length) {
        resultado.Respuesta = resultadosDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tipos de Tramite obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Tipos de Tramite';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Tipos de Tramite';
    }

    return resultado;
  }

  async getTiposTramitesPaginados(req:any, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    const orderBy = 'TiposTramites.Nombre';
    let tiposTramitesParseados = new PaginacionDtoParser<TiposTramiteDto>();
    let resultadoTiposTramites: Pagination<TiposTramiteEntity>;
    let filtro = paginacionArgs.filtro;

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaAdministracionParametrizacionDeTramites);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      const qbTiposTramites = await this.tiposServiciosRespository.createQueryBuilder('TiposTramites');

      if (filtro.nombre) {
        qbTiposTramites.andWhere('(lower(unaccent("TiposTramites"."Nombre")) like lower(unaccent(:nombre)))', {
          nombre: `%${filtro.nombre}%`,
        });
      }

      if (filtro.descripcion)
        qbTiposTramites.orWhere('LOWER(TiposTramites.Descripcion) like LOWER(:Descripcion)', { Descripcion: `%${filtro.descripcion}%` });

      qbTiposTramites.orderBy(orderBy, paginacionArgs.orden.ordenarPor);

      if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTiposTramites.orderBy('TiposTramites.Descripcion', 'ASC');
      if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTiposTramites.orderBy('TiposTramites.Descripcion', 'DESC');
      if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTiposTramites.orderBy('TiposTramites.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTiposTramites.orderBy('TiposTramites.Nombre', 'DESC');

      //let a = await qbTiposTramites.getMany();
      resultadoTiposTramites = await paginate<TiposTramiteEntity>(qbTiposTramites, paginacionArgs.paginationOptions);

      if (Array.isArray(resultadoTiposTramites.items) && resultadoTiposTramites.items.length) {
        let tiposTramitesLista: TiposTramiteDto[] = [];

        resultadoTiposTramites.items.forEach(item => {
          let nuevoElemento: TiposTramiteDto = {
            idTipoTramite: item.idTipoTramite,
            nombre: item.Nombre,
            descripcion: item.Descripcion,
            activo: item.activo,
            controlEdad: item.controlEdad,
            requiereLicenciaPrevia: item.requiereLicenciaPrevia,
          };

          tiposTramitesLista.push(nuevoElemento);
        });

        tiposTramitesParseados.items = tiposTramitesLista;
        tiposTramitesParseados.meta = resultadoTiposTramites.meta;
        tiposTramitesParseados.links = resultadoTiposTramites.links;

        resultado.Respuesta = tiposTramitesParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tipos de Tramite obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron Tipos de Tramite';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo Tipos de Tramite';
    }

    return resultado;
  }

  async getTipoTramite(idTipoTramite: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let tipoTramite: TiposTramiteEntity;
    let tipoTramiteDTO: TiposTramiteDto = new TiposTramiteDto();

    try {
      tipoTramite = await this.tiposServiciosRespository.findOne({
        relations: [
          'licenciaRequerida',
          'ordenTramiteExaminacion',
          'tipoTramiteLicenciaInstitucion',
          'rolTipoTramite',
          'tramiteDocumentoRequerido',
        ],
        join: { alias: 'tiposTramite', innerJoinAndSelect: {} },
        where: qb => {
          qb.where('tiposTramite.idTipoTramite = :ID', { ID: idTipoTramite });
        },
      });

      if (tipoTramite) {
        tipoTramiteDTO.idTipoTramite = tipoTramite.idTipoTramite;
        tipoTramiteDTO.nombre = tipoTramite.Nombre;
        tipoTramiteDTO.descripcion = tipoTramite.Descripcion;
        tipoTramiteDTO.activo = tipoTramite.activo;
        tipoTramiteDTO.controlEdad = tipoTramite.controlEdad;
        tipoTramiteDTO.requiereLicenciaPrevia = tipoTramite.requiereLicenciaPrevia;
        tipoTramiteDTO.idsOficinas = [];

        // Licencias Asociadas
        let licencias: TipoDeTramiteClaseLicenciaInstitucionDto[] = [];
        for (const licencia of tipoTramite.tipoTramiteLicenciaInstitucion) {
          licencias.push(licencia);
          // Si en el listado de Oficinas no existe ya este idOficina lo metemos
          if (!tipoTramiteDTO.idsOficinas.find(x => x == licencia.idOficina)) tipoTramiteDTO.idsOficinas.push(licencia.idOficina);
        }
        tipoTramiteDTO.licencias = licencias;

        // Roles Asociados
        let roles: RolTipoTramiteDTO[] = [];
        for (const rol of tipoTramite.rolTipoTramite) {
          roles.push(rol);
        }
        tipoTramiteDTO.rolesAsociados = roles;

        // Licencias Requeridas
        if (tipoTramiteDTO.requiereLicenciaPrevia == true) {
          let licenciasRequeridas: ClaseLicenciaRequeridaTipoTramiteDTO[] = [];
          for (const licenciaReq of tipoTramite.licenciaRequerida) {
            licenciasRequeridas.push(licenciaReq);
          }
          tipoTramiteDTO.licenciasPrevias = licenciasRequeridas;
        }

        // Actividades Asociadas
        let actividadesOrdenadas: OrdenTramiteExaminacionDTO[] = [];
        for (const actividad of tipoTramite.ordenTramiteExaminacion) {
          actividadesOrdenadas.push(actividad);
        }
        tipoTramiteDTO.actividadesAsociadas = actividadesOrdenadas;

        // Documentos Asociadas
        let documentosLicencia: TramiteDocumentoRequeridoDTO[] = [];
        for (const documento of tipoTramite.tramiteDocumentoRequerido) {
          documentosLicencia.push(documento);
        }
        tipoTramiteDTO.documentosRequeridos = documentosLicencia;

        resultado.Respuesta = tipoTramiteDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tipo de Tramite obtenido correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontró el Tipo de Tramite';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo el Tipo de Tramite';
    }

    return resultado;
  }

  async crearTipoTramite(tipoTramite: TiposTramiteDto): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    let nuevotipoTramite: TiposTramiteEntity = new TiposTramiteEntity();

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      nuevotipoTramite.Nombre = tipoTramite.nombre;
      nuevotipoTramite.Descripcion = tipoTramite.descripcion;
      nuevotipoTramite.activo = tipoTramite.activo;
      nuevotipoTramite.controlEdad = tipoTramite.controlEdad != undefined ? tipoTramite.controlEdad : null;
      nuevotipoTramite.requiereLicenciaPrevia = tipoTramite.requiereLicenciaPrevia;

      // Guardamos el TipoTramite en BBDD
      let tipoTramiteCreado = await queryRunner.manager.insert(TiposTramiteEntity, nuevotipoTramite);

      if (tipoTramiteCreado.identifiers.length == 0) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error creando Tipo de Tramite';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Si tiene licencias asociadas, las guardamos en BBDD
      if (tipoTramite.licencias != undefined && tipoTramite.licencias.length > 0) {
        for (let licencia of tipoTramite.licencias) {
          licencia.idTipoTramite = nuevotipoTramite.idTipoTramite;
          for (let IdOficina of tipoTramite.idsOficinas) {
            licencia.idOficina = IdOficina;

            let licenciaCreada = await queryRunner.manager.insert(TipoDeTramiteClaseLicenciaInstitucionEntity, licencia);
            if (licenciaCreada.identifiers.length == 0) {
              resultado.Error = 'Error creando Licencias asociadas';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          }
        }
      }

      // Si tiene roles asociados, los guardamos en BBDD
      if (tipoTramite.rolesAsociados != undefined && tipoTramite.rolesAsociados.length > 0) {
        for (const rolAsociado of tipoTramite.rolesAsociados) {
          rolAsociado.idTipoTramite = nuevotipoTramite.idTipoTramite;
          for (let IdOficina of tipoTramite.idsOficinas) {
            rolAsociado.idOficina = IdOficina;

            let rolTipoTramiteCreado = await queryRunner.manager.insert(RolTipoTramite, rolAsociado);
            if (rolTipoTramiteCreado.identifiers.length == 0) {
              resultado.Error = 'Error creando Roles asociados';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          }
        }
      }

      // Si tiene licencias previas requeridas, los guardamos en BBDD
      if (tipoTramite.licenciasPrevias != undefined && tipoTramite.licenciasPrevias.length > 0) {
        for (const licenciaPrevia of tipoTramite.licenciasPrevias) {
          licenciaPrevia.idTipoTramite = nuevotipoTramite.idTipoTramite;
          for (let IdOficina of tipoTramite.idsOficinas) {
            licenciaPrevia.idOficina = IdOficina;

            let claseLicenciaRequeridaTipoTramiteCreado = await queryRunner.manager.insert(
              ClaseLicenciaRequeridaTipoTramite,
              licenciaPrevia
            );
            if (claseLicenciaRequeridaTipoTramiteCreado.identifiers.length == 0) {
              resultado.Error = 'Error creando Licencias previas requeridas';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          }
        }
      }

      // Si tiene actividades asociadas, los guardamos en BBDD
      if (tipoTramite.actividadesAsociadas != undefined && tipoTramite.actividadesAsociadas.length > 0) {
        // Comprobamos que los numeros del orden no tenga duplicados
        let listadoOrden = tipoTramite.actividadesAsociadas.map(x => x.orden);
        if (listadoOrden.length === new Set(listadoOrden).size) {
          for (const actividadAsociada of tipoTramite.actividadesAsociadas) {
            actividadAsociada.idTipoTramite = nuevotipoTramite.idTipoTramite;
            for (let IdOficina of tipoTramite.idsOficinas) {
              actividadAsociada.idOficina = IdOficina;
              let ordenTramiteExaminacionCreado = await queryRunner.manager.insert(OrdenTramiteExaminacionEntity, actividadAsociada);
              if (ordenTramiteExaminacionCreado.identifiers.length == 0) {
                resultado.Error = 'Error creando actividades asociadas';
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                return resultado;
              }
            }
          }
        } else {
          resultado.Error = 'Error en el Orden de las Actividades';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }
      }

      // Si tiene documentos requeridos, los guardamos en BBDD
      if (tipoTramite.documentosRequeridos != undefined && tipoTramite.documentosRequeridos.length > 0) {
        for (const documentoRequerido of tipoTramite.documentosRequeridos) {
          documentoRequerido.idTipoTramite = nuevotipoTramite.idTipoTramite;
          let tramiteDocumentoRequeridoCreado = await queryRunner.manager.insert(TramiteDocumentoRequerido, documentoRequerido);
          if (tramiteDocumentoRequeridoCreado.identifiers.length == 0) {
            resultado.Error = 'Error creando documentos requeridos';
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            return resultado;
          }
        }
      }

      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Tipo de Tramite creado correctamente';
      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error creando Tipo de Tramite';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async editarTipoTramite(tipoTramite: TiposTramiteDto): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    let licenciasBBDD: TipoDeTramiteClaseLicenciaInstitucionEntity[] = [];
    let rolesBBDD: RolTipoTramite[] = [];
    let licenciasPreviasBBDD: ClaseLicenciaRequeridaTipoTramite[] = [];
    let actividadesBBDD: OrdenTramiteExaminacionEntity[] = [];
    let documentosBBDD: TramiteDocumentoRequerido[] = [];
    let licenciasNuevas: TipoDeTramiteClaseLicenciaInstitucionDto[] = [];
    let rolesNuevos: RolTipoTramiteDTO[] = [];
    let licenciasPreviaNuevas: ClaseLicenciaRequeridaTipoTramiteDTO[] = [];
    let actividadesNuevas: OrdenTramiteExaminacionDTO[] = [];
    let idsDocumentosNuevos: number[] = [];

    // Estados terminales
    // Tramite abandonado               1106
    // Denegacion informada             110X
    // Denegacion informada SML         1107
    // Denegacion informada JPL         1108
    // Recepcion conforme               1302
    // Tramite desistido                1105
    // Tramite Cancelado/Cerrado        1113

    let codigosEstadosTerminales: string[] = ['1106', '110X', '1107', '1108', '1302', '1105', '1113'];

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      // Comprobamos que en caso de deshabilitar, no afecte a ningun tramite en curso
      if (!tipoTramite.activo) {
        let tramitesEnCursoCount = await this.tramitesRepository
          .createQueryBuilder('tramites')
          .innerJoinAndMapOne(
            'tramites.tiposTramite',
            TiposTramiteEntity,
            'TiposTramite',
            'TiposTramite.idTipoTramite = tramites.idTipoTramite'
          )
          .innerJoinAndMapOne(
            'tramites.estadoTramite',
            EstadosTramiteEntity,
            'EstadosTramite',
            'EstadosTramite.idEstadoTramite = tramites.idEstadoTramite'
          )
          .andWhere('EstadosTramite.codigo not in (:...codigos)', { codigos: codigosEstadosTerminales })
          .andWhere('TiposTramite.idTipoTramite = :idTipoTramite', { idTipoTramite: tipoTramite.idTipoTramite })
          .getCount();

        if (tramitesEnCursoCount > 0) {
          resultado.Error = 'No se ha podido deshabilitar el tipo de tramite, ya que se está usando en solicitudes en curso';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }
      }

      // Actualizamos el TipoTramite en BBDD por Id
      let tipoTramiteEditado = await queryRunner.manager.update(TiposTramiteEntity, tipoTramite.idTipoTramite, {
        Nombre: tipoTramite.nombre,
        Descripcion: tipoTramite.descripcion,
        activo: tipoTramite.activo,
        controlEdad: tipoTramite.controlEdad,
        requiereLicenciaPrevia: tipoTramite.requiereLicenciaPrevia,
      });

      if (tipoTramiteEditado.affected == 0) {
        resultado.Error = 'Error al editar el Tipo de Tramite';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // LICENCIAS ASOCIADAS
      // Traemos las licencias que hubiera en la BBDD para este TipoTramite
      licenciasBBDD = await queryRunner.manager.find(TipoDeTramiteClaseLicenciaInstitucionEntity, {
        where: { idTipoTramite: tipoTramite.idTipoTramite },
      });

      // Por cada licencia y por cada idOficina recibidas, creamos una inserción en la tabla correspodiente
      for (const licencia of tipoTramite.licencias) {
        licencia.idTipoTramite = tipoTramite.idTipoTramite;
        let oficinasParaLicencia = licenciasBBDD.filter(x => x.idClaseLicencia == licencia.idClaseLicencia).map(x => x.idOficina);

        for (let IdOficina of tipoTramite.idsOficinas) {
          let licenciaNueva: TipoDeTramiteClaseLicenciaInstitucionDto = new TipoDeTramiteClaseLicenciaInstitucionDto();
          licenciaNueva.idClaseLicencia = licencia.idClaseLicencia;
          licenciaNueva.idOficina = IdOficina;
          licenciasNuevas.push(licenciaNueva);

          if (oficinasParaLicencia.length == 0 || !oficinasParaLicencia.includes(IdOficina)) {
            licencia.idOficina = IdOficina;
            let insercion = await queryRunner.manager.insert(TipoDeTramiteClaseLicenciaInstitucionEntity, licencia);

            if (insercion.identifiers.length == 0) {
              resultado.Error = 'Error al crear la Licencia';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          }
        }
      }

      // Borramos las Licencias antiguas de este TipoTramite
      let idsLicenciasBorrar: number[] = [];
      for (const licenciaAntigua of licenciasBBDD) {
        let borrar = true;
        for (const licenciaNueva of licenciasNuevas) {
          if (licenciaNueva.idOficina == licenciaAntigua.idOficina && licenciaNueva.idClaseLicencia == licenciaAntigua.idClaseLicencia) {
            borrar = false;
          }
        }
        if (borrar) idsLicenciasBorrar.push(licenciaAntigua.idTTramiteClaseLicenciaInstitucion);
      }
      if (idsLicenciasBorrar.length > 0) {
        await queryRunner.manager.delete(TipoDeTramiteClaseLicenciaInstitucionEntity, {
          idTTramiteClaseLicenciaInstitucion: In(idsLicenciasBorrar),
          idTipoTramite: tipoTramite.idTipoTramite,
        });
      }

      // ROLES ASOCIADOS
      // Traemos los roles que hubiera en la BBDD para este TipoTramite
      rolesBBDD = await queryRunner.manager.find(RolTipoTramite, { where: { idTipoTramite: tipoTramite.idTipoTramite } });

      // Por cada licencia recibida, las comprobamos con las de la BBDD por Id
      for (const rol of tipoTramite.rolesAsociados) {
        rol.idTipoTramite = tipoTramite.idTipoTramite;
        let oficinasParaLicencia = rolesBBDD.filter(x => x.idRol == rol.idRol).map(x => x.idOficina);

        for (let IdOficina of tipoTramite.idsOficinas) {
          let rolNuevo: RolTipoTramiteDTO = new RolTipoTramiteDTO();
          rolNuevo.idRol = rol.idRol;
          rolNuevo.idOficina = IdOficina;
          rolesNuevos.push(rolNuevo);

          if (oficinasParaLicencia.length == 0 || !oficinasParaLicencia.includes(IdOficina)) {
            rol.idOficina = IdOficina;
            let insercion = await queryRunner.manager.insert(RolTipoTramite, rol);

            if (insercion.identifiers.length == 0) {
              resultado.Error = 'Error al crear la Licencia';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          }
        }
      }

      // Borramos los Roles antiguos de este TipoTramite
      let idsRolesBorrar: number[] = [];
      for (const rolAntiguo of rolesBBDD) {
        let borrar = true;
        for (const rolNuevo of rolesNuevos) {
          if (rolNuevo.idOficina == rolAntiguo.idOficina && rolNuevo.idRol == rolAntiguo.idRol) {
            borrar = false;
          }
        }
        if (borrar) idsRolesBorrar.push(rolAntiguo.idRolTipoTramite);
      }
      if (idsRolesBorrar.length > 0) {
        await queryRunner.manager.delete(RolTipoTramite, {
          idRolTipoTramite: In(idsRolesBorrar),
          idTipoTramite: tipoTramite.idTipoTramite,
        });
      }

      // LICENCIAS REQUERIDAS
      // Traemos las Licencias Requeridas que hubiera en la BBDD para este TipoTramite
      licenciasPreviasBBDD = await queryRunner.manager.find(ClaseLicenciaRequeridaTipoTramite, {
        where: { idTipoTramite: tipoTramite.idTipoTramite },
      });

      // Por cada Licencia Requerida que se nos pasa, las comprobamos con las de la BBDD por Id
      for (const licenciaPrev of tipoTramite.licenciasPrevias) {
        licenciaPrev.idTipoTramite = tipoTramite.idTipoTramite;
        let oficinasParaLicencia = licenciasPreviasBBDD
          .filter(x => x.idClaseLicencia == licenciaPrev.idClaseLicencia)
          .map(x => x.idOficina);

        for (let IdOficina of tipoTramite.idsOficinas) {
          let licenciaPrevNueva: ClaseLicenciaRequeridaTipoTramiteDTO = new ClaseLicenciaRequeridaTipoTramiteDTO();
          licenciaPrevNueva.idClaseLicencia = licenciaPrev.idClaseLicencia;
          licenciaPrevNueva.idOficina = IdOficina;
          licenciasPreviaNuevas.push(licenciaPrevNueva);

          if (oficinasParaLicencia.length == 0 || !oficinasParaLicencia.includes(IdOficina)) {
            // CREAR
            licenciaPrev.idOficina = IdOficina;
            let insercion = await queryRunner.manager.insert(ClaseLicenciaRequeridaTipoTramite, licenciaPrev);

            if (insercion.identifiers.length == 0) {
              resultado.Error = 'Error al crear la Licencia';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          } else {
            // EDITAR
            let licenciaActualizar = licenciasPreviasBBDD.find(
              x => x.idClaseLicencia == licenciaPrev.idClaseLicencia && x.idOficina == IdOficina
            );
            if (licenciaActualizar.antiguedadAnos != licenciaPrev.antiguedadAnos) {
              await queryRunner.manager.update(ClaseLicenciaRequeridaTipoTramite, licenciaActualizar.idClaseLicenciaRequeridaTipoTramite, {
                antiguedadAnos: licenciaPrev.antiguedadAnos,
              });
            }
          }
        }
      }

      // Borramos las Licencias Requeridas antiguas de este TipoTramite
      let idsLicenciasPreviasBorrar: number[] = [];
      for (const licenciaAntigua of licenciasPreviasBBDD) {
        let borrar = true;
        for (const licenciaNueva of licenciasPreviaNuevas) {
          if (licenciaNueva.idOficina == licenciaAntigua.idOficina && licenciaNueva.idClaseLicencia == licenciaAntigua.idClaseLicencia) {
            borrar = false;
          }
        }
        if (borrar) idsLicenciasPreviasBorrar.push(licenciaAntigua.idClaseLicenciaRequeridaTipoTramite);
      }
      if (idsLicenciasPreviasBorrar.length > 0) {
        await queryRunner.manager.delete(ClaseLicenciaRequeridaTipoTramite, {
          idClaseLicenciaRequeridaTipoTramite: In(idsLicenciasPreviasBorrar),
          idTipoTramite: tipoTramite.idTipoTramite,
        });
      }

      // ACTIVIDADES ASOCIADAS
      // Traemos las Actividades que hubiera en la BBDD para este TipoTramite
      actividadesBBDD = await queryRunner.manager.find(OrdenTramiteExaminacionEntity, {
        where: { idTipoTramite: tipoTramite.idTipoTramite },
      });

      // Por cada Licencia Requerida que se nos pasa, las comprobamos con las de la BBDD por Id
      for (const actividad of tipoTramite.actividadesAsociadas) {
        actividad.idTipoTramite = tipoTramite.idTipoTramite;
        let oficinasParaActividad = actividadesBBDD.filter(x => x.idTipoExaminacion == actividad.idTipoExaminacion).map(x => x.idOficina);

        for (let IdOficina of tipoTramite.idsOficinas) {
          let actividadNueva: OrdenTramiteExaminacionDTO = new OrdenTramiteExaminacionDTO();
          actividadNueva.idTipoExaminacion = actividad.idTipoExaminacion;
          actividadNueva.idOficina = IdOficina;
          actividadesNuevas.push(actividadNueva);

          if (oficinasParaActividad.length == 0 || !oficinasParaActividad.includes(IdOficina)) {
            // CREAR
            actividad.idOficina = IdOficina;
            let insercion = await queryRunner.manager.insert(OrdenTramiteExaminacionEntity, actividad);

            if (insercion.identifiers.length == 0) {
              resultado.Error = 'Error al crear la Licencia';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          } else {
            // EDITAR
            let actividadActualizar = actividadesBBDD.find(x => x.idTipoExaminacion == actividad.idTipoExaminacion && x.idOficina == IdOficina);
            if (actividadActualizar && actividadActualizar.orden != actividad.orden) {
              await queryRunner.manager.update(OrdenTramiteExaminacionEntity, actividadActualizar.idOrdenTramiteExaminacion, {
                orden: actividad.orden,
              });
            }
          }
        }
      }

      // Borramos las Licencias Requeridas antiguas de este TipoTramite
      let idsActividadesBorrar: number[] = [];
      for (const actividadAntigua of actividadesBBDD) {
        let borrar = true;
        for (const actividadNueva of actividadesNuevas) {
          if (
            actividadNueva.idOficina == actividadAntigua.idOficina &&
            actividadNueva.idTipoExaminacion == actividadAntigua.idTipoExaminacion
          ) {
            borrar = false;
          }
        }
        if (borrar) idsActividadesBorrar.push(actividadAntigua.idOrdenTramiteExaminacion);
      }
      if (idsActividadesBorrar.length > 0) {
        await queryRunner.manager.delete(OrdenTramiteExaminacionEntity, {
          idOrdenTramiteExaminacion: In(idsActividadesBorrar),
          idTipoTramite: tipoTramite.idTipoTramite,
        });
      }

      // DOCUMENTOS REQUERIDOS
      // Traemos los Documentos que hubiera en la BBDD para este TipoTramite
      documentosBBDD = await queryRunner.manager.find(TramiteDocumentoRequerido, { where: { idTipoTramite: tipoTramite.idTipoTramite } });

      // Por cada Documentos Requerido que se nos pasa, los comprobamos con las de la BBDD por Id
      for (const documento of tipoTramite.documentosRequeridos) {
        idsDocumentosNuevos.push(documento.idTramiteDcoumentoReq);
        // Si ya existe en la BBDD lo editamos, si no, lo creamos
        if (documentosBBDD.find(x => x.idTramiteDcoumentoReq == documento.idTramiteDcoumentoReq)) {
          let licenciaPreviaEditada = await queryRunner.manager.update(TramiteDocumentoRequerido, documento, {
            idTipoDocumento: documento.idTipoDocumento,
            idClaseLicencia: documento.idClaseLicencia,
            activo: documento.activo,
          });

          if (licenciaPreviaEditada.affected == 0) {
            resultado.Error = 'Error al editar el documento';
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            return resultado;
          }
        } else {
          let licenciaPreviaCreada = await queryRunner.manager.insert(TramiteDocumentoRequerido, documento);

          if (licenciaPreviaCreada.identifiers.length == 0) {
            resultado.Error = 'Error al crear el documento';
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            return resultado;
          }
        }
      }

      // Borramos los Documentos Requeridos antiguos de este TipoTramite
      let idsDocumentosBorrar: number[] = [];
      for (const documento of documentosBBDD) {
        if (!idsDocumentosNuevos.find(x => x == documento.idTramiteDcoumentoReq)) {
          idsDocumentosBorrar.push(documento.idTramiteDcoumentoReq);
        }
      }
      if (idsDocumentosBorrar.length > 0) {
        await queryRunner.manager.delete(TramiteDocumentoRequerido, {
          idTramiteDcoumentoReq: In(idsDocumentosBorrar),
          idTipoTramite: tipoTramite.idTipoTramite,
        });
      }

      //Finalmente si no han habido problemas, realizamos el commit y devolvemos el resultado
      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Tipo de Tramite editado correctamente';
      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error editado Tipo de Tramite';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async cambiarEstadoTipoTramite(tipoTramite: TiposTramiteDto): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    // Estados terminales
    // Tramite abandonado               1106
    // Denegacion informada             110X
    // Denegacion informada SML         1107
    // Denegacion informada JPL         1108
    // Recepcion conforme               1302
    // Tramite desistido                1105
    // Tramite Cancelado/Cerrado        1113

    let codigosEstadosTerminales: string[] = ['1106', '110X', '1107', '1108', '1302', '1105', '1113'];

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      // Comprobamos que en caso de deshabilitar, no afecte a ningun tramite en curso
      if (!tipoTramite.activo) {
        let tramitesEnCursoCount = await this.tramitesRepository
          .createQueryBuilder('tramites')
          .innerJoinAndMapOne(
            'tramites.tiposTramite',
            TiposTramiteEntity,
            'TiposTramite',
            'TiposTramite.idTipoTramite = tramites.idTipoTramite'
          )
          .innerJoinAndMapOne(
            'tramites.estadoTramite',
            EstadosTramiteEntity,
            'EstadosTramite',
            'EstadosTramite.idEstadoTramite = tramites.idEstadoTramite'
          )
          .andWhere('EstadosTramite.codigo not in (:...codigos)', { codigos: codigosEstadosTerminales })
          .andWhere('TiposTramite.idTipoTramite = :idTipoTramite', { idTipoTramite: tipoTramite.idTipoTramite })
          .getCount();

        if (tramitesEnCursoCount > 0) {
          resultado.Error = 'No se ha podido deshabilitar el tipo de tramite, ya que se está usando en solicitudes en curso';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }
      }

      // Actualizamos el estado del TipoTramite en BBDD
      let tipoCreado = await queryRunner.manager.update(TiposTramiteEntity, tipoTramite.idTipoTramite, { activo: tipoTramite.activo });

      if (tipoCreado.affected > 0) {
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Actualizado el estado del Tipo de trámite correctamente';
        await queryRunner.commitTransaction();
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error cambiando el estado del Tipo de Trámite';
        await queryRunner.rollbackTransaction();
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error cambiando el estado del Tipo de Trámite';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async eliminarTipoTramite(idTipoTramite: number): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    let tipoTramiteBorrar = await this.tiposServiciosRespository.findOne({
      relations: ['tramites', 'formulariosExaminaciones'],
      join: { alias: 'tiposTramite', innerJoinAndSelect: {} },
      where: qb => {
        qb.where('tiposTramite.idTipoTramite = :ID', { ID: idTipoTramite });
      },
    });

    if (tipoTramiteBorrar) {
      if (tipoTramiteBorrar.tramites.length > 0 || tipoTramiteBorrar.formulariosExaminaciones.length > 0) {
        resultado.Error = 'Error. El Tipo de Tramite esta siendo usado.';
        return resultado;
      }
    }

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      // Eliminamos las Licencias Asociadas
      await queryRunner.manager.delete(TipoDeTramiteClaseLicenciaInstitucionEntity, { idTipoTramite: idTipoTramite });
      // Eliminamos los Roles Asociados
      await queryRunner.manager.delete(RolTipoTramite, { idTipoTramite: idTipoTramite });
      // Eliminamos las Licencias requeridas
      await queryRunner.manager.delete(ClaseLicenciaRequeridaTipoTramite, { idTipoTramite: idTipoTramite });
      // Eliminamos las Actividades Asociadas
      await queryRunner.manager.delete(OrdenTramiteExaminacionEntity, { idTipoTramite: idTipoTramite });
      // Eliminamos los Docimentos Requeridos
      await queryRunner.manager.delete(TramiteDocumentoRequerido, { idTipoTramite: idTipoTramite });

      // Eliminamos el TipoTramite en BBDD por Id
      let tipoTramiteEditado = await queryRunner.manager.delete(TiposTramiteEntity, idTipoTramite);

      if (tipoTramiteEditado.affected == 0) {
        resultado.Error = 'Error al eliminar el Tipo de Tramite';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      //Finalmente si no han habido problemas, realizamos el commit y devolvemos el resultado
      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Tipo de Tramite eliminado correctamente';
      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error al eliminar el Tipo de Tramite';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async getById(id: number) {
    return await this.tiposServiciosRespository.findOne(id);
  }

  async getParametrosPaginados(req:any, paginacionArgs: PaginacionArgs): Promise<Resultado> {

    const resultado: Resultado = new Resultado();
    const orderBy = 'ParametrosGeneral.Param';

    let parametrosParseados = new PaginacionDtoParser<ParametrosGeneralDto>();
    let resultadoParametrosGeneral: Pagination<ParametrosGeneral>;
    let filtro = paginacionArgs.filtro;

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministracionyParametrizaciondeTramitesBuscarParametros);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      const qbParametrosGeneral = await this.parametrosGeneralRepository.createQueryBuilder('ParametrosGeneral');

      if (filtro.nombreParametro) {
        qbParametrosGeneral.andWhere('(lower(unaccent("ParametrosGeneral"."Param")) like lower(unaccent(:nombre)))', {
          nombre: `%${filtro.nombreParametro}%`,
        });
      }

      if (filtro.descripcionParametro)
        qbParametrosGeneral.andWhere('LOWER(ParametrosGeneral.Descripcion) like LOWER(:Descripcion)', { Descripcion: `%${filtro.descripcionParametro}%` });

      qbParametrosGeneral.orderBy(orderBy, paginacionArgs.orden.ordenarPor);

      if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbParametrosGeneral.orderBy('ParametrosGeneral.Descripcion', 'ASC');
      if (paginacionArgs.orden.orden === 'descripcion' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbParametrosGeneral.orderBy('ParametrosGeneral.Descripcion', 'DESC');
      if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbParametrosGeneral.orderBy('ParametrosGeneral.Param', 'ASC');
      if (paginacionArgs.orden.orden === 'nombre' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbParametrosGeneral.orderBy('ParametrosGeneral.Param', 'DESC');

      //let a = await qbTiposTramites.getMany();
      resultadoParametrosGeneral = await paginate<ParametrosGeneral>(qbParametrosGeneral, paginacionArgs.paginationOptions);

      if (Array.isArray(resultadoParametrosGeneral.items) && resultadoParametrosGeneral.items.length) {
        let parametrosGeneralLista: ParametrosGeneralDto[] = [];

        resultadoParametrosGeneral.items.forEach(item => {
          let nuevoElemento: ParametrosGeneralDto = {
            idParametrosGeneral: item.idParametro,
            nombre: item.Param,
            descripcion: item.Descripcion,
            tipoDato: item.tipoDato,
            valueString: item.Value,
            valueNumber: item.ValueInt,
            valueBoolean: item.ValueBoolean
          };

          parametrosGeneralLista.push(nuevoElemento);
        });

        parametrosParseados.items = parametrosGeneralLista;
        parametrosParseados.meta = resultadoParametrosGeneral.meta;
        parametrosParseados.links = resultadoParametrosGeneral.links;

        resultado.Respuesta = parametrosParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Parámetros obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron parámetros';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo parámetros';
    }

    return resultado;
  }

  async updateParametro(req:any, updateParametroDto: ParametrosGeneralDto): Promise<Resultado> {
    //Inicializamos la respuesta
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {

      // Se procede a validar la información que nos viene en el dto y por el id
      if(!this.validarEntradaParametroGeneralDto(updateParametroDto)){
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se puede actualizar el parámetro, datos facilitados incorrectos.';

        return resultado;
      }

       // Comprobamos permisos	
       let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministracionyParametrizaciondeTramitesEditarParametros);	
    	
       // En caso de que el usuario no sea correcto	
       if (!usuarioValidado.ResultadoOperacion) {	
         return usuarioValidado;	
       }	 



      //creamos el objeto que vamos a actualizar
      let parametrosGeneralUpdate: ParametrosGeneral = new ParametrosGeneral();

      // Se mapean los atributos, quitan los que no se usan en la entidad original
      let idParametro : number = updateParametroDto.idParametrosGeneral;

      //parametrosGeneralUpdate.idParametro = updateParametroDto.idParametrosGeneral;
      parametrosGeneralUpdate.Descripcion = updateParametroDto.descripcion;
      parametrosGeneralUpdate.Param = updateParametroDto.nombre;
      parametrosGeneralUpdate.Value = updateParametroDto.valueString;
      parametrosGeneralUpdate.ValueBoolean = updateParametroDto.valueBoolean;
      parametrosGeneralUpdate.ValueInt = updateParametroDto.valueNumber;
      parametrosGeneralUpdate.tipoDato = updateParametroDto.tipoDato;


      //Guardamos la oficins
      var modificacion = await queryRunner.manager.update(ParametrosGeneral, idParametro, parametrosGeneralUpdate);

      //Comprobamos que se ha realizado la operacion
      if (modificacion.affected < 0) {
        resultado.Error = 'Error al editar el parámetro, puede que este ya no exista';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      await queryRunner.commitTransaction();
      resultado.Mensaje = 'Parámetro editado correctamente';
      resultado.ResultadoOperacion = true;

    } catch (error) {
      //capturamos el error
      await queryRunner.rollbackTransaction();
      console.log(error);
      resultado.Error = 'Error al editar el parámetro, por favor contacte con el administrador.';
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }  


  validarEntradaParametroGeneralDto(updateParametroDto : Partial<ParametrosGeneralDto>){
    // Primero validamos que vengan datos obligatorios
    if(!updateParametroDto || !updateParametroDto.idParametrosGeneral || !updateParametroDto.nombre || !updateParametroDto.descripcion || !updateParametroDto.tipoDato){
      return false;
    }
    // Segundo validamos que vengan datos obligatorios según el tipo de dato
    else{
      switch(updateParametroDto.tipoDato){
        case 1:{if(updateParametroDto.valueNumber && !updateParametroDto.valueBoolean){return true}else{return false;}};break; // Caso tipo dato  
        case 2:{if(updateParametroDto.valueString && !updateParametroDto.valueNumber && !updateParametroDto.valueBoolean){return true}else{return false;}};break; // Caso tipo dato
        case 3:{if(updateParametroDto.valueBoolean != undefined && !updateParametroDto.valueNumber){return true}else{return false;}};break; // Caso tipo dato
        default: return false;
      }
    }


  }

}
