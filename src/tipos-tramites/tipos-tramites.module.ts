import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { ParametrosGeneral } from 'src/parametros/entity/parametros.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { User } from 'src/users/entity/user.entity';
import { TiposTramitesController } from './controller/tipos-tramites.controller';
import { TiposTramiteEntity } from './entity/tipos-tramite.entity';
import { TiposTramitesService } from './services/tipos-tramites.service';

@Module({
  imports: 
  [
    TypeOrmModule.forFeature([TiposTramiteEntity, TipoExaminacionesEntity,User, RolesUsuarios, TramitesEntity, ParametrosGeneral])
  ],
  controllers: [TiposTramitesController],
  exports: [TiposTramitesService],
  providers: [TiposTramitesService, AuthService]
})
export class TiposTramitesModule { }
