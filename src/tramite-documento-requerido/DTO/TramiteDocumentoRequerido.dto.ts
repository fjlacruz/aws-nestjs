import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class TramiteDocumentoRequeridoDTO {

    @ApiPropertyOptional()
    idTipoTramite: number;

    @ApiPropertyOptional()
    idTipoDocumento: number;

    @ApiPropertyOptional()
    idClaseLicencia: number;
}

export class TramiteDocumentoRequeridoDto 
{
    @ApiProperty()
    idTipoTramite: number;

    @ApiProperty()
    idClaseLicencia: number;
}
