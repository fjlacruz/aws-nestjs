import { Test, TestingModule } from '@nestjs/testing';
import { TramiteDocumentoRequeridoController } from './tramite-documento-requerido.controller';

describe('TramiteDocumentoRequeridoController', () => {
  let controller: TramiteDocumentoRequeridoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TramiteDocumentoRequeridoController],
    }).compile();

    controller = module.get<TramiteDocumentoRequeridoController>(TramiteDocumentoRequeridoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
