import { Controller, Query } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { TramiteDocumentoRequeridoDto, TramiteDocumentoRequeridoDTO } from '../DTO/TramiteDocumentoRequerido.dto';
import { TramiteDocumentoRequeridoService } from '../services/tramite-documento-requerido.service';


@ApiTags('Tramite-documento-requerido')
@Controller('tramite-documento-requerido')
export class TramiteDocumentoRequeridoController {

    constructor(private readonly tramiteDocumentoRequeridoService: TramiteDocumentoRequeridoService) { }

    @Post('validaTramiteDocumentoRequerido')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que valida Tramite Documento Requerido' })
    async validaTramiteDocumentoRequerid(
        @Body() tramiteDocumentoRequeridoDTO: TramiteDocumentoRequeridoDTO) {

        if(tramiteDocumentoRequeridoDTO.idClaseLicencia==null){            
            return  await this.tramiteDocumentoRequeridoService.validarTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumento(tramiteDocumentoRequeridoDTO.idTipoTramite, tramiteDocumentoRequeridoDTO.idTipoDocumento);
        }else{
            return  await this.tramiteDocumentoRequeridoService.validarTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumentoAndIdClaseLicencia(tramiteDocumentoRequeridoDTO.idTipoTramite, tramiteDocumentoRequeridoDTO.idTipoDocumento, tramiteDocumentoRequeridoDTO.idClaseLicencia);
        }

    }

    
    @Post('getTramiteDocumentoRequerido')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que retorna los Documento Requerido de un Tramite' })
    async getTramiteDocumentoRequerid(
        @Body() tramiteDocumentoRequeridoDTO: TramiteDocumentoRequeridoDTO) {

        if(tramiteDocumentoRequeridoDTO.idClaseLicencia==null){            
            return  await this.tramiteDocumentoRequeridoService.getTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumento(tramiteDocumentoRequeridoDTO.idTipoTramite, tramiteDocumentoRequeridoDTO.idTipoDocumento);
        }else{
            return  await this.tramiteDocumentoRequeridoService.getTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumentoAndIdClaseLicencia(tramiteDocumentoRequeridoDTO.idTipoTramite, tramiteDocumentoRequeridoDTO.idTipoDocumento, tramiteDocumentoRequeridoDTO.idClaseLicencia);
        }

    }

    @Get('getTramiteDocumentoRequerido/:idTipoTramite/:idClaseLicencia')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que retorna los Documento Requerido de un Tramite' })
    async getTramiteDocumentoRequerido( @Param() dto: TramiteDocumentoRequeridoDto )
    {
        const data = await this.tramiteDocumentoRequeridoService.getByTipoTramiteAndClaseLicencia( dto );
        return data;
    }

    @Get('getTramiteDocumentoRequeridoBySolicitud/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que retorna los Documento Requerido de una Solicitud' })
    async getSolicitudDocumentoRequerido( @Param('id') id: number )
    {
        const data = await this.tramiteDocumentoRequeridoService.getBySolicitud( Number( id ) );
        return data;
    }
}
