import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('TiposDocumento')
export class TiposDocumentoEntity {
    
    @PrimaryColumn()
    idTipoDocumento:number;

    @Column()
    Nombre:String;

    @Column()
    Descripcion:String;

    @Column()
    created_at:Date;

    @Column()
    update_at:Date;

    @Column()
    idUsuario:number
}
