import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";


@Entity('TramiteDocumentoRequerido')
export class TramiteDocumentoRequeridoEntity {

    @PrimaryColumn()
    idTramiteDcoumentoReq: number;

    @Column()
    idTipoTramite: number;

    @Column()
    idTipoDocumento: number;

    @Column()
    idClaseLicencia: number;

    @Column()
    activo:Boolean;

}
