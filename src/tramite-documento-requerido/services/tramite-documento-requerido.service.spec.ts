import { Test, TestingModule } from '@nestjs/testing';
import { TramiteDocumentoRequeridoService } from './tramite-documento-requerido.service';

describe('TramiteDocumentoRequeridoService', () => {
  let service: TramiteDocumentoRequeridoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TramiteDocumentoRequeridoService],
    }).compile();

    service = module.get<TramiteDocumentoRequeridoService>(TramiteDocumentoRequeridoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
