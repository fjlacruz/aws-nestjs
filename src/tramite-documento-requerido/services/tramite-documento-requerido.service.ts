import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { Repository, UpdateResult } from 'typeorm';
import { getConnection } from "typeorm";
import { TramiteDocumentoRequeridoDto } from '../DTO/TramiteDocumentoRequerido.dto';
import { TiposDocumentoEntity } from '../entity/TiposDocumento.entity';
import { TramiteDocumentoRequeridoEntity } from '../entity/TramiteDocumentoRequerido.entity';

DocumentosTramitesEntity

@Injectable()
export class TramiteDocumentoRequeridoService {

    
    constructor
    (
      @InjectRepository(TramiteDocumentoRequeridoEntity) private readonly tramiteDocumentoRequeridoEntity: Repository<TramiteDocumentoRequeridoEntity>,
    )
    {}

    async validarTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumento(idTipoTramite :number, idTipoDocumento: number) {

        const data = await getConnection() .createQueryBuilder() 
        .select("TramiteDocumentoRequerido") 
        .from(TramiteDocumentoRequeridoEntity, "TramiteDocumentoRequerido") 
        .where("TramiteDocumentoRequerido.idTipoTramite = :idTipoTramite", { idTipoTramite: idTipoTramite })
        .andWhere("TramiteDocumentoRequerido.idTipoDocumento = :idTipoDocumento", {idTipoDocumento: idTipoDocumento})
        .getMany();
  
        if (!data)throw new NotFoundException("no hay registros en la tabla");

        for (var i in data) {
            if(data[i].activo == false)
            return false;
        }    
  
        return true;
  
      }

      async validarTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumentoAndIdClaseLicencia(idTipoTramite :number, idTipoDocumento: number, idClaseLicencia: number) {

        const data = await getConnection() .createQueryBuilder() 
        .select("TramiteDocumentoRequerido") 
        .from(TramiteDocumentoRequeridoEntity, "TramiteDocumentoRequerido") 
        .where("TramiteDocumentoRequerido.idTipoTramite = :idTipoTramite", { idTipoTramite: idTipoTramite })
        .andWhere("TramiteDocumentoRequerido.idTipoDocumento = :idTipoDocumento", {idTipoDocumento: idTipoDocumento})
        .andWhere("TramiteDocumentoRequerido.idClaseLicencia = :idClaseLicencia", {idClaseLicencia: idClaseLicencia})
        .getMany();
  
        if (!data)throw new NotFoundException("no hay registros en la tabla");

        for (var i in data) {
            if(data[i].activo == false)
            return false;
        }    
  
        return true;
        
      }

      async getTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumento(idTipoTramite :number, idTipoDocumento: number) {

        return this.tramiteDocumentoRequeridoEntity.createQueryBuilder('TramiteDocumentoRequerido')          
        .innerJoinAndMapMany('TramiteDocumentoRequerido.idTipoDocumento', TiposDocumentoEntity, 'doc', 'TramiteDocumentoRequerido.idTipoDocumento = doc.idTipoDocumento',)        
        .where("TramiteDocumentoRequerido.idTipoTramite = :idTipoTramite", { idTipoTramite: idTipoTramite })
        .andWhere("TramiteDocumentoRequerido.idTipoDocumento = :idTipoDocumento", {idTipoDocumento: idTipoDocumento})
        .andWhere("TramiteDocumentoRequerido.activo = true") 
        .getMany();
  
      }

      async getTramiteDocumentoRequeridoByIdTipoTramiteAndIdTipoDocumentoAndIdClaseLicencia(idTipoTramite :number, idTipoDocumento: number, idClaseLicencia: number) {

        return this.tramiteDocumentoRequeridoEntity.createQueryBuilder('TramiteDocumentoRequerido')          
        .innerJoinAndMapMany('TramiteDocumentoRequerido.idTipoDocumento', TiposDocumentoEntity, 'doc', 'TramiteDocumentoRequerido.idTipoDocumento = doc.idTipoDocumento',)        
        .where("TramiteDocumentoRequerido.idTipoTramite = :idTipoTramite", { idTipoTramite: idTipoTramite })
        .andWhere("TramiteDocumentoRequerido.idTipoDocumento = :idTipoDocumento", {idTipoDocumento: idTipoDocumento})
        .andWhere("TramiteDocumentoRequerido.idClaseLicencia = :idClaseLicencia", {idClaseLicencia: idClaseLicencia})
        .andWhere("TramiteDocumentoRequerido.activo = true") 
        .getMany();
  
      }
      async getByTipoTramiteAndClaseLicencia( dto: TramiteDocumentoRequeridoDto )
      {
        const data = await this.tramiteDocumentoRequeridoEntity
                    .createQueryBuilder( "requerido" )
                    .select(    "requerido.idTipoDocumento", "idTipoDocumento" )
                    .addSelect( "tipo.Nombre",               "Nombre" )
                    .innerJoin( "TiposDocumento",            "tipo",  "requerido.idTipoDocumento = tipo.idTipoDocumento" )
                    .where(     "requerido.idTipoTramite    = :idTipoTramite",   { idTipoTramite: dto.idTipoTramite } )
                    .andWhere(  "requerido.idClaseLicencia  = :idClaseLicencia", { idClaseLicencia: dto.idClaseLicencia } )
                    .andWhere(  "requerido.activo           = :activo",          { activo: true } )
                    .getRawMany();

        return data;
      }

      async getBySolicitud( idSolicitud: number )
      {
        const activo = true;
        const data   = await getConnection()
                      .createQueryBuilder()
                      .select(    "requerido.idTipoDocumento", "idTipoDocumento" )
                      .addSelect( "tipo.Nombre",               "Nombre" )
                      .from(      "Tramites", "tramite" )
                      .innerJoin( "TramitesClaseLicencia",      "licencia",  "tramite.idTramite = licencia.idTramite" )
                      .innerJoin( "TramiteDocumentoRequerido",  "requerido", "( licencia.idClaseLicencia = requerido.idClaseLicencia OR requerido.idClaseLicencia IS NULL )" )
                      .innerJoin( "TiposDocumento",             "tipo",     "requerido.idTipoDocumento = tipo.idTipoDocumento" )
                      .where(     "tramite.idSolicitud        = :idSolicitud", { idSolicitud } )
                      .andWhere(  "requerido.activo           = :activo",      { activo } )
                      .andWhere(  "requerido.idTipoTramite    = tramite.idTipoTramite" )
                      .groupBy(   "requerido.idTipoDocumento, requerido.activo, tipo.Nombre" )
                      .getRawMany();

        return data;
      }
}
