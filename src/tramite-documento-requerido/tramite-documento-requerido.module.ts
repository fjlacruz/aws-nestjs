import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TramiteDocumentoRequeridoController } from './controller/tramite-documento-requerido.controller';
import { TramiteDocumentoRequeridoEntity } from './entity/TramiteDocumentoRequerido.entity';
import { TramiteDocumentoRequeridoService } from './services/tramite-documento-requerido.service';

@Module({
  imports: [TypeOrmModule.forFeature([TramiteDocumentoRequeridoEntity])],
  providers: [TramiteDocumentoRequeridoService],
  exports: [TramiteDocumentoRequeridoService],
  controllers: [TramiteDocumentoRequeridoController]
})
export class TramiteDocumentoRequeridoModule {}
