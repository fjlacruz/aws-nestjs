import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateTramiteLicenciaDto {

    @ApiPropertyOptional()
    idTramiteClaseLicencia:number;
    @ApiPropertyOptional()
    idTramite:number;
    @ApiPropertyOptional()
    idClaseLicencia:number;
    
}