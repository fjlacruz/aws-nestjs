import { ApiPropertyOptional } from "@nestjs/swagger";

export class FotosTramiteLicenciaDto {
    @ApiPropertyOptional()
    fotoFrontalLicencia:string;
    @ApiPropertyOptional()
    fotoTraseraLicencia:string;
    @ApiPropertyOptional()
    codigoQR:string;
    @ApiPropertyOptional()
    letrasFolio:string;
    @ApiPropertyOptional()
    folio:string;
    @ApiPropertyOptional()
    fotoF8LicenciaBase64:string;
    @ApiPropertyOptional()
    fotoF8LicenciaNegativoBase64:string;
    @ApiPropertyOptional()
    fotoAnversoLicenciaBase64:string;
    @ApiPropertyOptional()
    fotoReversoLicenciaBase64:string;        
}