import { ApiPropertyOptional } from "@nestjs/swagger";

export class ModifyMotivoAdicional {

    @ApiPropertyOptional()
    idTramiteClaseLicencia: number;

    @ApiPropertyOptional()
    idTramite: number;

    @ApiPropertyOptional()
    MotivoAdicional: string;

    @ApiPropertyOptional()
    nombreArchivo: string;

    @ApiPropertyOptional()
    archivo: string;
}