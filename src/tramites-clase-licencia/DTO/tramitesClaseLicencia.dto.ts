import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocumentosTramitesDTO } from "src/documentos-tramites/DTO/documentos-tramites.dto";

export class TramitesClaseLicenciaDto {
    @ApiPropertyOptional()
    idTramiteClaseLicencia: number;

    @ApiPropertyOptional()
    idTramite: number;

    @ApiPropertyOptional()
    idClaseLicencia: number;

    @ApiPropertyOptional()
    fechaProximoControl: Date;

    @ApiPropertyOptional()
    folio: number;

    @ApiPropertyOptional()
    fechaRecepcionConforme: Date;

    @ApiPropertyOptional()
    idMotivoDenegacion: number;

    @ApiPropertyOptional()
    fechaReprobacion: Date;

    @ApiPropertyOptional()
    tipoDenegacion: string;

    @ApiPropertyOptional()
    plazoDenegacion: string;

    @ApiPropertyOptional()
    motivoAdicional: string;

    @ApiPropertyOptional()
    documentoTramite: DocumentosTramitesDTO;
}