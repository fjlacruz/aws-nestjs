import { Test, TestingModule } from '@nestjs/testing';
import { TramitesClaseLicenciaController } from './tramites-clase-licencia.controller';

describe('TramitesClaseLicenciaController', () => {
  let controller: TramitesClaseLicenciaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TramitesClaseLicenciaController],
    }).compile();

    controller = module.get<TramitesClaseLicenciaController>(TramitesClaseLicenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
