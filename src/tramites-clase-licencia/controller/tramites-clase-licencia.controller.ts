import { Body, Controller, Get, Param, Patch, Post, Put, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags, ApiBasicAuth } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { ModifyMotivoAdicional } from '../DTO/modifyMotivoAdicional.dto';
import { TramitesClaseLicenciaService } from '../services/tramites-clase-licencia.service';
import { CreateTramiteLicenciaDto } from '../DTO/createTramiteLicencia.dto';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';


@ApiTags('Tramites-clase-licencia')
@Controller('tramites-clase-licencia')
export class TramitesClaseLicenciaController {

    constructor(private readonly tramitesClaseLicenciaService: TramitesClaseLicenciaService, private readonly authService: AuthService) { }

    @Get('tramiteLicencia')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas los Tramites clases licencias' })
    async getComunas() {
        const data = await this.tramitesClaseLicenciaService.getTramitesClaseLicencias();
        return { data };
    }

    @Get('tramiteLicenciaById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve un Tramites clases licencias por Id' })
    async comunaById(@Param('id') id: number) {
        const data = await this.tramitesClaseLicenciaService.getTramitesClaseLicencia(id);
        return { data };
    }

    @Patch('tramiteLicenciaUpdate/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que actualiza datos de un Tramites clases licencias' })
    async comunaUpdate(@Param('id') id: number, @Body() data: CreateTramiteLicenciaDto) {
        return await this.tramitesClaseLicenciaService.update(id, data);
    }

    @Post('crearTramiteLicencia')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea una nuevo Tramites clases licencias' })
    async addComuna(
        @Body() createTramiteLicenciaDto: CreateTramiteLicenciaDto) {
        const data = await this.tramitesClaseLicenciaService.create(createTramiteLicenciaDto);
        return {data};
    }

    @Get('getTramiteClaseLicenciaById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Endpoint encargado de traer Trámites Clase Licencia por id de trámite' })
    async getTramiteClaseLicenciaById(@Param('id') id: number, @Req() req: any) {

        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaOtorgamientosDenegaciones
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const data = await this.tramitesClaseLicenciaService.getTramiteClaseLicenciaById(id);
            return { data };

        } else {

            return { data: validarPermisos };

        }

    }

    @Patch('modifyMotivoAdicionalById')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Endpoint encargado de modificar motivo adicional de trámite clase licencia por id Trámite Clase Licencia' })
    async modifyMotivoAdicionalById(@Body() modifyMotivoAdicional: ModifyMotivoAdicional, @Req() req: any) {

        let splittedBearerToken = req.headers.authorization.split(" ");
        let token = splittedBearerToken[1];

        let tokenPermisos = new TokenPermisoDto(token, [
            PermisosNombres.VisualizarListaOtorgamientosDenegaciones
        ]);

        let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

        if (validarPermisos.ResultadoOperacion) {

            const rolesUsuarios : RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];
            const idUsuarioConectado : number = rolesUsuarios[0].idUsuario;
            
            const data = await this.tramitesClaseLicenciaService.modifyMotivoAdicionalById(modifyMotivoAdicional, idUsuarioConectado);
            return { data };

        } else {

            return { data: validarPermisos };

        }

    }

}
