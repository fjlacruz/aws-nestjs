import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';
import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, ManyToOne, JoinColumn, OneToOne } from 'typeorm';

@Entity('TramitesClaseLicencia')
export class TramitesClaseLicencia {
  @PrimaryColumn()
  idTramiteClaseLicencia: number;

  @Column()
  idTramite: number;

  @OneToOne(() => TramitesEntity, tramite => tramite.tramitesClaseLicencia)
  @JoinColumn({ name: 'idTramite' })
  readonly tramite: TramitesEntity;

  @Column()
  idClaseLicencia: number;
  @OneToOne(() => ClasesLicenciasEntity, clasesLicencias => clasesLicencias.tramitesClaseLicencia)
  @JoinColumn({ name: 'idClaseLicencia' })
  readonly clasesLicencias: ClasesLicenciasEntity;

  // @Column()
  // fechaProximoControl: Date;

  // @Column()
  // fechaRecepcionConforme: Date;

  // @Column({
  //     type: 'bytea',
  //     nullable: true
  // })
  // fotoLicenciaFrontal?: Buffer;

  // @Column({
  //     type: 'bytea',
  //     nullable: true
  // })
  // fotoLicenciaTrasera?: Buffer;

  // @Column()
  // folio: number;

  // @Column()
  // MotivoDenegacion: string;

  // @Column()
  // FechaReprobacion: Date;

  // @Column()
  // TipoDenegacion: string;

  // @Column()
  // PlazoDenegacion: string;

  // @Column({
  //     type: 'varchar',
  //     length: 1000
  // })
  // MotivoAdicional: string;
}
