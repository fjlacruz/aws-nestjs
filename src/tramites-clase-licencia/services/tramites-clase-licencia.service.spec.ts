import { Test, TestingModule } from '@nestjs/testing';
import { TramitesClaseLicenciaService } from './tramites-clase-licencia.service';

describe('TramitesClaseLicenciaService', () => {
  let service: TramitesClaseLicenciaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TramitesClaseLicenciaService],
    }).compile();

    service = module.get<TramitesClaseLicenciaService>(TramitesClaseLicenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
