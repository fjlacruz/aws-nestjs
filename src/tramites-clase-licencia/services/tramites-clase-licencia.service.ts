import { InjectRepository } from '@nestjs/typeorm';
import { DocumentosTramitesDTO } from 'src/documentos-tramites/DTO/documentos-tramites.dto';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { TramitesConLicenciaDto } from 'src/tramites/DTO/tramitesConLicencia.dto';
import { Resultado } from 'src/utils/resultado';
import { getConnection, Repository, UpdateResult } from 'typeorm';
import { ModifyMotivoAdicional } from '../DTO/modifyMotivoAdicional.dto';
import { TramitesClaseLicenciaDto } from '../DTO/tramitesClaseLicencia.dto';
import { TramitesClaseLicencia } from '../entity/tramites-clase-licencia.entity';
import { TramitesClaseLicenciaModule } from '../tramites-clase-licencia.module';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTramiteLicenciaDto } from '../DTO/createTramiteLicencia.dto';
import { TiposDocumentoEntity } from 'src/documentos-tramites/entity/tipos-documento.entity';
import { tipoDocumentoDenegacion } from 'src/constantes';
import { User } from 'src/users/entity/user.entity';
import { TramitesEntity } from 'src/tramites/entity/tramites.entity';

@Injectable()
export class TramitesClaseLicenciaService {

    constructor(@InjectRepository(TramitesClaseLicencia) private readonly tramitesClaseLicenciaRespository: Repository<TramitesClaseLicencia>,
        @InjectRepository(DocumentosTramitesEntity) private readonly documentosTramitesRepository: Repository<DocumentosTramitesEntity>,
        @InjectRepository(User) private readonly userRepository: Repository<User>
    ) { }

    async getTramitesClaseLicencias() {
        return await this.tramitesClaseLicenciaRespository.find();
    }


    async getTramitesClaseLicencia(id:number){
        const region= await this.tramitesClaseLicenciaRespository.findOne(id);
        if (!region)throw new NotFoundException("Tramites Clase Licencia dont exist");
        
        return region;
    }


    async update(idTramiteClaseLicencia: number, data: Partial<CreateTramiteLicenciaDto>) {
        await this.tramitesClaseLicenciaRespository.update({ idTramiteClaseLicencia }, data);
        return await this.tramitesClaseLicenciaRespository.findOne({ idTramiteClaseLicencia });
        }
        
    async create(data: CreateTramiteLicenciaDto): Promise<TramitesClaseLicencia> {
        return await this.tramitesClaseLicenciaRespository.save(data);
    }

    async getTramiteClaseLicenciaById(tramiteId: number): Promise<Resultado> {

        let resultado: Resultado = new Resultado();

        let tramitesDto: TramitesClaseLicenciaDto = new TramitesClaseLicenciaDto();

        let documentoTramiteDto: DocumentosTramitesDTO = new DocumentosTramitesDTO();

        try {

            if (tramiteId != null || tramiteId != undefined) {

                let tramitesClaseLicencia: TramitesClaseLicencia = await this.tramitesClaseLicenciaRespository.findOne({
                    relations: ['tramite', 'tramite.motivoDenegacion'],
                    where: qb => {
                        qb.where (
                            'TramitesClaseLicencia__tramite.idTramite = :idTramite', { idTramite: tramiteId }
                        )
                    }
                });

                let documentoTramite = await this.documentosTramitesRepository.findOne({ idTramite: tramiteId });

                tramitesDto.idTramiteClaseLicencia = tramitesClaseLicencia.idTramiteClaseLicencia;
                tramitesDto.idTramite = tramitesClaseLicencia.idTramite;
                tramitesDto.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia;
                // tramitesDto.fechaProximoControl = tramitesClaseLicencia.fechaProximoControl;   TEMP
                // tramitesDto.folio = tramitesClaseLicencia.folio;
                // tramitesDto.fechaRecepcionConforme = tramitesClaseLicencia.fechaRecepcionConforme;
                tramitesDto.idMotivoDenegacion = tramitesClaseLicencia.tramite.idMotivoDenegacion;
                tramitesDto.fechaReprobacion = tramitesClaseLicencia.tramite.fechaReprobacion;
                tramitesDto.tipoDenegacion = tramitesClaseLicencia.tramite.tipoDenegacion;
                tramitesDto.plazoDenegacion = tramitesClaseLicencia.tramite.plazoDenegacion;
                tramitesDto.motivoAdicional = tramitesClaseLicencia.tramite.motivoAdicional;

                if (documentoTramite != null || documentoTramite != undefined) {

                    // Documento Tramite
                    documentoTramiteDto.idDocumentosTramite = documentoTramite.idDocumentosTramite;
                    documentoTramiteDto.NombreDocumento = documentoTramite.NombreDocumento;
                    documentoTramiteDto.fileBinary = documentoTramite.fileBinary.toString();
                    documentoTramiteDto.idTramite = documentoTramite.idTramite;
                    documentoTramiteDto.idUsuario = documentoTramite.idUsuario;
                    documentoTramiteDto.created_at = documentoTramite.created_at;
                    documentoTramiteDto.updated_at = documentoTramite.updated_at;
                    documentoTramiteDto.idTipoDocumento = documentoTramite.idTipoDocumento;

                    tramitesDto.documentoTramite = documentoTramiteDto;

                } else {
                    tramitesDto.documentoTramite = null;
                }


                resultado.Respuesta = tramitesDto;
                resultado.ResultadoOperacion = true;
                resultado.Mensaje = 'TramiteClaseLicencia obtenido correctamente'

            } else {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se ha pasado idTramite por parámetro'
            }

        } catch (error) {

            console.log(error);
            resultado.ResultadoOperacion = false;
            resultado.Error = error.message;

        }

        return resultado;

    }

    async modifyMotivoAdicionalById(motivoAdicional: ModifyMotivoAdicional, idUsuarioConectado: number) {

        let resultado: Resultado = new Resultado();
        
        let usuarioConectado = new User();

        if (idUsuarioConectado > 0) {
            usuarioConectado = await this.userRepository.findOne({idUsuario: idUsuarioConectado});
        } else {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error: el token no coincide con un usuario';
            console.log('Error: el token no coincide con un usuario')
            return resultado;
        }

        if(motivoAdicional.archivo != '' && !motivoAdicional.archivo.includes('pdf')) {
            resultado.ResultadoOperacion = false;
            resultado.Error = 'Error: el archivo no es pdf';
            console.log('Error: el archivo no es pdf')
            return resultado;
        }

        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {

            if (motivoAdicional.idTramiteClaseLicencia != null || motivoAdicional.idTramiteClaseLicencia != undefined) {

                let motivoUpdated  = await queryRunner.manager.update(TramitesEntity, motivoAdicional.idTramite,
                {
                    motivoAdicional: motivoAdicional.MotivoAdicional //TEMP
                });

                if (motivoUpdated.affected == 0) {
                    await queryRunner.rollbackTransaction();
                    resultado.ResultadoOperacion = false;
                    resultado.Error = 'No se ha encontrado el idTramiteClaseLicencia';
                } else {

                    // Si se ha mandado un documento, Creamos un DocumentoTramite con dichos datos y lo subimos a la BBDD
                    if (motivoAdicional.archivo != null && motivoAdicional.archivo != ''
                    && motivoAdicional.nombreArchivo != null && motivoAdicional.nombreArchivo != '') {
            
                        // Obtenemos el Id para el tipo Documento: "Documento de Denegacion de Licencia"
                        const idTipoDocumentoDenegado = (await queryRunner.manager.findOne(TiposDocumentoEntity, { where: {Nombre: tipoDocumentoDenegacion} })).idTipoDocumento;

                        // Si no hay ninguna TipoDocumento con el nombre buscado mostramos un error
                        if (idTipoDocumentoDenegado == null) {
                            resultado.ResultadoOperacion = false;
                            resultado.Error = 'No se encontro el tipo de Documento';
                            await queryRunner.rollbackTransaction();
                            await queryRunner.release();
                            return resultado;
                        }

                        let documentoTramite: DocumentosTramitesEntity = new DocumentosTramitesEntity();

                        documentoTramite.fileBinary = Buffer.from(motivoAdicional.archivo);
                        documentoTramite.NombreDocumento = motivoAdicional.nombreArchivo;
                        documentoTramite.created_at = new Date();
                        documentoTramite.updated_at = new Date();
                        documentoTramite.idTipoDocumento = idTipoDocumentoDenegado;
                        documentoTramite.idTramite = motivoAdicional.idTramite;
                        documentoTramite.idUsuario = usuarioConectado.idUsuario;

                        await queryRunner.manager.save(documentoTramite);
                    }

                    await queryRunner.commitTransaction();
                    resultado.Respuesta = motivoUpdated;
                    resultado.ResultadoOperacion = true;
                    resultado.Mensaje = 'Motivo Adicional modificado correctamente';
                }

            } else {

                await queryRunner.rollbackTransaction();
                resultado.ResultadoOperacion = false;
                resultado.Error = 'No se ha pasado idTramite por parámetro'
            }

        } catch (error) {
            await queryRunner.rollbackTransaction();
            resultado.ResultadoOperacion = false;
            resultado.Error = error.message;
        }
        finally{
            await queryRunner.release();
        }

        return resultado;

    }
}
