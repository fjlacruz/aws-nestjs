import { Module } from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { jwtConstants } from 'src/users/constants';
import { User } from 'src/users/entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TramitesClaseLicenciaController } from './controller/tramites-clase-licencia.controller';
import { TramitesClaseLicencia } from './entity/tramites-clase-licencia.entity';
import { TramitesClaseLicenciaService } from './services/tramites-clase-licencia.service';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';

@Module({
  imports: 
  [
    TypeOrmModule.forFeature(
    [
      TramitesClaseLicencia,
      User,
      RolesUsuarios,
      DocumentosTramitesEntity,
      DocsRolesUsuarioEntity
    ]),
  ],
  providers: [TramitesClaseLicenciaService, AuthService],
  exports: [TramitesClaseLicenciaService],
  controllers: [TramitesClaseLicenciaController]
})
export class TramitesClaseLicenciaModule {}
