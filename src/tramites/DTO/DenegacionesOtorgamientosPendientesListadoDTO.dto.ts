import { ApiPropertyOptional } from "@nestjs/swagger";

export class DenegacionesOtorgamientosPendientesListadoDTO {
    constructor(){}

    @ApiPropertyOptional()
    IdSolicitud: number;

    @ApiPropertyOptional()
    IdEstadoSolicitud: number;

    @ApiPropertyOptional()
    EstadoSolicitudNombre: string;    

    @ApiPropertyOptional()
    FechaCreacion: Date;   

    @ApiPropertyOptional()
    Run: Number;

    @ApiPropertyOptional()
    RunFormateado: string; 

    @ApiPropertyOptional()
    RunFormateadoSinPuntos: string;

    @ApiPropertyOptional()
    Nombres: string; 
    
    @ApiPropertyOptional()
    ApellidosPaterno: string; 
    
    @ApiPropertyOptional()
    ApellidosMaterno: string;     

    @ApiPropertyOptional()
    EsDenegacion: Boolean;

    @ApiPropertyOptional()
    Tramites: DenegacionesOtorgamientosTramitesDTO[]; 
    
    @ApiPropertyOptional()
    NombreCompletoPostulante: string;

    @ApiPropertyOptional()
    IdComunaAsociada: number;
}

export class DenegacionesOtorgamientosTramitesDTO {
    constructor(){}

    @ApiPropertyOptional()
    IdTramite: number;

    @ApiPropertyOptional()
    IdEstadoTramite: number;

    @ApiPropertyOptional()
    EstadoTramiteString: string; 

    @ApiPropertyOptional()
    IdTipoTramite: number;   
    
    @ApiPropertyOptional()
    TipoTramiteString: string; 
  
    @ApiPropertyOptional()
    IdClaseLicencia: number;

    @ApiPropertyOptional()
    AbreviacionClaseLicenciaAsocString: string;

    @ApiPropertyOptional()
    idoneidadMoral: number;

    @ApiPropertyOptional()
    examenMedico: number;

    @ApiPropertyOptional()
    examenTeorico: number;

    @ApiPropertyOptional()
    examenPractico: number;
    // @ApiPropertyOptional()
    // Examinaciones: DenegacionesOtorgamientosExaminacionesDTO[];
}

// export class DenegacionesOtorgamientosExaminacionesDTO{
//     @ApiPropertyOptional()
//     IdExaminacion: number;

//     @ApiPropertyOptional()
//     IdEstadoExaminacion: number;    

//     @ApiPropertyOptional()
//     IdTipoExaminacion: number;

//     @ApiPropertyOptional()
//     EstadoExaminacionString: string; 

//     @ApiPropertyOptional()
//     TipoExaminacionString: string;     

//     // @ApiPropertyOptional()
//     // AbreviacionClaseLicenciaAsocString: string;  
    
//     @ApiPropertyOptional()
//     IdTramitesAsociados: number[];

// }