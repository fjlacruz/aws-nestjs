import { Column, PrimaryColumn } from "typeorm";

export class DenegacionesXOtorgamientosPendientes
{
    @PrimaryColumn()
    idsolicitud: number;

    @Column()
    estadosolicitudnombre: string; 

    @Column()
    fechacreacion: Date;    

    @Column()
    estadotramiteidentificador: number;

    @Column()
    run: string;

    @Column()
    nombres: string;
    
    @Column()
    apellidospaterno: string;
    
    @Column()
    apellidosmaterno: string;
    
}