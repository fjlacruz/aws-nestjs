import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateTramiteDto {

    @ApiPropertyOptional()
    idTramite:number;
    @ApiPropertyOptional()
    idSolicitud: number;
    @ApiPropertyOptional()
    idTipoTramite:number;
    @ApiPropertyOptional()
    idEstadoTramite:number;
    @ApiPropertyOptional()
    observacion:string;
    @ApiPropertyOptional()
    created_at: Date;
    @ApiPropertyOptional()
    update_at: Date;
}