export class DatosClasesLicenciasConFechas {
    idClaseLicencia: string;
    descClaseLicencia: string;
    fechaPrimerOtorgamiento: string; 
    fechaActualOtorgamiento: string; 
    fechaProximoControl: string;
    asignado: boolean;
    folio: string;
    asignadoRC:boolean;
  }