import { FechasLicenciaDto } from './fechasLicencia.dto';

export class DatosLicenciaDto {
  nombre: string;
  apellidos: string;
  RUN: string;
  Municipalidad: string;
  Direccion: string;
  Observaciones: string;
  foto: string;
  licencias: FechasLicenciaDto[];
}
