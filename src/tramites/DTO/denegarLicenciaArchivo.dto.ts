export class DenegarLicenciaArchivoDto {
    idTramite: number;
    idMotivoDenegacion: number;
    fechaReprobacion: Date;
    plazoDenegacion: string;
    dias: string;
    motivoAdicional: string;
    archivo: string;
    nombreArchivo: string;
}