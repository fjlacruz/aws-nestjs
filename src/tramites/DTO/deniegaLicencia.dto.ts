export class DeniegaLicenciaDto {
    idTramite: number;
    runEmisor: string;
    motivo: string;
    motivoAdicional: string;
    receptorDescripcionDegenacion: string;
}