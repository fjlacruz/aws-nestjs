export class FechasLicenciaDto {
    nombre: string;
    primerOtorgamiento: Date;
    otorgamientoActual: Date;
    vencimiento: Date;
}