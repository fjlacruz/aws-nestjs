export class InstitucionesDTO {
    idInstitucion:number;
    idTipoInstitucion:number;
    nombre:string;
    descripcion:string;
}