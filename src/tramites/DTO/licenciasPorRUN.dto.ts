export class TramitesEnProcesoDto {
    id: number;
    run: string;
    estado: string;
    nombrePostulante: string; // nombre + apellidos
    tipoTramite: string;
    claseLicencia: string; 
    idoneidadMoral: number;
    examenMedico: number;
    examenTeorico: number;
    examenPractico: number;
}