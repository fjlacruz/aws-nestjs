import { ApiPropertyOptional } from "@nestjs/swagger";

export class OficinasDTO{

    @ApiPropertyOptional()
    idOficina:number;

    @ApiPropertyOptional()
    idInstitucion:number;

    @ApiPropertyOptional()
    idComuna:number;
    
    @ApiPropertyOptional()
    Nombre:string;

    @ApiPropertyOptional()
    Direccion:string;
    
    @ApiPropertyOptional()
    Descripcion:string;

    @ApiPropertyOptional()
    activo: boolean;
}
