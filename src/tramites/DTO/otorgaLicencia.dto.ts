export class OtorgaLicenciaDto {
    idTramite: number;
    fecha: Date;
    fechaString: string;
}