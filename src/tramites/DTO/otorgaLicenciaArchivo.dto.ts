import { ApiPropertyOptional } from "@nestjs/swagger";
import { OtorgaLicenciaDto } from "./otorgaLicencia.dto";
import { TramiteFotosLicenciaDto } from "./tramiteFotosLicencia.dto";

export class OtorgaLicenciaArchivoDto {
    @ApiPropertyOptional()
    archivo: string;
    @ApiPropertyOptional()
    nombreArchivo: string;
    @ApiPropertyOptional()
    observaciones: string;
    @ApiPropertyOptional()
    noSeleccionados: number[];
    @ApiPropertyOptional()
    seleccionados: OtorgaLicenciaDto[];
    @ApiPropertyOptional()
    tramiteFotosLicencia: TramiteFotosLicenciaDto;
    @ApiPropertyOptional()
    folio: string;
    @ApiPropertyOptional()
    idsRestricciones:number[];
}