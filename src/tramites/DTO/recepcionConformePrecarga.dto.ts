export class RecepcionConformePrecarga {
    Observaciones: string;
    DetalleFechasClasesLicencia: RecepcionConformePrecargaDetalleFechas[];
    IdsRestriccionesSeleccionadas:number[];
}

export class RecepcionConformePrecargaDetalleFechas {
    IdClaseLicencia: number;
    FechaProximoControlLicencia: Date;
    FechaSoloLectura: boolean;
    FechaProximoControlLicenciaString: string;
}