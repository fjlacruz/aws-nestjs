export class TramiteDTO {
    idTramite: number;
    estadoTramite: string;
    tipoTramite: string;
    idClaseLicencia: number;
    claseLicencia: string;

}