
export class TramiteConTipoDto {
    idTramite: number;
    tipoTramite: string;
    estadoTramite: string
}