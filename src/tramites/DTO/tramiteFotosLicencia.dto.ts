
export class TramiteFotosLicenciaDto {
    idsTramite: number[];
    fotoFrontal: string;
    fotoTrasera: string;
}