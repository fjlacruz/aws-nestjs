export class TramiteRecepcionDto {
    idTramite: number;
    documentoNombre: string;
    documentoBinary: string;
    fechaRecepcion: Date;
    conforme: boolean;
}