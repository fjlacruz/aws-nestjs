export class TramitesConLicenciaDto {
    id: number;
    run: string;
    nombrePostulante: string; // nombre + apellidos
    claseLicencia: string; 
    idClaseLicencia: number;
}