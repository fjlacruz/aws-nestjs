export class TramitesEnProcesoDto {
  idSolicitud: number;
  idTramite: number;
  run: string;
  estadoSolicitud: string;
  estadoTramite: string;
  estado: string;
  nombrePostulante: string; // nombre + apellidos
  tipoTramite: string;
  idTipoTramite: number;
  idComuna: any;
  claseLicencia: string;
  idoneidadMoral: number;
  examenMedico: number;
  examenTeorico: number;
  examenPractico: number;
  idClaseLicencia: number;
  municipalidadSolicitudString: string;
  emailPostulante: string;
  fechaSolicitud: Date;
}
