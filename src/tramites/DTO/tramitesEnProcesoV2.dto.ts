export class TramitesEnProcesoV2Dto {
    idSolicitud: number;
    idTramite: number[];
    run: string;
    nombrePostulante: string; // nombre + apellidos
    tipoTramite: string[]; // 
    estadoSolicitud: string;
    estadoTramite: string[];
    claseLicencia: string[]; 
    idoneidadMoral: number;
    examenMedico: number;
    examenTeorico: number[];
    examenPractico: number[];
}