import { Controller } from '@nestjs/common';
import { Get, Param } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { InstitucionesService } from '../services/instituciones.service';
var shell = require('shelljs');

@ApiTags('instituciones')
@Controller('instituciones')
export class InstitucionesController {
  constructor(private readonly institucionesService: InstitucionesService, private readonly authService: AuthService) {}

  @Get('getInstitucionesByUserId/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio' })
  async getTramitesByRUNPostulante(@Param('id') id: number) {
    const data = await this.institucionesService.getInstitucionesByIdUser(id);
    return { data };
  }


}
