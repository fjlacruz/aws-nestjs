import { Controller, Query, Req } from '@nestjs/common';
import { Get, Param, Post, Body } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { TramitesService } from '../services/tramites.service';
import { ApiBearerAuth } from '@nestjs/swagger';
import { CreateTramiteDto } from '../DTO/createTramite.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TramiteRecepcionDto } from '../DTO/tramiteRecepcion.dto';
import { PermisosNombres } from 'src/constantes';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { AuthService } from 'src/auth/auth.service';
import { DenegarLicenciaArchivoDto } from '../DTO/denegarLicenciaArchivo.dto';
import { DeniegaLicenciaDto } from '../DTO/deniegaLicencia.dto';
import { OtorgaLicenciaArchivoDto } from '../DTO/otorgaLicenciaArchivo.dto';
import { TramiteFotosLicenciaDto } from '../DTO/tramiteFotosLicencia.dto';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
var shell = require('shelljs');

@ApiTags('tramites')
@Controller('tramites')
export class TramitesController {
  constructor(private readonly tramitesService: TramitesService, private readonly authService: AuthService) {}

  @Get('getTramitesByRUNPostulante/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los tramites de un postulante por su RUN' })
  async getTramitesByRUNPostulante(@Param('run') run: number) {
    const data = await this.tramitesService.getTramitesByRUNPostulante(run);
    return { data };
  }

  @Get('getTramitesDePostulanteByRun/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los tramites de un postulante por su RUN' })
  async getTramitesDePostulanteByRun(@Param('run') run: number) {
    return await this.tramitesService.getTramitesByRUNPostulante(run);
  }

  @Get('getTramitesByIdPostulante/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve los tramites de un postulante por su id' })
  async getTramitesByIdPostulante(@Param('id') id: number) {
    const data = await this.tramitesService.getTramitesByIdPostulante(id);
    return { data };
  }

  @Post('creaTramite')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un tramite' })
  async addTramite(@Body() createTramiteDto: CreateTramiteDto) {
    const data = await this.tramitesService.create(createTramiteDto);
    return { data };
  }

  @Post('getTramitesSolicitud')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en proceso' })
  async getTramitesSolicitud(@Body() paginacionArgs: PaginacionArgs, @Req() req: any, @Query() query: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaTramitesEnProceso]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getTramitesSolicitud(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('getTramitesEnProceso')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en proceso' })
  async getTramitesSolicitudV2(@Body() paginacionArgs: PaginacionArgs, @Req() req: any, @Query() query: any) {

      const data = await this.tramitesService.getTramitesEnProceso(
        //query.idTipoExaminacion,
        req,
        paginacionArgs,
        query.run,
        query.nombreApellido,
        query.idSolicitud,
        query.idTramite,
        query.idTipoTramite,
        query.idEstadoTramite,
        query.idClaseLicencia
      );

      return { data };

  }

  @Post('getPostulantesEnEspera')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los postulantes a la espera de otorgar, denegar, imprimir o recepción conforme' })
  async getPostulantesEnEspera(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaTramitesEnListaDeEspera]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getPostulantesEnEspera(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('getPostulantesEnEsperaV2')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los postulantes a la espera de otorgar, denegar, imprimir o recepción conforme' })
  async getPostulantesEnEsperaV2(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaTramitesEnListaDeEspera]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getPostulantesEnEsperaV2(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('getOtorgamientoDenegacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en estado de otorgamiento o denegación' })
  async getOtorgamientoDenegacion(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaOtorgamientosDenegaciones]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getOtorgamientoDenegacion(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('getOtorgamientoDenegacionV2')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en estado de otorgamiento o denegación' })
  async getOtorgamientoDenegacionV2(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaOtorgamientosDenegaciones]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getOtorgamientoDenegacion(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('getVisualizacionLicencias')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en estado de imprimir licencias' })
  async getVisualizacionLicencias(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaTramitesEnVisualizacionLicencia]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getVisualizacionLicencias(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('getRecepcionConforme')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en estado de recepción conforme' })
  async getRecepcionConforme(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaRecepcionConforme]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getRecepcionConforme(paginacionArgs);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('postRecepcionConforme')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que recepciona conforme un tramite' })
  async postRecepcionConforme(@Body() tramiteRecepcion: TramiteRecepcionDto, @Req() req: any) {

      const data = await this.tramitesService.postRecepcionConforme(req, tramiteRecepcion);
      
      return { data };

  }

  @Get('getLicenciasOtorgarByRUN/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que obtiene las licencias en estado de Otorgamiento, por el RUN' })
  async getLicenciasOtorgarByRUN(@Param('run') run: string, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaOtorgamientosDenegaciones]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.getLicenciasOtorgarByRUN(run);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('comprobarEstadoCorrectoRPI/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que obtiene el estado del postulante en RPI' })
  async comprobarEstadoCorrectoRPI(@Param('run') run: string, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaOtorgamientosDenegaciones]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.comprobarEstadoCorrectoRPI(run);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('otorgarLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que otorga Licencia, envia peticion a Registro Civil y responden se inserta en la parte Mobile' })
  async otorgarLicencia(@Body() otorgaLicenciaArchivoDto: OtorgaLicenciaArchivoDto, @Req() req: any) {

      const data = await this.tramitesService.otorgarLicencia(req, otorgaLicenciaArchivoDto);

      return { data };

  }

  @Post('informarOtorgarLicencia')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para registrar la licencia en la Lic del registro civil',
  })
  async informarOtorgarLicencia(@Body() data: any) {
    return this.tramitesService.informarLicRNC(data);
  }

  @Post('informarDenegacion')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para informar denegaciones de la licencia en el registro civil',
  })
  async informarDenegacion(@Body() data: any) {
    return this.tramitesService.informarDenegacion(data);
  }

  @Post('traerDatosExistentes')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Devuelve la fecha del proximo control y las observaciones si ya ha sido otorgado el tramite previamente' })
  async traerDatosExistentes(@Body() idTramitesOtorgamiento: number[], @Req() req: any) {

      const data = await this.tramitesService.traerDatosExistentes(req, idTramitesOtorgamiento);
      
      return { data };

  }

  // @Post('enviarImagenesLicencia')
  // @ApiBearerAuth()
  // @ApiOperation({ summary: 'Servicio que envia imagenes licencia' })
  // async enviarImagenesLicencia(
  //     @Body() otorgaLicenciaArchivoDto: OtorgaLicenciaArchivoDto, @Req() req: any) {

  //         let splittedBearerToken = req.headers.authorization.split(" ");
  //         let token = splittedBearerToken[1];

  //         let tokenPermisos = new TokenPermisoDto(token, [
  //             PermisosNombres.ValidadorOtorgarLicencia
  //         ]);

  //         let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

  //         if (validarPermisos.ResultadoOperacion) {

  //             const data = await this.tramitesService.otorgarLicencia(otorgaLicenciaArchivoDto, token);
  //             return { data };

  //         } else {

  //             return { data: validarPermisos };
  //         }
  // }

  @Post('enviarCorreoDenegacion')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que envia correo al denegar una licencia' })
  async enviarCorreoDenegacion(@Body() deniegaLicenciaDto: DeniegaLicenciaDto, @Req() req: any) {
      return await this.tramitesService.enviarCorreoDenegacion(deniegaLicenciaDto, req);
  }

  @Post('denegarLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que deniega una Licencia (cambia el estado del tramite a Denegación informada)' })
  async denegarLicencia(@Body() denegarLicenciaDTO: DenegarLicenciaArchivoDto, @Req() req: any) {

      const data = await this.tramitesService.denegarLicencia(req, denegarLicenciaDTO);
      return { data };

  }

  @Post('subirFotosLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que sube las fotos frontal y trasera de la Licencia' })
  async subirFotosLicencia(@Body() fotoLicencia: TramiteFotosLicenciaDto, @Req() req: any) {

      const data = await this.tramitesService.subirLicencias(req, fotoLicencia);

      return { data };
  }

  // @Post('subirFotosLicenciaByIdsTramite')
  // @ApiBearerAuth()
  // @ApiOperation({ summary: 'Servicio que recoge la información del trámite y asocia las fotos de licencias' })
  // async subirFotosLicenciaByIdsTramite(@Body() idTramite: number, @Req() req: any) {
  //   let splittedBearerToken = req.headers.authorization.split(' ');
  //   let token = splittedBearerToken[1];

  //   let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaLicencias]);

  //   let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

  //   if (validarPermisos.ResultadoOperacion) {
  //     const data = await this.tramitesService.subirLicenciasByIdsTramite(idTramite);
  //     return { data };
  //   } else {
  //     return { data: validarPermisos };
  //   }
  // }

  @Get('obtenerDatosLicenciaFrontal/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que obtiene datos del Postulante para la imagen de la licencia, durante el otorgamiento' })
  async obtenerDatosLicenciaFrontal(@Param('run') run: string, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.VisualizarListaTramitesEnVisualizacionLicencia]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const data = await this.tramitesService.obtenerDatosLicenciaFrontal(run);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('validaStatusRcel')
  @ApiOperation({
    summary: 'Servicio que valida si el Registro Civil esta activo(OK) o no actino(NOK)',
  })
  async validaStatusRcel() {
    let respuesta = shell.exec('ping priv.srcei.cl');
    let status = respuesta.code;
    if (status === 0) {
      status = 'OK';
    } else {
      status = 'NOK';
    }

    return { status };
  }

  @Post('cambioEstadoInformarOtorgamientoTipo1')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Servicio para registrar domicilio del postulante en registro civil',
  })
  async direccion(@Body() data: string) {
    return 'cambioEstadoInformarOtorgamientoTipo1';
  }

  @Post('getTramitesEnVisualizacionLicencia')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en proceso' })
  async getTramitesEnVisualizacionLicencia(@Body() paginacionArgs: PaginacionArgs, @Req() req: any, @Query() query: any) {

      const data = await this.tramitesService.getTramitesEnVisualizacionLicencia(
        //query.idTipoExaminacion,
        req,
        paginacionArgs,
        query.run,
        query.nombreApellido,
        query.idSolicitud,
        query.idTipoTramite,
        query.idClaseLicencia
      );

      return { data };
      
  }

  @Post('getTramitesEnOtorgamientoDenegaciones')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en proceso' })
  async getTramitesEnOtorgamientoDenegaciones(@Body() paginacionArgs: PaginacionArgs, @Req() req: any, @Query() query: any) {

      const data = await this.tramitesService.getTramitesEnOtorgamientoDenegaciones(
        req,
        paginacionArgs,
        query.run,
        query.nombreApellido,
        query.idSolicitud,
        query.idTipoTramite,
        query.idClaseLicencia,
        query.idEstadoTramite,
        query.fechaSolicitud
      );

      return { data };

  }

  @Post('getTramitesEnListaDeEspera')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en proceso' })
  async getTramitesEnListaDeEspera(@Body() paginacionArgs: PaginacionArgs, @Query() query: any, @Req() req: any) {

      const data = await this.tramitesService.getTramitesEnListaDeEspera(
        req,
        paginacionArgs,
        query.run,
        query.nombreApellido,
        query.idSolicitud,
        query.idTipoTramite,
        query.idClaseLicencia,
        query.idEstadoTramite,
        query.fechaSolicitud
      );

      return { data };

  } 
  
  @Post('getTramitesEnRecepcionConforme')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que devuelve todos los tramites en recepcion conforme' })
  async getTramitesEnRecepcionConforme(@Body() paginacionArgs: PaginacionArgs, @Req() req: any, @Query() query: any) {

      const data = await this.tramitesService.getTramitesEnRecepcionConforme(
        req,
        paginacionArgs,
        query.run,
        query.nombreApellido,
        query.idSolicitud,
        query.idTipoTramite,
        query.idClaseLicencia
      );

      return { data };

  }

}
