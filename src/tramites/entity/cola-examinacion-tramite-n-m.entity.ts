import {Entity, Column, PrimaryColumn, ManyToOne, JoinColumn, OneToMany} from "typeorm";
import { ColaExaminacion } from "./cola-examinacion.entity";
import { TramitesEntity } from "./tramites.entity";

@Entity('ColaExaminacionTramiteNM')
export class ColaExaminacionTramiteNMEntity {

    @PrimaryColumn()
    idColaExaminacionTramite:number;

    @Column()
    idTramite:number;
    @ManyToOne(() => TramitesEntity, (tramites) => tramites.colaExaminacionNM)
    @JoinColumn({ name: 'idTramite' })
    readonly tramites: TramitesEntity;

    @ManyToOne(() => TramitesEntity, (tramites) => tramites.colaExaminacionNMAux)
    @JoinColumn({ name: 'idTramite' })
    readonly tramitesAux: TramitesEntity;

    @Column()
    idColaExaminacion:number;
    @ManyToOne(() => ColaExaminacion, (colaExaminacion) => colaExaminacion.colaExaminacionNM)
    @JoinColumn({ name: 'idColaExaminacion' })
    readonly colaExaminacion: ColaExaminacion;

    @ManyToOne(() => ColaExaminacion, (colaExaminacion) => colaExaminacion.colaExaminacionNMAux)
    @JoinColumn({ name: 'idColaExaminacion' })
    readonly colaExaminacionAux: ColaExaminacion;

    @OneToMany(() => ColaExaminacion, (colaExaminacionEstadoAlerta) => colaExaminacionEstadoAlerta.colaExaminacionNM)
    readonly colaExaminacionestadoAlerta: ColaExaminacion;    

}

