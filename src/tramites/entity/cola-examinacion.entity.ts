import { ApiProperty } from '@nestjs/swagger';
import { estadosExaminacionEntity } from 'src/cola-examinacion/entity/estadosExaminacion.entity';
import { OportunidadEntity } from 'src/oportunidad/oportunidad.entity';
import { ProrrogaExaminacionEntity } from 'src/prorrogas/entities/prorroga-examinacion.entity';
import { ResultadoExaminacionColaEntity } from 'src/resultado-examinacion/entity/ResultadoExaminacionCola.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { ColaExaminacionTramiteNMEntity } from './cola-examinacion-tramite-n-m.entity';
import { EstadosExaminacionEntity } from './estados-examinacion.entity';
import { TramitesEntity } from './tramites.entity';

@Entity('ColaExaminacion')
export class ColaExaminacion {
  @PrimaryColumn()
  idColaExaminacion: number;

  @Column()
  idTipoExaminacion: number;
  @ManyToOne(() => TipoExaminacionesEntity, (tipoExaminaciones) => tipoExaminaciones.colaExaminacion)
  @JoinColumn({ name: 'idTipoExaminacion' })
  readonly tipoExaminaciones: TipoExaminacionesEntity;

  @Column()
  historico: boolean;

  @Column()
  aprobado: boolean;

  @Column()
  created_at: Date;

  @Column()
  idEstadosExaminacion: number;
  @ManyToOne(() => estadosExaminacionEntity, (estadoExaminaciones) => estadoExaminaciones.colaExaminacion)
  @JoinColumn({ name: 'idEstadosExaminacion' })
  readonly estadoExaminaciones: EstadosExaminacionEntity;

  @Column()
  idTramite: number;
  @ManyToOne(() => TramitesEntity, (tramites) => tramites.colaExaminacion)
  @JoinColumn({ name: 'idTramite' })
  readonly tramites: TramitesEntity;

  @ApiProperty()
  @OneToMany(() => ColaExaminacionTramiteNMEntity, (colaExaminacionNM) => colaExaminacionNM.colaExaminacion)
  readonly colaExaminacionNM: ColaExaminacionTramiteNMEntity;

  @ApiProperty()
  @OneToMany(() => ColaExaminacionTramiteNMEntity, (colaExaminacionNM) => colaExaminacionNM.colaExaminacionAux)
  readonly colaExaminacionNMAux: ColaExaminacionTramiteNMEntity;

  @ApiProperty()
  @OneToMany(() => ColaExaminacionTramiteNMEntity, (colaExaminacionNMEstadoAlerta) => colaExaminacionNMEstadoAlerta.colaExaminacion)
  readonly colaExaminacionNMEstadoAlerta: ColaExaminacionTramiteNMEntity;

  @ApiProperty()
  @OneToMany(() => ProrrogaExaminacionEntity, (prorrogaExaminacion) => prorrogaExaminacion.colaExaminacion)
  readonly prorrogaExaminacion: ProrrogaExaminacionEntity[];

  @ApiProperty()
  @OneToMany(() => OportunidadEntity, (oportunidadExaminacion) => oportunidadExaminacion.colaExaminacion)
  readonly oportunidadExaminacion: OportunidadEntity[];

  @ApiProperty()
  @OneToMany(() => ResultadoExaminacionColaEntity, (resultadoExaminacionCola) => resultadoExaminacionCola.colaExaminacion)
  readonly resultadoExaminacionCola: ResultadoExaminacionColaEntity[];
  
}
