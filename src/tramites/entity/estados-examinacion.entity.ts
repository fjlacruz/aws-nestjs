import { ApiProperty } from "@nestjs/swagger";
import { MotivosDenegacionEntity } from "src/motivos-denegacion/entity/motivo-denegacion.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToMany} from "typeorm";
import { ColaExaminacion } from "./cola-examinacion.entity";


@Entity('estadosExaminacion')
export class EstadosExaminacionEntity {
    @PrimaryColumn()
    idEstadosExaminacion:number;

    @Column()
    nombreEstado:string;

    @Column()
    descripcion:string;

    @Column()
    idTipoExaminacion:number

    @Column()
    codigo:string;

    @ApiProperty()
    @OneToMany(() => ColaExaminacion, examinaciones => examinaciones.estadoExaminaciones)
    readonly examinaciones: ColaExaminacion[];
}
