import { ApiProperty } from "@nestjs/swagger";
import { Solicitudes } from "src/solicitudes/entity/solicitudes.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";

@Entity('EstadosSolicitud')
export class EstadosSolicitudEntity {

    @PrimaryGeneratedColumn()
    idEstado:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:number;

    @Column()
    IdEstadoProximo:number;

    @Column()
    codigo: string;

    @ApiProperty()
    @OneToMany(() => Solicitudes, solicitudes => solicitudes.estadosSolicitud)
    readonly solicitudes: Solicitudes;
}
