import { ApiProperty } from "@nestjs/swagger";
import { MotivosDenegacionEntity } from "src/motivos-denegacion/entity/motivo-denegacion.entity";
import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToMany} from "typeorm";
import { TramitesEntity } from "./tramites.entity";


@Entity('EstadosTramite')
export class EstadosTramiteEntity {

    @PrimaryColumn()
    idEstadoTramite:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;

    @Column()
    IdPredecesor:number

    @Column()
    codigo:string

    @ApiProperty()
    @OneToMany(() => TramitesEntity, tramites => tramites.estadoTramite)
    readonly tramites: TramitesEntity[];

    @ApiProperty()
    @OneToMany(() => MotivosDenegacionEntity, motivosDenegacion => motivosDenegacion.estadoTramite)
    readonly motivoDenegacion: MotivosDenegacionEntity[];
}
