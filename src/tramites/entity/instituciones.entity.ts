import { ApiProperty } from "@nestjs/swagger";
import { ComunasEntity } from "src/comunas/entity/comunas.entity";
import { PapelSeguridadEntity } from "src/papel-seguridad/entity/papel-seguridad.entity";
import { TipoDeTramiteClaseLicenciaInstitucionEntity } from "src/tipo-de-tramite-clase-licencia-institucion/entity/tipo-de-tramte-clase-licencia-institucion.entity";
import { OrdenTramiteExaminacionEntity } from "src/tipos-tramites/entity/ordenTramiteExaminacion.entity";
import { OficinasEntity } from "src/tramites/entity/oficinas.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, JoinColumn} from "typeorm";
import { TipoInstitucionEntity } from '../../tipo-institucion/entity/tipo-institucion.entity';

@Entity('Instituciones')
export class InstitucionesEntity {
  @PrimaryGeneratedColumn()
  idInstitucion: number;

  @Column()
  idTipoInstitucion: number;

  @ApiProperty()
  @OneToMany(() => TipoInstitucionEntity, tipoInstitucion => tipoInstitucion.idTipoInstitucion)
  readonly tipoInstitucionEntity: TipoInstitucionEntity;

  @Column()
  Nombre: string;

  @Column()
  Descripcion: string;
  @ApiProperty()
  @OneToOne(() => OficinasEntity, oficinas => oficinas.instituciones)
  readonly oficinas: OficinasEntity;

  @ApiProperty()
  @OneToMany(() => PapelSeguridadEntity, papelSeguridad => papelSeguridad.instituciones)
  readonly papelSeguridad: PapelSeguridadEntity[];

  @Column()
  idComuna: number;
  @OneToOne(() => ComunasEntity, comuna => comuna.instituciones)
  @JoinColumn({ name: 'idComuna' })
  readonly comunas: ComunasEntity;

  @ApiProperty()
  @OneToMany(() => OrdenTramiteExaminacionEntity, ordenTramiteExaminacion => ordenTramiteExaminacion.instituciones)
  readonly ordenTramiteExaminacion: OrdenTramiteExaminacionEntity[];

  @ApiProperty()
  @OneToMany(
    () => TipoDeTramiteClaseLicenciaInstitucionEntity,
    tipoTramiteLicenciaInstitucion => tipoTramiteLicenciaInstitucion.instituciones
  )
  readonly tipoTramiteLicenciaInstitucion: TipoDeTramiteClaseLicenciaInstitucionEntity[];

  @OneToOne(() => ComunasEntity, comunaAuxPermisos => comunaAuxPermisos.institucionesAuxPermisos)
  @JoinColumn({ name: 'idComuna' })
  readonly comunaAuxPermisos: ComunasEntity;

  @ApiProperty()
  @OneToMany(() => OficinasEntity, oficinas => oficinas.institucionesAuxPermisos)
  readonly oficinasAuxPermisos: OficinasEntity[];

}
