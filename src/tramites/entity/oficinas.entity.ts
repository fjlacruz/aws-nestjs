import { ApiProperty } from '@nestjs/swagger';
import { ClaseLicenciaRequeridaTipoTramite } from 'src/ClaseLicenciaRequeridaTipoTramite/entity/claseLicenciaRequeridaTipoTramite.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { DocsResolucionesEntity } from 'src/municipalidad/entity/docsResoluciones.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { RolTipoTramite } from 'src/rolTipoTramite/entity/rolTipoTramite.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TipoDeTramiteClaseLicenciaInstitucionEntity } from 'src/tipo-de-tramite-clase-licencia-institucion/entity/tipo-de-tramte-clase-licencia-institucion.entity';
import { OrdenTramiteExaminacionEntity } from 'src/tipos-tramites/entity/ordenTramiteExaminacion.entity';
import { InstitucionesEntity } from 'src/tramites/entity/instituciones.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm';
import { UsuarioOficinaEntity } from './usuarioOficina.entity';

@Entity('Oficinas')
export class OficinasEntity {
  @PrimaryGeneratedColumn()
  idOficina: number;

  @Column()
  idInstitucion: number;
  @OneToOne(() => InstitucionesEntity, instituciones => instituciones.oficinas)
  @JoinColumn({ name: 'idInstitucion' })
  readonly instituciones: InstitucionesEntity;

  @Column()
  idComuna: number;

  @OneToOne(() => ComunasEntity, comunas => comunas.oficinas)
  @JoinColumn({ name: 'idComuna' })
  readonly comunas: ComunasEntity;

  @Column()
  Nombre: string;

  @Column()
  Direccion: string;

  @Column()
  calleDireccion: string;
  @Column()
  nroDireccion: number;
  @Column()
  letraDireccion: string;
  @Column()
  restoDireccion: string;

  @Column()
  Descripcion: string;

  @ApiProperty()
  @OneToMany(() => UsuarioOficinaEntity, usuarioOficina => usuarioOficina.oficinas)
  readonly usuarioOficina: UsuarioOficinaEntity[];

  @ApiProperty()
  @OneToOne(() => RolesUsuarios, rolesUsuarios => rolesUsuarios.oficinas)
  readonly rolesUsuarios: RolesUsuarios[];

  @Column()
  creadoPor?: number;

  @Column()
  activo?: boolean;

  @Column()
  motivoRechazo?: string;

  @Column()
  fechaCreacion?: Date;

  // @Column()
  // directorTransito?:number;

  // @Column()
  // directorTransitoSubrogante?:number

  @Column()
  RUT?: number;

  @Column()
  DV?: string;

  @Column()
  codigoSRCeI?: string;

  @ApiProperty()
  @OneToMany(() => DocsResolucionesEntity, resoluciones => resoluciones.oficinas)
  readonly resoluciones: DocsResolucionesEntity[];

  @ApiProperty()
  @OneToMany(() => ClaseLicenciaRequeridaTipoTramite, licenciaRequerida => licenciaRequerida.oficinas)
  readonly licenciaRequerida: ClaseLicenciaRequeridaTipoTramite[];

  @ApiProperty()
  @OneToMany(() => RolTipoTramite, rolTipoTramite => rolTipoTramite.oficinas)
  readonly rolTipoTramite: RolTipoTramite[];

  @ApiProperty()
  @OneToMany(() => TipoDeTramiteClaseLicenciaInstitucionEntity, tipoTramiteLicenciaInstitucion => tipoTramiteLicenciaInstitucion.oficinas)
  readonly tipoTramiteLicenciaInstitucion: TipoDeTramiteClaseLicenciaInstitucionEntity[];

  @ApiProperty()
  @OneToMany(() => OrdenTramiteExaminacionEntity, ordenTramiteExaminacion => ordenTramiteExaminacion.oficinas)
  readonly ordenTramiteExaminacion: OrdenTramiteExaminacionEntity[];

  // @ApiProperty()
  // @OneToMany(()=> RegistroOficinaEntity,registros=> registros.oficinas)
  // readonly registros: RegistroOficinaEntity[];
  @ApiProperty()
  @OneToMany(() => Solicitudes, solicitudes => solicitudes.oficinas)
  readonly solicitudes: Solicitudes[];

  @ManyToOne(() => InstitucionesEntity, institucionesAuxPermisos => institucionesAuxPermisos.oficinasAuxPermisos)
  @JoinColumn({ name: 'idInstitucion' })
  readonly institucionesAuxPermisos: InstitucionesEntity;
}
