import { ApiProperty } from '@nestjs/swagger';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { ImagenesPostulanteEntity } from 'src/imagenes-postulante/entity/ImagenesPostulante.entity';
import { User } from 'src/users/entity/user.entity';
import { CertificadoEscuelaConductorEntity } from 'src/certificado-escuela-conductor/entity/certificado-escuela-conductor.entity';
import { OpcionSexoEntity } from 'src/opciones-sexo/entity/sexo.entity';
import { OpcionEstadosCivilEntity } from 'src/opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { OpcionesNivelEducacional } from 'src/opciones-nivel-educacional/entity/nivel-educacional.entity';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { CertificadosPostulante } from 'src/consulta-historica/entity/certificadosPostulante.entity';

@Entity('Postulantes')
export class PostulantesEntity {
  @PrimaryGeneratedColumn()
  idPostulante: number;

  @Column()
  RUN: number;

  @Column()
  DV: string;

  @Column()
  Nombres: string;

  @Column()
  ApellidoPaterno: string;

  @Column()
  ApellidoMaterno: string;

  @Column()
  Calle: string;

  @Column()
  CalleNro: number;

  @Column()
  Letra: string;

  @Column()
  RestoDireccion: string;

  @Column()
  Nacionalidad: string;

  @Column()
  Email: string;

  @Column()
  Profesion: string;

  @Column()
  Diplomatico: boolean;

  @Column()
  discapacidad: boolean;

  @Column()
  DetalleDiscapacidad: string;

  @Column()
  updated_at: Date;

  @Column()
  created_at: Date;

  @Column()
  idUsuario: number;
  @OneToOne(() => User, usuario => usuario.postulante)
  @JoinColumn({ name: 'idUsuario' })
  readonly usuario: User;

  @Column()
  Telefono: string;

  @Column()
  FechaNacimiento: Date;

  @Column()
  FechaDefuncion: Date;

  @Column()
  idRegion: number;
  @OneToOne(() => RegionesEntity, region => region.idRegion)
  @JoinColumn({ name: 'idRegion' })
  readonly region: RegionesEntity;

  @Column()
  idComuna: number;
  @OneToOne(() => ComunasEntity, comunas => comunas.postulante)
  @JoinColumn({ name: 'idComuna' })
  readonly comunas: ComunasEntity;

  @Column()
  idOpcionSexo: number;
  @ManyToOne(() => OpcionSexoEntity, opcionSexo => opcionSexo.postulante)
  @JoinColumn({ name: 'idOpcionSexo' })
  readonly opcionSexo: OpcionSexoEntity;

  @Column()
  idOpcionEstadosCivil: number;
  @ManyToOne(() => OpcionEstadosCivilEntity, opcionEstadosCivil => opcionEstadosCivil.postulante)
  @JoinColumn({ name: 'idOpcionEstadosCivil' })
  readonly opcionEstadosCivil: OpcionEstadosCivilEntity;

  @Column()
  idOpNivelEducacional: number;
  @ManyToOne(() => OpcionesNivelEducacional, opcionesNivelEducacional => opcionesNivelEducacional.postulante)
  @JoinColumn({ name: 'idOpNivelEducacional' })
  readonly opcionesNivelEducacional: OpcionesNivelEducacional;

  @ApiProperty()
  @OneToMany(() => Solicitudes, solicitudes => solicitudes.postulante)
  readonly solicitudes: Solicitudes[];

  @ApiProperty()
  @OneToOne(() => ConductoresEntity, conductor => conductor.postulante)
  readonly conductor: ConductoresEntity;

  @ApiProperty()
  @OneToMany(() => ImagenesPostulanteEntity, imagenesPostulante => imagenesPostulante.postulante)
  readonly imagenesPostulante: ImagenesPostulanteEntity[];

  @ApiProperty()
  @OneToOne(() => CertificadoEscuelaConductorEntity, certificadoEscuelaConductor => certificadoEscuelaConductor.postulante)
  readonly certificadoEscuelaConductor: CertificadoEscuelaConductorEntity;

  @ApiProperty()
  @OneToMany(() => CertificadosPostulante, certificadosPostulante => certificadosPostulante.postulante)
  readonly certificadosPostulante: CertificadosPostulante[];
}
