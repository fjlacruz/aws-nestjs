import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('Solicitudes')
export class Solicitudes {

    @PrimaryGeneratedColumn()
    idSolicitud: number;
    
    @Column()
    idTipoSolicitud: number;

    @Column()
    idPostulante:number;

    @Column()
    idUsuario:number;

    @Column()
    idOficina: number;

    @Column()
    idEstado: number;

    @Column()
    idInstitucion: number;
    
    @Column()
    FechaCreacion: Date;

    @Column()
    updated_at: Date;

    @Column()
    FechaCierre: Date;
}