import {Entity, PrimaryGeneratedColumn, Column, PrimaryColumn} from "typeorm";

@Entity('TiposTramite')
export class TiposTramiteEntity {

    @PrimaryColumn()
    idTipoTramite:number;

    @Column()
    Nombre:string;

    @Column()
    Descripcion:string;
}

