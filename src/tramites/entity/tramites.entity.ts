import { ApiProperty } from '@nestjs/swagger';
import { ColaExaminacion } from 'src/tramites/entity/cola-examinacion.entity';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  ManyToOne,
  JoinColumn,
  OneToOne,
  OneToMany,
} from 'typeorm';
import { EstadosTramiteEntity } from './estados-tramite.entity';
import { DocumentoLicenciaTramiteEntity } from 'src/documento-licencia-tramite/entity/documento-licencia-tramite.entity';
import { MotivosDenegacionEntity } from 'src/motivos-denegacion/entity/motivo-denegacion.entity';
import { ColaExaminacionTramiteNMEntity } from './cola-examinacion-tramite-n-m.entity';
import { ProrrogaTramiteEntity } from 'src/prorrogas/entities/prorroga-tramite.entity';

@Entity('Tramites')
export class TramitesEntity {
  @PrimaryGeneratedColumn()
  idTramite: number;

  @Column()
  idSolicitud: number;

  @ManyToOne(() => Solicitudes, (solicitudes) => solicitudes.tramites)
  @JoinColumn({ name: 'idSolicitud' })
  readonly solicitudes: Solicitudes;

  @Column()
  idTipoTramite: number;

  @ManyToOne(() => TiposTramiteEntity, (tiposTramite) => tiposTramite.tramites)
  @JoinColumn({ name: 'idTipoTramite' })
  readonly tiposTramite: TiposTramiteEntity;

  @Column()
  idEstadoTramite: number;

  @ManyToOne(() => EstadosTramiteEntity, (estadoTramite) => estadoTramite.tramites)
  @JoinColumn({ name: 'idEstadoTramite' })
  readonly estadoTramite: EstadosTramiteEntity;

  @Column({
    nullable: true
  })
  observacion: string;

  @Column({
    nullable: true
  })
  created_at: Date;

  @Column({
    nullable: true
  })
  update_at: Date;

  @Column({
    nullable: true
  })
  fechaRecepcionConforme: Date;
  
  @Column({
    nullable: true
  })
  fechaReprobacion: Date;
  
  @Column({
    nullable: true
  })
  plazoDenegacion: string;
  
  @Column({
    nullable: true
  })
  tipoDenegacion: string;
  
  @Column({
    nullable: true
  })
  motivoAdicional: string;

  @Column()
  idMotivoDenegacion: number;
  @ManyToOne(() => MotivosDenegacionEntity, motivoDenegacion => motivoDenegacion.tramite)
  @JoinColumn({ name: 'idMotivoDenegacion' })
  readonly motivoDenegacion: MotivosDenegacionEntity;

  @ApiProperty()
  @OneToOne(
    () => DocumentosTramitesEntity,
    (documentosTramites) => documentosTramites.tramite,
  )
  readonly documentosTramites: DocumentosTramitesEntity;


  @OneToOne(() => TramitesClaseLicencia, tramitesClaseLicencia => tramitesClaseLicencia.tramite)
  readonly tramitesClaseLicencia: TramitesClaseLicencia;

  @ApiProperty()
  @OneToMany(() => ColaExaminacion, (colaExaminacion) => colaExaminacion.tramites)
  readonly colaExaminacion: ColaExaminacion[];
  
  @ApiProperty()
  @OneToMany(() => DocumentoLicenciaTramiteEntity, documentoLicenciaTramite => documentoLicenciaTramite.tramites)
  readonly documentoLicenciaTramite: DocumentoLicenciaTramiteEntity[];

  @ApiProperty()
  @OneToMany(() => ColaExaminacionTramiteNMEntity, (colaExaminacionNM) => colaExaminacionNM.tramites)
  readonly colaExaminacionNM: ColaExaminacionTramiteNMEntity[];

  @ApiProperty()
  @OneToMany(() => ColaExaminacionTramiteNMEntity, (colaExaminacionNM) => colaExaminacionNM.tramitesAux)
  readonly colaExaminacionNMAux: ColaExaminacionTramiteNMEntity[];

  @ApiProperty()
  @OneToMany(() => ProrrogaTramiteEntity, (prorrogaTramite) => prorrogaTramite.tramites)
  readonly prorrogaTramite: ProrrogaTramiteEntity[];
}
