import { ComunasEntity } from "src/comunas/entity/comunas.entity";
import { User } from "src/users/entity/user.entity";
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany, ManyToOne} from "typeorm";
import { OficinasEntity } from "./oficinas.entity";

@Entity('UsuarioOficina')

export class UsuarioOficinaEntity{

    @PrimaryGeneratedColumn()
    idUsuarioOficina:number;

    @Column()
    idUsuario:number;
    @ManyToOne(() => User, usuario => usuario.usuarioOficina)
    @JoinColumn({ name: 'idUsuario' })
    readonly usuario: User;

    @Column()
    idOficina:number;
    @ManyToOne(() => OficinasEntity, oficinas => oficinas.usuarioOficina)
    @JoinColumn({ name: 'idOficina' })
    readonly oficinas: OficinasEntity;
}
