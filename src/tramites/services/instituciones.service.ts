import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';


import { User } from 'src/users/entity/user.entity';


import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { OficinaEntity } from 'src/oficina/oficina.entity';
import { InstitucionesEntity } from '../entity/instituciones.entity';
import { TipoInstitucionEntity } from 'src/tipo-institucion/entity/tipo-institucion.entity';
import { Roles } from 'src/roles/entity/roles.entity';

@Injectable()
export class InstitucionesService {
  constructor(
    @InjectRepository(User)
    private readonly userRespository: Repository<User>,

  ) { }

  async getInstitucionesByIdUser(id: number) {
    return this.userRespository
      .createQueryBuilder('Usuarios')
 
     
      .innerJoinAndMapMany('Usuarios.rolesU', RolesUsuarios, 'ru', 'ru.idUsuario = Usuarios.idUsuario')
      .innerJoinAndMapOne('ru.idRol', Roles, 'r', 'ru.idRol = r.idRol')
      .innerJoinAndMapOne('ru.idOficina', OficinaEntity, 'ofi', 'ru.idOficina = ofi.idOficina')
      .innerJoinAndMapOne('ofi.idInstitucion', InstitucionesEntity, 'inst', 'inst.idInstitucion = ofi.idInstitucion')
      .innerJoinAndMapOne('inst.idTipoInstitucion', TipoInstitucionEntity, 'tpoInst', 'inst.idTipoInstitucion = tpoInst.idTipoInstitucion')
     
      .where('Usuarios.idUsuario = :id', { id: id })
      .andWhere('tpoInst.idTipoInstitucion = 2')
      .getOne();
  }

  /*  /* .select('Usuarios.RUN', 'RUN')
      .addSelect('inst.Descripcion', 'institucion')
      .addSelect('tpoInst.Nombre', 'tipoInstitucion') */
      /* .addSelect('estado.Nombre', 'Nombre')
      .addSelect('estado.Descripcion', 'Descripcion') */
     /*  .innerJoin( RolesUsuarios, 'ru', 'ru.idUsuario = Usuarios.idUsuario')
      .innerJoin(OficinaEntity, 'ofi', 'ru.idOficina = ofi.idOficina')
      .innerJoin(InstitucionesEntity, 'inst', 'inst.idInstitucion = ofi.idInstitucion')
      .innerJoin(TipoInstitucionEntity, 'tpoInst', 'inst.idTipoInstitucion = tpoInst.idTipoInstitucion') */
    /*   .innerJoinAndMapMany('Usuarios.RolesUsuario',RolesUsuarios, 'ru', 'ru.idUsuario = Usuarios.idUsuario')
      .innerJoinAndMapMany("ru.oficinas",OficinaEntity, 'ofi', 'ru.idOficina = ofi.idOficina')
      .innerJoinAndMapMany("ofi.Instituciones",InstitucionesEntity, 'inst', 'inst.idInstitucion = ofi.idInstitucion')
      .innerJoinAndMapMany("inst.tipo",TipoInstitucionEntity, 'tpoInst', 'inst.idTipoInstitucion = tpoInst.idTipoInstitucion') 
      .where('Usuarios.idUsuario = :id', { id: id }) * */

}
