import { Test, TestingModule } from '@nestjs/testing';
import { TramitesController } from './tramites.service';

describe('TramitesController', () => {
  let service: TramitesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TramitesController],
    }).compile();

    service = module.get<TramitesController>(TramitesController);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
