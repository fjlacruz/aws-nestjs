import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { BASE_URL_BACK_MOBILE, BASE_URL_REGISTRO_CIVIL, URL_MOBILE_CREAR_LICENCIA, URL_REGISTRO_CIVIL_ESTADO_RPI_POSTULANTE } from 'src/approutes.config';

import {
  ColumnasTramites,
  estadosTramitesEnProceso,
  estadosTramitesEspera,
  relacionTramiteConExamenes,
  relacionTramiteSinExamenes,
  estadoTramitesEsperaRecepcionConforme,
  tipoDocumentoRecepcionConforme,
  estadosTramitesOtorgamiento,
  estadosTramitesOtorgado,
  estadoTramitesEsperaDenegacionLicencia,
  tipoDocumentoDenegacion,
  relacionTramiteConductor,
  tipoDocumentoOtorgamiento,
  estadoTramitesRecepcionConforme,
  relacionTramiteSinExamenesLicencia,
  estadosTramitesEsperaRegistro,
  TipoExaminaciones,
  EstadoSolicitudAbierta,
  EstadoSolicitudCerrada,
  estadoTramitesEsperaRecepcionConformeId,
  estadosTramitesOtorgamientoId,
  estadoTramitesEsperaDenegacionLicenciaId,
  estadosTramitesOtorgadoEnImpresionId,
  TipoExaminacionesIds,
  PermisosNombres,
  CodigoSolicitudAbierta,
  CodigoSolicitudCerrada,
  CodigoEstadoTramitesDesistido,
  CodigoEstadoTramitesAbandonado,
  CodigoEstadoTramitesDenegacionSML,
  CodigoEstadoTramitesDenegacionJPL,
  CodigoEstadoTramitesRecepcionConformeFisico,
  CodigoEstadoTramitesRecepcionConformeFisicoDigital,
  CodigoEstadoTramitesCanceladoCerrado,
} from 'src/constantes';
import { ClasesLicenciasEntity } from 'src/clases-licencia/entity/clases-licencias.entity';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { Resultado } from 'src/utils/resultado';
import { Brackets, QueryRunner, Repository } from 'typeorm';
import { CreateTramiteDto } from '../DTO/createTramite.dto';
import { TramiteRecepcionDto } from '../DTO/tramiteRecepcion.dto';
import { TramitesEnProcesoDto } from '../DTO/tramitesEnProceso.dto';
import { TramitesConLicenciaDto } from '../DTO/tramitesConLicencia.dto';
import { EstadosSolicitudEntity } from '../entity/estados-solicitud.entity';
import { EstadosTramiteEntity } from '../entity/estados-tramite.entity';
import { InstitucionesEntity } from '../entity/instituciones.entity';
import { OficinasEntity } from '../entity/oficinas.entity';
import { PostulantesEntity } from '../entity/postulantes.entity';
import { TramitesEntity } from '../entity/tramites.entity';
import { OtorgaLicenciaArchivoDto } from '../DTO/otorgaLicenciaArchivo.dto';
import { getConnection } from 'typeorm';
import { TiposDocumentoEntity } from 'src/documentos-tramites/entity/tipos-documento.entity';
import { DenegarLicenciaArchivoDto } from '../DTO/denegarLicenciaArchivo.dto';
import { DeniegaLicenciaDto } from '../DTO/deniegaLicencia.dto';
import { UtilService } from 'src/utils/utils.service';
import { TramiteFotosLicenciaDto } from '../DTO/tramiteFotosLicencia.dto';
import { PapelSeguridadEntity } from 'src/papel-seguridad/entity/papel-seguridad.entity';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { EstadosPCLEntity } from 'src/estados-PCL/entity/estados-PCL.entity';
import { EstadosEDDEntity } from 'src/estados-EDD/entity/estados-EDD.entity';
import { EstadosEDFEntity } from 'src/estados-EDF/entity/estados-EDF.entity';
import { DocumentosLicenciaClaseLicenciaEntity } from 'src/documento-licencia/entity/documento-licencia.entity';
import { ImagenLicenciaEntity } from 'src/imagen-licencia/entity/imagen-licencia.entity';
import { TipoImagenLicenciaEntity } from 'src/imagen-licencia/entity/tipo-imagen-licencia.entity';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { DatosLicenciaDto } from '../DTO/datosLicencia.dto';
import { FechasLicenciaDto } from '../DTO/fechasLicencia.dto';
import { User } from 'src/users/entity/user.entity';
import { DocumentoLicenciaMobileDto } from 'src/documento-licencia/DTO/documento-licencia-mobile';
import { LicenciaConConductorDto } from 'src/documento-licencia/DTO/licencia-con-conductor';
import { ConductorMobileDTO } from 'src/conductor/DTO/conductor-mobile.dto';
import { PapelSeguridadFolioEntity } from 'src/folio/entity/papelSeguridad.entity';
import { DocumentoLicenciaTramiteEntity } from 'src/documento-licencia-tramite/entity/documento-licencia-tramite.entity';
import { MotivosDenegacionEntity } from 'src/motivos-denegacion/entity/motivo-denegacion.entity';
import { AuthService } from 'src/auth/auth.service';
import { plantillaCorreo } from 'src/utils/cuerpoCorreos';
import { PostulanteEntity } from 'src/postulante/postulante.entity';
import { ColaExaminacionEntity } from 'src/cola-examinacion/entity/cola-examinacion.entity';
import { SolicitudDTO } from 'src/solicitudes/DTO/solicitud.dto';
import { Solicitudes } from 'src/solicitudes/entity/solicitudes.entity';
import { TramiteDTO } from '../DTO/tramite.dto';
import { ColaExaminacionTramiteNMEntity } from '../entity/cola-examinacion-tramite-n-m.entity';
import { RecepcionConformePrecarga, RecepcionConformePrecargaDetalleFechas } from '../DTO/recepcionConformePrecarga.dto';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { CambioestadorceiService } from 'src/cambio-estadorcei/services/cambioestadorcei/cambioestadorcei.service';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';
import { ServicioGenerarLicenciasService } from 'src/shared/services/ServiciosComunes/ServicioGenerarLicencias.services';

import { DatosClasesLicenciasConFechas } from '../DTO/datosClasesLicenciasConFechas.dto';
import { DocumentoLicenciaClaseLicenciaMobileDto } from 'src/documento-licencia/DTO/documento-licencia-clase-licencia-mobile';
import { DocumentosLicenciaRestriccionEntity } from 'src/documento-licencia-restriccion/entity/documento-licencia-restriccion.entity';
import { TipoExaminacionesEntity } from 'src/resultado-examinacion/entity/TipoExaminaciones.entity';
import { DenegacionesXOtorgamientosPendientes } from '../DTO/DenegacionesXOtorgamientosPendientes.dto';
import { devolverValorRUN, formatearRun } from 'src/shared/ValidarRun';
import {
  DenegacionesOtorgamientosPendientesListadoDTO,
  DenegacionesOtorgamientosTramitesDTO,
} from '../DTO/DenegacionesOtorgamientosPendientesListadoDTO.dto';
import { paginationMeta, paginationResult } from 'src/shared/Entities/pagination';

import * as moment from 'moment';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { transform } from 'lodash';
import { LotePapelSeguridadEntity } from 'src/folio/entity/lotePapelSeguridad.entity';
import { Proveedor } from 'src/proveedor/entities/proveedor.entity';

@Injectable()
export class TramitesService {
  constructor(
    @InjectRepository(TramitesEntity)
    private readonly tramitesRespository: Repository<TramitesEntity>,

    @InjectRepository(TipoImagenLicenciaEntity)
    private readonly tipoImagenLicenciaEntityRespository: Repository<TipoImagenLicenciaEntity>,

    @InjectRepository(Solicitudes)
    private readonly solicitudesEntityRespository: Repository<Solicitudes>,

    @InjectRepository(TramitesClaseLicencia)
    private readonly tramitesClaseLicenciaRespository: Repository<TramitesClaseLicencia>,

    @InjectRepository(PostulantesEntity)
    private readonly postulanteRespository: Repository<PostulantesEntity>,

    @InjectRepository(DocumentosLicencia)
    private readonly documentosLicenciaRespository: Repository<DocumentosLicencia>,

    @InjectRepository(PapelSeguridadFolioEntity)
    private readonly papelSeguridadRespository: Repository<PapelSeguridadFolioEntity>,

    @InjectRepository(EstadosPCLEntity)
    private readonly EstadosPCLRespository: Repository<EstadosPCLEntity>,

    @InjectRepository(EstadosEDDEntity)
    private readonly EstadosEDDRespository: Repository<EstadosEDDEntity>,

    @InjectRepository(EstadosEDFEntity)
    private readonly EstadosEDFRespository: Repository<EstadosEDFEntity>,

    @InjectRepository(EstadosTramiteEntity)
    private readonly estadosTramites: Repository<EstadosTramiteEntity>,

    @InjectRepository(User)
    private readonly userRepository: Repository<User>,

    private readonly utilService: UtilService,
    private readonly authService: AuthService,
    private cambioestadorceiService: CambioestadorceiService,
    private readonly ingresoPostulanteService: IngresoPostulanteService,
    private servicioGenerarLicenciasService: ServicioGenerarLicenciasService
  ) {}

  async getTramitesByRUNPostulante(run: number) {
    return this.tramitesRespository
      .createQueryBuilder('Tramites')
      .innerJoinAndMapMany('Tramites.idSolicitud', Solicitudes, 'sol', 'Tramites.idSolicitud = sol.idSolicitud')
      .innerJoinAndMapMany('sol.idPostulante', PostulantesEntity, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoinAndMapMany('sol.idOficina', OficinasEntity, 'ofi', 'sol.idOficina = ofi.idOficina')
      .innerJoinAndMapMany('sol.idEstado', EstadosSolicitudEntity, 'est', 'sol.idEstado = est.idEstado')
      .innerJoinAndMapMany('sol.idInstitucion', InstitucionesEntity, 'ins', 'sol.idInstitucion = ins.idInstitucion')
      .innerJoinAndMapMany('Tramites.idTipoTramite', TiposTramiteEntity, 'tipo', 'Tramites.idTipoTramite = tipo.idTipoTramite')
      .innerJoinAndMapMany('Tramites.idEstadoTramite', EstadosTramiteEntity, 'esttra', 'Tramites.idEstadoTramite = esttra.idEstadoTramite')
      .innerJoinAndMapOne('Tramites.TramiteClaseLicencia', TramitesClaseLicencia, 'tralic', 'Tramites.idTramite = tralic.idTramite')
      .innerJoinAndMapOne('Tramites.ClaseLicencia', ClasesLicenciasEntity, 'lic', 'tralic.idClaseLicencia = lic.idClaseLicencia')
      .where('pos.RUN = :id', { id: run })
      .orderBy('Tramites.idSolicitud')
      .getMany();
  }

  public getTramitesByIdPostulante(id: number): Promise<any> {
    return this.tramitesRespository
      .createQueryBuilder('Tramites')
      .innerJoinAndMapMany('Tramites.idSolicitud', Solicitudes, 'sol', 'Tramites.idSolicitud = sol.idSolicitud')
      .innerJoinAndMapMany('sol.idPostulante', PostulantesEntity, 'pos', 'sol.idPostulante = pos.idPostulante')
      .innerJoinAndMapMany('sol.idOficina', OficinasEntity, 'ofi', 'sol.idOficina = ofi.idOficina')
      .innerJoinAndMapMany('sol.idEstado', EstadosSolicitudEntity, 'est', 'sol.idEstado = est.idEstado')
      .innerJoinAndMapMany('sol.idInstitucion', InstitucionesEntity, 'ins', 'sol.idInstitucion = ins.idInstitucion')
      .innerJoinAndMapMany('Tramites.idTipoTramite', TiposTramiteEntity, 'tipo', 'Tramites.idTipoTramite = tipo.idTipoTramite')
      .innerJoinAndMapMany('Tramites.idEstadoTramite', EstadosTramiteEntity, 'esttra', 'Tramites.idEstadoTramite = esttra.idEstadoTramite')
      .innerJoinAndMapMany('Tramites.idTramiteClaseLicencia', TramitesClaseLicencia, 'tralic', 'Tramites.idTramite = tralic.idTramite')
      .innerJoinAndMapMany('tralic.idClaseLicencia', ClasesLicenciasEntity, 'lic', 'tralic.idClaseLicencia = lic.idClaseLicencia')
      .where('sol.idPostulante = :id', { id: id })
      .getMany();
  }

  async create(createTramiteDto: CreateTramiteDto): Promise<TramitesEntity> {
    return await this.tramitesRespository.save(createTramiteDto);
  }

  async getTramitesSolicitud(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    return await this.getTramites(paginacionArgs, estadosTramitesEnProceso, relacionTramiteConExamenes);
  }

  async getPostulantesEnEspera(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    return await this.getTramites(paginacionArgs, estadosTramitesEspera, relacionTramiteConExamenes);
  }

  async getOtorgamientoDenegacion(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    return await this.getTramitesPendientesOtorgarDenegar(paginacionArgs);
  }

  async getVisualizacionLicencias(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    //return await this.getTramites(paginacionArgs, estadosTramitesOtorgado, relacionTramiteSinExamenesLicencia);

    const estadosOtorgadoEnImpresion: number[] = [estadosTramitesOtorgadoEnImpresionId];

    return await this.getTramitesV2(paginacionArgs, relacionTramiteSinExamenesLicencia, estadosOtorgadoEnImpresion);
  }

  async getRecepcionConforme(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const estadosEnRecepcionConforme: number[] = [estadoTramitesEsperaRecepcionConformeId];

    return await this.getTramitesV2(paginacionArgs, relacionTramiteSinExamenes, estadosEnRecepcionConforme);
  }

  async getTramites(paginacionArgs: PaginacionArgs, estadosTramites, relations): Promise<Resultado> {
    const idOficina = this.authService.oficina().idOficina;
    const resultado = new Resultado();
    const orderBy = ColumnasTramites[paginacionArgs.orden.orden];

    let tramitesCLPaginados: Pagination<TramitesClaseLicencia>;
    let tramitesParseados = new PaginacionDtoParser<TramitesEnProcesoDto>();
    let filtro = '';
    let filtroString = '';

    try {
      if (paginacionArgs.filtro != null && paginacionArgs.filtro != '') {
        filtro = paginacionArgs.filtro;
        filtroString = filtro as string;
      }

      tramitesCLPaginados = await paginate<TramitesClaseLicencia>(this.tramitesClaseLicenciaRespository, paginacionArgs.paginationOptions, {
        relations: relations,
        where: qb => {
          qb.where('TramitesClaseLicencia__tramite__estadoTramite.Nombre IN(:...estadoTramites)', { estadoTramites: estadosTramites })
            .andWhere('TramitesClaseLicencia__tramite__solicitudes.idOficina = :idOficina', { idOficina })
            .andWhere(
              new Brackets(subQb => {
                subQb
                  .where(
                    "lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.Nombres)) || ' ' || lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoPaterno)) || ' ' || lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoMaterno)) like lower(unaccent(:nombre))",
                    { nombre: `%${filtroString}%` }
                  )
                  .orWhere(
                    "lower(unaccent(TramitesClaseLicencia__tramite__tiposTramite.Nombre)) || ' ' || lower(unaccent(TramitesClaseLicencia__clasesLicencias.Abreviacion)) like lower(unaccent(:tipoTramite))",
                    { tipoTramite: `%${filtroString}%` }
                  )
                  .orWhere('lower(unaccent(TramitesClaseLicencia__tramite__estadoTramite.Nombre)) like lower(unaccent(:estadoTramite))', {
                    estadoTramite: `%${filtroString}%`,
                  })
                  .orWhere(
                    "TramitesClaseLicencia__tramite__solicitudes__postulante.RUN || '-' || TramitesClaseLicencia__tramite__solicitudes__postulante.DV like :RUN",
                    { RUN: `%${filtro}%` }
                  )
                  .orWhere('CAST(TramitesClaseLicencia__tramite.idSolicitud AS TEXT) like :idSolicitud', { idSolicitud: `%${filtro}%` });
              })
            )
            .orderBy(orderBy, paginacionArgs.orden.ordenarPor);
        },
      });

      let tramitesDTO: TramitesEnProcesoDto[] = [];

      if (Array.isArray(tramitesCLPaginados.items) && tramitesCLPaginados.items.length) {
        tramitesCLPaginados.items.forEach(tramiteCL => {
          const tramiteDTO: TramitesEnProcesoDto = new TramitesEnProcesoDto();
          tramiteDTO.idSolicitud = tramiteCL.tramite.idSolicitud;
          tramiteDTO.idTramite = tramiteCL.tramite.idTramite;
          tramiteDTO.run = tramiteCL.tramite.solicitudes.postulante.RUN.toString() + '-' + tramiteCL.tramite.solicitudes.postulante.DV;
          tramiteDTO.nombrePostulante =
            tramiteCL.tramite.solicitudes.postulante.Nombres +
            ' ' +
            tramiteCL.tramite.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            tramiteCL.tramite.solicitudes.postulante.ApellidoMaterno;
          tramiteDTO.tipoTramite = tramiteCL.tramite.tiposTramite.Nombre;
          tramiteDTO.estadoTramite = tramiteCL.tramite.estadoTramite.Nombre;
          tramiteDTO.estado = tramiteCL.tramite.estadoTramite.Nombre;
          tramiteDTO.estadoSolicitud = tramiteCL.tramite.solicitudes.estadosSolicitud.Nombre;
          tramiteDTO.municipalidadSolicitudString = tramiteCL.tramite.solicitudes.oficinas
            ? tramiteCL.tramite.solicitudes.oficinas.instituciones.Nombre
            : '';

          if (tramiteCL.clasesLicencias) {
            tramiteDTO.claseLicencia = tramiteCL.clasesLicencias.Abreviacion;
            tramiteDTO.idClaseLicencia = tramiteCL.clasesLicencias.idClaseLicencia;
          } else tramiteDTO.claseLicencia = '';

          tramiteDTO.examenTeorico = 0;
          tramiteDTO.examenPractico = 0;
          tramiteDTO.examenMedico = 0;
          tramiteDTO.idoneidadMoral = 0;
          if (tramiteCL.tramite.colaExaminacionNM && tramiteCL.tramite.colaExaminacionNM.length > 0) {
            tramiteCL.tramite.colaExaminacionNM.forEach(examinacion => {
              if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenTeorico)
                tramiteDTO.examenTeorico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenPractico)
                tramiteDTO.examenPractico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenMedico)
                tramiteDTO.examenMedico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenIM)
                tramiteDTO.idoneidadMoral =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
            });
          }

          tramitesDTO.push(tramiteDTO);
        });

        tramitesParseados.items = tramitesDTO;
        tramitesParseados.meta = tramitesCLPaginados.meta;
        tramitesParseados.links = tramitesCLPaginados.links;

        resultado.Respuesta = tramitesParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }

  async getLicenciasOtorgarByRUN(run: string): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    let tramites: TramitesClaseLicencia[];

    try {
      tramites = await this.tramitesClaseLicenciaRespository.find({
        relations: relacionTramiteSinExamenes,
        where: qb => {
          qb.where('TramitesClaseLicencia__tramite__estadoTramite.Nombre IN(:...estadoTramite)', {
            estadoTramite: estadosTramitesOtorgamiento,
          })
            .andWhere(
              "TramitesClaseLicencia__tramite__solicitudes__postulante.RUN || '-' || TramitesClaseLicencia__tramite__solicitudes__postulante.DV = :RUN",
              { RUN: run }
            )
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 5 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 5) < 1)' +
                ')'
            )
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 4 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 4) < 1)' +
                ')'
            )
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 3 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 3) < 1)' +
                ')'
            )
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 2 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 2) < 1)' +
                ')'
            );
        },
      });

      let tramitesDTO: TramitesConLicenciaDto[] = [];

      if (tramites.length > 0) {
        tramites.forEach(tramite => {
          const tramiteDTO: TramitesConLicenciaDto = new TramitesConLicenciaDto();
          tramiteDTO.id = tramite.idTramite;
          tramiteDTO.run = tramite.tramite.solicitudes.postulante.RUN.toString() + '-' + tramite.tramite.solicitudes.postulante.DV;
          tramiteDTO.nombrePostulante =
            tramite.tramite.solicitudes.postulante.Nombres +
            ' ' +
            tramite.tramite.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            tramite.tramite.solicitudes.postulante.ApellidoMaterno;
          tramiteDTO.claseLicencia = tramite.clasesLicencias.Abreviacion;
          tramiteDTO.idClaseLicencia = tramite.clasesLicencias.idClaseLicencia;

          tramitesDTO.push(tramiteDTO);
        });

        resultado.Respuesta = tramitesDTO;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }

  // Servicio que devuelve la fecha del proximo control y observaciones si ya ha sido otorgada la licencia previamente
  async traerDatosExistentes(req, idTramitesOtorgamiento: number[]) {
    const resultado: Resultado = new Resultado();
    let usuarioConectado = new User();

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaOtorgamientosDenegaciones
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      const idUsuarioConectado = (await this.authService.usuario()).idUsuario;

      await queryRunner.connect();
      //await queryRunner.startTransaction();

      // Se coje el usuario correspondiente al token
      if (idUsuarioConectado > 0) {
        usuarioConectado = await this.userRepository.findOne({ idUsuario: idUsuarioConectado });
      } else {
        resultado.Error = 'Error: el token no coincide con un usuario';
        console.log('Error: el token no coincide con un usuario');
        resultado.ResultadoOperacion = false;
        return resultado;
      }

      // Se comprueba si viene algún trámite
      if (idTramitesOtorgamiento == null || idTramitesOtorgamiento.length < 1) {
        resultado.Error = 'Error: no se han especificado trámites para recepción conforme';
        resultado.ResultadoOperacion = false;
        return resultado;
      }

      // 1 Buscamos las observaciones y el proximo control del otorgamiento de la licencia previo

      const tramiteAsociado = await queryRunner.manager.find(TramitesEntity, {
        relations: [
          'colaExaminacionNM',
          'colaExaminacionNM.colaExaminacion',
          'colaExaminacionNM.colaExaminacion.resultadoExaminacionCola',
          'colaExaminacionNM.colaExaminacion.resultadoExaminacionCola.resultadoExaminacion',
          'colaExaminacionNM.colaExaminacion.resultadoExaminacionCola.resultadoExaminacion.respuestasFormularioExaminacion',
          'tramitesClaseLicencia',
          'tramitesClaseLicencia.clasesLicencias',
          'solicitudes',
          'solicitudes.postulante',
          'solicitudes.postulante.conductor',
          'solicitudes.tramites',
          'solicitudes.tramites.tramitesClaseLicencia',
          'solicitudes.tramites.tramitesClaseLicencia.clasesLicencias',
          'solicitudes.tramites.colaExaminacionNM',
          'solicitudes.tramites.colaExaminacionNM.colaExaminacion',
          'solicitudes.tramites.colaExaminacionNM.colaExaminacion.resultadoExaminacionCola',
          'solicitudes.tramites.colaExaminacionNM.colaExaminacion.resultadoExaminacionCola.resultadoExaminacion',
          'solicitudes.tramites.colaExaminacionNM.colaExaminacion.resultadoExaminacionCola.resultadoExaminacion.respuestasFormularioExaminacion',
        ],
        where: qb => {
          qb.where('TramitesEntity.idTramite IN (:...idTramite)', { idTramite: idTramitesOtorgamiento });
        },
      });

      // Se comprueba si viene algún trámite
      if (!tramiteAsociado && tramiteAsociado.length < 1) {
        resultado.Error =
          'Error: no se han especificado trámites para recepción conforme o no existen trámites asociados a los consultados.';
        resultado.ResultadoOperacion = false;
        return resultado;
      }

      //Primero se comprueba si el médico puso fecha de control ( si existe exámen médico)
      //const examinacionMedica = [];

      // Inicializamos el objeto de respuesta
      let respuestaObservacionesFechas: RecepcionConformePrecarga = new RecepcionConformePrecarga();
      respuestaObservacionesFechas.DetalleFechasClasesLicencia = [];
      respuestaObservacionesFechas.Observaciones = '';

      // Inicializamos array de ids de restricciones
      let idsRestriccionesInteger: number[] = [12]; // 12 en principio sin restricciones

      respuestaObservacionesFechas.DetalleFechasClasesLicencia = [];

      tramiteAsociado.forEach(async tramAsoc => {
        // Recorremos cada tramite asociado para recuperar las respuestas de examinación médica
        const examinacionMedica = tramAsoc.colaExaminacionNM
          .filter(x => x.colaExaminacion.idTipoExaminacion == TipoExaminacionesIds.ExamenMedico)
          .map(x => x.colaExaminacion);

        if (
          examinacionMedica &&
          examinacionMedica != null &&
          examinacionMedica[0].resultadoExaminacionCola &&
          examinacionMedica[0].resultadoExaminacionCola &&
          examinacionMedica[0].resultadoExaminacionCola.filter(x => x.resultadoExaminacion.respuestasFormularioExaminacion.length > 0)
            .length > 0
        ) {
          let respuestasExaminacion = examinacionMedica[0].resultadoExaminacionCola.filter(
            x => x.resultadoExaminacion.idFormularioExaminaciones == 15
          )[0].resultadoExaminacion.respuestasFormularioExaminacion;

          // Recogemos el tipo de pregunta multirrespuesta (restricciones)
          let respuestaRestricciones = respuestasExaminacion.filter(x => x.respuestaMultiple && x.respuestaMultiple != null)[0];

          // Recogemos las respuestas para el tipo de pregunta fecha (fecha del médico) y el tipo de pregunta restricciones
          let respuestaFecha = respuestasExaminacion.filter(x => x.respuestaFecha && x.respuestaFecha != null)[0]; // A partir de aquí recogemos la fecha y asignamos,
          // sólo tomar en cuenta si el array de opciones no tiene guardado el valor
          // "No seleccionado restricciones"

          let respuestaArrayRestricciones: any = respuestaRestricciones.respuestaMultiple;

          let respuestaArrayRestriccionesAux: number[] = respuestaArrayRestricciones as number[];

          // Inicializamos objeto para devolver el detalle de fechas junto a restricciones
          let recConfPrecDetalleFechas: RecepcionConformePrecargaDetalleFechas = new RecepcionConformePrecargaDetalleFechas();

          if (respuestaArrayRestriccionesAux.filter(x => x != 12).length > 0) {
            // Si no es sin restricciones, quitamos (si existe), la opción restricciones y añadimos las que nos vienen
            idsRestriccionesInteger = idsRestriccionesInteger.filter(x => x != 12); // Quitamos la opción 12
            idsRestriccionesInteger = idsRestriccionesInteger.concat(
              respuestaArrayRestriccionesAux.filter(y => !idsRestriccionesInteger.includes(y))
            ); // Agregamos las que no existan

            recConfPrecDetalleFechas.FechaProximoControlLicencia = respuestaFecha.respuestaFecha;
            recConfPrecDetalleFechas.FechaProximoControlLicenciaString = moment(respuestaFecha.respuestaFecha).format('YYYY-MM-DD');

            recConfPrecDetalleFechas.FechaSoloLectura = true;
            recConfPrecDetalleFechas.IdClaseLicencia = tramAsoc.tramitesClaseLicencia.idClaseLicencia;

            // Añadimos

            // inluye una respuesta que no sea sin restricciones
            //Se establece la fecha que impuso el examinador médico
            // resultado.Respuesta = this.informarObservacionesFechas(
            //   '',
            //   true,
            //   respuestaArrayRestriccionesAux,
            //   null,
            //   tramiteAsociado.solicitudes.tramites.map(x => x.tramitesClaseLicencia),
            //   respuestaFecha.respuestaFecha
            // );

            // resultado.ResultadoOperacion = true;

            // return resultado;
          } else {
            // En el caso de que no tuviera datos como conductor, es que no tiene datos cargados de licencias con anterioridad por lo que
            // procedemos a calcular la fecha con respecto a su fecha de nacimiento, se calcula primero las fechas porque pueden servirnos para otro caso más abajo

            // Se calcula el número de días a partir de la fecha de cumpleaños del postulante

            //1. Sumar los años hasta la fecha actual para la fecha de nacimiento del postulante
            const dateNow: any = new Date();
            const birthdayDate: any = tramAsoc.solicitudes.postulante.FechaNacimiento;

            let yearsDiff = dateNow.getFullYear() - birthdayDate.getFullYear();
            let currentBirthdayDate = new Date(birthdayDate.getFullYear() + yearsDiff, birthdayDate.getMonth(), birthdayDate.getDate());

            // Añadimos un año más si la fecha de cumpleaños del año actual ya ha pasado
            if (currentBirthdayDate < dateNow) {
              currentBirthdayDate = new Date(
                currentBirthdayDate.getFullYear() + 1,
                currentBirthdayDate.getMonth(),
                currentBirthdayDate.getDate()
              );
            }

            //2. Obtener los días entre las dos fechas
            // To calculate the time difference of two dates
            var difference_In_Time: number = currentBirthdayDate.getTime() - dateNow.getTime();

            // To calculate the no. of days between two dates
            var difference_In_Days: number = difference_In_Time / (1000 * 3600 * 24);

            // Sumamos el número de días según si la clase es profesional o no
            if (tramAsoc.tramitesClaseLicencia.clasesLicencias.Profesional) {
              difference_In_Days = difference_In_Days + 365 * 4; // 4 años en caso de licencia profesional
            } else {
              difference_In_Days = difference_In_Days + 365 * 6; // 6 años en caso de licencia no profesional
            }

            dateNow.setDate(dateNow.getDate() + difference_In_Days);

            recConfPrecDetalleFechas.FechaProximoControlLicencia = dateNow;
            recConfPrecDetalleFechas.FechaProximoControlLicenciaString = moment(dateNow).format('YYYY-MM-DD');
            recConfPrecDetalleFechas.FechaSoloLectura = false;
            recConfPrecDetalleFechas.IdClaseLicencia = tramAsoc.tramitesClaseLicencia.idClaseLicencia;

            //if (!tramAsoc.solicitudes.postulante.conductor) {
            //Se establece la fecha que impuso el examinador médico
            // resultado.Respuesta = this.informarObservacionesFechas(
            //   '',
            //   false,
            //   respuestaArrayRestriccionesAux,
            //   null,
            //   tramAsoc.solicitudes.tramites.map(x => x.tramitesClaseLicencia),
            //   null,
            //   dateNow
            // );
            // resultado.ResultadoOperacion = true;

            // return resultado;
            //}

            // Primero vemos si hay datos ya cargados, en otro caso procedemos a calcular las fechas
            // const docLicenciaExistente = await this.documentosLicenciaRespository
            //   .createQueryBuilder('DocumentoLicencia')
            //   .innerJoinAndMapOne(
            //     'DocumentoLicencia.papelSeguridad',
            //     PapelSeguridadEntity,
            //     'papelSeguridad',
            //     'DocumentoLicencia.idPapelSeguridad = papelSeguridad.idPapelSeguridad'
            //   )
            //   .innerJoinAndMapMany(
            //     'DocumentoLicencia.documentoLicenciaClaseLicencia',
            //     DocumentosLicenciaClaseLicenciaEntity,
            //     'documentoLicenciaClaseLicencia',
            //     'DocumentoLicencia.idDocumentoLicencia = documentoLicenciaClaseLicencia.idDocumentoLicencia'
            //   )
            //   .innerJoinAndMapMany(
            //     'DocumentoLicencia.documentoLicenciaTramite',
            //     DocumentoLicenciaTramiteEntity,
            //     'documentoLicenciaTramite',
            //     'DocumentoLicencia.idDocumentoLicencia = documentoLicenciaTramite.idDocumentoLicencia'
            //   )

            //   //Para filtrar por el trámite
            //   .andWhere('(DocumentoLicencia.idConductor = :_idConductor)', {
            //     _idConductor: tramAsoc.solicitudes.postulante.conductor.idConductor,
            //   })
            //   .andWhere('(DocumentoLicencia.fechacaducidad is null)')
            //   .andWhere('(documentoLicenciaTramite.idTramite = :_idTramite)', { _idTramite: tramAsoc.idTramite })

            //   .getOne();

            // // Al no existir datos anteriores cargamos la fecha con respecto a la fecha de nacimiento del postulante
            // if (!docLicenciaExistente) {
            //   respuestaObservacionesFechas.Observaciones = '';
            //   // resultado.Respuesta = this.informarObservacionesFechas(
            //   //   '',
            //   //   false,
            //   //   respuestaArrayRestriccionesAux,
            //   //   null,
            //   //   tramAsoc.solicitudes.tramites.map(x => x.tramitesClaseLicencia),
            //   //   null,
            //   //   currentBirthdayDate
            //   // );
            //   // resultado.ResultadoOperacion = true;

            //   // return resultado;
            // }
            // else{
            //   respuestaObservacionesFechas.Observaciones = docLicenciaExistente.Observaciones;
            // }

            // En otro caso cargamos los datos que ya nos vienen de la licencia anteriormente cargada
            //await queryRunner.commitTransaction();
            // resultado.Respuesta = this.informarObservacionesFechas(
            //   docLicenciaExistente.Observaciones,
            //   false,
            //   respuestaArrayRestriccionesAux,
            //   docLicenciaExistente,
            //   null,
            //   null
            // );
            // resultado.ResultadoOperacion = true;

            // return resultado;
          }

          respuestaObservacionesFechas.DetalleFechasClasesLicencia.push(recConfPrecDetalleFechas);
        }
      });

      // tramiteAsociado.solicitudes.tramites[0].colaExaminacionNM
      //   .filter(x => x.colaExaminacion.idTipoExaminacion == TipoExaminacionesIds.ExamenMedico)
      //   .map(x => x.colaExaminacion);
      respuestaObservacionesFechas.IdsRestriccionesSeleccionadas = idsRestriccionesInteger;

      resultado.Respuesta = respuestaObservacionesFechas;
      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Se devuelven datos de observaciones y fechas asociadas a los ids de clases de licencias.';

      return resultado;
    } catch (err) {
      resultado.Error = err;
      resultado.Mensaje = 'Ha ocurrido un error';
      //await queryRunner.rollbackTransaction();
    }

    return resultado;
  }

  // Método para obtener la próxima fecha de control según el caso:
  //1. si existe ya un documento de licencia previo
  //2. si el médico ha informado una fecha de próximo control
  //3. si el médico no ha informado una fecha de próximo control y no existe ya un doc de licencia previo
  private informarObservacionesFechas(
    Observaciones: string,
    FechaSoloLectura: boolean,
    DatosRestriccionesSeleccionadas: number[],
    DocLicenciaExistente?: DocumentosLicencia,
    TramiteClaseLicencia?: TramitesClaseLicencia[],
    FechaProximoControlLicenciaTCL?: Date,
    NuevaFechaParaSumarFechasLicenciasProYNoPro?: Date
  ) {
    let respuestaObservacionesFechas: RecepcionConformePrecarga = new RecepcionConformePrecarga();
    respuestaObservacionesFechas.Observaciones = Observaciones;
    respuestaObservacionesFechas.DetalleFechasClasesLicencia = [];
    //respuestaObservacionesFechas.FechaSoloLectura = FechaSoloLectura;

    if (DocLicenciaExistente) {
      DocLicenciaExistente.documentoLicenciaClaseLicencia.forEach(dlcl => {
        let rcpdf: RecepcionConformePrecargaDetalleFechas = new RecepcionConformePrecargaDetalleFechas();

        rcpdf.IdClaseLicencia = dlcl.idClaseLicencia;
        rcpdf.FechaProximoControlLicencia = dlcl.caducidadOtorgamiento;

        respuestaObservacionesFechas.DetalleFechasClasesLicencia.push(rcpdf);
      });
    }

    if (TramiteClaseLicencia) {
      TramiteClaseLicencia.forEach(cl => {
        let rcpdf: RecepcionConformePrecargaDetalleFechas = new RecepcionConformePrecargaDetalleFechas();

        rcpdf.IdClaseLicencia = cl.idClaseLicencia;
        // Caso en el que se informa con restricciones
        if (!NuevaFechaParaSumarFechasLicenciasProYNoPro) {
          rcpdf.FechaProximoControlLicencia = FechaProximoControlLicenciaTCL;
        } else {
          let nuevaFechaAsignar: Date = new Date(NuevaFechaParaSumarFechasLicenciasProYNoPro);

          if (cl.clasesLicencias.Profesional) {
            // En este caso se añaden 4 años a la fecha (en el parámetro ya viene la diferencia de días sumada con respecto al cumpleaños)
            nuevaFechaAsignar.setFullYear(NuevaFechaParaSumarFechasLicenciasProYNoPro.getFullYear() + 4);
            rcpdf.FechaProximoControlLicencia = nuevaFechaAsignar;
          } else {
            // En este caso se añaden 6 años a la fecha (en el parámetro ya viene la diferencia de días sumada con respecto al cumpleaños)
            nuevaFechaAsignar.setFullYear(NuevaFechaParaSumarFechasLicenciasProYNoPro.getFullYear() + 6);
            rcpdf.FechaProximoControlLicencia = nuevaFechaAsignar;
          }
        }

        respuestaObservacionesFechas.DetalleFechasClasesLicencia.push(rcpdf);
      });
    }

    respuestaObservacionesFechas.IdsRestriccionesSeleccionadas = DatosRestriccionesSeleccionadas;

    return respuestaObservacionesFechas;
  }

  //==================================================================================================================================================================================================================================================//
  //========================================================== Envio de data al registro civil para informar otorgamiento ============================================================================================================================//
  //==================================================================================================================================================================================================================================================//
  async informarLicRNC(data: any): Promise<any> {
    /*  tipos de tramites:
        tipo1:
        1er otorgamiento profesional:1; 1er otorgamiento no profesional:2, Control:3, Canje:7, Cambio de Restricciones médicas:12
        tipo2:
        Duplicado:4
        tipo3:
        Cambio de domicilio:5
        tipo4:
        Diplomatico:9
        Procesocomunes=>tipo1-tipo3-tipo4
         */
    console.log(
      '=============================================================================================================================================================================='
    );
    console.log(
      '================================================= data que llega del front ====================================================================================================='
    );
    console.log(
      '=============================================================================================================================================================================='
    );
    console.log(data);
    console.log(
      '=============================================================================================================================================================================='
    );

    try {
      let datos = data;
      let D;
      let licencias = [];
      let licTipoConsulta = [];
      licTipoConsulta = [...licTipoConsulta, 'A'];
      let DataLic = [];
      let datosConsulta;
      let datosLicencias;
      let run_;
      let validaTramite;
      let suspension;
      let estadoTramite;
      let Dup;
      let dataConsultaLic;

      //variables para duplicados
      let D_licComunaPriLic;
      let D_licComunaUltLic;
      let D_licFechaPriLic;
      let D_licFechaUltLic;
      let D_licFolioIni;
      let D_licFechaPrxCtl;
      let D_licRunEscuela;
      let D_licObservaciones;

      //=================== cunsulta lic para verificar datos para duplicados ================//
      for (Dup of datos) {
        dataConsultaLic = {
          idUsuario: Dup.idUsuario,
          run: Dup.run,
        };
      }
      let consulLic = await this.ingresoPostulanteService.consulLic(dataConsultaLic);
      let licPosee = consulLic.filter(claseLic => claseLic.licTipo != 'A   ');
      let d;
      for (d of licPosee) {
        D_licComunaPriLic = d.licComunaPriLic;
        D_licComunaUltLic = d.licComunaUltLic;
        D_licFechaPriLic = d.licFechaPriLic;
        D_licFechaUltLic = d.licFechaUltLic;
        D_licFolioIni = d.licFolioIni;
        D_licFechaPrxCtl = d.licFechaPrxCtl;
        D_licRunEscuela = d.licRunEscuela;
        D_licObservaciones = d.licObservaciones;
      }
      //===========================================================================================//
      for (D of datos) {
        let runEscCon = await getConnection().query(`select get_run_escuela_conductores(${D.idTramite})as resp`);
        let runEscuelaCon;
        if (runEscCon[0] == null || runEscCon[0] == undefined) {
          //console.log(0);
          runEscuelaCon = '0';
        } else {
          runEscuelaCon = runEscCon[0].resp.RUT;
          //console.log(runEscCon[0].resp.RUT);
        }

        run_ = D.run.toString().substring(0, D.run.length - 1);
        validaTramite = D.idTipoTramite;

        //consulta codigo de comuna del registro civil ( se envia el el codRNC de la tabla comunas que es el correpondiente a los codigos del RC)
        const ConsultaComunaRNC = await getConnection().query(`select bucarcomunarnc(${D.idComuna})as resp`);
        let comunaRNC = ConsultaComunaRNC[0].resp.codRNC;

        let fechPRXCTRL = D.fechaPRXCTRL;
        let dia = fechPRXCTRL.slice(0, 2);
        let mes = fechPRXCTRL.slice(2, 4);
        let anio = fechPRXCTRL.slice(4, 8);
        let fechaPRXCTRL = anio + '' + mes + '' + dia;

        //consulta la fecha de la ultima examinacion aprobada (para primer otorgamiento)
        const ConsultaFechaUltimaExaAprobada = await getConnection().query(`select get_fecha_ultima_exa_aprobada(${D.idTramite})as resp`);

        let fechaultExaAprob;
        if (ConsultaFechaUltimaExaAprobada == '') {
          //el tramite no amerita examinacion
          var f = new Date();
          let Mes;
          let Dia;

          if ((f.getMonth() + 1).toString().length == 1) {
            Mes = '0' + (f.getMonth() + 1).toString();
          } else {
            Mes = f.getMonth() + 1;
          }
          if (f.getDate().toString().length == 1) {
            Dia = '0' + f.getDate().toString();
          } else {
            Dia = f.getDate();
          }
          let fecha = f.getFullYear() + '' + Mes + '' + Dia;

          fechaultExaAprob = fecha;
        } else {
          fechaultExaAprob = ConsultaFechaUltimaExaAprobada[0].resp.updated_at;
          let dia = fechaultExaAprob.substr(-2, 2);
          let mes = fechaultExaAprob.substr(-5, 2);
          let anio = fechaultExaAprob.substr(0, 4);
          fechaultExaAprob = anio + '' + mes + '' + dia;
        }
        //consulta suspensiones
        let dataConsultaSusp = {
          run: D.run,
          idUsuario: D.idUsuario,
        };
        let consulSusp = await this.cambioestadorceiService.consultaSuspension(dataConsultaSusp);

        if (consulSusp.estadoSuspension == 1) {
          suspension = 1;
        } else {
          suspension = 0;
        }

        if (fechaultExaAprob == 0) {
          //return { respuestaProceso: 0, mensaje: 'Tiene examinaciones pendientes o no aprobadas para el tramite: ' + D.idTramite };
          let cambioEstado = { respuestaProceso: 0 };
          return cambioEstado;
        } else {
          //realiza cambio de estado a 1111 (En espera respuesta SRCeI)
          const actualizaEstadoTramite = await getConnection().query(`select actualizar_estado_sgl_a_1111(${D.idTramite})as resp`);

          //caso primer otorgamiento
          if (
            D.idTipoTramite == 1 ||
            D.idTipoTramite == 2 ||
            D.idTipoTramite == 3 ||
            D.idTipoTramite == 7 ||
            D.idTipoTramite == 12 ||
            D.idTipoTramite == 5 ||
            D.idTipoTramite == 9
          ) {
            let lic = {
              licRun: run_,
              licTipo: D.tipoLic,
              licComunaPriLic: comunaRNC,
              licComunaUltLic: comunaRNC,
              licComunaTraLic: '0',
              licFechaPriLic: fechaultExaAprob,
              licFechaUltLic: fechaultExaAprob,
              licFechaTraLic: '0',
              licFolio: D.nroFolio,
              licFolioIni: D.nroFolio,
              licFolioTra: '0',
              licLetraFolio: D.LetrasFolio,
              licLetraFolioIni: D.LetrasFolio,
              licLetraFolioTra: ' ',
              licEstadoClaLic: '103',
              licEstadoFisLic: '202',
              licEstadoDigLic: '302',
              licFechaPrxCtl: fechaPRXCTRL,
              licRunEscuela: runEscuelaCon, // se debe consultar el run si existe certificado asociado a la clase
              licObservaciones: D.observaciones,
            };
            licTipoConsulta = [...licTipoConsulta, D.tipoLic];
            licencias = [...licencias, lic];

            console.log(
              '=============================================================================================================================================================================='
            );
            console.log(
              '================================================= lic ====================================================================================================='
            );
            console.log(
              '=============================================================================================================================================================================='
            );

            console.log(lic);
          }
          //caso domicilio
          if (D.idTipoTramite == 4) {
            console.log('=========================== paso por domicilio =============================');
            let lic = {
              licRun: run_,
              licTipo: D.tipoLic,
              licComunaPriLic: D_licComunaPriLic,
              licComunaUltLic: D_licComunaUltLic,
              licComunaTraLic: '0',
              licFechaPriLic: D_licFechaPriLic,
              licFechaUltLic: D_licFechaUltLic,
              licFechaTraLic: '0',
              licFolio: D.nroFolio,
              licFolioIni: D_licFolioIni,
              licFolioTra: '0',
              licLetraFolio: D.LetrasFolio,
              licLetraFolioIni: D.licLetraFolioIni,
              licLetraFolioTra: ' ',
              licEstadoClaLic: '103',
              licEstadoFisLic: '202',
              licEstadoDigLic: '302',
              licFechaPrxCtl: D_licFechaPrxCtl,
              licRunEscuela: runEscuelaCon,
              licObservaciones: D_licObservaciones,
            };
            licTipoConsulta = [...licTipoConsulta, D.tipoLic];
            licencias = [...licencias, lic];
          }
          let licA = {
            licRun: run_,
            licTipo: 'A',
            licComunaPriLic: '0',
            licComunaUltLic: '0',
            licComunaTraLic: '0',
            licFechaPriLic: '0',
            licFechaUltLic: '0',
            licFechaTraLic: '0',
            licFolio: '0',
            licFolioIni: '0',
            licFolioTra: '0',
            licLetraFolio: ' ',
            licLetraFolioIni: ' ',
            licLetraFolioTra: ' ',
            licEstadoClaLic: '103',
            licEstadoFisLic: '202',
            licEstadoDigLic: '302',
            licFechaPrxCtl: '0',
            licRunEscuela: '0',
            licObservaciones: ' ',
          };

          licencias = [...licencias, licA];
        }

        datosLicencias = { Licencias: licencias };
        datosConsulta = { run: D.run, tipoLic: licTipoConsulta, tipoTramite: D.idTipoTramite, idUsuario: D.idUsuario };
      }

      let proceso = { proceso: 2 };
      DataLic = [...DataLic, datosConsulta, datosLicencias, proceso];

      // var datosRC = DataLic.filter((data, index, j) => index === j.findIndex(t => t.licTipo === data.licTipo));
      console.log(
        '==================================================================================================================================================='
      );
      console.log(
        '================================================= data que se envia a  cambioEstadoInformarOtorgamiento ==========================================='
      );
      console.log(
        '==================================================================================================================================================='
      );
      console.log(DataLic);
      console.log(
        '==================================================================================================================================================='
      );

      let cambioEstado = await this.cambioestadorceiService.cambioEstadoInformarOtorgamiento(DataLic);
      console.log('========================================================================================== ');
      console.log('==================================== respuesta del servicio... ==============================');
      console.log('========================================================================================== ');

      /*if (cambioEstado.respuestaProceso == 'OK') {
        let consultarestricciones = {
          idUsuario: D.idUsuario,
          RUN: D.run,
          tipoTramite: D.idTipoTramite,
        };

        //let consultaRestricciones = await this.cambioestadorceiService.consultaRestriccionesEmitirLic(consultarestricciones);
      }*/

      if (cambioEstado.respuestaProceso == 'OK') {
        // cambia estado del tramite a 1104
        for (let i = 0; i < datos.length; i++) {
          const actualizaEstadoTramite1104 = await getConnection().query(
            `select actualizar_estado_sgl_a_1104(${datos[i].idTramite})as resp`
          );
          if (actualizaEstadoTramite1104[0].resp == 1) {
            estadoTramite = 'el tramite se encuentra en estado 1104 (Otorgamiento informado y en impresión)';
          }
        }
      } else {
        // cambia a estado en caso de error
      }

      return { cambioEstado, suspension, estadoTramite };
    } catch (err) {
      console.log(err);
    }
  }

  async informarDenegacion(data: any): Promise<any> {
    console.log('========================== entro a denegar ==============================================');
    try {
      let idusuario = data.idUsuario;

      let observaciones = data.observaciones;
      let salidaSGL;
      let salidaRC;
      let dataConsulta = {
        idUsuario: data.idUsuario,
        run: data.rundv.replace(/\./g, ''),
      };
      let dataDen = {
        idusuario: data.idUsuario,
        idTramite: data.idTramite,
      };

      console.log('========================== dataConsulta ==============================================');
      console.log(dataConsulta);

      //consulta sentencias
      let sentencias = await this.cambioestadorceiService.consulSen(dataConsulta);
      console.log('========================== sentencias ==============================================');
      console.log(sentencias);

      if (sentencias.poseeSentencias == true && sentencias.respuesta[0].esSuspension == false) {
        return { respuestaInserSGL: 0, respuestaInsertRC: 0 };
      } else {
        // consulta historico
        let consulHIS = await this.ingresoPostulanteService.consulHis(dataConsulta);
        //console.log(consulHIS);

        //realiza cambio de estado a 1111
        let cambiEstado = this.cambioestadorceiService.actualizaEstadoSGL1111(dataDen);

        let getData = await getConnection().query(`select obtenerdatosdenegacion(${data.idTramite}) as resp`);
        let comuna = getData[0].resp.comuna;
        let EstadoClaseLic = getData[0].resp.EstadoClaseLic;

        console.log('========================== getData ==============================================');
        console.log(getData);

        let consulLic = await this.ingresoPostulanteService.consulLic(dataConsulta);

        if (consulLic == undefined) {
          consulLic = [];
        }
        let lic = JSON.parse(JSON.stringify(consulLic).replace(/"\s+|\s+"/g, '"'));
        let claseLics = lic.filter(claseLic => claseLic.licTipo == data.clase);

        if (claseLics == '' || claseLics == undefined) {
          console.log('inserta');
          //console.log(claseLics);

          let dataLic = [
            { idUsuario: idusuario },
            {
              Licencias: [
                {
                  licRun: data.run.replace(/\./g, ''),
                  licTipo: data.clase,
                  licComunaPriLic: '0',
                  licComunaUltLic: '0',
                  licComunaTraLic: '0',
                  licFechaPriLic: '0',
                  licFechaUltLic: '0',
                  licFechaTraLic: '0',
                  licFolio: '0',
                  licFolioIni: '0',
                  licFolioTra: '0',
                  licLetraFolio: ' ',
                  licLetraFolioIni: ' ',
                  licLetraFolioTra: ' ',
                  licEstadoClaLic: EstadoClaseLic,
                  licEstadoFisLic: '201',
                  licEstadoDigLic: '301',
                  licFechaPrxCtl: '0',
                  licRunEscuela: '0',
                  licObservaciones: observaciones,
                },
                {
                  licRun: data.run.replace(/\./g, ''),
                  licTipo: 'A',
                  licComunaPriLic: comuna,
                  licComunaUltLic: '0',
                  licComunaTraLic: '0',
                  licFechaPriLic: '0',
                  licFechaUltLic: '0',
                  licFechaTraLic: '0',
                  licFolio: '0',
                  licFolioIni: '0',
                  licFolioTra: '0',
                  licLetraFolio: ' ',
                  licLetraFolioIni: ' ',
                  licLetraFolioTra: ' ',
                  licEstadoClaLic: EstadoClaseLic,
                  licEstadoFisLic: '201',
                  licEstadoDigLic: '301',
                  licFechaPrxCtl: '0',
                  licRunEscuela: '0',
                  licObservaciones: ' ',
                },
              ],
            },
          ];

          let insertaLic = await this.ingresoPostulanteService.insertLic(dataLic);

          if (insertaLic.respuesta.codigo == 0) {
            salidaSGL = 'OK';
            console.log(insertaLic.respuesta.codigo);
            let insertDen = await this.cambioestadorceiService.informarDenegacion(dataDen);

            console.log(insertDen);
            if (insertDen.codigo == 200) {
              //actualiza estado del tramite
              this.cambioestadorceiService.cambioEstadoTramiteDenegacion(dataDen);
              let proceso = 4;
              let lics = dataLic[1].Licencias;
              await this.ingresoPostulanteService.insertatHistoricoCambiEstado(lics, dataConsulta, proceso);
              salidaRC = 'OK';
            } else {
              salidaRC = 'NOK';
            }
          } else {
            salidaSGL = 'NOK';
          }
        } else {
          console.log('==================================== actualiza ====================================');

          let dataLic = [
            { idUsuario: idusuario },
            {
              Licencias: [
                {
                  licRun: data.run.replace(/\./g, ''),
                  licTipo: data.clase,
                  licComunaPriLic: claseLics[0].licComunaPriLic,
                  licComunaUltLic: '0',
                  licComunaTraLic: '0',
                  licFechaPriLic: claseLics[0].licFechaPriLic,
                  licFechaUltLic: '0',
                  licFechaTraLic: '0',
                  licFolio: '0',
                  licFolioIni: claseLics[0].licFolioIni,
                  licFolioTra: '0',
                  licLetraFolio: ' ',
                  licLetraFolioIni: claseLics[0].licLetraFolioIni,
                  licLetraFolioTra: ' ',
                  licEstadoClaLic: EstadoClaseLic,
                  licEstadoFisLic: '201',
                  licEstadoDigLic: '301',
                  licFechaPrxCtl: '0',
                  licRunEscuela: '0',
                  licObservaciones: observaciones,
                },
                {
                  licRun: data.run.replace(/\./g, ''),
                  licTipo: 'A',
                  licComunaPriLic: comuna,
                  licComunaUltLic: '0',
                  licComunaTraLic: '0',
                  licFechaPriLic: '0',
                  licFechaUltLic: '0',
                  licFechaTraLic: '0',
                  licFolio: '0',
                  licFolioIni: '0',
                  licFolioTra: '0',
                  licLetraFolio: ' ',
                  licLetraFolioIni: ' ',
                  licLetraFolioTra: ' ',
                  licEstadoClaLic: EstadoClaseLic,
                  licEstadoFisLic: '201',
                  licEstadoDigLic: '301',
                  licFechaPrxCtl: '0',
                  licRunEscuela: '0',
                  licObservaciones: ' ',
                },
              ],
            },
          ];

          console.log('==================================== dataLic ====================================');

          let uplic = await this.ingresoPostulanteService.updateLic(dataLic);
          //console.log(uplic);

          if (uplic.respuesta.codigo == 0) {
            salidaSGL = 'OK';
            console.log(uplic.respuesta.codigo);
            let insertDen = await this.cambioestadorceiService.informarDenegacion(dataDen);
            console.log(insertDen);
            if (insertDen.codigo == 200) {
              salidaRC = 'OK';
              //actualiza estado del tramite
              this.cambioestadorceiService.cambioEstadoTramiteDenegacion(dataDen);
              let proceso = 4;
              let lics = dataLic[1].Licencias;
              await this.ingresoPostulanteService.insertatHistoricoCambiEstado(lics, dataConsulta, proceso);
            } else {
              salidaRC = 'NOK';
            }
          } else {
            salidaSGL = 'NOK';
          }
          console.log('================================ data que se envia a denegacion =========================================');
          console.log(dataLic[0]);
          console.log(dataLic[1].Licencias);

          return { respuesta: salidaRC };
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  //Servicio para Otrogar Licencia a usuario
  async otorgarLicencia(req: any, otorgaLicenciaArchivoDto: OtorgaLicenciaArchivoDto) {
    console.log('trámites.service.ts - otorgarLicencia - comienzo método');

    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    const idsTramites: number[] = [];
    let faltanFechas: boolean;
    let usuarioConectado = new User();

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      console.log('trámites.service.ts - otorgarLicencia - comienzo permisos');
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.OtorgamientoyDenegacionesOtorgamientosDenegacionesOtorgarlicencia
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      console.log('trámites.service.ts - otorgarLicencia - fin permisos');

      otorgaLicenciaArchivoDto.folio = otorgaLicenciaArchivoDto.folio.toUpperCase();

      // Validacion de las fechas
      otorgaLicenciaArchivoDto.seleccionados.forEach(tramite => {
        if (
          tramite.fecha == null ||
          tramite.fecha == undefined ||
          new Date(tramite.fecha) < new Date() ||
          !moment(tramite.fechaString, 'YYYY-MM-DD', true).isValid()
        ) {
          faltanFechas = true;
        }
        idsTramites.push(tramite.idTramite);
      });

      // ERROR Si falta alguna fechas
      if (faltanFechas == true) {
        resultado.Error = 'Error: faltan fechas o son anteriores a hoy';
        console.log('ERROR en las fechas');
        return resultado;
      }

      // ERROR Si se sube documento que no es PDF
      if (
        otorgaLicenciaArchivoDto.archivo != undefined &&
        otorgaLicenciaArchivoDto.archivo != '' &&
        !otorgaLicenciaArchivoDto.archivo.includes('pdf')
      ) {
        resultado.Error = 'Error: el archivo subido no es un pdf';
        console.log('Error: el archivo subido no es un pdf');
        return resultado;
      }

      // Se separan los numero y las letras de el Folio
      const numerosFolioExtraido = otorgaLicenciaArchivoDto.folio.split(' ')[1];
      const letrasFolioExtraido = otorgaLicenciaArchivoDto.folio.split(' ')[0];

      // ERROR Si el folio viene vacío o sin letras o sin números
      if (
        !numerosFolioExtraido ||
        !letrasFolioExtraido ||
        otorgaLicenciaArchivoDto.folio.length < 3 // || !numerosFolioExtraido.match('^[0-9]+$') || !letrasFolioExtraido.match('^[A-Z]+$')
      ) {
        resultado.Error = 'Error: No se ha proporcionado un folio válido';
        console.log('Error: No se ha proporcionado un folio válido');
        return resultado;
      }

      const idUsuarioConectado = (await this.authService.usuario()).idUsuario;

      console.log('trámites.service.ts - otorgarLicencia - comienzo obtener usuario');

      // Se coge el usuario correspondiente al token
      if (idUsuarioConectado > 0) {
        usuarioConectado = await this.userRepository.findOne({ idUsuario: idUsuarioConectado });
      } else {
        resultado.Error = 'Error: el token no coincide con un usuario';
        console.log('Error: el token no coincide con un usuario');
        return resultado;
      }

      console.log('trámites.service.ts - otorgarLicencia - fin obtener usuario');

      console.log('trámites.service.ts - otorgarLicencia - comienzo obtener trámites');

      // Busqueda de los tramites seleccionados por el usuario
      const tramites = await this.tramitesClaseLicenciaRespository.find({
        relations: relacionTramiteConductor,
        where: qb => {
          qb.where('TramitesClaseLicencia__tramite.idTramite IN(:...idsTramite)', { idsTramite: idsTramites }).andWhere(
            'TramitesClaseLicencia__tramite__estadoTramite.Nombre IN(:...estadoTramite)',
            { estadoTramite: estadosTramitesOtorgamiento }
          );
        },
      });

      console.log('trámites.service.ts - otorgarLicencia - fin obtener trámites');

      if (tramites.length > 0) {
        let papelSeguridad = new PapelSeguridadFolioEntity();
        let idConductor: number;
        let idDocLicencia: number;
        let estadoPCL: EstadosPCLEntity;
        let estadoEDD: EstadosEDDEntity;
        let estadoEDF: EstadosEDFEntity;

        // Si el postulante no tiene conductor, se crea uno
        if (tramites[0].tramite.solicitudes.postulante.conductor == null) {
          let conductoresEntity = new ConductoresEntity();
          conductoresEntity.created_at = new Date();
          conductoresEntity.update_at = new Date();
          conductoresEntity.idPostulante = tramites[0].tramite.solicitudes.idPostulante;
          conductoresEntity.ingresadoPor = usuarioConectado.idUsuario;

          console.log('trámites.service.ts - otorgarLicencia - comienzo insertar conductor');
          let user = new ConductoresEntity();
          await queryRunner.manager.insert(ConductoresEntity, conductoresEntity);
          idConductor = conductoresEntity.idConductor;
          console.log(conductoresEntity);
          console.log('trámites.service.ts - otorgarLicencia - fin insertar usuario');
          // Si ya tiene conductor se coje
        } else {
          let conductoresEntity = tramites[0].tramite.solicitudes.postulante.conductor;
          conductoresEntity.update_at = new Date();

          await queryRunner.manager.save(conductoresEntity);
          idConductor = tramites[0].tramite.solicitudes.postulante.conductor.idConductor;
        }

        console.log('trámites.service.ts - otorgarLicencia - comienzo obtener licencia existente');

        const docLicenciaExistente = await this.documentosLicenciaRespository.findOne({
          relations: ['papelSeguridad', 'estadosPCL', 'estadosDD', 'estadosDF', 'documentoLicenciaClaseLicencia'],
          where: { idConductor: idConductor, fechacaducidad: null },
        });

        console.log('trámites.service.ts - otorgarLicencia - fin obtener licencia existente');

        console.log('trámites.service.ts - otorgarLicencia - comienzo obtener papel seguridad');

        const qbPapelSeguridad = this.papelSeguridadRespository
        .createQueryBuilder('PapelSeguridad')
        .innerJoinAndMapOne('PapelSeguridad.lote', LotePapelSeguridadEntity, 'lote', 'PapelSeguridad.idLotePapelSeguridad = lote.idlotePapelSeguridad')
        .innerJoinAndMapOne('lote.proveedor', Proveedor, 'proveedor', 'lote.idProveedor = proveedor.idProveedor')
        //.innerJoinAndMapMany('lote.instituciones', InstitucionesEntity, 'instituciones', 'lote.idInstitucion = instituciones.idInstitucion')
        //.innerJoinAndMapMany('instituciones.oficinas', OficinasEntity, 'oficinas', 'instituciones.idInstitucion = oficinas.idInstitucion')
        //.innerJoinAndMapOne('oficinas.solicitudes', Solicitudes, 'solicitudes', 'oficinas.idOficina = solicitudes.idOficina')

        // Recogemos un papel de seguridad que pertenezca a la misma institución que la solicitud
        //.where('(solicitudes.idSolicitud = :_idSolicitud)', { _idSolicitud: tramites[0].tramite.idSolicitud })
        .andWhere('PapelSeguridad.Asignado = :estadoAsignado', { estadoAsignado: false })
        .andWhere('PapelSeguridad.habilitado = :estadoHabilitado', { estadoHabilitado: true })
        .andWhere('PapelSeguridad.Folio = :numerosFolio', { numerosFolio: numerosFolioExtraido })
        .andWhere('proveedor.codigo = :letrasFolio', { letrasFolio: letrasFolioExtraido });

        papelSeguridad = await qbPapelSeguridad.getOne();

        console.log('trámites.service.ts - otorgarLicencia - fin obtener papel seguridad');


        if (papelSeguridad == undefined) {
          resultado.Error = 'Error: no se encontró el Folio introducido o ya está asignado';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }

        let documentosLicencia: DocumentosLicencia = new DocumentosLicencia();

        console.log('trámites.service.ts - otorgarLicencia - comienzo comprobar documento licencia');
        // Si el postulante no tiene documento Licencia, se crea uno
        if (docLicenciaExistente == undefined) {
          // Se le asigna el papel seguridad al DocumentoLicencia
          documentosLicencia.idPapelSeguridad = papelSeguridad.idPapelSeguridad;

          // Se recogen los estados PCL, EDD, EDF
          estadoPCL = await this.EstadosPCLRespository.findOne({ estadoIncial: true });
          estadoEDD = await this.EstadosEDDRespository.findOne({ estadoIncial: true });
          estadoEDF = await this.EstadosEDFRespository.findOne({ estadoIncial: true });

          // le asignamos los diferentes campos a DocumentoLicencia
          documentosLicencia.idEstadoPCL = estadoPCL.idEstadoPCL;
          documentosLicencia.IdEstadoEDD = estadoEDD.IdEstadoEDD;
          documentosLicencia.IdEstadoEDF = estadoEDF.IdEstadoEDF;
          documentosLicencia.idUsuario = usuarioConectado.idUsuario;
          documentosLicencia.idConductor = idConductor;
          documentosLicencia.Observaciones = otorgaLicenciaArchivoDto.observaciones;
          documentosLicencia.created_at = new Date();
          documentosLicencia.update_at = new Date();

          // Si ya tiene docLicencia, se cojen las licencias que tuviese para crear nuevo DocLicencia
        } else {
          documentosLicencia.idPapelSeguridad = papelSeguridad.idPapelSeguridad;
          documentosLicencia.idEstadoPCL = docLicenciaExistente.idEstadoPCL;
          documentosLicencia.IdEstadoEDD = docLicenciaExistente.IdEstadoEDD;
          documentosLicencia.IdEstadoEDF = docLicenciaExistente.IdEstadoEDF;
          documentosLicencia.idUsuario = docLicenciaExistente.idUsuario;
          documentosLicencia.idConductor = docLicenciaExistente.idConductor;
          documentosLicencia.Observaciones = otorgaLicenciaArchivoDto.observaciones;
          documentosLicencia.created_at = new Date();
          documentosLicencia.update_at = new Date();

          // Caducamos el DocumentoLicencia anterior
          await queryRunner.manager.update(DocumentosLicencia, docLicenciaExistente.idDocumentoLicencia, { fechacaducidad: new Date() });
        }

        console.log('trámites.service.ts - otorgarLicencia - fin comprobar licencia existente');

        console.log('trámites.service.ts - otorgarLicencia - comienzo asignar papel seguridad asignado');

        // Cambiamos el PapelSeguridad a Asignado
        await queryRunner.manager.update(PapelSeguridadFolioEntity, papelSeguridad.idPapelSeguridad, { Asignado: true });

        // Insertamos el DocumentoLicencia
        await queryRunner.manager.save(documentosLicencia);
        idDocLicencia = documentosLicencia.idDocumentoLicencia;


        console.log('trámites.service.ts - otorgarLicencia - fin asignar papel seguridad asignado');

        console.log('trámites.service.ts - otorgarLicencia - comienzo recuperar fechas de trámite en las que se aprobó última examinación');

        // Recuperamos las fechas en la que se aprobó la última examinación de cada trámite
        let tramitesConFechasOtorgamiento = await this.tramitesRespository.find({
          relations: [
            'colaExaminacionNM',
            'colaExaminacionNM.colaExaminacion',
            'colaExaminacionNM.colaExaminacion.resultadoExaminacionCola',
            'colaExaminacionNM.colaExaminacion.resultadoExaminacionCola.resultadoExaminacion',
          ],
          where: qb => {
            qb.where('TramitesEntity.idTramite in (:...idsTramites)', {
              idsTramites: otorgaLicenciaArchivoDto.seleccionados.map(x => x.idTramite),
            });
          },
        });

        console.log('trámites.service.ts - otorgarLicencia - fin recuperar fechas de trámite en las que se aprobó última examinación');

        //Se crean tantos DocLicenciaClaseLicencia como Licencias tenga
        for (const tramiteSeleccionado of otorgaLicenciaArchivoDto.seleccionados) {
          for (const tramiteLicencia of tramites) {
            // Recuperamos la fecha de aprobación de examinación más actual para el otorgamiento seleccionado
            let fechaAprobacion: Date = this.SeleccionarFechaAprobacionMasActual(
              tramitesConFechasOtorgamiento.filter(x => x.idTramite == tramiteSeleccionado.idTramite)[0]
            );

            if (tramiteSeleccionado.idTramite == tramiteLicencia.idTramite) {
              // Insertamos el DocumentoLicenciaClaseLicencia
              let docLicenciaClaseLicencia: DocumentosLicenciaClaseLicenciaEntity = new DocumentosLicenciaClaseLicenciaEntity();
              docLicenciaClaseLicencia.idClaseLicencia = tramiteLicencia.clasesLicencias.idClaseLicencia;
              docLicenciaClaseLicencia.idDocumentoLicencia = idDocLicencia;
              docLicenciaClaseLicencia.primerOtorgamiento = fechaAprobacion;
              docLicenciaClaseLicencia.otorgamientoActual = fechaAprobacion;
              docLicenciaClaseLicencia.caducidadOtorgamiento = moment(tramiteSeleccionado.fechaString, 'YYYY-MM-DD', true).toDate();

              await queryRunner.manager.save(docLicenciaClaseLicencia);

              // Insertamos registro para DocumentoLicencia-Tramite
              const documentoLicenciaTramiteEntity = new DocumentoLicenciaTramiteEntity();
              documentoLicenciaTramiteEntity.created_at = new Date();
              documentoLicenciaTramiteEntity.idDocumentoLicencia = idDocLicencia;
              documentoLicenciaTramiteEntity.idTramite = tramiteSeleccionado.idTramite;

              await queryRunner.manager.insert(DocumentoLicenciaTramiteEntity, documentoLicenciaTramiteEntity);
            }
          }
        }

        if (docLicenciaExistente != undefined) {
          //Se crean tambien los DocLicenciaClaseLicencia que tuviese ya
          for (const docLicenciaClaseLicenciaExistente of docLicenciaExistente.documentoLicenciaClaseLicencia) {
            if (tramites.find(x => x.clasesLicencias.idClaseLicencia == docLicenciaClaseLicenciaExistente.idClaseLicencia) == undefined) {
              //tramites[0].tramite.colaExaminacionNM.map(x => x.colaExaminacion.created_at)

              let docLicenciaClaseLicencia: DocumentosLicenciaClaseLicenciaEntity = new DocumentosLicenciaClaseLicenciaEntity();
              docLicenciaClaseLicencia.idClaseLicencia = docLicenciaClaseLicenciaExistente.idClaseLicencia;
              docLicenciaClaseLicencia.idDocumentoLicencia = idDocLicencia;
              docLicenciaClaseLicencia.primerOtorgamiento = docLicenciaClaseLicenciaExistente.primerOtorgamiento;
              docLicenciaClaseLicencia.otorgamientoActual = docLicenciaClaseLicenciaExistente.otorgamientoActual;
              docLicenciaClaseLicencia.caducidadOtorgamiento = docLicenciaClaseLicenciaExistente.caducidadOtorgamiento;

              await queryRunner.manager.save(docLicenciaClaseLicencia);
            }
          }
        }
        console.log('trámites.service.ts - otorgarLicencia - comienzo subida documento');

        // Subida del documento
        if (otorgaLicenciaArchivoDto.archivo && otorgaLicenciaArchivoDto.archivo != '') {
          // Busqueda del tipo de documento 'Documento Otorgamiento de Licencia'
          const idTipoDocumento = (
            await queryRunner.manager.findOne(TiposDocumentoEntity, { where: { Nombre: tipoDocumentoOtorgamiento } })
          ).idTipoDocumento;

          for (const tramiteLicencia of tramites) {
            let documentoTramite: DocumentosTramitesEntity = new DocumentosTramitesEntity();
            documentoTramite.fileBinary = Buffer.from(otorgaLicenciaArchivoDto.archivo);
            documentoTramite.NombreDocumento = otorgaLicenciaArchivoDto.nombreArchivo;
            documentoTramite.created_at = new Date();
            documentoTramite.updated_at = new Date();
            documentoTramite.idTipoDocumento = idTipoDocumento;
            documentoTramite.idTramite = tramiteLicencia.idTramite;
            documentoTramite.idUsuario = usuarioConectado.idUsuario;

            await queryRunner.manager.save(documentoTramite);
          }
        }

        console.log('trámites.service.ts - otorgarLicencia - fin subida documento');

        // Por último, asignamos las restricciones seleccionadas y las guardamos en base de datos


        console.log('trámites.service.ts - otorgarLicencia - comienzo asignar restricciones');

        // Se recupera el id de documento de licencia existente
        const idDocumentoLicencia = documentosLicencia.idDocumentoLicencia;

        const restriccionesDocumentoLicencia = await queryRunner.manager.find(DocumentosLicenciaRestriccionEntity, {
          where: { idDocumentoLicencia: idDocumentoLicencia },
        });

        // Restricciones a agregar:
        const idsRestriccionesAdd = otorgaLicenciaArchivoDto.idsRestricciones.filter(
          x => !restriccionesDocumentoLicencia.map(y => y.idrestriccion).includes(x)
        );

        // Restricciones a eliminar:
        const idsRestriccionesRemove = restriccionesDocumentoLicencia.filter(
          x => !otorgaLicenciaArchivoDto.idsRestricciones.includes(x.idrestriccion)
        );

        // Procedemos a realizar las acciones con las restricciones asociadas al documento de licencia (Eliminar y luego crear)
        for (const idRestriccionRm of idsRestriccionesRemove) {
          await queryRunner.manager.delete(DocumentosLicenciaRestriccionEntity, {
            where: { idRestriccion: idRestriccionRm, idDocumentoLicencia: idDocumentoLicencia },
          });
        }

        for (const idRestriccionA of idsRestriccionesAdd) {
          let restriccionDocumentoLicencia: DocumentosLicenciaRestriccionEntity = new DocumentosLicenciaRestriccionEntity();

          restriccionDocumentoLicencia.idDocumentoLicencia = idDocumentoLicencia;
          restriccionDocumentoLicencia.idrestriccion = idRestriccionA;

          await queryRunner.manager.save(restriccionDocumentoLicencia);
        }

        console.log('trámites.service.ts - otorgarLicencia - fin asignar restricciones');

        console.log('trámites.service.ts - otorgarLicencia - comienzo resgistrar en registro civil');

        // LLamada a Registro Civil
        const respuestaRegCivil = await this.postRegistroCivil();

        // Si la respuesta de RegCivil es afirmativa, se cambia a estado Otorgado
        if (respuestaRegCivil.ResultadoOperacion) {
          // Busqueda del estado de tramite Otorgado
          const estadoTramiteOtorgada = await this.estadosTramites.findOne({
            where: qb => {
              qb.where('EstadosTramiteEntity.Nombre = :estadoTramite', { estadoTramite: estadosTramitesOtorgado[0] });
            },
          });

          for (const tramiteLicencia of tramites) {
            // Actualizamos el estado de el/los tramites a Otorgado y la fecha
            await queryRunner.manager.update(TramitesEntity, tramiteLicencia.idTramite, {
              idEstadoTramite: estadoTramiteOtorgada.idEstadoTramite,
              update_at: new Date(),
            });
          }

          console.log('trámites.service.ts - otorgarLicencia - fin resgistrar en registro civil');

          resultado.ResultadoOperacion = true;
          resultado.Mensaje = 'Licencia otorgada correctamente';
          await queryRunner.commitTransaction();

          // Si la respuesta de RegCivil es negativa, se cambia a estado Pendiente
        } else {
          // Busqueda del estado de tramite Pendiente de Registro Civil
          const estadoTramitePendienteRegCivil = await this.estadosTramites.findOne({
            where: qb => {
              qb.where('EstadosTramiteEntity.Nombre = :estadoTramite', { estadoTramite: estadosTramitesEsperaRegistro[0] });
            },
          });

          for (const tramiteLicencia of tramites) {
            // Actualizamos el estado de el/los tramites a Pendiente de Registro Civil y la fecha
            await queryRunner.manager.update(TramitesEntity, tramiteLicencia.idTramite, {
              idEstadoTramite: estadoTramitePendienteRegCivil.idEstadoTramite,
              update_at: new Date(),
            });
          }

          console.log('trámites.service.ts - otorgarLicencia - fin método');

          resultado.ResultadoOperacion = true;
          resultado.Mensaje = 'Otorgamiento queda pendiente de Registro Civil';
          await queryRunner.commitTransaction();
        }
      } else {
        resultado.Error = 'Error otorgando licencia';
        await queryRunner.rollbackTransaction();
      }
    } catch (error) {
      console.error(error);
      resultado.Error = 'Error otorgando licencia';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async postRegistroCivil(): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    //const request = require('request');

    // Comentar estas dos lineas, para conectar a Registro Civil
    resultado.ResultadoOperacion = true;
    return resultado;

    // return new Promise(function (resolve, reject) {
    //   request(BASE_URL_REGISTRO_CIVIL, { json: true }, function (error, res, body) {
    //     if (!error && (res.statusCode == 200 || res.statusCode == 201)) {
    //       resultado.ResultadoOperacion = true;
    //       resolve(resultado);
    //     } else {
    //       resultado.Error = error ? error : 'Error conectando a RegistroCivil';
    //       resultado.ResultadoOperacion = false;
    //       resolve(resultado);
    //     }
    //   });
    // });
  }

  async postLicenciaMobile(licenciaConConductorDto: LicenciaConConductorDto): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    const request = require('request');

    const tokenEncriptado = this.utilService.encriptar();

    try {
      return new Promise(function (resolve, reject) {
        request.post(
          BASE_URL_BACK_MOBILE + URL_MOBILE_CREAR_LICENCIA,
          { form: licenciaConConductorDto, headers: { Authorization: tokenEncriptado } },
          function (error, res, body) {
            if (!error && (res.statusCode == 200 || res.statusCode == 201)) {
              const resultadoMobile = JSON.parse(body);

              if (resultadoMobile.ResultadoOperacion) {
                resultado.ResultadoOperacion = true;
                resolve(resultado);
              } else {
                console.log(resultadoMobile.Error);
                resultado.Error = error ? error : resultadoMobile.Error;
                resultado.ResultadoOperacion = false;
                resolve(resultado);
              }
            } else {
              console.log(error);
              resultado.Error = error ? error : 'Error conectando a la parte Mobile';
              resultado.ResultadoOperacion = false;
              resolve(resultado);
            }
          }
        );
      });
    } catch (error) {
      console.log(error);
      resultado.Error = error ? error : 'Error desconocido al conectar a la parte Mobile';
      resultado.ResultadoOperacion = false;
      return resultado;
    }
  }

  async subirLicencias(req: any, fotoLicencia: TramiteFotosLicenciaDto) {
    const resultado: Resultado = new Resultado();

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.OtorgamientoyDenegacionesVisualizarLicenciasImprimirLicencia
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      //const resultadoInsercionFotos = await this.subirFotosLicencia(fotoLicencia, queryRunner);
      const resultadoInsercionFotos = await this.subirFotosLicenciaByIdTramite(fotoLicencia, queryRunner);

      if (resultadoInsercionFotos.ResultadoOperacion) {
        await queryRunner.commitTransaction();

        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Licencia subida correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = resultadoInsercionFotos.Error;
        await queryRunner.rollbackTransaction();
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error insertando las fotos de licencia';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  // Servicio que sube las fotos frontal y trasera de la Licencia
  async subirFotosLicencia(fotoLicencia: TramiteFotosLicenciaDto, queryRunner: QueryRunner, idDocLicencia?: number) {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      // Bloque de búsqueda del idDocumentoLicencia
      const respuestaTramite = await this.tramitesClaseLicenciaRespository.find({
        relations: relacionTramiteConductor,
        where: qb => {
          qb.where('TramitesClaseLicencia__tramite.idTramite IN(:...idsTramite)', { idsTramite: fotoLicencia.idsTramite });
        },
      });

      const idConductor = respuestaTramite[0].tramite.solicitudes.postulante.conductor.idConductor;

      const docLicenciaExistente = await this.documentosLicenciaRespository.findOne({
        relations: [
          'papelSeguridad',
          'estadosPCL',
          'estadosDD',
          'estadosDF',
          'documentoLicenciaClaseLicencia',
          'documentoLicenciaClaseLicencia.clasesLicencias',
          'documentoLicenciaTramite',
        ],
        where: { idConductor: idConductor, fechacaducidad: null },
      });

      const docLicenciaId = docLicenciaExistente.idDocumentoLicencia;

      // Se comprueba que vienen la foto frontal y la trasera
      if (
        fotoLicencia.fotoFrontal != null &&
        fotoLicencia.fotoFrontal != undefined &&
        fotoLicencia.fotoTrasera != null &&
        fotoLicencia.fotoTrasera != undefined
      ) {
        const nuevaFotoFrontal = new ImagenLicenciaEntity();
        nuevaFotoFrontal.created_at = new Date();
        nuevaFotoFrontal.updated_at = new Date();
        nuevaFotoFrontal.idDocumentoLicencia = docLicenciaId;
        nuevaFotoFrontal.idTipoImagenLicencia = await (
          await this.tipoImagenLicenciaEntityRespository.findOne({ Nombre: 'Física' })
        ).idTipoImagenLicencia;
        nuevaFotoFrontal.numeroImagen = 0;
        nuevaFotoFrontal.fileBinary = Buffer.from(fotoLicencia.fotoFrontal);

        await queryRunner.manager.save(nuevaFotoFrontal);

        const nuevaFotoTrasera = new ImagenLicenciaEntity();
        nuevaFotoTrasera.created_at = new Date();
        nuevaFotoTrasera.updated_at = new Date();
        nuevaFotoTrasera.idDocumentoLicencia = docLicenciaId;
        nuevaFotoTrasera.idTipoImagenLicencia = await (
          await this.tipoImagenLicenciaEntityRespository.findOne({ Nombre: 'Física' })
        ).idTipoImagenLicencia;
        nuevaFotoTrasera.numeroImagen = 1;
        nuevaFotoTrasera.fileBinary = Buffer.from(fotoLicencia.fotoTrasera);

        await queryRunner.manager.save(nuevaFotoTrasera);

        // Si el postulante no tiene foto se cancela el otorgamiento
        if (respuestaTramite[0].tramite.solicitudes.postulante.imagenesPostulante[0] == undefined) {
          resultado.Error = 'Error: El postulante no tiene foto';
          console.log('Error: El postulante no tiene foto');
          return resultado;
        }

        // Creamos el objeto para la licencia Mobile
        let licenciaConConductorDto = new LicenciaConConductorDto();
        let conductorMobileDTO = new ConductorMobileDTO();

        licenciaConConductorDto.conductor = conductorMobileDTO;
        licenciaConConductorDto.conductor.RUN = respuestaTramite[0].tramite.solicitudes.postulante.RUN;
        licenciaConConductorDto.conductor.DV = respuestaTramite[0].tramite.solicitudes.postulante.DV;
        licenciaConConductorDto.conductor.Nombres = respuestaTramite[0].tramite.solicitudes.postulante.Nombres;
        licenciaConConductorDto.conductor.ApellidoPaterno = respuestaTramite[0].tramite.solicitudes.postulante.ApellidoPaterno;
        licenciaConConductorDto.conductor.ApellidoMaterno = respuestaTramite[0].tramite.solicitudes.postulante.ApellidoMaterno;
        licenciaConConductorDto.conductor.Telefono = respuestaTramite[0].tramite.solicitudes.postulante.Telefono;
        licenciaConConductorDto.conductor.Email = respuestaTramite[0].tramite.solicitudes.postulante.Email;
        licenciaConConductorDto.conductor.Comuna = respuestaTramite[0].tramite.solicitudes.postulante.comunas.Nombre;
        licenciaConConductorDto.conductor.Calle = respuestaTramite[0].tramite.solicitudes.postulante.Calle;
        licenciaConConductorDto.conductor.CalleNro = respuestaTramite[0].tramite.solicitudes.postulante.CalleNro;
        licenciaConConductorDto.conductor.Letra = respuestaTramite[0].tramite.solicitudes.postulante.Letra;
        licenciaConConductorDto.conductor.RestroDireccion = respuestaTramite[0].tramite.solicitudes.postulante.RestoDireccion;
        licenciaConConductorDto.conductor.Sexo = respuestaTramite[0].tramite.solicitudes.postulante.opcionSexo.Nombre;
        licenciaConConductorDto.conductor.EstadoCivil = respuestaTramite[0].tramite.solicitudes.postulante.opcionEstadosCivil.Nombre;
        licenciaConConductorDto.conductor.Nacionalidad = respuestaTramite[0].tramite.solicitudes.postulante.Nacionalidad;
        licenciaConConductorDto.conductor.NivelEducacional =
          respuestaTramite[0].tramite.solicitudes.postulante.opcionesNivelEducacional.Nombre;
        licenciaConConductorDto.conductor.Profesion = respuestaTramite[0].tramite.solicitudes.postulante.Profesion;
        //licenciaConConductorDto.conductor.Diplomatico = respuestaTramite[0].tramite.solicitudes.postulante.Diplomatico;                              // ??????
        //licenciaConConductorDto.conductor.Discapacidad = respuestaTramite[0].tramite.solicitudes.postulante.discapacidad;                            // ??????
        licenciaConConductorDto.conductor.DetalleDiscapacidad = respuestaTramite[0].tramite.solicitudes.postulante.DetalleDiscapacidad;
        licenciaConConductorDto.conductor.FechaNacimiento = respuestaTramite[0].tramite.solicitudes.postulante.FechaNacimiento;
        licenciaConConductorDto.conductor.FechaDefuncion = respuestaTramite[0].tramite.solicitudes.postulante.FechaDefuncion;
        licenciaConConductorDto.conductor.idSGLConductor = idConductor;
        licenciaConConductorDto.conductor.municipalidad = respuestaTramite[0].tramite.solicitudes.postulante.comunas.instituciones
          ? respuestaTramite[0].tramite.solicitudes.postulante.comunas.instituciones.Nombre
          : respuestaTramite[0].tramite.solicitudes.postulante.comunas.Nombre;
        licenciaConConductorDto.conductor.fotoUsuarioPath = 'FotoUsuario'; //respuestaTramite[0].tramite.solicitudes.postulante.imagenesPostulante[0].nombreArchivo;    // Se coje la primera?
        licenciaConConductorDto.conductor.fotoUsuarioBinary =
          respuestaTramite[0].tramite.solicitudes.postulante.imagenesPostulante[0].archivo; // Se coje la primera?

        //Creamos el objeto del Documento Licencia para Mobile
        const documentosLicenciasMobile: DocumentoLicenciaMobileDto[] = [];
        for (const documentoLicenciaClaseLicencia of docLicenciaExistente.documentoLicenciaClaseLicencia) {
          const documentoLicenciaMobileDto = new DocumentoLicenciaMobileDto();
          // documentoLicenciaMobileDto.RUN =
          //   respuestaTramite[0].tramite.solicitudes.postulante.RUN + '-' + respuestaTramite[0].tramite.solicitudes.postulante.DV;
          documentoLicenciaMobileDto.folioPapelSeguridad =
            docLicenciaExistente.papelSeguridad.Folio + docLicenciaExistente.papelSeguridad.LetrasFolio;
          documentoLicenciaMobileDto.idFolioSGL = docLicenciaExistente.idPapelSeguridad;
          documentoLicenciaMobileDto.estadoEDD = docLicenciaExistente.estadosDD.Nombre;
          documentoLicenciaMobileDto.estadoEDF = docLicenciaExistente.estadosDF.Nombre;
          documentoLicenciaMobileDto.estadoPCL = docLicenciaExistente.estadosPCL.Nombre;
          documentoLicenciaMobileDto.clasesLicencia = documentoLicenciaClaseLicencia.clasesLicencias.Abreviacion;
          documentoLicenciaMobileDto.municipalidad = ''; //////////////////////////////////////////////////////////////////// Del postulante?? del usuario??
          documentoLicenciaMobileDto.restricciones = ''; //////////////////////////////////////////////////////////////////// Son array?
          documentoLicenciaMobileDto.fechaCaducidad = documentoLicenciaClaseLicencia.caducidadOtorgamiento;
          documentosLicenciasMobile.push(documentoLicenciaMobileDto);
        }

        // licenciaConConductorDto.documentosLicencia = documentosLicenciasMobile;

        // let imagenFrontalDto = new ImagenLicenciaDto();
        // imagenFrontalDto.created_at = nuevaFotoFrontal.created_at;
        // imagenFrontalDto.updated_at = nuevaFotoFrontal.updated_at;
        // imagenFrontalDto.fileBinary = nuevaFotoFrontal.fileBinary.toString();
        // imagenFrontalDto.numeroImagen = nuevaFotoFrontal.numeroImagen;
        // imagenFrontalDto.idTipoImagenLicencia = nuevaFotoFrontal.idTipoImagenLicencia;
        // imagenFrontalDto.idDocumentoLicencia = nuevaFotoFrontal.idDocumentoLicencia;

        // licenciaConConductorDto.fotoLicenciaFrontal = imagenFrontalDto;

        // let imagenTraseraDto = new ImagenLicenciaDto();
        // imagenTraseraDto.created_at = nuevaFotoTrasera.created_at;
        // imagenTraseraDto.updated_at = nuevaFotoTrasera.updated_at;
        // imagenTraseraDto.fileBinary = nuevaFotoTrasera.fileBinary.toString();
        // imagenTraseraDto.numeroImagen = nuevaFotoTrasera.numeroImagen;
        // imagenTraseraDto.idTipoImagenLicencia = nuevaFotoTrasera.idTipoImagenLicencia;
        // imagenTraseraDto.idDocumentoLicencia = nuevaFotoTrasera.idDocumentoLicencia;

        // licenciaConConductorDto.fotoLicenciaTrasera = imagenTraseraDto;

        // Insertado del documento licencia en la parte Mobile
        const respuestaLicenciaMobile = await this.postLicenciaMobile(licenciaConConductorDto);

        // if (respuestaLicenciaMobile.ResultadoOperacion) {

        // Busqueda del estado de tramite Acusar Recepcion Conforme
        const estadoTramiteAcusarRecepcionConforme = await this.estadosTramites.findOne({
          where: qb => {
            qb.where('EstadosTramiteEntity.idEstadoTramite = :estadoTramite', { estadoTramite: estadoTramitesEsperaRecepcionConformeId });
          },
        });

        // Actualizamos el estado de el/los tramites a Acusar Recepcion Conforme (de los trámites asociados al mismo documento de licencia guardado)

        for (const tramiteAsociado of docLicenciaExistente.documentoLicenciaTramite) {
          await queryRunner.manager.update(TramitesEntity, tramiteAsociado.idTramite, {
            idEstadoTramite: estadoTramiteAcusarRecepcionConforme.idEstadoTramite,
            update_at: new Date(),
          });
        }

        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Las fotos de licencia se ha guardado con éxito';
        // } else {
        // resultado.ResultadoOperacion = false;
        // resultado.Error = 'Error creando el documento licencia en la parte Mobile';
        // }
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'ERROR: no se recibieron las fotos de licencia';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error guardando las fotos de licencia';
    }

    return resultado;
  }

  async subirFotosLicenciaByIdTramite(fotoLicencia: TramiteFotosLicenciaDto, queryRunner: QueryRunner) {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    try {
      // Bloque de búsqueda del idDocumentoLicencia
      const respuestaTramite = await this.tramitesClaseLicenciaRespository.find({
        relations: [
          'tramite',
          'clasesLicencias',
          'tramite.solicitudes',
          'tramite.solicitudes.postulante',
          'tramite.tiposTramite',
          'tramite.estadoTramite',
          'tramite.colaExaminacionNM',
          'tramite.colaExaminacionNM.colaExaminacion',
          'tramite.solicitudes.postulante.conductor',
          'tramite.solicitudes.postulante.comunas',
          'tramite.solicitudes.postulante.comunas.instituciones',
          'tramite.solicitudes.postulante.opcionSexo',
          'tramite.solicitudes.postulante.opcionesNivelEducacional',
          'tramite.solicitudes.postulante.opcionEstadosCivil',
          'tramite.solicitudes.postulante.imagenesPostulante',
          'tramite.solicitudes.postulante.conductor.documentosLicencia',
          'tramite.solicitudes.postulante.conductor.documentosLicencia.documentoLicenciaClaseLicencia',
          'tramite.solicitudes.postulante.conductor.documentosLicencia.documentoLicenciaClaseLicencia.clasesLicencias',
          'tramite.tramitesClaseLicencia',
          'tramite.tramitesClaseLicencia.tramite',
          'tramite.tramitesClaseLicencia.tramite.solicitudes',
          'tramite.tramitesClaseLicencia.tramite.solicitudes.postulante',
          'tramite.tramitesClaseLicencia.tramite.solicitudes.postulante.comunas',
          'tramite.tramitesClaseLicencia.tramite.solicitudes.postulante.imagenesPostulante',
        ],
        where: qb => {
          qb.where('TramitesClaseLicencia__tramite.idTramite IN(:...idsTramite)', { idsTramite: fotoLicencia.idsTramite });
        },
      });

      // Si el postulante no tiene foto se cancela el otorgamiento
      if (respuestaTramite[0].tramite.solicitudes.postulante.imagenesPostulante[0] == undefined) {
        resultado.Error = 'Error: El postulante no tiene foto';
        console.log('Error: El postulante no tiene foto');
        return resultado;
      }

      const idConductor = respuestaTramite[0].tramite.solicitudes.postulante.conductor.idConductor;

      const docLicenciaExistente = await this.documentosLicenciaRespository.findOne({
        relations: [
          'papelSeguridad',
          'estadosPCL',
          'estadosDD',
          'estadosDF',
          'documentoLicenciaClaseLicencia',
          'documentoLicenciaClaseLicencia.clasesLicencias',
          'documentoLicenciaTramite',
        ],
        where: { idConductor: idConductor, fechacaducidad: null },
      });

      // Busqueda del estado de tramite Acusar Recepcion Conforme
      const estadoTramiteAcusarRecepcionConforme = await this.estadosTramites.findOne({
        where: qb => {
          qb.where('EstadosTramiteEntity.idEstadoTramite = :estadoTramite', { estadoTramite: estadoTramitesEsperaRecepcionConformeId });
        },
      });

      // Actualizamos el estado de el/los tramites a Acusar Recepcion Conforme (de los trámites asociados al mismo documento de licencia guardado)

      for (const tramiteAsociado of docLicenciaExistente.documentoLicenciaTramite) {
        await queryRunner.manager.update(TramitesEntity, tramiteAsociado.idTramite, {
          idEstadoTramite: estadoTramiteAcusarRecepcionConforme.idEstadoTramite,
          update_at: new Date(),
        });
      }

      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Las fotos de licencia se ha guardado con éxito';

      return resultado;
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error guardando las fotos de licencia';

      return resultado;
    }
  }

  async denegarLicencia(req: any, denegarLicenciaArchivoDto: DenegarLicenciaArchivoDto) {
    const resultado: Resultado = new Resultado();
    let tramiteClaseLicencia: TramitesClaseLicencia;
    let documentoTramite: DocumentosTramitesEntity = new DocumentosTramitesEntity();

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
      req,
      PermisosNombres.OtorgamientoyDenegacionesOtorgamientosDenegacionesDenegarLicencia
    );

    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }

    const idUsuarioConectado: number = usuarioValidado.Respuesta.idUsuario;

    if (denegarLicenciaArchivoDto == null || denegarLicenciaArchivoDto.idTramite <= 0) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Datos Incorrectos';
      return resultado;
    }

    if (denegarLicenciaArchivoDto.archivo != '' && !denegarLicenciaArchivoDto.archivo.includes('pdf')) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error: el archivo subido no es un pdf';
      console.log('Error: el archivo subido no es un pdf');
      return resultado;
    }

    let usuarioConectado = new User();

    if (idUsuarioConectado > 0) {
      usuarioConectado = await this.userRepository.findOne({ idUsuario: idUsuarioConectado });
    } else {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error: el token no coincide con un usuario';
      console.log('Error: el token no coincide con un usuario');
      return resultado;
    }

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      // Obtenemos el TramiteClaseLicencia y sus dependencias, a partir del id del Tramite
      tramiteClaseLicencia = await queryRunner.manager.findOne(TramitesClaseLicencia, {
        relations: relacionTramiteSinExamenes,
        where: qb => {
          qb.where('TramitesClaseLicencia__tramite.idTramite = :idsTramite', { idsTramite: denegarLicenciaArchivoDto.idTramite });
        },
      });

      // Si no hay ninguna licencia con el tramiteId buscado mostramos un error
      if (tramiteClaseLicencia == null) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontro la licencia';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Obtenemos el Id del estado trámite que esta a la espera de ser Recepcionado Conforme
      let estadoEnEspera = (
        await queryRunner.manager.findOne(EstadosTramiteEntity, { where: { Nombre: estadoTramitesEsperaDenegacionLicencia[0] } })
      ).idEstadoTramite;

      // Si no esta en estado de espera de Otorgamiento, se cancela
      if (tramiteClaseLicencia.tramite.idEstadoTramite != estadoEnEspera) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error, el tramite no esta a la espera de denegacion';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Si se ha mandado un documento, Creamos un DocumentoTramite con dichos datos y lo subimos a la BBDD
      if (
        denegarLicenciaArchivoDto.archivo != null &&
        denegarLicenciaArchivoDto.archivo != '' &&
        denegarLicenciaArchivoDto.nombreArchivo != null &&
        denegarLicenciaArchivoDto.nombreArchivo != ''
      ) {
        // Obtenemos el Id para el tipo Documento: "Documento de Denegacion de Licencia"
        const idTipoDocumentoDenegado = (
          await queryRunner.manager.findOne(TiposDocumentoEntity, { where: { Nombre: tipoDocumentoDenegacion } })
        ).idTipoDocumento;
        // Si no hay ninguna TipoDocumento con el nombre buscado mostramos un error
        if (idTipoDocumentoDenegado == null) {
          resultado.ResultadoOperacion = false;
          resultado.Error = 'No se encontro el tipo de Documento';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }

        documentoTramite.fileBinary = Buffer.from(denegarLicenciaArchivoDto.archivo);
        documentoTramite.NombreDocumento = denegarLicenciaArchivoDto.nombreArchivo;
        documentoTramite.created_at = new Date();
        documentoTramite.updated_at = new Date();
        documentoTramite.idTipoDocumento = idTipoDocumentoDenegado;
        documentoTramite.idTramite = tramiteClaseLicencia.idTramite;
        documentoTramite.idUsuario = usuarioConectado.idUsuario;

        await queryRunner.manager.save(documentoTramite);
      }

      const motivoDenegacionBBDD = await queryRunner.manager.findOne(
        MotivosDenegacionEntity,
        denegarLicenciaArchivoDto.idMotivoDenegacion,
        {
          relations: ['estadoTramite'],
        }
      );

      if (motivoDenegacionBBDD == undefined) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontro el motivo de denegación';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      const idSiguienteEstadoTramite = motivoDenegacionBBDD.idSiguienteEstadoTramite;

      //tramiteClaseLicencia.tramite.plazoDenegacion = denegarLicenciaArchivoDto.dias;

      // Comprobamos el plazo de Denegacion. Si es de tipo "Otro" obtendremos los "Dias"
      /*if (plazoDenegacion.find(x => x === denegarLicenciaArchivoDto.plazoDenegacion)) {
        if (denegarLicenciaArchivoDto.plazoDenegacion == plazoDenegacionOtro) {
          if (denegarLicenciaArchivoDto.dias != null && denegarLicenciaArchivoDto.dias != '') {
            tramiteClaseLicencia.tramite.plazoDenegacion = denegarLicenciaArchivoDto.dias;
          } else {
            resultado.ResultadoOperacion = false;
            resultado.Error = "Si el Plazo es 'Otro' debe indicar el número de días";
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            return resultado;
          }
        } else tramiteClaseLicencia.tramite.plazoDenegacion = denegarLicenciaArchivoDto.plazoDenegacion;
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontro el plazo';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }*/

      // Comprobamos si existe una fecha de reprobacion, si es así, la incluimos en el tramite
      if (!denegarLicenciaArchivoDto.fechaReprobacion) {
        // tramiteClaseLicencia.FechaReprobacion = denegarLicenciaArchivoDto.fechaReprobacion;
        //} else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se incluyo una fecha de reprobacion';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Si existe un motivo adicional lo añadimos al tramaite
      // if (denegarLicenciaArchivoDto.motivoAdicional != null && denegarLicenciaArchivoDto.motivoAdicional != '') {
      //     tramiteClaseLicencia.MotivoAdicional = denegarLicenciaArchivoDto.motivoAdicional;
      // }

      // Guardamos los datos para TramiteClaseLicencia
      //await queryRunner.manager.save(tramiteClaseLicencia);

      // Asignamos este nuevo estado al tramite
      if (idSiguienteEstadoTramite > 0) {
        await queryRunner.manager.update(TramitesEntity, tramiteClaseLicencia.tramite.idTramite, {
          idEstadoTramite: idSiguienteEstadoTramite,
          idMotivoDenegacion: motivoDenegacionBBDD.idMotivoDenegacion,
          motivoAdicional: denegarLicenciaArchivoDto.motivoAdicional,
          fechaReprobacion: denegarLicenciaArchivoDto.fechaReprobacion,
          plazoDenegacion: denegarLicenciaArchivoDto.plazoDenegacion,
        });

        // Completamos la Transaccion
        await queryRunner.commitTransaction();
        resultado.Respuesta = {
          idTramite: tramiteClaseLicencia.tramite.idTramite,
          tipoDenegacion: motivoDenegacionBBDD.estadoTramite.Nombre,
        };
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Licencia denegada correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error guardando el estado del tramite';
        await queryRunner.rollbackTransaction();
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo el tramite';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async postRecepcionConforme(req: any, tramiteRecepcion: TramiteRecepcionDto): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    let estadoTramite: EstadosTramiteEntity;
    let tramiteActual: TramitesEntity;
    let documentoTramite: DocumentosTramitesEntity = new DocumentosTramitesEntity();

    if (tramiteRecepcion.idTramite <= 0) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error, id del Tramite no valido';
      return resultado;
    }

    if (
      tramiteRecepcion.documentoBinary &&
      tramiteRecepcion.documentoBinary != null &&
      tramiteRecepcion.documentoBinary != '' &&
      !tramiteRecepcion.documentoBinary.includes('pdf')
    ) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error: el archivo subido no es un pdf';
      console.log('Error: el archivo subido no es un pdf');
      return resultado;
    }

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.OtorgamientoyDenegacionesRecepcionConformeRecepcionarConforme
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      const idUsuarioConectado = (await this.authService.usuario()).idUsuario;

      let usuarioConectado = new User();

      if (idUsuarioConectado > 0) {
        usuarioConectado = await this.userRepository.findOne({ idUsuario: idUsuarioConectado });
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error: el token no coincide con un usuario';
        console.log('Error: el token no coincide con un usuario');
        return resultado;
      }
      // Obtenemos el Tramite, a partir de su id

      if (tramiteRecepcion.idTramite > 0) {
        tramiteActual = await queryRunner.manager.findOne(TramitesEntity, {
          relations: [
            'solicitudes',
            'solicitudes.tramites',
            'solicitudes.tramites.estadoTramite',
            'solicitudes.oficinas',
            'solicitudes.oficinas.instituciones',
            'solicitudes.postulante',
            'solicitudes.postulante.imagenesPostulante',
            'solicitudes.postulante.conductor',
            'solicitudes.postulante.conductor.documentosLicencia',
            'solicitudes.postulante.conductor.documentosLicencia.documentoLicenciaClaseLicencia',
            'solicitudes.postulante.conductor.documentosLicencia.documentoLicenciaClaseLicencia.clasesLicencias',
          ],
          where: qb => {
            qb.where('TramitesEntity.idTramite = :idTramite', { idTramite: tramiteRecepcion.idTramite });
          },
        });
      }

      // Si no hay ningun Tramite con el tramiteId buscado, mostramos un error
      if (tramiteActual == null) {
        resultado.Error = 'Error obteniendo el Tramite';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Obtenemos el documento de licencia asociado junto a sus trámites
      const docLicenciaExistente = await this.documentosLicenciaRespository.findOne({
        relations: [
          'papelSeguridad',
          'estadosPCL',
          'estadosDD',
          'estadosDF',
          'documentoLicenciaClaseLicencia',
          'documentoLicenciaClaseLicencia.clasesLicencias',
          'documentoLicenciaTramite',
          'conductor',
        ],
        where: { idConductor: tramiteActual.solicitudes.postulante.conductor.idConductor, fechacaducidad: null },
      });

      // Dependiendo de si se ha validado la recepcion conforme o no, obtenemos un estadoTramite u otro.
      if (tramiteRecepcion.conforme) {
        estadoTramite = await queryRunner.manager.findOne(EstadosTramiteEntity, { where: { Nombre: estadoTramitesRecepcionConforme[0] } });
      } else {
        // estadoTramite = await queryRunner.manager.findOne(EstadosTramiteEntity, { where: { Nombre: estadoTramitesRecepcionNoConforme[0] } });
        estadoTramite = await queryRunner.manager.findOne(EstadosTramiteEntity, { where: { codigo: 1103 } }); // 1103 = A la espera de otorgamiento
      }

      // Obtenemos el Id del estadoTramite que esta a la espera de ser Recepcionado Conforme
      let estadoEnEspera = (
        await queryRunner.manager.findOne(EstadosTramiteEntity, { where: { Nombre: estadoTramitesEsperaRecepcionConforme[0] } })
      ).idEstadoTramite;

      // Comprobamos si el Tramite tiene el estado que espera ser Recepcionado Conforme
      if (tramiteActual.idEstadoTramite == estadoEnEspera) {
        // Si los datos son correctos, Creamos un DocumentoTramite con los datos que se nos pasan y lo subimos a la BBDD
        if (
          tramiteRecepcion.documentoNombre != null &&
          tramiteRecepcion.documentoBinary != null &&
          tramiteRecepcion.documentoNombre != '' &&
          tramiteRecepcion.documentoBinary != ''
        ) {
          // Obtenemos el Id para el tipo Documento = Recepcion Conforme
          const idTipoDocumento = (
            await queryRunner.manager.findOne(TiposDocumentoEntity, { where: { Nombre: tipoDocumentoRecepcionConforme } })
          ).idTipoDocumento;

          documentoTramite.fileBinary = Buffer.from(tramiteRecepcion.documentoBinary);
          documentoTramite.NombreDocumento = tramiteRecepcion.documentoNombre;
          documentoTramite.created_at = new Date();
          documentoTramite.updated_at = new Date();
          documentoTramite.idTipoDocumento = idTipoDocumento;
          documentoTramite.idTramite = tramiteActual.idTramite;
          documentoTramite.idUsuario = usuarioConectado.idUsuario;

          await queryRunner.manager.save(documentoTramite);
        }

        // Si existe el nuevo estado se lo asignamos al tramite
        if (estadoTramite != undefined && estadoTramite.Nombre != '') {
          for (const tramiteAsociado of docLicenciaExistente.documentoLicenciaTramite) {
            await queryRunner.manager.update(TramitesEntity, tramiteAsociado.idTramite, {
              idEstadoTramite: estadoTramite.idEstadoTramite,
              fechaRecepcionConforme: tramiteRecepcion.fechaRecepcion,
              update_at: new Date(),
            });
          }

          await queryRunner.manager.update(TramitesEntity, tramiteActual.idTramite, {
            idEstadoTramite: estadoTramite.idEstadoTramite,
            fechaRecepcionConforme: tramiteRecepcion.fechaRecepcion,
          });
        } else {
          resultado.Error = 'Error, No se encuentra el id del Estado Tramite';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }

        // Obtenemos el Id del estadoSolicitud Abierta
        let estadoAbierta = await queryRunner.manager.findOne(EstadosSolicitudEntity, { where: { codigo: CodigoSolicitudAbierta } });
        // Obtenemos el Id del estadoSolicitud Cerrada
        let estadoCerrada = await queryRunner.manager.findOne(EstadosSolicitudEntity, { where: { codigo: CodigoSolicitudCerrada } });

        // Si la Solicitud esta en estado Abierta y existe el estado Cerrada, se lo asignamos (estado 12 por reconsideracion jpl)
        if (
          estadoAbierta != undefined &&
          (tramiteActual.solicitudes.idEstado == estadoAbierta.idEstado || tramiteActual.solicitudes.idEstado == 12) &&
          estadoCerrada != undefined
        ) {
          if (tramiteRecepcion.conforme) {
            // Se marca como cerrada la solicitud, hace falta antes comprobar si hay otros trámites abiertos
            // Se recogen los demás trámites y se comprueban su estado
            const otrosTramites: TramitesEntity[] = tramiteActual.solicitudes.tramites.filter(x => x.idTramite != tramiteActual.idTramite);

            if (
              otrosTramites.length < 1 ||
              otrosTramites.every(tram =>
                [
                  CodigoEstadoTramitesDesistido,
                  CodigoEstadoTramitesAbandonado,
                  CodigoEstadoTramitesDenegacionSML,
                  CodigoEstadoTramitesDenegacionJPL,
                  CodigoEstadoTramitesRecepcionConformeFisico,
                  CodigoEstadoTramitesRecepcionConformeFisicoDigital,
                  CodigoEstadoTramitesCanceladoCerrado,
                ].includes(tram.estadoTramite.codigo)
              )
            ) {
              await queryRunner.manager.update(Solicitudes, tramiteActual.solicitudes.idSolicitud, {
                idEstado: estadoCerrada.idEstado,
                FechaCierre: new Date(),
              });
            }

            //En el caso de recepción conforme, aparte de cerrar la solicitud, se informa a mobile
            let resultadoCreacionMobile: boolean = await this.CrearConductorLicenciaMobile(docLicenciaExistente, tramiteActual);
          }
        } else {
          resultado.Error = 'Error, No se encuentra el id del Estado Solicitud';
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }

        // Completamos la Transaccion
        await queryRunner.commitTransaction();

        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'La Recepcion Conforme de los documentos se ha guardado con éxito';
      } else {
        resultado.Error = 'Error, este tramite no se puede Recepcionar Conforme';
        await queryRunner.rollbackTransaction();
      }
    } catch (error) {
      console.error(error);
      resultado.Error = 'Error guardando la Recepcion Conforme de los documentos';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  private async CrearConductorLicenciaMobile(docLicenciaExistente: DocumentosLicencia, tramiteActual: TramitesEntity): Promise<boolean> {
    try {
      let papelSeguridadCompleto = docLicenciaExistente.papelSeguridad.LetrasFolio + ' ' + docLicenciaExistente.papelSeguridad.Folio;

      let datosClasesLicenciasConFechas: DatosClasesLicenciasConFechas[] =
        await this.servicioGenerarLicenciasService.obtenerDatosClasesLicenciasFechas(
          tramiteActual.solicitudes.postulante,
          papelSeguridadCompleto,
          null
        );

      const licenciaConConductorDto: LicenciaConConductorDto = construirLicenciaConConductorDto(
        tramiteActual,
        docLicenciaExistente,
        datosClasesLicenciasConFechas
      );

      // Insertado del documento licencia en la parte Mobile
      const respuestaLicenciaMobile = await this.postLicenciaMobile(licenciaConConductorDto);

      return respuestaLicenciaMobile.ResultadoOperacion;
    } catch (Error) {
      return false;
    }
  }

  // Servicio que manda correo al denegar una licencia
  async enviarCorreoDenegacion(deniegaLicenciaDto: DeniegaLicenciaDto, req: any) {
    const resultado: Resultado = new Resultado();

    try {
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaOtorgamientosDenegaciones
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return { data: usuarioValidado };
      }

      const usuarioLogadoYRoles = usuarioValidado.Respuesta as User;

      const tramiteLicencia = await this.tramitesClaseLicenciaRespository.findOne({
        relations: [
          'tramite',
          'clasesLicencias',
          'tramite.solicitudes',
          'tramite.solicitudes.oficinas',
          'tramite.solicitudes.oficinas.instituciones',
          'tramite.solicitudes.oficinas.comunas',
          'tramite.solicitudes.oficinas.comunas.municipalidad',
          'tramite.solicitudes.estadosSolicitud',
          'tramite.solicitudes.postulante',
          'tramite.tiposTramite',
          'tramite.estadoTramite',
          'tramite.motivoDenegacion',
        ],
        where: qb => {
          qb.where('TramitesClaseLicencia.idTramite = :idTramite', { idTramite: deniegaLicenciaDto.idTramite });
        },
      });

      if (!tramiteLicencia) {
        resultado.Mensaje = 'No se obtuvo información sobre el trámite';
        resultado.ResultadoOperacion = false;

        return resultado;
      }

      if (!tramiteLicencia.tramite?.solicitudes?.postulante?.Email) {
        resultado.Mensaje = 'No se pudo obtener el email del postulante';
        resultado.ResultadoOperacion = false;

        return resultado;
      }

      const destinatario = tramiteLicencia.tramite.solicitudes.postulante.Email; // tramiteLicencia.tramite.solicitudes.postulante.Email;

      let cuerpoCorreo = plantillaCorreo;

      let runFormateadoUsuarioLogado: string = formatearRun(usuarioLogadoYRoles.RUN + '-' + usuarioLogadoYRoles.DV);

      let runFormateadoPostulante: string = formatearRun(
        tramiteLicencia.tramite.solicitudes.postulante.RUN + '-' + tramiteLicencia.tramite.solicitudes.postulante.DV
      );

      const fechaHoyString: string = moment(new Date()).format('DD/MM/YYYY');
      // Se llama al servicio que envia el correo
      let respuestaCorreo = await this.utilService.prepararObjetoCorreo(
        destinatario,
        deniegaLicenciaDto.motivo,
        cuerpoCorreo,
        'ILUSTRE ' + tramiteLicencia.tramite.solicitudes.oficinas.instituciones.Nombre.toUpperCase(),
        'CARTA DE DENEGACIÓN DE UNA CLASE DE LICENCIA DE CONDUCTOR',
        tramiteLicencia.tramite.solicitudes.oficinas.instituciones.Nombre + ', ' + fechaHoyString + '<br/><br/>',
        'Estimado ' +
          deniegaLicenciaDto.receptorDescripcionDegenacion +
          ',<br/> Yo ' +
          usuarioLogadoYRoles.Nombres +
          ' ' +
          usuarioLogadoYRoles.ApellidoPaterno +
          ' ' +
          usuarioLogadoYRoles.ApellidoMaterno +
          ', Rut ' +
          runFormateadoUsuarioLogado +
          ', con el poder que me confiere el cargo de ' +
          usuarioLogadoYRoles.rolesUsuarios[0].roles.Nombre +
          ', deniego al presente ' +
          tramiteLicencia.tramite.solicitudes.postulante.Nombres +
          ' ' +
          tramiteLicencia.tramite.solicitudes.postulante.ApellidoPaterno +
          ' ' +
          tramiteLicencia.tramite.solicitudes.postulante.ApellidoMaterno +
          ', Rut ' +
          runFormateadoPostulante +
          ', postulante a licencia de conductor clase ' +
          tramiteLicencia.clasesLicencias.Abreviacion +
          ', por el siguiente motivo: ' +
          deniegaLicenciaDto.motivoAdicional +
          ', se adjunta Id de Solicitud: ' +
          tramiteLicencia.tramite.idSolicitud +
          '.'
      );

      resultado.Mensaje = 'Correo enviado correctamente';
      resultado.ResultadoOperacion = true;

      return resultado;
    } catch (Error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error enviando correo';

      return resultado;
    }
  }

  // Sericio que obtiene los datos del Postulante para la imagen frontal de la licencia
  async obtenerDatosLicenciaFrontal(run: string) {
    const resultado: Resultado = new Resultado();

    let postulante: PostulantesEntity;

    try {
      postulante = await this.postulanteRespository.findOne({
        relations: [
          'comunas',
          'imagenesPostulante',
          'conductor',
          'conductor.documentosLicencia',
          'conductor.documentosLicencia.documentoLicenciaClaseLicencia',
          'conductor.documentosLicencia.documentoLicenciaClaseLicencia.clasesLicencias',
        ],
        where: qb => {
          qb.where("PostulantesEntity.RUN || '-' || PostulantesEntity.DV = :RUN", { RUN: run }).andWhere(
            'PostulantesEntity__conductor__documentosLicencia.fechacaducidad is null'
          );
        },
      });

      if (postulante != undefined) {
        if (postulante.imagenesPostulante.length == 0) {
          resultado.Error = 'Error: el postulante no tiene imagen'; // cuidado con cambiar este mensaje, en el front se comprueba si contiene la palabra "imagen"
          resultado.ResultadoOperacion = false;
          return resultado;
        }

        let datosLicenciaDto = new DatosLicenciaDto();
        datosLicenciaDto.nombre = postulante.Nombres;
        datosLicenciaDto.apellidos = postulante.ApellidoPaterno + ' ' + postulante.ApellidoMaterno;
        datosLicenciaDto.RUN = postulante.RUN + '-' + postulante.DV;
        datosLicenciaDto.Direccion =
          postulante.Calle + ' ' + postulante.CalleNro + ' ' + postulante.Letra + ' ' + postulante.RestoDireccion;
        datosLicenciaDto.Municipalidad = postulante.comunas.Nombre;

        if (postulante.imagenesPostulante[0]) datosLicenciaDto.foto = postulante.imagenesPostulante[0].archivo.toString();

        if (postulante.conductor && postulante.conductor.documentosLicencia) {
          datosLicenciaDto.Observaciones = postulante.conductor.documentosLicencia.Observaciones;

          const fechasLicencias: FechasLicenciaDto[] = [];

          postulante.conductor.documentosLicencia.documentoLicenciaClaseLicencia.forEach(docLicenciaClaseLicencia => {
            let fechasLicenciaDto = new FechasLicenciaDto();
            fechasLicenciaDto.nombre = docLicenciaClaseLicencia.clasesLicencias.Abreviacion;
            fechasLicenciaDto.primerOtorgamiento = docLicenciaClaseLicencia.primerOtorgamiento;
            fechasLicenciaDto.otorgamientoActual = docLicenciaClaseLicencia.otorgamientoActual;
            fechasLicenciaDto.vencimiento = docLicenciaClaseLicencia.caducidadOtorgamiento;

            fechasLicencias.push(fechasLicenciaDto);
          });

          datosLicenciaDto.licencias = fechasLicencias;
        }

        resultado.Respuesta = datosLicenciaDto;
        resultado.ResultadoOperacion = true;
      } else {
        resultado.Error = 'No se encontraron los datos del postulante';
        resultado.ResultadoOperacion = false;
      }
    } catch (error) {
      console.error(error);
      resultado.Error = 'Error obteniendo datos del postulante';
      resultado.ResultadoOperacion = false;
    }

    return resultado;
  }

  async getTramitesEnProceso(
    req: any,
    paginacionArgs: PaginacionArgs,
    run?,
    nombreApellido?,
    idSolicitud?,
    idTramite?,
    idTipoTramite?,
    idEstadoTramite?,
    idClaseLicencia?
  ) {
    let res: Resultado = new Resultado();
    let tramites: TramitesEntity[];

    // Estados de trámites admitidos :
    //                                    16 - "Pendiente Registro Civil"
    //                                    1  - "Trámite Iniciado"
    //                                    2  - "A la Espera de Informar Denegación"
    //                                    3  - "A la Espera de Informar Otorgamiento"
    //                                    19  - "Trámite re iniciado por reconsideración apelación JPL o SML"
    //                                    9  - "Otorgamiento informado y en impresión"

    const estadoTramitesPermitidos: number[] = [16, 1, 2, 3, 19, 9];

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaTramitesEnProceso
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      // 1. Hacemos la consulta
      const qbTramites = this.tramitesRespository
        .createQueryBuilder('tramites')
        .innerJoinAndMapOne('tramites.solicitudes', Solicitudes, 'solicitudes', 'tramites.idSolicitud = solicitudes.idSolicitud')
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne(
          'solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .innerJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .innerJoinAndMapOne(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        );

      // 2. Recogemos solo tramites con estados de tramites en proceso
      qbTramites.andWhere('tramites.idEstadoTramite in (:..._estadoTramitesPermitidos)', {
        _estadoTramitesPermitidos: estadoTramitesPermitidos,
      }); // 3.b Añadimos condición para filtrar por oficina si se da el caso // Filtramos por las oficinas en caso de que el usuario no sea administrador //Comprobamos si es superUsuario por si es necesario filtrar

      // 3 filtramos por oficina en caso de que no sea superuser
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null) {
          res.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          res.ResultadoOperacion = false;
          res.Respuesta = [];

          return res;
        } else {
          qbTramites.andWhere('solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
        }
      }

      // 4. Añadimos los filtros de front
      if (run) {
        const rut = run.split('-');

        const div = rut[1];
        let runsplited: string = rut[0];

        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');

        qbTramites.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbTramites.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }

      if (idTipoTramite) {
        qbTramites.andWhere('tiposTramite.idTipoTramite = :id', { id: idTipoTramite });
      }

      if (nombreApellido) {
        qbTramites.andWhere(
          '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${nombreApellido}%`,
          }
        );
      }

      if (idSolicitud) {
        qbTramites.andWhere('solicitudes.idSolicitud = :idSolicitud', {
          idSolicitud: idSolicitud,
        });
      }

      if (idTramite) {
        qbTramites.andWhere('tramites.idTramite = :_idTramite', {
          _idTramite: idTramite,
        });
      }

      if (idEstadoTramite) {
        qbTramites.andWhere('estadoTramite.idEstadoTramite = :idEstadoTramite', {
          idEstadoTramite: idEstadoTramite,
        });
      }

      if (idClaseLicencia) {
        qbTramites.andWhere('clasesLicencias.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: idClaseLicencia,
        });
      }

      if (paginacionArgs.orden.orden === 'Id' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('solicitudes.idSolicitud', 'ASC');
      if (paginacionArgs.orden.orden === 'Id' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('solicitudes.idSolicitud', 'DESC');
      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('tramites.idTramite', 'ASC');
      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('tramites.idTramite', 'DESC');
      if (paginacionArgs.orden.orden === 'RUN' && paginacionArgs.orden.ordenarPor === 'ASC') qbTramites.orderBy('postulante.RUN', 'ASC');
      if (paginacionArgs.orden.orden === 'RUN' && paginacionArgs.orden.ordenarPor === 'DESC') qbTramites.orderBy('postulante.RUN', 'DESC');
      if (paginacionArgs.orden.orden === 'NombrePostulante' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('postulante.Nombres', 'ASC');
      if (paginacionArgs.orden.orden === 'NombrePostulante' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('postulante.Nombres', 'DESC');
      if (paginacionArgs.orden.orden === 'TipoTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('tiposTramite.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'TipoTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('tiposTramite.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'EstadoSolicitud' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('estadosSolicitud.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'EstadoSolicitud' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('estadosSolicitud.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'EstadoTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('estadoTramite.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'EstadoTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('estadoTramite.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'Clase' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('clasesLicencias.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'Clase' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('clasesLicencias.Nombre', 'DESC');

      //tramites = await qbTramites.getMany();
      let tramites: any = await paginate<TramitesEntity>(qbTramites, paginacionArgs.paginationOptions);

      let tramitesEnProcesoListDto: TramitesEnProcesoDto[] = [];

      // Mapeamos a dto
      tramites.items.forEach(t => {
        let tep: TramitesEnProcesoDto = new TramitesEnProcesoDto();

        tep.idSolicitud = t.idSolicitud;
        tep.idTramite = t.idTramite;
        tep.run = t.solicitudes.postulante.RUN + '-' + t.solicitudes.postulante.DV;
        tep.nombrePostulante =
          t.solicitudes.postulante.Nombres +
          ' ' +
          t.solicitudes.postulante.ApellidoPaterno +
          ' ' +
          t.solicitudes.postulante.ApellidoMaterno;
        tep.tipoTramite = t.tiposTramite.Nombre;

        tep.estadoTramite = t.estadoTramite.Nombre;
        tep.estadoSolicitud = t.solicitudes.estadosSolicitud.Nombre;
        tep.claseLicencia = t.tramitesClaseLicencia.clasesLicencias.Abreviacion;

        tramitesEnProcesoListDto.push(tep);
      });

      tramites.items = tramitesEnProcesoListDto;
      res.ResultadoOperacion = true;
      res.Respuesta = tramites;
    } catch (err) {
      res.Error = err;
      res.ResultadoOperacion = false;
    }

    return res;
  }

  async getPostulantesEnEsperaV2(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    let res: Resultado = new Resultado();
    let orden = paginacionArgs.orden.orden;
    let ordenarPor = paginacionArgs.orden.ordenarPor;
    let solicitudesDTO: SolicitudDTO[] = [];

    try {
      // 1. Hacemos la consulta
      const qbSolicitudes = this.solicitudesEntityRespository
        .createQueryBuilder('Solicitudes')
        .innerJoinAndMapOne('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne(
          'Solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'Solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .innerJoinAndMapMany(
          'tramites.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'tramites.idTramite = colaExaminacion.idTramite'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        )
        //Inner Joins para filtros
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdTramite',
          TramitesEntity,
          'tramitesFiltroIdTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdTramite.idSolicitud'
        )
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdTipoTramite',
          TramitesEntity,
          'tramitesFiltroIdTipoTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdTipoTramite.idSolicitud'
        )
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdEstadoTramite',
          TramitesEntity,
          'tramitesFiltroIdEstadoTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdEstadoTramite.idSolicitud'
        )

        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdClaseLicencia',
          TramitesEntity,
          'tramitesFiltroIdClaseLicencia',
          'Solicitudes.idSolicitud = tramitesFiltroIdClaseLicencia.idSolicitud'
        )
        .innerJoinAndMapOne(
          'tramitesFiltroIdClaseLicencia.tramitesClaseLicenciaFiltroIdClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicenciaFiltroIdClaseLicencia',
          'tramitesFiltroIdClaseLicencia.idTramite = tramitesClaseLicenciaFiltroIdClaseLicencia.idTramite'
        );
      //   'tramite.colaExaminacionNM',
      //   'tramite.colaExaminacionNM.colaExaminacion',
      //   'tramite.colaExaminacionNM.colaExaminacion.tipoExaminaciones'

      // Estados de trámites admitidos :
      //
      // Acusar Recibo Conforme                           5
      // A la espera de informar Otorgamiento             3
      // A la espera de informar Denegacion               2
      // Otorgamiento informado y en impresion            9

      //const estadoTramitesPermitidos: number[] = [2, 3, 5, 9];

      qbSolicitudes.where('tramitesFiltroIdEstadoTramite.idEstadoTramite = 5');
      qbSolicitudes.orWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = 9');
      qbSolicitudes.orWhere(
        new Brackets(subQ =>
          subQ
            .andWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = 3')
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 5 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 5) < 1)' +
                ')'
            )
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 4 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 4) < 1)' +
                ')'
            )
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 3 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 3) < 1)' +
                ')'
            )
            .andWhere(
              '(' +
                '((SELECT 	count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 2 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                ' OR ' +
                '((SELECT count(*)' +
                'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 2) < 1)' +
                ')'
            )
        )
      );
      qbSolicitudes.orWhere(
        new Brackets(subQ =>
          subQ
            .andWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = 2')

            .andWhere(
              new Brackets(subQ2 =>
                subQ2

                  .orWhere(
                    '(' +
                      '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                      'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 5) > 0)' +
                      ')'
                  )

                  .orWhere(
                    '(' +
                      '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                      'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 4) > 0)' +
                      ')'
                  )

                  .orWhere(
                    '(' +
                      '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                      'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 3) > 0)' +
                      ')'
                  )

                  .orWhere(
                    '(' +
                      '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                      'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 2) > 0)' +
                      ')'
                  )
              )
            )
        )
      );
      // {
      //     //_idEstadosTramites: estadoTramitesPermitidos,

      //     new Brackets(subQb =>
      //       {
      //         subQb
      //           .where( 'tramitesFiltroIdEstadoTramite.idEstadoTramite = 5' )
      //           .orWhere( 'tramitesFiltroIdEstadoTramite.idEstadoTramite = 9' )
      //           .orWhere( 'tramitesFiltroIdEstadoTramite.idEstadoTramite = 2' )
      //           .orWhere( 'tramitesFiltroIdEstadoTramite.idEstadoTramite = 3' )
      //         }
      //     )

      // };

      if (orden === 'Id' && ordenarPor === 'ASC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'ASC');
      if (orden === 'Id' && ordenarPor === 'DESC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'DESC');
      if (orden === 'RUN' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.RUN', 'ASC');
      if (orden === 'RUN' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.RUN', 'DESC');
      if (orden === 'NombrePostulante' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.Nombres', 'ASC');
      if (orden === 'NombrePostulante' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.Nombres', 'DESC');
      if (orden === 'claseLicencia' && ordenarPor === 'ASC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'ASC');
      if (orden === 'claseLicencia' && ordenarPor === 'DESC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'DESC');

      // 2. Recogemos solo tramites con estados de tramites en proceso
      //qbSolicitudes.andWhere('estadoTramite.idEstadoTramite in (:estadoTramitesPermitidos)', { estados: estadoTramitesPermitidos})

      // 3 filtramos por oficina en caso de que no sea superuser
      //qb...... TODO

      const resultadoSolicitudes: any = await paginate<Solicitudes>(qbSolicitudes, paginacionArgs.paginationOptions);

      // 5 Mapeamos la respuesta
      resultadoSolicitudes.items.forEach(solicitud => {
        let solicitudDto: SolicitudDTO = new SolicitudDTO();

        solicitudDto.idSolicitud = solicitud.idSolicitud;
        solicitudDto.run = solicitud.postulante.RUN + '-' + solicitud.postulante.DV;
        solicitudDto.nombrePostulante =
          solicitud.postulante.Nombres + ' ' + solicitud.postulante.ApellidoPaterno + ' ' + solicitud.postulante.ApellidoMaterno;

        solicitud.tramites.forEach(tramite => {
          let tramiteDto = new TramiteDTO();

          tramiteDto.claseLicencia = tramite.tramitesClaseLicencia.clasesLicencias.Abreviacion;
          tramiteDto.estadoTramite = tramite.estadoTramite.Nombre;
          tramiteDto.tipoTramite = tramite.tiposTramite.Nombre;

          solicitudDto.tramites.push(tramiteDto);
        });

        solicitudesDTO.push(solicitudDto);
      });

      resultadoSolicitudes.items = solicitudesDTO;

      res.ResultadoOperacion = true;
      res.Respuesta = resultadoSolicitudes;
    } catch (err) {
      res.Error = err;
      res.ResultadoOperacion = false;
    }

    return res;
  }

  // Según un trámite en estado de otorgamiento, recoger su fecha de examinación aprobada más actual
  private SeleccionarFechaAprobacionMasActual(tramite: TramitesEntity): Date {
    let resExaminacion: Date = null;

    tramite.colaExaminacionNM.forEach(cenm => {
      cenm.colaExaminacion.resultadoExaminacionCola.forEach(rec => {
        if (resExaminacion == null || resExaminacion < rec.resultadoExaminacion.fechaAprobacion)
          resExaminacion = rec.resultadoExaminacion.fechaAprobacion;
      });
    });

    return resExaminacion;
  }

  async comprobarEstadoCorrectoRPI(run: string) {
    let resultado: Resultado = new Resultado();

    try {
      //Llamada al servicio de consulta de estado de RPI para validar usuario
      let resultadoRegistroCivil = this.getEstadoRPIPostulante(run);

      if (true) {
        resultado.Mensaje = 'Estado correcto en RPI';
        resultado.Respuesta = true;
        resultado.ResultadoOperacion = true;

        return resultado;
      } else {
        resultado.Mensaje = 'Estado no correcto en RPI';
        resultado.Respuesta = false;
        resultado.ResultadoOperacion = true;

        return resultado;
      }
    } catch (Error) {
      resultado.Error = 'Se ha producido un error al conectar con registro civil, inténtelo más tarde.';
      resultado.ResultadoOperacion = false;

      return resultado;
    }
  }

  // Llamada al back de Registro Civil, se le pasa por parametros la URL a la que llamará
  async getEstadoRPIPostulante(run: string): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    const request = require('request');

    const tokenEncriptado = this.utilService.encriptar();

    try {
      return new Promise(function (resolve, reject) {
        request.post(
          BASE_URL_REGISTRO_CIVIL + URL_REGISTRO_CIVIL_ESTADO_RPI_POSTULANTE,
          { json: {}, headers: { Authorization: tokenEncriptado } },
          function (error, res, body) {
            if (!error && res.statusCode == 200) {
              resultado.Respuesta = body.respuesta;

              resultado.ResultadoOperacion = true;
              resolve(resultado);
            } else {
              console.log(error);
              resultado.Error = error ? error : 'Error conectando a registro civil';
              resultado.ResultadoOperacion = false;
              resolve(resultado);
            }
          }
        );
      });
    } catch (error) {
      console.log(error);
      resultado.Error = 'Error conectando a registro civil';
      resultado.ResultadoOperacion = false;
      return resultado;
    }
  }

  async getTramitesV2(paginacionArgs: PaginacionArgs, relations, estadosTramiteConsulta?: number[]): Promise<Resultado> {
    //const idOficina = this.authService.oficina().idOficina;

    const resultado = new Resultado();
    const orderBy = ColumnasTramites[paginacionArgs.orden.orden];

    let tramitesCLPaginados: Pagination<TramitesClaseLicencia>;
    let tramitesParseados = new PaginacionDtoParser<TramitesEnProcesoDto>();
    let filtro = '';
    let filtroString = '';

    try {
      if (paginacionArgs.filtro != null && paginacionArgs.filtro != '') {
        filtro = paginacionArgs.filtro;
        filtroString = filtro as string;
      }

      tramitesCLPaginados = await paginate<TramitesClaseLicencia>(this.tramitesClaseLicenciaRespository, paginacionArgs.paginationOptions, {
        relations: relations,

        where: qb => {
          // Si estadosTramite es nulo o no viene con un estado al menos, no filtramos, en caso contrario filtramos por los estados que nos vienen
          if (estadosTramiteConsulta && estadosTramiteConsulta.length > 0) {
            qb.andWhere('TramitesClaseLicencia__tramite__estadoTramite.idEstadoTramite IN(:...estadoTramites)', {
              estadoTramites: estadosTramiteConsulta,
            });
          }

          qb.andWhere(
            new Brackets(subQb => {
              subQb
                .where(
                  "lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.Nombres)) || ' ' || " +
                    "lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoPaterno)) || ' ' || " +
                    'lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoMaterno)) like lower(unaccent(:nombre))',
                  { nombre: `%${filtroString}%` }
                )
                .orWhere(
                  "lower(unaccent(TramitesClaseLicencia__tramite__tiposTramite.Nombre)) || ' ' || " +
                    'lower(unaccent(TramitesClaseLicencia__clasesLicencias.Abreviacion)) like lower(unaccent(:tipoTramite))',
                  { tipoTramite: `%${filtroString}%` }
                )
                .orWhere('lower(unaccent(TramitesClaseLicencia__tramite__estadoTramite.Nombre)) like lower(unaccent(:estadoTramite))', {
                  estadoTramite: `%${filtroString}%`,
                })
                .orWhere(
                  "TramitesClaseLicencia__tramite__solicitudes__postulante.RUN || '-' || " +
                    'TramitesClaseLicencia__tramite__solicitudes__postulante.DV like :RUN',
                  { RUN: `%${filtro}%` }
                )
                .orWhere('CAST(TramitesClaseLicencia__tramite.idSolicitud AS TEXT) like :idSolicitud', { idSolicitud: `%${filtro}%` });
            })
          ).orderBy(orderBy, paginacionArgs.orden.ordenarPor);
        },
      });

      //console.log(' ============================================================================================================== ');
      // console.log(tramitesCLPaginados.items);
      //console.log(' ============================================================================================================== ');

      let tramitesDTO: TramitesEnProcesoDto[] = [];
      let postulante: PostulantesEntity;

      if (Array.isArray(tramitesCLPaginados.items) && tramitesCLPaginados.items.length) {
        tramitesCLPaginados.items.forEach(tramiteCL => {
          const tramiteDTO: TramitesEnProcesoDto = new TramitesEnProcesoDto();
          let datosLicenciaDto = new DatosLicenciaDto();

          tramiteDTO.idSolicitud = tramiteCL.tramite.idSolicitud;
          tramiteDTO.idTramite = tramiteCL.tramite.idTramite;
          tramiteDTO.run = tramiteCL.tramite.solicitudes.postulante.RUN.toString() + '-' + tramiteCL.tramite.solicitudes.postulante.DV;
          tramiteDTO.nombrePostulante =
            tramiteCL.tramite.solicitudes.postulante.Nombres +
            ' ' +
            tramiteCL.tramite.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            tramiteCL.tramite.solicitudes.postulante.ApellidoMaterno;
          tramiteDTO.tipoTramite = tramiteCL.tramite.tiposTramite.Nombre;
          tramiteDTO.idTipoTramite = tramiteCL.tramite.tiposTramite.idTipoTramite;
          tramiteDTO.idComuna = tramiteCL.tramite.solicitudes.postulante.idComuna;
          tramiteDTO.estadoTramite = tramiteCL.tramite.estadoTramite.Nombre;
          tramiteDTO.estado = tramiteCL.tramite.estadoTramite.Nombre;
          tramiteDTO.estadoSolicitud = tramiteCL.tramite.solicitudes.estadosSolicitud.Nombre;
          tramiteDTO.municipalidadSolicitudString = tramiteCL.tramite.solicitudes.oficinas
            ? tramiteCL.tramite.solicitudes.oficinas.instituciones.Nombre
            : '';

          if (tramiteCL.clasesLicencias) {
            tramiteDTO.claseLicencia = tramiteCL.clasesLicencias.Abreviacion;
            tramiteDTO.idClaseLicencia = tramiteCL.clasesLicencias.idClaseLicencia;
          } else tramiteDTO.claseLicencia = '';

          tramiteDTO.examenTeorico = 0;
          tramiteDTO.examenPractico = 0;
          tramiteDTO.examenMedico = 0;
          tramiteDTO.idoneidadMoral = 0;
          if (tramiteCL.tramite.colaExaminacionNM && tramiteCL.tramite.colaExaminacionNM.length > 0) {
            tramiteCL.tramite.colaExaminacionNM.forEach(examinacion => {
              if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenTeorico)
                tramiteDTO.examenTeorico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenPractico)
                tramiteDTO.examenPractico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenMedico)
                tramiteDTO.examenMedico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenIM)
                tramiteDTO.idoneidadMoral =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
            });
          }

          tramitesDTO.push(tramiteDTO);
        });

        tramitesParseados.items = tramitesDTO;
        tramitesParseados.meta = tramitesCLPaginados.meta;
        tramitesParseados.links = tramitesCLPaginados.links;

        resultado.Respuesta = tramitesParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }

  async getTramitesPendientesOtorgarDenegar(paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const estadosOtogarmientoDenegacion: number[] = [estadosTramitesOtorgamientoId, estadoTramitesEsperaDenegacionLicenciaId];

    const resultado = new Resultado();
    const orderBy = ColumnasTramites[paginacionArgs.orden.orden];

    let tramitesCLPaginados: Pagination<TramitesClaseLicencia>;
    let tramitesParseados = new PaginacionDtoParser<TramitesEnProcesoDto>();
    let filtro = '';
    let filtroString = '';

    try {
      if (paginacionArgs.filtro != null && paginacionArgs.filtro != '') {
        filtro = paginacionArgs.filtro;
        filtroString = filtro as string;
      }

      tramitesCLPaginados = await paginate<TramitesClaseLicencia>(this.tramitesClaseLicenciaRespository, paginacionArgs.paginationOptions, {
        relations: [
          'tramite',
          'clasesLicencias',
          'tramite.solicitudes',
          'tramite.solicitudes.postulante',
          'tramite.solicitudes.estadosSolicitud',
          'tramite.tiposTramite',
          'tramite.estadoTramite',
          'tramite.colaExaminacion',
          'tramite.colaExaminacionNM',
          'tramite.colaExaminacionNM.colaExaminacion',
          'tramite.colaExaminacionNM.colaExaminacion.tipoExaminaciones',
          'tramite.solicitudes.oficinas',
          'tramite.solicitudes.oficinas.instituciones',
        ],

        where: qb => {
          // Si estadosTramite es nulo o no viene con un estado al menos, no filtramos, en caso contrario filtramos por los estados que nos vienen

          qb.andWhere('TramitesClaseLicencia__tramite__estadoTramite.idEstadoTramite IN(:...estadoTramites)', {
            estadoTramites: estadosOtogarmientoDenegacion,
          });

          qb.andWhere(
            new Brackets(subQb => {
              subQb
                .where(
                  "lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.Nombres)) || ' ' || " +
                    "lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoPaterno)) || ' ' || " +
                    'lower(unaccent(TramitesClaseLicencia__tramite__solicitudes__postulante.ApellidoMaterno)) like lower(unaccent(:nombre))',
                  { nombre: `%${filtroString}%` }
                )
                .orWhere(
                  "lower(unaccent(TramitesClaseLicencia__tramite__tiposTramite.Nombre)) || ' ' || " +
                    'lower(unaccent(TramitesClaseLicencia__clasesLicencias.Abreviacion)) like lower(unaccent(:tipoTramite))',
                  { tipoTramite: `%${filtroString}%` }
                )
                .orWhere('lower(unaccent(TramitesClaseLicencia__tramite__estadoTramite.Nombre)) like lower(unaccent(:estadoTramite))', {
                  estadoTramite: `%${filtroString}%`,
                })
                .orWhere(
                  "TramitesClaseLicencia__tramite__solicitudes__postulante.RUN || '-' || " +
                    'TramitesClaseLicencia__tramite__solicitudes__postulante.DV like :RUN',
                  { RUN: `%${filtro}%` }
                )
                .orWhere('CAST(TramitesClaseLicencia__tramite.idSolicitud AS TEXT) like :idSolicitud', { idSolicitud: `%${filtro}%` });
            })
          );

          qb.andWhere(
            new Brackets(subQb => {
              subQb
                .where(
                  new Brackets(subQ2 => {
                    // En el primer sub Where vamos a indicar que se muestren las que tengan o examinación idon moral, médica, teórica o práctica aprobada
                    // o que esta no exista para aprobarse    TramitesClaseLicencia__tramite.idTramite
                    subQ2
                      .andWhere(
                        '(' +
                          '((SELECT 	count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 5 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                          ' OR ' +
                          '((SELECT count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 5) < 1)' +
                          ')'
                      )
                      .andWhere(
                        '(' +
                          '((SELECT 	count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 4 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                          ' OR ' +
                          '((SELECT count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 4) < 1)' +
                          ')'
                      )
                      .andWhere(
                        '(' +
                          '((SELECT 	count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 3 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                          ' OR ' +
                          '((SELECT count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 3) < 1)' +
                          ')'
                      )
                      .andWhere(
                        '(' +
                          '((SELECT 	count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 2 AND "CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite) > 0)' +
                          ' OR ' +
                          '((SELECT count(*)' +
                          'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                          'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                          'WHERE	"CETNMAux"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND"CEAux"."idTipoExaminacion" = 2) < 1)' +
                          ')'
                      );
                  })
                )

                .orWhere(
                  '(' +
                    '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                    'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                    'WHERE "CETNMAuxDen"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 5) > 0)' +
                    ')'
                )

                .orWhere(
                  '(' +
                    '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                    'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                    'WHERE "CETNMAuxDen"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 4) > 0)' +
                    ')'
                )

                .orWhere(
                  '(' +
                    '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                    'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                    'WHERE "CETNMAuxDen"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 3) > 0)' +
                    ')'
                )

                .orWhere(
                  '(' +
                    '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                    'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                    'WHERE "CETNMAuxDen"."idTramite" = TramitesClaseLicencia__tramite.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 2) > 0)' +
                    ')'
                );
            })
          ).orderBy(orderBy, paginacionArgs.orden.ordenarPor);
        },
      });

      //console.log(' ============================================================================================================== ');
      // console.log(tramitesCLPaginados.items);
      //console.log(' ============================================================================================================== ');

      let tramitesDTO: TramitesEnProcesoDto[] = [];
      let postulante: PostulantesEntity;

      if (Array.isArray(tramitesCLPaginados.items) && tramitesCLPaginados.items.length) {
        tramitesCLPaginados.items.forEach(tramiteCL => {
          const tramiteDTO: TramitesEnProcesoDto = new TramitesEnProcesoDto();
          let datosLicenciaDto = new DatosLicenciaDto();

          tramiteDTO.idSolicitud = tramiteCL.tramite.idSolicitud;
          tramiteDTO.idTramite = tramiteCL.tramite.idTramite;
          tramiteDTO.run = tramiteCL.tramite.solicitudes.postulante.RUN.toString() + '-' + tramiteCL.tramite.solicitudes.postulante.DV;
          tramiteDTO.nombrePostulante =
            tramiteCL.tramite.solicitudes.postulante.Nombres +
            ' ' +
            tramiteCL.tramite.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            tramiteCL.tramite.solicitudes.postulante.ApellidoMaterno;
          tramiteDTO.tipoTramite = tramiteCL.tramite.tiposTramite.Nombre;
          tramiteDTO.idTipoTramite = tramiteCL.tramite.tiposTramite.idTipoTramite;
          tramiteDTO.idComuna = tramiteCL.tramite.solicitudes.postulante.idComuna;
          tramiteDTO.estadoTramite = tramiteCL.tramite.estadoTramite.Nombre;
          tramiteDTO.estado = tramiteCL.tramite.estadoTramite.Nombre;
          tramiteDTO.estadoSolicitud = tramiteCL.tramite.solicitudes.estadosSolicitud.Nombre;
          tramiteDTO.municipalidadSolicitudString = tramiteCL.tramite.solicitudes.oficinas
            ? tramiteCL.tramite.solicitudes.oficinas.instituciones.Nombre
            : '';

          tramiteDTO.fechaSolicitud = tramiteCL.tramite.solicitudes.FechaCreacion;

          if (tramiteCL.clasesLicencias) {
            tramiteDTO.claseLicencia = tramiteCL.clasesLicencias.Abreviacion;
            tramiteDTO.idClaseLicencia = tramiteCL.clasesLicencias.idClaseLicencia;
          } else tramiteDTO.claseLicencia = '';

          tramiteDTO.examenTeorico = 0;
          tramiteDTO.examenPractico = 0;
          tramiteDTO.examenMedico = 0;
          tramiteDTO.idoneidadMoral = 0;
          if (tramiteCL.tramite.colaExaminacionNM && tramiteCL.tramite.colaExaminacionNM.length > 0) {
            tramiteCL.tramite.colaExaminacionNM.forEach(examinacion => {
              if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenTeorico)
                tramiteDTO.examenTeorico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenPractico)
                tramiteDTO.examenPractico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenMedico)
                tramiteDTO.examenMedico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenIM)
                tramiteDTO.idoneidadMoral =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
            });
          }

          tramitesDTO.push(tramiteDTO);
        });

        tramitesParseados.items = tramitesDTO;
        tramitesParseados.meta = tramitesCLPaginados.meta;
        tramitesParseados.links = tramitesCLPaginados.links;

        resultado.Respuesta = tramitesParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }

  async getTramitesEnVisualizacionLicencia(
    req: any,
    paginacionArgs: PaginacionArgs,
    run?,
    nombreApellido?,
    idSolicitud?,
    idTipoTramite?,
    idClaseLicencia?
  ) {
    let resultado: Resultado = new Resultado();
    let tramitesParseados = new PaginacionDtoParser<TramitesEnProcesoDto>();

    // Estados de trámites admitidos :
    //                                    16 - "Pendiente Registro Civil"
    //                                    1  - "Trámite Iniciado"
    //                                    2  - "A la Espera de Informar Denegación"
    //                                    3  - "A la Espera de Informar Otorgamiento"
    //                                    19  - "Trámite re iniciado por reconsideración apelación JPL o SML"
    //                                    9  - "Otorgamiento informado y en impresión"

    const estadoTramitesPermitidos: number[] = [9];

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaTramitesEnVisualizacionLicencia
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      // 1. Hacemos la consulta
      const qbTramites = this.tramitesRespository
        .createQueryBuilder('tramites')
        .innerJoinAndMapOne('tramites.solicitudes', Solicitudes, 'solicitudes', 'tramites.idSolicitud = solicitudes.idSolicitud')
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne(
          'solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        //.innerJoinAndMapMany('tramites.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
        //.innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .leftJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .leftJoinAndMapOne(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .leftJoinAndMapOne(
          'colaExaminacion.tipoExaminacion',
          TipoExaminacionesEntity,
          'tipoExaminacion',
          'colaExaminacion.idTipoExaminacion = tipoExaminacion.idTipoExaminacion'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
          //   'tramite.colaExaminacionNM',
          //   'tramite.colaExaminacionNM.colaExaminacion',
          //   'tramite.colaExaminacionNM.colaExaminacion.tipoExaminaciones'
        );

      // 2. Recogemos solo tramites con estados de tramites en proceso
      qbTramites.andWhere('tramites.idEstadoTramite in (:..._estadoTramitesPermitidos)', {
        _estadoTramitesPermitidos: estadoTramitesPermitidos,
      });

      // 3 filtramos por oficina en caso de que no sea superuser
      // 3.b Añadimos condición para filtrar por oficina si se da el caso
      // Filtramos por las oficinas en caso de que el usuario no sea administrador
      //Comprobamos si es superUsuario por si es necesario filtrar
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null) {
          resultado.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          resultado.ResultadoOperacion = false;
          resultado.Respuesta = [];

          return resultado;
        } else {
          qbTramites.andWhere('solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
        }
      }

      // 4. Añadimos los filtros de front
      if (run) {
        const rut = run.split('-');

        const div = rut[1];
        let runsplited: string = rut[0];

        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');

        qbTramites.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbTramites.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }

      if (idTipoTramite) {
        qbTramites.andWhere('tiposTramite.idTipoTramite = :id', { id: idTipoTramite });
      }

      if (nombreApellido) {
        qbTramites.andWhere(
          '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${nombreApellido}%`,
          }
        );
      }

      if (idSolicitud) {
        qbTramites.andWhere('solicitudes.idSolicitud = :idSolicitud', {
          idSolicitud: idSolicitud,
        });
      }

      // if (idTramite) {
      //   qbTramites.andWhere('tramites.idTramite = :_idTramite', {
      //     _idTramite: idTramite,
      //   });
      // }

      if (idClaseLicencia) {
        qbTramites.andWhere('clasesLicencias.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: idClaseLicencia,
        });
      }

      if (paginacionArgs.orden.orden === 'Id' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('solicitudes.idSolicitud', 'ASC');
      if (paginacionArgs.orden.orden === 'Id' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('solicitudes.idSolicitud', 'DESC');
      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('tramites.idTramite', 'ASC');
      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('tramites.idTramite', 'DESC');
      if (paginacionArgs.orden.orden === 'RUN' && paginacionArgs.orden.ordenarPor === 'ASC') qbTramites.orderBy('postulante.RUN', 'ASC');
      if (paginacionArgs.orden.orden === 'RUN' && paginacionArgs.orden.ordenarPor === 'DESC') qbTramites.orderBy('postulante.RUN', 'DESC');
      if (paginacionArgs.orden.orden === 'NombrePostulante' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('postulante.Nombres', 'ASC');
      if (paginacionArgs.orden.orden === 'NombrePostulante' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('postulante.Nombres', 'DESC');
      if (paginacionArgs.orden.orden === 'TipoTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('tiposTramite.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'TipoTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('tiposTramite.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'EstadoSolicitud' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('estadosSolicitud.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'EstadoSolicitud' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('estadosSolicitud.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'EstadoTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('estadoTramite.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'EstadoTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('estadoTramite.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'Clase' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('clasesLicencias.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'Clase' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('clasesLicencias.Nombre', 'DESC');

      let tramitesDTO: TramitesEnProcesoDto[] = [];
      let postulante: PostulantesEntity;

      //tramites = await qbTramites.getMany();
      let tramitesCLPaginados: any = await paginate<TramitesEntity>(qbTramites, paginacionArgs.paginationOptions);

      if (Array.isArray(tramitesCLPaginados.items) && tramitesCLPaginados.items.length) {
        tramitesCLPaginados.items.forEach(tramiteCL => {
          const tramiteDTO: TramitesEnProcesoDto = new TramitesEnProcesoDto();
          let datosLicenciaDto = new DatosLicenciaDto();

          tramiteDTO.idSolicitud = tramiteCL.idSolicitud;
          tramiteDTO.idTramite = tramiteCL.idTramite;
          tramiteDTO.run = tramiteCL.solicitudes.postulante.RUN.toString() + '-' + tramiteCL.solicitudes.postulante.DV;
          tramiteDTO.nombrePostulante =
            tramiteCL.solicitudes.postulante.Nombres +
            ' ' +
            tramiteCL.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            tramiteCL.solicitudes.postulante.ApellidoMaterno;
          tramiteDTO.tipoTramite = tramiteCL.tiposTramite.Nombre;
          tramiteDTO.idTipoTramite = tramiteCL.tiposTramite.idTipoTramite;
          tramiteDTO.idComuna = tramiteCL.solicitudes.postulante.idComuna;
          tramiteDTO.estadoTramite = tramiteCL.estadoTramite.Nombre;
          tramiteDTO.estado = tramiteCL.estadoTramite.Nombre;
          tramiteDTO.estadoSolicitud = tramiteCL.solicitudes.estadosSolicitud.Nombre;
          tramiteDTO.municipalidadSolicitudString = tramiteCL.solicitudes.oficinas
            ? tramiteCL.solicitudes.oficinas.instituciones.Nombre
            : '';

          if (tramiteCL.tramitesClaseLicencia.clasesLicencias) {
            tramiteDTO.claseLicencia = tramiteCL.tramitesClaseLicencia.clasesLicencias.Abreviacion;
            tramiteDTO.idClaseLicencia = tramiteCL.tramitesClaseLicencia.clasesLicencias.idClaseLicencia;
          } else tramiteDTO.claseLicencia = '';

          tramiteDTO.examenTeorico = 0;
          tramiteDTO.examenPractico = 0;
          tramiteDTO.examenMedico = 0;
          tramiteDTO.idoneidadMoral = 0;
          if (tramiteCL.colaExaminacionNM && tramiteCL.colaExaminacionNM.length > 0) {
            tramiteCL.colaExaminacionNM.forEach(examinacion => {
              if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenTeorico)
                tramiteDTO.examenTeorico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenPractico)
                tramiteDTO.examenPractico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenMedico)
                tramiteDTO.examenMedico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenIM)
                tramiteDTO.idoneidadMoral =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
            });
          }

          tramitesDTO.push(tramiteDTO);
        });

        tramitesParseados.items = tramitesDTO;
        tramitesParseados.meta = tramitesCLPaginados.meta;
        tramitesParseados.links = tramitesCLPaginados.links;

        resultado.Respuesta = tramitesParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }

  async getTramitesEnOtorgamientoDenegaciones(
    req: any,
    paginacionArgs: PaginacionArgs,
    run?,
    nombreApellido?,
    idSolicitud?,
    idTipoTramite?,
    idClaseLicencia?,
    idEstadoTramite?,
    fechaSolicitud?
  ) {
    let resultado: Resultado = new Resultado();
    let tramitesParseados = new PaginacionDtoParser<TramitesEnProcesoDto>();

    // Estados de trámites admitidos :
    //                                    16 - "Pendiente Registro Civil"
    //                                    1  - "Trámite Iniciado"
    //                                    2  - "A la Espera de Informar Denegación"
    //                                    3  - "A la Espera de Informar Otorgamiento"
    //                                    19  - "Trámite re iniciado por reconsideración apelación JPL o SML"
    //                                    9  - "Otorgamiento informado y en impresión"

    const estadoTramitesPermitidos: number[] = [2];

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaOtorgamientosDenegaciones
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      // Nueva Query
      //Calcular a partir de qué registro nos traemos
      let offset: number = 0;
      if (paginacionArgs.paginationOptions.page != 1)
        offset = ((paginacionArgs.paginationOptions.page as number) - 1) * (paginacionArgs.paginationOptions.limit as number);

      //Recogemos según los ids de oportunidades Examinación que existan en la tabla que recuperamos antes desde la función

      let columnaOrden: string = null;
      let modoOrden: string = null; // true ASC / false DESC

      if (paginacionArgs.orden.orden === 'Id') columnaOrden = '"solicitudes_idSolicitud"';
      else if (paginacionArgs.orden.orden === 'RUN') columnaOrden = '"postulante_RUN"';
      else if (paginacionArgs.orden.orden === 'NombrePostulante') columnaOrden = '"postulante_Nombres"';
      else if (paginacionArgs.orden.orden === 'FechaInicio') columnaOrden = '"solicitudes_FechaCreacion"';

      if (paginacionArgs.orden.ordenarPor && paginacionArgs.orden.ordenarPor != null) {
        if (paginacionArgs.orden.ordenarPor === 'ASC') modoOrden = 'ASC';
        else modoOrden = 'DESC';
      }

      let filtroOficinasPermisos: string = null;
      // 3.b Añadimos condición para filtrar por oficina si se da el caso
      // Filtramos por las oficinas en caso de que el usuario no sea administrador
      //Comprobamos si es superUsuario por si es necesario filtrar
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null && idOficinasAsociadas.length < 1) {
          resultado.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          resultado.ResultadoOperacion = false;
          resultado.Respuesta = [];

          return resultado;
        } else {
          filtroOficinasPermisos = '(' + idOficinasAsociadas.join(',') + ')';
        }
      }

      const queryUnionOtorgamientoDenegacionPendientes: string = this.getQueryUnionPendientesDenegacionXPendientesOtorgamiento(
        false,
        columnaOrden,
        modoOrden,
        filtroOficinasPermisos
      );
      const queryUnionOtorgamientoDenegacionPendientesCount: string = this.getQueryUnionPendientesDenegacionXPendientesOtorgamiento(
        true,
        columnaOrden,
        modoOrden,
        filtroOficinasPermisos
      );

      // Llamamos a la query que hace la unión
      const dengacionesXotorgamientosQuery: DenegacionesXOtorgamientosPendientes[] = (await getConnection().manager.query(
        queryUnionOtorgamientoDenegacionPendientes,
        [
          idSolicitud && idSolicitud != '' ? idSolicitud : null,
          fechaSolicitud ? moment(fechaSolicitud).format('YYYY-MM-DD') : null,
          idTipoTramite ? idTipoTramite : null,
          run ? devolverValorRUN(run) : null,
          idClaseLicencia ? idClaseLicencia : null,
          idEstadoTramite ? idEstadoTramite : null,
          nombreApellido ? '%' + nombreApellido + '%' : null,
          paginacionArgs.paginationOptions.limit,
          offset,
        ]
      )) as DenegacionesXOtorgamientosPendientes[];

      // Llamamos a la query que hace el conteo del total de elementos                                                                                                                                                  // Llamamos a la query que hace la unión
      const dengacionesXotorgamientosQueryCount: any = await getConnection().manager.query(
        queryUnionOtorgamientoDenegacionPendientesCount,
        [
          idSolicitud && idSolicitud != '' ? idSolicitud : null,
          fechaSolicitud ? moment(fechaSolicitud).format('YYYY-MM-DD') : null,
          idTipoTramite ? idTipoTramite : null,
          run ? devolverValorRUN(run) : null,
          idClaseLicencia ? idClaseLicencia : null,
          idEstadoTramite ? idEstadoTramite : null,
          nombreApellido && nombreApellido != '' ? '%' + nombreApellido + '%' : null,
        ]
      );

      let contadorTotalElementos = dengacionesXotorgamientosQueryCount[0].count;

      //Recogemos según los ids de prorroga Examinación que existan en la tabla que recuperamos antes desde la función
      const idsDenegaciones = (
        dengacionesXotorgamientosQuery.filter(x => x.estadotramiteidentificador == 2) as DenegacionesXOtorgamientosPendientes[]
      ).map(x => x.idsolicitud);
      const idsOtorgamientos = (
        dengacionesXotorgamientosQuery.filter(x => x.estadotramiteidentificador == 3) as DenegacionesXOtorgamientosPendientes[]
      ).map(x => x.idsolicitud);

      // 1. Hacemos la consulta para denegaciones

      const qbSolicitudesDenegaciones = this.solicitudesEntityRespository
        .createQueryBuilder('solicitudes')
        .innerJoinAndMapMany('solicitudes.tramites', TramitesEntity, 'tramites', 'solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne(
          'solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        .innerJoinAndMapOne('solicitudes.oficinas', OficinasEntity, 'oficinas', 'solicitudes.idOficina = oficinas.idOficina')
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .innerJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .innerJoinAndMapOne(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .innerJoinAndMapOne(
          'colaExaminacion.tipoExaminaciones',
          TipoExaminacionesEntity,
          'tipoExaminaciones',
          'colaExaminacion.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        );

      qbSolicitudesDenegaciones.andWhere('(solicitudes.idSolicitud IN (:..._idsDenegaciones))', {
        _idsDenegaciones: idsDenegaciones,
      });

      qbSolicitudesDenegaciones.andWhere('(tramites.idEstadoTramite = 2)');

      // 2. Hacemos la consulta para otorgamientos

      const qbSolicitudesOtorgamientos = this.solicitudesEntityRespository
        .createQueryBuilder('solicitudes')
        .innerJoinAndMapMany('solicitudes.tramites', TramitesEntity, 'tramites', 'solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne(
          'solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        .innerJoinAndMapOne('solicitudes.oficinas', OficinasEntity, 'oficinas', 'solicitudes.idOficina = oficinas.idOficina')
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .leftJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .leftJoinAndMapOne(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .leftJoinAndMapOne(
          'colaExaminacion.tipoExaminaciones',
          TipoExaminacionesEntity,
          'tipoExaminaciones',
          'colaExaminacion.idTipoExaminacion = tipoExaminaciones.idTipoExaminacion'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        );

      // Orders by internos por trámites

      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'ASC') {
        qbSolicitudesDenegaciones.orderBy('tramites.idTramite', 'ASC');
        qbSolicitudesOtorgamientos.orderBy('tramites.idTramite', 'ASC');
      }
      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'DESC') {
        qbSolicitudesDenegaciones.orderBy('tramites.idTramite', 'DESC');
        qbSolicitudesOtorgamientos.orderBy('tramites.idTramite', 'DESC');
      }

      if (paginacionArgs.orden.orden === 'ClaseLicencia' && paginacionArgs.orden.ordenarPor === 'ASC') {
        qbSolicitudesDenegaciones.orderBy('clasesLicencias.idClaseLicencia', 'ASC');
        qbSolicitudesOtorgamientos.orderBy('clasesLicencias.idClaseLicencia', 'ASC');
      }
      if (paginacionArgs.orden.orden === 'ClaseLicencia' && paginacionArgs.orden.ordenarPor === 'DESC') {
        qbSolicitudesDenegaciones.orderBy('clasesLicencias.idClaseLicencia', 'DESC');
        qbSolicitudesOtorgamientos.orderBy('clasesLicencias.idClaseLicencia', 'DESC');
      }

      // else if(paginacionArgs.orden.orden === 'TipoTramiteString')
      //   columnaOrden = 'nombreTipoTramite';
      // else if(paginacionArgs.orden.orden === 'EstadoTramiteString')
      //   columnaOrden = 'nombreEstadoTramite';

      qbSolicitudesOtorgamientos.andWhere('(solicitudes.idSolicitud IN (:..._idsOtorgamientos))', {
        _idsOtorgamientos: idsOtorgamientos,
      });

      qbSolicitudesOtorgamientos.andWhere('(tramites.idEstadoTramite = 3)');

      let resultadoDenegaciones: Solicitudes[] = [];
      let resultadoOtorgamientos: Solicitudes[] = [];

      if (idsDenegaciones && idsDenegaciones.length > 0) {
        resultadoDenegaciones = await qbSolicitudesDenegaciones.getMany();
      }

      if (idsOtorgamientos && idsOtorgamientos.length > 0) {
        resultadoOtorgamientos = await qbSolicitudesOtorgamientos.getMany();
      }

      // Inicializamos el listado
      let listadoSolicitudesDTO: DenegacionesOtorgamientosPendientesListadoDTO[] = [];

      // // Transformacion al DTO
      dengacionesXotorgamientosQuery.forEach(dxo => {
        let plDTO: DenegacionesOtorgamientosPendientesListadoDTO = new DenegacionesOtorgamientosPendientesListadoDTO();

        // Variable para guardar el listado de tramites
        let tramitesPorRecorrer: TramitesEntity[] = [];

        // Variable para guardar el listado de examinaciones
        let examinacionesPorRecorrer: ColaExaminacionTramiteNMEntity[] = [];

        // Variable para asignar la clase de licencia (Abreviación)
        let claseAbreviacionTramite: string = '';

        //Si es nulo, entendemos que es una oportunidad, asignamos según el caso
        if (dxo.estadotramiteidentificador == 2) {
          plDTO.EsDenegacion = true;

          let denegacionRecuperada: Solicitudes = resultadoDenegaciones.find(x => x.idSolicitud == dxo.idsolicitud);

          plDTO.IdSolicitud = denegacionRecuperada.idSolicitud;
          plDTO.IdEstadoSolicitud = denegacionRecuperada.idEstado;
          plDTO.EstadoSolicitudNombre = denegacionRecuperada.estadosSolicitud.Nombre;
          plDTO.FechaCreacion = denegacionRecuperada.FechaCreacion;
          plDTO.Run = denegacionRecuperada.postulante.RUN;
          plDTO.RunFormateado = formatearRun(denegacionRecuperada.postulante.RUN + '-' + denegacionRecuperada.postulante.DV);
          plDTO.RunFormateadoSinPuntos = denegacionRecuperada.postulante.RUN + '-' + denegacionRecuperada.postulante.DV;
          plDTO.Nombres = denegacionRecuperada.postulante.Nombres;
          plDTO.ApellidosPaterno = denegacionRecuperada.postulante.ApellidoPaterno;
          plDTO.ApellidosMaterno = denegacionRecuperada.postulante.ApellidoMaterno;

          plDTO.NombreCompletoPostulante =
            denegacionRecuperada.postulante.Nombres +
            ' ' +
            denegacionRecuperada.postulante.ApellidoPaterno +
            ' ' +
            denegacionRecuperada.postulante.ApellidoMaterno;

          plDTO.IdComunaAsociada = denegacionRecuperada.oficinas.idComuna;

          tramitesPorRecorrer = denegacionRecuperada.tramites;
        } else {
          plDTO.EsDenegacion = false;

          let otorgamientoRecuperado: Solicitudes = resultadoOtorgamientos.find(x => x.idSolicitud == dxo.idsolicitud);

          plDTO.IdSolicitud = otorgamientoRecuperado.idSolicitud;
          plDTO.IdEstadoSolicitud = otorgamientoRecuperado.idEstado;
          plDTO.EstadoSolicitudNombre = otorgamientoRecuperado.estadosSolicitud.Nombre;
          plDTO.FechaCreacion = otorgamientoRecuperado.FechaCreacion;
          plDTO.Run = otorgamientoRecuperado.postulante.RUN;
          plDTO.RunFormateado = formatearRun(otorgamientoRecuperado.postulante.RUN + '-' + otorgamientoRecuperado.postulante.DV);
          plDTO.RunFormateadoSinPuntos = otorgamientoRecuperado.postulante.RUN + '-' + otorgamientoRecuperado.postulante.DV;
          plDTO.Nombres = otorgamientoRecuperado.postulante.Nombres;
          plDTO.ApellidosPaterno = otorgamientoRecuperado.postulante.ApellidoPaterno;
          plDTO.ApellidosMaterno = otorgamientoRecuperado.postulante.ApellidoMaterno;

          plDTO.NombreCompletoPostulante =
            otorgamientoRecuperado.postulante.Nombres +
            ' ' +
            otorgamientoRecuperado.postulante.ApellidoPaterno +
            ' ' +
            otorgamientoRecuperado.postulante.ApellidoMaterno;

          plDTO.IdComunaAsociada = otorgamientoRecuperado.oficinas.idComuna;

          tramitesPorRecorrer = otorgamientoRecuperado.tramites;
        }

        // Inicializamos el listado de trámites
        let listadoDengOtorgTramitesDTO: DenegacionesOtorgamientosTramitesDTO[] = [];

        tramitesPorRecorrer.forEach(tram => {
          // Inicializamos el DTO
          let denOtorgTramiteDTO: DenegacionesOtorgamientosTramitesDTO = new DenegacionesOtorgamientosTramitesDTO();

          denOtorgTramiteDTO.IdTramite = tram.idTramite;
          denOtorgTramiteDTO.IdEstadoTramite = tram.idEstadoTramite;
          denOtorgTramiteDTO.EstadoTramiteString = tram.estadoTramite.Nombre;
          denOtorgTramiteDTO.IdTipoTramite = tram.tiposTramite.idTipoTramite;
          denOtorgTramiteDTO.TipoTramiteString = tram.tiposTramite.Nombre;
          denOtorgTramiteDTO.IdClaseLicencia = tram.tramitesClaseLicencia.idClaseLicencia;
          denOtorgTramiteDTO.AbreviacionClaseLicenciaAsocString = tram.tramitesClaseLicencia.clasesLicencias.Abreviacion;
          //denOtorgTramiteDTO.Examinaciones = ;

          if (tram.colaExaminacionNM && tram.colaExaminacionNM.length > 0) {
            tram.colaExaminacionNM.forEach(examinacion => {
              if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenTeorico)
                denOtorgTramiteDTO.examenTeorico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenPractico)
                denOtorgTramiteDTO.examenPractico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenMedico)
                denOtorgTramiteDTO.examenMedico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminaciones.nombreExaminacion == TipoExaminaciones.ExamenIM)
                denOtorgTramiteDTO.idoneidadMoral =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
            });
          } else {
            denOtorgTramiteDTO.examenTeorico = 0;
            denOtorgTramiteDTO.examenPractico = 0;
            denOtorgTramiteDTO.examenMedico = 0;
            denOtorgTramiteDTO.idoneidadMoral = 0;
          }

          listadoDengOtorgTramitesDTO.push(denOtorgTramiteDTO);
        });

        plDTO.Tramites = listadoDengOtorgTramitesDTO;
        listadoSolicitudesDTO.push(plDTO);
      });

      // Fin consulta nueva

      // if (nombreApellido) {
      //   qbSolicitudesDenegaciones.andWhere(
      //     '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
      //       ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
      //       ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
      //     {
      //       nombre: `%${nombreApellido}%`,
      //     }
      //   );
      // }

      if (Array.isArray(listadoSolicitudesDTO) && listadoSolicitudesDTO.length) {
        //Generamos la repuesta
        let pRes: paginationResult = new paginationResult();
        pRes.items = listadoSolicitudesDTO;

        let metaPagination: paginationMeta = new paginationMeta();
        metaPagination.itemCount = listadoSolicitudesDTO.length;
        metaPagination.totalItems = +contadorTotalElementos;
        metaPagination.itemsPerPage = paginacionArgs.paginationOptions.limit as number;
        metaPagination.totalPages = Math.ceil(metaPagination.itemCount / metaPagination.itemsPerPage);
        metaPagination.currentPage = paginacionArgs.paginationOptions.page as number;

        pRes.meta = metaPagination;

        resultado.Respuesta = pRes;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Solicitudes obtenidas correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }

  async getTramitesEnListaDeEspera(
    req: any,
    paginacionArgs: PaginacionArgs,
    run?,
    nombreApellido?,
    idSolicitud?,
    idTipoTramite?,
    idClaseLicencia?,
    idEstadoTramite?,
    fechaSolicitud?
  ): Promise<Resultado> {
    let res: Resultado = new Resultado();
    let orden = paginacionArgs.orden.orden;
    let ordenarPor = paginacionArgs.orden.ordenarPor;
    let solicitudesDTO: SolicitudDTO[] = [];

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaTramitesEnListaDeEspera
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      // 1. Hacemos la consulta
      const qbSolicitudes = this.solicitudesEntityRespository
        .createQueryBuilder('Solicitudes')
        .innerJoinAndMapOne('Solicitudes.postulante', PostulanteEntity, 'postulante', 'Solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapMany('Solicitudes.tramites', TramitesEntity, 'tramites', 'Solicitudes.idSolicitud = tramites.idSolicitud')
        .innerJoinAndMapOne(
          'Solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'Solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .leftJoinAndMapMany(
          'tramites.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'tramites.idTramite = colaExaminacion.idTramite'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        )
        //Inner Joins para filtros
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdTramite',
          TramitesEntity,
          'tramitesFiltroIdTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdTramite.idSolicitud'
        )
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdTipoTramite',
          TramitesEntity,
          'tramitesFiltroIdTipoTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdTipoTramite.idSolicitud'
        )
        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdEstadoTramite',
          TramitesEntity,
          'tramitesFiltroIdEstadoTramite',
          'Solicitudes.idSolicitud = tramitesFiltroIdEstadoTramite.idSolicitud'
        )

        .innerJoinAndMapMany(
          'Solicitudes.tramitesFiltroIdClaseLicencia',
          TramitesEntity,
          'tramitesFiltroIdClaseLicencia',
          'Solicitudes.idSolicitud = tramitesFiltroIdClaseLicencia.idSolicitud'
        )
        .innerJoinAndMapOne(
          'tramitesFiltroIdClaseLicencia.tramitesClaseLicenciaFiltroIdClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicenciaFiltroIdClaseLicencia',
          'tramitesFiltroIdClaseLicencia.idTramite = tramitesClaseLicenciaFiltroIdClaseLicencia.idTramite'
        );

      // Estados de trámites admitidos :
      //
      // Acusar Recibo Conforme                           5
      // A la espera de informar Otorgamiento             3
      // A la espera de informar Denegacion               2
      // Otorgamiento informado y en impresion            9

      //const estadoTramitesPermitidos: number[] = [2, 3, 5, 9];

      qbSolicitudes.where(
        new Brackets(subQ =>
          subQ
            .orWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = 5')
            .orWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = 9')
            .orWhere(
              new Brackets(subQCondState =>
                subQCondState
                  .andWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = 3')
                  .andWhere(
                    '(' +
                      '((SELECT 	count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 5 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                      ' OR ' +
                      '((SELECT count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 5) < 1)' +
                      ')'
                  )
                  .andWhere(
                    '(' +
                      '((SELECT 	count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 4 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                      ' OR ' +
                      '((SELECT count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 4) < 1)' +
                      ')'
                  )
                  .andWhere(
                    '(' +
                      '((SELECT 	count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 3 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                      ' OR ' +
                      '((SELECT count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 3) < 1)' +
                      ')'
                  )
                  .andWhere(
                    '(' +
                      '((SELECT 	count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CEAux"."aprobado" = true AND "CEAux"."idTipoExaminacion" = 2 AND "CETNMAux"."idTramite" = tramites.idTramite) > 0)' +
                      ' OR ' +
                      '((SELECT count(*)' +
                      'FROM 	public."ColaExaminacionTramiteNM" AS "CETNMAux"' +
                      'INNER JOIN public."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion"' +
                      'WHERE	"CETNMAux"."idTramite" = tramites.idTramite AND"CEAux"."idTipoExaminacion" = 2) < 1)' +
                      ')'
                  )
              )
            )
            .orWhere(
              new Brackets(subQCondState =>
                subQCondState
                  .andWhere('tramitesFiltroIdEstadoTramite.idEstadoTramite = 2')

                  .andWhere(
                    new Brackets(subQCondStateExam =>
                      subQCondStateExam

                        .orWhere(
                          '(' +
                            '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                            'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                            'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 5) > 0)' +
                            ')'
                        )
                        .orWhere(
                          '(' +
                            '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                            'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                            'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 4) > 0)' +
                            ')'
                        )
                        .orWhere(
                          '(' +
                            '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                            'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                            'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 3) > 0)' +
                            ')'
                        )
                        .orWhere(
                          '(' +
                            '((SELECT count(*) FROM public."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
                            'INNER JOIN public."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
                            'WHERE "CETNMAuxDen"."idTramite" = tramites.idTramite AND "CEAuxDen"."aprobado" = false AND "CEAuxDen"."idTipoExaminacion" = 2) > 0)' +
                            ')'
                        )
                    )
                  )
              )
            )
        )
      );

      // 3.b Añadimos condición para filtrar por oficina si se da el caso
      // Filtramos por las oficinas en caso de que el usuario no sea administrador
      //Comprobamos si es superUsuario por si es necesario filtrar
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null) {
          res.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          res.ResultadoOperacion = false;
          res.Respuesta = [];

          return res;
        } else {
          qbSolicitudes.andWhere('Solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
        }
      }

      // 4. Añadimos los filtros de front
      if (run) {
        const rut = run.split('-');

        const div = rut[1];
        let runsplited: string = rut[0];

        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');

        qbSolicitudes.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbSolicitudes.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }

      if (idTipoTramite) {
        qbSolicitudes.andWhere('tiposTramite.idTipoTramite = :id', { id: idTipoTramite });
      }

      if (nombreApellido) {
        qbSolicitudes.andWhere(
          '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${nombreApellido}%`,
          }
        );
      }

      if (idSolicitud) {
        qbSolicitudes.andWhere('Solicitudes.idSolicitud = :idSolicitud', {
          idSolicitud: idSolicitud,
        });
      }

      if (idClaseLicencia) {
        qbSolicitudes.andWhere('clasesLicencias.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: idClaseLicencia,
        });
      }

      if (idEstadoTramite) {
        qbSolicitudes.andWhere('tramites.idEstadoTramite = :_idEstadoTramite', {
          _idEstadoTramite: idEstadoTramite,
        });
      }

      if (fechaSolicitud)
        qbSolicitudes.andWhere('"Solicitudes"."FechaCreacion"::date = :created_at::date', {
          //"FechaCreacion" > '2022-07-20'
          created_at: fechaSolicitud,
        });

      if (orden === 'Id' && ordenarPor === 'ASC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'ASC');
      if (orden === 'Id' && ordenarPor === 'DESC') qbSolicitudes.orderBy('Solicitudes.idSolicitud', 'DESC');
      if (orden === 'RUN' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.RUN', 'ASC');
      if (orden === 'RUN' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.RUN', 'DESC');
      if (orden === 'NombrePostulante' && ordenarPor === 'ASC') qbSolicitudes.orderBy('postulante.Nombres', 'ASC');
      if (orden === 'NombrePostulante' && ordenarPor === 'DESC') qbSolicitudes.orderBy('postulante.Nombres', 'DESC');
      if (orden === 'claseLicencia' && ordenarPor === 'ASC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'ASC');
      if (orden === 'claseLicencia' && ordenarPor === 'DESC') qbSolicitudes.orderBy('clasesLicencias.Abreviacion', 'DESC');

      // 2. Recogemos solo tramites con estados de tramites en proceso
      //qbSolicitudes.andWhere('estadoTramite.idEstadoTramite in (:estadoTramitesPermitidos)', { estados: estadoTramitesPermitidos})

      // 3 filtramos por oficina en caso de que no sea superuser
      //qb...... TODO

      const resultadoSolicitudes: any = await paginate<Solicitudes>(qbSolicitudes, paginacionArgs.paginationOptions);

      // 5 Mapeamos la respuesta
      resultadoSolicitudes.items.forEach(solicitud => {
        let solicitudDto: SolicitudDTO = new SolicitudDTO();

        solicitudDto.fechaSolicitud = solicitud.FechaCreacion;
        solicitudDto.idSolicitud = solicitud.idSolicitud;
        solicitudDto.run = solicitud.postulante.RUN + '-' + solicitud.postulante.DV;
        solicitudDto.nombrePostulante =
          solicitud.postulante.Nombres + ' ' + solicitud.postulante.ApellidoPaterno + ' ' + solicitud.postulante.ApellidoMaterno;

        solicitud.tramites.forEach(tramite => {
          let tramiteDto = new TramiteDTO();

          tramiteDto.claseLicencia = tramite.tramitesClaseLicencia.clasesLicencias.Abreviacion;
          tramiteDto.estadoTramite = tramite.estadoTramite.Nombre;
          tramiteDto.tipoTramite = tramite.tiposTramite.Nombre;

          solicitudDto.tramites.push(tramiteDto);
        });

        solicitudesDTO.push(solicitudDto);
      });

      resultadoSolicitudes.items = solicitudesDTO;

      res.ResultadoOperacion = true;
      res.Respuesta = resultadoSolicitudes;
    } catch (err) {
      res.Error = 'Se ha producido un error, por favor, contacte con el administrador.';
      res.ResultadoOperacion = false;
    }

    return res;
  }

  private getQueryUnionPendientesDenegacionXPendientesOtorgamiento(
    obtenerContador: Boolean,
    columnaOrdenacion?: string,
    modoOrdenacion?: string,
    filtroIdsOficinas?: string
  ): string {
    const obtenerConsultaContador: string = obtenerContador
      ? 'select count(distinct "solicitudes_idSolicitud") from ( '
      : 'select "solicitudes_idSolicitud" AS idsolicitud, ' +
        '"estadosSolicitud_Nombre" AS estadosolicitudnombre, ' +
        '"solicitudes_FechaCreacion" AS fechacreacion, ' +
        '"estadoTramite_Identificador" AS estadotramiteidentificador, ' +
        '"postulante_RUN" AS run, ' +
        '"postulante_Nombres" AS nombres, ' +
        '"postulante_ApellidoPaterno" AS apellidospaterno, ' +
        '"postulante_ApellidoMaterno" AS apellidosmaterno from ( ';

    const consultaPaginacionContador: string = obtenerContador ? '' : ' limit $8 offset $9 ';

    const consultaOrdenacion: string =
      columnaOrdenacion && modoOrdenacion && columnaOrdenacion != null && modoOrdenacion != null
        ? ' GROUP BY 1,2,3,4,5,6,7 ORDER BY ' + columnaOrdenacion + ' ' + modoOrdenacion + ' '
        : '';

    return (
      '' +
      obtenerConsultaContador +
      '	SELECT DISTINCT "solicitudes"."idSolicitud" AS "solicitudes_idSolicitud", ' +
      '	"solicitudes"."FechaCreacion" AS "solicitudes_FechaCreacion", ' +
      '	"postulante"."RUN" AS "postulante_RUN", ' +
      '	"postulante"."Nombres" AS "postulante_Nombres", ' +
      '	"postulante"."ApellidoPaterno" AS "postulante_ApellidoPaterno", ' +
      '	"postulante"."ApellidoMaterno" AS "postulante_ApellidoMaterno", ' +
      '	"estadosSolicitud"."Nombre" AS "estadosSolicitud_Nombre", ' +
      '	2 AS "estadoTramite_Identificador" ' +
      '	FROM "Solicitudes" "solicitudes" ' +
      '	INNER JOIN "Tramites" "tramites" ON "solicitudes"."idSolicitud" = "tramites"."idSolicitud" ' +
      '	INNER JOIN "Postulantes" "postulante" ON "solicitudes"."idPostulante" = "postulante"."idPostulante" ' +
      '	INNER JOIN "EstadosSolicitud" "estadosSolicitud" ON "solicitudes"."idEstado" = "estadosSolicitud"."idEstado" ' +
      '	INNER JOIN "TiposTramite" "tiposTramite" ON "tramites"."idTipoTramite" = "tiposTramite"."idTipoTramite" ' +
      '	INNER JOIN "ColaExaminacionTramiteNM" "colaExaminacionNM" ON "tramites"."idTramite" = "colaExaminacionNM"."idTramite" ' +
      '	INNER JOIN "ColaExaminacion" "colaExaminacion" ON "colaExaminacionNM"."idColaExaminacion" = "colaExaminacion"."idColaExaminacion" ' +
      '	INNER JOIN "TipoExaminaciones" "tipoExaminacion" ON "colaExaminacion"."idTipoExaminacion" = "tipoExaminacion"."idTipoExaminacion" ' +
      '	INNER JOIN "EstadosTramite" "estadoTramite" ON "tramites"."idEstadoTramite" = "estadoTramite"."idEstadoTramite" ' +
      '	INNER JOIN "TramitesClaseLicencia" "tramitesClaseLicencia" ON "tramites"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '	INNER JOIN "ClasesLicencias" "clasesLicencias" ON "clasesLicencias"."idClaseLicencia" = "tramitesClaseLicencia"."idClaseLicencia" ' +
      '	WHERE ("tramites"."idEstadoTramite" = 2 ' +
      ' AND ("solicitudes"."idSolicitud" = $1 OR $1 IS NULL) ' +
      ' AND (("solicitudes"."FechaCreacion"::date = $2) OR ($2 IS NULL)) ' +
      ' AND ("tiposTramite"."idTipoTramite" = $3 OR $3 IS NULL) ' +
      ' AND ("postulante"."RUN" = $4 OR $4 IS NULL) ' +
      ' AND ("clasesLicencias"."idClaseLicencia" = $5 OR $5 IS NULL) ' +
      ' AND ("tramites"."idEstadoTramite" = $6 OR $6 IS NULL) ' +
      ' AND ((lower(unaccent("postulante"."Nombres")) like lower(unaccent($7))) OR (lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent($7))) OR (lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent($7))) OR $7 IS NULL) ' +
      (filtroIdsOficinas ? ' AND ("solicitudes"."idOficina" IN ' + filtroIdsOficinas + ') ' : '') +
      '								AND ((( ' +
      '													 ' +
      '																			(SELECT COUNT(*) ' +
      '																				FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
      '																				INNER JOIN PUBLIC."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
      '																				WHERE "CETNMAuxDen"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																					AND "CEAuxDen"."aprobado" = FALSE ' +
      '																					AND "CEAuxDen"."idTipoExaminacion" = 5) > 0) ' +
      '													OR (( ' +
      '																			(SELECT COUNT(*) ' +
      '																				FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
      '																				INNER JOIN PUBLIC."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
      '																				WHERE "CETNMAuxDen"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																					AND "CEAuxDen"."aprobado" = FALSE ' +
      '																					AND "CEAuxDen"."idTipoExaminacion" = 4) > 0)) ' +
      '													OR (( ' +
      '																			(SELECT COUNT(*) ' +
      '																				FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
      '																				INNER JOIN PUBLIC."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
      '																				WHERE "CETNMAuxDen"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																					AND "CEAuxDen"."aprobado" = FALSE ' +
      '																					AND "CEAuxDen"."idTipoExaminacion" = 3) > 0)) ' +
      '													OR (( ' +
      '																			(SELECT COUNT(*) ' +
      '																				FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAuxDen" ' +
      '																				INNER JOIN PUBLIC."ColaExaminacion" AS "CEAuxDen" ON "CETNMAuxDen"."idColaExaminacion" = "CEAuxDen"."idColaExaminacion" ' +
      '																				WHERE "CETNMAuxDen"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																					AND "CEAuxDen"."aprobado" = FALSE ' +
      '																					AND "CEAuxDen"."idTipoExaminacion" = 2) > 0))))) ' +
      '  union all ' +
      '	SELECT DISTINCT "solicitudes"."idSolicitud" AS "solicitudes_idSolicitud", ' +
      '	"solicitudes"."FechaCreacion" AS "solicitudes_FechaCreacion", ' +
      '	"postulante"."RUN" AS "postulante_RUN", ' +
      '	"postulante"."Nombres" AS "postulante_Nombres", ' +
      '	"postulante"."ApellidoPaterno" AS "postulante_ApellidoPaterno", ' +
      '	"postulante"."ApellidoMaterno" AS "postulante_ApellidoMaterno", ' +
      '	"estadosSolicitud"."Nombre" AS "estadosSolicitud_Nombre", ' +
      '	3 AS "estadoTramite_Identificador" ' +
      '	FROM "Solicitudes" "solicitudes" ' +
      '	INNER JOIN "Tramites" "tramites" ON "solicitudes"."idSolicitud" = "tramites"."idSolicitud" ' +
      '	INNER JOIN "Postulantes" "postulante" ON "solicitudes"."idPostulante" = "postulante"."idPostulante" ' +
      '	INNER JOIN "EstadosSolicitud" "estadosSolicitud" ON "solicitudes"."idEstado" = "estadosSolicitud"."idEstado" ' +
      '	INNER JOIN "TiposTramite" "tiposTramite" ON "tramites"."idTipoTramite" = "tiposTramite"."idTipoTramite" ' +
      '	LEFT JOIN "ColaExaminacionTramiteNM" "colaExaminacionNM" ON "tramites"."idTramite" = "colaExaminacionNM"."idTramite" ' +
      '	LEFT JOIN "ColaExaminacion" "colaExaminacion" ON "colaExaminacionNM"."idColaExaminacion" = "colaExaminacion"."idColaExaminacion" ' +
      '	LEFT JOIN "TipoExaminaciones" "tipoExaminacion" ON "colaExaminacion"."idTipoExaminacion" = "tipoExaminacion"."idTipoExaminacion" ' +
      '	INNER JOIN "EstadosTramite" "estadoTramite" ON "tramites"."idEstadoTramite" = "estadoTramite"."idEstadoTramite" ' +
      '	INNER JOIN "TramitesClaseLicencia" "tramitesClaseLicencia" ON "tramites"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '	INNER JOIN "ClasesLicencias" "clasesLicencias" ON "clasesLicencias"."idClaseLicencia" = "tramitesClaseLicencia"."idClaseLicencia" ' +
      '	WHERE ("tramites"."idEstadoTramite" = 3 ' +
      ' AND ("solicitudes"."idSolicitud" = $1 OR $1 IS NULL) ' +
      ' AND (("solicitudes"."FechaCreacion"::date = $2) OR ($2 IS NULL)) ' +
      ' AND ("tiposTramite"."idTipoTramite" = $3 OR $3 IS NULL) ' +
      ' AND ("postulante"."RUN" = $4 OR $4 IS NULL) ' +
      ' AND ("clasesLicencias"."idClaseLicencia" = $5 OR $5 IS NULL) ' +
      ' AND ("tramites"."idEstadoTramite" = $6 OR $6 IS NULL) ' +
      ' AND ((lower(unaccent("postulante"."Nombres")) like lower(unaccent($7))) OR (lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent($7))) OR (lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent($7))) OR $7 IS NULL) ' +
      (filtroIdsOficinas ? ' AND ("solicitudes"."idOficina" IN ' + filtroIdsOficinas + ') ' : '') +
      '								AND (((( ' +
      '																	(SELECT COUNT(*) ' +
      '																		FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																		INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																		WHERE "CEAux"."aprobado" = TRUE ' +
      '																			AND "CEAux"."idTipoExaminacion" = 5 ' +
      '																			AND "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite") > 0) ' +
      '															OR ( ' +
      '																				(SELECT COUNT(*) ' +
      '																					FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																					INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																					WHERE "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																						AND"CEAux"."idTipoExaminacion" = 5) < 1)) ' +
      '														AND (( ' +
      '																					(SELECT COUNT(*) ' +
      '																						FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																						INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																						WHERE "CEAux"."aprobado" = TRUE ' +
      '																							AND "CEAux"."idTipoExaminacion" = 4 ' +
      '																							AND "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite") > 0) ' +
      '																			OR ( ' +
      '																								(SELECT COUNT(*) ' +
      '																									FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																									INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																									WHERE "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																										AND"CEAux"."idTipoExaminacion" = 4) < 1)) ' +
      '														AND (( ' +
      '																					(SELECT COUNT(*) ' +
      '																						FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																						INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																						WHERE "CEAux"."aprobado" = TRUE ' +
      '																							AND "CEAux"."idTipoExaminacion" = 3 ' +
      '																							AND "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite") > 0) ' +
      '																			OR ( ' +
      '																								(SELECT COUNT(*) ' +
      '																									FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																									INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																									WHERE "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																										AND"CEAux"."idTipoExaminacion" = 3) < 1)) ' +
      '														AND (( ' +
      '																					(SELECT COUNT(*) ' +
      '																						FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																						INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																						WHERE "CEAux"."aprobado" = TRUE ' +
      '																							AND "CEAux"."idTipoExaminacion" = 2 ' +
      '																							AND "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite") > 0) ' +
      '																			OR ( ' +
      '																								(SELECT COUNT(*) ' +
      '																									FROM PUBLIC."ColaExaminacionTramiteNM" AS "CETNMAux" ' +
      '																									INNER JOIN PUBLIC."ColaExaminacion" AS "CEAux" ON "CETNMAux"."idColaExaminacion" = "CEAux"."idColaExaminacion" ' +
      '																									WHERE "CETNMAux"."idTramite" = "tramitesClaseLicencia"."idTramite" ' +
      '																										AND"CEAux"."idTipoExaminacion" = 2) < 1))) ' +
      ')) ' +
      consultaOrdenacion +
      ') AS PendientesDenegacionXPendientesOtorgamiento ' +
      consultaPaginacionContador
    );
  }

  async getTramitesEnRecepcionConforme(
    req: any,
    paginacionArgs: PaginacionArgs,
    run?,
    nombreApellido?,
    idSolicitud?,
    idTipoTramite?,
    idClaseLicencia?
  ) {
    let resultado: Resultado = new Resultado();
    let tramitesParseados = new PaginacionDtoParser<TramitesEnProcesoDto>();

    // Estados de trámites admitidos :
    //                                    16 - "Pendiente Registro Civil"
    //                                    1  - "Trámite Iniciado"
    //                                    2  - "A la Espera de Informar Denegación"
    //                                    3  - "A la Espera de Informar Otorgamiento"
    //                                    19  - "Trámite re iniciado por reconsideración apelación JPL o SML"
    //                                    9  - "Otorgamiento informado y en impresión"
    //                                    5 -   "Acusar recibo conforme"

    const estadoTramitesPermitidos: number[] = [5];

    try {
      // Comprobamos permisos
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(
        req,
        PermisosNombres.VisualizarListaRecepcionConforme
      );

      // En caso de que el usuario no sea correcto
      if (!usuarioValidado.ResultadoOperacion) {
        return usuarioValidado;
      }

      // 1. Hacemos la consulta
      const qbTramites = this.tramitesRespository
        .createQueryBuilder('tramites')
        .innerJoinAndMapOne('tramites.solicitudes', Solicitudes, 'solicitudes', 'tramites.idSolicitud = solicitudes.idSolicitud')
        .innerJoinAndMapOne('solicitudes.postulante', PostulanteEntity, 'postulante', 'solicitudes.idPostulante = postulante.idPostulante')
        .innerJoinAndMapOne(
          'solicitudes.estadosSolicitud',
          EstadosSolicitudEntity,
          'estadosSolicitud',
          'solicitudes.idEstado = estadosSolicitud.idEstado'
        )
        .innerJoinAndMapOne(
          'tramites.tiposTramite',
          TiposTramiteEntity,
          'tiposTramite',
          'tramites.idTipoTramite = tiposTramite.idTipoTramite'
        )
        .leftJoinAndMapMany(
          'tramites.colaExaminacionNM',
          ColaExaminacionTramiteNMEntity,
          'colaExaminacionNM',
          'tramites.idTramite = colaExaminacionNM.idTramite'
        )
        .leftJoinAndMapOne(
          'colaExaminacionNM.colaExaminacion',
          ColaExaminacionEntity,
          'colaExaminacion',
          'colaExaminacionNM.idColaExaminacion = colaExaminacion.idColaExaminacion'
        )
        .leftJoinAndMapOne(
          'colaExaminacion.tipoExaminacion',
          TipoExaminacionesEntity,
          'tipoExaminacion',
          'colaExaminacion.idTipoExaminacion = tipoExaminacion.idTipoExaminacion'
        )
        .innerJoinAndMapOne(
          'tramites.estadoTramite',
          EstadosTramiteEntity,
          'estadoTramite',
          'tramites.idEstadoTramite = estadoTramite.idEstadoTramite'
        )
        .innerJoinAndMapOne(
          'tramites.tramitesClaseLicencia',
          TramitesClaseLicencia,
          'tramitesClaseLicencia',
          'tramites.idTramite = tramitesClaseLicencia.idTramite'
        )
        .innerJoinAndMapOne(
          'tramitesClaseLicencia.clasesLicencias',
          ClasesLicenciasEntity,
          'clasesLicencias',
          'clasesLicencias.idClaseLicencia = tramitesClaseLicencia.idClaseLicencia'
        );

      // 2. Recogemos solo tramites con estados de tramites en proceso
      qbTramites.andWhere('tramites.idEstadoTramite in (:..._estadoTramitesPermitidos)', {
        _estadoTramitesPermitidos: estadoTramitesPermitidos,
      });

      // 3 filtramos por oficina en caso de que no sea superuser
      // 3.b Añadimos condición para filtrar por oficina si se da el caso
      // Filtramos por las oficinas en caso de que el usuario no sea administrador
      //Comprobamos si es superUsuario por si es necesario filtrar
      if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
        // Recogemos las oficinas asociadas al usuario
        let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

        if (idOficinasAsociadas == null) {
          resultado.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
          resultado.ResultadoOperacion = false;
          resultado.Respuesta = [];

          return resultado;
        } else {
          qbTramites.andWhere('solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
        }
      }

      // 4. Añadimos los filtros de front
      if (run) {
        const rut = run.split('-');

        const div = rut[1];
        let runsplited: string = rut[0];

        runsplited = runsplited.replace('.', '');
        runsplited = runsplited.replace('.', '');

        qbTramites.andWhere('postulante.RUN = :run', {
          run: runsplited,
        });

        qbTramites.andWhere('postulante.DV = :dv', {
          dv: div,
        });
      }

      if (idTipoTramite) {
        qbTramites.andWhere('tiposTramite.idTipoTramite = :id', { id: idTipoTramite });
      }

      if (nombreApellido) {
        qbTramites.andWhere(
          '(lower(unaccent("postulante"."Nombres")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoPaterno")) like lower(unaccent(:nombre))' +
            ' or lower(unaccent("postulante"."ApellidoMaterno")) like lower(unaccent(:nombre)))',
          {
            nombre: `%${nombreApellido}%`,
          }
        );
      }

      if (idSolicitud) {
        qbTramites.andWhere('solicitudes.idSolicitud = :idSolicitud', {
          idSolicitud: idSolicitud,
        });
      }

      if (idClaseLicencia) {
        qbTramites.andWhere('clasesLicencias.idClaseLicencia = :idClaseLicencia', {
          idClaseLicencia: idClaseLicencia,
        });
      }

      if (paginacionArgs.orden.orden === 'Id' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('solicitudes.idSolicitud', 'ASC');
      if (paginacionArgs.orden.orden === 'Id' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('solicitudes.idSolicitud', 'DESC');
      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('tramites.idTramite', 'ASC');
      if (paginacionArgs.orden.orden === 'IdTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('tramites.idTramite', 'DESC');
      if (paginacionArgs.orden.orden === 'RUN' && paginacionArgs.orden.ordenarPor === 'ASC') qbTramites.orderBy('postulante.RUN', 'ASC');
      if (paginacionArgs.orden.orden === 'RUN' && paginacionArgs.orden.ordenarPor === 'DESC') qbTramites.orderBy('postulante.RUN', 'DESC');
      if (paginacionArgs.orden.orden === 'NombrePostulante' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('postulante.Nombres', 'ASC');
      if (paginacionArgs.orden.orden === 'NombrePostulante' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('postulante.Nombres', 'DESC');
      if (paginacionArgs.orden.orden === 'TipoTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('tiposTramite.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'TipoTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('tiposTramite.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'EstadoSolicitud' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('estadosSolicitud.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'EstadoSolicitud' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('estadosSolicitud.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'EstadoTramite' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('estadoTramite.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'EstadoTramite' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('estadoTramite.Nombre', 'DESC');
      if (paginacionArgs.orden.orden === 'Clase' && paginacionArgs.orden.ordenarPor === 'ASC')
        qbTramites.orderBy('clasesLicencias.Nombre', 'ASC');
      if (paginacionArgs.orden.orden === 'Clase' && paginacionArgs.orden.ordenarPor === 'DESC')
        qbTramites.orderBy('clasesLicencias.Nombre', 'DESC');

      let tramitesDTO: TramitesEnProcesoDto[] = [];
      let postulante: PostulantesEntity;

      //tramites = await qbTramites.getMany();
      let tramitesCLPaginados: any = await paginate<TramitesEntity>(qbTramites, paginacionArgs.paginationOptions);

      if (Array.isArray(tramitesCLPaginados.items) && tramitesCLPaginados.items.length) {
        tramitesCLPaginados.items.forEach(tramiteCL => {
          const tramiteDTO: TramitesEnProcesoDto = new TramitesEnProcesoDto();
          let datosLicenciaDto = new DatosLicenciaDto();

          tramiteDTO.idSolicitud = tramiteCL.idSolicitud;
          tramiteDTO.idTramite = tramiteCL.idTramite;
          tramiteDTO.run = tramiteCL.solicitudes.postulante.RUN.toString() + '-' + tramiteCL.solicitudes.postulante.DV;
          tramiteDTO.nombrePostulante =
            tramiteCL.solicitudes.postulante.Nombres +
            ' ' +
            tramiteCL.solicitudes.postulante.ApellidoPaterno +
            ' ' +
            tramiteCL.solicitudes.postulante.ApellidoMaterno;
          tramiteDTO.tipoTramite = tramiteCL.tiposTramite.Nombre;
          tramiteDTO.idTipoTramite = tramiteCL.tiposTramite.idTipoTramite;
          tramiteDTO.idComuna = tramiteCL.solicitudes.postulante.idComuna;
          tramiteDTO.estadoTramite = tramiteCL.estadoTramite.Nombre;
          tramiteDTO.estado = tramiteCL.estadoTramite.Nombre;
          tramiteDTO.estadoSolicitud = tramiteCL.solicitudes.estadosSolicitud.Nombre;
          tramiteDTO.municipalidadSolicitudString = tramiteCL.solicitudes.oficinas
            ? tramiteCL.solicitudes.oficinas.instituciones.Nombre
            : '';

          if (tramiteCL.tramitesClaseLicencia.clasesLicencias) {
            tramiteDTO.claseLicencia = tramiteCL.tramitesClaseLicencia.clasesLicencias.Abreviacion;
            tramiteDTO.idClaseLicencia = tramiteCL.tramitesClaseLicencia.clasesLicencias.idClaseLicencia;
          } else tramiteDTO.claseLicencia = '';

          tramiteDTO.examenTeorico = 0;
          tramiteDTO.examenPractico = 0;
          tramiteDTO.examenMedico = 0;
          tramiteDTO.idoneidadMoral = 0;
          if (tramiteCL.colaExaminacionNM && tramiteCL.colaExaminacionNM.length > 0) {
            tramiteCL.colaExaminacionNM.forEach(examinacion => {
              if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenTeorico)
                tramiteDTO.examenTeorico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenPractico)
                tramiteDTO.examenPractico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenMedico)
                tramiteDTO.examenMedico =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
              else if (examinacion.colaExaminacion.tipoExaminacion.nombreExaminacion == TipoExaminaciones.ExamenIM)
                tramiteDTO.idoneidadMoral =
                  examinacion.colaExaminacion.aprobado == null ? 0 : examinacion.colaExaminacion.aprobado == true ? 1 : -1;
            });
          }

          tramitesDTO.push(tramiteDTO);
        });

        tramitesParseados.items = tramitesDTO;
        tramitesParseados.meta = tramitesCLPaginados.meta;
        tramitesParseados.links = tramitesCLPaginados.links;

        resultado.Respuesta = tramitesParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Tramites obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron tramites';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo tramites';
    }

    return resultado;
  }
}

function construirLicenciaConConductorDto(
  respuestaTramite: TramitesEntity,
  docLicenciaExistente: DocumentosLicencia,
  datosClasesLicenciasFechas: DatosClasesLicenciasConFechas[]
): LicenciaConConductorDto {
  // Creamos el objeto para la licencia Mobile
  let licenciaConConductorDto = new LicenciaConConductorDto();
  let conductorMobileDTO = new ConductorMobileDTO();

  // Se rellenan datos del conductor
  licenciaConConductorDto.conductor = conductorMobileDTO;
  licenciaConConductorDto.conductor.RUN = respuestaTramite.solicitudes.postulante.RUN;
  licenciaConConductorDto.conductor.DV = respuestaTramite.solicitudes.postulante.DV;
  licenciaConConductorDto.conductor.Nombres = respuestaTramite.solicitudes.postulante.Nombres;
  licenciaConConductorDto.conductor.ApellidoPaterno = respuestaTramite.solicitudes.postulante.ApellidoPaterno;
  licenciaConConductorDto.conductor.ApellidoMaterno = respuestaTramite.solicitudes.postulante.ApellidoMaterno;
  licenciaConConductorDto.conductor.Telefono = respuestaTramite.solicitudes.postulante.Telefono;
  licenciaConConductorDto.conductor.Email = respuestaTramite.solicitudes.postulante.Email;
  licenciaConConductorDto.conductor.Calle = respuestaTramite.solicitudes.postulante.Calle;
  licenciaConConductorDto.conductor.CalleNro = respuestaTramite.solicitudes.postulante.CalleNro;
  licenciaConConductorDto.conductor.Letra = respuestaTramite.solicitudes.postulante.Letra;
  licenciaConConductorDto.conductor.RestroDireccion = respuestaTramite.solicitudes.postulante.RestoDireccion;
  licenciaConConductorDto.conductor.Sexo = respuestaTramite.solicitudes.postulante.opcionSexo
    ? respuestaTramite.solicitudes.postulante.opcionSexo.Nombre
    : 'N/A';

  licenciaConConductorDto.conductor.Comuna = respuestaTramite.solicitudes.postulante.comunas
    ? respuestaTramite.solicitudes.postulante.comunas.Nombre
    : 'N/A';
  licenciaConConductorDto.conductor.IdComunaSGL = respuestaTramite.solicitudes.postulante.comunas
    ? respuestaTramite.solicitudes.postulante.comunas.idComuna
    : null;

  // Se rellenan datos de conductor, su comuna, foto
  licenciaConConductorDto.conductor.idSGLConductor = docLicenciaExistente.conductor?.idConductor;

  licenciaConConductorDto.conductor.municipalidad = respuestaTramite.solicitudes.postulante.comunas
    ? respuestaTramite.solicitudes.postulante.comunas.instituciones.Nombre
    : 'N/A';

  licenciaConConductorDto.conductor.fotoUsuarioPath = 'FotoUsuario';
  licenciaConConductorDto.conductor.fotoUsuarioBinary = respuestaTramite.solicitudes.postulante.imagenesPostulante[0].archivo; // Se coge la primera?

  //Creamos el objeto del Documento Licencia para Mobile, licencia junto a sus clases
  const documentosLicenciasMobile: DocumentoLicenciaMobileDto[] = [];

  for (const documentoLicenciaClaseLicencia of docLicenciaExistente.documentoLicenciaClaseLicencia) {
    const documentoLicenciaMobileDto = new DocumentoLicenciaMobileDto();

    documentoLicenciaMobileDto.folioPapelSeguridad =
      docLicenciaExistente.papelSeguridad.Folio + docLicenciaExistente.papelSeguridad.LetrasFolio;
    documentoLicenciaMobileDto.idFolioSGL = docLicenciaExistente.idPapelSeguridad;
    documentoLicenciaMobileDto.estadoEDD = docLicenciaExistente.estadosDD.Nombre;
    documentoLicenciaMobileDto.estadoEDF = docLicenciaExistente.estadosDF.Nombre;
    documentoLicenciaMobileDto.estadoPCL = docLicenciaExistente.estadosPCL.Nombre;
    documentoLicenciaMobileDto.clasesLicencia = documentoLicenciaClaseLicencia.clasesLicencias.Abreviacion;
    documentoLicenciaMobileDto.restricciones = ''; //////////////////////////////////////////////////////////////////// Son array?
    documentoLicenciaMobileDto.fechaCaducidad = documentoLicenciaClaseLicencia.caducidadOtorgamiento;
    documentoLicenciaMobileDto.municipalidad = respuestaTramite.solicitudes.oficinas.instituciones.Nombre;
    documentosLicenciasMobile.push(documentoLicenciaMobileDto);
  }

  licenciaConConductorDto.documentosLicencia = documentosLicenciasMobile;

  // Mapeo de las clases de licencias para mobile
  licenciaConConductorDto.datosClasesLicencias = [];

  datosClasesLicenciasFechas
    .filter(y => y.asignado)
    .forEach(x => {
      const dlclmDto = new DocumentoLicenciaClaseLicenciaMobileDto();

      dlclmDto.idClaseLicencia = x.idClaseLicencia;
      dlclmDto.descClaseLicencia = x.descClaseLicencia;
      dlclmDto.fechaPrimerOtorgamiento = x.fechaPrimerOtorgamiento;
      dlclmDto.fechaActualOtorgamiento = x.fechaActualOtorgamiento;
      dlclmDto.fechaProximoControl = x.fechaProximoControl;

      licenciaConConductorDto.datosClasesLicencias.push(dlclmDto);
    });

  return licenciaConConductorDto;
}
