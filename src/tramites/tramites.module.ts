import { Module } from '@nestjs/common';
import { TramitesController } from './controller/tramites.controller';
import { TramitesService } from './services/tramites.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TramitesEntity } from './entity/tramites.entity';
import { Solicitudes } from './entity/solicitudes.entity';
import { PostulantesEntity } from './entity/postulantes.entity';
import { TramitesClaseLicencia } from 'src/tramites-clase-licencia/entity/tramites-clase-licencia.entity';
import { EstadosTramiteEntity } from './entity/estados-tramite.entity';
import { DocumentosTramitesEntity } from 'src/documentos-tramites/entity/documentos-tramites.entity';
import { User } from 'src/users/entity/user.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { jwtConstants } from 'src/users/constants';
import { AuthService } from 'src/auth/auth.service';
import { UtilService } from 'src/utils/utils.service';
import { EstadosPCLEntity } from 'src/estados-PCL/entity/estados-PCL.entity';
import { EstadosEDDEntity } from 'src/estados-EDD/entity/estados-EDD.entity';
import { EstadosEDFEntity } from 'src/estados-EDF/entity/estados-EDF.entity';
import { DocumentosLicenciaClaseLicenciaEntity } from 'src/documento-licencia/entity/documento-licencia.entity';
import { TipoImagenLicenciaEntity } from 'src/imagen-licencia/entity/tipo-imagen-licencia.entity';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';
import { PapelSeguridadFolioEntity } from 'src/folio/entity/papelSeguridad.entity';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { CambioestadorceiService } from 'src/cambio-estadorcei/services/cambioestadorcei/cambioestadorcei.service';
import { IngresoPostulanteService } from 'src/ingreso-postulante/services/ingreso-postulante/ingreso-postulante.service';
import { ConductoresEntity } from 'src/conductor/entity/conductor.entity';
import { Postulantes } from 'src/ingreso-postulante/entity/aspirante.entity';
import { ServicioGenerarLicenciasService } from 'src/shared/services/ServiciosComunes/ServicioGenerarLicencias.services';
import { TiposTramiteEntity } from 'src/tipos-tramites/entity/tipos-tramite.entity';
import { InstitucionesController } from './controller/instituciones.controller';
import { InstitucionesService } from './services/instituciones.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TramitesEntity,
      TramitesClaseLicencia,
      Solicitudes,
      PostulantesEntity,
      DocumentosLicenciaClaseLicenciaEntity,
      DocumentosLicencia,
      TipoImagenLicenciaEntity,
      EstadosPCLEntity,
      EstadosEDDEntity,
      EstadosEDFEntity,
      PapelSeguridadFolioEntity,
      EstadosTramiteEntity,
      DocsRolesUsuarioEntity,
      DocumentosTramitesEntity,
      User,
      RolesUsuarios,
      ComunasEntity,
      ConductoresEntity,
      Postulantes,
      TiposTramiteEntity
    ]),
  ],
  providers: [TramitesService, UtilService,InstitucionesService, AuthService, CambioestadorceiService, IngresoPostulanteService, ServicioGenerarLicenciasService],
  exports: [TramitesService, ServicioGenerarLicenciasService],
  controllers: [TramitesController,InstitucionesController],
})
export class TramitesModule {}
