import { ApiPropertyOptional } from "@nestjs/swagger";
export class ImagenesUsuarioDTO{

    @ApiPropertyOptional()
    idImagenes: number;

    @ApiPropertyOptional()
    fileBinary: Buffer;

    @ApiPropertyOptional()
    created_at: Date;

    @ApiPropertyOptional()
    idUsuario: number;
}
