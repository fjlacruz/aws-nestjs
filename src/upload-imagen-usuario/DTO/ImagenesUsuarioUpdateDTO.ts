import { ApiPropertyOptional } from "@nestjs/swagger";

export class ImagenesUsuarioUpdateDTO {
  
    @ApiPropertyOptional()
    fileBinary: Buffer;

    @ApiPropertyOptional()
    created_at: Date;

    @ApiPropertyOptional()
    idUsuario: number;
}
