import { Test, TestingModule } from '@nestjs/testing';
import { UploadImagenUsuarioController } from './upload-imagen-usuario.controller';

describe('UploadImagenUsuarioController', () => {
  let controller: UploadImagenUsuarioController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UploadImagenUsuarioController],
    }).compile();

    controller = module.get<UploadImagenUsuarioController>(UploadImagenUsuarioController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
