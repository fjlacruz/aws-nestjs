import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { ImagenesUsuarioDTO } from '../DTO/ImagenesUsuarioDTO.dto';
import { ImagenesUsuarioUpdateDTO } from '../DTO/ImagenesUsuarioUpdateDTO';
import { UploadImagenUsuarioService } from '../services/upload-imagen-usuario.service';


@ApiTags('upload-imagen-usuario')
@Controller('upload-imagen-usuario')
export class UploadImagenUsuarioController {

    constructor(private readonly uploadImagenUsuarioService: UploadImagenUsuarioService) { }

    @Get('imagenes')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve todas las Imagenes' })
    async getImagenes() {
        const data = await this.uploadImagenUsuarioService.getImagenes();
        return { data };
    }


    @Get('imagenById/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve una imagen por Id' })
    async getImagenById(@Param('id') id: number) {
        const data = await this.uploadImagenUsuarioService.getImagen(id);
        return { data };
    }


    @Post('uploadImagenUsuario')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que crea un nuevo Registro de Imagen' })
    async uploadImagenUsuario(
        @Body() imagenesUsuarioDTO: ImagenesUsuarioDTO) {

        const data = await this.uploadImagenUsuarioService.createImagen(imagenesUsuarioDTO);
        return {data};

    }

    @Get('imagenByIdUsuario/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que devuelve las imagenes por idUsuarion' })
    async imagenByIdUsuario(@Param('id') id: number) {
        const data = await this.uploadImagenUsuarioService.getImagenesByIdUsuario(id);
        return { data };
    }


    @Post('updateImagen/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio que modifica la metadatos de la imagen' })
    async updateImagen(
        @Param('id') id: number, @Body() imagenesUsuarioDTO: ImagenesUsuarioUpdateDTO) {
        const data = await this.uploadImagenUsuarioService.update(id, imagenesUsuarioDTO);
        return {data};

    }
}
