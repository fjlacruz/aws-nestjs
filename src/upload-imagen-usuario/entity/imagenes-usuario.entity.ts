import {Entity, PrimaryGeneratedColumn, Column,PrimaryColumn} from "typeorm";

@Entity('ImagenesUsuario')
export class ImagenesUsuarioEntity {

    @PrimaryColumn()
    idImagenes: number;

    @Column({
        name: 'fileBinary',
        type: 'bytea',
        nullable: false,
    })
    fileBinary: Buffer;

    @Column()
    created_at: Date;

    @Column()
    idUsuario: number;

}
