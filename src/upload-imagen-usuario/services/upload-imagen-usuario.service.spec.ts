import { Test, TestingModule } from '@nestjs/testing';
import { UploadImagenUsuarioService } from './upload-imagen-usuario.service';

describe('UploadImagenUsuarioService', () => {
  let service: UploadImagenUsuarioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UploadImagenUsuarioService],
    }).compile();

    service = module.get<UploadImagenUsuarioService>(UploadImagenUsuarioService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
