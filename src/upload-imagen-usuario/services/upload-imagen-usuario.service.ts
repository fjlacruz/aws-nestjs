import { Injectable , NotFoundException} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection, getRepository } from 'typeorm';
import { ImagenesUsuarioDTO } from '../DTO/ImagenesUsuarioDTO.dto';
import { ImagenesUsuarioEntity } from '../entity/imagenes-usuario.entity';


@Injectable()
export class UploadImagenUsuarioService {
    constructor(@InjectRepository(ImagenesUsuarioEntity) private readonly imagenesUsuarioRespository: Repository<ImagenesUsuarioEntity>) { }

    async getImagenes() {
        return await this.imagenesUsuarioRespository.find();
    }

    async getImagen(id:number){
        const documento= await this.imagenesUsuarioRespository.findOne(id);
        if (!documento)throw new NotFoundException("imagen dont exist");
        
        return documento;
    }

    async createImagen(imagenesUsuarioDTO: ImagenesUsuarioDTO): Promise<ImagenesUsuarioDTO> {
        return await this.imagenesUsuarioRespository.save(imagenesUsuarioDTO);
    }

    async getImagenesByIdUsuario(id:number){
       const documento = await getConnection().createQueryBuilder() 
                .select("ImagenesUsuario") 
                .from(ImagenesUsuarioEntity, "ImagenesUsuario") 
                .where("ImagenesUsuario.idUsuario = :id", { id: id})
                .orderBy('ImagenesUsuario.idImagenes', 'DESC')
                .getOne();
        if (!documento)throw new NotFoundException("imagen dont exist");
        return documento;
    }

    async update(idImagenes: number, data: Partial<ImagenesUsuarioDTO>) {
        await this.imagenesUsuarioRespository.update({ idImagenes }, data);
        return await this.imagenesUsuarioRespository.findOne({ idImagenes });
    }

}
