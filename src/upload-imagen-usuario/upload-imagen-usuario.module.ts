import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UploadImagenUsuarioController } from './controller/upload-imagen-usuario.controller';
import { ImagenesUsuarioEntity } from './entity/imagenes-usuario.entity';
import { UploadImagenUsuarioService } from './services/upload-imagen-usuario.service';


@Module({
  imports: [TypeOrmModule.forFeature([ImagenesUsuarioEntity])],
  providers: [UploadImagenUsuarioService],
  exports: [UploadImagenUsuarioService],
  controllers: [UploadImagenUsuarioController]
})
export class UploadImagenUsuarioModule {}
