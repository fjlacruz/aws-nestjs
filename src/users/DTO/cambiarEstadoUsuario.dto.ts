import { ApiPropertyOptional } from "@nestjs/swagger";

export class CambiarEstadoUsuarioDto {
  
    @ApiPropertyOptional()
    RUN: string;
    @ApiPropertyOptional()
    motivo?: string;
    @ApiPropertyOptional()
    emailUsuarioActivado?: string;
}
