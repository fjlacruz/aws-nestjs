import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class ChangePasswordDTO {
  @ApiProperty({
    name: 'password',
    type: 'string',
    required: true,
    example: false,
  })
  @IsNotEmpty({ message: 'password es obligatorio' })
  @IsString({message: 'password debe ser un string'})
  @MinLength(8, {message: 'password debe tener más de 8 caracteres'})
  password: string;

  @ApiProperty({
    name: 'password',
    type: 'string',
    required: true,
    example: false,
  })
  @IsNotEmpty({ message: 'newPassword es obligatorio' })
  @IsString({message: 'newPassword debe ser un string'})
  @MinLength(8, {message: 'newPassword debe tener más de 8 caracteres'})
  newPassword:string;
}
