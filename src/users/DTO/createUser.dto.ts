import { ApiPropertyOptional } from '@nestjs/swagger';

export class CreateUserDto {
  idUsuario?: number;
  @ApiPropertyOptional()
  RUN: number;

  DV: string;

  email: string;

  Nombre: string;
  @ApiPropertyOptional()
  passwd: string;

  tokenCU: string;

  conectado: boolean;

  Telefono: string;
}

export default CreateUserDto;
