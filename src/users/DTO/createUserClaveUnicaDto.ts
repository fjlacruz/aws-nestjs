import { ApiPropertyOptional } from "@nestjs/swagger";

export class CreateUserClaveUnicaDto {

    @ApiPropertyOptional()
    RUN: number;
    
    @ApiPropertyOptional()
    acces_token: string;
    
}
