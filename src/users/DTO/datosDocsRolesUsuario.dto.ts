
export class datosDocsRolesUsuarioDTO {

    idDocsRolesUsuario: number;
    idRol: number;
    idUsuario: number;
    idDocsRequeridoRol: number;
    nombreDoc:string;
    archivo:string;
    created_at: Date;
}
