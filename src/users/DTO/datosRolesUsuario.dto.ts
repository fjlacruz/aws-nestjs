import { datosDocsRolesUsuarioDTO } from "src/users/DTO/datosDocsRolesUsuario.dto";

export class datosRolesUsuarioDto {

    idRol: number;
    idOficina:number;
    nombreOficina:string;
    NombreRol: string;
    DescripcionRol: string;
    idCalidadJuridica: number;
    idEstamento: number;
    idGrado: number;
    idRegion:number;
    nombreRegion:string;
    editable: boolean;
    documentos: datosDocsRolesUsuarioDTO[];
}
