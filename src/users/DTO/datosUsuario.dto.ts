import { ApiPropertyOptional } from '@nestjs/swagger';
import { datosRolesUsuarioDto } from './datosRolesUsuario.dto';

export class DatosUsuarioDto {
  @ApiPropertyOptional()
  idUsuario: number;
  @ApiPropertyOptional()
  run: string;
  @ApiPropertyOptional()
  nombre: string;
  @ApiPropertyOptional()
  apellidoPaterno: string;
  @ApiPropertyOptional()
  apellidoMaterno: string;
  @ApiPropertyOptional()
  fechaNacimiento: Date;
  @ApiPropertyOptional()
  fechaDefuncion: Date;
  @ApiPropertyOptional()
  idSexo: number;
  @ApiPropertyOptional()
  email: string;
  @ApiPropertyOptional()
  password: string;
  @ApiPropertyOptional()
  telefono: string;
  @ApiPropertyOptional()
  dirCalle: string;
  @ApiPropertyOptional()
  dirNumero: number;
  @ApiPropertyOptional()
  dirLetra: string;
  @ApiPropertyOptional()
  dirResto: string;
  @ApiPropertyOptional()
  estado: boolean;
  @ApiPropertyOptional()
  motivoRechazo: string;
  @ApiPropertyOptional()
  idNacionalidad: number;
  @ApiPropertyOptional()
  idEstadoCivil: number;
  @ApiPropertyOptional()
  idNivel: number;
  @ApiPropertyOptional()
  profesion: string;
  @ApiPropertyOptional()
  diplomatico: boolean;
  @ApiPropertyOptional()
  discapacidad: boolean;
  @ApiPropertyOptional()
  indiqueDiscapacidad: string;
  @ApiPropertyOptional()
  idComuna: number;
  @ApiPropertyOptional()
  idRegion: number;
  @ApiPropertyOptional()
  docRoles: datosRolesUsuarioDto[];
  //docLicencias: LicenciasConEstadosDto;
}
