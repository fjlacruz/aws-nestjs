import { ApiPropertyOptional } from "@nestjs/swagger";
import { RolDto } from "src/roles/DTO/rol.dto";

export class DocRolesUsuarioDto {

    @ApiPropertyOptional()
    idDocsRolesUsuario: number;

    @ApiPropertyOptional()
    idRol: number;

    @ApiPropertyOptional()
    rol: RolDto;
    
    @ApiPropertyOptional()
    idInstitucion:number;

    @ApiPropertyOptional()
    nombreDoc:string;

    @ApiPropertyOptional()
    archivo:string;

    @ApiPropertyOptional()
    created_at: string;

    @ApiPropertyOptional()
    calidadJuridica:string;

    @ApiPropertyOptional()
    estamento:string;

    @ApiPropertyOptional()
    grado:string;

}
