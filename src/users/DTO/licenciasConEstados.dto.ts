
export class LicenciasConEstadosDto {
  licencias: string[];
  antiguedad: string;
  estadoRNC: string;
  estadoEDD: string;
  estadoEDF: string;
}