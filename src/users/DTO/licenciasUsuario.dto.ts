export class LicenciasUsuarioDto {
    claseLicencia: string;
    antiguedad: string;
    estadoRNC: number;
    estadoEDD: number;
    estadoEDF: number;
  }