export class ListadoUsuariosDto {
    
    idUsuario: number;
    run: string;
    nombreCompleto: string;
    roles: string[];
    regionesRoles: string[];
    //municipalidad: string;
    region: string;
    //clasesLicencias: string[];
    activado?: boolean;
    editable: boolean;
}
   
export default ListadoUsuariosDto;