export class ResponseUserDto {
  idUsuario: number;
  RUN: number;
  DV: string;
  Nombres: string;
  created_at: Date;
  update_at: Date;
  modify_by: number;
  email: string;
  ApellidoPaterno: string;
  ApellidoMaterno: string;
  Activo: boolean;
  conectado: boolean;
  DirCalle: string;
  DirNro: number;
  DirLetra: string;
  idOpcionSexo: number;
  Telefono: string;
  FechaNacimiento: Date;
  comuna: number;
}

export default ResponseUserDto;
