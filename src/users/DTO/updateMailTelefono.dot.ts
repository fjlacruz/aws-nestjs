import { ApiPropertyOptional } from "@nestjs/swagger";

export class UpdateMailTelefonoDto {
 
    @ApiPropertyOptional() 
    email: string;
  
    @ApiPropertyOptional()  
    telefono:string;
}
