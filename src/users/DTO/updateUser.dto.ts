
export class UpdateUserDto {

    //idUsuario: number;
    RUN: number;
    DV:number;
    Nombres:string;
    created_at:Date;
    update_at:Date;
    modify_by:number;
    passwd: string;
    email: string;
    ApellidoPaterno:string;
    ApellidoMaterno:string;
    tokenCU: string;
    conectado: boolean;
    DirCalle:string;
    DirNro:number;
    DirLetra:string;
    Activo: boolean;
    idOpcionSexo: number;
    FechaNacimiento:Date;
    Telefono:string;
    LastUserConnection: Date;
    TokenToResetPasswd: string;
}
