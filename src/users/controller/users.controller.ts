import { Get, Param, Post, Body, Patch, Put, Req, UsePipes, ValidationPipe } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { CreateUserDto } from '../DTO/createUser.dto';
import { UsersService } from '../services/users.service';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { ResetPassworDTO } from '../DTO/resetPassworDTO';
import { UpdatePasswordDto } from '../DTO/update-password.dto';
import { UpdateUserDto } from '../DTO/updateUser.dto';
import { UpdateMailTelefonoDto } from '../DTO/updateMailTelefono.dot';
import { DatosUsuarioDto } from '../DTO/datosUsuario.dto';
import { FiltrosUserDTO } from '../DTO/filtrosUserDTO.dto';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import { TokenPermisoDto } from 'src/utils/token.permiso.dto';
import { PermisosNombres } from 'src/constantes';
import { AuthService } from 'src/auth/auth.service';
import { CambiarEstadoUsuarioDto } from '../DTO/cambiarEstadoUsuario.dto';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Public } from '../../auth/guards/auth.guard';
import { RolDto } from 'src/roles/DTO/rol.dto';
import { Resultado } from 'src/utils/resultado';
import { ChangePasswordDTO } from '../DTO/changePassword.dto';

@ApiTags('Servicios-Usuario')
@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService, private readonly authService: AuthService) {}
  @Get('/getUsers')
  async getUsers() {
    const data = await this.userService.getUsers();
    return { data };
  }

  @Post('/getUsersFiltered')
  async getUsersFiltered(@Body() paginacionArgs: PaginacionArgs, @Req() req: any) {

      const data = await this.userService.getUsersFiltered(req, paginacionArgs);
      return { data };

  }

  @Get(':id')
  async getUser(@Param('id') id: number) {
    const data = await this.userService.getUser(id);
    return { data };
  }

  // @Post('creaUsuario')
  // async addUsuario(@Body() createUserDto: CreateUserDto) {
  //   const data = await this.userService.createUser(createUserDto);
  //   return { data };
  // }

  @Get('usuarioByRUN/:run')
  @Public()
  async getUsuarioByRUN(@Param('run') RUN: number) {
    const data = await this.userService.getUsuarioByRUN(RUN);
    return { data };
  }

  @Get('usuarioConRolesByRUN/:run')
  @ApiBearerAuth()
  async usuarioConRolesByRUN(@Param('run') RUN: string, @Req() req: any) {
      const data = await this.userService.getUsuarioConRolesByRUN(req, RUN);
      return { data };
  }

  @Get('rolDenegarByUserId/:id')
  @ApiBearerAuth()
  async rolDenegarByUserId(@Param('id') id: string, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const rolesUsuarios: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];
      const rolDenegar = rolesUsuarios[0].roles.Nombre;
      return { rolDenegar };
    } else {
      return { data: validarPermisos };
    }
  }

  @Patch('paginasUsuario/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza datos un usuario' })
  async uppdateUser(@Param('id') id: number, @Body() data: Partial<CreateUserDto>) {
    return await this.userService.update(id, data);
  }

  @Post('activarUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que activa un Usuario' })
  async activarUsuario(@Body() EstadoUsuario: CambiarEstadoUsuarioDto, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const rolesUsuarios: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      // Obsoleto
      //const data = await this.userService.activarUsuario(EstadoUsuario, rolesUsuarios);
      //return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('desactivarUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que desactiva un Usuario' })
  async desactivarUsuario(@Body() EstadoUsuario: CambiarEstadoUsuarioDto, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const rolesUsuarios: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.userService.desactivarUsuario(EstadoUsuario, rolesUsuarios);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('rechazarUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que rechaza un Usuario' })
  async rechazarUsuario(@Body() EstadoUsuario: CambiarEstadoUsuarioDto, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeUsuariosActivarDesactivarUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const rolesUsuarios: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.userService.rechazarUsuario(EstadoUsuario, rolesUsuarios);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Patch('usuarioUpdate/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza datos de un Usuario' })
  async UsuarioByRUN(@Param('run') RUN: number, @Body() updateMailTelefonoDto: UpdateMailTelefonoDto, @Req() req: any) {
    const data = await this.userService.updateUserAdminUsuario(req, RUN, updateMailTelefonoDto.email, updateMailTelefonoDto.telefono);
    return { data };
  }

  @Patch('usuarioUpdatePerfilDeUsuario/:run')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza datos de un Usuario' })
  async UsuarioByRUNPerfilDeUsuario(@Param('run') RUN: number, @Body() updateMailTelefonoDto: UpdateMailTelefonoDto, @Req() req: any) {
    const data = await this.userService.updateUserPerfilDeUsuario(req, RUN, updateMailTelefonoDto.email, updateMailTelefonoDto.telefono);
    return { data };
  }

  @Post('resetPassword')
  @ApiOperation({ summary: 'Servicio de envio de correo para recuperar contraseña usuario' })
  async resetPassword(@Body() resetPassworDTO: ResetPassworDTO) {
    const data = await this.userService.sendMailResetPassword(resetPassworDTO.email);
    return { data };
  }

  @Patch('changePassword')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'End-point para modificar la contraseña de un usuario' })
  @UsePipes(ValidationPipe) // Habilitamos las validaciones
  public async changePassword(@Body() changePasswordDTO: ChangePasswordDTO, @Req() req): Promise<{ data: Resultado }> {
    let data = null;
    const splitBearerToken = req.headers.authorization.split(' ');
    const token = splitBearerToken[1];
    const tokenPermisos = new TokenPermisoDto(token, [PermisosNombres.AccesoDatosPersonalesDelUsuario]);

    const validarPermisos = await this.authService.checkUserAndRol(tokenPermisos);

    if (validarPermisos.ResultadoOperacion) {
      const idUsuario = (await this.authService.usuario()).idUsuario; //validarPermisos.Respuesta[0].idUsuario;
      data = await this.userService.changePassword(idUsuario, changePasswordDTO);
    } else {
      data = validarPermisos;
    }

    return { data };
  }

  @Post('altaUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que crea un Usuario con sus Roles' })
  async altaUsuario(@Body() userDto: DatosUsuarioDto, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeUsuariosCrearNuevoUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      const rolesUsuarios: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.userService.altaUsuario(userDto, rolesUsuarios);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Post('actualizarUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que actualiza un Usuario y sus roles' })
  async actualizarUsuario(@Body() userDto: DatosUsuarioDto, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.AdministracionSGLCAdministraciondeUsuariosModificarDatosUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      let rolesUsuarioConectado: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.userService.actualizarUsuario(userDto, rolesUsuarioConectado);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('delUsuario/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que borra un Usuario' })
  async delUsuario(@Param('id') id: number, @Req() req: any) {
    let splittedBearerToken = req.headers.authorization.split(' ');
    let token = splittedBearerToken[1];

    let tokenPermiso = new TokenPermisoDto(token, [PermisosNombres.PerfildeUsuarioDatosdelUsuarioModificarDatosdelUsuario]);

    let validarPermisos = await this.authService.checkUserAndRol(tokenPermiso);

    if (validarPermisos.ResultadoOperacion) {
      let rolesUsuarioConectado: RolesUsuarios[] = validarPermisos.Respuesta as RolesUsuarios[];

      const data = await this.userService.delUsuario(id, rolesUsuarioConectado);
      return { data };
    } else {
      return { data: validarPermisos };
    }
  }

  @Get('completo/:idUsuario')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Servicio que consulta un Usuario' })
  async getUsuario(@Param('idUsuario') id: number, @Req() req: any) {
    return await this.userService.usuarioCompleto(req, Number(id)).then((data: any) => {
      return { data, code: 200 };
    });
  }
}
