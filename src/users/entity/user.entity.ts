import { ApiProperty } from '@nestjs/swagger';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';
import { NotificacionesEntity } from 'src/notificaciones/entity/notificaciones.entity';
import { OpcionEstadosCivilEntity } from 'src/opcion-estados-civil/entity/OpcionEstadosCivil.entity';
import { OpcionNacionalidadesEntity } from 'src/opcion-nacionalidades/entity/opcion-nacionalidades.entity';
import { OpcionSexoEntity } from 'src/opciones-sexo/entity/sexo.entity';
import { DocumentosLicencia } from 'src/registro-pago/entity/documentos-licencia.entity';
import { RegistroUsuarios } from 'src/registro-usuarios/entity/registro-usuario.entity';
import { ResultadoExaminacionEntity } from 'src/resultado-examinacion/entity/resultado-Examinacion.entity';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { Roles } from 'src/roles/entity/roles.entity';
import { PostulantesEntity } from 'src/tramites/entity/postulantes.entity';
import { UsuarioOficinaEntity } from 'src/tramites/entity/usuarioOficina.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, ManyToOne, JoinColumn } from 'typeorm';

@Entity('Usuarios')
export class User {
  @PrimaryGeneratedColumn()
  idUsuario: number;

  @Column()
  RUN: number;

  @Column()
  DV: string;

  @Column()
  Nombres: string;

  @Column({ type: 'timestamp' })
  created_at: Date;

  @Column()
  update_at: Date;

  @Column()
  modify_by: number;

  @Column()
  passwd: string;

  @Column()
  email: string;

  @Column()
  ApellidoPaterno: string;

  @Column()
  ApellidoMaterno: string;

  @Column()
  tokenCU: string;

  @Column()
  conectado: boolean;

  @Column()
  DirCalle: string;

  @Column()
  DirNro: number;

  @Column()
  DirLetra: string;

  @Column()
  dirResto: string;

  @Column()
  Activo: boolean;

  @Column()
  idOpcionSexo: number;
  @OneToOne(() => OpcionSexoEntity, opcionSexo => opcionSexo.user)
  @JoinColumn({ name: 'idOpcionSexo' })
  readonly opcionSexo: OpcionSexoEntity;

  @Column()
  idOpcionNacionalidad: number;
  @OneToOne(() => OpcionNacionalidadesEntity, opcionNacionalidad => opcionNacionalidad.user)
  @JoinColumn({ name: 'idOpcionNacionalidad' })
  readonly opcionNacionalidad: OpcionNacionalidadesEntity;

  @Column()
  idOpcionEstadoCivil: number;
  @OneToOne(() => OpcionEstadosCivilEntity, opcionEstadoCivil => opcionEstadoCivil.user)
  @JoinColumn({ name: 'idOpcionEstadoCivil' })
  readonly opcionEstadoCivil: OpcionEstadosCivilEntity;

  @Column()
  idComuna: number;
  @OneToOne(() => ComunasEntity, comunas => comunas.user)
  @JoinColumn({ name: 'idComuna' })
  readonly comunas: ComunasEntity;

  @Column()
  FechaNacimiento: Date;

  @Column()
  fechaDefuncion: Date;

  @Column()
  Telefono: string;

  @Column()
  idNivel: number;

  @Column()
  profesion: string;

  @Column()
  diplomatico: boolean;

  @Column()
  discapacidad: boolean;

  @Column()
  indiqueDiscapacidad: string;

  @Column()
  LastUserConnection: Date;

  @Column()
  TokenToResetPasswd: string;

  @ApiProperty()
  @OneToMany(() => RegistroUsuarios, registroUsuarios => registroUsuarios.usuario)
  readonly registroUsuarios: RegistroUsuarios[];

  @ApiProperty()
  @OneToMany(() => RegistroUsuarios, registroUsuarios => registroUsuarios.usuario)
  readonly ingresadoPor: RegistroUsuarios[];

  @ApiProperty()
  @OneToMany(() => RegistroUsuarios, registroUsuarios => registroUsuarios.usuario)
  readonly aprobadoPor: RegistroUsuarios[];

  @ApiProperty()
  @OneToMany(() => RolesUsuarios, rolesUsuarios => rolesUsuarios.user)
  rolesUsuarios: RolesUsuarios[];

  @ApiProperty()
  @OneToOne(() => PostulantesEntity, postulante => postulante.usuario)
  readonly postulante: PostulantesEntity;

  @ApiProperty()
  @OneToMany(() => UsuarioOficinaEntity, usuarioOficina => usuarioOficina.usuario)
  readonly usuarioOficina: UsuarioOficinaEntity[];

  @ApiProperty()
  @OneToMany(() => DocumentosLicencia, documentosLicencia => documentosLicencia.usuario)
  readonly documentosLicencia: DocumentosLicencia[];

  @ApiProperty()
  @OneToMany(() => DocsRolesUsuarioEntity, docsRolesUsuario => docsRolesUsuario.usuario)
  readonly docsRolesUsuario: DocsRolesUsuarioEntity[];

  @ApiProperty()
  @OneToMany(() => Roles, roles => roles.usuario)
  readonly roles: Roles[];

  @ApiProperty()
  @OneToMany(() => NotificacionesEntity, notificacion => notificacion.usuarios)
  readonly notificacion: NotificacionesEntity[];

  @ApiProperty()
  @OneToMany(() => ResultadoExaminacionEntity, resultadoExaminacion => resultadoExaminacion.usuarioAprob)
  readonly resultadoExaminacion: ResultadoExaminacionEntity[];  
}
