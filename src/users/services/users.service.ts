import { orderBy } from 'lodash';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, In, Brackets, UpdateResult } from 'typeorm';
import CreateUserDto from '../DTO/createUser.dto';
import ResponseUserDto from '../DTO/responseUser.dto';
import { User } from '../entity/user.entity';
import { MailerService } from '@nestjs-modules/mailer';
import { ParametrosGeneralEntity } from '../entity/parametros-general.entity';
import { JwtService } from '@nestjs/jwt';
import { getConnection } from 'typeorm';
import { UpdatePasswordDto } from '../DTO/update-password.dto';
import { DatosUsuarioDto } from '../DTO/datosUsuario.dto';
import { Resultado } from 'src/utils/resultado';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { PaginacionArgs } from 'src/utils/PaginacionArgs';
import {
  asuntoActivacionUsuario,
  asuntoUsuarioActivado,
  asuntoUsuarioActivadoToActivatedUser,
  asuntoUsuarioDesactivado,
  asuntoUsuarioPendienteActivacion,
  asuntoUsuarioRechazado,
  ColumnasUsuarios,
  pattern,
  PermisosNombres,
  registroUsuarioActivacion,
  registroUsuarioAsignacionRol,
  registroUsuarioCreacionUsuario,
  registroUsuarioDesactivacion,
  registroUsuarioEdicion,
  registroUsuarioEliminacion,
  registroUsuarioRechazo,
  SECRET_KEY_FRONT,
  TipoRolesIds,
  tipoRolOficina,
  tipoRolRegion,
  tipoRolRegionID,
  tipoRolSuperUsuarioID,
} from 'src/constantes';
import { ListadoUsuariosDto } from '../DTO/listadoUsuarios.dto';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';
import { datosRolesUsuarioDto } from '../DTO/datosRolesUsuario.dto';
import { datosDocsRolesUsuarioDTO } from '../DTO/datosDocsRolesUsuario.dto';
import { PaginacionDtoParser } from 'src/utils/PaginacionDtoParser';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { CambiarEstadoUsuarioDto } from '../DTO/cambiarEstadoUsuario.dto';
import { UtilService } from 'src/utils/utils.service';
import { Email } from 'src/utils/Email';
import { RegistroUsuarios } from 'src/registro-usuarios/entity/registro-usuario.entity';
import { TipoResgistroUsuario } from 'src/tipo-resgistro-usuario/entity/tipo-resgistro-usuario.entity';
import { correoUsuarioActivacion, correoUsuarioActivado, correoUsuarioActivadoToUser, plantillaCorreo } from 'src/utils/cuerpoCorreos';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { Roles } from 'src/roles/entity/roles.entity';
import { RolActivaRol } from 'src/rol-activa-rol/entity/rol-activa-rol.entity';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { RolAsignaRol } from 'src/rol-asigna-rol/entoty/rol-asigna-rol.entity';
import { DocRolesUsuarioDto } from '../DTO/docRolesUsuario.dto';
import { SetResultadoService } from 'src/shared/services/set-resultado/set-resultado.service';
import { ChangePasswordDTO } from '../DTO/changePassword.dto';
import * as CryptoJS from 'crypto-js';
import { ComunasEntity } from 'src/comunas/entity/comunas.entity';
import { SolicitudAltaUsuarioEntity } from '../../solicitudes-alta-usuarios/solicitudAltaUsuario.entity';
import { fromEvent } from 'rxjs';
import { AuthService } from 'src/auth/auth.service';
import { getFormulariosExaminacionesTipoClaseDto } from 'src/formularios-examinaciones/DTO/getformulariosexaminacionTipoClase.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Roles) private readonly rolesRepository: Repository<Roles>,
    @InjectRepository(RolActivaRol) private readonly rolActivaRolRepository: Repository<RolActivaRol>,
    @InjectRepository(RolAsignaRol) private readonly rolAsignaRolRepository: Repository<RolAsignaRol>,
    @InjectRepository(RolesUsuarios) private readonly rolesUsuariosRepository: Repository<RolesUsuarios>,
    @InjectRepository(RegionesEntity) private readonly regionesRepository: Repository<RegionesEntity>,
    @InjectRepository(RegistroUsuarios) private readonly registroUsuariosRepository: Repository<RegistroUsuarios>,
    @InjectRepository(TipoResgistroUsuario) private readonly tipoRegistroUsuariosRepository: Repository<TipoResgistroUsuario>,
    @InjectRepository(DocsRolesUsuarioEntity) private readonly docsRolesUsuarioRepository: Repository<DocsRolesUsuarioEntity>,
    @InjectRepository(OficinasEntity) private readonly oficinasRepository: Repository<OficinasEntity>,
    @InjectRepository(SolicitudAltaUsuarioEntity)
    private readonly solicitudAltaUsuarioEntityRepository: Repository<SolicitudAltaUsuarioEntity>,
    private readonly mailerService: MailerService,
    private readonly utilService: UtilService,
    private jwtService: JwtService,
    private readonly setResultadoService: SetResultadoService,
    private readonly authService: AuthService
  ) {}

  async getUsers() {
    return await this.usersRepository.find();
  }
  /*
    async getUser(id:number){
        const user= await this.usersRespository.findOne(id);
        if (!user)throw new NotFoundException("user dont exist");

        return user;
    }*/

  async getUser(id: number) {
    const user = await this.usersRepository.findOne(id);
    if (!user) throw new NotFoundException('user dont exist');

    const userDto = new ResponseUserDto();

    userDto.ApellidoMaterno = user.ApellidoMaterno;
    userDto.ApellidoPaterno = user.ApellidoPaterno;
    userDto.DV = user.DV;
    userDto.DirCalle = user.DirCalle;
    userDto.DirLetra = user.DirLetra;
    userDto.DirNro = user.DirNro;
    userDto.Nombres = user.Nombres;
    userDto.RUN = user.RUN;
    userDto.idOpcionSexo = user.idOpcionSexo;
    userDto.conectado = user.conectado;
    userDto.created_at = user.created_at;
    userDto.email = user.email;
    userDto.idUsuario = user.idUsuario;
    userDto.modify_by = user.modify_by;
    userDto.update_at = user.update_at;
    userDto.Activo = user.Activo;

    return userDto;
  }

  async getUserLogin(email: string) {
    const user = await this.usersRepository.findOne({ email });
    if (!user) throw new NotFoundException('user dont exist the Email');
    return user;
  }
  async getUserLoginRun(RUN: number) {
    const user = await this.usersRepository.findOne({ RUN });
    if (!user) throw new NotFoundException('user dont exist the RUN');
    return user;
  }

  async getUsersFiltered(req, paginacionArgs: PaginacionArgs): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    const orderBy = ColumnasUsuarios[paginacionArgs.orden.orden];
    let usuariosParseados = new PaginacionDtoParser<ListadoUsuariosDto>();
    let usuarios: Pagination<User>;
    let filtro = paginacionArgs.filtro;
    let esSuperUsuario: boolean = false;
    let idsRegionesUsuarioConectado: number[] = [];
    let puedeEditar: boolean = false;

    try {

      // Comprobamos permisos	
      let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.VisualizarListaIncripcionAdministracionUsuarios);	
    	
      // En caso de que el usuario no sea correcto	
      if (!usuarioValidado.ResultadoOperacion) {	
        return usuarioValidado;	
      }	


      // Obtenemos los ids de los Roles del UsuarioConectado
      for (const rolUsuario of usuarioValidado.Respuesta.rolesUsuarios) {
        if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID) {
          esSuperUsuario = true;
        } else {
          if (rolUsuario.idRegion != null) idsRegionesUsuarioConectado.push(rolUsuario.idRegion);
        }
      }

      // Eliminamos Regiones duplicadas del usuario conectado
      idsRegionesUsuarioConectado = idsRegionesUsuarioConectado.filter((x, i) => idsRegionesUsuarioConectado.indexOf(x) === i);

      usuarios = await paginate<User>(this.usersRepository, paginacionArgs.paginationOptions, {
        relations: ['rolesUsuarios', 
                    'rolesUsuarios.roles', 
                    'comunas', 
                    'comunas.regiones',
                    'rolesUsuarios.oficinas', 
                    'rolesUsuarios.oficinas.comunas', 
                    'rolesUsuarios.oficinas.instituciones',
                    'rolesUsuarios.oficinas.instituciones.comunas',
                    'rolesUsuarios.oficinas.instituciones.comunas.regiones'],
        join: {
          alias: 'usuarios',
          leftJoinAndSelect: {
            rolesUsuarios: 'usuarios.rolesUsuarios',
            roles: 'rolesUsuarios.roles',
            comunas: 'usuarios.comunas',
            regiones: 'comunas.regiones',
          },
        },
        where: qb => {

          // Se establecen filtros según los roles del usuario

          // Filtramos por las oficinas en caso de que el usuario no sea administrador
          //Comprobamos si es superUsuario por si es necesario filtrar
          if (this.authService.checkUserSuperAdmin(usuarioValidado.Respuesta.rolesUsuarios) == false) {
            // Recogemos las oficinas asociadas al usuario
            let idOficinasAsociadas: number[] = this.authService.getOficinasUser(usuarioValidado.Respuesta.rolesUsuarios);

            if (idOficinasAsociadas == null) {
              resultado.Error = 'Usted no tiene ninguna oficina asignada para consultar.';
              resultado.ResultadoOperacion = false;
              resultado.Respuesta = [];

              return resultado;
            } else {
              //qbSolicitudes.andWhere('Solicitudes.idOficina IN (:..._idsOficinas)', { _idsOficinas: idOficinasAsociadas });
              //qb.andWhere('usua__roUs__ofic__inst__comu__regi.idRegion = :region', { region: filtro.region });
              qb.andWhere('usuarios__rolesUsuarios__oficinas.idOficina IN (:..._idsOficinasPermisos)', { _idsOficinasPermisos: idOficinasAsociadas });
            }
          }  


          if (filtro.nombreApellido) {
            qb.andWhere('(LOWER(usuarios.Nombres) like LOWER(:nombre) OR (LOWER(usuarios.ApellidoPaterno) like LOWER(:nombre)) OR (LOWER(usuarios.ApellidoMaterno) like LOWER(:nombre)))', { nombre: `%${filtro.nombreApellido}%` });
          }
          // if (filtro.nombreApellido)
          //   qb.orWhere('LOWER(usuarios.ApellidoPaterno) like LOWER(:nombre)', { nombre: `%${filtro.nombreApellido}%` });
          // if (filtro.nombreApellido)
          //   qb.orWhere('LOWER(usuarios.ApellidoMaterno) like LOWER(:nombre)', { nombre: `%${filtro.nombreApellido}%` });
          if (filtro.run) {
            const rut = filtro.run.split('-');
            let runsplited: string = rut[0];
            runsplited = runsplited.replace('.', '');
            runsplited = runsplited.replace('.', '');
            const div = rut[1];

            qb.andWhere('usuarios.RUN = :run', {
              run: runsplited,
            });

            qb.andWhere('usuarios.DV = :dv', {
              dv: div,
            });

          
            
          }

          if (filtro.region != null && filtro.region != '') {
            qb.andWhere('usua__roUs__ofic__inst__comu__regi.idRegion = :region', { region: filtro.region });
          }
          
          if (filtro.rol != null && filtro.rol != '') {
            qb.andWhere('roles.idRol = :rol', { rol: filtro.rol });
          }
          if (filtro.estado != '') {
            if (filtro.estado == 'null') {
              qb.andWhere('usuarios.Activo IS NULL');
            } else {
              qb.andWhere('usuarios.Activo = :estado', { estado: filtro.estado });
            }
          }
          qb.orderBy(orderBy, paginacionArgs.orden.ordenarPor);
        },
      });

      if (Array.isArray(usuarios.items) && usuarios.items.length) {
        let idUsuarios: number[] = [];

        usuarios.items.forEach(usuario => {
          idUsuarios.push(usuario.idUsuario);
        });

        const rolesUsuariosObtenidos = await this.rolesUsuariosRepository.find({
          relations: ['regiones', 
                      'roles', 
                      'oficinas', 
                      'oficinas.comunas', 
                      'oficinas.instituciones',
                      'oficinas.instituciones.comunas',
                      'oficinas.instituciones.comunas.regiones'],
          where: { idUsuario: In(idUsuarios) },
        });

        let usuariosLista: ListadoUsuariosDto[] = [];

        for (const usuario of usuarios.items) {
          puedeEditar = false;
          let idsRegiones: number[] = [];

          let usuarioLista: ListadoUsuariosDto = new ListadoUsuariosDto();
          usuarioLista.idUsuario = usuario.idUsuario;
          usuarioLista.run = usuario.RUN + '-' + usuario.DV;
          usuarioLista.nombreCompleto = usuario.Nombres + ' ' + usuario.ApellidoPaterno + ' ' + usuario.ApellidoMaterno;
          usuarioLista.activado = usuario.Activo;
          usuarioLista.region = usuario.comunas ? usuario.comunas.regiones.Nombre : null;
          usuario.rolesUsuarios = rolesUsuariosObtenidos.filter(x => x.idUsuario == usuario.idUsuario);

          if (usuario.rolesUsuarios.length > 0) {

            let roles: string[] = [];
            let regionesRoles : string[] = [];

            for (const rolUsuario of usuario.rolesUsuarios) {
              // Obtenemos el id de la Region del Rol o de la Oficina si no tuviese Region
              if (rolUsuario.idRegion) {
                idsRegiones.push(rolUsuario.idRegion);
                roles.push(rolUsuario.roles.Nombre + ' (' + rolUsuario.regiones.Nombre + ')');
              } else if (rolUsuario.idOficina) {
                idsRegiones.push(rolUsuario.oficinas.comunas.idRegion);
                roles.push(rolUsuario.roles.Nombre + ' (' + rolUsuario.oficinas.Nombre + ')');
              } else {
                roles.push(rolUsuario.roles.Nombre);
              }

              if(rolUsuario.regiones){
                regionesRoles.push(rolUsuario.regiones.Nombre);
              }
              else if(rolUsuario.oficinas && rolUsuario.oficinas.instituciones && rolUsuario.oficinas.instituciones.comunas.regiones){
                regionesRoles.push(rolUsuario.oficinas.instituciones.comunas.regiones.Nombre);
              }
              else {
                regionesRoles.push(''); // Para no dejar un hueco asociado a un rol en el listado
              }
            }

            // Eliminamos Regiones duplicadas del usuario
            idsRegiones = idsRegiones.filter((x, i) => idsRegiones.indexOf(x) === i);

            idsRegiones.forEach(idRegion => {
              if (esSuperUsuario || idsRegionesUsuarioConectado.includes(idRegion)) {
                puedeEditar = true;
              }
            });

            usuarioLista.roles = roles;
            usuarioLista.regionesRoles = regionesRoles;
          } else {
            if (esSuperUsuario || idsRegionesUsuarioConectado.length > 0) puedeEditar = true;
          }
          usuarioLista.editable = puedeEditar;

          usuariosLista.push(usuarioLista);
        }

        usuariosParseados.items = usuariosLista;
        usuariosParseados.meta = usuarios.meta;
        usuariosParseados.links = usuarios.links;

        resultado.Respuesta = usuariosParseados;
        resultado.ResultadoOperacion = true;
        resultado.Mensaje = 'Usuarios obtenidos correctamente';
      } else {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'No se encontraron usuarios';
      }
    } catch (error) {
      console.error(error);
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error obteniendo usuarios';
    }

    return resultado;
  }

  async createUser(createAspiranteDto: CreateUserDto): Promise<User> {
    return await this.usersRepository.save(createAspiranteDto);
  }

  async altaUsuario(userDTO: DatosUsuarioDto, rolesUsuarioConectado: RolesUsuarios[]): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    // const RUT = userDTO.run.split('-');

    const rut = userDTO.run.split('-');
    let runsplited: string = rut[0];
    runsplited = runsplited.replace('.', '');
    runsplited = runsplited.replace('.', '');
    const div = rut[1];

    let creandoUnSuperusuario = false;

    let usuarioConectado = new User();

    // Se obtiene el usuario conectado
    usuarioConectado = await this.usersRepository.findOne({ idUsuario: rolesUsuarioConectado[0].idUsuario });

    if (usuarioConectado == undefined) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error: obteniendo usuario conectado';
      return resultado;
    }

    // Se obtiene el tipo registro para la Creación del usuario
    const tipoRegistroUsuario = await this.tipoRegistroUsuariosRepository.find({ Nombre: registroUsuarioCreacionUsuario });

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      //Recogemos listado de usuarios para comprobar que no hay alguno con el mismo RUN
      const ListadoUsuarios = await this.usersRepository.find({ where: { RUN: runsplited, DV: div } });

      if ((await ListadoUsuarios.length) != 0) {
        resultado.Error = 'Ya existe un usuario con el mismo RUN';
        return resultado;
      }
      // Creamos el objeto User que vamos a guardar en la BBDD
      let nuevoUsuario: User = new User();
      nuevoUsuario.Activo = null;
      nuevoUsuario.Nombres = userDTO.nombre;
      nuevoUsuario.ApellidoPaterno = userDTO.apellidoPaterno;
      nuevoUsuario.ApellidoMaterno = userDTO.apellidoMaterno;
      nuevoUsuario.FechaNacimiento = new Date(userDTO.fechaNacimiento);
      nuevoUsuario.RUN = Number(runsplited);
      nuevoUsuario.DV = div;
      nuevoUsuario.idOpcionSexo = userDTO.idSexo;
      nuevoUsuario.idOpcionNacionalidad = userDTO.idNacionalidad;
      nuevoUsuario.idOpcionEstadoCivil = userDTO.idEstadoCivil;
      nuevoUsuario.idComuna = userDTO.idComuna;
      nuevoUsuario.email = userDTO.email;
      nuevoUsuario.Telefono = userDTO.telefono;
      nuevoUsuario.DirCalle = userDTO.dirCalle;
      nuevoUsuario.DirNro = userDTO.dirNumero;
      nuevoUsuario.DirLetra = userDTO.dirLetra;
      nuevoUsuario.conectado = false;
      nuevoUsuario.created_at = new Date();
      nuevoUsuario.passwd = userDTO.password;

      // Guardamos el Usuario en BBDD
      await queryRunner.manager.save(User, nuevoUsuario);

      // Creamos el RegistroUsuario para la creacion del usuario
      let registroUsuario: RegistroUsuarios = new RegistroUsuarios();
      registroUsuario.IdTipoRegistroUsuario = tipoRegistroUsuario.find(
        x => (x.Nombre = registroUsuarioCreacionUsuario)
      ).idTipoResgistroUsuario;

      if (registroUsuario.IdTipoRegistroUsuario == undefined) {
        resultado.Error = 'Error buscando tipo de Registro para creacion de usuario';
        return resultado;
      }
      registroUsuario.IdIngresadoPor = usuarioConectado.idUsuario;
      registroUsuario.idUsuario = nuevoUsuario.idUsuario;
      registroUsuario.Aprobado = null;
      registroUsuario.Fecha = new Date();
      registroUsuario.Observacion = '';

      // Guardamos el RegistroUsuario en BBDD
      await queryRunner.manager.save(RegistroUsuarios, registroUsuario);

      const solicitudAltaUsuario: SolicitudAltaUsuarioEntity = new SolicitudAltaUsuarioEntity();
      solicitudAltaUsuario.idUsuario = usuarioConectado.idUsuario;
      solicitudAltaUsuario.motivoRechazo = '';
      solicitudAltaUsuario.idSolicitante = nuevoUsuario.idUsuario;
      solicitudAltaUsuario.idAprobador = nuevoUsuario.idUsuario;
      solicitudAltaUsuario.created_at = new Date();
      solicitudAltaUsuario.updated_at = new Date();
      // await this.solicitudAltaUsuarioEntityRepository.save(solicitudAltaUsuario);
      await queryRunner.manager.save(SolicitudAltaUsuarioEntity, solicitudAltaUsuario);
      // Comprobamos si el usuario tiene Roles
      if (userDTO.docRoles && userDTO.docRoles.length > 0) {
        let listaRolesActivadores: RolesUsuarios[] = [];

        // Por cada Rol que tiene el usuario se deben crear un RolUsuario, y un DocsRolesUsuario
        for (const rolUser of userDTO.docRoles) {
          // Creamos el objeto RolesUsuarios con los datos proporcionados
          let rolUsuario: RolesUsuarios = new RolesUsuarios();
          rolUsuario.idRol = rolUser.idRol;
          rolUsuario.idUsuario = nuevoUsuario.idUsuario;
          rolUsuario.idOficina = rolUser.idOficina ? rolUser.idOficina : null;
          rolUsuario.idCalidadJuridica = rolUser.idCalidadJuridica;
          rolUsuario.idEstamento = rolUser.idEstamento;
          rolUsuario.idGrado = rolUser.idGrado;
          rolUsuario.idRegion = rolUser.idRegion ? rolUser.idRegion : null;

          // Guardamos los datos en la tabla RolesUsuario en BBDD
          await queryRunner.manager.insert(RolesUsuarios, rolUsuario);

          // Creamos el RegistroUsuario para la asignacion de rol
          let registroUsuarioRol: RegistroUsuarios = new RegistroUsuarios();
          registroUsuarioRol.IdTipoRegistroUsuario = tipoRegistroUsuario.find(
            x => (x.Nombre = registroUsuarioAsignacionRol)
          ).idTipoResgistroUsuario;

          if (registroUsuarioRol.IdTipoRegistroUsuario == undefined) {
            resultado.Error = 'Error buscando tipo de Registro para asignacion de rol';
            return resultado;
          }
          registroUsuarioRol.IdIngresadoPor = usuarioConectado.idUsuario;
          registroUsuarioRol.idUsuario = nuevoUsuario.idUsuario;
          registroUsuarioRol.Aprobado = null;
          registroUsuarioRol.Fecha = new Date();
          registroUsuarioRol.Observacion = '';
          registroUsuarioRol.IdRol = rolUsuario.idRol;

          // Guardamos el RegistroUsuario en BBDD
          await queryRunner.manager.save(RegistroUsuarios, registroUsuarioRol);

          let idRegion;

          if (rolUsuario.idOficina == undefined && rolUsuario.idRegion == undefined) {
            //Si es no tiene oficina ni region, es Superusuario
            creandoUnSuperusuario = true;
          } else {
            //Cogemos id de la region el rol para comprobar si tenemos permiso para asignar este Rol
            if (rolUsuario.idRegion == undefined) {
              rolUsuario.regiones = await (
                await this.oficinasRepository.findOne({
                  relations: ['comunas', 'comunas.regiones'],
                  where: { idOficina: rolUsuario.idOficina },
                })
              ).comunas.regiones;

              idRegion = rolUsuario.regiones.idRegion;
            } else {
              idRegion = rolUsuario.idRegion;
            }
          }

          //Cogemos id de la region el rol para comprobar si tenemos permiso para asignar este Rol
          if (rolUsuario.idRegion == undefined && !creandoUnSuperusuario) {
            rolUsuario.regiones = await (
              await this.oficinasRepository.findOne({
                relations: ['comunas', 'comunas.regiones'],
                where: { idOficina: rolUsuario.idOficina },
              })
            ).comunas.regiones;

            idRegion = rolUsuario.regiones.idRegion;
          } else if (!creandoUnSuperusuario) {
            idRegion = rolUsuario.idRegion;
          }

          if (!creandoUnSuperusuario) {
            // Comprobamos si tenemos permiso para crear el Rol o Roles. Si no es asi terminamos el servicio
            rolesUsuarioConectado.forEach(rolUserConectado => {
              if (
                (rolUserConectado.roles.tipoRoles.Nombre == 'Region' && rolUserConectado.idRegion != idRegion) ||
                (rolUserConectado.roles.tipoRoles.Nombre == 'Oficina' && rolUserConectado.idOficina != rolUsuario.idOficina) ||
                rolUserConectado.roles.tipoRoles.Nombre != 'Superusuario'
              ) {
                resultado.ResultadoOperacion = false;
                resultado.Error = 'Error: no tiene permisos para crear los roles de este usuario';
                return resultado;
              }
            });
          }

          // Se  comprueba si hay un Documento para dicho Rol
          //if (rolUser.documentos && rolUser.documentos.length > 0) {
          // Iteramos los Documentos
          for (const doc of rolUser.documentos) {
            // Comprobamos que el DocRol no sea null, y que sea de uno de los tipos esperados
            if (
              doc.archivo != '' &&
              doc.archivo != null &&
              (doc.archivo.includes('text/plain') ||
                doc.archivo.includes('application/pdf') ||
                doc.archivo.includes('image/jpeg') ||
                doc.archivo.includes('image/png'))
            ) {
              // Creamos el objeto DocsRolesUsuario con los datos proporcionados
              let docRolUsuario: DocsRolesUsuarioEntity = new DocsRolesUsuarioEntity();
              docRolUsuario.idRol = rolUser.idRol;
              docRolUsuario.idUsuario = nuevoUsuario.idUsuario;
              docRolUsuario.idDocsRequeridoRol = doc.idDocsRequeridoRol;
              docRolUsuario.created_at = new Date();
              docRolUsuario.nombreDoc = doc.nombreDoc;
              docRolUsuario.archivo = Buffer.from(doc.archivo);

              // Guardamos los datos en la tabla DocsRolesUsuario en BBDD
              await queryRunner.manager.insert(DocsRolesUsuarioEntity, docRolUsuario);
            } else {
              resultado.Error = 'Error: el archivo subido no tiene el formado esperado';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          }
          // } else {
          //   resultado.Error = 'Error: no se subio un documento para el rol';
          //   await queryRunner.rollbackTransaction();
          //   await queryRunner.release();
          //   return resultado;
          // }

          if (!creandoUnSuperusuario) {
            // Se recogen los activadores
            const usuarioSeremitt = await this.recogerActivadores(rolUsuario, rolesUsuarioConectado);

            if (usuarioSeremitt.ResultadoOperacion) {
              const rolesActivador: RolesUsuarios[] = usuarioSeremitt.Respuesta as RolesUsuarios[];
              rolesActivador.forEach(rolActivador => {
                listaRolesActivadores.push(rolActivador);
              });
            }
          }
        }

        if (!creandoUnSuperusuario && listaRolesActivadores.length > 0) {

          listaRolesActivadores.sort(function (a, b) {
            return b.idUsuario - a.idUsuario;
          });

          // Se preparan datos para el correo y se envia
          const userName =
            nuevoUsuario.RUN +
            '-' +
            nuevoUsuario.DV +
            ', ' +
            nuevoUsuario.Nombres +
            ' ' +
            nuevoUsuario.ApellidoPaterno +
            ' ' +
            nuevoUsuario.ApellidoMaterno;
          const directorName = usuarioConectado.Nombres + ' ' + usuarioConectado.ApellidoPaterno + ' ' + usuarioConectado.ApellidoMaterno;

          if(listaRolesActivadores && listaRolesActivadores.length > 0)
            await this.envioCorreoActivadores(listaRolesActivadores, userName, directorName);
        }
      }

      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Usuario agregado correctamente';
      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      resultado.Error = 'Error creando el Usuario';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async delUsuario(idUsuario: number, rolesUsuarioConectado: RolesUsuarios[]) {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;
    let idsRolesUsuarioConectado: number[] = [];
    let idsRegionesUsuarioConectado: number[] = [];
    let rolesEditables: RolActivaRol[] = [];
    let esSuperUsuario: boolean = false;
    let permisoEliminar: boolean = false;

    // Obtenemos los ids de los Roles del UsuarioConectado
    for (const rolUsuario of rolesUsuarioConectado) {
      if (rolUsuario.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID) {
        esSuperUsuario = true;
      } else {
        idsRegionesUsuarioConectado.push(rolUsuario.idRegion);
      }
      idsRolesUsuarioConectado.push(rolUsuario.idRol);
    }

    // Eliminamos Regiones duplicadas
    idsRegionesUsuarioConectado = idsRegionesUsuarioConectado.filter((x, i) => idsRegionesUsuarioConectado.indexOf(x) === i);

    // Traemos los Roles que puede eliminar el UsuarioConectado
    rolesEditables = await this.rolActivaRolRepository.find({ idRolActivador: In(idsRolesUsuarioConectado) });
    if (rolesEditables.length <= 0 && !esSuperUsuario) {
      resultado.Error = 'Error. No tienes permiso para realizar esta operacion';
      return resultado;
    }

    // Se obtiene el tipo registro para Actualizar Usuario ?
    const tipoRegistroUsuario = await this.tipoRegistroUsuariosRepository.findOne({ Nombre: registroUsuarioEliminacion });
    if (tipoRegistroUsuario == undefined) {
      resultado.Error = 'Error buscando estado adecuado';
      return resultado;
    }

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    // COMPROBAR SI EL USUAURIO NO ESTA ASIGNADO A UN CONDUCTOR / SOLICITUD / TRAMITE ?

    try {
      // Obtenemos los Roles y DocRoles del Usuario almacenados en DDBB
      let rolesAntiguos: RolesUsuarios[] = await queryRunner.manager.find(RolesUsuarios, {
        relations: ['oficinas', 'oficinas.comunas'],
        where: { idUsuario: rolesUsuarioConectado[0].idUsuario },
      });

      // Comprobamos si podemos editar este Usuario, segun la region de los roles.
      for (const rolUsuario of rolesAntiguos) {
        // Obtenemos el id de la Region del Rol
        let idRegionRol = rolUsuario.idRegion ? rolUsuario.idRegion : null;

        // Si no tiene idRegion, obtenemos la Region mediante el id de la Oficina
        if (idRegionRol == null) {
          let idOficina = rolUsuario.idOficina ? rolUsuario.idOficina : null;
          let oficina: OficinasEntity = await queryRunner.manager.findOne(OficinasEntity, {
            relations: ['comunas'],
            where: { idOficina: idOficina },
          });
          if (oficina) idRegionRol = oficina.comunas.idRegion;
          else {
            resultado.Error = 'Error. No se encuentra la Region del Usuario';
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            return resultado;
          }
        }
        if (
          (rolesEditables.find(x => x.idRolDestinoActivacion == rolUsuario.idRol) &&
            idsRegionesUsuarioConectado.find(x => x == idRegionRol)) ||
          esSuperUsuario
        ) {
          permisoEliminar = true;
        }
      }
      // Si no se tienen permisos se cancela la eliminacion de usuario
      if (permisoEliminar == false) {
        resultado.Error = 'Error. No tiene permisos para eliminar este Usuario';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Obtenemos el Usuario por RUN
      let usuario: User = await queryRunner.manager.findOne(User, { idUsuario });

      if (!usuario) {
        resultado.Error = 'Error. No se encuentra el Usuario';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      } else {
        // Actualizamos el estado del Usuario a desactivado
        await queryRunner.manager.update(User, idUsuario, { Activo: false });

        // Creamos el RegistroUsuario para ActualizarUsuario
        let registroUsuario: RegistroUsuarios = new RegistroUsuarios();
        registroUsuario.IdTipoRegistroUsuario = tipoRegistroUsuario.idTipoResgistroUsuario;
        registroUsuario.IdIngresadoPor = rolesUsuarioConectado[0].idUsuario;
        registroUsuario.IdAprobadoPor = rolesUsuarioConectado[0].idUsuario;
        registroUsuario.idUsuario = usuario.idUsuario;
        registroUsuario.Aprobado = false; // False o True?
        registroUsuario.Fecha = new Date();
        registroUsuario.Observacion = '';

        // Guardamos el Registro en BBDD
        await queryRunner.manager.save(RegistroUsuarios, registroUsuario);
      }

      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Usuario eliminado correctamente';
      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      resultado.Error = 'Error eliminando Usuario';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async actualizarUsuario(userDTO: DatosUsuarioDto, rolesUsuarioConectado: RolesUsuarios[]): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    resultado.ResultadoOperacion = false;

    // Se obtiene el tipo registro para Actualizar Usuario ?
    const tipoRegistroUsuario = await this.tipoRegistroUsuariosRepository.findOne({ Nombre: registroUsuarioEdicion });
    if (tipoRegistroUsuario == undefined) {
      resultado.Error = 'Error. Tipo de registro no encontrado';
      return resultado;
    }

    // Obtenemos Conexion para empezar una Transaccion
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      //// USUARIO
      // Obtenemos el Usuario que vamos a Actualizar mediante su RUN
      let usuario: User = await queryRunner.manager.findOne(User, {
        where: qb => {
          qb.where("User.RUN || '-' || User.DV = :RUN", { RUN: userDTO.run });
        },
      });

      if (!usuario) {
        resultado.Error = 'Error. No se encuentra el Usuario';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Se consultan los nuevos roles a asignar y su tipo
      let rolesNuevosAsignar: Roles[] = [];

      if (userDTO.docRoles.length > 0) {
        rolesNuevosAsignar = await this.rolesRepository
          .createQueryBuilder('roles')
          .where('roles.idRol in (:...idRolesNuevos)', { idRolesNuevos: userDTO.docRoles.map(x => x.idRol) })
          .getMany();
      }

      if (!userDTO.docRoles.every(x => rolesNuevosAsignar.some(y => y.idRol == x.idRol))) {
        resultado.Error = 'Error. No se encuentra alguno de los roles facilitados';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Se consultan los roles antiguos asignados
      let rolesAntiguos: RolesUsuarios[] = await this.rolesUsuariosRepository
        .createQueryBuilder('rolesUsuario')
        .innerJoinAndMapOne('rolesUsuario.roles', Roles, 'roles', 'rolesUsuario.idRol = roles.idRol')
        .leftJoinAndMapMany(
          'roles.docsRolesUsuario',
          DocsRolesUsuarioEntity,
          'docsRolesUsuario',
          '(roles.idRol = docsRolesUsuario.idRol AND rolesUsuario.idUsuario = docsRolesUsuario.idUsuario)'
        )
        .leftJoinAndMapOne('rolesUsuario.oficinas', OficinasEntity, 'oficinas', 'rolesUsuario.idOficina = oficinas.idOficina')
        .leftJoinAndMapOne('oficinas.comunas', ComunasEntity, 'comunas', 'oficinas.idComuna = comunas.idComuna')

        .where('rolesUsuario.idUsuario = :_idRolUsuario', {
          _idRolUsuario: usuario.idUsuario,
        })

        .getMany();

      let docRolesAntiguos: DocsRolesUsuarioEntity[] = [];

      rolesAntiguos.forEach(docRoles => {
        if (docRoles.roles.docsRolesUsuario) {
          docRoles.roles.docsRolesUsuario.forEach(documento => {
            docRolesAntiguos.push(documento);
          });
        }
      });

      // Guardamos el Usuario con sus nuevos datos en BBDD
      await queryRunner.manager.update(User, usuario.idUsuario, {
        Nombres: userDTO.nombre,
        ApellidoPaterno: userDTO.apellidoPaterno ? userDTO.apellidoPaterno : '',
        ApellidoMaterno: userDTO.apellidoMaterno ? userDTO.apellidoMaterno : '',
        FechaNacimiento: new Date(userDTO.fechaNacimiento),
        fechaDefuncion: userDTO.fechaDefuncion ? new Date(userDTO.fechaDefuncion) : null,
        idOpcionSexo: userDTO.idSexo,
        idOpcionNacionalidad: userDTO.idNacionalidad,
        idOpcionEstadoCivil: userDTO.idEstadoCivil,
        idComuna: userDTO.idComuna,
        idNivel: userDTO.idNivel,
        profesion: userDTO.profesion,
        diplomatico: userDTO.diplomatico,
        discapacidad: userDTO.discapacidad,
        indiqueDiscapacidad: userDTO.indiqueDiscapacidad,
        email: userDTO.email,
        Telefono: userDTO.telefono,
        DirCalle: userDTO.dirCalle,
        DirNro: userDTO.dirNumero,
        DirLetra: userDTO.dirLetra,
        dirResto: userDTO.dirResto,
        update_at: new Date(),
      });

      // Creamos el RegistroUsuario para ActualizarUsuario
      let registroUsuario: RegistroUsuarios = new RegistroUsuarios();
      registroUsuario.IdTipoRegistroUsuario = tipoRegistroUsuario.idTipoResgistroUsuario;
      registroUsuario.IdIngresadoPor = rolesUsuarioConectado[0].idUsuario;
      registroUsuario.IdAprobadoPor = rolesUsuarioConectado[0].idUsuario;
      registroUsuario.idUsuario = usuario.idUsuario;
      registroUsuario.Aprobado = usuario.Activo;
      registroUsuario.Fecha = new Date();

      // Guardamos el Registro en BBDD
      await queryRunner.manager.save(RegistroUsuarios, registroUsuario);
      if (userDTO.estado === false) {
        let solicitudAltaUsuario: SolicitudAltaUsuarioEntity = await this.solicitudAltaUsuarioEntityRepository.findOne({
          where: { idUsuario: userDTO.idUsuario },
        });
        if (solicitudAltaUsuario === undefined) {
          solicitudAltaUsuario = new SolicitudAltaUsuarioEntity();
          solicitudAltaUsuario.idUsuario = userDTO.idUsuario;
          solicitudAltaUsuario.motivoRechazo = userDTO.motivoRechazo;
          solicitudAltaUsuario.idSolicitante = rolesUsuarioConectado[0].idUsuario;
          solicitudAltaUsuario.idAprobador = rolesUsuarioConectado[0].idUsuario;
          solicitudAltaUsuario.created_at = new Date();
          solicitudAltaUsuario.updated_at = new Date();
        } else {
          solicitudAltaUsuario.motivoRechazo = userDTO.motivoRechazo;
          solicitudAltaUsuario.updated_at = new Date();
        }

        await this.solicitudAltaUsuarioEntityRepository.save(solicitudAltaUsuario);
      }

      //// ROLES
      // Se comprueba si nos han pasado roles desde el Front
      if (userDTO.docRoles && userDTO.docRoles.length > 0) {
        // Iteramos cada Rol para Editarlo, Eliminarlo o Añadirlo según el caso
        for (const nuevoRol of userDTO.docRoles.filter(x => x.editable == true)) {
          // Obtenemos el id de la Region del Rol
          let idRegionRol = nuevoRol.idRegion ? nuevoRol.idRegion : null;

          // Si no tiene idRegion, obtenemos la Region mediante el id de la Oficina
          let nuevoRolAsignarBBDD = rolesNuevosAsignar.filter(x => x.idRol == nuevoRol.idRol)[0];
          if (idRegionRol == null && nuevoRolAsignarBBDD.tipoRol != 1 && nuevoRolAsignarBBDD.tipoRol != 4) {
            // si es distinto de 1 (superusuario) o 4 genérico
            let idOficina = nuevoRol.idOficina ? nuevoRol.idOficina : null;
            let oficina: OficinasEntity = await queryRunner.manager.findOne(OficinasEntity, {
              relations: ['comunas'],
              where: { idOficina: idOficina },
            });
            if (oficina) idRegionRol = oficina.comunas.idRegion;
            else {
              resultado.Error = 'Error. No se encuentra la Region u oficina del Rol';
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              return resultado;
            }
          }

            // Si el Rol ya existe en los rolesAntiguos del Usuario lo guardamos como rolAntiguo
            let rolAntiguo = rolesAntiguos.find(x => x.idRol == nuevoRol.idRol);

            if (rolAntiguo) {
              await queryRunner.manager.update(RolesUsuarios, rolAntiguo.idRolesUsuario, {
                idRol: rolAntiguo.idRol,
                idUsuario: rolAntiguo.idUsuario,
                activo: userDTO.estado, ///// CAMBIAR EL ESTADO AL QUE NOS PASAN DEL FRONT
                idOficina: nuevoRol.idOficina,
                idRegion: nuevoRol.idRegion,
                idCalidadJuridica: nuevoRol.idCalidadJuridica,
                idEstamento: nuevoRol.idEstamento,
                idGrado: nuevoRol.idGrado,
              });
            } else {
              // Si no existe el nuevo Rol lo añadimos
              let nuevoRolUsuario: RolesUsuarios = new RolesUsuarios();
              nuevoRolUsuario.idRol = nuevoRol.idRol;
              nuevoRolUsuario.idUsuario = usuario.idUsuario;
              nuevoRolUsuario.idOficina = nuevoRol.idOficina;
              nuevoRolUsuario.idRegion = nuevoRol.idRegion;
              nuevoRolUsuario.idCalidadJuridica = nuevoRol.idCalidadJuridica;
              nuevoRolUsuario.idEstamento = nuevoRol.idEstamento;
              nuevoRolUsuario.idGrado = nuevoRol.idGrado;
              nuevoRolUsuario.activo = userDTO.estado; ///// CAMBIAR EL ESTADO AL QUE NOS PASAN DEL FRONT
              await queryRunner.manager.insert(RolesUsuarios, nuevoRolUsuario);
            }


            // Iteramos cada DocRol
            for (const docRolnuevo of nuevoRol.documentos) {
              let docRolAntiguo = docRolesAntiguos.find(x => x.idDocsRolesUsuario == docRolnuevo.idDocsRolesUsuario);

              if (docRolAntiguo) {
                // Si existe el documento, actualizamos su nombre y DocRequerido
                await queryRunner.manager.update(DocsRolesUsuarioEntity, docRolAntiguo.idDocsRolesUsuario, {
                  nombreDoc: docRolnuevo.nombreDoc,
                  idDocsRequeridoRol: docRolnuevo.idDocsRequeridoRol,
                });
              } else {
                // Comprobamos que el DocRol no sea null, y que sea de uno de los tipos esperados
                if (
                  docRolnuevo.archivo != '' &&
                  docRolnuevo.archivo != null &&
                  (docRolnuevo.archivo.includes('text/plain') ||
                    docRolnuevo.archivo.includes('application/pdf') ||
                    docRolnuevo.archivo.includes('image/jpeg') ||
                    docRolnuevo.archivo.includes('image/png'))
                ) {
                  // Si no existe el documento, lo creamos

                  // Si el nombre de fichero viene vacío lo indicamos
                  if (docRolnuevo.nombreDoc == '') {
                    resultado.Error = 'Error: uno de los archivos a subir está vacío.';
                    await queryRunner.rollbackTransaction();
                    await queryRunner.release();
                    return resultado;
                  }

                  let docRolUsuario: DocsRolesUsuarioEntity = new DocsRolesUsuarioEntity();
                  docRolUsuario.idRol = nuevoRol.idRol;
                  docRolUsuario.idUsuario = usuario.idUsuario;
                  docRolUsuario.created_at = new Date();
                  docRolUsuario.nombreDoc = docRolnuevo.nombreDoc;
                  docRolUsuario.archivo = Buffer.from(docRolnuevo.archivo);
                  docRolUsuario.idDocsRequeridoRol = docRolnuevo.idDocsRequeridoRol;

                  // Guardamos los datos en la tabla DocsRolesUsuario en BBDD
                  await queryRunner.manager.insert(DocsRolesUsuarioEntity, docRolUsuario);
                } else {
                  resultado.Error = 'Error: el archivo subido no tiene el formato esperado';
                  await queryRunner.rollbackTransaction();
                  await queryRunner.release();
                  return resultado;
                }
              }
            }
        }

        // Borramos los Roles antiguos del Usuario
        let idsRolesABorrar: number[] = [];
        for (const rolAntiguo of rolesAntiguos) {
          if (!userDTO.docRoles.find(x => x.idRol == rolAntiguo.idRol)) {
            idsRolesABorrar.push(rolAntiguo.idRol);
          }
        }
        if (idsRolesABorrar.length > 0) {
          await queryRunner.manager.delete(RolesUsuarios, { idRol: In(idsRolesABorrar), idUsuario: usuario.idUsuario });
        }

        // Borramos los DocRoles antiguos del Usuario
        let idsDocsABorrar: number[] = [];
        for (const docRolAntiguo of docRolesAntiguos) {
          if (!userDTO.docRoles.find(x => x.documentos.find(x => x.idDocsRolesUsuario == docRolAntiguo.idDocsRolesUsuario))) {
            idsDocsABorrar.push(docRolAntiguo.idDocsRolesUsuario);
          }
        }
        if (idsDocsABorrar.length > 0) {
          await queryRunner.manager.delete(DocsRolesUsuarioEntity, {
            idDocsRolesUsuario: In(idsDocsABorrar),
            idUsuario: usuario.idUsuario,
          });
        }
      } else {
        // Si no se han pasado Roles

        // Borramos los Roles para los que tenemos permisos
        let idsRolesABorrar: number[] = [];

        for (const rolAntiguo of rolesAntiguos) {

          idsRolesABorrar.push(rolAntiguo.idRol);

        }

        if (idsRolesABorrar.length > 0) {
          await queryRunner.manager.delete(RolesUsuarios, { idRol: In(idsRolesABorrar), idUsuario: usuario.idUsuario });
        }

        // Borramos los DocRoles para los que tenemos permisos
        let idsDocsABorrar: number[] = [];
        for (const docRolAntiguo of docRolesAntiguos) {

          idsDocsABorrar.push(docRolAntiguo.idDocsRolesUsuario);

        }

        if (idsDocsABorrar.length > 0) {
          await queryRunner.manager.delete(DocsRolesUsuarioEntity, {
            idDocsRolesUsuario: In(idsDocsABorrar),
            idUsuario: usuario.idUsuario,
          });
        }
      }

      // Comprobamos si el estado ha cambiado para en cada caso Activar, Desactivar o Rechazar
      if (usuario.Activo != userDTO.estado) {
        let CambiarEstadoUsuarioDto : CambiarEstadoUsuarioDto = { RUN: userDTO.run, motivo: userDTO.motivoRechazo, emailUsuarioActivado: usuario.email };

        let usuarioActualizado;

        

        // Si se activa el usuario, habría que recoger los roles que tengan el flag recibeNotificacionActivacionUsuario a true
        // para enviar los emails a estos (sólo para casos de roles regionales o de oficina)
        const tipoRolesOficinaRegion : number[] = [TipoRolesIds.Oficina];
        
        // Recogemos los roles asociados al usuario activado que sean de tipo oficina

        const qbRolesUsuario = this.rolesUsuariosRepository
        .createQueryBuilder('RolesUsuario')
        .innerJoinAndMapOne('RolesUsuario.roles', Roles, 'roles', 'RolesUsuario.idRol = roles.idRol')
        .leftJoinAndMapOne('RolesUsuario.oficinas', OficinasEntity, 'oficinas', 'RolesUsuario.idOficina = oficinas.idOficina')
        .leftJoinAndMapOne('oficinas.comunas', ComunasEntity, 'comunas', 'oficinas.idComuna = comunas.idComuna')

        qbRolesUsuario.andWhere('RolesUsuario.idUsuario = :_idUsuario', {_idUsuario:userDTO.idUsuario});
        qbRolesUsuario.andWhere('roles.tipoRol IN (:..._idsTipoRol)', {_idsTipoRol:tipoRolesOficinaRegion});

        const rolesUsuarioResult = await qbRolesUsuario.getMany();

        // Recogemos todos los ids de oficinas asociadas al usuario
        const oficinasAsociadas : number [] = this.authService.getOficinasUser(rolesUsuarioResult);
        
        let emailResponsables : string[] = [];
        let nombresCompletosResponsables : string[] = []; 

        // if(oficinasAsociadas.length < 1){
          
        //   usuarioActualizado = await this.activarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner);
        // }
        if(oficinasAsociadas.length > 0){

            
            // Enviamos el email a los usuarios encargados de ser informados a estos roles a nivel de oficina
            const qbUsuarios = this.usersRepository
            .createQueryBuilder('Usuario')
            .innerJoinAndMapMany('Usuario.rolesUsuarios', RolesUsuarios, 'rolesUsuarios','Usuario.idUsuario = rolesUsuarios.idUsuario')
            .innerJoinAndMapOne('rolesUsuarios.roles', Roles, 'roles', 'rolesUsuarios.idRol = roles.idRol')

            qbUsuarios.andWhere('roles.tipoRol = :_tipoRolOficina', {_tipoRolOficina:TipoRolesIds.Oficina});
            qbUsuarios.andWhere('rolesUsuarios.idOficina IN (:..._idOficinasUsuarioRol)', {_idOficinasUsuarioRol:oficinasAsociadas});
            qbUsuarios.andWhere('roles.recibeNotificacionActivacionUsuario = true');

            let usuariosRegionalesResult = await qbUsuarios.getMany();

            if(usuariosRegionalesResult.length > 0){
              emailResponsables = usuariosRegionalesResult.filter(x => x.email != null).map(y => y.email);
              nombresCompletosResponsables = usuariosRegionalesResult.filter(x => x.Nombres != null && x.ApellidoPaterno != null && x.ApellidoMaterno != null)
                                                                                    .map(z => (z.Nombres + " " + z.ApellidoPaterno + " " + z.ApellidoMaterno));
              //usuarioActualizado = await this.activarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner);
            }
            // else{
            //   let emailResponsables : string[] = usuariosRegionalesResult.filter(x => x.email != null).map(y => y.email);
            //   let nombresCompletosResponsables : string[] = usuariosRegionalesResult.filter(x => x.Nombres != null && x.ApellidoPaterno != null && x.ApellidoMaterno != null)
            //                                                                         .map(z => (z.Nombres + " " + z.ApellidoPaterno + " " + z.ApellidoMaterno));

            //   // if(emailResponsables.length < 1){
            //   //   usuarioActualizado = await this.activarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner);
            //   // }
            //   // else{
            //   //   usuarioActualizado = await this.activarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner, emailResponsables.join(';'), nombresCompletosResponsables.join(','));
            //   // } 
            // } 
        }




        if (userDTO.estado == true){       
          if(emailResponsables.length < 1){
            usuarioActualizado = await this.activarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner);
          }
          else{
            usuarioActualizado = await this.activarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner, emailResponsables.join(';'), nombresCompletosResponsables.join(','));
          }           
        }
        if (userDTO.estado == false && userDTO.motivoRechazo != '' && userDTO.motivoRechazo != null) {
          //usuarioActualizado = await this.rechazarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner, emailResponsables.join(';'), nombresCompletosResponsables.join(','));

          if(emailResponsables.length < 1){
            usuarioActualizado = await this.rechazarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner);
          }
          else{
            usuarioActualizado = await this.rechazarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner, emailResponsables.join(';'), nombresCompletosResponsables.join(','));
          } 

        } else if (userDTO.estado == false || userDTO.estado == null) {
          //usuarioActualizado = await this.desactivarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner, emailResponsables.join(';'), nombresCompletosResponsables.join(','));

          if(emailResponsables.length < 1){
            usuarioActualizado = await this.desactivarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner);
          }
          else{
            usuarioActualizado = await this.desactivarUsuario(CambiarEstadoUsuarioDto, rolesUsuarioConectado, queryRunner, emailResponsables.join(';'), nombresCompletosResponsables.join(','));
          } 

        }

        if (!usuarioActualizado.ResultadoOperacion) {
          resultado.Error = usuarioActualizado.Error;
          await queryRunner.rollbackTransaction();
          await queryRunner.release();
          return resultado;
        }
      }

      resultado.ResultadoOperacion = true;
      resultado.Mensaje = 'Usuario editado correctamente';
      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      resultado.Error = 'Error editando el Usuario';
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }

    return resultado;
  }

  async update(idUsuario: number, data: Partial<CreateUserDto>) {
    await this.usersRepository.update({ idUsuario }, data);
    return await this.usersRepository.findOne({ idUsuario });
  }

  async updateUserLogin(idUsuario: number, data: Partial<CreateUserDto>) {
    await this.usersRepository
      .createQueryBuilder()
      .update(User)
      .set({ tokenCU: data.tokenCU, conectado: data.conectado })
      .where({ idUsuario })
      .execute();
    return { code: 200, message: 'success' };
  }


  async getUsuarioByRUN(RUN: number) {
    const user = await this.usersRepository.findOne({ where: { RUN } });
    if (!user) throw new NotFoundException('user dont exist');

    const userDto = new ResponseUserDto();

    const ConsultaComuna = await getConnection().query(`select comuna_usuario(${user.idUsuario})as resp`);
    let comuna = ConsultaComuna[0].resp.codRNC;

    userDto.Activo = user.Activo;
    userDto.ApellidoMaterno = user.ApellidoMaterno;
    userDto.ApellidoPaterno = user.ApellidoPaterno;
    userDto.DV = user.DV;
    userDto.DirCalle = user.DirCalle;
    userDto.DirLetra = user.DirLetra;
    userDto.DirNro = user.DirNro;
    userDto.Nombres = user.Nombres;
    userDto.RUN = user.RUN;
    userDto.idOpcionSexo = user.idOpcionSexo;
    userDto.conectado = user.conectado;
    userDto.created_at = user.created_at;
    userDto.email = user.email;
    userDto.idUsuario = user.idUsuario;
    userDto.modify_by = user.modify_by;
    userDto.update_at = user.update_at;
    userDto.Telefono = user.Telefono;
    userDto.FechaNacimiento = user.FechaNacimiento;
    userDto.comuna = comuna;

    return userDto;
  }

  async getUsuarioConRolesByRUN(req, RUN: string) {
    const resultado: Resultado = new Resultado();

    // Obtenemos de los Roles del UsuarioConectado: Regiones, Oficinas y si esSuperUsuario o Seremitt
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AdministracionSGLCAdministraciondeUsuariosVisualizarDetalledeUsuario);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }     

    try {
      const user = await this.usersRepository
        .createQueryBuilder('user')
        .leftJoinAndSelect('user.comunas', 'comunas')
        .leftJoinAndSelect('user.registroUsuarios', 'registroUsuario')
        .where("user.RUN || '-' || user.DV = :run", { run: RUN })
        .orderBy('registroUsuario.idRegistroUsuarios', 'DESC')
        .limit(1)
        .getOne();

      if (user == undefined) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'En estos momentos no podemos completar su acción. Intente nuevamente en unos minutos';
        return resultado;
      }

      // Obtenemos los Roles del Usuario si los hubiera
      const rolesUsuario = await this.rolesUsuariosRepository.find({
        relations: ['roles', 'user', 'oficinas', 'oficinas.comunas', 'regiones'],
        join: {
          alias: 'rolesUsuario',
          leftJoinAndSelect: {
            roles: 'rolesUsuario.roles',
            usuario: 'rolesUsuario.user',
            regiones: 'rolesUsuario.regiones',
            oficinas: 'rolesUsuario.oficinas',
            comunas: 'oficinas.comunas',
          },
        },
        where: qb => {
          qb.where('usuario.idUsuario = :id', { id: user.idUsuario });
        },
      });

      let docRolesUsuario: DocsRolesUsuarioEntity[] = [];

      if (Array.isArray(rolesUsuario) && rolesUsuario.length) {
        // Obtenemos los DocumentosRoles si los hubiera
        docRolesUsuario = await this.docsRolesUsuarioRepository.find({
          where: { idUsuario: user.idUsuario },
        });
      }

      const userDto = new DatosUsuarioDto();

      userDto.idUsuario = user.idUsuario;
      userDto.apellidoMaterno = user.ApellidoMaterno;
      userDto.apellidoPaterno = user.ApellidoPaterno;
      userDto.dirCalle = user.DirCalle;
      userDto.dirLetra = user.DirLetra;
      userDto.dirNumero = user.DirNro;
      userDto.dirResto = user.dirResto;
      userDto.nombre = user.Nombres;
      userDto.run = user.RUN + '-' + user.DV;
      userDto.idSexo = user.idOpcionSexo;
      userDto.idNacionalidad = user.idOpcionNacionalidad;
      userDto.idEstadoCivil = user.idOpcionEstadoCivil;
      userDto.idComuna = user.idComuna;
      userDto.idRegion = user.comunas.idRegion;
      userDto.email = user.email;
      userDto.telefono = user.Telefono;
      userDto.password = user.passwd;
      userDto.idNivel = user.idNivel;
      userDto.profesion = user.profesion;
      userDto.diplomatico = user.diplomatico;
      userDto.discapacidad = user.discapacidad;
      userDto.indiqueDiscapacidad = user.indiqueDiscapacidad;
      userDto.estado = user.Activo;

      if (!user.Activo) {
        const solicitudAltaUsuario: SolicitudAltaUsuarioEntity = await this.solicitudAltaUsuarioEntityRepository.findOne({
          where: { idUsuario: userDto.idUsuario },
        });
        userDto.motivoRechazo = solicitudAltaUsuario?.motivoRechazo;
      }

      if (user.FechaNacimiento) {
        userDto.fechaNacimiento = user.FechaNacimiento;
      } else {
        userDto.fechaNacimiento = null;
      }
      if (user.fechaDefuncion) {
        userDto.fechaDefuncion = user.fechaDefuncion;
      } else {
        userDto.fechaDefuncion = null;
      }

      userDto.docRoles = [];

      rolesUsuario.forEach(rolUsuario => {
        const rolesUsuarioDto = new datosRolesUsuarioDto();

        rolesUsuarioDto.NombreRol = rolUsuario.roles.Nombre;
        rolesUsuarioDto.DescripcionRol = rolUsuario.roles.Descripcion;
        rolesUsuarioDto.idEstamento = rolUsuario.idEstamento;
        rolesUsuarioDto.idGrado = rolUsuario.idGrado;
        rolesUsuarioDto.idCalidadJuridica = rolUsuario.idCalidadJuridica;
        rolesUsuarioDto.idOficina = rolUsuario.idOficina;
        rolesUsuarioDto.nombreOficina = rolUsuario.oficinas ? rolUsuario.oficinas.Nombre : null;
        rolesUsuarioDto.idRegion = rolUsuario.idRegion;
        rolesUsuarioDto.nombreRegion = rolUsuario.regiones ? rolUsuario.regiones.Nombre : null;
        rolesUsuarioDto.idRol = rolUsuario.idRol;
        rolesUsuarioDto.editable = true;


        if (docRolesUsuario && docRolesUsuario.length > 0) {
          rolesUsuarioDto.documentos = [];

          let docsParaEsteRol = docRolesUsuario.filter(x => x.idRol == rolUsuario.idRol);

          docsParaEsteRol.forEach(docRol => {
            const docsRolesUsuarioDto = new datosDocsRolesUsuarioDTO();
            docsRolesUsuarioDto.idDocsRolesUsuario = docRol.idDocsRolesUsuario;
            docsRolesUsuarioDto.idUsuario = docRol.idUsuario;
            docsRolesUsuarioDto.idRol = docRol.idRol;
            docsRolesUsuarioDto.archivo = docRol.archivo?.toString();
            docsRolesUsuarioDto.nombreDoc = docRol.nombreDoc;
            docsRolesUsuarioDto.created_at = docRol.created_at;
            docsRolesUsuarioDto.idDocsRequeridoRol = docRol.idDocsRequeridoRol;
            rolesUsuarioDto.documentos.push(docsRolesUsuarioDto);
          });
        }

        userDto.docRoles.push(rolesUsuarioDto);
      });

      resultado.Respuesta = userDto;
      resultado.ResultadoOperacion = true;
    } catch (error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'En estos momentos no podemos completar su acción. Intente nuevamente en unos minutos';
    }

    return resultado;
  }

  async activarUsuario(EstadoUsuario: CambiarEstadoUsuarioDto, rolesUsuarioConectado: RolesUsuarios[], transaccion?, emailResponsables?: string, nombresResponsables?: string) {
    const resultado: Resultado = new Resultado();

    const tipoRegistroUsuario = await this.tipoRegistroUsuariosRepository.findOne({ Nombre: registroUsuarioActivacion });
    if (tipoRegistroUsuario == undefined) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error buscando estado Activo';
      return resultado;
    }

    const resutladoCambioEstado = await this.cambiarEstadoUsuario(
      true,
      EstadoUsuario.RUN,
      tipoRegistroUsuario,
      rolesUsuarioConectado,
      '',
      transaccion,
      EstadoUsuario.emailUsuarioActivado,
      emailResponsables,
      nombresResponsables
    );
    return resutladoCambioEstado;
  }

  async desactivarUsuario(EstadoUsuario: CambiarEstadoUsuarioDto, rolesUsuarioConectado: RolesUsuarios[], transaccion?, emailResponsables?: string, nombresResponsables?: string) {
    const resultado: Resultado = new Resultado();

    const tipoRegistroUsuario = await this.tipoRegistroUsuariosRepository.findOne({ Nombre: registroUsuarioDesactivacion });
    if (tipoRegistroUsuario == undefined) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error buscando estado Inactivo';
      return resultado;
    }

    const resutladoCambioEstado = await this.cambiarEstadoUsuario(
      false, 
      EstadoUsuario.RUN,
      tipoRegistroUsuario,
      rolesUsuarioConectado,
      EstadoUsuario.motivo,
      transaccion,
      EstadoUsuario.emailUsuarioActivado,
      emailResponsables,
      nombresResponsables
    );
    return resutladoCambioEstado;
  }

  async rechazarUsuario(EstadoUsuario: CambiarEstadoUsuarioDto, rolesUsuarioConectado: RolesUsuarios[], transaccion?, emailResponsables?: string, nombresResponsables?: string) {
    let resultado: Resultado = new Resultado();

    const tipoRegistroUsuario = await this.tipoRegistroUsuariosRepository.findOne({ Nombre: registroUsuarioRechazo });
    if (tipoRegistroUsuario == undefined) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error buscando estado Rechazado';
      return resultado;
    }

    const resutladoCambioEstado = await this.cambiarEstadoUsuario(
      false,
      EstadoUsuario.RUN,
      tipoRegistroUsuario,
      rolesUsuarioConectado,
      EstadoUsuario.motivo,
      transaccion,
      EstadoUsuario.emailUsuarioActivado,
      emailResponsables,
      nombresResponsables
    );

    return resutladoCambioEstado;
  }

  async cambiarEstadoUsuario(
    tipoCambioEstadoActivar:boolean,
    RUN: string,
    tipoRegistro: TipoResgistroUsuario,
    rolesUsuarioConectado: RolesUsuarios[],
    motivo: string,
    transaccion?,
    emailUsuarioActivado?,
    emailResponsables?,
    nombresResponsables?
  ) {
    const resultado: Resultado = new Resultado();
    const run = RUN.split('-')[0];
    const dv = RUN.split('-')[1];

    let queryRunner;

    // Se obtiene conexion para empezar una Transaccion
    if (transaccion) {
      queryRunner = transaccion;
    } else {
      const connection = getConnection();
      queryRunner = connection.createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();
    }

    try {
      const user = await this.usersRepository.findOne({ where: { RUN: run, DV: dv } });

      if (user == undefined) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error obteniendo el usuario';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      let resultadoUpdate;
      let asuntoCorreo;
      let estadoUsuario;
      let aprobado = false;

      // Segun el tipo de Registro, se actualiza el campo Activo del usuario y se coge el asunto para el email
      if (tipoRegistro.Nombre == registroUsuarioRechazo) {
        resultadoUpdate = await queryRunner.manager.update(User, user.idUsuario, { Activo: false });
        estadoUsuario = 'Estado: Rechazado';
        asuntoCorreo = asuntoUsuarioRechazado;
      } else if (tipoRegistro.Nombre == registroUsuarioDesactivacion) {
        resultadoUpdate = await queryRunner.manager.update(User, user.idUsuario, { Activo: false });
        estadoUsuario = 'Estado: Desactivado';
        asuntoCorreo = asuntoUsuarioDesactivado;
      } else {
        resultadoUpdate = await queryRunner.manager.update(User, user.idUsuario, { Activo: true });
        estadoUsuario = 'Estado: Activado';
        asuntoCorreo = asuntoUsuarioActivado;

        aprobado = true;
      }

      if (resultadoUpdate.affected == 0) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error activando o desactivando usuario';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      let usuarioConectado: User;
      // Se coje el usuario conectado, asi tendremos el id del usuario que realiza la gestion
      usuarioConectado = await this.usersRepository.findOne({ idUsuario: rolesUsuarioConectado[0].idUsuario });

      if (usuarioConectado == undefined) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error: obteniendo el usuario conectado';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      // Se crea el objeto Registro Usuario
      const usuarioHistorico = new RegistroUsuarios();
      usuarioHistorico.Fecha = new Date();
      usuarioHistorico.IdTipoRegistroUsuario = tipoRegistro.idTipoResgistroUsuario;
      usuarioHistorico.idUsuario = user.idUsuario;
      usuarioHistorico.Aprobado = aprobado;
      usuarioHistorico.IdAprobadoPor = usuarioConectado.idUsuario;
      usuarioHistorico.Observacion = motivo;

      const resultadoHitorico = await queryRunner.manager.insert(RegistroUsuarios, usuarioHistorico);

      if (resultadoHitorico.identifiers > 0) {
        resultado.ResultadoOperacion = false;
        resultado.Error = 'Error: problema registrando en el historico de usuario';
        await queryRunner.rollbackTransaction();
        await queryRunner.release();
        return resultado;
      }

      const userName =
        user.RUN + '-' + user.DV + ', ' + user.Nombres + ' ' + user.ApellidoPaterno + ' ' + user.ApellidoMaterno + ', ' + estadoUsuario; /// RUN + nombre + Estado activacion

      let destinatario = '';

      // Si existe algún responsable, se envía el correo de activación
      if(emailResponsables && emailResponsables != null){
        destinatario = emailResponsables; 

        // Se recupera la plantilla
        let cuerpoCorreo = plantillaCorreo;

        if(tipoCambioEstadoActivar == true){
          // Se llama al servicio que envia el correo
          let respuestaCorreo = await this.utilService.prepararObjetoCorreo(
            destinatario,
            asuntoCorreo,
            cuerpoCorreo,
            'Estimado(a)/s ' + nombresResponsables + ',',
            'Junto con saludar, comunicamos que el usuario ' + user.Nombres + ' ' + user.ApellidoPaterno + ' ' + user.ApellidoMaterno + ', RUN ' + user.RUN + '-' + user.DV + 
            'ha cambiado su estado a ACTIVO<br/><br/>',
            '',
            'Saludos cordiales, <br/><br/>'
          );
        }
        else{
          // Se llama al servicio que envia el correo
          let respuestaCorreo = await this.utilService.prepararObjetoCorreo(
            destinatario,
            asuntoCorreo,
            cuerpoCorreo,
            'Estimado(a)/s ' + nombresResponsables + ',',
            'Junto con saludar, comunicamos que el usuario ' + user.Nombres + ' ' + user.ApellidoPaterno + ' ' + user.ApellidoMaterno + ', RUN ' + user.RUN + '-' + user.DV + 
            'ha cambiado su estado a Inactivo/Rechazado por el motivo: ' + motivo + ' <br/><br/>',
            '',
            'Saludos cordiales, <br/><br/>'
          );          
        }
      }

      if(tipoRegistro.Nombre != registroUsuarioRechazo && tipoRegistro.Nombre != registroUsuarioDesactivacion){
        if(emailUsuarioActivado && emailUsuarioActivado != null && tipoCambioEstadoActivar == true){

          // Se recupera la plantilla
          let cuerpoCorreo = plantillaCorreo;

          // Se llama al servicio que envia el correo
          let respuestaCorreo = await this.utilService.prepararObjetoCorreo(
            emailUsuarioActivado,
            'Su usuario ha sido activado',
            cuerpoCorreo,
            'Estimado(a),',
            'A nuestra consideración, se informa la activación de su usuario en la plataforma. ',
            'Cualquier duda o comentario, no dude en comunicarse con nosotros, <br/><br/>',
            'Saludos Cordiales. '
          );

        }
      }

      resultado.Mensaje = 'El usuario ha cambiado de estado correctamente';
      resultado.ResultadoOperacion = true;

      //Si la transaccion no viene pasada por parametros desde otro endpoint, se finaliza aqui
      if (transaccion == undefined) await queryRunner.commitTransaction();

    } catch (error) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'Error cambiando estado del usuario';
      await queryRunner.rollbackTransaction();
    } finally {
      if (transaccion == undefined) await queryRunner.release();
    }
    return resultado;
  }

  async updateUserPerfilDeUsuario(req:any, RUN: number, email: string, Telefono: string) {
    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.PerfildeUsuarioDatosdelUsuarioModificarDatosdelUsuario);
    
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	 

    return this.updateUser(RUN, email, Telefono);
  }

  async updateUserAdminUsuario(req:any, RUN: number, email: string, Telefono: string) {
    // let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.);
    
    // // En caso de que el usuario no sea correcto
    // if (!usuarioValidado.ResultadoOperacion) {
    //   return usuarioValidado;
    // }	 

    return this.updateUser(RUN, email, Telefono);
  }

  private async updateUser(RUN: number, email: string, Telefono: string) {
    const regex = new RegExp(/^[+]\d{2} \d{9}$/);
    if (regex.test(Telefono)) {
      await this.usersRepository.createQueryBuilder().update(User).set({ email: email, Telefono: Telefono }).where({ RUN }).execute();
      return { code: 200, message: 'success' };
    } else {
      return { code: 400, error: 'Formato de Teléfono inválido' };
    }
  }

  async sendMailResetPassword(email: string) {
    const user = await this.getUserLoginEmail(email);
    if (!user) throw new NotFoundException('user dont exist the email');
    const payload = { username: user.RUN, sub: user.idUsuario };
    const access_token = this.jwtService.sign(payload);

    const EmailAccount = await getRepository(ParametrosGeneralEntity)
      .createQueryBuilder('ParametrosGeneral')
      .where("ParametrosGeneral.Param = 'correoComunicadosOficiales'")
      .getOne();
    const TextoHTMLparaEmailResetPass = await getRepository(ParametrosGeneralEntity)
      .createQueryBuilder('ParametrosGeneral')
      .where("ParametrosGeneral.Param = 'TextoHTMLparaEmailResetPass'")
      .getOne();
    const subjectEmailResetPass = await getRepository(ParametrosGeneralEntity)
      .createQueryBuilder('ParametrosGeneral')
      .where("ParametrosGeneral.Param = 'subjectEmailResetPass'")
      .getOne();
    const linkResetPassword = await getRepository(ParametrosGeneralEntity)
      .createQueryBuilder('ParametrosGeneral')
      .where("ParametrosGeneral.Param = 'linkResetPassword'")
      .getOne();

    const link = "<a href='" + linkResetPassword.Value + '?token=' + access_token + "'>aquí</a>";
    const textoHTML = TextoHTMLparaEmailResetPass.Value.replace('<link>', link);

    await getConnection()
      .createQueryBuilder()
      .update(User)
      .set({ TokenToResetPasswd: access_token })
      .where('idUsuario = :id', { id: user.idUsuario })
      .execute();

    this.sendMail(email, EmailAccount.Value, subjectEmailResetPass.Value, '', textoHTML);
    return { code: 200, message: 'success' };
  }

  async resetPassword(updatePasswordDto: UpdatePasswordDto) {
    const user = await this.getUserLoginToken(updatePasswordDto.TokenToResetPasswd);
    if (!user) throw new NotFoundException('user dont exist the TokenToResetPasswd');

    await getConnection()
      .createQueryBuilder()
      .update(User)
      .set({ passwd: updatePasswordDto.newPasswd, TokenToResetPasswd: '' })
      .where('idUsuario = :id', { id: user.idUsuario })
      .execute();

    return { code: 200, message: 'success' };
  }

  async getUserLoginToken(TokenToResetPasswd: string) {
    const user = await this.usersRepository.findOne({ TokenToResetPasswd });
    if (!user) throw new NotFoundException('user dont exist the TokenToResetPasswd');
    return user;
  }

  async getUserLoginEmail(email: string) {
    const user = await this.usersRepository.findOne({ email });
    if (!user) throw new NotFoundException('user dont exist the email');
    return user;
  }

  public sendMail(email: string, from: string, subject: string, text: string, html: string): void {
    
    console.log('>>>>> Inputs >>  ' + 'email: ' + email + ' from: ' + from + ' subjet: ' + subject);
    
    this.mailerService.sendMail({
        to: email,
        from: from,
        subject: subject,
        text: text,
        html: html,
      })
      .then(success => {
        console.log('success');
        console.log(success);
      })
      .catch(err => {
        console.log('error');
        console.log(err);
      });
  }

  public async recogerActivadores(rolUsuario: RolesUsuarios, rolesUsuarioConectado: RolesUsuarios[]): Promise<Resultado> {
    const resultado: Resultado = new Resultado();
    let idRegion : number;

    // Si el rolUsuario no tiene idRegion, lo cogemos de la region
    if (rolUsuario.idRegion == undefined || rolUsuario.idRegion == null) {
      idRegion = rolUsuario.regiones.idRegion;
    } else {
      idRegion = rolUsuario.idRegion;
    }

    const rolesCreador: Roles[] = [];

    // Se recogen los roles del usuario conectado, para mas adelante coger el Cargo de dicho usuario
    rolesUsuarioConectado.forEach(rolUserConectado => {
      if (
        (rolUserConectado.roles.tipoRoles.Nombre == tipoRolRegion && rolUserConectado.idRegion == idRegion) ||
        (rolUserConectado.roles.tipoRoles.Nombre == tipoRolOficina && rolUserConectado.idOficina == rolUsuario.idOficina) ||
        rolUserConectado.roles.tipoRoles.idTipoRol == tipoRolSuperUsuarioID
      )
        // Coger tambien el del Superusuario?
        rolesCreador.push(rolUserConectado.roles);
    });

    // Se ordenan para coger el rol con el mayor Cargo
    rolesCreador.sort(function (a, b) {
      return a.tipoRol - b.tipoRol;
    });

    if (rolesCreador.length == 0) {
      resultado.ResultadoOperacion = false;
      resultado.Error = 'No se encontro rol para el usuario conectado';
      return resultado;
    }

    // Se recoge el roles activadores, seremitt y de la misma oficina
    const rolesConActivadores = await this.rolesRepository.find({
      relations: [
        'rolesUsuarios',
        'rolesUsuarios.user',
      ],
      where: qb => {
        qb.where('Roles.tipoRol = :_idTipoRol', { _idTipoRol: tipoRolRegionID })
        .andWhere(
          'Roles__rolesUsuarios.idRegion = :_idRegion', { _idRegion: idRegion}
        );
      },
    });

    let rolesUsuarioActivadores: RolesUsuarios[] = [];

    if(rolesConActivadores && rolesConActivadores.length > 0){
      //rolesUsuarioActivadores = rolesConActivadores.map(x => x.rolesUsuarios);
      rolesConActivadores.forEach(x => {
        rolesUsuarioActivadores = rolesUsuarioActivadores.concat(x.rolesUsuarios);
      });
    }

    resultado.ResultadoOperacion = true;
    resultado.Respuesta = rolesUsuarioActivadores;

    return resultado;
  }

  async envioCorreoActivadores(rolesActivadores: RolesUsuarios[], userName: string, directorName: string) {
    let idActivador = 0;
    let Zona = '';


      //   cuerpoCorreo = plantillaCorreo.replace('cuerpoCorreo', cuerpoCorreo);
        let emailDestinatarios : string = rolesActivadores.map(x => x.user.email).join(';');
        let nombresDestinatarios : string = rolesActivadores.map(x => x.user.Nombres + ' ' + x.user.ApellidoPaterno + ' ' + x.user.ApellidoMaterno).join(',');

        //const destinatario = rolesActivadores[i].user.email;

        // Se llama al servicio que envia el correo
        // Se recupera la plantilla
        let cuerpoCorreo = plantillaCorreo;

        // Se llama al servicio que envia el correo
        let respuestaCorreo = await this.utilService.prepararObjetoCorreo(
          emailDestinatarios,
          asuntoUsuarioPendienteActivacion,
          cuerpoCorreo,
          'Estimado(a)/s ' + nombresDestinatarios + ',',
          'A nuestra consideración, se informa que los siguientes usuarios están pendientes de activar: ',
          userName + '<br/><br/>',
          'Cualquier duda o comentario, no dude en comunicarse con nosotros, <br/><br/>' +
          'Saludos Cordiales. '
        );

  }

  async usuarioCompleto(req:any, idUsuario: number) {

    let usuarioValidado = await this.authService.checkUserAndRolAndReturnRolesFromPermission(req, PermisosNombres.AccesoDatosPersonalesDelUsuario);
  
    // En caso de que el usuario no sea correcto
    if (!usuarioValidado.ResultadoOperacion) {
      return usuarioValidado;
    }	  

    const relations = ['opcionSexo', 'opcionNacionalidad', 'opcionEstadoCivil', 'comunas'];
    const usuario = await this.usersRepository.findOne(idUsuario, { relations });

    delete usuario.passwd;
    delete usuario.tokenCU;
    delete usuario.TokenToResetPasswd;
    delete usuario.LastUserConnection;

    return usuario;
  }

  public async changePassword(idUsuario: number, changePasswordDTO: ChangePasswordDTO): Promise<Resultado> {
    const resultado: Resultado = new Resultado();

    try {
      const { password, newPassword } = changePasswordDTO;
      const user: User = await this.usersRepository
        .createQueryBuilder()
        .where({ idUsuario })
        .getOneOrFail()
        .then((res: User) => res)
        .catch((err: Error) => {
          // * Modificamos resultado para errores
          this.setResultadoService.setError(resultado, 'No se ha podido encontrar el usuario');
          return null;
        });

      const encriptUserPass = CryptoJS.AES.decrypt(user.passwd, SECRET_KEY_FRONT).toString(CryptoJS.enc.Utf8);
      const encriptUserNewPass = CryptoJS.AES.decrypt(password, SECRET_KEY_FRONT).toString(CryptoJS.enc.Utf8);

      if (user && encriptUserPass === encriptUserNewPass) {
        await this.usersRepository
          .createQueryBuilder()
          .update()
          .set({ passwd: newPassword })
          .where({ idUsuario })
          .execute()
          .then((res: UpdateResult) => {
            if (res.affected > 0) {
              // * Modificamos resultado para respuesta
              this.setResultadoService.setResponse(resultado, 'Se ha modificado la respuesta correctamente');
            } else {
              // * Modificamos resultado para errores
              this.setResultadoService.setError(resultado, 'No se ha modificado ninguna contraseña');
            }
          })
          .catch((err: Error) => {
            // * Modificamos resultado para errores
            this.setResultadoService.setError(resultado, 'No se ha podido cambiar la contraseña');
          });
      } else {
        // * Modificamos resultado para errores
        this.setResultadoService.setError(resultado, 'La contraseña facilitada no es la correcta');
      }
    } catch (error) {
      // * Modificamos resultado para errores
      this.setResultadoService.setError(resultado, 'No se ha podido cambiar la contraseña');
    }

    return resultado;
  }
}
