import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './controller/users.controller';
import { User } from './entity/user.entity';
import { UsersService } from './services/users.service';
import { JwtStrategy } from './jwt.strategy';
import { RolesUsuarios } from 'src/roles-usuarios/entity/roles-usuarios.entity';
import { DocsRolesUsuarioEntity } from 'src/documentos-usuarios/entity/DocsRolesUsuario.entity';
import { AuthService } from 'src/auth/auth.service';
import { UtilService } from 'src/utils/utils.service';
import { RegistroUsuarios } from 'src/registro-usuarios/entity/registro-usuario.entity';
import { TipoResgistroUsuario } from 'src/tipo-resgistro-usuario/entity/tipo-resgistro-usuario.entity';
import { Roles } from 'src/roles/entity/roles.entity';
import { OficinasEntity } from 'src/tramites/entity/oficinas.entity';
import { RolActivaRol } from 'src/rol-activa-rol/entity/rol-activa-rol.entity';
import { RegionesEntity } from 'src/escuela-conductores/entity/regiones.entity';
import { RolAsignaRol } from 'src/rol-asigna-rol/entoty/rol-asigna-rol.entity';
import { SharedModule } from 'src/shared/shared.module';
import { SolicitudAltaUsuarioEntity } from '../solicitudes-alta-usuarios/solicitudAltaUsuario.entity';

@Module({
  imports: [
    SharedModule,
    TypeOrmModule.forFeature([
      User,
      Roles,
      RolesUsuarios,
      RegionesEntity,
      OficinasEntity,
      DocsRolesUsuarioEntity,
      RegistroUsuarios,
      TipoResgistroUsuario,
      RolActivaRol,
      RolAsignaRol,
      SolicitudAltaUsuarioEntity
    ])
  ],
  controllers: [UsersController],
  providers: [UsersService, JwtStrategy, AuthService, UtilService],
  exports: [UsersService],
})
export class UsersModule {}
