import { EmailAttachment } from "./email-attachments-request.entity";

export class Email {
  from: string;
  to: string;
  subject: string;
  text: string;
  html: string;
  attachments: EmailAttachment[];

  constructor(from, to, subject, text, html, attachments) {
      this.from = from;
      this.to = to;
      this.subject = subject;
      this.text = text;
      this.html = html;
      this.attachments = attachments;
  }
}