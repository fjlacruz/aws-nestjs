export class OrdenPaginacion {

    orden: string;
    ordenarPor: SortBy;

}

export enum SortBy{
    ASC = 'ASC',
    DESC = 'DESC'
}