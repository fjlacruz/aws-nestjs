import { IPaginationOptions } from 'nestjs-typeorm-paginate';
import { OrdenPaginacion } from './OrdenPaginacion';

export class PaginacionArgs {
    orden: OrdenPaginacion;
    filtro: any;
    paginationOptions: IPaginationOptions;
    filtroBusqueda: string;
}
