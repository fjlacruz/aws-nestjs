import { IPaginationLinks, IPaginationMeta } from 'nestjs-typeorm-paginate/dist/interfaces';

/**
 * Clase responsable por unir objetos con información de paginación
 * en el caso de que no se necesite toda la información que viene
 * paginada.
 */
export class PaginacionDtoParser<PaginacionObject> {
    items: PaginacionObject[];
    meta: IPaginationMeta;
    links?: IPaginationLinks;
}
