export const correoUsuarioActivacion = '<p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Estimado(a) ActivadorName, </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">A nuestra consideración, se informa que tiene pendiente la activación del(los) siguientes usuarios: </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;"> userName </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Cualquier duda o comentario, no dude en comunicarse con nosotros, </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Saludos Cordiales. </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">DirectorName</p>';
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">CargoName</p>';
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">ZonaName</p>';

// export const correoUsuarioActivado = '<br /> Estimado(a) DestinatarioName: <br /> <br /> ' +
// 'A nuestra consideración, se informa el estado de activación de el(los) siguientes usuarios: <br /> <br /> ' +
// 'userName <br /> <br /> ' +
// 'Cualquier duda o comentario, no dude en comunicarse con nosotros, <br /> <br /> ' +
// 'Saludos Cordiales. <br /> <br /> ' +
// 'SeremittName <br /> <br />';

export const correoUsuarioActivado =  '<p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Estimado(a) DestinatarioName,</p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">A nuestra consideración, se informa el estado de activación de el(los) siguientes usuarios: </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;"> userName </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Cualquier duda o comentario, no dude en comunicarse con nosotros, </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Saludos Cordiales. </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">SeremittName</p>';

export const plantillaCorreo = '' +
    '<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background: #FFFFFF; font-family: Arial; font-size: 14px; color: #000000; padding: 0 ;margin: 0 auto; -webkit-text-size-adjust: 100%;">'+
'<style>'+
    'body {'+
        'background-color: #FFFFFF;'+
        'font-family: Arial;'+
        'font-size: 14px;'+
        'color: #000000;'+
        'margin: 0 auto;'+
        'padding: 0;'+
        '-webkit-text-size-adjust: 100%;'+
    '}'+

    '* {'+
        'line-height: inherit;'+
    '}'+

    'a {'+
        'color: #E22C2C;'+
        'text-decoration: underline;'+
        'border: 0;'+
    '}'+
    'a img, img {'+
     '   border: 0;'+
    '}'+
    'a:hover {'+
        'color: #CC0000;'+
        'text-decoration: underline;'+
    '}'+

    'a[x-apple-data-detectors=true] {'+
        'color: inherit !important;'+
        'text-decoration: none !important;'+
    '}'+
    ''+

    'table,'+
    'td,'+
    'tr {'+
      '    vertical-align: top;'+
      '  border-collapse: collapse;'+
      '}'+
      'table td {'+
        '  font-family: Arial;'+
        '   color: #000000;'+
        '  background: #FFFFFF;'+
        'font-size: 14px;'+
        '}'+
        'table td,'+
        'table td p {'+
          'line-height: 21px;'+
          '}'+
          'table td h2 {'+
            'font-family: Arial;'+
            'color: #000000;'+
            'text-align: left;'+
            'font-size: 16px;'+
            'margin-top: 0px;'+
            'margin-bottom: 20px;'+
            'line-height: 24px;'+
            '}'+
            'table td p {'+
              '        font-family: Arial;'+
              '        color: #424242;'+
              '        font-size: 14px;'+
              '        line-height: 21px;'+
              '       margin-top: 0px;'+
              '       margin-right: 50px;'+
              '       margin-bottom: 20px;'+
              '        margin-left: 50px;'+
              '    }'+
              '    table td strong,'+
              '    table td p strong {'+
                '        color: #000000 !important;'+
                '    }'+
                '    table td img {'+
                  '        float: left;'+
                  '       display: block;'+
                  '    }'+
                  '   table td img.logotipo {'+
                    '       float: none;'+
                    '       display: block;'+
                    '       width: 35%;'+
                    '       height: auto;'+
                    '       max-width: 180px;'+
                    '    }'+
                    '    table td a.boton {'+
                      '        display: block;'+
                      '       font-size: 16px;'+
                      '       background-color: #E22C2C;'+
                      '       color: #FFFFFF;'+
                      '       height: 25px;'+
                      '      padding: 15px 0;'+
                      '      vertical-aling: middle;'+
                      '      text-align: center;'+
                      '    }'+
                      '   table td .texto-legal {'+
                        '       font-family: Arial;'+
                        '       color: #98A3AF;'+
                        '      text-align: justify;'+
                        '      font-size: 11px;'+
                        '       margin-top: 0px;'+
                        '      margin-right: 50px;'+
                        '       margin-bottom: 0;'+
                        '       margin-left: 50px;'+
                        '      line-height: 24px;'+
                        '   }'+
'</style>'+
'<center>'+
'<table id="bodyTable" bgcolor="#ffffff" cellpadding="0" cellspacing="0" style="table-layout: fixed; vertical-align: top; width: 100%; min-width: 320px; max-width: 600px; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; border: 0;" valign="top" width="100%" border="0">'+
'       <tr>'+
'           <td align="justify" style="background: #FFFFFF; font-size: 14px;line-height: 21px;">'+
'               descripcionCabeceraCorreo@kreata.ee' +
'           </td>'+
'       </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="spacer-600x50@kreata.ee" width="100%" height="50" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'       <tr>'+
'           <td align="justify" style="background: #FFFFFF; font-size: 14px;line-height: 21px;">'+
'               <b>cabeceraCuerpoCorreo@kreata.ee</b>' +
'           </td>'+
'       </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="spacer-600x50@kreata.ee" width="100%" height="50" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'       <tr>'+
'           <td align="justify" style="background: #FFFFFF; font-size: 14px;line-height: 21px;">'+
'               saludoCuerpoCorreo@kreata.ee' +
'           </td>'+
'       </tr>'+
'       <tr>'+
'           <td align="justify" style="background: #FFFFFF; font-size: 14px;line-height: 21px;">'+
'               textoCuerpoCorreo@kreata.ee' +
'           </td>'+
'       </tr>'+
'       <tr>'+
'           <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="image-top@kreata.ee" width="100%" height="2" alt="" style="border: 0;float: left;display: block;"></td>'+
'       </tr>'+
'       <tr>'+
'           <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="spacer-600x50@kreata.ee" width="100%" height="50" alt="" style="border: 0;float: left;display: block;"></td>'+
'       </tr>'+
'       <tr>'+
'           <td align="center" style="background: #FFFFFF; font-size: 14px;"><img class="logotipo" src="logo-mtt@kreata.ee" width="180" height="165" alt="MTT / Ministerio de Transportes y Telecomunicaciones. Gobierno de Chile" style="border: 0;float: none;display: block; margin: 0 auto;"></td>'+
'       </tr>'+
'       <tr>'+
'           <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="spacer-600x50@kreata.ee" width="100%" height="50" alt="" style="border: 0;float: left;display: block;"></td>'+
'       </tr>'+

// '       <tr>'+
// '           <td align="left" style="background: #FFFFFF; font-size: 14px; line-height: 21px;">'+
// '               <p style="font-family: Arial; color: #424242; text-align: left; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;"><img src="firma@kreata.ee" width="135" height="70" alt="Firma Cecilia Godoy" style="border: 0;float: left;display: block;"></p>'+
// '           </td>'+
// '       </tr>'+
// '        <tr>'+
// '            <td align="left" style="background: #FFFFFF; font-size: 14px; line-height: 21px;">'+
// '                <p style="font-family: Arial; color: #424242; text-align: left; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 0; margin-left: 50px; line-height: 21px;"><strong style="color:#000000 !important;">Cecilia Godoy</strong><br />División de Transporte Público Regional</p>'+
// '            </td>'+
// '        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="spacer-600x50@kreata.ee" width="100%" height="50" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;"><p style="font-family: Arial; color: #FFFFFF; text-align: center; font-size: 16px; margin-top: 0px; margin-right: 50px; margin-bottom: 0; margin-left: 50px; line-height: 21px;"><a title="Más información" class="boton" href="#" target="_blank" style="display: block; font-size: 16px; background-color: #E22C2C; color: #FFFFFF; height: 25px; padding: 15px 0; vertical-aling: middle; text-align: center;"><strong style="color:#FFFFFF !important;">Más información</strong></a></p></td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF;font-size: 14px;"><img src="spacer-600x50@kreata.ee" width="100%" height="50" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF;font-size: 14px;"><img src="divider-gradient@kreata.ee" width="100%" height="40" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF;font-size: 14px;">'+
'                <table id="bodyTable_Footer" bgcolor="#ffffff" cellpadding="0" cellspacing="0" style="table-layout: fixed; vertical-align: top; width: 100%; min-width: 320px; max-width: 600px; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; border: 0;" valign="top" width="100%" border="0">'+
'                    <tr>'+
'                        <td width="50%" align="left" style="background: #FFFFFF; font-size: 11px; line-height: 19px;">'+
'                            <p style="font-family: Arial; color: #424242; text-align: left; font-size: 11px; margin-top: 0px; margin-right: 0; margin-bottom: 0; margin-left: 50px; line-height: 19px;"><img class="logo-footer" src="logo-footer@kreata.ee" width="110" height="55" alt="Ministerio de Transportes y Telecomunicaciones" style="border: 0;float: left;display: block;"></p>'+
'                        </td>'+
'                        <td width="50%" align="right" style="background: #FFFFFF; font-size: 11px; line-height: 19px;">'+
'                            <p style="font-family: Arial; color: #424242; text-align: right; font-size: 11px; margin-top: 0px; margin-right: 50px; margin-bottom: 0; margin-left: 0; line-height: 19px;">'+
'                                Amunátegui #139, Santiago<br />'+
'                                <a href="tel:+56224213000" title="Llamar al número +56 2 2421 3000" style="color: #E22C2C; text-decoration: underline; border: 0; margin-left: 10px;">+56 2 2421 3000</a> <a href="http://mtt.gob.cl/" target="_blank" title="Ir a http://mtt.gob.cl/" style="color: #E22C2C; text-decoration: underline; border: 0; margin-left: 10px;">mtt.gob.cl</a><br />'+
'                                Lunes a viernes 9:00 a 14:00 h.'+
'                            </p>'+
'                        </td>'+
'                    </tr>'+
'                </table>'+
'            </td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="spacer-600x40@kreata.ee" width="100%" height="40" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;">'+
'                <table id="bodyTable_Social" bgcolor="#ffffff" cellpadding="0" cellspacing="0" style="table-layout: fixed; vertical-align: top; width: 120px; min-width: 120px; max-width: 120px; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; border: 0;" valign="top" width="120" border="0">'+
'                    <tr>'+
'                        <td align="center" style="background: #FFFFFF; font-size: 14px;"><a href="http://www.facebook.com/pages/Ministerio-de-Transportes-y-Telecomunicaciones/416320378388221?ref=ts&fref=ts" target="_blank" title="Facebook"><img src="ico-facebook@kreata.ee" width="40" height="40" alt="Facebook" style="border: 0;float: left;display: block;"></a></td>'+
'                        <td align="center" style="background: #FFFFFF; font-size: 14px;"><a href="https://twitter.com/MTTChile" target="_blank" title="Twitter"><img src="ico-twitter@kreata.ee" width="40" height="40" alt="Twitter" style="border: 0;float: left;display: block;"></a></td>'+
'                        <td align="center" style="background: #FFFFFF; font-size: 14px;"><a href="https://www.youtube.com/channel/UCeulOAj58K6o5BjaFDBypiQ" target="_blank" title="Youtube"><img src="ico-youtube@kreata.ee" width="40" height="40" alt="Youtube" style="border: 0;float: left;display: block;"></a></td>'+
'                    </tr>'+
'                </table>'+
'            </td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="divider-600x70@kreata.ee" width="100%" height="70" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'        <tr>'+
'            <td align="justify" style="background: #FFFFFF; color: #98A3AF; font-size: 11px;line-height: 24px;">'+
'                <p class="texto-legal" style="font-family: Arial; color: #98A3AF; text-align: justify; font-size: 11px; margin-top: 0px; margin-right: 50px; margin-bottom: 0; margin-left: 50px; line-height: 24px;">La información contenida en esta transmisión es confidencial y no puede ser usada o difundida por personas distintas a su(s) destinatario(s). El uso no autorizado de la información contenida en esta transmisión puede ser sancionado criminalmente de conformidad con la ley chilena. Si ha recibido esta transmisión por error por favor destrúyala y notifique al remitente. Atendiendo que no existe certidumbre que el presente mensaje no será modificado como resultado de su transmisión o por correo electrónico, el Servicio de Registro Civil e Identificación no será responsable si el contenido del mismo ha sido modificado.</p>'+        
'            </td>'+
'        </tr>'+
'        <tr>'+
'            <td align="center" style="background: #FFFFFF; font-size: 14px;"><img src="divider-600x70@kreata.ee" width="100%" height="50" alt="" style="border: 0;float: left;display: block;"></td>'+
'        </tr>'+
'    </table>'+
'</center>'+
'</body>';


export const correoUsuarioActivadoToUser =  '<p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Estimado(a),</p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">A nuestra consideración, se informa la activación de su usuario en la plataforma. </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Cualquier duda o comentario, no dude en comunicarse con nosotros, </p>'+
'               <p style="font-family: Arial; color: #424242; text-align: justify; font-size: 14px; margin-top: 0px; margin-right: 50px; margin-bottom: 20px; margin-left: 50px; line-height: 21px;">Saludos Cordiales. </p>'