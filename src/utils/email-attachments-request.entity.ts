export class EmailAttachment {
    filename: string;
    //path: string;
    cid: string;
    content: string;
    encoding: string;
  
    constructor(filename, 
        //path, 
        cid, 
        content, 
        encoding) {
        this.filename = filename;
        //this.path = path;
        this.cid = cid;
        this.content = content;
        this.encoding = encoding
    }
  }