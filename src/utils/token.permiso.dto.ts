export class TokenPermisoDto {
    tokenCU: string;
    permisos?: string[];

    constructor(token: string, permisos?: string[]){
        this.tokenCU = token;
        this.permisos = permisos;
    }
}