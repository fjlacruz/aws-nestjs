import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { config_email, SECRET_PASS_CRYPTO, TOKEN_NOTIFICACION } from 'src/constantes';
import { DIVIDER_50X65_GIF, DIVIDER_600X50_GIF, DIVIDER_600X70_GIF, DIVIDER_GRADIENT, FIRMA_GIF, ICO_FACEBOOK_GIF, ICO_TWITTER_GIF, ICO_YOUTUBE_GIF, LOGO_FOOTER_PNG, LOGO_MTT_GIF, SPACER_600x40, SPACER_600x50, UPBAR_600x2_GIF } from 'src/shared/Constantes/constantesCorreoElectrónico';
import { Email } from './Email';

@Injectable()
export class UtilService {
  constructor(private readonly mailerService: MailerService) {}

  async prepararObjetoCorreo( destinatario: string, 
                              asuntoUsuarioActivado: string, 
                              cuerpoCorreo: string, 
                              descripcionCabeceraCorreo: string, 
                              cabeceraCuerpoCorreo: string, 
                              saludoCuerpoCorreo: string, 
                              textoCuerpoCorreo: string) {

    // Se sustituyen las imágenes correspondientes
    cuerpoCorreo = this.preparacionImagenesObjetoCorreo(cuerpoCorreo);

    cuerpoCorreo = this.preparacionTextosObjetoCorreo(cuerpoCorreo, descripcionCabeceraCorreo, cabeceraCuerpoCorreo, saludoCuerpoCorreo, textoCuerpoCorreo);

    const objetoCorreo: Email = {
      from: 'sglc@sglc-mail.ingeniaglobal.cl',
      to: destinatario,
      subject: asuntoUsuarioActivado,
      text: '',
      html: cuerpoCorreo,
      attachments: [],
    };

    // Se llama al servicio que envia el correo
    await this.enviarCorreo(objetoCorreo);
  }

  // Metodo para enviar correos. Recibe objetos con el destinatario, asunto, mensaje, etc...
  async enviarCorreo(objetoCorreo) {
    this.mailerService.sendMail(objetoCorreo);
  }

  // Metodo para encryptar el token segun la cadena secreta
  public encriptar() {
    // Se codifican los datos
    var crypto = require('crypto');
    //Construimos el header codificando el token con la clave secret
    const hash = crypto.createHash('sha256', SECRET_PASS_CRYPTO).update(TOKEN_NOTIFICACION).digest('hex');
    const token = 'Bearer ' + hash;

    return token;
  }

  preparacionTextosObjetoCorreo(cuerpoCorreo : string, descripcionCabeceraCorreo: string, cabeceraCuerpoCorreo: string, saludoCuerpoCorreo: string, textoCuerpoCorreo: string){
    // Sustituimos textos

    cuerpoCorreo = cuerpoCorreo.replace('descripcionCabeceraCorreo@kreata.ee', descripcionCabeceraCorreo);

    cuerpoCorreo = cuerpoCorreo.replace('cabeceraCuerpoCorreo@kreata.ee', cabeceraCuerpoCorreo);

    cuerpoCorreo = cuerpoCorreo.replace('saludoCuerpoCorreo@kreata.ee', saludoCuerpoCorreo);

    cuerpoCorreo = cuerpoCorreo.replace('textoCuerpoCorreo@kreata.ee', textoCuerpoCorreo);

  
    return cuerpoCorreo;
  }

  preparacionImagenesObjetoCorreo(cuerpoCorreo : string){
    // Sustituimos divisores

    cuerpoCorreo = cuerpoCorreo.replace('image-top@kreata.ee', UPBAR_600x2_GIF);

    cuerpoCorreo = cuerpoCorreo.replace('divider-50x65@kreata.ee', DIVIDER_50X65_GIF);

    cuerpoCorreo = cuerpoCorreo.replace('spacer-600x50@kreata.ee', SPACER_600x50);

    cuerpoCorreo = cuerpoCorreo.replace('divider-600x70@kreata.ee', DIVIDER_600X70_GIF);

    cuerpoCorreo = cuerpoCorreo.replace('divider-gradient@kreata.ee', DIVIDER_GRADIENT);

    cuerpoCorreo = cuerpoCorreo.replace('firma@kreata.ee', FIRMA_GIF);
    
    cuerpoCorreo = cuerpoCorreo.replace('ico-facebook@kreata.ee', ICO_FACEBOOK_GIF);   
    
    cuerpoCorreo = cuerpoCorreo.replace('ico-twitter@kreata.ee', ICO_TWITTER_GIF);  
    
    cuerpoCorreo = cuerpoCorreo.replace('ico-youtube@kreata.ee', ICO_YOUTUBE_GIF);      
    // Sustituimos en la firma el logo
    cuerpoCorreo = cuerpoCorreo.replace('logo-mtt@kreata.ee', LOGO_MTT_GIF);

    cuerpoCorreo = cuerpoCorreo.replace('spacer-600x50@kreata.ee', SPACER_600x50);
    cuerpoCorreo = cuerpoCorreo.replace('spacer-600x50@kreata.ee', SPACER_600x50);
    cuerpoCorreo = cuerpoCorreo.replace('spacer-600x50@kreata.ee', SPACER_600x50);
    cuerpoCorreo = cuerpoCorreo.replace('spacer-600x50@kreata.ee', SPACER_600x50);

    cuerpoCorreo = cuerpoCorreo.replace('spacer-600x40@kreata.ee', SPACER_600x40);

    cuerpoCorreo = cuerpoCorreo.replace('logo-footer@kreata.ee', LOGO_FOOTER_PNG);
    
    cuerpoCorreo = cuerpoCorreo.replace('spacer-600x50@kreata.ee', SPACER_600x50);

    cuerpoCorreo = cuerpoCorreo.replace('divider-600x70@kreata.ee', DIVIDER_600X70_GIF);
  
    return cuerpoCorreo;
  }  
 
}
