import { ApiPropertyOptional } from "@nestjs/swagger";

export class RunDTO {
    @ApiPropertyOptional()
    RUN:String;

    @ApiPropertyOptional()
    DV:String;
}
