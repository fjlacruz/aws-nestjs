import { Test, TestingModule } from '@nestjs/testing';
import { ValidarRunController } from './validar-run.controller';

describe('ValidarRunController', () => {
  let controller: ValidarRunController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ValidarRunController],
    }).compile();

    controller = module.get<ValidarRunController>(ValidarRunController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
