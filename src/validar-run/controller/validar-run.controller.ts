import { Controller } from '@nestjs/common';
import { Get, Param, Post, Body, Put, Patch } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiBearerAuth, ApiBasicAuth } from '@nestjs/swagger';
import { RunDTO } from '../DTO/run.dto';
import { ValidarRunService } from '../services/validar-run.service';


@ApiTags('Validar-run')
@Controller('validar-run')
export class ValidarRunController {

    constructor(private readonly validarRunService: ValidarRunService) { }
    @Post('crearComuna')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Servicio para validar un RUN' })
    async addComuna(
        @Body() runDTO: RunDTO) {
        const data = await this.validarRunService.validarRun(runDTO.RUN+"-"+runDTO.DV);
        return {data};
    }
}
