import { Test, TestingModule } from '@nestjs/testing';
import { ValidarRunService } from './validar-run.service';

describe('ValidarRunService', () => {
  let service: ValidarRunService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ValidarRunService],
    }).compile();

    service = module.get<ValidarRunService>(ValidarRunService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
