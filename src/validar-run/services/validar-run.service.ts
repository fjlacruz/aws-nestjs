import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class ValidarRunService {

    dv: any;
    serie: any;

    constructor() { }

    async validarRun(rut:String){
         if (rut.toString().trim() != '' && rut.toString().indexOf('-') > 0) {
            var caracteres = new Array();
            this.serie = new Array(2, 3, 4, 5, 6, 7);
            var dig = rut.toString().substr(rut.toString().length - 1, 1);
            rut = rut.toString().substr(0, rut.toString().length - 2);
            
            for (var i = 0; i < rut.length; i++) {
                caracteres[i] = parseInt(rut.charAt((rut.length - (i + 1))));
            }
    
            var sumatoria = 0;
            var k = 0;
            var resto = 0;
    
            for (var j = 0; j < caracteres.length; j++) {
                if (k === 6) {
                    k = 0;
                }
                sumatoria += parseInt(caracteres[j]) * parseInt(this.serie[k]);
                k++;
            }
    
            resto = sumatoria % 11;
            this.dv = 11 - resto;
    
            if (this.dv === 10) {
                this.dv = "K";
            }
            else if (this.dv === 11) {
                this.dv = 0;
            }
    
            if (this.dv.toString().trim().toUpperCase() == dig.toString().trim().toUpperCase())
                return true;
            else
                return false;
        }
        else {
            return false;
        }
    }
}
