import { Module } from '@nestjs/common';
import { ValidarRunController } from './controller/validar-run.controller';
import { ValidarRunService } from './services/validar-run.service';

@Module({
  providers: [ValidarRunService],
  exports: [ValidarRunService],
  controllers: [ValidarRunController]
})
export class ValidarRunModule {}
